.class public Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;
.super Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;
.source "ComposeAlbumSetAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter$MediaSelectionCallback;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ComposeAlbumSetAdapter"


# instance fields
.field private mAlbumTitleCountSize:I

.field private mAlbumTitleLeftMargin:I

.field private mAlbumTitleTextColor:I

.field private mAlbumTitleTextHeight:I

.field private mAlbumTitleTextSize:I

.field private mAlbumTitleTextWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "config"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;
    .param p4, "modeOption"    # I

    .prologue
    .line 72
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;I)V

    .line 69
    const v0, -0xa0a0a0b

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleTextColor:I

    .line 73
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->notifyLayoutChanged()V

    .line 74
    return-void
.end method

.method private drawDecorViewAlbum(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 34
    .param p1, "inter"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 286
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 287
    .local v12, "decorView":Lcom/sec/android/gallery3d/glcore/GlView;
    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispSelectCnt:Z

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSetSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v25

    .line 290
    .local v25, "selectedCount":I
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getLatestAlbumInfo()I

    move-result v4

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v5

    if-ne v4, v5, :cond_3

    const/4 v15, 0x1

    .line 291
    .local v15, "isNewMark":Z
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getUserSelectedAlbum()[Ljava/lang/String;

    move-result-object v33

    .line 292
    .local v33, "userSelect":[Ljava/lang/String;
    const/4 v4, 0x1

    aget-object v4, v33, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v32

    .line 293
    .local v32, "userItemid":I
    const/4 v4, 0x2

    aget-object v4, v33, v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v30

    .line 294
    .local v30, "userItemTakenTime":J
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v16

    .line 295
    .local v16, "isSecret":Z
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getPlayTypeIconRsrc(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v23

    .line 296
    .local v23, "rsrcPlayType":I
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getContentTypeIconRsrc(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v22

    .line 297
    .local v22, "rsrcMediaType":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewCameraIconMarginLeft()I

    move-result v17

    .line 298
    .local v17, "leftMargin":I
    move/from16 v11, v17

    .line 301
    .local v11, "bottomMargin":I
    if-eqz v15, :cond_1

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v4

    move/from16 v0, v32

    if-le v4, v0, :cond_0

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v4

    cmp-long v4, v4, v30

    if-gtz v4, :cond_1

    .line 302
    :cond_0
    const/4 v15, 0x0

    .line 304
    :cond_1
    if-nez v15, :cond_4

    if-nez v23, :cond_4

    if-nez v22, :cond_4

    if-nez v16, :cond_4

    .line 305
    const/4 v4, 0x0

    .line 393
    :goto_2
    return-object v4

    .line 287
    .end local v11    # "bottomMargin":I
    .end local v15    # "isNewMark":Z
    .end local v16    # "isSecret":Z
    .end local v17    # "leftMargin":I
    .end local v22    # "rsrcMediaType":I
    .end local v23    # "rsrcPlayType":I
    .end local v25    # "selectedCount":I
    .end local v30    # "userItemTakenTime":J
    .end local v32    # "userItemid":I
    .end local v33    # "userSelect":[Ljava/lang/String;
    :cond_2
    const/16 v25, 0x0

    goto :goto_0

    .line 290
    .restart local v25    # "selectedCount":I
    :cond_3
    const/4 v15, 0x0

    goto :goto_1

    .line 307
    .restart local v11    # "bottomMargin":I
    .restart local v15    # "isNewMark":Z
    .restart local v16    # "isSecret":Z
    .restart local v17    # "leftMargin":I
    .restart local v22    # "rsrcMediaType":I
    .restart local v23    # "rsrcPlayType":I
    .restart local v30    # "userItemTakenTime":J
    .restart local v32    # "userItemid":I
    .restart local v33    # "userSelect":[Ljava/lang/String;
    :cond_4
    if-eqz v15, :cond_6

    const v4, 0x7f020103

    move/from16 v0, v22

    if-eq v0, v4, :cond_5

    const v4, 0x7f020119

    move/from16 v0, v22

    if-ne v0, v4, :cond_6

    .line 309
    :cond_5
    const/4 v15, 0x0

    .line 312
    :cond_6
    if-nez v12, :cond_7

    .line 313
    new-instance v12, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v12    # "decorView":Lcom/sec/android/gallery3d/glcore/GlView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v12, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 315
    .restart local v12    # "decorView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_7
    const/4 v4, 0x3

    invoke-virtual {v12, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v21

    check-cast v21, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 316
    .local v21, "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v23, :cond_c

    .line 317
    if-nez v21, :cond_8

    .line 318
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResources:Landroid/content/res/Resources;

    move/from16 v0, v23

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    .line 319
    .local v13, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v21, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v21    # "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 320
    .restart local v21    # "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 321
    const/4 v4, 0x2

    const/4 v5, 0x2

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 322
    invoke-virtual {v13}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v13}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 323
    const/4 v4, 0x3

    move-object/from16 v0, v21

    invoke-virtual {v12, v0, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 329
    .end local v13    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_8
    :goto_3
    const/4 v4, 0x4

    invoke-virtual {v12, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 330
    .local v10, "attrView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v22, :cond_e

    .line 331
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResources:Landroid/content/res/Resources;

    move/from16 v0, v22

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    .line 332
    .restart local v13    # "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v10, :cond_d

    .line 333
    new-instance v10, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v10    # "attrView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v10, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 334
    .restart local v10    # "attrView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v10, v13}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 335
    const/4 v4, 0x1

    const/4 v5, 0x3

    invoke-virtual {v10, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 336
    invoke-virtual {v13}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v13}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-virtual {v10, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 337
    const/4 v4, 0x0

    const/4 v5, 0x0

    move/from16 v0, v17

    invoke-virtual {v10, v0, v4, v5, v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 338
    const/4 v4, 0x4

    invoke-virtual {v12, v10, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 346
    .end local v13    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_9
    :goto_4
    const/16 v4, 0xc

    invoke-virtual {v12, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v27

    check-cast v27, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 347
    .local v27, "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    const/4 v4, 0x6

    invoke-virtual {v12, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v19

    check-cast v19, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 348
    .local v19, "newMarkView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResources:Landroid/content/res/Resources;

    const v5, 0x7f0e04ad

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 349
    .local v18, "newLabel":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewMarkFontSize()I

    move-result v26

    .line 350
    .local v26, "textSize":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewAlbumFontColor()I

    move-result v7

    .line 351
    .local v7, "textColor":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewMarkTextTopMargin()I

    move-result v20

    .line 352
    .local v20, "newTextTopPadding":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    const-string v6, "albumViewMode"

    const/4 v8, 0x0

    invoke-static {v5, v6, v8}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxWidthPixel(I)I

    move-result v28

    .line 354
    .local v28, "textW":I
    if-gtz v25, :cond_f

    if-eqz v15, :cond_f

    .line 355
    if-nez v19, :cond_a

    .line 356
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020110

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    .line 357
    .restart local v13    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move/from16 v0, v26

    int-to-float v6, v0

    move/from16 v0, v28

    int-to-float v8, v0

    const/4 v9, 0x1

    invoke-static/range {v4 .. v9}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;Ljava/lang/String;FIFZ)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v27

    .line 358
    const/4 v4, 0x2

    const/4 v5, 0x1

    move-object/from16 v0, v27

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 359
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v4

    invoke-virtual {v4}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v14

    .line 360
    .local v14, "fm":Landroid/graphics/Paint$FontMetrics;
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget v5, v14, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget v6, v14, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v5, v6

    sub-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-int v4, v4

    add-int v29, v4, v20

    .line 361
    .local v29, "topMargin":I
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1, v5, v6}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 362
    new-instance v19, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v19    # "newMarkView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v19

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 363
    .restart local v19    # "newMarkView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 364
    const/4 v4, 0x3

    const/4 v5, 0x1

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 365
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getWidth()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNewLabelPadding()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getHeight()I

    move-result v5

    add-int v5, v5, v29

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 366
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewMarkTopMargin()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewMarkRightMargin()I

    move-result v6

    const/4 v8, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5, v6, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 367
    const/4 v4, 0x6

    move-object/from16 v0, v19

    invoke-virtual {v12, v0, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 368
    const/16 v4, 0xc

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 374
    .end local v13    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v14    # "fm":Landroid/graphics/Paint$FontMetrics;
    .end local v29    # "topMargin":I
    :cond_a
    :goto_5
    const/16 v4, 0xa

    invoke-virtual {v12, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v24

    check-cast v24, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 375
    .local v24, "secretIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v16, :cond_11

    .line 376
    if-nez v24, :cond_b

    .line 377
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 378
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0201a9

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    .line 381
    .restart local v13    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_6
    new-instance v24, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v24    # "secretIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v24

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 382
    .restart local v24    # "secretIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, v24

    invoke-virtual {v0, v13}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 383
    const/4 v4, 0x1

    const/4 v5, 0x3

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 384
    invoke-virtual {v13}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v13}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 385
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v17

    move/from16 v2, v17

    invoke-virtual {v0, v1, v4, v5, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 386
    const/4 v4, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 387
    const/16 v4, 0xa

    move-object/from16 v0, v24

    invoke-virtual {v12, v0, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .end local v13    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_b
    :goto_7
    move-object v4, v12

    .line 393
    goto/16 :goto_2

    .line 325
    .end local v7    # "textColor":I
    .end local v10    # "attrView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v18    # "newLabel":Ljava/lang/String;
    .end local v19    # "newMarkView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v20    # "newTextTopPadding":I
    .end local v24    # "secretIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v26    # "textSize":I
    .end local v27    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    .end local v28    # "textW":I
    :cond_c
    if-eqz v21, :cond_8

    .line 326
    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto/16 :goto_3

    .line 340
    .restart local v10    # "attrView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .restart local v13    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_d
    invoke-virtual {v10, v13}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    .line 342
    .end local v13    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_e
    if-eqz v10, :cond_9

    .line 343
    invoke-virtual {v12, v10}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto/16 :goto_4

    .line 370
    .restart local v7    # "textColor":I
    .restart local v18    # "newLabel":Ljava/lang/String;
    .restart local v19    # "newMarkView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .restart local v20    # "newTextTopPadding":I
    .restart local v26    # "textSize":I
    .restart local v27    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    .restart local v28    # "textW":I
    :cond_f
    if-eqz v19, :cond_a

    .line 371
    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto/16 :goto_5

    .line 380
    .restart local v24    # "secretIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0200d7

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    .restart local v13    # "drawable":Landroid/graphics/drawable/Drawable;
    goto :goto_6

    .line 389
    .end local v13    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_11
    if-eqz v24, :cond_b

    .line 390
    move-object/from16 v0, v24

    invoke-virtual {v12, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto :goto_7
.end method

.method private getContentTypeIconRsrc(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)I
    .locals 8
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const v3, 0x7f0201a1

    const v4, 0x7f0201a0

    const v7, 0x7f020119

    const v5, 0x7f020103

    const v6, 0x7f020102

    .line 397
    const/4 v1, 0x0

    .line 399
    .local v1, "rsrcMediaType":I
    instance-of v2, p1, Lcom/sec/android/gallery3d/data/LocalAlbum;

    if-nez v2, :cond_0

    instance-of v2, p1, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-nez v2, :cond_0

    instance-of v2, p2, Lcom/sec/android/gallery3d/data/LocalImage;

    if-nez v2, :cond_0

    instance-of v2, p2, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-eqz v2, :cond_11

    .line 401
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v0

    .line 402
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 403
    :cond_1
    if-eqz p2, :cond_5

    .line 404
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 409
    :cond_2
    :goto_0
    instance-of v2, p1, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v2, :cond_3

    move-object v2, p1

    check-cast v2, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->isCameraAlbum()Z

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    if-eqz p2, :cond_9

    instance-of v2, p2, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v2, :cond_9

    check-cast p2, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .end local p2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->isCameraItem()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 412
    :cond_4
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mEasyMode:Z

    if-eqz v2, :cond_7

    .line 413
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardCameraPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    move v2, v3

    .line 468
    .end local v0    # "path":Ljava/lang/String;
    :goto_1
    return v2

    .line 406
    .restart local v0    # "path":Ljava/lang/String;
    .restart local p2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .end local p2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_6
    move v2, v4

    .line 413
    goto :goto_1

    .line 416
    :cond_7
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardCameraPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    move v2, v5

    goto :goto_1

    :cond_8
    move v2, v6

    goto :goto_1

    .line 419
    :cond_9
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 420
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mEasyMode:Z

    if-eqz v2, :cond_a

    const v2, 0x7f0201ac

    goto :goto_1

    :cond_a
    move v2, v7

    goto :goto_1

    .line 422
    :cond_b
    instance-of v2, p1, Lcom/sec/android/gallery3d/data/FilterTypeSet;

    if-eqz v2, :cond_13

    .line 423
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isCameraPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 424
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mEasyMode:Z

    if-eqz v2, :cond_c

    move v2, v4

    .line 425
    goto :goto_1

    :cond_c
    move v2, v6

    .line 427
    goto :goto_1

    .line 428
    :cond_d
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardCameraPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 429
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mEasyMode:Z

    if-eqz v2, :cond_e

    move v2, v3

    .line 430
    goto :goto_1

    :cond_e
    move v2, v5

    .line 432
    goto :goto_1

    .line 433
    :cond_f
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 434
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mEasyMode:Z

    if-eqz v2, :cond_10

    .line 435
    const v2, 0x7f0201ac

    goto :goto_1

    :cond_10
    move v2, v7

    .line 437
    goto :goto_1

    .line 441
    .end local v0    # "path":Ljava/lang/String;
    .restart local p2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_11
    instance-of v2, p1, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;

    if-nez v2, :cond_12

    instance-of v2, p2, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-eqz v2, :cond_15

    .line 442
    :cond_12
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mEasyMode:Z

    if-eqz v2, :cond_14

    .line 443
    const v1, 0x7f0201b0

    .end local p2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_13
    :goto_2
    move v2, v1

    .line 468
    goto :goto_1

    .line 445
    .restart local p2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_14
    const v1, 0x7f0200da

    goto :goto_2

    .line 446
    :cond_15
    instance-of v2, p1, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;

    if-nez v2, :cond_16

    instance-of v2, p1, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;

    if-nez v2, :cond_16

    instance-of v2, p2, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-nez v2, :cond_16

    instance-of v2, p2, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-eqz v2, :cond_19

    .line 448
    :cond_16
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaidu:Z

    if-nez v2, :cond_18

    .line 449
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mEasyMode:Z

    if-eqz v2, :cond_17

    .line 450
    const v1, 0x7f0201af

    goto :goto_2

    .line 452
    :cond_17
    const v1, 0x7f0200d9

    goto :goto_2

    .line 454
    :cond_18
    const v1, 0x7f0200d8

    goto :goto_2

    .line 455
    :cond_19
    instance-of v2, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    if-nez v2, :cond_1a

    instance-of v2, p2, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v2, :cond_1c

    .line 456
    :cond_1a
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mEasyMode:Z

    if-eqz v2, :cond_1b

    .line 457
    const v1, 0x7f0201b3

    goto :goto_2

    .line 459
    :cond_1b
    const v1, 0x7f0201e2

    goto :goto_2

    .line 460
    :cond_1c
    instance-of v2, p1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;

    if-nez v2, :cond_1d

    instance-of v2, p1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;

    if-nez v2, :cond_1d

    instance-of v2, p2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    if-nez v2, :cond_1d

    instance-of v2, p2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    if-eqz v2, :cond_13

    .line 463
    :cond_1d
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mEasyMode:Z

    if-eqz v2, :cond_1e

    .line 464
    const v1, 0x7f0201b4

    goto :goto_2

    .line 466
    :cond_1e
    const v1, 0x7f0200dd

    goto :goto_2
.end method


# virtual methods
.method public addShowGroupId(Ljava/lang/Long;)Z
    .locals 1
    .param p1, "groupId"    # Ljava/lang/Long;

    .prologue
    .line 828
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->addShowGroupId(Ljava/lang/Long;)Z

    move-result v0

    return v0
.end method

.method public clearShowGroupId()V
    .locals 1

    .prologue
    .line 836
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->clearShowGroupId()V

    .line 837
    return-void
.end method

.method public drawAlbumLabel(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;)V
    .locals 17
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "sizeType"    # I
    .param p3, "album"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    .prologue
    .line 697
    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 698
    .local v7, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    move-object/from16 v0, p3

    iget v2, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemCount:I

    .line 699
    .local v2, "count":I
    if-eqz v7, :cond_1

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v1

    .line 701
    .local v1, "albumName":Ljava/lang/String;
    :goto_0
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v14, :cond_0

    instance-of v14, v7, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    if-nez v14, :cond_0

    instance-of v14, v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    if-nez v14, :cond_0

    .line 703
    if-eqz v1, :cond_0

    const-string v14, "/"

    invoke-virtual {v1, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 704
    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 705
    .local v12, "values":[Ljava/lang/String;
    const/4 v14, 0x1

    aget-object v1, v12, v14

    .line 709
    .end local v12    # "values":[Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-static {v14, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getAlbumNumber(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    .line 710
    .local v9, "photoNumlabel":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 711
    const/4 v14, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 712
    .local v10, "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    const/16 v14, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 714
    .local v4, "countView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    if-nez v4, :cond_3

    .line 715
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v14, :cond_2

    .line 716
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleCountSize:I

    int-to-float v14, v14

    const/high16 v15, -0x1000000

    invoke-static {v9, v14, v15}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v4

    .line 720
    :goto_1
    const/4 v14, 0x1

    const/4 v15, 0x2

    invoke-virtual {v4, v14, v15}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 721
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v14

    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setTextViewPaintForAlbum(Landroid/text/TextPaint;Z)V

    .line 722
    const/16 v14, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 728
    :goto_2
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getWidth()I

    move-result v5

    .line 730
    .local v5, "countViewWidth":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleTextSize:I

    int-to-float v14, v14

    invoke-static {v14}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getDefaultPaintForLabel(F)Landroid/text/TextPaint;

    move-result-object v8

    .line 731
    .local v8, "paint":Landroid/text/TextPaint;
    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v14}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setTextViewPaintForAlbum(Landroid/text/TextPaint;Z)V

    .line 732
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleTextWidth:I

    sub-int/2addr v15, v5

    int-to-float v15, v15

    invoke-static {v1, v14, v15, v8}, Lcom/sec/android/gallery3d/glcore/GlTextView;->ellipsizeString(Ljava/lang/String;Ljava/lang/String;FLandroid/text/TextPaint;)Ljava/lang/String;

    move-result-object v6

    .line 734
    .local v6, "label":Ljava/lang/String;
    if-nez v10, :cond_5

    .line 735
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v14, :cond_4

    .line 736
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleTextSize:I

    int-to-float v14, v14

    const/high16 v15, -0x1000000

    invoke-static {v6, v14, v15}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v10

    .line 740
    :goto_3
    const/4 v14, 0x1

    const/4 v15, 0x2

    invoke-virtual {v10, v14, v15}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 741
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v14

    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setTextViewPaintForAlbum(Landroid/text/TextPaint;Z)V

    .line 742
    const/4 v14, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v14}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 748
    :goto_4
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getWidth()I

    move-result v13

    .line 749
    .local v13, "width":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleTextWidth:I

    sub-int/2addr v14, v5

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleTextHeight:I

    invoke-virtual {v10, v14, v15}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setSize(II)V

    .line 751
    const/4 v11, 0x0

    .local v11, "titleLeftMargin":I
    const/4 v3, 0x0

    .line 752
    .local v3, "countLeftMargin":I
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mIsRTL:Z

    if-eqz v14, :cond_6

    .line 753
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleTextWidth:I

    add-int v15, v13, v5

    sub-int v11, v14, v15

    .line 754
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleTextWidth:I

    sub-int v3, v14, v5

    .line 759
    :goto_5
    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v10, v11, v14, v15, v0}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 760
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v14

    invoke-virtual {v14}, Landroid/text/TextPaint;->descent()F

    move-result v14

    float-to-int v14, v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v4, v3, v14, v15, v0}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 762
    return-void

    .line 699
    .end local v1    # "albumName":Ljava/lang/String;
    .end local v3    # "countLeftMargin":I
    .end local v4    # "countView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    .end local v5    # "countViewWidth":I
    .end local v6    # "label":Ljava/lang/String;
    .end local v8    # "paint":Landroid/text/TextPaint;
    .end local v9    # "photoNumlabel":Ljava/lang/String;
    .end local v10    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    .end local v11    # "titleLeftMargin":I
    .end local v13    # "width":I
    :cond_1
    const-string v1, ""

    goto/16 :goto_0

    .line 718
    .restart local v1    # "albumName":Ljava/lang/String;
    .restart local v4    # "countView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    .restart local v9    # "photoNumlabel":Ljava/lang/String;
    .restart local v10    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    :cond_2
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleCountSize:I

    int-to-float v14, v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleTextColor:I

    invoke-static {v9, v14, v15}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v4

    goto/16 :goto_1

    .line 724
    :cond_3
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleCountSize:I

    int-to-float v15, v15

    invoke-virtual {v14, v15}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 725
    invoke-virtual {v4, v9}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setText(Ljava/lang/String;)V

    .line 726
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v14

    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setTextViewPaintForAlbum(Landroid/text/TextPaint;Z)V

    goto/16 :goto_2

    .line 738
    .restart local v5    # "countViewWidth":I
    .restart local v6    # "label":Ljava/lang/String;
    .restart local v8    # "paint":Landroid/text/TextPaint;
    :cond_4
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleTextSize:I

    int-to-float v14, v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleTextColor:I

    invoke-static {v6, v14, v15}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v10

    goto/16 :goto_3

    .line 744
    :cond_5
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleTextSize:I

    int-to-float v15, v15

    invoke-virtual {v14, v15}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 745
    invoke-virtual {v10, v6}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setText(Ljava/lang/String;)V

    .line 746
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v14

    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setTextViewPaintForAlbum(Landroid/text/TextPaint;Z)V

    goto/16 :goto_4

    .line 756
    .restart local v3    # "countLeftMargin":I
    .restart local v11    # "titleLeftMargin":I
    .restart local v13    # "width":I
    :cond_6
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleLeftMargin:I

    .line 757
    move v3, v13

    goto :goto_5
.end method

.method protected drawAlbumLabelBG(Lcom/sec/android/gallery3d/glcore/GlView;I)Lcom/sec/android/gallery3d/glcore/GlImageView;
    .locals 2
    .param p1, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p2, "sizeType"    # I

    .prologue
    .line 681
    if-nez p1, :cond_0

    .line 682
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 693
    .local v0, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_0
    return-object v0

    .end local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_0
    move-object v0, p1

    .line 684
    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_0
.end method

.method public drawBorder(Lcom/sec/android/gallery3d/glcore/GlView;Z)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 7
    .param p1, "glView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p2, "visible"    # Z

    .prologue
    const/16 v6, 0xd

    const/4 v5, 0x2

    .line 873
    if-nez p1, :cond_0

    .line 874
    new-instance p1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local p1    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {p1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 876
    .restart local p1    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_0
    invoke-virtual {p1, v6}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 877
    .local v0, "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_2

    .line 878
    if-nez v0, :cond_1

    .line 879
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f02026e

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 880
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 881
    .restart local v0    # "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 882
    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setScaleRatio(F)V

    .line 883
    invoke-virtual {v0, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 884
    invoke-virtual {p1, v0, v6}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 892
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    :goto_0
    return-object p1

    .line 887
    :cond_2
    if-eqz v0, :cond_1

    .line 888
    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto :goto_0
.end method

.method protected drawCheckBox(Lcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;)Lcom/sec/android/gallery3d/glcore/GlImageView;
    .locals 5
    .param p1, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p2, "album"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    .prologue
    .line 663
    if-nez p1, :cond_0

    .line 664
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 668
    .local v0, "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_0
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v3, :cond_1

    const v2, 0x7f020122

    .line 669
    .local v2, "checkOnId":I
    :goto_1
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v3, :cond_2

    const v1, 0x7f020121

    .line 670
    .local v1, "checkOffId":I
    :goto_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v4, p2, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 671
    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 674
    :goto_3
    return-object v0

    .end local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v1    # "checkOffId":I
    .end local v2    # "checkOnId":I
    :cond_0
    move-object v0, p1

    .line 666
    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_0

    .line 668
    :cond_1
    const v2, 0x7f0204ad

    goto :goto_1

    .line 669
    .restart local v2    # "checkOnId":I
    :cond_2
    const v1, 0x7f0204a6

    goto :goto_2

    .line 673
    .restart local v1    # "checkOffId":I
    :cond_3
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    goto :goto_3
.end method

.method public drawDecorViewOnRequest(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 1
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 866
    const/4 v0, 0x0

    return-object v0
.end method

.method protected drawSelectedCountView(Lcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;)Lcom/sec/android/gallery3d/glcore/GlImageView;
    .locals 13
    .param p1, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p2, "album"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    .prologue
    .line 766
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 767
    .local v1, "dimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    iget-object v6, p2, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 771
    .local v6, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez p1, :cond_0

    .line 772
    new-instance v5, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v5, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 773
    .local v5, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResources:Landroid/content/res/Resources;

    const v11, 0x7f020208

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 775
    .local v2, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v5, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 776
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v5, v9, v10, v11, v12}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 780
    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_0
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSetSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v0

    .line 781
    .local v0, "count":I
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumSelectCountFontColor()I

    move-result v3

    .line 782
    .local v3, "fontColor":I
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumSelectCountFontSize()I

    move-result v4

    .line 783
    .local v4, "fontSize":I
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    .line 784
    .local v7, "selectedStr":Ljava/lang/String;
    const/4 v9, 0x2

    invoke-virtual {v5, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 785
    .local v8, "selectedTextView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    if-nez v8, :cond_1

    .line 786
    int-to-float v9, v4

    invoke-static {v7, v9, v3}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v8

    .line 787
    const/4 v9, 0x2

    const/4 v10, 0x2

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 788
    const/4 v9, 0x2

    invoke-virtual {v5, v8, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 792
    :goto_1
    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mRetLevel:I

    .line 793
    return-object v5

    .end local v0    # "count":I
    .end local v3    # "fontColor":I
    .end local v4    # "fontSize":I
    .end local v5    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v7    # "selectedStr":Ljava/lang/String;
    .end local v8    # "selectedTextView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    :cond_0
    move-object v5, p1

    .line 778
    check-cast v5, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v5    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_0

    .line 790
    .restart local v0    # "count":I
    .restart local v3    # "fontColor":I
    .restart local v4    # "fontSize":I
    .restart local v7    # "selectedStr":Ljava/lang/String;
    .restart local v8    # "selectedTextView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    :cond_1
    invoke-virtual {v8, v7}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setText(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected drawThumbnailImage(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;Lcom/sec/android/gallery3d/data/MediaItem;II)Z
    .locals 3
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "imageItem"    # Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p4, "subPosition"    # I
    .param p5, "totalCount"    # I

    .prologue
    .line 632
    const/4 v0, 0x0

    .line 634
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz p3, :cond_0

    if-nez p2, :cond_1

    .line 635
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->readyDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 636
    const/4 v2, 0x0

    .line 656
    :goto_0
    return v2

    .line 638
    :cond_1
    iget-object v0, p2, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 639
    if-nez v0, :cond_2

    .line 640
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->readyDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 656
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    .line 642
    :cond_2
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v1

    .line 643
    .local v1, "rotation":I
    invoke-virtual {p1, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageBitmap(Landroid/graphics/Bitmap;I)V

    goto :goto_1
.end method

.method public forceReload()V
    .locals 1

    .prologue
    .line 849
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->forceReload()V

    .line 850
    return-void
.end method

.method public getAlbumIndexFromFilePath(Ljava/lang/String;)I
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 812
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getAlbumIndexFromFilePath(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getAlbumIndexFromMediaSet(Lcom/sec/android/gallery3d/data/MediaSet;)I
    .locals 1
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 816
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getAlbumIndexFromMediaSet(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v0

    return v0
.end method

.method public getAllCount()I
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getAllCount()I

    move-result v0

    return v0
.end method

.method public getAllItem(I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "albumIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 529
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getAllItem(I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getAllItemCount(I)I
    .locals 1
    .param p1, "albumIndex"    # I

    .prologue
    .line 534
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getAllItemCount(I)I

    move-result v0

    return v0
.end method

.method public getCodeForMediaItem(Lcom/sec/android/gallery3d/data/MediaItem;)I
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getCodeForMediaItem(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 504
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getCount()I

    move-result v0

    return v0
.end method

.method public getCount(I)I
    .locals 1
    .param p1, "albumIndex"    # I

    .prologue
    .line 509
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getCount(I)I

    move-result v0

    return v0
.end method

.method public getCurrentState(II)I
    .locals 9
    .param p1, "position"    # I
    .param p2, "subPosition"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 141
    const/4 v3, 0x0

    .line 143
    .local v3, "retVal":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget v7, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-lt p1, v7, :cond_1

    .line 144
    const-string v5, "ComposeAlbumSetAdapter"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getSelectState pos = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", size = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget v8, v8, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    :cond_0
    :goto_0
    return v6

    .line 147
    :cond_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v7, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v7, p1

    .line 148
    .local v0, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-eqz v0, :cond_0

    .line 151
    if-gez p2, :cond_6

    .line 152
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v8, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSetSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v1

    .line 153
    .local v1, "count":I
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAllSelectMode:Z

    if-eqz v7, :cond_3

    iget v7, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-lt v1, v7, :cond_2

    move v4, v5

    .line 154
    .local v4, "selected":Z
    :goto_1
    if-eqz v4, :cond_5

    :goto_2
    move v6, v5

    goto :goto_0

    .end local v4    # "selected":Z
    :cond_2
    move v4, v6

    .line 153
    goto :goto_1

    :cond_3
    if-lez v1, :cond_4

    move v4, v5

    goto :goto_1

    :cond_4
    move v4, v6

    goto :goto_1

    .restart local v4    # "selected":Z
    :cond_5
    move v5, v6

    .line 154
    goto :goto_2

    .line 156
    .end local v1    # "count":I
    .end local v4    # "selected":Z
    :cond_6
    iget-object v7, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    array-length v7, v7

    if-lt p2, v7, :cond_7

    .line 157
    const-string v5, "ComposeAlbumSetAdapter"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getSelectState subPos = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", length = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    array-length v8, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 160
    :cond_7
    iget-object v7, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    aget-object v2, v7, p2

    .line 161
    .local v2, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v2, :cond_8

    .line 162
    const-string v5, "ComposeAlbumSetAdapter"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getSelectState mediaItem is null = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", sub = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 165
    :cond_8
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v7

    if-eqz v7, :cond_9

    move v3, v5

    :goto_3
    move v6, v3

    .line 166
    goto/16 :goto_0

    :cond_9
    move v3, v6

    .line 165
    goto :goto_3
.end method

.method public getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p1, "albumIndex"    # I
    .param p2, "photoIndex"    # I

    .prologue
    .line 524
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemImage(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "albumIndex"    # I
    .param p2, "photoIndex"    # I

    .prologue
    .line 568
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getImage(II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getLatestAlbumInfo()I
    .locals 3

    .prologue
    .line 472
    const/4 v0, -0x1

    .line 473
    .local v0, "id":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    const-string v2, "latest_update_album"

    invoke-static {v1, v2, v0}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 474
    return v0
.end method

.method public getLineCount(I)I
    .locals 1
    .param p1, "albumIndex"    # I

    .prologue
    .line 519
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getLineCount(I)I

    move-result v0

    return v0
.end method

.method public getScreenNailImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 573
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    if-nez v0, :cond_0

    .line 574
    const/4 v0, 0x0

    .line 575
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public getShowGroupId()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 832
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getShowGroupId()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getSource()Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "albumIndex"    # I

    .prologue
    .line 499
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    return-object v0
.end method

.method public getUserSelectedAlbum()[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 478
    const/4 v3, 0x3

    new-array v1, v3, [Ljava/lang/String;

    const-string v3, "-1"

    aput-object v3, v1, v5

    const/4 v3, 0x1

    const-string v4, "-1"

    aput-object v4, v1, v3

    const-string v3, "-1"

    aput-object v3, v1, v6

    .line 481
    .local v1, "info":[Ljava/lang/String;
    invoke-virtual {v1}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 484
    .local v2, "tempInfo":[Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    const-string/jumbo v4, "user_selected_album"

    invoke-static {v3, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadStringKey(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 486
    .local v0, "Albuminfo":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 487
    const-string v3, ";"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 489
    :cond_0
    array-length v3, v2

    if-ne v3, v6, :cond_1

    .line 490
    array-length v3, v2

    invoke-static {v2, v5, v1, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 494
    :goto_0
    return-object v1

    .line 492
    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public getView(IILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 8
    .param p1, "position"    # I
    .param p2, "subPosition"    # I
    .param p3, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p4, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p5, "ext"    # I
    .param p6, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 594
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mRetLevel:I

    .line 595
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-lt p1, v0, :cond_1

    .line 626
    :cond_0
    :goto_0
    return-object v1

    .line 598
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v6, v0, p1

    .line 599
    .local v6, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-eqz v6, :cond_0

    iget-object v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_0

    iget v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-eqz v0, :cond_0

    .line 601
    if-gez p2, :cond_4

    .line 602
    const/16 v0, -0xa

    if-ne p2, v0, :cond_2

    .line 603
    invoke-virtual {p0, p3, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->drawCheckBox(Lcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;)Lcom/sec/android/gallery3d/glcore/GlImageView;

    move-result-object v1

    .local v1, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_0

    .line 604
    .end local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_2
    const/4 v0, -0x4

    if-ne p2, v0, :cond_3

    .line 605
    invoke-virtual {p0, p3, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->drawSelectedCountView(Lcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;)Lcom/sec/android/gallery3d/glcore/GlImageView;

    move-result-object v1

    .restart local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_0

    .line 607
    .end local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_3
    invoke-virtual {p0, p3, p2}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->drawAlbumLabelBG(Lcom/sec/android/gallery3d/glcore/GlView;I)Lcom/sec/android/gallery3d/glcore/GlImageView;

    move-result-object v1

    .line 608
    .restart local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {p0, v1, p2, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->drawAlbumLabel(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;)V

    goto :goto_0

    .line 614
    .end local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_4
    if-nez p3, :cond_5

    .line 615
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 620
    .restart local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_1
    iget-object v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    aget-object v3, v0, p2

    .line 621
    .local v3, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemImage:[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    aget-object v2, v0, p2

    .line 622
    .local v2, "imageItem":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    iget v5, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    move-object v0, p0

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->drawThumbnailImage(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;Lcom/sec/android/gallery3d/data/MediaItem;II)Z

    move-result v7

    .line 623
    .local v7, "result":Z
    if-eqz v7, :cond_0

    .line 624
    iget-object v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mUpdated:[B

    aget-byte v0, v0, p2

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mRetLevel:I

    goto :goto_0

    .end local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v2    # "imageItem":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    .end local v3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v7    # "result":Z
    :cond_5
    move-object v1, p3

    .line 617
    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_1
.end method

.method public getViewInfo(IIILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;Z)Z
    .locals 20
    .param p1, "position"    # I
    .param p2, "subPosition"    # I
    .param p3, "option"    # I
    .param p4, "inter"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;
    .param p5, "borderVisible"    # Z

    .prologue
    .line 171
    const/4 v13, 0x0

    .line 177
    .local v13, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    const/16 v16, 0x0

    .line 178
    .local v16, "cropRect":Landroid/graphics/Rect;
    const/4 v4, 0x0

    .local v4, "width":I
    const/4 v5, 0x0

    .line 180
    .local v5, "height":I
    if-nez p4, :cond_0

    const/4 v7, 0x0

    .line 282
    :goto_0
    return v7

    .line 181
    :cond_0
    const/4 v7, 0x0

    move-object/from16 v0, p4

    iput-object v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mBitmap:Landroid/graphics/Bitmap;

    .line 182
    const/4 v7, 0x0

    move-object/from16 v0, p4

    iput-boolean v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mSelected:Z

    .line 183
    const/4 v7, 0x0

    move-object/from16 v0, p4

    iput-object v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 184
    const/4 v7, 0x1

    move-object/from16 v0, p4

    iput-boolean v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mReorderEnable:Z

    .line 186
    if-eqz p3, :cond_2

    .line 187
    const/16 v19, 0x0

    .line 188
    .local v19, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    move-object/from16 v17, v0

    .line 189
    .local v17, "imageItem":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    invoke-virtual/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->getItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v18

    .line 190
    .local v18, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    move-object/from16 v0, v17

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 191
    .local v14, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v7

    move-object/from16 v0, p4

    iput v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mRotation:I

    .line 228
    :goto_1
    if-eqz v13, :cond_1

    .line 229
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v8, v13, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v7

    move-object/from16 v0, p4

    iput-boolean v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mSelected:Z

    .line 231
    :cond_1
    if-nez v14, :cond_c

    move-object/from16 v0, p4

    iget v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mIsBroken:I

    if-nez v7, :cond_c

    .line 232
    const/4 v7, 0x0

    move-object/from16 v0, p4

    iput-object v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mCropRect:Landroid/graphics/Rect;

    .line 233
    const/4 v7, 0x0

    move-object/from16 v0, p4

    iput-object v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDynamicCropRect:Landroid/graphics/Rect;

    .line 234
    const/4 v7, 0x0

    move-object/from16 v0, p4

    iput-object v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mBitmap:Landroid/graphics/Bitmap;

    .line 235
    const/4 v7, 0x0

    goto :goto_0

    .line 194
    .end local v14    # "bitmap":Landroid/graphics/Bitmap;
    .end local v17    # "imageItem":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    .end local v18    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v19    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_2
    if-ltz p1, :cond_3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget v7, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    move/from16 v0, p1

    if-lt v0, v7, :cond_4

    .line 195
    :cond_3
    const-string v7, "ComposeAlbumSetAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "GetViewInfo pos = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, p1

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", size = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget v9, v9, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 199
    :cond_4
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v7, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v13, v7, p1

    .line 200
    if-eqz v13, :cond_5

    iget v7, v13, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    move/from16 v0, p2

    if-lt v0, v7, :cond_7

    .line 201
    :cond_5
    if-nez v13, :cond_6

    .line 202
    const-string v7, "ComposeAlbumSetAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "GetViewInfo album is null = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, p2

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    :goto_2
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 204
    :cond_6
    const-string v7, "ComposeAlbumSetAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "GetViewInfo position is over index  = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, p2

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", length = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v13, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 208
    :cond_7
    iget-object v7, v13, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    aget-object v18, v7, p2

    .line 209
    .restart local v18    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v18, :cond_8

    .line 210
    const-string v7, "ComposeAlbumSetAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "GetViewInfo mediaItem is null = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, p1

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", sub = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, p2

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 213
    :cond_8
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v7

    move-object/from16 v0, p4

    iput v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mRotation:I

    .line 214
    iget-object v0, v13, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    move-object/from16 v19, v0

    .line 215
    .restart local v19    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v7, v13, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemImage:[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    aget-object v17, v7, p2

    .line 216
    .restart local v17    # "imageItem":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    if-nez v17, :cond_9

    const/4 v14, 0x0

    .line 218
    .restart local v14    # "bitmap":Landroid/graphics/Bitmap;
    :goto_3
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v7

    if-eqz v7, :cond_b

    .line 219
    const/4 v7, 0x4

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v8

    if-ne v7, v8, :cond_a

    .line 220
    const/4 v7, 0x2

    move-object/from16 v0, p4

    iput v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mIsBroken:I

    goto/16 :goto_1

    .line 216
    .end local v14    # "bitmap":Landroid/graphics/Bitmap;
    :cond_9
    move-object/from16 v0, v17

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_3

    .line 222
    .restart local v14    # "bitmap":Landroid/graphics/Bitmap;
    :cond_a
    const/4 v7, 0x1

    move-object/from16 v0, p4

    iput v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mIsBroken:I

    goto/16 :goto_1

    .line 224
    :cond_b
    const/4 v7, 0x0

    move-object/from16 v0, p4

    iput v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mIsBroken:I

    goto/16 :goto_1

    .line 238
    :cond_c
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPickMode(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_d

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumMode:Z

    if-eqz v7, :cond_13

    .line 239
    :cond_d
    const/4 v7, 0x0

    move-object/from16 v0, p4

    iput-boolean v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispExpansionIcon:Z

    .line 244
    :goto_4
    if-eqz v19, :cond_15

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumMode:Z

    if-eqz v7, :cond_15

    .line 245
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, v19

    move-object/from16 v3, v18

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->drawDecorViewAlbum(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v7

    move-object/from16 v0, p4

    iput-object v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 252
    :cond_e
    :goto_5
    if-eqz v19, :cond_f

    move-object/from16 v0, v19

    instance-of v7, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-nez v7, :cond_f

    .line 253
    const/4 v7, 0x0

    move-object/from16 v0, p4

    iput-boolean v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mReorderEnable:Z

    .line 255
    :cond_f
    move-object/from16 v0, p4

    iget-object v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    move-object/from16 v0, p0

    move/from16 v1, p5

    invoke-virtual {v0, v7, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->drawBorder(Lcom/sec/android/gallery3d/glcore/GlView;Z)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v7

    move-object/from16 v0, p4

    iput-object v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 257
    move-object/from16 v0, p4

    iget v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mIsBroken:I

    if-nez v7, :cond_16

    .line 258
    if-eqz v14, :cond_10

    .line 259
    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 260
    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 263
    :cond_10
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceThumbnail:Z

    if-eqz v7, :cond_11

    move-object/from16 v0, v18

    instance-of v7, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v7, :cond_11

    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v7

    if-nez v7, :cond_11

    if-eqz v17, :cond_11

    move-object/from16 v0, v17

    iget v7, v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThmType:I

    const/4 v8, 0x3

    if-eq v7, v8, :cond_11

    .line 267
    check-cast v18, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v18    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/LocalImage;->getRectOfAllFaces()Landroid/graphics/RectF;

    move-result-object v6

    .line 268
    .local v6, "faceRect":Landroid/graphics/RectF;
    if-eqz v6, :cond_11

    .line 269
    move-object/from16 v0, p4

    iget v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjWidth:F

    move-object/from16 v0, p4

    iget v8, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjHeight:F

    move-object/from16 v0, p4

    iget v9, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mRotation:I

    invoke-static/range {v4 .. v9}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->calcFaceCropRect(IILandroid/graphics/RectF;FFI)Landroid/graphics/Rect;

    move-result-object v16

    .line 272
    .end local v6    # "faceRect":Landroid/graphics/RectF;
    :cond_11
    if-nez v16, :cond_12

    .line 273
    int-to-float v8, v4

    int-to-float v9, v5

    move-object/from16 v0, p4

    iget v10, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjWidth:F

    move-object/from16 v0, p4

    iget v11, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjHeight:F

    move-object/from16 v0, p4

    iget v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mRotation:I

    move-object/from16 v7, p0

    invoke-virtual/range {v7 .. v12}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->calcCenterCropRect(FFFFI)Landroid/graphics/Rect;

    move-result-object v16

    .line 279
    :cond_12
    :goto_6
    move-object/from16 v0, v16

    move-object/from16 v1, p4

    iput-object v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mCropRect:Landroid/graphics/Rect;

    .line 281
    move-object/from16 v0, p4

    iput-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mBitmap:Landroid/graphics/Bitmap;

    .line 282
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 241
    .restart local v18    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_13
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-boolean v7, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSelectionSet:Z

    if-nez v7, :cond_14

    const/4 v7, 0x1

    :goto_7
    move-object/from16 v0, p4

    iput-boolean v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispExpansionIcon:Z

    goto/16 :goto_4

    :cond_14
    const/4 v7, 0x0

    goto :goto_7

    .line 247
    :cond_15
    move-object/from16 v0, p4

    iget-object v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    if-nez v7, :cond_e

    if-eqz v17, :cond_e

    move-object/from16 v0, v17

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

    if-eqz v7, :cond_e

    .line 248
    move-object/from16 v0, v17

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

    move-object/from16 v0, p4

    iput-object v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    goto/16 :goto_5

    .line 276
    :cond_16
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResources:Landroid/content/res/Resources;

    move-object/from16 v0, p4

    iget v9, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mIsBroken:I

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/gallery3d/util/ResourceManager;->getBrokenThumnailSize(Landroid/content/res/Resources;I)I

    move-result v15

    .line 277
    .local v15, "brokenImageSize":I
    int-to-float v8, v15

    int-to-float v9, v15

    move-object/from16 v0, p4

    iget v10, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjWidth:F

    move-object/from16 v0, p4

    iget v11, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjHeight:F

    move-object/from16 v0, p4

    iget v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mRotation:I

    move-object/from16 v7, p0

    invoke-virtual/range {v7 .. v12}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->calcCenterCropRect(FFFFI)Landroid/graphics/Rect;

    move-result-object v16

    goto :goto_6
.end method

.method public hasLocalMediaSet()Z
    .locals 9

    .prologue
    .line 797
    const/4 v3, 0x0

    .line 798
    .local v3, "result":Z
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v4, v5, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 799
    .local v4, "topMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v0

    .line 800
    .local v0, "albumCount":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ABCO"

    mul-int/lit16 v7, v0, 0x3e8

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5, v6, v7, v8}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 801
    const/4 v1, 0x0

    .local v1, "ix":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 802
    invoke-virtual {v4, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 803
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    instance-of v5, v2, Lcom/sec/android/gallery3d/data/LocalAlbum;

    if-nez v5, :cond_0

    instance-of v5, v2, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v5, :cond_2

    .line 804
    :cond_0
    const/4 v3, 0x1

    .line 808
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    return v3

    .line 801
    .restart local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public isBurstShot(Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaSet;)Z
    .locals 6
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 855
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->isShowAllGroup()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getShowGroupId()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p2, p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isBurstShot(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 860
    .local v1, "result":Z
    :goto_0
    return v1

    .line 855
    .end local v1    # "result":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 857
    :catch_0
    move-exception v0

    .line 858
    .local v0, "e":Ljava/lang/NullPointerException;
    const/4 v1, 0x0

    .restart local v1    # "result":Z
    goto :goto_0
.end method

.method public isShowAllGroup()Z
    .locals 1

    .prologue
    .line 845
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->isShowAllGroup()Z

    move-result v0

    return v0
.end method

.method public notifyLayoutChanged()V
    .locals 2

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mEasyMode:Z

    if-nez v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0d030f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleTextHeight:I

    .line 80
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0d0313

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleTextSize:I

    .line 81
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0d0314

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleCountSize:I

    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0d0311

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleTextWidth:I

    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0d0315

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleLeftMargin:I

    .line 84
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0b005d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleTextColor:I

    .line 91
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0d0317

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleTextHeight:I

    .line 87
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0d031a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleTextSize:I

    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0d031b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleCountSize:I

    .line 89
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0d0319

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mAlbumTitleTextWidth:I

    goto :goto_0
.end method

.method public requestScreenNail(II)Z
    .locals 2
    .param p1, "code"    # I
    .param p2, "type"    # I

    .prologue
    .line 549
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->requestScreenNail(IIZ)Z

    move-result v0

    return v0
.end method

.method public requestScreenNail(Lcom/sec/android/gallery3d/data/MediaItem;I)Z
    .locals 2
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "type"    # I

    .prologue
    .line 544
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->requestScreenNail(Lcom/sec/android/gallery3d/data/MediaItem;IZ)Z

    move-result v0

    return v0
.end method

.method public requestScreenNailUrgent(II)Z
    .locals 2
    .param p1, "code"    # I
    .param p2, "type"    # I

    .prologue
    .line 559
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->requestScreenNail(IIZ)Z

    move-result v0

    return v0
.end method

.method public requestScreenNailUrgent(Lcom/sec/android/gallery3d/data/MediaItem;I)Z
    .locals 2
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "type"    # I

    .prologue
    .line 554
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->requestScreenNail(Lcom/sec/android/gallery3d/data/MediaItem;IZ)Z

    move-result v0

    return v0
.end method

.method public requestToNext(I)Z
    .locals 1
    .param p1, "toward"    # I

    .prologue
    .line 539
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->requestToNext(I)Z

    move-result v0

    return v0
.end method

.method public requestUpdateBitmap(III)V
    .locals 1
    .param p1, "code"    # I
    .param p2, "level"    # I
    .param p3, "type"    # I

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->requestUpdateBitmap(III)V

    .line 133
    return-void
.end method

.method public setActiveWindow(IIII)V
    .locals 1
    .param p1, "activeStart"    # I
    .param p2, "activeEnd"    # I
    .param p3, "contentStart"    # I
    .param p4, "contentEnd"    # I

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->setActiveWindow(IIII)V

    .line 123
    return-void
.end method

.method public setFilterMimeType(Ljava/lang/String;)V
    .locals 0
    .param p1, "filterMimeType"    # Ljava/lang/String;

    .prologue
    .line 825
    return-void
.end method

.method public setFocusAlbumFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 821
    return-void
.end method

.method public setInitThumbType(I)V
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    int-to-byte v1, p1

    iput-byte v1, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mLevel:B

    .line 113
    return-void
.end method

.method public setScreenNailImage(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;I)V
    .locals 8
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "type"    # I

    .prologue
    .line 580
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    if-eqz v0, :cond_0

    .line 581
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->recycle()V

    .line 583
    :cond_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    new-instance v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v4, 0x3

    move-object v2, p1

    move v5, p3

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;IIILandroid/graphics/Bitmap;)V

    iput-object v0, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    .line 584
    return-void
.end method

.method public setSelectionSource(Lcom/sec/android/gallery3d/ui/SelectionManager;)V
    .locals 1
    .param p1, "selectionMgr"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->setSelectionSource(Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    .line 109
    return-void
.end method

.method public setShowAllGroup(Z)V
    .locals 1
    .param p1, "showAll"    # Z

    .prologue
    .line 840
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->setShowAllGroup(Z)V

    .line 841
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->forceReload()V

    .line 842
    return-void
.end method

.method public setSource(Lcom/sec/android/gallery3d/data/MediaSet;)Z
    .locals 2
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->setSource(Lcom/sec/android/gallery3d/data/MediaSet;Z)Z

    move-result v0

    return v0
.end method

.method public setSource(Lcom/sec/android/gallery3d/data/MediaSet;Z)Z
    .locals 1
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "firstLoad"    # Z

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->setSource(Lcom/sec/android/gallery3d/data/MediaSet;Z)Z

    move-result v0

    return v0
.end method

.method public setSource(Lcom/sec/android/gallery3d/data/MediaSet;ZZ)Z
    .locals 1
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "firstLoad"    # Z
    .param p3, "recycle"    # Z

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->setSource(Lcom/sec/android/gallery3d/data/MediaSet;ZZ)Z

    move-result v0

    return v0
.end method

.method public setThumbReslevel(I)V
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->setThumbReslevel(I)V

    .line 118
    return-void
.end method
