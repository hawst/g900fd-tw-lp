.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$17;
.super Ljava/lang/Object;
.source "GallerySearchViewState.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->initNewFilterSearchView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V
    .locals 0

    .prologue
    .line 635
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$17;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 640
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$17;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$1900(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 641
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$17;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$17;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->getDetailPanelHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->setFilterTagViewHeight(I)V

    .line 642
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$17;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$1900(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->getCount()I

    move-result v0

    .line 643
    .local v0, "count":I
    const/4 v2, 0x0

    .line 644
    .local v2, "result":Ljava/lang/String;
    if-ge p3, v0, :cond_0

    .line 645
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$17;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$1900(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;

    move-result-object v3

    invoke-virtual {v3, p3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->getItem(I)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;

    move-result-object v1

    .line 646
    .local v1, "item":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;
    if-eqz v1, :cond_0

    .line 647
    iget-object v2, v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;->mTitle:Ljava/lang/String;

    .line 649
    .end local v1    # "item":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$17;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$1800(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$17;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$1800(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v3

    instance-of v3, v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    if-eqz v3, :cond_1

    .line 650
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$17;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$1800(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->setHistoryQuery(Ljava/lang/String;)V

    .line 653
    .end local v0    # "count":I
    .end local v2    # "result":Ljava/lang/String;
    :cond_1
    return-void
.end method
