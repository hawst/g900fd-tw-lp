.class Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2$1;
.super Landroid/os/CountDownTimer;
.source "HelpIconLabelState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;->onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 146
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2$1;->this$1:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2$1;->this$1:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 150
    return-void
.end method

.method public onTick(J)V
    .locals 2
    .param p1, "l"    # J

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2$1;->this$1:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->IsShowCompletePopup:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->access$100(Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2$1;->this$1:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;

    const/4 v1, 0x1

    # setter for: Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->IsShowCompletePopup:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->access$102(Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;Z)Z

    .line 156
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2$1;->this$1:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;

    const/16 v1, 0xa

    iput v1, v0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mViewMode:I

    .line 157
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2$1;->this$1:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->setHelpView()V

    .line 159
    :cond_0
    return-void
.end method
