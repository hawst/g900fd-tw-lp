.class Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;
.super Ljava/lang/Object;
.source "GlPhotoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DragInfo"
.end annotation


# instance fields
.field public mActive:Z

.field private mDnAreaY:F

.field private mRightAreaX:F

.field private mThumbOjbectHeight:F

.field private mUpAreaY:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 1

    .prologue
    .line 2624
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2625
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->mActive:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/samsung/gallery/glview/GlPhotoView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/glview/GlPhotoView$1;

    .prologue
    .line 2624
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    return-void
.end method


# virtual methods
.method public getIntensity(FF)F
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 2648
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    div-float/2addr v0, v2

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v1

    sub-float/2addr v0, v1

    cmpl-float v0, p2, v0

    if-lez v0, :cond_0

    .line 2651
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    div-float/2addr v0, v2

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v1

    sub-float p2, v0, v1

    .line 2655
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->mThumbOjbectHeight:F

    const/high16 v1, 0x40800000    # 4.0f

    div-float/2addr v0, v1

    sub-float/2addr p2, v0

    .line 2657
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->mRightAreaX:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->mUpAreaY:F

    cmpl-float v0, p2, v0

    if-lez v0, :cond_1

    .line 2658
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->mUpAreaY:F

    sub-float v0, p2, v0

    .line 2663
    :goto_0
    return v0

    .line 2660
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->mRightAreaX:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_2

    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->mDnAreaY:F

    cmpg-float v0, p2, v0

    if-gez v0, :cond_2

    .line 2661
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->mDnAreaY:F

    sub-float v0, p2, v0

    goto :goto_0

    .line 2663
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 2633
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWideMode:Z

    if-eqz v2, :cond_0

    sget v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SPLIT_RATIO_LAND:F

    .line 2636
    .local v0, "ratio":F
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumThumbnailHeight(I)F

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->mThumbOjbectHeight:F

    .line 2641
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidthSpace:F

    mul-float v1, v0, v2

    .line 2642
    .local v1, "splitWidth":F
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidthSpace:F

    div-float/2addr v2, v4

    sub-float v2, v1, v2

    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->mRightAreaX:F

    .line 2643
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    div-float/2addr v2, v4

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->mThumbOjbectHeight:F

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v3

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->mUpAreaY:F

    .line 2644
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    neg-float v2, v2

    div-float/2addr v2, v4

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->mThumbOjbectHeight:F

    add-float/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->mDnAreaY:F

    .line 2645
    return-void

    .line 2633
    .end local v0    # "ratio":F
    .end local v1    # "splitWidth":F
    :cond_0
    sget v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SPLIT_RATIO_PORT:F

    goto :goto_0
.end method
