.class public Lcom/sec/samsung/gallery/view/detailview/FaceTagFeatureFactory;
.super Ljava/lang/Object;
.source "FaceTagFeatureFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/detailview/FaceTagFeatureFactory$1;,
        Lcom/sec/samsung/gallery/view/detailview/FaceTagFeatureFactory$DisabledFaceTagFeature;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method

.method public static getDefualt(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "photoView"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 14
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v0, :cond_0

    .line 15
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    invoke-direct {v0, p0, p1}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/PhotoView;)V

    .line 17
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeatureFactory$DisabledFaceTagFeature;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeatureFactory$DisabledFaceTagFeature;-><init>(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeatureFactory$1;)V

    goto :goto_0
.end method
