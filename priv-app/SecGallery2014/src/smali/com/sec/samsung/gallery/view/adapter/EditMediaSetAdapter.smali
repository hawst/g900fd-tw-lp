.class public Lcom/sec/samsung/gallery/view/adapter/EditMediaSetAdapter;
.super Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;
.source "EditMediaSetAdapter.java"


# instance fields
.field private mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

.field private mIsEnabledSelectionMode:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    .line 18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaSetAdapter;->mIsEnabledSelectionMode:Z

    .line 21
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    .line 22
    return-void
.end method


# virtual methods
.method public drawCheckboxAndMark(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V
    .locals 12
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I

    .prologue
    .line 39
    if-nez p1, :cond_1

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaSetAdapter;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v6

    .line 43
    .local v6, "selectionManager":Lcom/sec/android/gallery3d/ui/SelectionManager;
    const/4 v8, 0x4

    invoke-virtual {p1, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 44
    .local v0, "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/view/adapter/EditMediaSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    .line 45
    .local v4, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v3

    .line 46
    .local v3, "itemCount":I
    :goto_1
    if-eqz v6, :cond_a

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v8

    if-eqz v8, :cond_a

    iget-boolean v8, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaSetAdapter;->mIsEnabledSelectionMode:Z

    if-eqz v8, :cond_a

    .line 47
    if-nez v0, :cond_2

    .line 48
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 49
    .restart local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 50
    const/16 v8, 0x32

    const/16 v9, 0x32

    invoke-virtual {v0, v8, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 51
    const/16 v8, 0xa

    const/16 v9, 0x1e

    const/16 v10, 0x1e

    const/16 v11, 0xa

    invoke-virtual {v0, v8, v9, v10, v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 63
    :goto_2
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 70
    const/4 v8, 0x3

    const/4 v9, 0x1

    invoke-virtual {v0, v8, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 71
    const/4 v8, 0x4

    invoke-virtual {p1, v0, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 74
    :cond_2
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v8, :cond_7

    const v2, 0x7f020122

    .line 75
    .local v2, "checkOnId":I
    :goto_3
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v8, :cond_8

    const v1, 0x7f020121

    .line 76
    .local v1, "checkOffId":I
    :goto_4
    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 77
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/view/adapter/EditMediaSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 78
    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    goto :goto_0

    .line 45
    .end local v1    # "checkOffId":I
    .end local v2    # "checkOnId":I
    .end local v3    # "itemCount":I
    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    .line 53
    .restart local v3    # "itemCount":I
    :cond_4
    const/16 v8, 0x3c

    const/16 v9, 0x3c

    invoke-virtual {v0, v8, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 54
    const/4 v7, 0x2

    .line 55
    .local v7, "top_margine":I
    const/4 v5, 0x4

    .line 56
    .local v5, "right_margine":I
    const/4 v8, 0x1

    if-gt v3, v8, :cond_5

    .line 57
    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v9, v9, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_top_margin:I

    add-int/2addr v9, v7

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v10, v10, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_right_margin:I

    add-int/2addr v10, v5

    const/4 v11, 0x0

    invoke-virtual {v0, v8, v9, v10, v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    goto :goto_2

    .line 58
    :cond_5
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v8, 0xf

    if-ge v3, v8, :cond_6

    .line 59
    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v9, v9, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_2_top_margin:I

    add-int/2addr v9, v7

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v10, v10, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_2_right_margin:I

    add-int/2addr v10, v5

    const/4 v11, 0x0

    invoke-virtual {v0, v8, v9, v10, v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    goto :goto_2

    .line 61
    :cond_6
    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v9, v9, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_3_top_margin:I

    add-int/2addr v9, v7

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v10, v10, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_3_right_margin:I

    add-int/2addr v10, v5

    const/4 v11, 0x0

    invoke-virtual {v0, v8, v9, v10, v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    goto :goto_2

    .line 74
    .end local v5    # "right_margine":I
    .end local v7    # "top_margine":I
    :cond_7
    const v2, 0x7f0204ad

    goto :goto_3

    .line 75
    .restart local v2    # "checkOnId":I
    :cond_8
    const v1, 0x7f0204a6

    goto :goto_4

    .line 80
    .restart local v1    # "checkOffId":I
    :cond_9
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 82
    .end local v1    # "checkOffId":I
    .end local v2    # "checkOnId":I
    :cond_a
    if-eqz v0, :cond_0

    .line 83
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "parentLayer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 30
    invoke-super {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 32
    .local v0, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {p0, v0, p1}, Lcom/sec/samsung/gallery/view/adapter/EditMediaSetAdapter;->drawCheckboxAndMark(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V

    .line 34
    return-object v0
.end method

.method public setEnableSelectionMode(Z)V
    .locals 0
    .param p1, "isEnabledSelectionMode"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaSetAdapter;->mIsEnabledSelectionMode:Z

    .line 26
    return-void
.end method
