.class Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$8;
.super Ljava/lang/Object;
.source "StartGalleryViewCmd.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->startDefaultMode()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;)V
    .locals 0

    .prologue
    .line 329
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$8;->this$0:Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 332
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$8;->this$0:Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->access$000(Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->getLastestActivityState(Landroid/content/Context;)Ljava/lang/Class;

    move-result-object v0

    .line 333
    .local v0, "activityState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$8;->this$0:Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->access$000(Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;)Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    # getter for: Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mBundle:Landroid/os/Bundle;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->access$100()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 334
    return-void
.end method
