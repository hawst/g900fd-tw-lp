.class Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit$1;
.super Ljava/lang/Object;
.source "PhotoActionBarForEdit.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->setTitle(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;

.field final synthetic val$numberOfItemsSelected:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;I)V
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;

    iput p2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit$1;->val$numberOfItemsSelected:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 393
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->access$000(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 413
    :cond_0
    :goto_0
    return-void

    .line 395
    :cond_1
    const/4 v0, 0x0

    .line 396
    .local v0, "numberOfSelectedItems":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->access$200(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;)Landroid/view/Menu;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 397
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->access$300(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->invalidateOptionsMenu()V

    .line 399
    :cond_2
    const/4 v1, 0x0

    .line 400
    .local v1, "res":Landroid/content/res/Resources;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->access$400(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 401
    iget v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit$1;->val$numberOfItemsSelected:I

    if-ne v2, v5, :cond_3

    .line 402
    const v2, 0x7f0e004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit$1;->val$numberOfItemsSelected:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 408
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;

    iget v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit$1;->val$numberOfItemsSelected:I

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->setSelectAllButtonTitle(ILjava/lang/String;)V
    invoke-static {v2, v3, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;ILjava/lang/String;)V

    .line 410
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->access$200(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;)Landroid/view/Menu;

    move-result-object v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit$1;->val$numberOfItemsSelected:I

    if-lez v2, :cond_0

    .line 411
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->access$200(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;)Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0f02cc

    invoke-interface {v2, v3, v5}, Landroid/view/Menu;->setGroupEnabled(IZ)V

    goto :goto_0

    .line 405
    :cond_3
    const v2, 0x7f0e004e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit$1;->val$numberOfItemsSelected:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
