.class Lcom/sec/samsung/gallery/util/KNOXOperations$MediaObjectComparator;
.super Ljava/lang/Object;
.source "KNOXOperations.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/util/KNOXOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MediaObjectComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/gallery3d/data/MediaObject;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/util/KNOXOperations$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/util/KNOXOperations$1;

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/sec/samsung/gallery/util/KNOXOperations$MediaObjectComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/gallery3d/data/MediaObject;Lcom/sec/android/gallery3d/data/MediaObject;)I
    .locals 6
    .param p1, "object1"    # Lcom/sec/android/gallery3d/data/MediaObject;
    .param p2, "object2"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    const/4 v4, 0x0

    .line 93
    instance-of v5, p1, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v5, :cond_1

    instance-of v5, p2, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v5, :cond_1

    .line 94
    check-cast p1, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local p1    # "object1":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v2

    .line 95
    .local v2, "setPath1":Ljava/lang/String;
    check-cast p2, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local p2    # "object2":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v3

    .line 96
    .local v3, "setPath2":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v4, v5

    .line 104
    .end local v2    # "setPath1":Ljava/lang/String;
    .end local v3    # "setPath2":Ljava/lang/String;
    :cond_0
    :goto_0
    return v4

    .line 97
    .restart local p1    # "object1":Lcom/sec/android/gallery3d/data/MediaObject;
    .restart local p2    # "object2":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    instance-of v5, p1, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v5, :cond_0

    instance-of v5, p2, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v5, :cond_0

    .line 98
    check-cast p1, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local p1    # "object1":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 99
    .local v0, "itemPath1":Ljava/lang/String;
    const-string v5, "/"

    invoke-virtual {v0, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 100
    check-cast p2, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local p2    # "object2":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v1

    .line 101
    .local v1, "itemPath2":Ljava/lang/String;
    const-string v5, "/"

    invoke-virtual {v1, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 102
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v4, v5

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 91
    check-cast p1, Lcom/sec/android/gallery3d/data/MediaObject;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/gallery3d/data/MediaObject;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/util/KNOXOperations$MediaObjectComparator;->compare(Lcom/sec/android/gallery3d/data/MediaObject;Lcom/sec/android/gallery3d/data/MediaObject;)I

    move-result v0

    return v0
.end method
