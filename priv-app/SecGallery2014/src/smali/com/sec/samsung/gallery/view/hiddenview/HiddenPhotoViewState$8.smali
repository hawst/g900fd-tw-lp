.class Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$8;
.super Ljava/lang/Object;
.source "HiddenPhotoViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$8;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V
    .locals 3
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/GlAbsListView;
    .param p2, "position"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v2, 0x0

    .line 241
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$8;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getGenericMotionFocus()I

    move-result v0

    .line 242
    .local v0, "genericMotionFocus":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$8;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->setGenericMotionFocus(I)V

    .line 243
    if-ltz v0, :cond_0

    .line 244
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$8;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->notifyDataSetChanged(II)V

    .line 246
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$8;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    move-result-object v1

    invoke-virtual {v1, p2, v2}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->notifyDataSetChanged(II)V

    .line 247
    return-void
.end method
