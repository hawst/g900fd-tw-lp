.class public Lcom/sec/samsung/gallery/drawer/DrawerItemFilterBy;
.super Lcom/sec/samsung/gallery/drawer/DrawerItem;
.source "DrawerItemFilterBy.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/sec/samsung/gallery/drawer/DrawerItemFilterBy;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/drawer/DrawerItemFilterBy;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(IIILcom/sec/samsung/gallery/core/TabTagType;)V
    .locals 0
    .param p1, "groupid"    # I
    .param p2, "stringId"    # I
    .param p3, "ttsId"    # I
    .param p4, "tagType"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/drawer/DrawerItem;-><init>(IIILcom/sec/samsung/gallery/core/TabTagType;)V

    .line 28
    return-void
.end method

.method public static createFilterbyItems(IIIILcom/sec/samsung/gallery/core/TabTagType;)Lcom/sec/samsung/gallery/drawer/DrawerItem;
    .locals 1
    .param p0, "groupId"    # I
    .param p1, "stringId"    # I
    .param p2, "ttsId"    # I
    .param p3, "iconRes"    # I
    .param p4, "tagType"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    .line 31
    new-instance v0, Lcom/sec/samsung/gallery/drawer/DrawerItemFilterBy;

    invoke-direct {v0, p0, p1, p2, p4}, Lcom/sec/samsung/gallery/drawer/DrawerItemFilterBy;-><init>(IIILcom/sec/samsung/gallery/core/TabTagType;)V

    .line 32
    .local v0, "item":Lcom/sec/samsung/gallery/drawer/DrawerItem;
    invoke-virtual {v0, p3}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->setIconId(I)V

    .line 34
    return-object v0
.end method


# virtual methods
.method public selectItem(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/StateManager;IZ)V
    .locals 9
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "statusProxy"    # Lcom/sec/android/gallery3d/app/StateManager;
    .param p3, "index"    # I
    .param p4, "isPickMode"    # Z

    .prologue
    .line 39
    sget-object v6, Lcom/sec/samsung/gallery/drawer/DrawerItemFilterBy;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Local Selected : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewMode()Ljava/lang/Class;

    move-result-object v3

    .line 41
    .local v3, "currentViewState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 44
    .local v1, "bundle":Landroid/os/Bundle;
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    if-eqz v6, :cond_2

    .line 46
    iget v6, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemFilterBy;->mNameStringId:I

    const v7, 0x7f0e0441

    if-ne v6, v7, :cond_2

    .line 47
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "EVVI"

    invoke-static {v6, v7, p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 49
    const-class v6, Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    if-ne v3, v6, :cond_0

    .line 50
    const-string v6, "KEY_VIEW_REDRAW"

    const/4 v7, 0x1

    invoke-virtual {v1, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 52
    :cond_0
    sget-object v6, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_EVENT:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {p2, v6}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 53
    const-class v0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .line 54
    .local v0, "activityState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 98
    .end local v0    # "activityState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    :cond_1
    :goto_0
    return-void

    .line 59
    :cond_2
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v6, :cond_a

    .line 60
    const-string v2, ""

    .line 61
    .local v2, "categoryType":Ljava/lang/String;
    iget v6, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemFilterBy;->mNameStringId:I

    const v7, 0x7f0e00a7

    if-ne v6, v7, :cond_3

    .line 62
    sget-object v6, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {p2, v6}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 63
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    const-class v7, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0

    .line 65
    :cond_3
    iget v6, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemFilterBy;->mNameStringId:I

    const v7, 0x7f0e0478

    if-ne v6, v7, :cond_5

    .line 66
    sget-object v6, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SCENERY:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {p2, v6}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 67
    const-string v2, "Scenery"

    .line 85
    :cond_4
    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/local/categoryalbumset/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 86
    .local v5, "topSetPath":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/local/categoryalbum/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 87
    .local v4, "path":Ljava/lang/String;
    const-string v6, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v1, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v6, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v1, v6, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v6, "KEY_MEDIA_SET_POSITION"

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 90
    const-string v6, "KEY_NO_SPLIT_MODE"

    const/4 v7, 0x1

    invoke-virtual {v1, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 91
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    const-class v7, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-virtual {v6, v7, v1}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0

    .line 68
    .end local v4    # "path":Ljava/lang/String;
    .end local v5    # "topSetPath":Ljava/lang/String;
    :cond_5
    iget v6, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemFilterBy;->mNameStringId:I

    const v7, 0x7f0e047c

    if-ne v6, v7, :cond_6

    .line 69
    sget-object v6, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_DOCUMENTS:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {p2, v6}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 70
    const-string v2, "Documents"

    goto :goto_1

    .line 71
    :cond_6
    iget v6, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemFilterBy;->mNameStringId:I

    const v7, 0x7f0e0479

    if-ne v6, v7, :cond_7

    .line 72
    sget-object v6, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FOOD:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {p2, v6}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 73
    const-string v2, "Food"

    goto :goto_1

    .line 74
    :cond_7
    iget v6, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemFilterBy;->mNameStringId:I

    const v7, 0x7f0e047b

    if-ne v6, v7, :cond_8

    .line 75
    sget-object v6, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_PETS:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {p2, v6}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 76
    const-string v2, "Pets"

    goto :goto_1

    .line 77
    :cond_8
    iget v6, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemFilterBy;->mNameStringId:I

    const v7, 0x7f0e047d

    if-ne v6, v7, :cond_9

    .line 78
    sget-object v6, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_VEHICLES:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {p2, v6}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 79
    const-string v2, "Vehicles"

    goto/16 :goto_1

    .line 80
    :cond_9
    iget v6, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemFilterBy;->mNameStringId:I

    const v7, 0x7f0e047f

    if-ne v6, v7, :cond_4

    .line 81
    sget-object v6, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FLOWERS:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {p2, v6}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 82
    const-string v2, "Flower"

    goto/16 :goto_1

    .line 93
    .end local v2    # "categoryType":Ljava/lang/String;
    :cond_a
    iget v6, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemFilterBy;->mNameStringId:I

    const v7, 0x7f0e00a7

    if-ne v6, v7, :cond_1

    .line 94
    sget-object v6, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {p2, v6}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 95
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    const-class v7, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_0
.end method
