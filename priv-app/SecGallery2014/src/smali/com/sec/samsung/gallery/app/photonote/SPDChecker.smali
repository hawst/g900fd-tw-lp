.class public Lcom/sec/samsung/gallery/app/photonote/SPDChecker;
.super Ljava/lang/Object;
.source "SPDChecker.java"


# static fields
.field private static final BUFREADCOMMENT:I = 0x400

.field private static final IMAGE_NOTE:[B

.field private static final PHOTO_NOTE:[B

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const-class v0, Lcom/sec/samsung/gallery/app/photonote/SPDChecker;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/SPDChecker;->TAG:Ljava/lang/String;

    .line 8
    const/16 v0, 0xc

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/SPDChecker;->IMAGE_NOTE:[B

    .line 12
    const/16 v0, 0x1e

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/SPDChecker;->PHOTO_NOTE:[B

    return-void

    .line 8
    nop

    :array_0
    .array-data 1
        0x41t
        0x4dt
        0x53t
        -0x1t
        -0x1t
        0x53t
        0x61t
        0x6dt
        0x73t
        0x75t
        0x6et
        0x67t
    .end array-data

    .line 12
    :array_1
    .array-data 1
        0x44t
        0x6ft
        0x63t
        0x75t
        0x6dt
        0x65t
        0x6et
        0x74t
        0x20t
        0x66t
        0x6ft
        0x72t
        0x20t
        0x53t
        0x41t
        0x4dt
        0x53t
        0x55t
        0x4et
        0x47t
        0x20t
        0x53t
        0x2dt
        0x50t
        0x65t
        0x6et
        0x20t
        0x53t
        0x44t
        0x4bt
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static isSAMMValid(Ljava/io/RandomAccessFile;)Z
    .locals 8
    .param p0, "file"    # Ljava/io/RandomAccessFile;

    .prologue
    const/4 v5, 0x0

    .line 20
    :try_start_0
    sget-object v6, Lcom/sec/samsung/gallery/app/photonote/SPDChecker;->IMAGE_NOTE:[B

    array-length v4, v6

    .line 21
    .local v4, "markerSize":I
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v2

    .line 22
    .local v2, "fileSize":J
    add-int/lit8 v6, v4, 0x1

    new-array v0, v6, [B

    .line 24
    .local v0, "buf":[B
    int-to-long v6, v4

    sub-long v6, v2, v6

    invoke-virtual {p0, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 25
    const/4 v6, 0x0

    invoke-virtual {p0, v0, v6, v4}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v6

    if-eq v6, v4, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 43
    .end local v0    # "buf":[B
    .end local v2    # "fileSize":J
    .end local v4    # "markerSize":I
    :goto_0
    return v5

    .line 30
    .restart local v0    # "buf":[B
    .restart local v2    # "fileSize":J
    .restart local v4    # "markerSize":I
    :cond_0
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_1
    if-ge v1, v4, :cond_2

    .line 31
    sget-object v6, Lcom/sec/samsung/gallery/app/photonote/SPDChecker;->IMAGE_NOTE:[B

    aget-byte v6, v6, v1

    aget-byte v7, v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eq v6, v7, :cond_1

    .line 32
    const/4 v0, 0x0

    .line 33
    goto :goto_0

    .line 30
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 37
    :cond_2
    const/4 v0, 0x0

    .line 38
    const/4 v5, 0x1

    goto :goto_0

    .line 39
    .end local v0    # "buf":[B
    .end local v1    # "index":I
    .end local v2    # "fileSize":J
    .end local v4    # "markerSize":I
    :catch_0
    move-exception v6

    goto :goto_0
.end method

.method private static isSPDValid(Ljava/io/RandomAccessFile;)Z
    .locals 8
    .param p0, "file"    # Ljava/io/RandomAccessFile;

    .prologue
    const/4 v5, 0x0

    .line 48
    :try_start_0
    sget-object v6, Lcom/sec/samsung/gallery/app/photonote/SPDChecker;->PHOTO_NOTE:[B

    array-length v4, v6

    .line 49
    .local v4, "markerSize":I
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v2

    .line 50
    .local v2, "fileSize":J
    add-int/lit8 v6, v4, 0x1

    new-array v0, v6, [B

    .line 52
    .local v0, "buf":[B
    int-to-long v6, v4

    sub-long v6, v2, v6

    invoke-virtual {p0, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 53
    const/4 v6, 0x0

    invoke-virtual {p0, v0, v6, v4}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v6

    if-eq v6, v4, :cond_0

    .line 54
    const/4 v0, 0x0

    .line 71
    .end local v0    # "buf":[B
    .end local v2    # "fileSize":J
    .end local v4    # "markerSize":I
    :goto_0
    return v5

    .line 58
    .restart local v0    # "buf":[B
    .restart local v2    # "fileSize":J
    .restart local v4    # "markerSize":I
    :cond_0
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_1
    if-ge v1, v4, :cond_2

    .line 59
    sget-object v6, Lcom/sec/samsung/gallery/app/photonote/SPDChecker;->PHOTO_NOTE:[B

    aget-byte v6, v6, v1

    aget-byte v7, v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eq v6, v7, :cond_1

    .line 60
    const/4 v0, 0x0

    .line 61
    goto :goto_0

    .line 58
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 65
    :cond_2
    const/4 v0, 0x0

    .line 66
    const/4 v5, 0x1

    goto :goto_0

    .line 67
    .end local v0    # "buf":[B
    .end local v1    # "index":I
    .end local v2    # "fileSize":J
    .end local v4    # "markerSize":I
    :catch_0
    move-exception v6

    goto :goto_0
.end method

.method private static isSPDValidCheckAgain(Ljava/io/RandomAccessFile;)Z
    .locals 28
    .param p0, "file"    # Ljava/io/RandomAccessFile;

    .prologue
    .line 80
    const-wide/32 v14, 0xffff

    .line 81
    .local v14, "uMaxBack":J
    const-wide/16 v16, 0x0

    .line 82
    .local v16, "uPosFound":J
    const/16 v24, 0x404

    :try_start_0
    move/from16 v0, v24

    new-array v4, v0, [B

    .line 84
    .local v4, "buf":[B
    invoke-virtual/range {p0 .. p0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v22

    .line 85
    .local v22, "uSizeFile":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 87
    const-wide/16 v12, 0x4

    .line 88
    .local v12, "uBackRead":J
    :cond_0
    cmp-long v24, v12, v14

    if-gez v24, :cond_1

    .line 92
    const-wide/16 v24, 0x400

    add-long v24, v24, v12

    cmp-long v24, v24, v14

    if-lez v24, :cond_2

    .line 93
    move-wide v12, v14

    .line 97
    :goto_0
    sub-long v18, v22, v12

    .line 98
    .local v18, "uReadPos":J
    const-wide/16 v24, 0x404

    sub-long v26, v22, v18

    cmp-long v24, v24, v26

    if-gez v24, :cond_3

    const-wide/16 v20, 0x404

    .line 101
    .local v20, "uReadSize":J
    :goto_1
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 102
    const/16 v24, 0x0

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v4, v1, v2}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v24

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v24, v0

    cmp-long v24, v24, v20

    if-eqz v24, :cond_4

    .line 122
    .end local v18    # "uReadPos":J
    .end local v20    # "uReadSize":J
    :cond_1
    :goto_2
    const-wide/16 v24, 0x0

    cmp-long v24, v16, v24

    if-gtz v24, :cond_6

    .line 123
    const/4 v4, 0x0

    .line 124
    const/16 v24, 0x0

    .line 152
    .end local v4    # "buf":[B
    .end local v12    # "uBackRead":J
    .end local v22    # "uSizeFile":J
    :goto_3
    return v24

    .line 95
    .restart local v4    # "buf":[B
    .restart local v12    # "uBackRead":J
    .restart local v22    # "uSizeFile":J
    :cond_2
    const-wide/16 v24, 0x400

    add-long v12, v12, v24

    goto :goto_0

    .line 98
    .restart local v18    # "uReadPos":J
    :cond_3
    sub-long v20, v22, v18

    goto :goto_1

    .line 106
    .restart local v20    # "uReadSize":J
    :cond_4
    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v24, v0

    add-int/lit8 v5, v24, -0x3

    .local v5, "i":I
    move v6, v5

    .end local v5    # "i":I
    .local v6, "i":I
    :goto_4
    add-int/lit8 v5, v6, -0x1

    .end local v6    # "i":I
    .restart local v5    # "i":I
    if-lez v6, :cond_5

    .line 108
    aget-byte v24, v4, v5

    const/16 v25, 0x50

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_a

    add-int/lit8 v24, v5, 0x1

    aget-byte v24, v4, v24

    const/16 v25, 0x4b

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_a

    add-int/lit8 v24, v5, 0x2

    aget-byte v24, v4, v24

    const/16 v25, 0x5

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_a

    add-int/lit8 v24, v5, 0x3

    aget-byte v24, v4, v24

    const/16 v25, 0x6

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_a

    .line 111
    int-to-long v0, v5

    move-wide/from16 v24, v0

    add-long v16, v18, v24

    .line 116
    :cond_5
    const-wide/16 v24, 0x0

    cmp-long v24, v16, v24

    if-eqz v24, :cond_0

    goto :goto_2

    .line 128
    .end local v5    # "i":I
    .end local v18    # "uReadPos":J
    .end local v20    # "uReadSize":J
    :cond_6
    const-wide/16 v24, 0x14

    add-long v24, v24, v16

    move-object/from16 v0, p0

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 129
    invoke-virtual/range {p0 .. p0}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v24

    move/from16 v0, v24

    and-int/lit16 v8, v0, 0xff

    .line 130
    .local v8, "num1":I
    invoke-virtual/range {p0 .. p0}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v24

    move/from16 v0, v24

    and-int/lit16 v9, v0, 0xff

    .line 131
    .local v9, "num2":I
    shl-int/lit8 v24, v9, 0x8

    or-int v11, v24, v8

    .line 134
    .local v11, "zipCommentSize":I
    const-wide/16 v24, 0x14

    add-long v24, v24, v16

    const-wide/16 v26, 0x2

    add-long v24, v24, v26

    int-to-long v0, v11

    move-wide/from16 v26, v0

    add-long v24, v24, v26

    move-object/from16 v0, p0

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 135
    invoke-virtual/range {p0 .. p0}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v24

    move/from16 v0, v24

    and-int/lit16 v8, v0, 0xff

    .line 136
    invoke-virtual/range {p0 .. p0}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v24

    move/from16 v0, v24

    and-int/lit16 v9, v0, 0xff

    .line 137
    shl-int/lit8 v24, v9, 0x8

    or-int v10, v24, v8

    .line 139
    .local v10, "spdTagSize":I
    const-wide/16 v24, 0x14

    add-long v24, v24, v16

    const-wide/16 v26, 0x2

    add-long v24, v24, v26

    int-to-long v0, v11

    move-wide/from16 v26, v0

    add-long v24, v24, v26

    const-wide/16 v26, 0x2

    add-long v24, v24, v26

    int-to-long v0, v10

    move-wide/from16 v26, v0

    add-long v24, v24, v26

    const-wide/16 v26, 0x1e

    sub-long v24, v24, v26

    move-object/from16 v0, p0

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 140
    const/16 v24, 0x0

    const/16 v25, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v4, v1, v2}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v24

    const/16 v25, 0x1e

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_9

    .line 141
    const/4 v7, 0x0

    .local v7, "index":I
    :goto_5
    const/16 v24, 0x1e

    move/from16 v0, v24

    if-ge v7, v0, :cond_8

    .line 142
    aget-byte v24, v4, v7

    sget-object v25, Lcom/sec/samsung/gallery/app/photonote/SPDChecker;->PHOTO_NOTE:[B

    aget-byte v25, v25, v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_7

    .line 143
    const/16 v24, 0x0

    goto/16 :goto_3

    .line 141
    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 146
    :cond_8
    const/16 v24, 0x1

    goto/16 :goto_3

    .line 148
    .end local v4    # "buf":[B
    .end local v7    # "index":I
    .end local v8    # "num1":I
    .end local v9    # "num2":I
    .end local v10    # "spdTagSize":I
    .end local v11    # "zipCommentSize":I
    .end local v12    # "uBackRead":J
    .end local v22    # "uSizeFile":J
    :catch_0
    move-exception v24

    .line 152
    :cond_9
    const/16 v24, 0x0

    goto/16 :goto_3

    .restart local v4    # "buf":[B
    .restart local v5    # "i":I
    .restart local v12    # "uBackRead":J
    .restart local v18    # "uReadPos":J
    .restart local v20    # "uReadSize":J
    .restart local v22    # "uSizeFile":J
    :cond_a
    move v6, v5

    .end local v5    # "i":I
    .restart local v6    # "i":I
    goto/16 :goto_4
.end method

.method public static isValid(Ljava/io/RandomAccessFile;)Z
    .locals 2
    .param p0, "file"    # Ljava/io/RandomAccessFile;

    .prologue
    const/4 v0, 0x1

    .line 157
    invoke-static {p0}, Lcom/sec/samsung/gallery/app/photonote/SPDChecker;->isSPDValid(Ljava/io/RandomAccessFile;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/sec/samsung/gallery/app/photonote/SPDChecker;->isSPDValidCheckAgain(Ljava/io/RandomAccessFile;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 163
    :cond_0
    :goto_0
    return v0

    .line 160
    :cond_1
    invoke-static {p0}, Lcom/sec/samsung/gallery/app/photonote/SPDChecker;->isSAMMValid(Ljava/io/RandomAccessFile;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 163
    const/4 v0, 0x0

    goto :goto_0
.end method
