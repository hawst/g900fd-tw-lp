.class public Lcom/sec/samsung/gallery/view/utils/ShowHintPopup;
.super Ljava/lang/Object;
.source "ShowHintPopup.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static onButtonLongClick(Landroid/view/View;Landroid/content/Context;)Z
    .locals 1
    .param p0, "v"    # Landroid/view/View;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    const/4 v0, -0x1

    invoke-static {p0, p1, v0}, Lcom/sec/samsung/gallery/view/utils/ShowHintPopup;->onButtonLongClick(Landroid/view/View;Landroid/content/Context;I)Z

    move-result v0

    return v0
.end method

.method public static onButtonLongClick(Landroid/view/View;Landroid/content/Context;I)Z
    .locals 13
    .param p0, "v"    # Landroid/view/View;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "yOffset"    # I

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 15
    invoke-virtual {p0}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/CharSequence;->length()I

    move-result v9

    if-nez v9, :cond_0

    move v9, v10

    .line 51
    :goto_0
    return v9

    .line 20
    :cond_0
    const/4 v9, 0x2

    new-array v6, v9, [I

    .line 22
    .local v6, "screenPos":[I
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 23
    .local v3, "displayFrame":Landroid/graphics/Rect;
    invoke-virtual {p0, v6}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 24
    invoke-virtual {p0, v3}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 26
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v8

    .line 27
    .local v8, "width":I
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 28
    .local v4, "height":I
    aget v9, v6, v11

    div-int/lit8 v12, v4, 0x2

    add-int v5, v9, v12

    .line 29
    .local v5, "midy":I
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v7, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 30
    .local v7, "screenWidth":I
    const/4 v1, 0x0

    .line 31
    .local v1, "actionBarHeight":I
    if-lez p2, :cond_2

    .line 32
    move v1, p2

    .line 41
    :cond_1
    :goto_1
    invoke-virtual {p0}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-static {p1, v9, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    .line 42
    .local v2, "cheatSheet":Landroid/widget/Toast;
    iget v9, v3, Landroid/graphics/Rect;->top:I

    sub-int v9, v5, v9

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v12

    if-ge v9, v12, :cond_3

    .line 44
    const/16 v9, 0x35

    aget v10, v6, v10

    sub-int v10, v7, v10

    div-int/lit8 v12, v8, 0x2

    sub-int/2addr v10, v12

    aget v12, v6, v11

    add-int/2addr v12, v1

    invoke-virtual {v2, v9, v10, v12}, Landroid/widget/Toast;->setGravity(III)V

    .line 50
    :goto_2
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    move v9, v11

    .line 51
    goto :goto_0

    .end local v2    # "cheatSheet":Landroid/widget/Toast;
    :cond_2
    move-object v9, p1

    .line 35
    check-cast v9, Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 36
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_1

    .line 37
    invoke-virtual {v0}, Landroid/app/ActionBar;->getHeight()I

    move-result v1

    goto :goto_1

    .line 48
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    .restart local v2    # "cheatSheet":Landroid/widget/Toast;
    :cond_3
    const/16 v9, 0x51

    invoke-virtual {v2, v9, v10, v1}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_2
.end method
