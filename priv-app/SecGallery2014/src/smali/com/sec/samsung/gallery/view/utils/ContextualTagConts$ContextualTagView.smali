.class public Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;
.super Ljava/lang/Object;
.source "ContextualTagConts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/utils/ContextualTagConts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ContextualTagView"
.end annotation


# static fields
.field private static sInstance:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;


# instance fields
.field public final CA_ICON_LEFT_MARGIN:I

.field public final CA_ITEM_BG_HEIGHT:I

.field public final CA_ITEM_HEIGHT:I

.field public final CA_TEXT_BG_VERTICAL_MARGIN:I

.field public final CA_TEXT_BG_VERTICAL_MARGIN_FOR_DATE:I

.field public final CA_TEXT_DATE_TOP_MARGIN:I

.field public final CA_TEXT_HORIZONTAL_BOTTOM_MARGIN:I

.field public final CA_TEXT_HORIZONTAL_LEFT_MARGIN:I

.field public final CA_TEXT_HORIZONTAL_MAX_AREA:I

.field public final CA_TEXT_RIGHT_MARGIN:I

.field public final CA_TEXT_SIZE:I

.field public final CA_TEXT_SIZE_BIG:I

.field public final CA_TEXT_SIZE_FOR_DATE:I

.field public final CA_TEXT_SIZE_NORMAL:I

.field public final CA_TEXT_SIZE_SMALL:I

.field public final CA_TEXT_VERTICAL_BOTTOM_MARGIN:I

.field public final CA_TEXT_VERTICAL_LEFT_MARGIN:I

.field public final CA_TEXT_VERTICAL_MAX_AREA:I

.field public final MW_BOTTOM_MARGIN_LANDSCAPE:I

.field public final MW_BOTTOM_MARGIN_LANDSCAPE_MAX:I

.field public final MW_BOTTOM_MARGIN_LANDSCAPE_MAX_NEEDED:I

.field public final MW_BOTTOM_MARGIN_PORTRAIT:I

.field public final MW_BOTTOM_MARGIN_PORTRAIT_MAX:I

.field public final MW_BOTTOM_MARGIN_PORTRAIT_MAX_NEEDED:I

.field public final TEXT_HORIZONTAL_TOP_MARGIN:I

.field public final TEXT_SIZE_BIG:I

.field public final TEXT_SIZE_SMALL:I

.field public final TEXT_VERTICAL_TOP_MARGIN:I

.field public final THUMBNAIL_HEIGHT_LAND:I

.field public final THUMBNAIL_HEIGHT_PORT:I

.field public final THUMBNAIL_WIDTH_LAND:I

.field public final THUMBNAIL_WIDTH_PORT:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 70
    .local v0, "r":Landroid/content/res/Resources;
    const v1, 0x7f0c0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE_BIG:I

    .line 71
    const v1, 0x7f0c0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE_NORMAL:I

    .line 72
    const v1, 0x7f0c0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE_SMALL:I

    .line 73
    const v1, 0x7f0d02ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE_FOR_DATE:I

    .line 74
    const v1, 0x7f0c0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_BG_VERTICAL_MARGIN_FOR_DATE:I

    .line 75
    const v1, 0x7f0d02cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_BG_VERTICAL_MARGIN:I

    .line 76
    const v1, 0x7f0c0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_VERTICAL_LEFT_MARGIN:I

    .line 77
    const v1, 0x7f0d02ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_HORIZONTAL_LEFT_MARGIN:I

    .line 78
    const v1, 0x7f0c0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_VERTICAL_MAX_AREA:I

    .line 79
    const v1, 0x7f0d02cf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_VERTICAL_BOTTOM_MARGIN:I

    .line 80
    const v1, 0x7f0c0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_HORIZONTAL_MAX_AREA:I

    .line 81
    const v1, 0x7f0c0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iget v2, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE_BIG:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_HORIZONTAL_BOTTOM_MARGIN:I

    .line 82
    const v1, 0x7f0c0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_RIGHT_MARGIN:I

    .line 85
    const v1, 0x7f0c000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_VERTICAL_TOP_MARGIN:I

    .line 86
    const v1, 0x7f0c000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_HORIZONTAL_TOP_MARGIN:I

    .line 87
    const v1, 0x7f0c000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_SIZE_BIG:I

    .line 88
    const v1, 0x7f0c000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_SIZE_SMALL:I

    .line 90
    const v1, 0x7f0c000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->THUMBNAIL_WIDTH_LAND:I

    .line 91
    const v1, 0x7f0c000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->THUMBNAIL_HEIGHT_LAND:I

    .line 92
    const v1, 0x7f0c0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->THUMBNAIL_WIDTH_PORT:I

    .line 93
    const v1, 0x7f0c0011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->THUMBNAIL_HEIGHT_PORT:I

    .line 95
    const v1, 0x7f0d012d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->MW_BOTTOM_MARGIN_PORTRAIT:I

    .line 96
    const v1, 0x7f0d012e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->MW_BOTTOM_MARGIN_LANDSCAPE:I

    .line 97
    const v1, 0x7f0d012f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->MW_BOTTOM_MARGIN_PORTRAIT_MAX:I

    .line 98
    const v1, 0x7f0d0130

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->MW_BOTTOM_MARGIN_LANDSCAPE_MAX:I

    .line 99
    const v1, 0x7f0d0131

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->MW_BOTTOM_MARGIN_PORTRAIT_MAX_NEEDED:I

    .line 100
    const v1, 0x7f0d0132

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->MW_BOTTOM_MARGIN_LANDSCAPE_MAX_NEEDED:I

    .line 102
    const v1, 0x7f0d02c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_ITEM_HEIGHT:I

    .line 103
    const v1, 0x7f0d02c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_ICON_LEFT_MARGIN:I

    .line 104
    const v1, 0x7f0d02cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_DATE_TOP_MARGIN:I

    .line 105
    const v1, 0x7f0d02cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE:I

    .line 107
    const v1, 0x7f0d02d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_ITEM_BG_HEIGHT:I

    .line 108
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    const-class v1, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->sInstance:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->sInstance:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    .line 60
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->sInstance:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized releaseInstance()V
    .locals 2

    .prologue
    .line 64
    const-class v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    sput-object v1, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->sInstance:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    monitor-exit v0

    return-void

    .line 64
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
