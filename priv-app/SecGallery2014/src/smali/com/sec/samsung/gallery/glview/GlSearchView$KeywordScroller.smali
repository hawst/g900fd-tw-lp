.class Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;
.super Lcom/sec/samsung/gallery/glview/GlScroller;
.source "GlSearchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlSearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "KeywordScroller"
.end annotation


# static fields
.field private static final ITEM_HSIZE_RATIO_PORT:F = 0.94f

.field private static final ITEM_HSIZE_RATIO_WIDE:F = 0.94f

.field private static final ITEM_VSIZE_RATIO:F = 0.22f

.field private static final PADDING_LEFT:F = 10.0f

.field private static final PADDING_RIGHT:F = 10.0f

.field private static final PADDING_TOP_H:F = 51.0f

.field private static final PADDING_TOP_PAD:F = 10.0f

.field private static final PADDING_TOP_V:F = 32.0f


# instance fields
.field private mColCnt:I

.field private mFullVisibleCnt:I

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V
    .locals 0

    .prologue
    .line 1034
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlScroller;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/glview/GlSearchView;Lcom/sec/samsung/gallery/glview/GlSearchView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/glview/GlSearchView$1;

    .prologue
    .line 1034
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;-><init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V

    return-void
.end method


# virtual methods
.method public onFocusChange(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 1134
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    if-eqz v0, :cond_0

    .line 1135
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeyboardKeywordFocused:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$2000(Lcom/sec/samsung/gallery/glview/GlSearchView;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v2, p1, v0}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;->onFocusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    .line 1137
    :cond_0
    return-void

    .line 1135
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onMove(F)V
    .locals 1
    .param p1, "delta"    # F

    .prologue
    .line 1129
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iput p1, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordScroll:F

    .line 1130
    return-void
.end method

.method public onScrollStep(I)V
    .locals 3
    .param p1, "step"    # I

    .prologue
    .line 1141
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 1142
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    sget v2, Lcom/sec/samsung/gallery/glview/GlAbsListView;->SCROLL_STATE_FLING:I

    invoke-interface {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;->onScrollStateChanged(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V

    .line 1144
    :cond_0
    return-void
.end method

.method public reset()V
    .locals 9

    .prologue
    const/high16 v5, 0x41200000    # 10.0f

    const/high16 v8, 0x40000000    # 2.0f

    .line 1052
    const/4 v4, 0x0

    .line 1053
    .local v4, "paddingTop":F
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlSearchView;->mWidth:I

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeight:I

    if-le v6, v7, :cond_1

    .line 1054
    const v0, 0x3f70a3d7    # 0.94f

    .line 1055
    .local v0, "itemWidthRatioFromGap":F
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v6

    if-eqz v6, :cond_0

    move v4, v5

    .line 1056
    :goto_0
    const/4 v6, 0x4

    iput v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mColCnt:I

    .line 1063
    :goto_1
    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mColCnt:I

    iput v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemHVisibleCnt:I

    .line 1064
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlSearchView;->mWidthSpace:F

    sub-float/2addr v6, v5

    sub-float v1, v6, v5

    .line 1065
    .local v1, "mWidthValid":F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemHVisibleCnt:I

    int-to-float v5, v5

    div-float v5, v1, v5

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemGapW:F

    .line 1066
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemGapW:F

    mul-float/2addr v5, v0

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemW:F

    .line 1067
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemW:F

    const v6, 0x3e6147ae    # 0.22f

    mul-float/2addr v5, v6

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemH:F

    .line 1068
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemH:F

    const/high16 v6, 0x40a00000    # 5.0f

    add-float/2addr v5, v6

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemGapH:F

    .line 1070
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # invokes: Lcom/sec/samsung/gallery/glview/GlSearchView;->getKeywordCount()I
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$2100(Lcom/sec/samsung/gallery/glview/GlSearchView;)I

    move-result v5

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemHVisibleCnt:I

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, -0x1

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemHVisibleCnt:I

    div-int v6, v5, v6

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlSearchView;->mWidth:I

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeight:I

    if-le v5, v7, :cond_3

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->MAX_KEYWORD_ROW_COUNT_LAND:I
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$2200()I

    move-result v5

    :goto_2
    invoke-static {v6, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 1072
    .local v3, "maxCount":I
    int-to-float v5, v3

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemGapH:F

    mul-float/2addr v5, v6

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mScrollVisibleH:F

    .line 1074
    const/high16 v2, 0x41200000    # 10.0f

    .line 1075
    .local v2, "marginX":F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemGapW:F

    div-float/2addr v5, v8

    add-float/2addr v5, v2

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlSearchView;->mWidthSpace:F

    div-float/2addr v6, v8

    sub-float/2addr v5, v6

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemStartX:F

    .line 1076
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeightSpace:F

    div-float/2addr v5, v8

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemGapH:F

    div-float/2addr v6, v8

    sub-float/2addr v5, v6

    sub-float/2addr v5, v4

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemStartY:F

    .line 1078
    sget v5, Lcom/sec/samsung/gallery/glview/GlAbsListView;->DEF_DISTANCE:F

    neg-float v5, v5

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemStartZ:F

    .line 1079
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mScrollVisibleH:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemGapH:F

    div-float/2addr v5, v6

    float-to-int v5, v5

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemVVisibleCnt:I

    .line 1080
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemHVisibleCnt:I

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemVVisibleCnt:I

    mul-int/2addr v5, v6

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemVisibleCnt:I

    .line 1081
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemGapH:F

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mScrollVStart:F

    .line 1082
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mScrollVisibleH:F

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mScrollVEnd:F

    .line 1083
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mScroll:F

    .line 1084
    return-void

    .line 1055
    .end local v1    # "mWidthValid":F
    .end local v2    # "marginX":F
    .end local v3    # "maxCount":I
    :cond_0
    const/high16 v4, 0x424c0000    # 51.0f

    goto/16 :goto_0

    .line 1058
    .end local v0    # "itemWidthRatioFromGap":F
    :cond_1
    const v0, 0x3f70a3d7    # 0.94f

    .line 1059
    .restart local v0    # "itemWidthRatioFromGap":F
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v6

    if-eqz v6, :cond_2

    move v4, v5

    .line 1060
    :goto_3
    const/4 v6, 0x2

    iput v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mColCnt:I

    goto/16 :goto_1

    .line 1059
    :cond_2
    const/high16 v4, 0x42000000    # 32.0f

    goto :goto_3

    .line 1070
    .restart local v1    # "mWidthValid":F
    :cond_3
    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->MAX_KEYWORD_ROW_COUNT_PORT:I
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$2300()I

    move-result v5

    goto :goto_2
.end method

.method public resetSet(IIZ)V
    .locals 8
    .param p1, "startIndex"    # I
    .param p2, "totalCount"    # I
    .param p3, "resetScroll"    # Z

    .prologue
    .line 1098
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemHVisibleCnt:I

    add-int/2addr v5, p2

    add-int/lit8 v5, v5, -0x1

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemHVisibleCnt:I

    div-int/2addr v5, v6

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemRowCount:I

    .line 1099
    const/4 v0, 0x0

    .line 1100
    .local v0, "maxRange":F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mScrollVisibleH:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemGapH:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemRowCount:I

    int-to-float v7, v7

    mul-float/2addr v6, v7

    cmpg-float v5, v5, v6

    if-gez v5, :cond_0

    .line 1101
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemGapH:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemRowCount:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mScrollVisibleH:F

    sub-float v0, v5, v6

    .line 1106
    :cond_0
    const/4 v3, 0x0

    .line 1108
    .local v3, "setScroll":Z
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemHVisibleCnt:I

    div-int v2, p1, v5

    .line 1109
    .local v2, "rowIndex":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemHVisibleCnt:I

    add-int/2addr v5, p2

    add-int/lit8 v5, v5, -0x1

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemHVisibleCnt:I

    div-int v1, v5, v6

    .line 1110
    .local v1, "rowCount":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemVVisibleCnt:I

    if-gt v1, v5, :cond_3

    .line 1111
    const/4 v2, 0x0

    .line 1117
    :cond_1
    :goto_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$500(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    move-result-object v5

    int-to-float v6, v2

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemGapH:F

    mul-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 1118
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemHVisibleCnt:I

    mul-int v4, v2, v5

    .line 1120
    .local v4, "startRowIndex":I
    const/4 v5, 0x1

    invoke-super {p0, v4, p2, v5}, Lcom/sec/samsung/gallery/glview/GlScroller;->resetSet(IIZ)V

    .line 1121
    if-eqz v3, :cond_2

    .line 1122
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->setScroll(F)V

    .line 1123
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$500(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 1125
    :cond_2
    return-void

    .line 1112
    .end local v4    # "startRowIndex":I
    :cond_3
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mFullVisibleCnt:I

    add-int/2addr v5, v2

    if-lt v5, v1, :cond_1

    .line 1114
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemGapH:F

    div-float v5, v0, v5

    float-to-int v2, v5

    .line 1115
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 5
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    .line 1090
    iget v1, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ltz v1, :cond_0

    iget v1, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # invokes: Lcom/sec/samsung/gallery/glview/GlSearchView;->getKeywordCount()I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$2100(Lcom/sec/samsung/gallery/glview/GlSearchView;)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1091
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v2, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    .line 1092
    .local v0, "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 1094
    .end local v0    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_0
    return-void
.end method
