.class public Lcom/sec/samsung/gallery/decoder/DecoderInterface;
.super Ljava/lang/Object;
.source "DecoderInterface.java"


# static fields
.field private static bUseCenterFirstRD:Z


# instance fields
.field public BUFFER_TYPE_MTN:I

.field public LARGETHUMBNAIL_CENTER:I

.field public LARGETHUMBNAIL_NEXT:I

.field public LARGETHUMBNAIL_PREV:I

.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

.field private mSlidingAnimationInterface:Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;

.field private mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->bUseCenterFirstRD:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x2

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 56
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v0, :cond_1

    .line 57
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->LARGETHUMBNAIL_CENTER:I

    .line 58
    iput v1, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->LARGETHUMBNAIL_NEXT:I

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->LARGETHUMBNAIL_PREV:I

    .line 60
    iput v1, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->BUFFER_TYPE_MTN:I

    .line 62
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getXIVInterface()Lcom/sec/samsung/gallery/decoder/XIVInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    if-eqz v0, :cond_0

    .line 65
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->create()Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mSlidingAnimationInterface:Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;

    .line 66
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAnimationInterface()Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    goto :goto_0
.end method

.method private CenterFirstdrawTiles(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;IIFII)V
    .locals 20
    .param p1, "tiv"    # Lcom/sec/android/gallery3d/ui/TileImageView;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p3, "r"    # Landroid/graphics/Rect;
    .param p4, "level"    # I
    .param p5, "size"    # I
    .param p6, "length"    # F
    .param p7, "offsetX"    # I
    .param p8, "offsetY"    # I

    .prologue
    .line 212
    move-object/from16 v0, p3

    iget v2, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p3

    iget v3, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    move/from16 v0, p5

    int-to-float v3, v0

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    move/from16 v19, v0

    .line 213
    .local v19, "nBlockW":I
    move-object/from16 v0, p3

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p3

    iget v3, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    move/from16 v0, p5

    int-to-float v3, v0

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    move/from16 v18, v0

    .line 214
    .local v18, "nBlockH":I
    const/4 v14, 0x0

    .line 216
    .local v14, "dir":I
    move-object/from16 v0, p3

    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 217
    .local v5, "ty":I
    move-object/from16 v0, p3

    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 219
    .local v4, "tx":I
    move-object/from16 v0, p3

    iget v12, v0, Landroid/graphics/Rect;->left:I

    .line 220
    .local v12, "MinX":I
    move-object/from16 v0, p3

    iget v10, v0, Landroid/graphics/Rect;->right:I

    .line 221
    .local v10, "MaxX":I
    move-object/from16 v0, p3

    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 222
    .local v13, "MinY":I
    move-object/from16 v0, p3

    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 224
    .local v11, "MaxY":I
    const/4 v15, 0x0

    .line 225
    .local v15, "i":I
    const/16 v16, 0x0

    .line 227
    .local v16, "j":I
    const/16 v17, 0x0

    .local v17, "k":I
    :goto_0
    mul-int v2, v19, v18

    move/from16 v0, v17

    if-ge v0, v2, :cond_1

    .line 228
    move/from16 v0, p8

    int-to-float v2, v0

    int-to-float v3, v15

    mul-float v3, v3, p6

    add-float v8, v2, v3

    .line 229
    .local v8, "y":F
    move/from16 v0, p7

    int-to-float v2, v0

    move/from16 v0, v16

    int-to-float v3, v0

    mul-float v3, v3, p6

    add-float v7, v2, v3

    .local v7, "x":F
    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move/from16 v6, p4

    move/from16 v9, p6

    .line 230
    invoke-virtual/range {v2 .. v9}, Lcom/sec/android/gallery3d/ui/TileImageView;->drawTile(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIIFFF)V

    .line 231
    packed-switch v14, :pswitch_data_0

    .line 227
    :cond_0
    :goto_1
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 233
    :pswitch_0
    add-int v4, v4, p5

    .line 234
    add-int/lit8 v16, v16, 0x1

    .line 235
    if-le v4, v10, :cond_0

    .line 236
    add-int v13, v13, p5

    .line 237
    sub-int v4, v4, p5

    .line 238
    add-int/lit8 v16, v16, -0x1

    .line 239
    add-int v5, v5, p5

    .line 240
    add-int/lit8 v15, v15, 0x1

    .line 241
    const/4 v14, 0x1

    goto :goto_1

    .line 245
    :pswitch_1
    add-int v5, v5, p5

    .line 246
    add-int/lit8 v15, v15, 0x1

    .line 247
    if-le v5, v11, :cond_0

    .line 248
    sub-int v10, v10, p5

    .line 249
    sub-int v5, v5, p5

    .line 250
    add-int/lit8 v15, v15, -0x1

    .line 251
    sub-int v4, v4, p5

    .line 252
    add-int/lit8 v16, v16, -0x1

    .line 253
    const/4 v14, 0x2

    goto :goto_1

    .line 257
    :pswitch_2
    sub-int v4, v4, p5

    .line 258
    add-int/lit8 v16, v16, -0x1

    .line 259
    if-ge v4, v12, :cond_0

    .line 260
    sub-int v11, v11, p5

    .line 261
    add-int v4, v4, p5

    .line 262
    add-int/lit8 v16, v16, 0x1

    .line 263
    sub-int v5, v5, p5

    .line 264
    add-int/lit8 v15, v15, -0x1

    .line 265
    const/4 v14, 0x3

    goto :goto_1

    .line 269
    :pswitch_3
    sub-int v5, v5, p5

    .line 270
    add-int/lit8 v15, v15, -0x1

    .line 271
    if-ge v5, v13, :cond_0

    .line 272
    add-int v12, v12, p5

    .line 273
    add-int v5, v5, p5

    .line 274
    add-int/lit8 v15, v15, 0x1

    .line 275
    add-int v4, v4, p5

    .line 276
    add-int/lit8 v16, v16, 0x1

    .line 277
    const/4 v14, 0x0

    goto :goto_1

    .line 284
    .end local v7    # "x":F
    .end local v8    # "y":F
    :cond_1
    return-void

    .line 231
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private SequencedrawTiles(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;IIFII)V
    .locals 11
    .param p1, "tiv"    # Lcom/sec/android/gallery3d/ui/TileImageView;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p3, "r"    # Landroid/graphics/Rect;
    .param p4, "level"    # I
    .param p5, "size"    # I
    .param p6, "length"    # F
    .param p7, "offsetX"    # I
    .param p8, "offsetY"    # I

    .prologue
    .line 287
    iget v4, p3, Landroid/graphics/Rect;->top:I

    .local v4, "ty":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    iget v1, p3, Landroid/graphics/Rect;->bottom:I

    if-ge v4, v1, :cond_1

    .line 288
    move/from16 v0, p8

    int-to-float v1, v0

    int-to-float v2, v9

    mul-float v2, v2, p6

    add-float v7, v1, v2

    .line 289
    .local v7, "y":F
    iget v3, p3, Landroid/graphics/Rect;->left:I

    .local v3, "tx":I
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_1
    iget v1, p3, Landroid/graphics/Rect;->right:I

    if-ge v3, v1, :cond_0

    .line 290
    move/from16 v0, p7

    int-to-float v1, v0

    int-to-float v2, v10

    mul-float v2, v2, p6

    add-float v6, v1, v2

    .local v6, "x":F
    move-object v1, p1

    move-object v2, p2

    move v5, p4

    move/from16 v8, p6

    .line 291
    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/gallery3d/ui/TileImageView;->drawTile(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIIFFF)V

    .line 289
    add-int v3, v3, p5

    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 287
    .end local v6    # "x":F
    :cond_0
    add-int v4, v4, p5

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 294
    .end local v3    # "tx":I
    .end local v7    # "y":F
    .end local v10    # "j":I
    :cond_1
    return-void
.end method

.method public static decodeByteArray([BII)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    .line 82
    invoke-static {p0, p1, p2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "opts"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 73
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    if-nez v0, :cond_0

    .line 74
    invoke-static {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 78
    :goto_0
    return-object v0

    .line 75
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecMMCodec:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    if-nez v0, :cond_1

    .line 76
    invoke-static {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 78
    :cond_1
    invoke-static {p0, p1, p2, p3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "pathName"    # Ljava/lang/String;

    .prologue
    .line 86
    invoke-static {p0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "pathName"    # Ljava/lang/String;
    .param p1, "opts"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 90
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecMMCodec:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-boolean v0, p1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    if-nez v0, :cond_0

    .line 91
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 93
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;IZ)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p2, "targetSize"    # I
    .param p3, "isLarger"    # Z

    .prologue
    .line 145
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    if-nez v0, :cond_0

    .line 146
    invoke-static {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 150
    :goto_0
    return-object v0

    .line 147
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecMMCodec:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    if-nez v0, :cond_1

    .line 148
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 150
    :cond_1
    invoke-static {p0, p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static decodeFileDescriptor(Ljava/io/FileDescriptor;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "fd"    # Ljava/io/FileDescriptor;

    .prologue
    .line 97
    invoke-static {p0}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "outPadding"    # Landroid/graphics/Rect;
    .param p2, "opts"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 101
    invoke-static {p0, p1, p2}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "id"    # I

    .prologue
    .line 115
    invoke-static {p0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "id"    # I
    .param p2, "opts"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 105
    const/4 v1, 0x0

    .line 107
    .local v1, "result":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-static {p0, p1, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 111
    :goto_0
    return-object v1

    .line 108
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method

.method public static decodeResourceStream(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "value"    # Landroid/util/TypedValue;
    .param p2, "is"    # Ljava/io/InputStream;
    .param p3, "pad"    # Landroid/graphics/Rect;
    .param p4, "opts"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 119
    invoke-static {p0, p1, p2, p3, p4}, Landroid/graphics/BitmapFactory;->decodeResourceStream(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "is"    # Ljava/io/InputStream;

    .prologue
    .line 123
    invoke-static {p0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "is"    # Ljava/io/InputStream;
    .param p1, "outPadding"    # Landroid/graphics/Rect;
    .param p2, "opts"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 127
    invoke-static {p0, p1, p2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static getTextureSizeFromImageSize(I)I
    .locals 1
    .param p0, "size"    # I

    .prologue
    .line 183
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiplier:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->isUseImageFitTexture()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    invoke-static {p0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->getTextureSizeFromImageSize(I)I

    move-result v0

    .line 186
    :goto_0
    return v0

    :cond_0
    if-lez p0, :cond_1

    invoke-static {p0}, Lcom/sec/android/gallery3d/common/Utils;->nextPowerOf2(I)I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isUseFastLargeThumbnail()Z
    .locals 1

    .prologue
    .line 326
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v0, :cond_0

    .line 327
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->isUseFastLargeThumbnail()Z

    move-result v0

    .line 328
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isUseImageFitTexture()Z
    .locals 1

    .prologue
    .line 174
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v0, :cond_0

    .line 175
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->isUseImageFitTexture()Z

    move-result v0

    .line 177
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final setTileDecSize(I)V
    .locals 1
    .param p0, "size"    # I

    .prologue
    .line 431
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v0, :cond_0

    .line 432
    invoke-static {p0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->setTileDecSize(I)V

    .line 434
    :cond_0
    return-void
.end method


# virtual methods
.method public BitmapReuseFreeBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 380
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->isUseTileReuseMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->BitmapReuseFreeBitmap(Landroid/graphics/Bitmap;)V

    .line 385
    :goto_0
    return-void

    .line 383
    :cond_0
    invoke-static {}, Lcom/sec/android/photos/data/GalleryBitmapPool;->getInstance()Lcom/sec/android/photos/data/GalleryBitmapPool;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/photos/data/GalleryBitmapPool;->put(Landroid/graphics/Bitmap;)Z

    goto :goto_0
.end method

.method public centerDrawTiles(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;IIFIILcom/sec/android/gallery3d/ui/ScreenNail;II)V
    .locals 12
    .param p1, "tiv"    # Lcom/sec/android/gallery3d/ui/TileImageView;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p3, "r"    # Landroid/graphics/Rect;
    .param p4, "level"    # I
    .param p5, "size"    # I
    .param p6, "mScale"    # F
    .param p7, "offsetX"    # I
    .param p8, "offsetY"    # I
    .param p9, "mScreenNail"    # Lcom/sec/android/gallery3d/ui/ScreenNail;
    .param p10, "mImageWidth"    # I
    .param p11, "mImageHeight"    # I

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    invoke-virtual/range {v0 .. v11}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->centerDrawTiles(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;IIFIILcom/sec/android/gallery3d/ui/ScreenNail;II)V

    .line 209
    :cond_0
    return-void
.end method

.method public checkFlingVelocity(FF)V
    .locals 1
    .param p1, "vX"    # F
    .param p2, "vY"    # F

    .prologue
    .line 303
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_1

    .line 304
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->checkFlingVelocity(FF)V

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 305
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mSlidingAnimationInterface:Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mSlidingAnimationInterface:Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->setFlingVelocity(F)V

    goto :goto_0
.end method

.method public createCenterLargeThumbnail(Lcom/sec/android/gallery3d/data/MediaItem;I)V
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "index"    # I

    .prologue
    .line 332
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->createCenterLargeThumbnail(Lcom/sec/android/gallery3d/data/MediaItem;I)V

    .line 335
    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->dispatchTouchEvent(Landroid/view/MotionEvent;)V

    .line 141
    :cond_0
    return-void
.end method

.method public drawOnCenter(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIIIFFLcom/sec/android/gallery3d/ui/ScreenNail;I)Z
    .locals 9
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "offsetX"    # I
    .param p3, "offsetY"    # I
    .param p4, "imageWidth"    # I
    .param p5, "imageHeight"    # I
    .param p6, "rotation"    # F
    .param p7, "scale"    # F
    .param p8, "backUp"    # Lcom/sec/android/gallery3d/ui/ScreenNail;
    .param p9, "level"    # I

    .prologue
    .line 347
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->drawOnCenter(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIIIFFLcom/sec/android/gallery3d/ui/ScreenNail;)Z

    move-result v0

    .line 352
    :goto_0
    return v0

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    if-eqz v0, :cond_1

    .line 350
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p7

    move-object/from16 v7, p8

    move/from16 v8, p9

    invoke-virtual/range {v0 .. v8}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->drawOnCenter(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIIIFLcom/sec/android/gallery3d/ui/ScreenNail;I)Z

    move-result v0

    goto :goto_0

    .line 352
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public drawOnScreenNail(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;ZLcom/sec/android/gallery3d/ui/ScreenNail;IIIIII)V
    .locals 10
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "isNext"    # Z
    .param p3, "backup"    # Lcom/sec/android/gallery3d/ui/ScreenNail;
    .param p4, "cx"    # I
    .param p5, "cy"    # I
    .param p6, "drawW"    # I
    .param p7, "drawH"    # I
    .param p8, "rotation"    # I
    .param p9, "diff"    # I

    .prologue
    .line 438
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    invoke-virtual/range {v0 .. v9}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->drawOnScreenNail(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;ZLcom/sec/android/gallery3d/ui/ScreenNail;IIIIII)V

    .line 441
    :cond_0
    return-void
.end method

.method public drawTiles(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;IIFIIII)Z
    .locals 9
    .param p1, "tiv"    # Lcom/sec/android/gallery3d/ui/TileImageView;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p3, "r"    # Landroid/graphics/Rect;
    .param p4, "level"    # I
    .param p5, "size"    # I
    .param p6, "length"    # F
    .param p7, "mRealOffsetX"    # I
    .param p8, "mRealOffsetY"    # I
    .param p9, "offsetX"    # I
    .param p10, "offsetY"    # I

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p9

    move/from16 v8, p10

    invoke-virtual/range {v0 .. v8}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->drawTiles(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;IIFII)V

    .line 202
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    if-eqz v0, :cond_1

    .line 194
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-object v1, p2

    move v2, p4

    move v3, p5

    move/from16 v4, p7

    move/from16 v5, p8

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->drawTiles(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)Z

    move-result v0

    goto :goto_1

    .line 196
    :cond_1
    sget-boolean v0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->bUseCenterFirstRD:Z

    if-eqz v0, :cond_2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p9

    move/from16 v8, p10

    .line 197
    invoke-direct/range {v0 .. v8}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->CenterFirstdrawTiles(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;IIFII)V

    goto :goto_0

    :cond_2
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p9

    move/from16 v8, p10

    .line 199
    invoke-direct/range {v0 .. v8}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->SequencedrawTiles(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;IIFII)V

    goto :goto_0
.end method

.method public getAlphablendingAnimationInterface()Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    if-eqz v0, :cond_0

    .line 457
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->getAlphablendingAnimationInterface()Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    move-result-object v0

    .line 459
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXIVInterface()Lcom/sec/samsung/gallery/decoder/XIVInterface;
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    return-object v0
.end method

.method public initDecoder(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1, "obj1"    # Ljava/lang/Object;
    .param p2, "obj2"    # Ljava/lang/Object;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    check-cast p1, Landroid/view/WindowManager;

    .end local p1    # "obj1":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/gallery3d/app/GalleryApp;

    .end local p2    # "obj2":Ljava/lang/Object;
    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->initXIV(Landroid/view/WindowManager;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 135
    :cond_0
    return-void
.end method

.method public isDoingDecoding()Z
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->isDoingDecoding()Z

    move-result v0

    .line 158
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFilmMode()Z
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->isFilmMode()Z

    move-result v0

    .line 405
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isImageChanging()Z
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    if-eqz v0, :cond_0

    .line 424
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->isImageChanging()Z

    move-result v0

    .line 426
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLevelChanged(I)Z
    .locals 1
    .param p1, "currLevel"    # I

    .prologue
    .line 444
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->isLevelChanged(I)Z

    move-result v0

    .line 446
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMinLevel(FII)Z
    .locals 1
    .param p1, "currentScale"    # F
    .param p2, "imageWidth"    # I
    .param p3, "backUpWidth"    # I

    .prologue
    .line 318
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->isMinLevel(FII)Z

    move-result v0

    .line 321
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMultiViewState()Z
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->isMultiViewState()Z

    move-result v0

    .line 314
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScrolling()Z
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    if-eqz v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->isScrolling()Z

    move-result v0

    .line 419
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isZoomState(I)Z
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 371
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->isZoomState()Z

    move-result v0

    .line 376
    :goto_0
    return v0

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    if-eqz v0, :cond_1

    .line 374
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->isZoomState(I)Z

    move-result v0

    goto :goto_0

    .line 376
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPhotoView()Z
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->onPhotoView()Z

    move-result v0

    .line 392
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public prepareAlphablendingAnimation()V
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    if-eqz v0, :cond_0

    .line 451
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->prepareAlphablendingAnimation()V

    .line 453
    :cond_0
    return-void
.end method

.method public readyToDraw(I)Z
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 364
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->readyToDraw(I)Z

    move-result v0

    .line 367
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setQrd(Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V
    .locals 1
    .param p1, "regionDecoder"    # Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->setQrd(Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V

    .line 300
    :cond_0
    return-void
.end method

.method public setScrolling(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 410
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    if-eqz v0, :cond_0

    .line 411
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setScrolling(Z)V

    .line 413
    :cond_0
    return-void
.end method

.method public uploadCenterLargeThumbnail(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z
    .locals 1
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 338
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->uploadCenterLargeThumbnail(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z

    move-result v0

    .line 341
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public useCenterLargeThumbnail()Z
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->useCenterLargeThumbnail()Z

    move-result v0

    .line 360
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public waitDecodingStop()V
    .locals 4

    .prologue
    .line 162
    const/4 v0, 0x0

    .line 163
    .local v0, "count":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->isDoingDecoding()Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0xa

    if-ge v0, v2, :cond_0

    .line 165
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 167
    :catch_0
    move-exception v1

    .line 168
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 171
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_0
    return-void
.end method
