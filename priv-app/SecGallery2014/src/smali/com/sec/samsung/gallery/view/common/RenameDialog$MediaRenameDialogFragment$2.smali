.class Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$2;
.super Ljava/lang/Object;
.source "RenameDialog.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 10
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "spanned"    # Landroid/text/Spanned;
    .param p5, "dstStart"    # I
    .param p6, "dstEnd"    # I

    .prologue
    .line 192
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/common/EmojiList;->hasEmojiString(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 193
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mInvalidToast:Landroid/widget/Toast;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->access$300(Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    .line 194
    const-string v6, ""

    .line 249
    :goto_0
    return-object v6

    .line 197
    :cond_0
    sub-int v8, p3, p2

    const/16 v9, 0x64

    if-le v8, v9, :cond_1

    .line 198
    add-int/lit8 p3, p2, 0x64

    .line 199
    :cond_1
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 200
    .local v5, "origTxt":Ljava/lang/String;
    move-object v6, v5

    .line 201
    .local v6, "validTxt":Ljava/lang/String;
    const/4 v2, 0x0

    .line 203
    .local v2, "invalidFlag":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v8, Lcom/sec/samsung/gallery/view/common/RenameDialog;->INVALID_CHAR:[Ljava/lang/String;

    array-length v8, v8

    if-ge v0, v8, :cond_4

    .line 204
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    .line 205
    .local v7, "validTxtLength":I
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_2
    if-ge v3, v7, :cond_3

    .line 206
    sget-object v8, Lcom/sec/samsung/gallery/view/common/RenameDialog;->INVALID_CHAR:[Ljava/lang/String;

    aget-object v8, v8, v0

    invoke-virtual {v6, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 207
    .local v1, "index":I
    if-ltz v1, :cond_2

    .line 208
    const/4 v2, 0x1

    .line 210
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v1, v8, :cond_2

    .line 212
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x0

    invoke-virtual {v6, v9, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v9, v1, 0x1

    invoke-virtual {v6, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 205
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 203
    .end local v1    # "index":I
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 240
    .end local v3    # "j":I
    .end local v7    # "validTxtLength":I
    :cond_4
    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v8

    sub-int v9, p6, p5

    sub-int/2addr v8, v9

    rsub-int/lit8 v4, v8, 0x32

    .line 241
    .local v4, "keep":I
    if-gtz v4, :cond_5

    .line 245
    :cond_5
    if-eqz v2, :cond_6

    .line 246
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mInvalidToast:Landroid/widget/Toast;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->access$300(Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 249
    :cond_6
    const/4 v6, 0x0

    goto :goto_0
.end method
