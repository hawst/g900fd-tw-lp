.class public Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;
.super Lcom/sec/samsung/gallery/view/ViewObservable;
.source "NewAlbumCopyMoveDialog.java"


# instance fields
.field private final mCtx:Landroid/content/Context;

.field private mDialog:Landroid/app/Dialog;

.field private mIsClickConsumed:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/ViewObservable;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mIsClickConsumed:Z

    .line 39
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mCtx:Landroid/content/Context;

    .line 40
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->createDialog()V

    .line 41
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->putAppUseLog()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->handleOnClick(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->expandSplitView()V

    return-void
.end method

.method private expandSplitView()V
    .locals 3

    .prologue
    .line 98
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mCtx:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    .line 99
    .local v1, "mViewStateProxy":Lcom/sec/android/gallery3d/app/StateManager;
    if-eqz v1, :cond_0

    .line 100
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 101
    .local v0, "mViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    if-eqz v0, :cond_0

    instance-of v2, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    if-eqz v2, :cond_0

    .line 102
    check-cast v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    .end local v0    # "mViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->SetExpandSplitView()V

    .line 105
    :cond_0
    return-void
.end method

.method private handleOnClick(I)V
    .locals 1
    .param p1, "eventType"    # I

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mIsClickConsumed:Z

    if-nez v0, :cond_0

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mIsClickConsumed:Z

    .line 110
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->notifyObservers(Ljava/lang/Object;)V

    .line 112
    :cond_0
    return-void
.end method

.method private putAppUseLog()V
    .locals 7

    .prologue
    .line 126
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mCtx:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    .line 127
    .local v1, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 128
    .local v2, "topSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_0

    .line 129
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v0

    .line 130
    .local v0, "albumCount":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mCtx:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ABCO"

    mul-int/lit16 v5, v0, 0x3e8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mCtx:Landroid/content/Context;

    invoke-static {v3, v4, v5, v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 133
    .end local v0    # "albumCount":I
    :cond_0
    return-void
.end method


# virtual methods
.method public createDialog()V
    .locals 3

    .prologue
    .line 56
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mCtx:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 58
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mCtx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01a5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 59
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mCtx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0237

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 61
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mCtx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog$1;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog$1;-><init>(Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 71
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mCtx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01ac

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog$2;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog$2;-><init>(Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 81
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mCtx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0046

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog$3;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog$3;-><init>(Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 91
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mDialog:Landroid/app/Dialog;

    .line 92
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mDialog:Landroid/app/Dialog;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 95
    :cond_0
    return-void
.end method

.method public dismissDialog()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 53
    :cond_0
    return-void
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    .line 122
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTitle(I)V
    .locals 1
    .param p1, "titleId"    # I

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setTitle(I)V

    .line 117
    :cond_0
    return-void
.end method

.method public showDialog()V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 47
    :cond_0
    return-void
.end method
