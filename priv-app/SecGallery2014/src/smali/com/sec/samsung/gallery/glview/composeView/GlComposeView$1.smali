.class Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;
.super Lcom/sec/android/gallery3d/glcore/GlHandler;
.source "GlComposeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/glcore/GlHandler;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    return-void
.end method


# virtual methods
.method public onMessage(ILjava/lang/Object;III)V
    .locals 7
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    const/16 v4, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 262
    const/16 v0, 0xe

    if-ne p1, v0, :cond_0

    .line 263
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_LAUNCH_FINNISH:Z

    .line 300
    :goto_0
    return-void

    .line 267
    :cond_0
    if-ne p1, v2, :cond_2

    .line 268
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0, p3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->updateSize(I)V

    .line 299
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onMessageExtra(ILjava/lang/Object;III)V

    goto :goto_0

    .line 269
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    .line 270
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->updateContentRange(II)V
    invoke-static {v0, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$000(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;II)V

    goto :goto_1

    .line 271
    :cond_3
    const/4 v0, 0x3

    if-ne p1, v0, :cond_4

    .line 272
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v0, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->update(II)Z

    goto :goto_1

    .line 273
    :cond_4
    const/4 v0, 0x4

    if-ne p1, v0, :cond_5

    .line 274
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v0, p3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->updateCheckBox(I)V

    goto :goto_1

    .line 275
    :cond_5
    const/4 v0, 0x5

    if-ne p1, v0, :cond_6

    .line 276
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setCheckBoxVisibility()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$100(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    .line 277
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setCheckBoxVisibility()V

    goto :goto_1

    .line 278
    :cond_6
    const/4 v0, 0x6

    if-ne p1, v0, :cond_7

    .line 279
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getRootObject()Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v6

    .line 280
    .local v6, "rootObj":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-eqz v6, :cond_1

    .line 281
    iget-object v0, v6, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixValEx:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 282
    iget-object v0, v6, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixValEx:[F

    const v2, -0x3f79999a    # -4.2f

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGravityData:F
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$200(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)F

    move-result v4

    mul-float/2addr v2, v4

    const/high16 v5, 0x3f800000    # 1.0f

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->setRotateM([FIFFFF)V

    goto :goto_1

    .line 284
    .end local v6    # "rootObj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_7
    const/4 v0, 0x7

    if-ne p1, v0, :cond_8

    .line 285
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v0, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setFocusBorderVisible(II)V

    goto :goto_1

    .line 286
    :cond_8
    if-eq p1, v4, :cond_9

    const/16 v0, 0x9

    if-ne p1, v0, :cond_a

    .line 287
    :cond_9
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->handleQuickScroll(II)V
    invoke-static {v0, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$300(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;II)V

    goto :goto_1

    .line 288
    :cond_a
    const/16 v0, 0xb

    if-ne p1, v0, :cond_b

    .line 289
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->updateFromQueue()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$400(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    goto :goto_1

    .line 290
    :cond_b
    const/16 v0, 0xc

    if-ne p1, v0, :cond_c

    .line 291
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->resetLayout()V

    goto/16 :goto_1

    .line 292
    :cond_c
    const/16 v0, 0xd

    if-ne p1, v0, :cond_d

    .line 293
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v0, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setTitleFocusBorderVisible(II)V

    goto/16 :goto_1

    .line 294
    :cond_d
    const/16 v0, 0xf

    if-ne p1, v0, :cond_e

    .line 295
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->initPenSelect()V

    goto/16 :goto_1

    .line 296
    :cond_e
    const/16 v0, 0x10

    if-ne p1, v0, :cond_1

    .line 297
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # setter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateDefaultMax:I
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$502(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;I)I

    goto/16 :goto_1
.end method
