.class interface abstract Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$ITimeViewMode;
.super Ljava/lang/Object;
.source "TimeViewEventHandle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "ITimeViewMode"
.end annotation


# virtual methods
.method public abstract disableView()V
.end method

.method public abstract enableView()V
.end method

.method public abstract initializeView()V
.end method

.method public abstract onBackPressed()V
.end method

.method public abstract onItemClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)V
.end method
