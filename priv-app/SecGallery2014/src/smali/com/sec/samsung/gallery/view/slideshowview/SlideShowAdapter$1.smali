.class Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;
.super Ljava/lang/Object;
.source "SlideShowAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->createFilterBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

.field final synthetic val$bitmap:Landroid/graphics/Bitmap;

.field final synthetic val$filter:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;Landroid/graphics/Bitmap;I)V
    .locals 0

    .prologue
    .line 476
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->val$bitmap:Landroid/graphics/Bitmap;

    iput p3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->val$filter:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 479
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->val$bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->val$bitmap:Landroid/graphics/Bitmap;

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->filter_bitmap:Landroid/graphics/Bitmap;
    invoke-static {v0, v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->access$102(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 482
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    mul-int v9, v0, v3

    .line 483
    .local v9, "size":I
    new-array v1, v9, [I

    .line 484
    .local v1, "bitmap_data":[I
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->val$bitmap:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 486
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSecImaging:Lcom/samsung/android/secimaging/SecImaging;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->access$200(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;)Lcom/samsung/android/secimaging/SecImaging;

    move-result-object v0

    if-nez v0, :cond_1

    .line 487
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v3, "cannot find SecImaging"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 522
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->access$400(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 523
    :try_start_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->access$400(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 524
    monitor-exit v3

    .line 527
    .end local v1    # "bitmap_data":[I
    .end local v9    # "size":I
    :cond_0
    :goto_0
    return-void

    .line 524
    .restart local v1    # "bitmap_data":[I
    .restart local v9    # "size":I
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 491
    :cond_1
    :try_start_2
    iget v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->val$filter:I

    packed-switch v0, :pswitch_data_0

    .line 508
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSecImaging:Lcom/samsung/android/secimaging/SecImaging;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->access$200(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;)Lcom/samsung/android/secimaging/SecImaging;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v3, v4}, Lcom/samsung/android/secimaging/SecImaging;->doProcess([III)[I

    move-result-object v2

    .line 511
    .local v2, "filter_data":[I
    if-eqz v2, :cond_2

    .line 512
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v2 .. v7}, Landroid/graphics/Bitmap;->createBitmap([IIIIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->filter_bitmap:Landroid/graphics/Bitmap;
    invoke-static {v0, v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->access$102(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 522
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->access$400(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 523
    :try_start_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->access$400(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 524
    monitor-exit v3

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 493
    .end local v2    # "filter_data":[I
    :pswitch_1
    :try_start_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSecImaging:Lcom/samsung/android/secimaging/SecImaging;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->access$200(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;)Lcom/samsung/android/secimaging/SecImaging;

    move-result-object v0

    const-string v3, "com.sec.android.filter,libgreyscale.so"

    invoke-virtual {v0, v3}, Lcom/samsung/android/secimaging/SecImaging;->setEffect(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    goto :goto_1

    .line 517
    .end local v1    # "bitmap_data":[I
    .end local v9    # "size":I
    :catch_0
    move-exception v8

    .line 518
    .local v8, "e":Ljava/lang/OutOfMemoryError;
    :try_start_5
    invoke-virtual {v8}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 522
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->access$400(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 523
    :try_start_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->access$400(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 524
    monitor-exit v3

    goto :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    .line 497
    .end local v8    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v1    # "bitmap_data":[I
    .restart local v9    # "size":I
    :pswitch_2
    :try_start_7
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSecImaging:Lcom/samsung/android/secimaging/SecImaging;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->access$200(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;)Lcom/samsung/android/secimaging/SecImaging;

    move-result-object v0

    const-string v3, "com.sec.android.filter,libsepia.so"

    invoke-virtual {v0, v3}, Lcom/samsung/android/secimaging/SecImaging;->setEffect(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    goto :goto_1

    .line 519
    .end local v1    # "bitmap_data":[I
    .end local v9    # "size":I
    :catch_1
    move-exception v8

    .line 520
    .local v8, "e":Ljava/lang/Exception;
    :try_start_8
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 522
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->access$400(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 523
    :try_start_9
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->access$400(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 524
    monitor-exit v3

    goto/16 :goto_0

    :catchall_3
    move-exception v0

    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    throw v0

    .line 501
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v1    # "bitmap_data":[I
    .restart local v9    # "size":I
    :pswitch_3
    :try_start_a
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSecImaging:Lcom/samsung/android/secimaging/SecImaging;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->access$200(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;)Lcom/samsung/android/secimaging/SecImaging;

    move-result-object v0

    const-string v3, "com.sec.android.filter,libvintage.so"

    invoke-virtual {v0, v3}, Lcom/samsung/android/secimaging/SecImaging;->setEffect(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    goto/16 :goto_1

    .line 522
    .end local v1    # "bitmap_data":[I
    .end local v9    # "size":I
    :catchall_4
    move-exception v0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mLock:Ljava/lang/Object;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->access$400(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 523
    :try_start_b
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->access$400(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notify()V

    .line 524
    monitor-exit v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    throw v0

    :catchall_5
    move-exception v0

    :try_start_c
    monitor-exit v3
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    throw v0

    .line 491
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
