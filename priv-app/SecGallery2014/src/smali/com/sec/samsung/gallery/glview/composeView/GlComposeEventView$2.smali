.class Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$2;
.super Ljava/lang/Object;
.source "GlComposeEventView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getOject(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 244
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v0

    .line 245
    .local v0, "object":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-nez v0, :cond_0

    .line 246
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    add-int/lit8 v3, p1, -0x1

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 247
    .local v1, "thumbObject":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-nez v1, :cond_1

    .line 248
    const/4 v0, 0x0

    .line 251
    .end local v0    # "object":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .end local v1    # "thumbObject":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_0
    :goto_0
    return-object v0

    .line 249
    .restart local v0    # "object":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .restart local v1    # "thumbObject":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_1
    iget-object v0, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mAlbumTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    goto :goto_0
.end method

.method public update(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 4
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 230
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v1, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveStart:I

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveEnd:I

    if-ge v1, v2, :cond_1

    .line 231
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 232
    .local v0, "glObject":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-eqz v0, :cond_0

    .line 233
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    invoke-virtual {p1, v2, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->addChild(Landroid/view/View;I)V

    .line 234
    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mAlbumTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v2, :cond_0

    .line 235
    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mAlbumTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 236
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mAlbumTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    invoke-virtual {p1, v2, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->addChild(Landroid/view/View;I)V

    .line 230
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 240
    .end local v0    # "glObject":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_1
    return-void
.end method
