.class public final enum Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;
.super Ljava/lang/Enum;
.source "StartCollageCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/StartCollageCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CollageCmdType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;

.field public static final enum START_COLLAGE_MAKER:Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;

.field public static final enum START_COLLAGE_VIEWER:Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    new-instance v0, Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;

    const-string v1, "START_COLLAGE_MAKER"

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;->START_COLLAGE_MAKER:Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;

    new-instance v0, Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;

    const-string v1, "START_COLLAGE_VIEWER"

    invoke-direct {v0, v1, v3}, Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;->START_COLLAGE_VIEWER:Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;

    .line 32
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;

    sget-object v1, Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;->START_COLLAGE_MAKER:Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;->START_COLLAGE_VIEWER:Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;->$VALUES:[Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 32
    const-class v0, Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;

    return-object v0
.end method

.method public static values()[Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;->$VALUES:[Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;

    invoke-virtual {v0}, [Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;

    return-object v0
.end method
