.class public Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;
.super Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
.source "GlComposeSplitView.java"


# static fields
.field protected static final CMD_SELECT_ALBUM:I = 0x65

.field protected static final CMD_UPDATE_HEADER:I = 0x64

.field protected static final DURATION:I = 0x226

.field protected static final INDENT_NEW_ALBUM_DIVIDER:I = 0x3

.field public static SPLIT_RATIO_LAND:F = 0.0f

.field public static SPLIT_RATIO_PORT:F = 0.0f

.field public static final STATUS_EXPAND_CHANGE:I = 0xa


# instance fields
.field public mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field private mComposeSplitViewAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

.field mExpandedAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

.field mFadeInAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

.field private mFlags:I

.field public mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field private mInterpolator:Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

.field mListenerNewAlbumClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

.field private mNeedSetMode:Z

.field private mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

.field public mNewAlbumHeaderFocused:Z

.field private mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

.field private mPressed:Z

.field mShrinkAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

.field private mSupportExpand:Z

.field private mValidView:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/sec/android/gallery3d/data/MediaItem;ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "initCode"    # I
    .param p3, "initItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p4, "option"    # I
    .param p5, "config"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 47
    invoke-direct/range {p0 .. p5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;-><init>(Landroid/content/Context;ILcom/sec/android/gallery3d/data/MediaItem;ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;)V

    .line 36
    new-instance v0, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mInterpolator:Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    .line 37
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 38
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 40
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mSupportExpand:Z

    .line 41
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPressed:Z

    .line 42
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNeedSetMode:Z

    .line 43
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFlags:I

    .line 44
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbumHeaderFocused:Z

    .line 469
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView$1;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mListenerNewAlbumClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 476
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView$2;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mExpandedAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 492
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView$3;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mShrinkAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 508
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView$4;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFadeInAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 526
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView$5;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mComposeSplitViewAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

    .line 49
    iget v0, p5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mLandRatio:F

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->SPLIT_RATIO_LAND:F

    .line 50
    iget v0, p5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mPortRatio:F

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->SPLIT_RATIO_PORT:F

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->resetBackgroundLayout()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;)Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    return-object v0
.end method

.method private createArrowObject()V
    .locals 6

    .prologue
    const/4 v4, 0x2

    .line 225
    new-instance v2, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 226
    .local v2, "imgView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const v3, 0x7f02026a

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 227
    invoke-virtual {v2, v4, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 229
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitFocusedArrowWidthPixel()I

    move-result v1

    .line 230
    .local v1, "arrowW":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitFocusedArrowHeightPixel()I

    move-result v0

    .line 232
    .local v0, "arrowH":I
    new-instance v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 233
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    new-instance v4, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v4, v5, v1, v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 235
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 236
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPosIndex()I

    move-result v4

    iput v4, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDispmnt:I

    .line 237
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 238
    return-void
.end method

.method private createBackgroundObject()V
    .locals 3

    .prologue
    .line 216
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 217
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setUseTouchEvent(Z)V

    .line 218
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEmptyFill(Z)V

    .line 219
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0051

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEmptyFillColor(I)V

    .line 222
    :cond_0
    return-void
.end method

.method private initializeMode()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 354
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 355
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->initialize(Z)V

    .line 356
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->getHeaderObject()Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mListenerNewAlbumClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)V

    .line 357
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->animateScrollForNewAlbum(ZZ)V

    .line 359
    :cond_0
    return-void
.end method

.method private resetBackgroundLayout()V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 157
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-nez v7, :cond_1

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mWideMode:Z

    if-eqz v7, :cond_2

    sget v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->SPLIT_RATIO_LAND:F

    .line 161
    .local v5, "ratio":F
    :goto_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-boolean v7, v7, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mValidView:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->convX(I)F

    move-result v7

    neg-float v3, v7

    .line 162
    .local v3, "expandDelta":F
    :goto_2
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitExtraGapPixel()I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->convX(I)F

    move-result v7

    neg-float v4, v7

    .line 163
    .local v4, "extraGap":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/high16 v8, 0x3f000000    # 0.5f

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mWidthSpace:F

    mul-float/2addr v8, v9

    const/high16 v9, 0x3f800000    # 1.0f

    sub-float v9, v5, v9

    mul-float/2addr v8, v9

    add-float/2addr v8, v3

    add-float/2addr v8, v4

    sget v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->DEF_DISTANCE:F

    neg-float v9, v9

    invoke-virtual {v7, v8, v6, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 164
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mWidthSpace:F

    mul-float/2addr v8, v5

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mHeightSpace:F

    invoke-virtual {v7, v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    .line 166
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitFocusedArrowWidthPixel()I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->convX(I)F

    move-result v1

    .line 167
    .local v1, "arrowW":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitFocusedArrowHeightPixel()I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->convY(I)F

    move-result v0

    .line 168
    .local v0, "arrowH":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getWidth(Z)F

    move-result v7

    sub-float/2addr v7, v1

    const/high16 v8, 0x40000000    # 2.0f

    div-float v2, v7, v8

    .line 169
    .local v2, "cx":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v7, :cond_0

    .line 170
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v7, v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    .line 171
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v8, 0x1

    invoke-virtual {v7, v2, v6, v6, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFFI)V

    goto :goto_0

    .line 160
    .end local v0    # "arrowH":F
    .end local v1    # "arrowW":F
    .end local v2    # "cx":F
    .end local v3    # "expandDelta":F
    .end local v4    # "extraGap":F
    .end local v5    # "ratio":F
    :cond_2
    sget v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->SPLIT_RATIO_PORT:F

    goto :goto_1

    .restart local v5    # "ratio":F
    :cond_3
    move v3, v6

    .line 161
    goto :goto_2
.end method

.method private resetSplitLayout()V
    .locals 9

    .prologue
    const/4 v5, 0x2

    const/4 v8, 0x1

    const/high16 v4, 0x40400000    # 3.0f

    const/4 v2, 0x0

    .line 332
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->resetLayout()V

    .line 333
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mMode:I

    if-ne v0, v5, :cond_2

    .line 334
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->stopTransAnim(Z)V

    .line 335
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->getHeight()F

    move-result v1

    neg-float v1, v1

    sub-float/2addr v1, v4

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFFI)V

    .line 336
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->getHeight()F

    move-result v0

    add-float v7, v0, v4

    .line 342
    .local v7, "cy":F
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    invoke-virtual {v0, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->setScrollTopMargine(F)V

    .line 343
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setMaxScrollable()V

    .line 344
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    invoke-virtual {v0, v2, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setRange(FF)V

    .line 345
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollable:F

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    invoke-virtual {v0, v1, v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setScroll(FZ)V

    .line 347
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_1

    .line 350
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDispmnt:I

    neg-float v3, v7

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(IFFFI)V

    .line 351
    :cond_1
    return-void

    .line 338
    .end local v7    # "cy":F
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mValidView:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->convX(I)F

    move-result v0

    neg-float v6, v0

    .line 339
    .local v6, "cx":F
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v6, v2, v2, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFFI)V

    .line 340
    const/4 v7, 0x0

    .restart local v7    # "cy":F
    goto :goto_0

    .end local v6    # "cx":F
    .end local v7    # "cy":F
    :cond_3
    move v6, v2

    .line 338
    goto :goto_1
.end method

.method private setValidView()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 114
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mWideMode:Z

    if-eqz v4, :cond_0

    sget v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->SPLIT_RATIO_LAND:F

    .line 115
    .local v1, "ratio":F
    :goto_0
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mWidth:I

    int-to-float v4, v4

    mul-float/2addr v4, v1

    float-to-int v2, v4

    .line 116
    .local v2, "splitWidth":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-boolean v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-eqz v4, :cond_1

    neg-int v0, v2

    .line 118
    .local v0, "offset":I
    :goto_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mValidView:Landroid/graphics/Rect;

    if-nez v4, :cond_2

    .line 119
    new-instance v4, Landroid/graphics/Rect;

    add-int v5, v2, v0

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mHeight:I

    invoke-direct {v4, v0, v3, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mValidView:Landroid/graphics/Rect;

    .line 122
    :goto_2
    return-void

    .line 114
    .end local v0    # "offset":I
    .end local v1    # "ratio":F
    .end local v2    # "splitWidth":I
    :cond_0
    sget v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->SPLIT_RATIO_PORT:F

    goto :goto_0

    .restart local v1    # "ratio":F
    .restart local v2    # "splitWidth":I
    :cond_1
    move v0, v3

    .line 116
    goto :goto_1

    .line 121
    .restart local v0    # "offset":I
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mValidView:Landroid/graphics/Rect;

    add-int v5, v2, v0

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mHeight:I

    invoke-virtual {v4, v0, v3, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_2
.end method


# virtual methods
.method public addLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 1
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 55
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->addLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    .line 56
    instance-of v0, p1, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    if-eqz v0, :cond_0

    .line 57
    check-cast p1, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    .end local p1    # "layer":Lcom/sec/android/gallery3d/glcore/GlLayer;
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iput-object p0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    .line 60
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSupportExpand:Z

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mSupportExpand:Z

    .line 62
    :cond_0
    return-void
.end method

.method public animateScrollForNewAlbum(ZZ)V
    .locals 10
    .param p1, "active"    # Z
    .param p2, "doAnimation"    # Z

    .prologue
    const/high16 v4, 0x40400000    # 3.0f

    const/4 v9, 0x0

    .line 404
    if-eqz p2, :cond_4

    .line 405
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mInterpolator:Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;->setType(I)V

    .line 406
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->resetTransformAnimation()V

    .line 407
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEnablePosAnim(Z)V

    .line 408
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPosAnimMode(Z)V

    .line 409
    if-eqz p1, :cond_3

    .line 410
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourcePos(FFF)V

    .line 411
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->getHeight()F

    move-result v2

    neg-float v2, v2

    sub-float/2addr v2, v4

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetPos(FFF)V

    .line 416
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    const-wide/16 v2, 0x226

    const-wide/16 v4, 0x0

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mInterpolator:Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->startTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;JJLcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    .line 424
    :goto_1
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->getHeight()F

    move-result v9

    .line 425
    .local v9, "scrollMargine":F
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_1

    .line 426
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDispmnt:I

    const/4 v2, 0x0

    neg-float v3, v9

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(IFFFI)V

    .line 427
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->getScroll()F

    move-result v8

    .line 428
    .local v8, "scroll":F
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    invoke-virtual {v0, v9}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->setScrollTopMargine(F)V

    .line 429
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setMaxScrollable()V

    .line 430
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    if-eqz v0, :cond_2

    .line 431
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mWidthViewRatio:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mHeightViewRatio:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mWidthSpace:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mHeightSpace:F

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->getHeight()F

    move-result v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getMarginTop()F

    move-result v6

    add-float/2addr v5, v6

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->reset(FFFFFF)V

    .line 432
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setRange(FF)V

    .line 433
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v1, v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getValidScroll(F)F

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setScroll(FZ)V

    .line 437
    .end local v8    # "scroll":F
    .end local v9    # "scrollMargine":F
    :goto_2
    return-void

    .line 413
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourcePos(FFF)V

    .line 414
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getY()F

    move-result v2

    neg-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetPos(FFF)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 434
    :catch_0
    move-exception v7

    .line 435
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 418
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_4
    if-eqz p1, :cond_5

    .line 419
    :try_start_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->getHeight()F

    move-result v2

    neg-float v2, v2

    sub-float/2addr v2, v4

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFFI)V

    goto/16 :goto_1

    .line 421
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFFI)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method public fadeIn()V
    .locals 9

    .prologue
    const-wide/16 v2, 0x226

    const-wide/16 v4, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 381
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 382
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mInterpolator:Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;->setType(I)V

    .line 383
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->resetTransformAnimation()V

    .line 384
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEnablePosAnim(Z)V

    .line 385
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPosAnimMode(Z)V

    .line 386
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v7, v7, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourcePos(FFF)V

    .line 387
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mValidView:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->convX(I)F

    move-result v1

    invoke-virtual {v0, v1, v7, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetPos(FFF)V

    .line 388
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mShrinkAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mInterpolator:Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->startTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;JJLcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    .line 389
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->setValidView()V

    .line 390
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->resetTransformAnimation()V

    .line 392
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEnablePosAnim(Z)V

    .line 393
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPosAnimMode(Z)V

    .line 394
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v7, v7, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourcePos(FFF)V

    .line 395
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mValidView:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->convX(I)F

    move-result v1

    invoke-virtual {v0, v1, v7, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetPos(FFF)V

    .line 396
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFadeInAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mInterpolator:Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->startTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;JJLcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    .line 398
    :cond_0
    return-void
.end method

.method public fadeOut()V
    .locals 9

    .prologue
    const-wide/16 v2, 0x226

    const-wide/16 v4, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 362
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mInterpolator:Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    invoke-virtual {v0, v8}, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;->setType(I)V

    .line 363
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->resetTransformAnimation()V

    .line 364
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEnablePosAnim(Z)V

    .line 365
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPosAnimMode(Z)V

    .line 366
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v7, v7, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourcePos(FFF)V

    .line 367
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mValidView:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->convX(I)F

    move-result v1

    neg-float v1, v1

    invoke-virtual {v0, v1, v7, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetPos(FFF)V

    .line 368
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mExpandedAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mInterpolator:Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->startTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;JJLcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    .line 369
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->setValidView()V

    .line 370
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->resetTransformAnimation()V

    .line 372
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEnablePosAnim(Z)V

    .line 373
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPosAnimMode(Z)V

    .line 374
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v7, v7, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourcePos(FFF)V

    .line 375
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mValidView:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->convX(I)F

    move-result v1

    neg-float v1, v1

    invoke-virtual {v0, v1, v7, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetPos(FFF)V

    .line 376
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mInterpolator:Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->startTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;JJLcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    .line 378
    :cond_0
    return-void
.end method

.method public getFirstThumbObject()Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mGlObject:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getNewAlbumObject()Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->getHeaderObject()Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v0

    return-object v0
.end method

.method public handleSelectAlbum(II)V
    .locals 8
    .param p1, "index"    # I
    .param p2, "changed"    # I

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 288
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-nez v3, :cond_1

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 291
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    if-eqz v3, :cond_0

    .line 294
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    if-eqz v3, :cond_2

    .line 295
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 297
    :cond_2
    if-eqz p2, :cond_3

    .line 298
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    .line 300
    :cond_3
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mMode:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_4

    if-ne p1, v7, :cond_4

    .line 301
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbumHeaderFocused:Z

    .line 302
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3, v7, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setFocused(IZ)V

    .line 303
    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->setArrowObjectVisibility(Z)V

    .line 304
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->getNewAlbumObject()Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->checkStartSpreadAnimation(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)V

    goto :goto_0

    .line 307
    :cond_4
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbumHeaderFocused:Z

    .line 308
    shl-int/lit8 v0, p1, 0x10

    .line 309
    .local v0, "code":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getScrollForIndex(I)F

    move-result v2

    .line 310
    .local v2, "scroll":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getVisibleScrollDelta(F)F

    move-result v1

    .line 311
    .local v1, "newScroll":F
    const/4 v3, 0x0

    cmpl-float v3, v1, v3

    if-eqz v3, :cond_5

    .line 312
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollable:F

    add-float/2addr v1, v3

    .line 313
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getValidScroll(F)F

    move-result v1

    .line 314
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 315
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3, v1, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setScroll(FZ)V

    .line 317
    :cond_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3, v0, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setFocused(IZ)V

    .line 318
    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->setArrowObjectVisibility(Z)V

    .line 319
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    shl-int/lit8 v4, p1, 0x10

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->checkStartSpreadAnimation(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)V

    goto :goto_0
.end method

.method public isFadeAnimRunning()Z
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->isRunning()Z

    move-result v0

    return v0
.end method

.method protected onCreate()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 66
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mSupportExpand:Z

    if-eqz v0, :cond_0

    .line 67
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->createBackgroundObject()V

    .line 69
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->setValidView()V

    .line 70
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onCreate()V

    .line 72
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mSupportExpand:Z

    if-nez v0, :cond_3

    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 86
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mInitialCode:I

    invoke-virtual {v0, v1, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setFocused(IZ)V

    .line 87
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    .line 88
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->IsCheckMode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNeedSetMode:Z

    .line 90
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlingInTalkBack:Z

    if-eqz v0, :cond_2

    .line 91
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mGlComposeViewAccessibility:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mComposeSplitViewAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->setAccessibilityNodeInfoListener(Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;)V

    .line 93
    :cond_2
    return-void

    .line 75
    :cond_3
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-nez v0, :cond_4

    .line 76
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->createArrowObject()V

    .line 80
    :goto_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->resetBackgroundLayout()V

    .line 81
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mValidView:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->convX(I)F

    move-result v1

    neg-float v1, v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v4, v4, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFFI)V

    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    goto :goto_0

    .line 78
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    goto :goto_1
.end method

.method protected onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 445
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->isSlideShowMode:Z

    if-eqz v3, :cond_1

    .line 466
    :cond_0
    :goto_0
    return v1

    .line 447
    :cond_1
    const/16 v3, 0xa8

    if-eq p1, v3, :cond_2

    const/16 v3, 0xa9

    if-eq p1, v3, :cond_2

    const/16 v3, 0x118

    if-eq p1, v3, :cond_2

    const/16 v3, 0x117

    if-ne p1, v3, :cond_3

    :cond_2
    move v1, v2

    .line 449
    goto :goto_0

    .line 452
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getCount()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->isScreenLocked()Z

    move-result v3

    if-nez v3, :cond_0

    .line 455
    const/16 v3, 0x42

    if-eq p1, v3, :cond_4

    const/16 v3, 0x17

    if-ne p1, v3, :cond_6

    .line 456
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-ltz v3, :cond_5

    .line 457
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    if-eqz v3, :cond_5

    .line 458
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    shl-int/lit8 v3, v3, 0x10

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v0

    .line 459
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v0, :cond_0

    .line 460
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-interface {v3, v4, v5, v6, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)Z

    .line 461
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v4, 0x4

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {v3, v4, v5, v1, v1}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .end local v0    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_5
    move v1, v2

    .line 464
    goto :goto_0

    .line 466
    :cond_6
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method protected onMessageExtra(ILjava/lang/Object;III)V
    .locals 1
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    .line 126
    const/16 v0, 0x64

    if-ne p1, v0, :cond_1

    .line 127
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->updateHeaderImage()V

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    .line 129
    invoke-virtual {p0, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->handleSelectAlbum(II)V

    goto :goto_0
.end method

.method protected onMoved(II)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 187
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPressed:Z

    if-nez v0, :cond_0

    .line 188
    const/4 v0, 0x0

    .line 190
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onMoved(II)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPressed(II)Z
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 177
    if-ne p1, v1, :cond_0

    if-eq p2, v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mValidView:Landroid/graphics/Rect;

    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 178
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPressed:Z

    .line 179
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onPressed(II)Z

    move-result v0

    .line 182
    :goto_0
    return v0

    .line 181
    :cond_2
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPressed:Z

    goto :goto_0
.end method

.method protected onReleased(IIII)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    .line 195
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPressed:Z

    if-nez v0, :cond_0

    .line 196
    const/4 v0, 0x0

    .line 198
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onReleased(IIII)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSetMode(IILjava/lang/Object;)V
    .locals 4
    .param p1, "mode"    # I
    .param p2, "parm"    # I
    .param p3, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 203
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    if-nez v2, :cond_1

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 204
    :cond_1
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mMode:I

    if-ne v2, v3, :cond_2

    .line 205
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->clean()V

    .line 206
    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->animateScrollForNewAlbum(ZZ)V

    .line 208
    :cond_2
    if-ne p1, v3, :cond_0

    .line 209
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->initialize(Z)V

    .line 210
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->getHeaderObject()Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mListenerNewAlbumClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)V

    .line 211
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-nez v2, :cond_3

    move v0, v1

    :cond_3
    invoke-virtual {p0, v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->animateScrollForNewAlbum(ZZ)V

    goto :goto_0
.end method

.method protected playSoundOnClickThumb()V
    .locals 2

    .prologue
    .line 523
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 524
    return-void
.end method

.method public redrawNewAlbumImage()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 260
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 263
    :cond_0
    return-void
.end method

.method protected resetLayout()V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->setValidView()V

    .line 107
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->resetLayout()V

    .line 108
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->resetSplitLayout()V

    .line 109
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->resetScrollBar()V

    .line 110
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->resetBackgroundLayout()V

    .line 111
    return-void
.end method

.method protected resetScrollBar()V
    .locals 7

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    if-nez v0, :cond_1

    .line 139
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->setValidView()V

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->resetScrollBar()V

    .line 145
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->getHeight()F

    move-result v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getMarginTop()F

    move-result v1

    add-float v5, v0, v1

    .line 146
    .local v5, "scrollTopMargin":F
    :goto_1
    const/4 v6, 0x0

    .line 147
    .local v6, "scrollBottomMargin":F
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mWidthViewRatio:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mHeightViewRatio:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mWidthSpace:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mHeightSpace:F

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->reset(FFFFFF)V

    .line 148
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->setValidView()V

    .line 149
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mWidth:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mValidView:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0xa

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->convX(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setRightPadding(F)V

    .line 151
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->isExpanded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setVisibility(Z)V

    goto :goto_0

    .line 145
    .end local v5    # "scrollTopMargin":F
    .end local v6    # "scrollBottomMargin":F
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getMarginTop()F

    move-result v5

    goto :goto_1
.end method

.method public selectAlbum(IZ)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "changed"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 278
    if-eqz p2, :cond_0

    .line 279
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->markStartSpreadAnimation()V

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    if-eqz v0, :cond_1

    .line 282
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mMode:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    const/4 v0, -0x1

    if-ne p1, v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbumHeaderFocused:Z

    .line 283
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/16 v3, 0x65

    if-eqz p2, :cond_3

    :goto_1
    invoke-virtual {v0, v3, p1, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 285
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 282
    goto :goto_0

    :cond_3
    move v1, v2

    .line 283
    goto :goto_1
.end method

.method public setAdapter(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    .prologue
    .line 97
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setAdapter(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)V

    .line 98
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNeedSetMode:Z

    if-eqz v0, :cond_0

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNeedSetMode:Z

    .line 100
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->initializeMode()V

    .line 102
    :cond_0
    return-void
.end method

.method public setArrowObjectVisibility(Z)V
    .locals 1
    .param p1, "isVisible"    # Z

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getVisibility()Z

    move-result v0

    if-ne p1, v0, :cond_1

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 243
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    goto :goto_0
.end method

.method public setFlags(IZ)V
    .locals 2
    .param p1, "flag"    # I
    .param p2, "set"    # Z

    .prologue
    .line 323
    if-eqz p2, :cond_0

    .line 324
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFlags:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFlags:I

    .line 327
    :goto_0
    return-void

    .line 326
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFlags:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFlags:I

    goto :goto_0
.end method

.method public setNewAlbumUnfocused()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 255
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbumHeaderFocused:Z

    .line 256
    invoke-virtual {p0, v1, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->updateNewAlbumImage(Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 257
    return-void
.end method

.method public updateFolderItem(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/4 v3, 0x0

    .line 273
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v1, 0x3

    shl-int/lit8 v2, p1, 0x10

    invoke-virtual {v0, v1, v2, v3, v3}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 275
    :cond_0
    return-void
.end method

.method public updateNewAlbumImage(Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v2, 0x0

    .line 266
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbum:Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    invoke-virtual {v0, p2, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->updateBitmapFromAdapter(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;)V

    .line 267
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 270
    :cond_0
    return-void
.end method
