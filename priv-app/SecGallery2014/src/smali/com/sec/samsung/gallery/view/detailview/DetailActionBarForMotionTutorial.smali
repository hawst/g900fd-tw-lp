.class Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;
.super Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;
.source "DetailActionBarForMotionTutorial.java"


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x1

    .line 16
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 20
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mMainActionBar:Landroid/app/ActionBar;

    const v1, 0x7f020311

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setIcon(I)V

    .line 21
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->setTitle()V

    .line 22
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method private setTitle()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial$1;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 79
    return-void
.end method


# virtual methods
.method protected initActionBarBg(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 51
    return-void
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 26
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 27
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f120043

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 29
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->setCameraButtonVisibility(Landroid/view/Menu;Z)V

    .line 30
    return-void
.end method

.method protected onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 1
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 42
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 47
    :goto_0
    return-void

    .line 44
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryMotion()Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->showMotionTutorialInfoDialog()V

    goto :goto_0

    .line 42
    :pswitch_data_0
    .packed-switch 0x7f0f02dd
        :pswitch_0
    .end packed-switch
.end method

.method protected onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 34
    return-void
.end method

.method protected setupButtons()V
    .locals 0

    .prologue
    .line 38
    return-void
.end method
