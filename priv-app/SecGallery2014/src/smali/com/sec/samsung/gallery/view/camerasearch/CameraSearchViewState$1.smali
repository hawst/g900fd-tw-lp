.class Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$1;
.super Ljava/lang/Object;
.source "CameraSearchViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/app/LoadingListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$1;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadingFinished(Z)V
    .locals 1
    .param p1, "loadingFailed"    # Z

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$1;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$400(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSuggestionAdapter()V

    .line 111
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$1;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$100(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$1;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$100(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->closeProgressDialog()V

    .line 113
    :cond_0
    return-void
.end method

.method public onLoadingStarted()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 99
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$1;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mShowProgressDialog:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$000(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    :goto_0
    return-void

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$1;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # setter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mShowProgressDialog:Z
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$002(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;Z)Z

    .line 103
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$1;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    new-instance v1, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-direct {v1}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;-><init>()V

    # setter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$102(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;)Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$1;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$100(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$1;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$200(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$1;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$300(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    const v5, 0x7f0e0032

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->showProgressDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0
.end method
