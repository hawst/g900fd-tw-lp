.class Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$2;
.super Ljava/lang/Object;
.source "ShowCopyMoveDialogCmd.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->handleSingleFileOperation(Lcom/sec/android/gallery3d/ui/SelectionManager;Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

.field final synthetic val$dstAlbumPath:Ljava/lang/String;

.field final synthetic val$dstFile:Ljava/io/File;

.field final synthetic val$operationId:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Ljava/lang/String;ILjava/io/File;)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    iput-object p2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$2;->val$dstAlbumPath:Ljava/lang/String;

    iput p3, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$2;->val$operationId:I

    iput-object p4, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$2;->val$dstFile:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dlg"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$2;->val$dstAlbumPath:Ljava/lang/String;

    iget v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$2;->val$operationId:I

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$2;->val$dstFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->handleMultipleFileOperation(Ljava/lang/String;IILjava/lang/String;)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Ljava/lang/String;IILjava/lang/String;)V

    .line 299
    return-void
.end method
