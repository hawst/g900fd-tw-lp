.class public Lcom/sec/samsung/gallery/glview/GlScrollBar;
.super Lcom/sec/samsung/gallery/glview/GlBaseObject;
.source "GlScrollBar.java"


# static fields
.field public static THRESHOLD:F


# instance fields
.field protected mBottom:F

.field private mCanvasColor:I

.field protected mDefZ:F

.field protected mHeightSpace:F

.field private mMaxHeight:F

.field private mMinHeight:F

.field private mOffsetX:F

.field private mOffsetY:F

.field private mOffsetZ:F

.field private mPaddingRight:F

.field private mThinkness:F

.field protected mTop:F

.field protected mWidthSpace:F

.field private mXRatio:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/high16 v0, 0x40000000    # 2.0f

    sput v0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->THRESHOLD:F

    return-void
.end method

.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V
    .locals 1
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "width"    # F
    .param p3, "height"    # F

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 13
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mXRatio:F

    .line 14
    const/high16 v0, -0x3bb80000    # -800.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mDefZ:F

    .line 16
    const/high16 v0, 0x43480000    # 200.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mMaxHeight:F

    .line 17
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mMinHeight:F

    .line 18
    const v0, -0xc5c5c6

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mCanvasColor:I

    .line 25
    return-void
.end method

.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFI)V
    .locals 6
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "width"    # F
    .param p3, "height"    # F
    .param p4, "textureWidth"    # I

    .prologue
    .line 28
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlScrollBar;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V

    .line 29
    return-void
.end method

.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V
    .locals 4
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "width"    # F
    .param p3, "height"    # F
    .param p4, "textureWidth"    # I
    .param p5, "textureHeight"    # I

    .prologue
    const/4 v3, 0x0

    .line 32
    invoke-direct/range {p0 .. p5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V

    .line 13
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mXRatio:F

    .line 14
    const/high16 v0, -0x3bb80000    # -800.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mDefZ:F

    .line 16
    const/high16 v0, 0x43480000    # 200.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mMaxHeight:F

    .line 17
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mMinHeight:F

    .line 18
    const v0, -0xc5c5c6

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mCanvasColor:I

    .line 33
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlScrollBar$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlScrollBar$1;-><init>(Lcom/sec/samsung/gallery/glview/GlScrollBar;)V

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 39
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidthSpace:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mWidthSpace:F

    .line 40
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeightSpace:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mHeightSpace:F

    .line 41
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mHeightSpace:F

    const/high16 v1, 0x41a00000    # 20.0f

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mMaxHeight:F

    .line 42
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mWidthSpace:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mWidth:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mPaddingRight:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mDefZ:F

    const/high16 v2, 0x40a00000    # 5.0f

    add-float/2addr v1, v2

    invoke-virtual {p0, v0, v3, v1}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->setPos(FFF)V

    .line 43
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->setAlpha(F)V

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/glview/GlScrollBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlScrollBar;

    .prologue
    .line 7
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mCanvasColor:I

    return v0
.end method

.method private getHeight(FF)F
    .locals 7
    .param p1, "min"    # F
    .param p2, "max"    # F

    .prologue
    const/4 v4, 0x0

    .line 124
    sub-float v3, p2, p1

    .line 125
    .local v3, "range":F
    sget v5, Lcom/sec/samsung/gallery/glview/GlScrollBar;->THRESHOLD:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mHeightSpace:F

    mul-float v0, v5, v6

    .line 126
    .local v0, "T":F
    cmpl-float v5, v3, v0

    if-lez v5, :cond_1

    .line 127
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mMinHeight:F

    .line 132
    :cond_0
    :goto_0
    return v4

    .line 130
    :cond_1
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mMinHeight:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mMaxHeight:F

    sub-float/2addr v5, v6

    div-float v1, v5, v0

    .line 131
    .local v1, "a":F
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mMaxHeight:F

    .line 132
    .local v2, "b":F
    cmpl-float v5, v3, v4

    if-lez v5, :cond_0

    mul-float v4, v1, v3

    add-float/2addr v4, v2

    goto :goto_0
.end method


# virtual methods
.method public reset(FFF)V
    .locals 1
    .param p1, "width"    # F
    .param p2, "height"    # F
    .param p3, "top"    # F

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->reset(FFFF)V

    .line 59
    return-void
.end method

.method public reset(FFFF)V
    .locals 2
    .param p1, "width"    # F
    .param p2, "height"    # F
    .param p3, "top"    # F
    .param p4, "bottom"    # F

    .prologue
    .line 62
    iput p3, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mTop:F

    .line 63
    iput p4, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mBottom:F

    .line 64
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mWidthSpace:F

    .line 65
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mHeightSpace:F

    .line 66
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mHeightSpace:F

    const/high16 v1, 0x41a00000    # 20.0f

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mMaxHeight:F

    .line 67
    return-void
.end method

.method public setCanvasColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mCanvasColor:I

    if-eq v0, p1, :cond_0

    .line 48
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mCanvasColor:I

    .line 49
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->invalidate()V

    .line 51
    :cond_0
    return-void
.end method

.method public setDefZ(F)V
    .locals 0
    .param p1, "z"    # F

    .prologue
    .line 74
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mDefZ:F

    .line 75
    return-void
.end method

.method public setMaxHeight(F)V
    .locals 0
    .param p1, "maxHeight"    # F

    .prologue
    .line 110
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mMaxHeight:F

    .line 111
    return-void
.end method

.method public setMinHeight(F)V
    .locals 0
    .param p1, "minHeight"    # F

    .prologue
    .line 114
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mMinHeight:F

    .line 115
    return-void
.end method

.method public setOffset(FFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 118
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mOffsetX:F

    .line 119
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mOffsetY:F

    .line 120
    iput p3, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mOffsetZ:F

    .line 121
    return-void
.end method

.method public setPosXRatio(F)V
    .locals 0
    .param p1, "r"    # F

    .prologue
    .line 70
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mXRatio:F

    .line 71
    return-void
.end method

.method public setRightPadding(F)V
    .locals 0
    .param p1, "padding"    # F

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mPaddingRight:F

    .line 79
    return-void
.end method

.method public setThinkness(F)V
    .locals 0
    .param p1, "thinkness"    # F

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mThinkness:F

    .line 55
    return-void
.end method

.method public update(FFF)V
    .locals 10
    .param p1, "scroll"    # F
    .param p2, "min"    # F
    .param p3, "max"    # F

    .prologue
    const/4 v9, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    .line 82
    sub-float v4, p3, p2

    .line 83
    .local v4, "range":F
    invoke-direct {p0, p2, p3}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->getHeight(FF)F

    move-result v3

    .line 84
    .local v3, "height":F
    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mThinkness:F

    cmpl-float v6, v6, v9

    if-lez v6, :cond_0

    .line 85
    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mThinkness:F

    invoke-virtual {p0, v6, v3}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->setSize(FF)V

    .line 90
    :goto_0
    cmpg-float v6, v4, v9

    if-gtz v6, :cond_2

    .line 107
    :goto_1
    return-void

    .line 87
    :cond_0
    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mWidthSpace:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mHeightSpace:F

    cmpl-float v6, v6, v7

    if-lez v6, :cond_1

    const v6, 0x40accccd    # 5.4f

    :goto_2
    invoke-virtual {p0, v6, v3}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->setSize(FF)V

    goto :goto_0

    :cond_1
    const v6, 0x400ccccd    # 2.2f

    goto :goto_2

    .line 101
    :cond_2
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mHeightSpace:F

    .line 102
    .local v0, "H":F
    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mTop:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mBottom:F

    add-float/2addr v6, v7

    add-float/2addr v6, v3

    sub-float/2addr v6, v0

    div-float v1, v6, v4

    .line 103
    .local v1, "a":F
    neg-float v6, v0

    div-float/2addr v6, v8

    div-float v7, v3, v8

    add-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mBottom:F

    add-float/2addr v6, v7

    mul-float v7, p3, v1

    sub-float v2, v6, v7

    .line 104
    .local v2, "b":F
    mul-float v6, v1, p1

    add-float v5, v6, v2

    .line 105
    .local v5, "y":F
    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mWidthSpace:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mXRatio:F

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mWidthSpace:F

    div-float/2addr v7, v8

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mWidth:F

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mPaddingRight:F

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mOffsetX:F

    add-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mOffsetY:F

    add-float/2addr v7, v5

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mDefZ:F

    const/high16 v9, 0x3f800000    # 1.0f

    sub-float/2addr v8, v9

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mOffsetZ:F

    add-float/2addr v8, v9

    invoke-virtual {p0, v6, v7, v8}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->setPos(FFF)V

    goto :goto_1
.end method
