.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$3;
.super Lorg/puremvc/java/patterns/mediator/Mediator;
.source "PhotoViewComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/Object;

    .prologue
    .line 643
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$3;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-direct {p0, p2, p3}, Lorg/puremvc/java/patterns/mediator/Mediator;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public handleNotification(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 7
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v5, 0x0

    .line 653
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getName()Ljava/lang/String;

    move-result-object v1

    .line 654
    .local v1, "notiName":Ljava/lang/String;
    const-string v4, "EXIT_SELECTION_MODE"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 655
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 656
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$3;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    # setter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumPath:Ljava/lang/String;
    invoke-static {v6, v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$202(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Ljava/lang/String;)Ljava/lang/String;

    .line 658
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$3;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 659
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$3;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 661
    .local v0, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-nez v4, :cond_2

    .line 662
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    .line 664
    .local v2, "params":Ljava/lang/Object;
    if-eqz v2, :cond_1

    instance-of v4, v2, Ljava/lang/Integer;

    if-nez v4, :cond_3

    .line 665
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$3;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->exitSelectionMode()V

    .line 678
    .end local v0    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    .end local v2    # "params":Ljava/lang/Object;
    :cond_2
    :goto_0
    return-void

    .line 667
    .restart local v0    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    .restart local v2    # "params":Ljava/lang/Object;
    :cond_3
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "params":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 668
    .local v3, "result":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$3;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    const/16 v4, 0x10

    if-ge v3, v4, :cond_4

    const/4 v4, 0x1

    :goto_1
    invoke-virtual {v6, v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->exitSelectionMode(Z)V

    goto :goto_0

    :cond_4
    move v4, v5

    goto :goto_1

    .line 672
    .end local v0    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    .end local v3    # "result":I
    :cond_5
    const-string v4, "EXIT_NEW_ALBUM_MODE"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 673
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object v2, v4

    check-cast v2, [Ljava/lang/Object;

    .line 674
    .local v2, "params":[Ljava/lang/Object;
    aget-object v4, v2, v5

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 675
    .local v3, "result":Z
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$3;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    if-eqz v3, :cond_6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$3;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumPath:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "/"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_2
    # setter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumPath:Ljava/lang/String;
    invoke-static {v5, v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$202(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Ljava/lang/String;)Ljava/lang/String;

    .line 676
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$3;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->exitSelectionMode()V

    goto :goto_0

    .line 675
    :cond_6
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public listNotificationInterests()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 646
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "EXIT_SELECTION_MODE"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "EXIT_NEW_ALBUM_MODE"

    aput-object v2, v0, v1

    return-object v0
.end method
