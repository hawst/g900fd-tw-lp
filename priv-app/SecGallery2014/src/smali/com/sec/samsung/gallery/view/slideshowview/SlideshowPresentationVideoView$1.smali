.class Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;
.super Ljava/lang/Object;
.source "SlideshowPresentationVideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 4
    .param p1, "arg0"    # Landroid/media/MediaPlayer;

    .prologue
    .line 84
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onPrepared()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 86
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mHolder:Landroid/view/SurfaceHolder;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$200(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 87
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->gainAudioFocus()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)V

    .line 88
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 89
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v2

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mVideoHeight:I
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$402(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;I)I

    .line 90
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v2

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mVideoWidth:I
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$502(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;I)I

    .line 92
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mHolder:Landroid/view/SurfaceHolder;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$200(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/view/SurfaceHolder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mVideoWidth:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$500(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mVideoHeight:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$400(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)I

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 94
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1$1;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;)V

    const-string v2, "SlideshowDecoding"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 110
    .local v0, "decodingThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 112
    return-void
.end method
