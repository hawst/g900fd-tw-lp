.class public Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;
.super Ljava/lang/Object;
.source "GlComposeBaseAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AdapterInterface"
.end annotation


# instance fields
.field public mBitmap:Landroid/graphics/Bitmap;

.field public mCropRect:Landroid/graphics/Rect;

.field public mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

.field public mDisabled:Z

.field public mDispCheckBox:Z

.field public mDispExpansionIcon:Z

.field public mDispSelectCnt:Z

.field public mDynamicCropRect:Landroid/graphics/Rect;

.field public mIsBroken:I

.field public mObjHeight:F

.field public mObjWidth:F

.field public mReorderEnable:Z

.field public mRotation:I

.field public mSelected:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
