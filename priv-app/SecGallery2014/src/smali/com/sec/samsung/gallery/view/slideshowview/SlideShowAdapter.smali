.class public Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;
.super Lcom/sec/samsung/gallery/glview/GlBaseAdapter;
.source "SlideShowAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$ReverseDateTakenComparator;
    }
.end annotation


# static fields
.field private static final DISPLAY_BUFFER_SIZE:I = 0x4

.field protected static final GETBITMAP_TIME_LIMIT_MS:I = 0x1388

.field private static final INVALID_INDEX:I = -0x1

.field protected static final ROTATION_0:I = 0x0

.field protected static final ROTATION_180:I = 0x2

.field protected static final ROTATION_270:I = 0x3

.field protected static final ROTATION_90:I = 0x1

.field private static final SECIMAGING_FILTER_GREY:Ljava/lang/String; = "com.sec.android.filter,libgreyscale.so"

.field private static final SECIMAGING_FILTER_SEPIA:Ljava/lang/String; = "com.sec.android.filter,libsepia.so"

.field private static final SECIMAGING_FILTER_VINTAGE:Ljava/lang/String; = "com.sec.android.filter,libvintage.so"

.field private static final SLIDESHOW_FILTER_GREY:I = 0x0

.field private static final SLIDESHOW_FILTER_SEPIA:I = 0x2

.field private static final SLIDESHOW_FILTER_SIMPLE:I = 0x1

.field private static final SLIDESHOW_FILTER_VINTAGE:I = 0x3

.field private static TAG:Ljava/lang/String;


# instance fields
.field private filter_bitmap:Landroid/graphics/Bitmap;

.field private mAlbumCount:I

.field private mAlbumStartIndexArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected mBurstPlaySpeed:I

.field private mContext:Landroid/content/Context;

.field private mDisplayItems:[Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;

.field private mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

.field protected mHeight:I

.field private mIsBurstPlay:Z

.field mIsGallerySplitWindow:Z

.field private mIsNoSlectionMode:Z

.field mIsResourceExist:Z

.field private mItemIndex:[I

.field private final mLock:Ljava/lang/Object;

.field private mMediaItemArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private mMediaItemCount:I

.field protected mMediaItemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;"
        }
    .end annotation
.end field

.field private mMediaItemStart:I

.field mNeedResizeOnResume:Z

.field protected mPrevHeight:I

.field protected mPrevRotation:I

.field protected mPrevWidth:I

.field protected mRotation:I

.field private mSecImaging:Lcom/samsung/android/secimaging/SecImaging;

.field private mSlideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field mUseDataManager:Z

.field private mWaitDuration:I

.field private mWideMode:Z

.field protected mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 137
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;-><init>()V

    .line 83
    iput v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mRotation:I

    .line 85
    iput v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mPrevRotation:I

    .line 87
    iput v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mWidth:I

    .line 89
    iput v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mHeight:I

    .line 91
    iput v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mPrevWidth:I

    .line 93
    iput v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mPrevHeight:I

    .line 95
    iput v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mWaitDuration:I

    .line 99
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    .line 101
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mNeedResizeOnResume:Z

    .line 103
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mUseDataManager:Z

    .line 105
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mIsResourceExist:Z

    .line 107
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mIsGallerySplitWindow:Z

    .line 113
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemList:Ljava/util/List;

    .line 115
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemArray:Ljava/util/ArrayList;

    .line 118
    iput v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mAlbumCount:I

    .line 121
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mLock:Ljava/lang/Object;

    .line 123
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSecImaging:Lcom/samsung/android/secimaging/SecImaging;

    .line 127
    iput v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemCount:I

    .line 129
    iput v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemStart:I

    .line 133
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mBurstPlaySpeed:I

    .line 135
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mWideMode:Z

    .line 253
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mIsNoSlectionMode:Z

    .line 138
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mContext:Landroid/content/Context;

    .line 139
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;-><init>(Landroid/content/Context;)V

    .line 143
    invoke-direct {p0, p2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->initialize(Landroid/os/Bundle;)V

    .line 144
    return-void
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->filter_bitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;)Lcom/samsung/android/secimaging/SecImaging;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSecImaging:Lcom/samsung/android/secimaging/SecImaging;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method private checkScreenOrientation()Z
    .locals 6

    .prologue
    .line 723
    const/4 v4, 0x0

    .line 724
    .local v4, "wideMode":Z
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mContext:Landroid/content/Context;

    check-cast v5, Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Display;->getRotation()I

    move-result v3

    .line 726
    .local v3, "orientation":I
    const/4 v5, 0x1

    if-eq v3, v5, :cond_0

    const/4 v5, 0x3

    if-ne v3, v5, :cond_1

    .line 727
    :cond_0
    const/4 v4, 0x1

    .line 728
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getLcdRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v1

    .line 729
    .local v1, "LCDSize":Landroid/graphics/Rect;
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 730
    .local v0, "LCDHeight":I
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 731
    .local v2, "LCDWidth":I
    iput v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mWidth:I

    .line 732
    iput v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mHeight:I

    .line 741
    :goto_0
    return v4

    .line 734
    .end local v0    # "LCDHeight":I
    .end local v1    # "LCDSize":Landroid/graphics/Rect;
    .end local v2    # "LCDWidth":I
    :cond_1
    const/4 v4, 0x0

    .line 735
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getLcdRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v1

    .line 736
    .restart local v1    # "LCDSize":Landroid/graphics/Rect;
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 737
    .restart local v0    # "LCDHeight":I
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 738
    .restart local v2    # "LCDWidth":I
    iput v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mWidth:I

    .line 739
    iput v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mHeight:I

    goto :goto_0
.end method

.method private createFilterBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "filter"    # I

    .prologue
    .line 473
    if-nez p1, :cond_0

    .line 474
    const/4 v0, 0x0

    .line 530
    :goto_0
    return-object v0

    .line 476
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;Landroid/graphics/Bitmap;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 530
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->filter_bitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private destroyResources()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 412
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mItemIndex:[I

    if-eqz v2, :cond_0

    .line 413
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mItemIndex:[I

    .line 415
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mDisplayItems:[Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;

    if-nez v2, :cond_1

    .line 430
    :goto_0
    return-void

    .line 418
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mDisplayItems:[Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;

    monitor-enter v3

    .line 419
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v2, 0x4

    if-ge v0, v2, :cond_3

    .line 420
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mDisplayItems:[Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;

    aget-object v1, v2, v0

    .line 421
    .local v1, "imageItem":Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;
    if-nez v1, :cond_2

    .line 419
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 424
    :cond_2
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->cancelImageRequest()V

    .line 425
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->recycle()V

    .line 426
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mDisplayItems:[Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;

    const/4 v4, 0x0

    aput-object v4, v2, v0

    goto :goto_2

    .line 429
    .end local v1    # "imageItem":Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 428
    :cond_3
    const/4 v2, 0x0

    :try_start_1
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mDisplayItems:[Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;

    .line 429
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private drawVideoPlayIcon(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 11
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 701
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f020180

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 704
    .local v3, "oriVideoPlayBitmap":Landroid/graphics/Bitmap;
    if-eqz p1, :cond_0

    if-nez v3, :cond_2

    .line 705
    :cond_0
    const/4 p1, 0x0

    .line 719
    .end local p1    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    :goto_0
    return-object p1

    .line 707
    .restart local p1    # "bitmap":Landroid/graphics/Bitmap;
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 708
    .local v2, "imgWidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 709
    .local v1, "imgHeight":I
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 710
    .local v5, "playIconWidth":I
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 712
    .local v4, "playIconHeight":I
    if-eqz v3, :cond_1

    .line 713
    sub-int v8, v2, v5

    div-int/lit8 v6, v8, 0x2

    .line 714
    .local v6, "x":I
    sub-int v8, v1, v4

    div-int/lit8 v7, v8, 0x2

    .line 715
    .local v7, "y":I
    new-instance v0, Landroid/graphics/Canvas;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v8

    if-eqz v8, :cond_3

    move-object v8, p1

    :goto_1
    invoke-direct {v0, v8}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 717
    .local v0, "canvas":Landroid/graphics/Canvas;
    int-to-float v8, v6

    int-to-float v9, v7

    new-instance v10, Landroid/graphics/Paint;

    invoke-direct {v10}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v0, v3, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 715
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    :cond_3
    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v9, 0x1

    invoke-virtual {p1, v8, v9}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v8

    goto :goto_1
.end method

.method private getBitmap(IJ)Landroid/graphics/Bitmap;
    .locals 20
    .param p1, "index"    # I
    .param p2, "time"    # J

    .prologue
    .line 542
    const/4 v2, 0x0

    .line 543
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    const/4 v12, 0x0

    .line 544
    .local v12, "repCnt":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v14

    .line 545
    .local v14, "setting":Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v16

    sub-long v4, v16, p2

    .line 547
    .local v4, "elapsedTime":J
    const-wide/16 v16, 0x1388

    cmp-long v16, v4, v16

    if-lez v16, :cond_0

    .line 548
    sget-object v16, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo v17, "time limit exceeded! finish!"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    const v17, 0x7f0e0246

    invoke-static/range {v16 .. v17}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 550
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    check-cast v16, Landroid/app/Activity;

    invoke-virtual/range {v16 .. v16}, Landroid/app/Activity;->finish()V

    .line 551
    const/16 v16, 0x0

    .line 631
    :goto_0
    return-object v16

    .line 554
    :cond_0
    if-ltz p1, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemCount:I

    move/from16 v16, v0

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, p1

    move/from16 v1, v16

    if-le v0, v1, :cond_2

    .line 555
    :cond_1
    sget-object v16, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "wrong index! "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    const/16 v16, 0x0

    goto :goto_0

    .line 559
    :cond_2
    invoke-direct/range {p0 .. p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->loadBitmapResource(I)V

    .line 562
    :try_start_0
    invoke-direct/range {p0 .. p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getSlideItem(I)Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;

    move-result-object v15

    .line 563
    .local v15, "slideItem":Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;
    if-eqz v15, :cond_3

    .line 564
    invoke-virtual {v15}, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 566
    :cond_3
    if-eqz v15, :cond_4

    if-nez v2, :cond_4

    invoke-virtual {v15}, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->isRequestInProgress()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v16

    if-eqz v16, :cond_4

    .line 567
    const/16 v16, 0x0

    goto :goto_0

    .line 569
    .end local v15    # "slideItem":Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;
    :catch_0
    move-exception v3

    .line 570
    .local v3, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v3}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 572
    .end local v3    # "e":Ljava/lang/NullPointerException;
    :cond_4
    sget-object v16, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "getBitmap index = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ", bitmap = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ", wait = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    const/4 v9, 0x0

    .line 575
    .local v9, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mIsNoSlectionMode:Z

    move/from16 v16, v0

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_b

    .line 576
    invoke-direct/range {p0 .. p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getItemNoSelection(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v9

    .line 585
    :cond_5
    :goto_1
    const/4 v8, 0x0

    .line 586
    .local v8, "isBroken":Z
    if-eqz v9, :cond_6

    if-nez v2, :cond_6

    .line 587
    const/4 v8, 0x1

    .line 588
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->isVideo(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v16

    if-eqz v16, :cond_d

    .line 589
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0200a8

    invoke-virtual/range {v16 .. v18}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 598
    :cond_6
    :goto_2
    const/16 v16, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    .line 599
    instance-of v0, v9, Lcom/sec/android/gallery3d/data/LocalImage;

    move/from16 v16, v0

    if-eqz v16, :cond_7

    if-eqz v9, :cond_7

    .line 600
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->makeFacesInfo(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 603
    :cond_7
    if-nez v8, :cond_8

    .line 605
    if-eqz v9, :cond_8

    .line 606
    :try_start_1
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v2, v0, v1}, Lcom/sec/android/gallery3d/common/BitmapUtils;->rotateBitmap(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v2

    .line 615
    :cond_8
    :goto_3
    if-eqz v9, :cond_a

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->isVideo(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v16

    if-eqz v16, :cond_a

    .line 616
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    .line 617
    .local v7, "imageWidth":I
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 618
    .local v6, "imageHeight":I
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v6}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getScaleValue(II)F

    move-result v13

    .line 620
    .local v13, "scaleValue":F
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v13}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getResizedWidth(IF)I

    move-result v11

    .line 621
    .local v11, "reSizeWidth":I
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v13}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getResizedHeight(IF)I

    move-result v10

    .line 623
    .local v10, "reSizeHeight":I
    if-eq v11, v7, :cond_9

    if-eq v10, v6, :cond_9

    .line 624
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v11, v10}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getScaledBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 626
    :cond_9
    invoke-static {v9}, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->isPicasaImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v16

    if-nez v16, :cond_a

    invoke-virtual {v14}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getVideoPlayEnable()Z

    move-result v16

    if-nez v16, :cond_a

    .line 627
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->drawVideoPlayIcon(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    .end local v6    # "imageHeight":I
    .end local v7    # "imageWidth":I
    .end local v10    # "reSizeHeight":I
    .end local v11    # "reSizeWidth":I
    .end local v13    # "scaleValue":F
    :cond_a
    move-object/from16 v16, v2

    .line 631
    goto/16 :goto_0

    .line 578
    .end local v8    # "isBroken":Z
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemList:Ljava/util/List;

    move-object/from16 v16, v0

    if-eqz v16, :cond_c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemCount:I

    move/from16 v16, v0

    rem-int v16, p1, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemList:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_c

    .line 579
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemList:Ljava/util/List;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemCount:I

    move/from16 v17, v0

    rem-int v17, p1, v17

    invoke-interface/range {v16 .. v17}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    check-cast v9, Lcom/sec/android/gallery3d/data/MediaItem;

    .restart local v9    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    goto/16 :goto_1

    .line 580
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemArray:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    if-eqz v16, :cond_5

    .line 581
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemArray:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    check-cast v9, Lcom/sec/android/gallery3d/data/MediaItem;

    .restart local v9    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    goto/16 :goto_1

    .line 592
    .restart local v8    # "isBroken":Z
    :cond_d
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0200a7

    invoke-virtual/range {v16 .. v18}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    goto/16 :goto_2

    .line 608
    :catch_1
    move-exception v3

    .line 609
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_3

    .line 610
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v3

    .line 611
    .local v3, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v3}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto/16 :goto_3
.end method

.method private getItemNoSelection(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 10
    .param p1, "reqIndex"    # I

    .prologue
    const/4 v8, 0x1

    .line 838
    const/4 v5, 0x0

    .line 840
    .local v5, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :try_start_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v7, :cond_4

    iget v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mAlbumCount:I

    if-lez v7, :cond_4

    .line 841
    const/4 v1, 0x0

    .line 842
    .local v1, "albumIndex":I
    const/4 v6, 0x0

    .line 843
    .local v6, "modIndex":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mAlbumStartIndexArray:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 844
    .local v4, "index":I
    if-le v4, p1, :cond_1

    .line 849
    .end local v4    # "index":I
    :cond_0
    iget v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mAlbumCount:I

    if-ne v7, v8, :cond_3

    .line 850
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v7, v7, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v7, :cond_2

    .line 851
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v8, 0x1

    invoke-virtual {v7, v6, v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    move-object v5, v0

    .line 864
    .end local v1    # "albumIndex":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v6    # "modIndex":I
    :goto_1
    return-object v5

    .line 846
    .restart local v1    # "albumIndex":I
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "index":I
    .restart local v6    # "modIndex":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 847
    sub-int v6, p1, v4

    .line 848
    goto :goto_0

    .line 853
    .end local v4    # "index":I
    :cond_2
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v6, v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    move-object v5, v0

    goto :goto_1

    .line 856
    :cond_3
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    add-int/lit8 v8, v1, -0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v6, v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    move-object v5, v0

    goto :goto_1

    .line 859
    .end local v1    # "albumIndex":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v6    # "modIndex":I
    :cond_4
    sget-object v7, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "requestBitmapResource: mSourceMediaSet is null on index = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 861
    :catch_0
    move-exception v2

    .line 862
    .local v2, "e":Ljava/lang/IndexOutOfBoundsException;
    sget-object v7, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    const-string v8, "IndexOutOfBoundsException at getItemNoSelection"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private getResizedHeight(IF)I
    .locals 2
    .param p1, "imageHeight"    # I
    .param p2, "scaleValue"    # F

    .prologue
    .line 765
    int-to-float v1, p1

    mul-float/2addr v1, p2

    float-to-int v0, v1

    .line 766
    .local v0, "reSizeHeight":I
    rem-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_0

    .line 767
    add-int/lit8 v0, v0, -0x1

    .line 768
    rem-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_0

    .line 769
    add-int/lit8 v0, v0, -0x2

    .line 772
    :cond_0
    return v0
.end method

.method private getResizedWidth(IF)I
    .locals 2
    .param p1, "imageWidth"    # I
    .param p2, "scaleValue"    # F

    .prologue
    .line 754
    int-to-float v1, p1

    mul-float/2addr v1, p2

    float-to-int v0, v1

    .line 755
    .local v0, "reSizeWidth":I
    rem-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_0

    .line 756
    add-int/lit8 v0, v0, -0x1

    .line 757
    rem-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_0

    .line 758
    add-int/lit8 v0, v0, -0x2

    .line 761
    :cond_0
    return v0
.end method

.method private getScaleValue(II)F
    .locals 3
    .param p1, "imageWidth"    # I
    .param p2, "imageHeight"    # I

    .prologue
    .line 745
    const/high16 v0, 0x3f800000    # 1.0f

    .line 746
    .local v0, "scaleValue":F
    iget v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mWidth:I

    int-to-float v1, v1

    int-to-float v2, p1

    div-float v0, v1, v2

    .line 747
    iget v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mHeight:I

    int-to-float v1, v1

    int-to-float v2, p2

    div-float/2addr v1, v2

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 748
    iget v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mHeight:I

    int-to-float v1, v1

    int-to-float v2, p2

    div-float v0, v1, v2

    .line 750
    :cond_0
    return v0
.end method

.method private getScaledBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "reSizeWidth"    # I
    .param p3, "reSizeHeight"    # I

    .prologue
    .line 776
    if-lez p2, :cond_0

    if-gtz p3, :cond_1

    .line 777
    :cond_0
    sget-object v4, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "size value to resize are wrong! "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " x "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p1

    .line 797
    .end local p1    # "bitmap":Landroid/graphics/Bitmap;
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v0

    .line 781
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local p1    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    if-eqz p1, :cond_2

    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    if-nez v4, :cond_2

    .line 782
    const/4 v3, 0x0

    .line 784
    .local v3, "scaledBitmap":Landroid/graphics/Bitmap;
    const/4 v4, 0x1

    :try_start_1
    invoke-static {p1, p2, p3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 789
    :goto_1
    if-eqz v3, :cond_2

    .line 790
    :try_start_2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 791
    move-object p1, v3

    .end local v3    # "scaledBitmap":Landroid/graphics/Bitmap;
    :cond_2
    :goto_2
    move-object v0, p1

    .line 797
    .end local p1    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 785
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v3    # "scaledBitmap":Landroid/graphics/Bitmap;
    .restart local p1    # "bitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v2

    .line 786
    .local v2, "ome":Ljava/lang/OutOfMemoryError;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 787
    invoke-virtual {v2}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 794
    .end local v2    # "ome":Ljava/lang/OutOfMemoryError;
    .end local v3    # "scaledBitmap":Landroid/graphics/Bitmap;
    :catch_1
    move-exception v1

    .line 795
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method private getSlideItem(I)Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 342
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mDisplayItems:[Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mItemIndex:[I

    aget v3, v3, p1

    aget-object v1, v2, v3

    .line 343
    .local v1, "imgItem":Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->getIndex()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-ne v2, p1, :cond_0

    .line 349
    .end local v1    # "imgItem":Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;
    :goto_0
    return-object v1

    .line 346
    :catch_0
    move-exception v0

    .line 347
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 349
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private initialize(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "extra"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 256
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    .line 257
    .local v1, "app":Lcom/sec/android/gallery3d/app/GalleryAppImpl;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v2

    .line 258
    .local v2, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getSlideShowSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSlideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 260
    iput-boolean v10, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mIsBurstPlay:Z

    .line 261
    iput-object v11, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemList:Ljava/util/List;

    .line 262
    iput-object v11, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemArray:Ljava/util/ArrayList;

    .line 263
    iput-boolean v10, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mIsNoSlectionMode:Z

    .line 264
    iput-object v11, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 267
    :try_start_0
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSlideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v9, :cond_6

    .line 268
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSlideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCloneMediaList()Ljava/util/LinkedList;

    move-result-object v7

    .line 269
    .local v7, "originalList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v9

    if-lez v9, :cond_2

    .line 271
    if-eqz p1, :cond_0

    .line 272
    const-string v9, "burstplay"

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    iput-boolean v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mIsBurstPlay:Z

    .line 273
    const-string v9, "burstplayspeed"

    const/4 v10, 0x2

    invoke-virtual {p1, v9, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v9

    iput v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mBurstPlaySpeed:I

    .line 276
    :cond_0
    iput-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemList:Ljava/util/List;

    .line 281
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSlideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 283
    iget-boolean v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mIsBurstPlay:Z

    if-eqz v9, :cond_1

    .line 284
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemList:Ljava/util/List;

    new-instance v10, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$ReverseDateTakenComparator;

    const/4 v11, 0x0

    invoke-direct {v10, v11}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$ReverseDateTakenComparator;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;)V

    invoke-static {v9, v10}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 287
    :cond_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemList:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    iput v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemCount:I

    .line 288
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSlideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCurrentIndex()I

    move-result v9

    iput v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemStart:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 333
    .end local v7    # "originalList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :goto_0
    const/4 v9, 0x4

    new-array v9, v9, [Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;

    iput-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mDisplayItems:[Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;

    .line 334
    iget v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemCount:I

    new-array v9, v9, [I

    iput-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mItemIndex:[I

    .line 335
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mItemIndex:[I

    const/4 v10, -0x1

    invoke-static {v9, v10}, Ljava/util/Arrays;->fill([II)V

    .line 336
    return-void

    .line 290
    .restart local v7    # "originalList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_2
    const/4 v9, 0x1

    :try_start_1
    iput-boolean v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mIsNoSlectionMode:Z

    .line 291
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemList:Ljava/util/List;

    .line 292
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSlideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSourceMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 293
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v9, :cond_3

    .line 294
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v9

    iput v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mAlbumCount:I

    .line 295
    iget v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mAlbumCount:I

    if-nez v9, :cond_4

    .line 296
    const/4 v9, 0x1

    iput v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mAlbumCount:I

    .line 297
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mAlbumStartIndexArray:Ljava/util/ArrayList;

    .line 298
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mAlbumStartIndexArray:Ljava/util/ArrayList;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 299
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v9

    iput v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemCount:I

    .line 313
    :cond_3
    :goto_1
    const/4 v9, 0x0

    iput v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemStart:I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 329
    .end local v7    # "originalList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :catch_0
    move-exception v3

    .line 330
    .local v3, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v3}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 302
    .end local v3    # "e":Ljava/lang/NullPointerException;
    .restart local v7    # "originalList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_4
    :try_start_2
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mAlbumStartIndexArray:Ljava/util/ArrayList;

    .line 303
    const/4 v8, 0x0

    .line 304
    .local v8, "startIndex":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    iget v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mAlbumCount:I

    if-ge v4, v9, :cond_5

    .line 305
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mAlbumStartIndexArray:Ljava/util/ArrayList;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v4, v10}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 306
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v9, v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 307
    .local v0, "album":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v9

    add-int/2addr v8, v9

    .line 304
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 310
    .end local v0    # "album":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_5
    iput v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemCount:I

    goto :goto_1

    .line 317
    .end local v4    # "i":I
    .end local v7    # "originalList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .end local v8    # "startIndex":I
    :cond_6
    const/4 v6, 0x0

    .line 318
    .local v6, "mediaPath":Ljava/lang/String;
    const/4 v5, 0x0

    .line 320
    .local v5, "index":Ljava/lang/Integer;
    if-eqz p1, :cond_7

    .line 321
    const-string v9, "path"

    const-string v10, "/mnt/"

    invoke-virtual {p1, v9, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 322
    const-string v9, "index"

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 325
    :cond_7
    invoke-virtual {v2, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaItem(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemArray:Ljava/util/ArrayList;

    .line 326
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemArray:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    iput v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemCount:I

    .line 327
    const/4 v9, 0x0

    iput v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemStart:I
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0
.end method

.method private isVideo(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 2
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 697
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadBitmapResource(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/4 v2, 0x4

    .line 398
    iget v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemCount:I

    if-gt v1, v2, :cond_0

    .line 399
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemCount:I

    if-ge v0, v1, :cond_1

    .line 400
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->requestBitmapResource(I)V

    .line 399
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 404
    .end local v0    # "i":I
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    if-ge v0, v2, :cond_1

    .line 405
    add-int v1, p1, v0

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->requestBitmapResource(I)V

    .line 404
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 407
    :cond_1
    return-void
.end method

.method private makeFacesInfo(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 12
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const v10, 0x3f99999a    # 1.2f

    .line 635
    const/4 v1, 0x0

    .local v1, "faces":[Lcom/sec/android/gallery3d/data/Face;
    move-object v8, p1

    .line 637
    check-cast v8, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/LocalImage;->loadFace()[Lcom/sec/android/gallery3d/data/Face;

    move-result-object v1

    .line 639
    if-nez v1, :cond_1

    .line 674
    :cond_0
    return-void

    .line 643
    :cond_1
    array-length v8, v1

    if-lez v8, :cond_0

    .line 644
    array-length v8, v1

    new-array v8, v8, [Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    .line 645
    if-eqz v1, :cond_0

    .line 646
    const/4 v4, 0x0

    .line 647
    .local v4, "imageW":F
    const/4 v3, 0x0

    .line 648
    .local v3, "imageH":F
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v8

    const/16 v9, 0x5a

    if-eq v8, v9, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v8

    const/16 v9, 0x10e

    if-ne v8, v9, :cond_4

    .line 649
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v8

    int-to-float v4, v8

    .line 650
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v8

    int-to-float v3, v8

    .line 655
    :goto_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v8, v1

    if-ge v2, v8, :cond_0

    .line 656
    aget-object v8, v1, v2

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Face;->getFaceRect()Landroid/graphics/Rect;

    move-result-object v0

    .line 657
    .local v0, "faceRect":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    new-instance v9, Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    invoke-direct {v9}, Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;-><init>()V

    aput-object v9, v8, v2

    .line 658
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    aget-object v8, v8, v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v9, v4

    iput v9, v8, Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;->ratioX:F

    .line 659
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v11

    div-float v6, v4, v8

    .line 660
    .local v6, "xScaleValue":F
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v11

    div-float v7, v3, v8

    .line 661
    .local v7, "yScaleValue":F
    cmpg-float v8, v6, v7

    if-gez v8, :cond_5

    move v5, v6

    .line 662
    .local v5, "scaleValue":F
    :goto_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    aget-object v8, v8, v2

    iput v5, v8, Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;->scaleValue:F

    .line 663
    const/high16 v8, 0x40200000    # 2.5f

    cmpg-float v8, v5, v8

    if-gez v8, :cond_6

    .line 664
    cmpg-float v8, v5, v10

    if-gez v8, :cond_3

    .line 665
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    aget-object v8, v8, v2

    iput v10, v8, Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;->scaleValue:F

    .line 667
    :cond_3
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    aget-object v8, v8, v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v9, v3

    iput v9, v8, Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;->ratioY:F

    .line 655
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 652
    .end local v0    # "faceRect":Landroid/graphics/Rect;
    .end local v2    # "i":I
    .end local v5    # "scaleValue":F
    .end local v6    # "xScaleValue":F
    .end local v7    # "yScaleValue":F
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v8

    int-to-float v4, v8

    .line 653
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v8

    int-to-float v3, v8

    goto :goto_0

    .restart local v0    # "faceRect":Landroid/graphics/Rect;
    .restart local v2    # "i":I
    .restart local v6    # "xScaleValue":F
    .restart local v7    # "yScaleValue":F
    :cond_5
    move v5, v7

    .line 661
    goto :goto_2

    .line 669
    .restart local v5    # "scaleValue":F
    :cond_6
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    aget-object v8, v8, v2

    iget v9, v0, Landroid/graphics/Rect;->top:I

    int-to-float v9, v9

    div-float/2addr v9, v3

    iput v9, v8, Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;->ratioY:F

    goto :goto_3
.end method

.method private requestBitmapResource(I)V
    .locals 10
    .param p1, "index"    # I

    .prologue
    .line 353
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mDisplayItems:[Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mItemIndex:[I

    if-nez v5, :cond_2

    .line 354
    :cond_0
    sget-object v5, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    const-string v6, "requestBitmapResource() : mDisplayItems or mItemIndex is null"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    :cond_1
    :goto_0
    return-void

    .line 358
    :cond_2
    iget v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemCount:I

    rem-int v4, p1, v5

    .line 359
    .local v4, "reqIndex":I
    rem-int/lit8 v3, p1, 0x4

    .line 360
    .local v3, "pos":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mDisplayItems:[Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;

    aget-object v1, v5, v3

    .line 361
    .local v1, "imageItem":Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;
    if-eqz v1, :cond_4

    .line 362
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 363
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->getIndex()I

    move-result v5

    if-ne v5, v4, :cond_3

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->isRequestInProgress()Z

    move-result v5

    if-nez v5, :cond_1

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 369
    :cond_3
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->cancelImageRequest()V

    .line 370
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->recycle()V

    .line 374
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_4
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemList:Ljava/util/List;

    if-eqz v5, :cond_5

    .line 375
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemList:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 386
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mItemIndex:[I

    aput v3, v5, v4

    .line 387
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mDisplayItems:[Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;

    monitor-enter v6

    .line 388
    :try_start_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mDisplayItems:[Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;

    new-instance v7, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mContext:Landroid/content/Context;

    iget-boolean v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mIsBurstPlay:Z

    invoke-direct {v7, v8, v2, v4, v9}, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;IZ)V

    aput-object v7, v5, v3

    .line 389
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mDisplayItems:[Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;

    aget-object v5, v5, v3

    if-nez v5, :cond_8

    .line 390
    monitor-exit v6

    goto :goto_0

    .line 393
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 376
    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_5
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemArray:Ljava/util/ArrayList;

    if-eqz v5, :cond_6

    .line 377
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .restart local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    goto :goto_1

    .line 379
    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_6
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mIsNoSlectionMode:Z

    if-eqz v5, :cond_7

    .line 380
    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getItemNoSelection(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .restart local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    goto :goto_1

    .line 382
    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_7
    sget-object v5, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "requestBitmapResource: mMediaItemList is null on index = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 392
    .restart local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_8
    :try_start_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mDisplayItems:[Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;

    aget-object v5, v5, v3

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->requestImage()V

    .line 393
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394
    sget-object v5, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "requestBitmapResource Requested = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", pos = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method


# virtual methods
.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 151
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    const-string v1, "destory"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->destroyResources()V

    .line 154
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSlideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSlideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 156
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSlideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 159
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecImaging:Z

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSecImaging:Lcom/samsung/android/secimaging/SecImaging;

    if-eqz v0, :cond_1

    .line 161
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSecImaging:Lcom/samsung/android/secimaging/SecImaging;

    invoke-virtual {v0}, Lcom/samsung/android/secimaging/SecImaging;->deInitialize()V

    .line 162
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSecImaging:Lcom/samsung/android/secimaging/SecImaging;

    .line 165
    :cond_1
    return-void
.end method

.method public getBurstPlaySpeed()I
    .locals 1

    .prologue
    .line 917
    iget v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mBurstPlaySpeed:I

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 834
    iget v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemCount:I

    return v0
.end method

.method public getFacesInfo()[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;
    .locals 1

    .prologue
    .line 677
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    return-object v0
.end method

.method public getImageCount()I
    .locals 1

    .prologue
    .line 681
    iget v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemCount:I

    return v0
.end method

.method public getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 6
    .param p1, "reqIndex"    # I

    .prologue
    .line 894
    const/4 v2, 0x0

    .line 895
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mIsNoSlectionMode:Z

    if-eqz v3, :cond_0

    .line 896
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getItemNoSelection(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    .line 909
    :goto_0
    return-object v3

    .line 899
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemList:Ljava/util/List;

    if-eqz v3, :cond_1

    .line 900
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    move-object v2, v0

    :goto_1
    move-object v3, v2

    .line 909
    goto :goto_0

    .line 901
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemArray:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    .line 902
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemArray:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    move-object v2, v0

    goto :goto_1

    .line 904
    :cond_2
    sget-object v3, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "requestBitmapResource: mMediaItemList is null on index = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 906
    :catch_0
    move-exception v1

    .line 907
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    sget-object v3, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    const-string v4, "IndexOutOfBoundsException at getItem"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 50
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemImage(II)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "index"    # I
    .param p2, "type"    # I

    .prologue
    .line 435
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v3

    .line 437
    .local v3, "setting":Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    const/4 v6, 0x4

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 440
    .local v1, "filter":I
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 441
    .local v4, "time":J
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->filter_bitmap:Landroid/graphics/Bitmap;

    .line 443
    invoke-direct {p0, p1, v4, v5}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getBitmap(IJ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 445
    .local v2, "original_bitmap":Landroid/graphics/Bitmap;
    const/4 v6, 0x1

    if-eq v1, v6, :cond_0

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mIsBurstPlay:Z

    if-eqz v6, :cond_1

    .line 467
    .end local v2    # "original_bitmap":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object v2

    .line 448
    .restart local v2    # "original_bitmap":Landroid/graphics/Bitmap;
    :cond_1
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecImaging:Z

    if-eqz v6, :cond_0

    .line 449
    if-eqz v2, :cond_2

    .line 450
    invoke-direct {p0, v2, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->createFilterBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    .line 451
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mLock:Ljava/lang/Object;

    monitor-enter v7

    .line 453
    :try_start_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mLock:Ljava/lang/Object;

    const-wide/16 v8, 0x514

    invoke-virtual {v6, v8, v9}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 457
    :goto_1
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->filter_bitmap:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_3

    .line 461
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->filter_bitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 454
    :catch_0
    move-exception v0

    .line 455
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 457
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6

    .line 463
    :cond_3
    sget-object v6, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    const-string v7, "filter_bitmap is null returning original bitmap"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getMediaObject(I)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 689
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 690
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 692
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStartIndex()I
    .locals 1

    .prologue
    .line 685
    iget v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemStart:I

    return v0
.end method

.method public getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 829
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWaitDuration()J
    .locals 2

    .prologue
    .line 913
    iget v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mWaitDuration:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public isDataContainsMTPpath()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 868
    const/4 v2, 0x0

    .line 869
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<+Lcom/sec/android/gallery3d/data/MediaObject;>;"
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemArray:Ljava/util/ArrayList;

    if-eqz v5, :cond_2

    .line 870
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemArray:Ljava/util/ArrayList;

    .line 877
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 878
    .local v3, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v3, :cond_0

    .line 882
    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v3    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    .line 883
    .local v1, "itemPath":Lcom/sec/android/gallery3d/data/Path;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Path;->getPrefix()Ljava/lang/String;

    move-result-object v5

    const-string v6, "mtp"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 886
    const/4 v4, 0x1

    .line 889
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "itemPath":Lcom/sec/android/gallery3d/data/Path;
    :cond_1
    return v4

    .line 871
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemList:Ljava/util/List;

    if-eqz v5, :cond_1

    .line 872
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemList:Ljava/util/List;

    goto :goto_0
.end method

.method public isDataContainsSDcardPath()Z
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 801
    const/4 v2, 0x0

    .line 802
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<+Lcom/sec/android/gallery3d/data/MediaObject;>;"
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemArray:Ljava/util/ArrayList;

    if-eqz v6, :cond_2

    .line 803
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemArray:Ljava/util/ArrayList;

    .line 810
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 811
    .local v3, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v3, :cond_0

    .line 815
    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v3    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 816
    .local v0, "filePath":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardPath(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v4, v5

    .line 824
    .end local v0    # "filePath":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    return v4

    .line 804
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemList:Ljava/util/List;

    if-eqz v6, :cond_1

    .line 805
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mMediaItemList:Ljava/util/List;

    goto :goto_0

    .line 819
    .restart local v0    # "filePath":Ljava/lang/String;
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_3
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v6, :cond_0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v4, v5

    .line 820
    goto :goto_1
.end method

.method public isRequestInProgress(I)Z
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 534
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getSlideItem(I)Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;

    move-result-object v0

    .line 535
    .local v0, "slideItem":Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;
    if-nez v0, :cond_0

    .line 536
    const/4 v1, 0x0

    .line 538
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->isRequestInProgress()Z

    move-result v1

    goto :goto_0
.end method

.method public isServiceRunning(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 222
    const-string v3, "activity"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 225
    .local v1, "manager":Landroid/app/ActivityManager;
    const/16 v3, 0x64

    :try_start_0
    invoke-virtual {v1, v3}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v2

    .line 226
    .local v2, "serviceList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    if-eqz v2, :cond_1

    .line 227
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 228
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningServiceInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    .line 229
    const/4 v3, 0x1

    .line 235
    .end local v0    # "i":I
    .end local v2    # "serviceList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :goto_1
    return v3

    .line 227
    .restart local v0    # "i":I
    .restart local v2    # "serviceList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 233
    .end local v0    # "i":I
    .end local v2    # "serviceList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :catch_0
    move-exception v3

    .line 235
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public onConfigChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 214
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->checkScreenOrientation()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mWideMode:Z

    .line 215
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 169
    sget-object v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    const-string v2, "onPause()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->destroyResources()V

    .line 171
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSlideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v1, :cond_0

    .line 172
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSlideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setCurrentIndex(I)V

    .line 174
    :cond_0
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecImaging:Z

    if-eqz v1, :cond_1

    .line 176
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSecImaging:Lcom/samsung/android/secimaging/SecImaging;

    if-eqz v1, :cond_1

    .line 177
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSecImaging:Lcom/samsung/android/secimaging/SecImaging;

    invoke-virtual {v1}, Lcom/samsung/android/secimaging/SecImaging;->release()V

    .line 178
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSecImaging:Lcom/samsung/android/secimaging/SecImaging;

    invoke-virtual {v1}, Lcom/samsung/android/secimaging/SecImaging;->deInitialize()V

    .line 179
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSecImaging:Lcom/samsung/android/secimaging/SecImaging;
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessError; {:try_start_0 .. :try_end_0} :catch_2

    .line 189
    :cond_1
    :goto_0
    return-void

    .line 181
    :catch_0
    move-exception v0

    .line 182
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    sget-object v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    const-string v2, "UnsatisfiedLinkError : SecImaging"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 183
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v0

    .line 184
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    sget-object v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    const-string v2, "NoClassDefFoundError : SecImaging"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 185
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_2
    move-exception v0

    .line 186
    .local v0, "e":Ljava/lang/IllegalAccessError;
    sget-object v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    const-string v2, "IllegalAccessError : SecImaging"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 193
    sget-object v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    const-string v2, "onResume()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mNeedResizeOnResume:Z

    .line 195
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->loadBitmapResource(I)V

    .line 196
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->checkScreenOrientation()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mWideMode:Z

    .line 197
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecImaging:Z

    if-eqz v1, :cond_0

    .line 199
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSecImaging:Lcom/samsung/android/secimaging/SecImaging;

    if-nez v1, :cond_0

    .line 200
    new-instance v1, Lcom/samsung/android/secimaging/SecImaging;

    invoke-direct {v1}, Lcom/samsung/android/secimaging/SecImaging;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSecImaging:Lcom/samsung/android/secimaging/SecImaging;

    .line 201
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mSecImaging:Lcom/samsung/android/secimaging/SecImaging;

    invoke-virtual {v1}, Lcom/samsung/android/secimaging/SecImaging;->initialize()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessError; {:try_start_0 .. :try_end_0} :catch_2

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 203
    :catch_0
    move-exception v0

    .line 204
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    sget-object v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    const-string v2, "UnsatisfiedLinkError : SecImaging"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 205
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v0

    .line 206
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    sget-object v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    const-string v2, "NoClassDefFoundError : SecImaging"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 207
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_2
    move-exception v0

    .line 208
    .local v0, "e":Ljava/lang/IllegalAccessError;
    sget-object v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->TAG:Ljava/lang/String;

    const-string v2, "IllegalAccessError : SecImaging"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected setGalleryMultiWindowMode(Z)V
    .locals 0
    .param p1, "isSplitWindow"    # Z

    .prologue
    .line 218
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mIsGallerySplitWindow:Z

    .line 219
    return-void
.end method

.method public setSlideShowInfo(III)V
    .locals 0
    .param p1, "waitDuration"    # I
    .param p2, "effectDuration"    # I
    .param p3, "effectNumber"    # I

    .prologue
    .line 147
    iput p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mWaitDuration:I

    .line 148
    return-void
.end method
