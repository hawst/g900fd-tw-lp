.class public Lcom/sec/samsung/gallery/controller/PostGalleryCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "PostGalleryCmd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/PostGalleryCmd$3;,
        Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;,
        Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private enumType:[Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

.field private mActivity:Landroid/app/Activity;

.field private mCategoryContentObserver:Landroid/database/ContentObserver;

.field private mHandler:Landroid/os/Handler;

.field private mPostCommandQueue:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;

.field private mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

.field private syncMasterAuto:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 61
    new-instance v0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;-><init>(Lcom/sec/samsung/gallery/controller/PostGalleryCmd;Lcom/sec/samsung/gallery/controller/PostGalleryCmd$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mPostCommandQueue:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;

    .line 63
    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mHandler:Landroid/os/Handler;

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->syncMasterAuto:Z

    .line 65
    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mActivity:Landroid/app/Activity;

    .line 72
    invoke-static {}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->values()[Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->enumType:[Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    .line 360
    new-instance v0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$2;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$2;-><init>(Lcom/sec/samsung/gallery/controller/PostGalleryCmd;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mCategoryContentObserver:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/PostGalleryCmd;)[Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/PostGalleryCmd;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->enumType:[Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/controller/PostGalleryCmd;Landroid/app/Activity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/PostGalleryCmd;
    .param p1, "x1"    # Landroid/app/Activity;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->create(Landroid/app/Activity;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/controller/PostGalleryCmd;Landroid/app/Activity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/PostGalleryCmd;
    .param p1, "x1"    # Landroid/app/Activity;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->resume(Landroid/app/Activity;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/controller/PostGalleryCmd;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/PostGalleryCmd;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/controller/PostGalleryCmd;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/PostGalleryCmd;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private cloudSync(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 328
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCloud:Z

    if-eqz v1, :cond_1

    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->isCloudContentSyncOn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 329
    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->cloudAlbumsSync(Landroid/content/Context;)V

    move-object v1, p1

    .line 330
    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DRSY"

    const/16 v3, 0x3e8

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 334
    :goto_0
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z

    if-eqz v1, :cond_0

    .line 335
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;-><init>()V

    .line 336
    .local v0, "refer":Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->isTCloudAccoutActivated(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 337
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->tCloudAlbumsSync(Landroid/content/Context;)V

    .line 339
    .end local v0    # "refer":Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    :cond_0
    return-void

    :cond_1
    move-object v1, p1

    .line 332
    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DRSY"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0
.end method

.method private create(Landroid/app/Activity;)V
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 185
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_TIME_ENABLE:Z

    if-eqz v0, :cond_0

    .line 186
    const-string v0, "Gallery_Performance"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PostGalleryCmd create() START "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-wide v4, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_TIME:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    :goto_0
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->start(Landroid/content/Context;)V

    .line 192
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretDirMounted(Landroid/content/Context;)V

    .line 193
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->initNearby(Landroid/app/Activity;)V

    .line 194
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->registerCategoryContentObserver()V

    .line 195
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v1

    move-object v0, p1

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/util/TTSUtil;->init(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 196
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->initSLink(Landroid/app/Activity;)V

    .line 198
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->putAppUseLog(Landroid/app/Activity;)V

    .line 199
    const-string v0, "Gallery_Performance"

    const-string v1, "PostGalleryCmd create() END"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    return-void

    .line 188
    :cond_0
    const-string v0, "Gallery_Performance"

    const-string v1, "PostGalleryCmd create() START "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private destroy(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 230
    const-string v1, "Gallery_Performance"

    const-string v2, "PostGalleryCmd destroy() START "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/TTSUtil;->onDestroy()V

    .line 233
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z

    if-eqz v1, :cond_0

    move-object v1, p1

    .line 234
    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    move-result-object v0

    .line 235
    .local v0, "slinkManager":Lcom/sec/android/gallery3d/remote/slink/SLinkManager;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->releaseWakeLockIfNeeded(Ljava/lang/String;)V

    .line 238
    .end local v0    # "slinkManager":Lcom/sec/android/gallery3d/remote/slink/SLinkManager;
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->unregisterCategoryContentObserver()V

    .line 239
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z

    if-eqz v1, :cond_1

    move-object v1, p1

    .line 240
    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->releaseInstance()V

    .line 242
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->putAppUseLog(Landroid/app/Activity;)V

    .line 243
    const-string v1, "Gallery_Performance"

    const-string v2, "PostGalleryCmd destroy() END"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    return-void
.end method

.method private initNearby(Landroid/app/Activity;)V
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v5, 0x0

    .line 203
    move-object v3, p1

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    .line 204
    .local v1, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-static {p1, v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->setActivity(Landroid/app/Activity;Lcom/sec/android/gallery3d/data/DataManager;)V

    .line 206
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNearby:Z

    if-eqz v3, :cond_1

    .line 207
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChnUsageAlertPopup:Z

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 209
    const-string v3, "ChangePlayerWifiDataAlertDialogOff"

    invoke-static {p1, v3, v5}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "ScanNearbyDeviceWifiDataAlertDialogOff"

    invoke-static {p1, v3, v5}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_2

    .line 213
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 214
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 215
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 216
    const-string v3, "com.samsung.android.sconnect.action.IMAGE_DMR"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "android.intent.action.MULTIPLE_PICK"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 217
    :cond_0
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->startNearbyFramework(Lcom/sec/android/gallery3d/data/DataManager;)V

    .line 227
    .end local v0    # "action":Ljava/lang/String;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_0
    return-void

    .line 221
    :cond_2
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->startNearbyFramework(Lcom/sec/android/gallery3d/data/DataManager;)V

    goto :goto_0

    .line 224
    :cond_3
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->startNearbyFramework(Lcom/sec/android/gallery3d/data/DataManager;)V

    goto :goto_0
.end method

.method private initSLink(Landroid/app/Activity;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v3, 0x0

    .line 296
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z

    if-eqz v1, :cond_0

    move-object v1, p1

    .line 297
    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    move-result-object v0

    .line 298
    .local v0, "slinkManager":Lcom/sec/android/gallery3d/remote/slink/SLinkManager;
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChnUsageAlertPopup:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 299
    const-string v1, "ScanNearbyDeviceWifiDataAlertDialogOff"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 309
    .end local v0    # "slinkManager":Lcom/sec/android/gallery3d/remote/slink/SLinkManager;
    :cond_0
    :goto_0
    return-void

    .line 303
    .restart local v0    # "slinkManager":Lcom/sec/android/gallery3d/remote/slink/SLinkManager;
    :cond_1
    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->acquireWakeLockIfNeeded(Ljava/lang/String;)V

    goto :goto_0

    .line 306
    :cond_2
    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->acquireWakeLockIfNeeded(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private pause(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 282
    const-string v0, "Gallery_Performance"

    const-string v1, "PostGalleryCmd pause() START "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->stopFindNewEvent()V

    .line 285
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z

    if-eqz v0, :cond_1

    .line 286
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    if-nez v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    .line 289
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->unregisterInitStateChangedReceiver()V

    .line 292
    :cond_1
    const-string v0, "Gallery_Performance"

    const-string v1, "PostGalleryCmd pause() END"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    return-void
.end method

.method private putAppUseLog(Landroid/app/Activity;)V
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 342
    move-object v3, p1

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    .line 343
    .local v1, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 344
    .local v2, "topSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v0

    .local v0, "albumCount":I
    move-object v3, p1

    .line 345
    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ABCO"

    mul-int/lit16 v5, v0, 0x3e8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5, p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 347
    return-void
.end method

.method private registerCategoryContentObserver()V
    .locals 4

    .prologue
    .line 350
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->DCM_IMAGE_TABLE_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mCategoryContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 353
    :cond_0
    return-void
.end method

.method private remoteSync(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 313
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z

    if-eqz v1, :cond_0

    move-object v1, p1

    .line 314
    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    const-string/jumbo v2, "slink"

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getSource(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSource;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/slink/SLinkSource;

    .line 315
    .local v0, "slinkSource":Lcom/sec/android/gallery3d/remote/slink/SLinkSource;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkSource;->getSLinkClient()Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    move-result-object v2

    move-object v1, p1

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->setLoaderManager(Landroid/app/Activity;)V

    .line 318
    .end local v0    # "slinkSource":Lcom/sec/android/gallery3d/remote/slink/SLinkSource;
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->cloudSync(Landroid/content/Context;)V

    .line 319
    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->startSync(Landroid/content/Context;)V

    move-object v1, p1

    .line 320
    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    const-string v2, "picasa"

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getSource(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSource;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->requestSyncAll()V

    .line 321
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->syncMasterAuto:Z

    .line 322
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->syncMasterAuto:Z

    if-eqz v1, :cond_1

    .line 323
    const/4 v1, 0x1

    const-wide/16 v2, -0x1

    invoke-static {p1, v1, v2, v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->requestSync(Landroid/content/Context;IJ)V

    .line 325
    :cond_1
    return-void
.end method

.method private resume(Landroid/app/Activity;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v3, 0x1

    .line 247
    const-string v1, "Gallery_Performance"

    const-string v2, "PostGalleryCmd resume() START "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePostInflateNavigationDrawer:Z

    if-eqz v1, :cond_0

    move-object v1, p1

    .line 249
    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v0

    .line 250
    .local v0, "drawer":Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->getPostInflateStatus()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, p1

    .line 251
    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "INFLATE_DRAWER"

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;)V

    .line 255
    .end local v0    # "drawer":Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
    :cond_0
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChnUsageAlertPopup:Z

    if-ne v1, v3, :cond_6

    .line 256
    const-string v1, "LocationPermissionAlertDialogOff"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    if-ne v1, v3, :cond_1

    .line 257
    invoke-static {}, Lcom/sec/android/gallery3d/util/LocationUtils;->getInstance()Lcom/sec/android/gallery3d/util/LocationUtils;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/util/LocationUtils;->init(Landroid/content/Context;)V

    .line 262
    :cond_1
    :goto_0
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v2

    move-object v1, p1

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/util/TTSUtil;->onResume(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 263
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->startFindNewEvent()V

    .line 264
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->remoteSync(Landroid/content/Context;)V

    .line 266
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceThumbnail:Z

    if-eqz v1, :cond_2

    .line 267
    const/4 v1, 0x0

    invoke-static {p1, v1, v3}, Lcom/sec/samsung/gallery/access/face/FaceInterface;->requestFaceScan(Landroid/content/Context;Ljava/util/List;Z)V

    .line 269
    :cond_2
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->checkUseRecommandDropboxPopup(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mActivity:Landroid/app/Activity;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryCoverMode()Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverMode()Z

    move-result v1

    if-nez v1, :cond_3

    .line 270
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->showRecommandDropboxPopup()V

    .line 272
    :cond_3
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z

    if-eqz v1, :cond_5

    .line 273
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    if-nez v1, :cond_4

    .line 274
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mActivity:Landroid/app/Activity;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    .line 276
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->registerInitStateChangedReceiver()V

    .line 278
    :cond_5
    const-string v1, "Gallery_Performance"

    const-string v2, "PostGalleryCmd resume() END"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    return-void

    .line 260
    :cond_6
    invoke-static {}, Lcom/sec/android/gallery3d/util/LocationUtils;->getInstance()Lcom/sec/android/gallery3d/util/LocationUtils;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/util/LocationUtils;->init(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private run(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 172
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mPostCommandQueue:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 173
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mPostCommandQueue:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;->dequeue()Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    move-result-object v0

    .line 174
    .local v0, "cmd":Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;
    sget-object v1, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->DESTROY:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    if-ne v0, v1, :cond_1

    .line 175
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->destroy(Landroid/app/Activity;)V

    goto :goto_0

    .line 176
    :cond_1
    sget-object v1, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->PAUSE:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    if-ne v0, v1, :cond_2

    .line 177
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->pause(Landroid/app/Activity;)V

    goto :goto_0

    .line 178
    :cond_2
    if-eqz v0, :cond_0

    .line 179
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 182
    .end local v0    # "cmd":Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;
    :cond_3
    return-void
.end method

.method private showRecommandDropboxPopup()V
    .locals 5

    .prologue
    .line 394
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mActivity:Landroid/app/Activity;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    .line 395
    .local v0, "galleryFacade":Lcom/sec/samsung/gallery/core/GalleryFacade;
    const-string v1, "RECOMMEND_DROPBOX"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mActivity:Landroid/app/Activity;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 398
    return-void
.end method

.method private unregisterCategoryContentObserver()V
    .locals 2

    .prologue
    .line 356
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mCategoryContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 358
    :cond_0
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 6
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 77
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Object;

    move-object v2, v3

    check-cast v2, [Ljava/lang/Object;

    .line 78
    .local v2, "params":[Ljava/lang/Object;
    const/4 v3, 0x0

    aget-object v0, v2, v3

    check-cast v0, Landroid/app/Activity;

    .line 79
    .local v0, "activity":Landroid/app/Activity;
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mActivity:Landroid/app/Activity;

    .line 80
    const/4 v3, 0x1

    aget-object v1, v2, v3

    check-cast v1, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    .line 82
    .local v1, "cmdType":Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mHandler:Landroid/os/Handler;

    if-nez v3, :cond_0

    .line 83
    new-instance v3, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$1;

    invoke-virtual {v0}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, p0, v4, v0}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/PostGalleryCmd;Landroid/os/Looper;Landroid/app/Activity;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mHandler:Landroid/os/Handler;

    .line 103
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mPostCommandQueue:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;

    monitor-enter v4

    .line 104
    :try_start_0
    sget-object v3, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->RUN:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    if-ne v1, v3, :cond_4

    .line 105
    sget-boolean v3, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->sbNeedFullLoading:Z

    if-nez v3, :cond_1

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z

    if-eqz v3, :cond_2

    sget-boolean v3, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->sbNeedFullLoading:Z

    if-eqz v3, :cond_2

    .line 106
    :cond_1
    monitor-exit v4

    .line 132
    :goto_0
    return-void

    .line 108
    :cond_2
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->run(Landroid/app/Activity;)V

    .line 131
    :cond_3
    :goto_1
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 109
    :cond_4
    :try_start_1
    sget-object v3, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->CREATE:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    if-ne v1, v3, :cond_5

    .line 110
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mPostCommandQueue:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;

    sget-object v5, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->CREATE:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;->enqueue(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V

    goto :goto_1

    .line 111
    :cond_5
    sget-object v3, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->RESUME:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    if-ne v1, v3, :cond_7

    .line 112
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mPostCommandQueue:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;->size()I

    move-result v3

    if-nez v3, :cond_6

    .line 113
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mPostCommandQueue:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;

    sget-object v5, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->RESUME:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;->enqueue(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V

    .line 114
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->run(Landroid/app/Activity;)V

    goto :goto_1

    .line 116
    :cond_6
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mPostCommandQueue:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;

    sget-object v5, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->RESUME:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;->enqueue(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V

    goto :goto_1

    .line 118
    :cond_7
    sget-object v3, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->DESTROY:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    if-ne v1, v3, :cond_8

    .line 119
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mPostCommandQueue:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;

    sget-object v5, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->CREATE:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;->removeIfExist(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 120
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mPostCommandQueue:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;

    sget-object v5, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->DESTROY:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;->enqueue(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V

    .line 121
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->run(Landroid/app/Activity;)V

    goto :goto_1

    .line 123
    :cond_8
    sget-object v3, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->PAUSE:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    if-ne v1, v3, :cond_9

    .line 124
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mPostCommandQueue:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;

    sget-object v5, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->RESUME:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;->removeIfExist(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 125
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mPostCommandQueue:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;

    sget-object v5, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->PAUSE:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostCommandQueue;->enqueue(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V

    .line 126
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->run(Landroid/app/Activity;)V

    goto :goto_1

    .line 129
    :cond_9
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
.end method
