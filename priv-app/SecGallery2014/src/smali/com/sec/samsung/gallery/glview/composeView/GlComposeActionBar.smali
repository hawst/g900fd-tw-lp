.class public Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;
.super Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
.source "GlComposeActionBar.java"


# static fields
.field public static final AB_DELETE_ID:I = 0x4

.field public static final AB_EDIT_ID:I = 0x2

.field public static final AB_FLASH_ANNOTATE_ID:I = 0x1

.field public static final AB_SHARE_ID:I = 0x3


# instance fields
.field private mBackGroundView:Lcom/sec/android/gallery3d/glcore/GlImageView;

.field private final mBaseDividerId:I

.field private mContext:Landroid/content/Context;

.field private mCurrentAbsoluteRightPosition:I

.field private mDividerConfig:Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;

.field private mHoverEnter:Z

.field private mLeftMargin:I

.field private mNeedToLayoutActionBar:Z

.field private mRightMargin:I

.field private mVisibilityMap:Landroid/util/SparseIntArray;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;Landroid/content/Context;)V
    .locals 2
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    .line 15
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mBaseDividerId:I

    .line 24
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mCurrentAbsoluteRightPosition:I

    .line 25
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mRightMargin:I

    .line 26
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mLeftMargin:I

    .line 29
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mHoverEnter:Z

    .line 31
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mNeedToLayoutActionBar:Z

    .line 35
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->removeAllAnimation()V

    .line 36
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mContext:Landroid/content/Context;

    .line 37
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mVisibilityMap:Landroid/util/SparseIntArray;

    .line 38
    return-void
.end method

.method private addDivider(I)V
    .locals 6
    .param p1, "dividerId"    # I

    .prologue
    .line 52
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 53
    .local v0, "divView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mDividerConfig:Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;->width:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mDividerConfig:Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;->height:I

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 54
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mDividerConfig:Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;->horizontalAlign:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mDividerConfig:Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;->verticalAlign:I

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 55
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mDividerConfig:Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;->drawableId:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mDividerConfig:Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;->width:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mDividerConfig:Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;->height:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mContext:Landroid/content/Context;

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/gallery3d/common/BitmapUtils;->getNinePatch(IIILandroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 57
    .local v1, "ninePatch":Landroid/graphics/Bitmap;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 58
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mBackGroundView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {v2, v0, p1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 60
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->setDividerPosition(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 61
    return-void
.end method

.method private dispatchHoverEventInter(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    .line 184
    .local v0, "gv":Lcom/sec/android/gallery3d/glcore/GlView;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlView;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 185
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlView;->getChild(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/samsung/gallery/glview/GlButtonView;

    if-eqz v2, :cond_1

    .line 186
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlView;->getChild(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/glcore/GlView;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 191
    :cond_0
    return-void

    .line 184
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private getVisibleButtonsCount()I
    .locals 3

    .prologue
    .line 121
    const/4 v1, 0x0

    .line 122
    .local v1, "visibleItemsCount":I
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mVisibilityMap:Landroid/util/SparseIntArray;

    invoke-virtual {v2}, Landroid/util/SparseIntArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 123
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mVisibilityMap:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v2

    if-nez v2, :cond_0

    .line 124
    add-int/lit8 v1, v1, 0x1

    .line 122
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 126
    :cond_1
    return v1
.end method

.method private setButtonPosition(Lcom/sec/android/gallery3d/glcore/GlView;)V
    .locals 3
    .param p1, "actionBarButton"    # Lcom/sec/android/gallery3d/glcore/GlView;

    .prologue
    const/4 v2, 0x0

    .line 70
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mCurrentAbsoluteRightPosition:I

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mRightMargin:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mCurrentAbsoluteRightPosition:I

    .line 71
    const/4 v0, 0x5

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mCurrentAbsoluteRightPosition:I

    invoke-virtual {p1, v2, v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlView;->setMargine(IIII)V

    move-object v0, p1

    .line 72
    check-cast v0, Lcom/sec/samsung/gallery/glview/GlButtonView;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mCurrentAbsoluteRightPosition:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlButtonView;->setButtonRightMargine(I)V

    .line 73
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mCurrentAbsoluteRightPosition:I

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlView;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mCurrentAbsoluteRightPosition:I

    .line 74
    return-void
.end method

.method private setDividerPosition(Lcom/sec/android/gallery3d/glcore/GlView;)V
    .locals 3
    .param p1, "divView"    # Lcom/sec/android/gallery3d/glcore/GlView;

    .prologue
    const/4 v2, 0x0

    .line 64
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mCurrentAbsoluteRightPosition:I

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mLeftMargin:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mCurrentAbsoluteRightPosition:I

    .line 65
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mCurrentAbsoluteRightPosition:I

    invoke-virtual {p1, v2, v2, v0, v2}, Lcom/sec/android/gallery3d/glcore/GlView;->setMargine(IIII)V

    .line 66
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mCurrentAbsoluteRightPosition:I

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlView;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mCurrentAbsoluteRightPosition:I

    .line 67
    return-void
.end method


# virtual methods
.method public addActionBarItem(Lcom/sec/samsung/gallery/glview/GlButtonView;I)V
    .locals 2
    .param p1, "actionBarButton"    # Lcom/sec/samsung/gallery/glview/GlButtonView;
    .param p2, "childId"    # I

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mBackGroundView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {v0, p2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 90
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mVisibilityMap:Landroid/util/SparseIntArray;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 83
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mCurrentAbsoluteRightPosition:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mDividerConfig:Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;

    if-eqz v0, :cond_1

    .line 84
    add-int/lit16 v0, p2, 0x3e8

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->addDivider(I)V

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mBackGroundView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 88
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->setButtonPosition(Lcom/sec/android/gallery3d/glcore/GlView;)V

    goto :goto_0
.end method

.method protected dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x0

    .line 159
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getPenHoveringIconLabels(Landroid/content/Context;)Z

    move-result v1

    .line 160
    .local v1, "isPenHoverIconLabelsOn":Z
    if-nez v1, :cond_1

    .line 179
    :cond_0
    :goto_0
    return v6

    .line 163
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v2, v4

    .line 164
    .local v2, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v3, v4

    .line 165
    .local v3, "y":I
    invoke-virtual {p0, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->checkPosIn(II)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 166
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mHoverEnter:Z

    .line 167
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->getXlt()F

    move-result v4

    neg-float v4, v4

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->getYlt()F

    move-result v5

    neg-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 168
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->dispatchHoverEventInter(Landroid/view/MotionEvent;)V

    .line 169
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->getXlt()F

    move-result v4

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->getYlt()F

    move-result v5

    invoke-virtual {p1, v4, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    goto :goto_0

    .line 170
    :cond_2
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mHoverEnter:Z

    if-eqz v4, :cond_0

    .line 171
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mHoverEnter:Z

    .line 172
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->getXlt()F

    move-result v4

    neg-float v4, v4

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->getYlt()F

    move-result v5

    neg-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 173
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 174
    .local v0, "action":I
    const/16 v4, 0xa

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->setAction(I)V

    .line 175
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->dispatchHoverEventInter(Landroid/view/MotionEvent;)V

    .line 176
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->getXlt()F

    move-result v4

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->getYlt()F

    move-result v5

    invoke-virtual {p1, v4, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 177
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_0
.end method

.method public getBG()Lcom/sec/android/gallery3d/glcore/GlImageView;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mBackGroundView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    return-object v0
.end method

.method protected onPressed(II)Z
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 205
    const/4 v0, 0x1

    return v0
.end method

.method public redrawActionBar()V
    .locals 7

    .prologue
    const/16 v6, 0x3e8

    const/4 v5, 0x0

    .line 93
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mNeedToLayoutActionBar:Z

    if-eqz v4, :cond_4

    .line 94
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mNeedToLayoutActionBar:Z

    .line 95
    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mCurrentAbsoluteRightPosition:I

    .line 96
    const/4 v1, 0x0

    .line 97
    .local v1, "drawnVisibleButtons":I
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->getVisibleButtonsCount()I

    move-result v4

    add-int/lit8 v3, v4, -0x1

    .line 99
    .local v3, "separatorsNeededToBeDrawn":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mBackGroundView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getChildCount()I

    move-result v4

    if-ge v2, v4, :cond_4

    .line 100
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mBackGroundView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getChild(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    .line 101
    .local v0, "child":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlView;->getId()I

    move-result v4

    if-ge v4, v6, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    .line 102
    add-int/lit8 v1, v1, 0x1

    .line 103
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->setButtonPosition(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 99
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 105
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlView;->getId()I

    move-result v4

    if-le v4, v6, :cond_0

    .line 106
    if-eqz v1, :cond_2

    if-nez v3, :cond_3

    .line 107
    :cond_2
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->setVisibility(I)V

    goto :goto_1

    .line 110
    :cond_3
    add-int/lit8 v1, v1, -0x1

    .line 111
    add-int/lit8 v3, v3, -0x1

    .line 112
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->setDividerPosition(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 113
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlView;->setVisibility(I)V

    goto :goto_1

    .line 117
    .end local v0    # "child":Lcom/sec/android/gallery3d/glcore/GlView;
    .end local v1    # "drawnVisibleButtons":I
    .end local v2    # "i":I
    .end local v3    # "separatorsNeededToBeDrawn":I
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mBackGroundView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 118
    return-void
.end method

.method public removeBGChildview()V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mBackGroundView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mBackGroundView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 153
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mBackGroundView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->removeAllChilds()V

    .line 155
    :cond_0
    return-void
.end method

.method public resetButton()V
    .locals 4

    .prologue
    .line 193
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mBackGroundView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getChildCount()I

    move-result v0

    .line 194
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 195
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mBackGroundView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getChild(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    .line 196
    .local v2, "view":Lcom/sec/android/gallery3d/glcore/GlView;
    instance-of v3, v2, Lcom/sec/samsung/gallery/glview/GlButtonView;

    if-eqz v3, :cond_0

    .line 197
    check-cast v2, Lcom/sec/samsung/gallery/glview/GlButtonView;

    .end local v2    # "view":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlButtonView;->resetButton()V

    .line 194
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 201
    :cond_1
    return-void
.end method

.method public setBackGroundView(Lcom/sec/android/gallery3d/glcore/GlImageView;)V
    .locals 1
    .param p1, "backgroundView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mBackGroundView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    if-eqz v0, :cond_0

    .line 45
    :goto_0
    return-void

    .line 43
    :cond_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mBackGroundView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mBackGroundView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    goto :goto_0
.end method

.method public setButtonsMargin(II)V
    .locals 0
    .param p1, "leftMargin"    # I
    .param p2, "rightMargin"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mLeftMargin:I

    .line 143
    iput p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mRightMargin:I

    .line 144
    return-void
.end method

.method public setDefaultDividerConfig(Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;)V
    .locals 0
    .param p1, "dividerConfig"    # Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mDividerConfig:Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;

    .line 49
    return-void
.end method

.method public setItemVisibility(II)V
    .locals 3
    .param p1, "buttonId"    # I
    .param p2, "glViewVisibility"    # I

    .prologue
    const/4 v2, 0x1

    .line 130
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mVisibilityMap:Landroid/util/SparseIntArray;

    invoke-virtual {v1, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 131
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mBackGroundView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    .line 132
    .local v0, "view":Lcom/sec/android/gallery3d/glcore/GlView;
    if-eqz v0, :cond_1

    .line 133
    if-ne p2, v2, :cond_0

    .line 134
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mBackGroundView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    .line 135
    :cond_0
    invoke-virtual {v0, p2}, Lcom/sec/android/gallery3d/glcore/GlView;->setVisibility(I)V

    .line 138
    :cond_1
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->mNeedToLayoutActionBar:Z

    .line 139
    return-void
.end method
