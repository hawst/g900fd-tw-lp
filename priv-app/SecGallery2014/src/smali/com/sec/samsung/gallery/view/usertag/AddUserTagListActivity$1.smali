.class Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "AddUserTagListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 73
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 74
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 75
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 76
    .local v1, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$000(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 77
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->SECRETBOX_PATH:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 78
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$000(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 79
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->finish()V

    .line 87
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-void

    .line 82
    .restart local v1    # "uri":Landroid/net/Uri;
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$000(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 83
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->finish()V

    goto :goto_0
.end method
