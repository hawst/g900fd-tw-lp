.class Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$18$1;
.super Ljava/lang/Object;
.source "AlbumViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/OnProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$18;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$18;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$18;)V
    .locals 0

    .prologue
    .line 2056
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$18$1;->this$1:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$18;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 2068
    const/4 v0, 0x0

    return v0
.end method

.method public handleOperation(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2073
    .local p1, "objectList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public onCompleted(Z)V
    .locals 2
    .param p1, "result"    # Z

    .prologue
    .line 2063
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$18$1;->this$1:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$18;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$18;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$6000(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "REFRESH_SELECTION"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;)V

    .line 2064
    return-void
.end method

.method public onProgress(II)V
    .locals 0
    .param p1, "progress"    # I
    .param p2, "total"    # I

    .prologue
    .line 2059
    return-void
.end method
