.class Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick$1;
.super Ljava/lang/Object;
.source "AlbumActionBarForMultiPick.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->setTitle(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;

.field final synthetic val$numberOfItemsSelected:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;I)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;

    iput p2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 36
    const/4 v0, 0x0

    .line 37
    .local v0, "numberOfSelectedItems":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mMenu:Landroid/view/Menu;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->access$000(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;)Landroid/view/Menu;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 38
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->access$100(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->invalidateOptionsMenu()V

    .line 40
    :cond_0
    iget v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    if-ltz v4, :cond_1

    .line 41
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->access$200(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 42
    .local v1, "res":Landroid/content/res/Resources;
    const v4, 0x7f0e004f

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v2, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 46
    .end local v1    # "res":Landroid/content/res/Resources;
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mIsPersonPick:Z
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->access$300(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 47
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->access$400(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 48
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;

    iget v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->setSelectAllButtonTitle(ILjava/lang/String;)V
    invoke-static {v4, v5, v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->access$500(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;ILjava/lang/String;)V

    .line 49
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mMenu:Landroid/view/Menu;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->access$000(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;)Landroid/view/Menu;

    move-result-object v4

    const v5, 0x7f0f0024

    iget v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    if-lez v6, :cond_3

    :goto_0
    invoke-static {v4, v5, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemEnabled(Landroid/view/Menu;IZ)V

    .line 50
    return-void

    :cond_3
    move v2, v3

    .line 49
    goto :goto_0
.end method
