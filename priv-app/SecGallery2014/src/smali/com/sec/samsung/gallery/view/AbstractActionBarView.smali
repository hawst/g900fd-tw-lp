.class public abstract Lcom/sec/samsung/gallery/view/AbstractActionBarView;
.super Lcom/sec/samsung/gallery/view/ViewObservable;
.source "AbstractActionBarView.java"


# static fields
.field protected static final APP_DEFALUT_MIME_TYPE:Ljava/lang/String; = "application/octet-stream"

.field public static final STYLE_BLACK:I = 0x1

.field public static final STYLE_DEFAULT:I = 0x0

.field public static final STYLE_TRANSPARENT:I = 0x2


# instance fields
.field protected final mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field protected mAttribute:I

.field protected final mMainActionBar:Landroid/app/ActionBar;

.field protected mMimeType:Ljava/lang/String;

.field protected final mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field protected final mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field protected mSpinnerView:Landroid/widget/ImageView;

.field protected final mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field protected mStyle:I

.field protected mUriList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;I)V

    .line 72
    return-void
.end method

.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;I)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "style"    # I

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/ViewObservable;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mStyle:I

    .line 53
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 54
    iput p2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mStyle:I

    .line 55
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mMainActionBar:Landroid/app/ActionBar;

    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 57
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 60
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->initialize()V

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->setActionBarLayout()V

    return-void
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView$1;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 85
    return-void
.end method

.method private setActionBarLayout()V
    .locals 7

    .prologue
    const v6, 0x7f11002a

    const v5, 0x7f0200e6

    .line 88
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mMainActionBar:Landroid/app/ActionBar;

    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 91
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mStyle:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 92
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/Context;->setTheme(I)V

    .line 93
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mMainActionBar:Landroid/app/ActionBar;

    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02003c

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 104
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mMainActionBar:Landroid/app/ActionBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 105
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->setHomeAsUpIndicator()V

    .line 107
    const/16 v1, 0x10

    .line 108
    .local v1, "flags":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 109
    return-void

    .line 95
    .end local v1    # "flags":I
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/Context;->setTheme(I)V

    .line 96
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v2, :cond_2

    .line 97
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    .line 98
    .local v0, "colorDrawable":Landroid/graphics/drawable/ColorDrawable;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0033

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 99
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2, v0}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 101
    .end local v0    # "colorDrawable":Landroid/graphics/drawable/ColorDrawable;
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mMainActionBar:Landroid/app/ActionBar;

    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private setHomeAsUpIndicator()V
    .locals 3

    .prologue
    .line 113
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePostInflateNavigationDrawer:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->getPostInflateStatus()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 114
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 117
    .local v0, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v1, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-nez v1, :cond_1

    instance-of v1, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 120
    :cond_0
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDrawerIconNotSetAlpha:Z

    if-nez v1, :cond_2

    instance-of v1, v0, Lcom/sec/samsung/gallery/view/help/HelpViewState;

    if-nez v1, :cond_2

    .line 121
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v1

    const/16 v2, 0xb2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setDrawerIconWithAlpha(I)V

    .line 127
    .end local v0    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_1
    :goto_0
    return-void

    .line 123
    .restart local v0    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setDrawerIcon()V

    goto :goto_0
.end method

.method private showUpButtonOnTopLeft()V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView$5;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView$5;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 206
    return-void
.end method


# virtual methods
.method public checkShowToastOverAvailableSelect(Lcom/sec/android/gallery3d/data/MediaItem;I)Z
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "numberOfSelect"    # I

    .prologue
    .line 315
    const/4 v0, 0x1

    return v0
.end method

.method public disablePopupMenu()V
    .locals 0

    .prologue
    .line 300
    return-void
.end method

.method public enablePopupMenu()V
    .locals 0

    .prologue
    .line 303
    return-void
.end method

.method public getActionBar()Landroid/app/ActionBar;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method public getPopUpMenu()Landroid/widget/ListPopupWindow;
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSelectionModeBarHeight()I
    .locals 1

    .prologue
    .line 326
    const/4 v0, 0x0

    return v0
.end method

.method public getTotalSelectedItemsCount()I
    .locals 1

    .prologue
    .line 330
    const/4 v0, 0x0

    return v0
.end method

.method protected hide()V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView$3;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView$3;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 149
    return-void
.end method

.method protected initDisplayOptions()V
    .locals 0

    .prologue
    .line 243
    return-void
.end method

.method public isManualRotateIconEnabled()Z
    .locals 1

    .prologue
    .line 334
    const/4 v0, 0x0

    return v0
.end method

.method protected isReadyToEnableHomeButton()Z
    .locals 1

    .prologue
    .line 348
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePostInflateNavigationDrawer:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->getPostInflateStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    const/4 v0, 0x0

    .line 351
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onConfigChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "configuration"    # Landroid/content/res/Configuration;

    .prologue
    .line 312
    return-void
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 252
    return-void
.end method

.method protected onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 0
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 258
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView$7;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView$7;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 291
    return-void
.end method

.method protected onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 255
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 294
    return-void
.end method

.method public selectionModeBarVisibiltiy(IZ)V
    .locals 0
    .param p1, "visible"    # I
    .param p2, "enableAnim"    # Z

    .prologue
    .line 323
    return-void
.end method

.method public setDisplayOptions(ZZ)V
    .locals 2
    .param p1, "displayHomeAsUp"    # Z
    .param p2, "showTitle"    # Z

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mMainActionBar:Landroid/app/ActionBar;

    if-nez v0, :cond_0

    .line 240
    :goto_0
    return-void

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/samsung/gallery/view/AbstractActionBarView$6;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarView;ZZ)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setDropDownIcon(Z)V
    .locals 0
    .param p1, "isShow"    # Z

    .prologue
    .line 65
    return-void
.end method

.method protected setIconOnlyButton(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 215
    if-nez p1, :cond_1

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->isOnlyIconDisplayedOnActionBar(Landroid/content/Context;)Z

    move-result v0

    .line 219
    .local v0, "iconOnly":Z
    if-eqz v0, :cond_0

    .line 220
    const-string v1, ""

    invoke-interface {p1, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected setSelectedItemCount(I)V
    .locals 0
    .param p1, "numberOfSelectedItems"    # I

    .prologue
    .line 267
    return-void
.end method

.method protected setSuggestionAdapter()V
    .locals 0

    .prologue
    .line 279
    return-void
.end method

.method protected setTextOnlyButton(Landroid/view/MenuItem;)V
    .locals 1
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 209
    if-nez p1, :cond_0

    .line 212
    :goto_0
    return-void

    .line 211
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected setTitle(I)V
    .locals 0
    .param p1, "numberOfSelectedItems"    # I

    .prologue
    .line 261
    return-void
.end method

.method protected setTitle(II)V
    .locals 0
    .param p1, "numberOfSelectedItems"    # I
    .param p2, "countOfMediaItem"    # I

    .prologue
    .line 264
    return-void
.end method

.method protected setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "albunName"    # Ljava/lang/String;

    .prologue
    .line 270
    return-void
.end method

.method protected setTitle(Ljava/lang/String;I)V
    .locals 0
    .param p1, "albumName"    # Ljava/lang/String;
    .param p2, "numberOfItemsInAlbum"    # I

    .prologue
    .line 273
    return-void
.end method

.method protected setTitle(Ljava/lang/String;II)V
    .locals 0
    .param p1, "itemName"    # Ljava/lang/String;
    .param p2, "currentPosition"    # I
    .param p3, "numberOfItemsInAlbum"    # I

    .prologue
    .line 276
    return-void
.end method

.method protected setupButtons()V
    .locals 0

    .prologue
    .line 246
    return-void
.end method

.method protected final show()V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView$2;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 140
    return-void
.end method

.method protected showAllButtonsOnTopLeft()V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->showUpButtonOnTopLeft()V

    .line 153
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->showHomeButtonOnTopLeft()V

    .line 154
    return-void
.end method

.method public showCameraButton(Z)V
    .locals 0
    .param p1, "show"    # Z

    .prologue
    .line 297
    return-void
.end method

.method public showDropDownIcon(Z)V
    .locals 0
    .param p1, "isShow"    # Z

    .prologue
    .line 68
    return-void
.end method

.method protected showHomeButtonOnTopLeft()V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView$4;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView$4;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 182
    return-void
.end method

.method public unregisterOrientationListener()V
    .locals 0

    .prologue
    .line 338
    return-void
.end method

.method protected updateActionbarButton()V
    .locals 0

    .prologue
    .line 309
    return-void
.end method

.method public updateBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 320
    return-void
.end method

.method protected updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V
    .locals 0
    .param p1, "button"    # Lcom/sec/samsung/gallery/util/Consts$ButtonType;
    .param p2, "visible"    # I
    .param p3, "toggleImage"    # Z

    .prologue
    .line 249
    return-void
.end method

.method public updateConfirm(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "format"    # Ljava/lang/String;

    .prologue
    .line 282
    return-void
.end method

.method protected updateDoneButton(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 306
    return-void
.end method

.method public updateFuntionSimplificationMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 344
    const/4 v0, 0x1

    return v0
.end method
