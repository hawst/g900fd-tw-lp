.class public Lcom/sec/samsung/gallery/drawer/DrawerItemTitle;
.super Lcom/sec/samsung/gallery/drawer/DrawerItem;
.source "DrawerItemTitle.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/sec/samsung/gallery/drawer/DrawerItemTitle;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/drawer/DrawerItemTitle;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(ILcom/sec/samsung/gallery/core/TabTagType;)V
    .locals 0
    .param p1, "groupid"    # I
    .param p2, "tagType"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/drawer/DrawerItem;-><init>(ILcom/sec/samsung/gallery/core/TabTagType;)V

    .line 18
    return-void
.end method

.method private constructor <init>(ILjava/lang/CharSequence;ILcom/sec/samsung/gallery/core/TabTagType;)V
    .locals 0
    .param p1, "groupid"    # I
    .param p2, "name"    # Ljava/lang/CharSequence;
    .param p3, "ttsId"    # I
    .param p4, "tagType"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    .line 13
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/drawer/DrawerItem;-><init>(ILjava/lang/CharSequence;ILcom/sec/samsung/gallery/core/TabTagType;)V

    .line 14
    return-void
.end method

.method public static createEmptyTitleItems(I)Lcom/sec/samsung/gallery/drawer/DrawerItem;
    .locals 2
    .param p0, "groupId"    # I

    .prologue
    .line 26
    new-instance v0, Lcom/sec/samsung/gallery/drawer/DrawerItemTitle;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/drawer/DrawerItemTitle;-><init>(ILcom/sec/samsung/gallery/core/TabTagType;)V

    return-object v0
.end method

.method public static createGroupTitleItems(IILjava/lang/String;)Lcom/sec/samsung/gallery/drawer/DrawerItem;
    .locals 2
    .param p0, "groupId"    # I
    .param p1, "ttsId"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 21
    new-instance v0, Lcom/sec/samsung/gallery/drawer/DrawerItemTitle;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p2, p1, v1}, Lcom/sec/samsung/gallery/drawer/DrawerItemTitle;-><init>(ILjava/lang/CharSequence;ILcom/sec/samsung/gallery/core/TabTagType;)V

    return-object v0
.end method


# virtual methods
.method public selectItem(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/StateManager;IZ)V
    .locals 0
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "statusProxy"    # Lcom/sec/android/gallery3d/app/StateManager;
    .param p3, "index"    # I
    .param p4, "isPickMode"    # Z

    .prologue
    .line 33
    return-void
.end method
