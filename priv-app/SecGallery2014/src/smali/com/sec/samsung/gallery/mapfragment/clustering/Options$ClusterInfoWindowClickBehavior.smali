.class public final enum Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;
.super Ljava/lang/Enum;
.source "Options.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ClusterInfoWindowClickBehavior"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

.field public static final enum HIDE_INFO_WINDOW:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

.field public static final enum NO_OP:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

.field public static final enum ZOOM_TO_BOUNDS:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 354
    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    const-string v1, "ZOOM_TO_BOUNDS"

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;->ZOOM_TO_BOUNDS:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    const-string v1, "HIDE_INFO_WINDOW"

    invoke-direct {v0, v1, v3}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;->HIDE_INFO_WINDOW:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    const-string v1, "NO_OP"

    invoke-direct {v0, v1, v4}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;->NO_OP:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    .line 353
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    sget-object v1, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;->ZOOM_TO_BOUNDS:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;->HIDE_INFO_WINDOW:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;->NO_OP:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;->$VALUES:[Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 353
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 353
    const-class v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    return-object v0
.end method

.method public static values()[Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;
    .locals 1

    .prologue
    .line 353
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;->$VALUES:[Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    invoke-virtual {v0}, [Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    return-object v0
.end method
