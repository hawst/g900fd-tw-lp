.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$17;
.super Ljava/lang/Object;
.source "PhotoViewComponent.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V
    .locals 0

    .prologue
    .line 2760
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$17;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)Z
    .locals 1
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "albumIndex"    # I
    .param p4, "itemIndex"    # I

    .prologue
    .line 2764
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$17;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    if-eqz v0, :cond_0

    .line 2765
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$17;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->onNewAlbumClick()V

    .line 2768
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2767
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$17;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v0, p3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->onAlbumClick(I)V

    goto :goto_0
.end method
