.class public Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;
.super Ljava/lang/Object;
.source "MoreInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$LoadBitmapDataTask;,
        Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$EditSaveTask;,
        Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;,
        Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$MyAnimatorListener;
    }
.end annotation


# static fields
.field public static final ACTION_RESULT_CANCEL:I = 0x1

.field public static final ACTION_RESULT_DONE:I = 0x0

.field public static final ACTION_RESULT_NONE:I = -0x1

.field private static final ALPHA_VLUE:I = 0xb3

.field public static final EVENT_MOREINFO_START:I = 0x0

.field private static final MESSAGE_BITMAP:I = 0x0

.field private static final MESSAGE_UPDATE_ACTIONBAR:I = 0x1

.field private static final MIX_RATIO:F = 0.4f

.field public static final MODE_TARTGET_EDIT:I = 0x2

.field public static final MODE_TARTGET_NORMAL:I = 0x1

.field public static final MODE_TARTGET_USERTAG_EDIT:I = 0x3

.field private static final MORE_COLOR_VALUE:D = 2.0

.field private static final TAG:Ljava/lang/String; = "MoreInfo"

.field public static final TYPE_CATEGORY:I = 0x0

.field public static final TYPE_TAG:I = 0x1


# instance fields
.field private final mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

.field private mBackgorundColor:I

.field private mBodyView:Landroid/widget/LinearLayout;

.field private final mContext:Landroid/content/Context;

.field private mEditListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;

.field private final mFragmentManager:Landroid/app/FragmentManager;

.field private mHandler:Landroid/os/Handler;

.field private mIsEditMode:Z

.field private mIsEnabled:Z

.field private mListItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;",
            ">;"
        }
    .end annotation
.end field

.field private mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mMoreInfoDateItem:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemSimple;

.field private mMoreInfoItemCategory:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;

.field private mMoreInfoItemDetails:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;

.field private mMoreInfoItemUserTag:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

.field private mMoreInfoLocationItem:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

.field private mRootView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/samsung/gallery/view/ActionBarManager;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p3, "manager"    # Lcom/sec/samsung/gallery/view/ActionBarManager;

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mListItems:Ljava/util/ArrayList;

    .line 107
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$1;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mHandler:Landroid/os/Handler;

    .line 139
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$2;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mEditListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;

    .line 148
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    .line 149
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 150
    iput-object p3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 151
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b003d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mBackgorundColor:I

    .line 152
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mFragmentManager:Landroid/app/FragmentManager;

    .line 153
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->setEnable(Z)V

    .line 154
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->initActionBar()V

    .line 155
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mBackgorundColor:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;
    .param p1, "x1"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mBackgorundColor:I

    return p1
.end method

.method static synthetic access$100(Landroid/graphics/Bitmap;ID)I
    .locals 2
    .param p0, "x0"    # Landroid/graphics/Bitmap;
    .param p1, "x1"    # I
    .param p2, "x2"    # D

    .prologue
    .line 54
    invoke-static {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->getAverageColor(Landroid/graphics/Bitmap;ID)I

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mIsEnabled:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->updateActionBar()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->isEditMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->startDestroyAnimation()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->resetEditMode()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mListItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->invalidate()V

    return-void
.end method

.method private actionEditCancel()V
    .locals 4

    .prologue
    .line 394
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->isUserTagEditMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 395
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoItemUserTag:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->exitUserTagEditMode(Z)V

    .line 404
    :goto_0
    return-void

    .line 399
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mListItems:Ljava/util/ArrayList;

    monitor-enter v3

    .line 400
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mListItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;

    .line 401
    .local v1, "item":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->actionEditCancel()V

    goto :goto_1

    .line 403
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private actionEditDone()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 385
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->isUserTagEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoItemUserTag:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->exitUserTagEditMode(Z)V

    .line 391
    :goto_0
    return-void

    .line 390
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$EditSaveTask;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$EditSaveTask;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;Landroid/content/Context;Z)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$EditSaveTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private static getAverageColor(Landroid/graphics/Bitmap;ID)I
    .locals 14
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "alpha"    # I
    .param p2, "more"    # D

    .prologue
    .line 580
    const/4 v6, 0x0

    .line 581
    .local v6, "red":I
    const/4 v4, 0x0

    .line 582
    .local v4, "green":I
    const/4 v2, 0x0

    .line 583
    .local v2, "blue":I
    const/4 v5, 0x0

    .line 585
    .local v5, "pixelCount":I
    const/4 v8, 0x0

    .local v8, "y":I
    :goto_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    if-ge v8, v9, :cond_1

    .line 586
    const/4 v7, 0x0

    .local v7, "x":I
    :goto_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    if-ge v7, v9, :cond_0

    .line 587
    invoke-virtual {p0, v7, v8}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v3

    .line 589
    .local v3, "c":I
    add-int/lit8 v5, v5, 0x1

    .line 590
    invoke-static {v3}, Landroid/graphics/Color;->red(I)I

    move-result v9

    add-int/2addr v6, v9

    .line 591
    invoke-static {v3}, Landroid/graphics/Color;->green(I)I

    move-result v9

    add-int/2addr v4, v9

    .line 592
    invoke-static {v3}, Landroid/graphics/Color;->blue(I)I

    move-result v9

    add-int/2addr v2, v9

    .line 586
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 585
    .end local v3    # "c":I
    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 595
    .end local v7    # "x":I
    :cond_1
    const/4 v9, 0x4

    new-array v0, v9, [I

    const/4 v9, 0x0

    aput p1, v0, v9

    const/4 v9, 0x1

    int-to-double v10, v6

    int-to-double v12, v5

    mul-double v12, v12, p2

    div-double/2addr v10, v12

    double-to-int v10, v10

    aput v10, v0, v9

    const/4 v9, 0x2

    int-to-double v10, v4

    int-to-double v12, v5

    mul-double v12, v12, p2

    div-double/2addr v10, v12

    double-to-int v10, v10

    aput v10, v0, v9

    const/4 v9, 0x3

    int-to-double v10, v2

    int-to-double v12, v5

    mul-double v12, v12, p2

    div-double/2addr v10, v12

    double-to-int v10, v10

    aput v10, v0, v9

    .line 599
    .local v0, "averageColor":[I
    const/4 v9, 0x4

    new-array v1, v9, [I

    const/4 v9, 0x0

    aput p1, v1, v9

    const/4 v9, 0x1

    const/4 v10, 0x0

    aput v10, v1, v9

    const/4 v9, 0x2

    const/4 v10, 0x0

    aput v10, v1, v9

    const/4 v9, 0x3

    const/4 v10, 0x0

    aput v10, v1, v9

    .line 602
    .local v1, "blackColor":[I
    const v9, 0x3ecccccd    # 0.4f

    invoke-static {v0, v1, v9}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mixColors([I[IF)I

    move-result v9

    return v9
.end method

.method private initChildView()V
    .locals 15

    .prologue
    const v2, 0x7f030097

    .line 314
    const/4 v1, 0x0

    .line 316
    .local v1, "index":I
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemSimple;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;

    new-instance v5, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;

    add-int/lit8 v11, v1, 0x1

    .end local v1    # "index":I
    .local v11, "index":I
    const v6, 0x7f030099

    const v7, 0x7f020216

    const v8, 0x7f0e0103

    invoke-direct {v5, v1, v6, v7, v8}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;-><init>(IIII)V

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemSimple;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;Lcom/sec/android/gallery3d/data/MediaItem;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoDateItem:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemSimple;

    .line 319
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mListItems:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoDateItem:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemSimple;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsGuestMode:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isGooglePlayServicesAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 322
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;

    new-instance v5, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;

    add-int/lit8 v1, v11, 0x1

    .end local v11    # "index":I
    .restart local v1    # "index":I
    const v6, 0x7f030098

    const v7, 0x7f020201

    const v8, 0x7f0e0074

    invoke-direct {v5, v11, v6, v7, v8}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;-><init>(IIII)V

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;Lcom/sec/android/gallery3d/data/MediaItem;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoLocationItem:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    .line 325
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoLocationItem:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mEditListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->setEditListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;)V

    .line 326
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mListItems:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoLocationItem:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 328
    :goto_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v0, :cond_0

    .line 329
    new-instance v6, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;

    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;

    add-int/lit8 v11, v1, 0x1

    .end local v1    # "index":I
    .restart local v11    # "index":I
    const v3, 0x7f020214

    const v4, 0x7f0e00a9

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;-><init>(IIIII)V

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {v6, v7, v8, v0, v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;Lcom/sec/android/gallery3d/data/MediaItem;)V

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoItemCategory:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;

    .line 332
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoItemCategory:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mEditListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->setEditListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;)V

    .line 333
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mListItems:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoItemCategory:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 335
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    iget-object v13, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    iget-object v14, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;

    new-instance v3, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;

    add-int/lit8 v1, v11, 0x1

    .end local v11    # "index":I
    .restart local v1    # "index":I
    const v6, 0x7f020202

    const v7, 0x7f0e046a

    const/4 v8, 0x1

    move v4, v11

    move v5, v2

    invoke-direct/range {v3 .. v8}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;-><init>(IIIII)V

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-object v4, v0

    move-object v5, v13

    move-object v6, v14

    move-object v7, v3

    invoke-direct/range {v4 .. v9}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/samsung/gallery/view/ActionBarManager;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoItemUserTag:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    .line 338
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoItemUserTag:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mEditListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->setEditListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;)V

    .line 339
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mListItems:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoItemUserTag:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 342
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;

    new-instance v4, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;

    add-int/lit8 v11, v1, 0x1

    .end local v1    # "index":I
    .restart local v11    # "index":I
    const v5, 0x7f030095

    const v6, 0x7f0201ff

    const v7, 0x7f0e004b

    invoke-direct {v4, v1, v5, v6, v7}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;-><init>(IIII)V

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;Lcom/sec/android/gallery3d/data/MediaItem;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoItemDetails:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;

    .line 345
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mListItems:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoItemDetails:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 347
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mListItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;

    .line 348
    .local v12, "item":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mBodyView:Landroid/widget/LinearLayout;

    invoke-virtual {v12}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 350
    .end local v12    # "item":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;
    :cond_1
    return-void

    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_2
    move v1, v11

    .end local v11    # "index":I
    .restart local v1    # "index":I
    goto/16 :goto_0
.end method

.method private initView()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 290
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v3, :cond_0

    .line 291
    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    const v4, 0x7f110039

    invoke-direct {v1, v3, v4}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 292
    .local v1, "ct":Landroid/view/ContextThemeWrapper;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 296
    .end local v1    # "ct":Landroid/view/ContextThemeWrapper;
    .local v2, "inflater":Landroid/view/LayoutInflater;
    :goto_0
    const v3, 0x7f030094

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;

    .line 297
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4, v5}, Landroid/app/Activity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 299
    const-string v3, "MoreInfo"

    const-string v4, "moreinfo : start"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->startLoadBitmapTask()V

    .line 301
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;

    const v4, 0x7f0f0055

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 302
    .local v0, "container":Landroid/view/View;
    new-instance v3, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$5;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$5;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 308
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->updateLayout()V

    .line 309
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;

    const v4, 0x7f0f0197

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mBodyView:Landroid/widget/LinearLayout;

    .line 310
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->initChildView()V

    .line 311
    return-void

    .line 294
    .end local v0    # "container":Landroid/view/View;
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .restart local v2    # "inflater":Landroid/view/LayoutInflater;
    goto :goto_0
.end method

.method private invalidate()V
    .locals 3

    .prologue
    .line 462
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mListItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;

    .line 463
    .local v1, "item":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->invalidate()V

    goto :goto_0

    .line 465
    .end local v1    # "item":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;
    :cond_0
    return-void
.end method

.method private isDataEdited()Z
    .locals 4

    .prologue
    .line 451
    const/4 v0, 0x0

    .line 452
    .local v0, "dataChanged":Z
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mListItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;

    .line 453
    .local v2, "item":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->dataEdited()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 454
    const/4 v0, 0x1

    .line 458
    .end local v2    # "item":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;
    :cond_1
    return v0
.end method

.method private isEditMode()Z
    .locals 1

    .prologue
    .line 247
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mIsEditMode:Z

    return v0
.end method

.method private isUserTagEditMode()Z
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoItemUserTag:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoItemUserTag:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->isUserTagEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static mixColors([I[IF)I
    .locals 11
    .param p0, "color1"    # [I
    .param p1, "color2"    # [I
    .param p2, "mixRatio"    # F

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v5, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    .line 606
    aget v4, p0, v5

    int-to-float v4, v4

    mul-float/2addr v4, p2

    aget v5, p1, v5

    int-to-float v5, v5

    sub-float v6, v7, p2

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-int v0, v4

    .line 607
    .local v0, "a":I
    aget v4, p0, v8

    int-to-float v4, v4

    mul-float/2addr v4, p2

    aget v5, p1, v8

    int-to-float v5, v5

    sub-float v6, v7, p2

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-int v3, v4

    .line 608
    .local v3, "r":I
    aget v4, p0, v9

    int-to-float v4, v4

    mul-float/2addr v4, p2

    aget v5, p1, v9

    int-to-float v5, v5

    sub-float v6, v7, p2

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-int v2, v4

    .line 609
    .local v2, "g":I
    aget v4, p0, v10

    int-to-float v4, v4

    mul-float/2addr v4, p2

    aget v5, p1, v10

    int-to-float v5, v5

    sub-float v6, v7, p2

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-int v1, v4

    .line 610
    .local v1, "b":I
    invoke-static {v0, v3, v2, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    return v4
.end method

.method private resetEditMode()V
    .locals 3

    .prologue
    .line 468
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mListItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;

    .line 469
    .local v1, "item":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->resetEditMode()V

    goto :goto_0

    .line 471
    .end local v1    # "item":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;
    :cond_0
    return-void
.end method

.method private setEnable(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 232
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mIsEnabled:Z

    if-ne v0, p1, :cond_0

    .line 240
    :goto_0
    return-void

    .line 234
    :cond_0
    if-eqz p1, :cond_1

    .line 235
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->initView()V

    .line 239
    :goto_1
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mIsEnabled:Z

    goto :goto_0

    .line 237
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->onDestroy()V

    goto :goto_1
.end method

.method private startDestroyAnimation()V
    .locals 6

    .prologue
    .line 192
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 193
    .local v3, "parent":Landroid/view/ViewGroup;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mFragmentManager:Landroid/app/FragmentManager;

    const-string v5, "googlemap"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 195
    .local v2, "map":Landroid/app/Fragment;
    if-eqz v2, :cond_0

    .line 196
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    const v5, 0x7f040007

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 202
    .local v0, "animation":Landroid/view/animation/Animation;
    new-instance v4, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$4;

    invoke-direct {v4, p0, v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$4;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;Landroid/view/ViewGroup;)V

    invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 223
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 224
    return-void

    .line 198
    .end local v0    # "animation":Landroid/view/animation/Animation;
    :catch_0
    move-exception v1

    .line 199
    .local v1, "e":Ljava/lang/IllegalStateException;
    const-string v4, "MoreInfo"

    const-string v5, "onDestroy : IllegalState in MoreInfo"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private startLoadBitmapTask()V
    .locals 3

    .prologue
    .line 544
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    .line 545
    .local v0, "threadPool":Lcom/sec/android/gallery3d/util/ThreadPool;
    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$LoadBitmapDataTask;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$LoadBitmapDataTask;-><init>(Lcom/sec/android/gallery3d/data/MediaItem;)V

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$6;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$6;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    .line 559
    return-void
.end method

.method private switchViewMode(Z)Z
    .locals 6
    .param p1, "isEdit"    # Z

    .prologue
    const/4 v3, 0x1

    const/16 v5, 0x500

    .line 265
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mIsEditMode:Z

    if-ne p1, v2, :cond_0

    .line 285
    .end local p1    # "isEdit":Z
    :goto_0
    return p1

    .line 267
    .restart local p1    # "isEdit":Z
    :cond_0
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mIsEditMode:Z

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mIsEditMode:Z

    .line 268
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 269
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mListItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;

    .line 270
    .local v1, "item":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mIsEditMode:Z

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->setMode(Z)V

    goto :goto_2

    .line 267
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 272
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mIsEditMode:Z

    if-eqz v2, :cond_3

    .line 273
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x400

    invoke-virtual {v2, v3, v5}, Landroid/view/Window;->setFlags(II)V

    .line 284
    :goto_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;

    const v3, 0x7f0f0196

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    .line 285
    iget-boolean p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mIsEditMode:Z

    goto :goto_0

    .line 278
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v5, v5}, Landroid/view/Window;->setFlags(II)V

    goto :goto_3
.end method

.method private updateActionBar()V
    .locals 3

    .prologue
    .line 407
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v0

    .line 408
    .local v0, "actionBar":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    instance-of v1, v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoEditActionBar;

    if-eqz v1, :cond_1

    .line 409
    check-cast v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoEditActionBar;

    .end local v0    # "actionBar":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoEditActionBar;->updateLayout()V

    .line 410
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->isDataEdited()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateDoneButton(Z)V

    .line 414
    :cond_0
    :goto_0
    return-void

    .line 411
    .restart local v0    # "actionBar":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    :cond_1
    instance-of v1, v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditActionbar;

    if-eqz v1, :cond_0

    .line 412
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoItemUserTag:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->updateEditViewActionBar()V

    goto :goto_0
.end method


# virtual methods
.method public changeMode([Ljava/lang/Object;)V
    .locals 5
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 432
    aget-object v2, p1, v4

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 433
    .local v1, "targetMode":I
    packed-switch v1, :pswitch_data_0

    .line 441
    :goto_0
    aget-object v2, p1, v3

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 442
    .local v0, "action":I
    if-nez v0, :cond_2

    .line 443
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->actionEditDone()V

    .line 447
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->initActionBar()V

    .line 448
    return-void

    .line 436
    .end local v0    # "action":I
    :pswitch_0
    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    move v2, v3

    :goto_2
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->switchViewMode(Z)Z

    goto :goto_0

    :cond_1
    move v2, v4

    goto :goto_2

    .line 444
    .restart local v0    # "action":I
    :cond_2
    if-ne v0, v3, :cond_0

    .line 445
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->actionEditCancel()V

    goto :goto_1

    .line 433
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public initActionBar()V
    .locals 3

    .prologue
    .line 418
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mIsEditMode:Z

    if-nez v1, :cond_0

    .line 419
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 427
    .local v0, "actionBarView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 428
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->updateActionBar()V

    .line 429
    return-void

    .line 421
    .end local v0    # "actionBarView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->isUserTagEditMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 422
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditActionbar;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .restart local v0    # "actionBarView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    goto :goto_0

    .line 424
    .end local v0    # "actionBarView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    :cond_1
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoEditActionBar;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoEditActionBar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .restart local v0    # "actionBarView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    goto :goto_0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mIsEnabled:Z

    return v0
.end method

.method public isVisiable()Z
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackPressed()Z
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 158
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mIsEditMode:Z

    if-eqz v3, :cond_0

    .line 160
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoItemUserTag:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoItemUserTag:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->isUserTagEditMode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 161
    new-array v0, v4, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    .line 169
    .local v0, "params":[Ljava/lang/Object;
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v3, "MOREINFO_EVENT"

    invoke-virtual {v1, v3, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 171
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mEditListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;

    invoke-interface {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;->onEdit(Z)V

    move v1, v2

    .line 174
    .end local v0    # "params":[Ljava/lang/Object;
    :cond_0
    return v1

    .line 165
    :cond_1
    new-array v0, v4, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    .restart local v0    # "params":[Ljava/lang/Object;
    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->updateLayout()V

    .line 228
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->updateActionBar()V

    .line 229
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mIsEnabled:Z

    if-nez v0, :cond_0

    .line 189
    :goto_0
    return-void

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoItemUserTag:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mIsEditMode:Z

    if-eqz v0, :cond_1

    .line 180
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoItemUserTag:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->resetEditMode()V

    .line 182
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$3;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$3;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public refresh()V
    .locals 1

    .prologue
    .line 614
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoDateItem:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemSimple;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemSimple;->invalidate()V

    .line 615
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoItemUserTag:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    if-eqz v0, :cond_0

    .line 616
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoItemUserTag:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->invalidate()V

    .line 617
    :cond_0
    return-void
.end method

.method public setLatLng(Ljava/lang/Double;Ljava/lang/Double;)V
    .locals 1
    .param p1, "lat"    # Ljava/lang/Double;
    .param p2, "log"    # Ljava/lang/Double;

    .prologue
    .line 494
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mIsEditMode:Z

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mMoreInfoLocationItem:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->setLatLng(Ljava/lang/Double;Ljava/lang/Double;)V

    .line 497
    :cond_0
    return-void
.end method

.method public setVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->isVisiable()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 256
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 258
    :cond_0
    return-void

    .line 256
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public updateLayout()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 353
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 356
    .local v4, "resources":Landroid/content/res/Resources;
    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 357
    .local v0, "config":Landroid/content/res/Configuration;
    if-eqz v0, :cond_2

    iget v8, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_2

    .line 358
    const v8, 0x7f0d0374

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 359
    .local v3, "paddingTop":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;

    invoke-virtual {v8, v10, v3, v10, v10}, Landroid/view/View;->setPadding(IIII)V

    .line 366
    :goto_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;

    const v9, 0x7f0f0196

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 367
    .local v5, "scrollView":Landroid/view/View;
    const v8, 0x7f0d007f

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 368
    .local v1, "paddingLeft":I
    const v8, 0x7f0d007e

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 369
    .local v2, "paddingRight":I
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v8

    if-eqz v8, :cond_0

    if-eqz v0, :cond_0

    iget v8, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_0

    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v8, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 371
    const/16 v1, 0x19

    .line 372
    const/16 v2, 0x19

    .line 374
    :cond_0
    invoke-virtual {v5, v1, v10, v2, v10}, Landroid/view/View;->setPadding(IIII)V

    .line 377
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;

    const v9, 0x7f0f01a8

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 378
    .local v7, "userTagEditView":Landroid/view/View;
    const v8, 0x7f0d009f

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 379
    .local v6, "tagPaddingTop":I
    if-eqz v7, :cond_1

    .line 380
    invoke-virtual {v7, v1, v6, v2, v10}, Landroid/view/View;->setPadding(IIII)V

    .line 382
    :cond_1
    return-void

    .line 361
    .end local v1    # "paddingLeft":I
    .end local v2    # "paddingRight":I
    .end local v3    # "paddingTop":I
    .end local v5    # "scrollView":Landroid/view/View;
    .end local v6    # "tagPaddingTop":I
    .end local v7    # "userTagEditView":Landroid/view/View;
    :cond_2
    const v8, 0x7f0d0375

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 362
    .restart local v3    # "paddingTop":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;

    invoke-virtual {v8, v10, v3, v10, v10}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method
