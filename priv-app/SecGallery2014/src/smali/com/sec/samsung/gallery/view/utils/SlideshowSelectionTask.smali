.class public Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;
.super Landroid/os/AsyncTask;
.source "SlideshowSelectionTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/android/gallery3d/data/MediaSet;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static mIsBurstShot:Z


# instance fields
.field isClusterAlbum:Z

.field private final mComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mFromAlbumView:Z

.field private mIsOldest:Z

.field private mMaxCount:I

.field private mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field private mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

.field private mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

.field private mTaskRunning:Z

.field resultItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mIsBurstShot:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onProgressListener"    # Lcom/sec/android/gallery3d/data/OnProgressListener;
    .param p3, "maxCount"    # I

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 38
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mFromAlbumView:Z

    .line 40
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mIsOldest:Z

    .line 41
    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mMaxCount:I

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mTaskRunning:Z

    .line 44
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$1;-><init>(Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 80
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->isClusterAlbum:Z

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->resultItems:Ljava/util/ArrayList;

    .line 83
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$2;-><init>(Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mComparator:Ljava/util/Comparator;

    .line 54
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mContext:Landroid/content/Context;

    .line 55
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->resultItems:Ljava/util/ArrayList;

    .line 57
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mFromAlbumView:Z

    .line 58
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mIsOldest:Z

    .line 59
    iput p3, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mMaxCount:I

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;ZI)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onProgressListener"    # Lcom/sec/android/gallery3d/data/OnProgressListener;
    .param p3, "fromAlbumView"    # Z
    .param p4, "maxCount"    # I

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 38
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mFromAlbumView:Z

    .line 40
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mIsOldest:Z

    .line 41
    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mMaxCount:I

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mTaskRunning:Z

    .line 44
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$1;-><init>(Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 80
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->isClusterAlbum:Z

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->resultItems:Ljava/util/ArrayList;

    .line 83
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$2;-><init>(Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mComparator:Ljava/util/Comparator;

    .line 63
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mContext:Landroid/content/Context;

    .line 64
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->resultItems:Ljava/util/ArrayList;

    .line 66
    iput-boolean p3, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mFromAlbumView:Z

    .line 67
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mIsOldest:Z

    .line 68
    iput p4, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mMaxCount:I

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;ZZI)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onProgressListener"    # Lcom/sec/android/gallery3d/data/OnProgressListener;
    .param p3, "fromAlbumView"    # Z
    .param p4, "isOldest"    # Z
    .param p5, "maxCount"    # I

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 38
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mFromAlbumView:Z

    .line 40
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mIsOldest:Z

    .line 41
    iput v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mMaxCount:I

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mTaskRunning:Z

    .line 44
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$1;-><init>(Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 80
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->isClusterAlbum:Z

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->resultItems:Ljava/util/ArrayList;

    .line 83
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$2;-><init>(Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mComparator:Ljava/util/Comparator;

    .line 72
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mContext:Landroid/content/Context;

    .line 73
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->resultItems:Ljava/util/ArrayList;

    .line 75
    iput-boolean p3, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mFromAlbumView:Z

    .line 76
    iput-boolean p4, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mIsOldest:Z

    .line 77
    iput p5, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mMaxCount:I

    .line 78
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mTaskRunning:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mTaskRunning:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;)Lcom/sec/android/gallery3d/data/OnProgressListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mIsOldest:Z

    return v0
.end method

.method public static setBurstMode(Z)V
    .locals 0
    .param p0, "value"    # Z

    .prologue
    .line 193
    sput-boolean p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mIsBurstShot:Z

    .line 194
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 34
    check-cast p1, [Lcom/sec/android/gallery3d/data/MediaSet;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->doInBackground([Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/Void;
    .locals 4
    .param p1, "arg"    # [Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const/4 v3, 0x0

    .line 96
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSlideShowSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    .line 97
    .local v1, "selectionProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    aget-object v0, p1, v3

    .line 99
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    sget-boolean v2, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mIsBurstShot:Z

    if-nez v2, :cond_1

    .line 104
    instance-of v2, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    if-nez v2, :cond_0

    instance-of v2, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    if-eqz v2, :cond_2

    .line 105
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->isClusterAlbum:Z

    .line 109
    :goto_0
    new-instance v2, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$3;

    invoke-direct {v2, p0, v1}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$3;-><init>(Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/MediaSet;->enumerateTotalMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V

    .line 149
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->resultItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalSelectedItems()I

    move-result v2

    if-nez v2, :cond_1

    .line 150
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->resultItems:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mComparator:Ljava/util/Comparator;

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 151
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->resultItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Ljava/util/List;)V

    .line 154
    :cond_1
    const/4 v2, 0x0

    return-object v2

    .line 107
    :cond_2
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->isClusterAlbum:Z

    goto :goto_0
.end method

.method public increaseProgress(JZ)V
    .locals 3
    .param p1, "step"    # J
    .param p3, "isFileSize"    # Z

    .prologue
    .line 197
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->increaseProgress(JZZ)V

    .line 198
    return-void
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->closeCustomProgressDialog()V

    .line 160
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 161
    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 34
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->onCancelled(Ljava/lang/Void;)V

    return-void
.end method

.method protected onCancelled(Ljava/lang/Void;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->closeCustomProgressDialog()V

    .line 166
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onCancelled(Ljava/lang/Object;)V

    .line 167
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 34
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const/4 v2, 0x0

    .line 171
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/data/OnProgressListener;->onCompleted(Z)V

    .line 172
    sget-boolean v0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mIsBurstShot:Z

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->closeCustomProgressDialog(I)V

    .line 174
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->setBurstMode(Z)V

    .line 178
    :goto_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 179
    return-void

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->closeCustomProgressDialog(I)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 5

    .prologue
    .line 183
    new-instance v0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    .line 184
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mMaxCount:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalCount(I)V

    .line 186
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mMaxCount:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalSize(J)V

    .line 187
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mContext:Landroid/content/Context;

    const v2, 0x7f0e0032

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->showProgressDialog(Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 189
    :cond_0
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 190
    return-void
.end method
