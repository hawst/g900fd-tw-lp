.class Lcom/sec/samsung/gallery/glview/GlButtonView$1;
.super Lcom/sec/android/gallery3d/glcore/GlHandler;
.source "GlButtonView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/glview/GlButtonView;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlRootView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlButtonView;Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/glcore/GlHandler;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    return-void
.end method


# virtual methods
.method public onMessage(ILjava/lang/Object;III)V
    .locals 4
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    .line 56
    const/16 v0, 0x64

    if-ne p1, v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlButtonView;->mNormalDrawableId:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlButtonView;->access$000(Lcom/sec/samsung/gallery/glview/GlButtonView;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlButtonView;->mResMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlButtonView;->mResources:Landroid/content/res/Resources;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlButtonView;->access$100(Lcom/sec/samsung/gallery/glview/GlButtonView;)Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlButtonView;->mNormalDrawableId:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/GlButtonView;->access$000(Lcom/sec/samsung/gallery/glview/GlButtonView;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlButtonView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlButtonView;->mPressedDrawableId:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlButtonView;->access$200(Lcom/sec/samsung/gallery/glview/GlButtonView;)I

    move-result v0

    if-eqz v0, :cond_2

    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlButtonView;->mResMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlButtonView;->mResources:Landroid/content/res/Resources;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlButtonView;->access$300(Lcom/sec/samsung/gallery/glview/GlButtonView;)Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlButtonView;->mPressedDrawableId:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/GlButtonView;->access$200(Lcom/sec/samsung/gallery/glview/GlButtonView;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlButtonView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 64
    :cond_2
    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    # invokes: Lcom/sec/samsung/gallery/glview/GlButtonView;->playSoundAndHapticFeedback()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlButtonView;->access$400(Lcom/sec/samsung/gallery/glview/GlButtonView;)V

    goto :goto_0
.end method
