.class Lcom/sec/samsung/gallery/glview/GlGridView$10;
.super Ljava/lang/Object;
.source "GlGridView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGridView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V
    .locals 0

    .prologue
    .line 1054
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$10;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 6
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1056
    move-object v1, p1

    check-cast v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget v0, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 1058
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$10;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mClickable:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$800(Lcom/sec/samsung/gallery/glview/GlGridView;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1070
    :cond_0
    :goto_0
    return v4

    .line 1059
    :cond_1
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReOrderAlbums:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$10;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mIsDragEnabled:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$900(Lcom/sec/samsung/gallery/glview/GlGridView;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1060
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$10;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$600(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->resetDragInfo()V

    .line 1061
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$10;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$600(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->startDragAnim(Lcom/sec/android/gallery3d/glcore/GlObject;II)V

    goto :goto_0

    .line 1065
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$10;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnItemLongClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;

    if-eqz v1, :cond_0

    .line 1066
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$10;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$502(Lcom/sec/samsung/gallery/glview/GlGridView;I)I

    .line 1067
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$10;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnItemLongClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$10;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlGridView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;->onItemLongClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 1068
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$10;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v0, v5, v5}, Lcom/sec/samsung/gallery/glview/GlGridView;->setCommand(IIII)V

    goto :goto_0
.end method
