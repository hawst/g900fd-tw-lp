.class Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;
.super Ljava/lang/Object;
.source "StartFaceTagCmd.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->requestRemoveConfirmTag(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;

.field final synthetic val$action:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

.field final synthetic val$clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field final synthetic val$mediaSets:Ljava/util/ArrayList;

.field final synthetic val$parentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field final synthetic val$selectedPaths:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;

    iput-object p2, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iput-object p3, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$action:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    iput-object p4, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$mediaSets:Ljava/util/ArrayList;

    iput-object p5, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$selectedPaths:Ljava/util/ArrayList;

    iput-object p6, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$parentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 210
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_2

    .line 211
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$action:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    sget-object v1, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;->REMOVE_TAG:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    if-ne v0, v1, :cond_1

    .line 212
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;

    iget-object v1, v1, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$mediaSets:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->removeClusterName(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;)V

    .line 214
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "EXIT_SELECTION_MODE"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 216
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$action:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    sget-object v1, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;->CONFIRM_TAG:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    if-ne v0, v1, :cond_0

    .line 217
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;

    iget-object v1, v1, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$mediaSets:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->confirmClusterName(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;)V

    .line 219
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "EXIT_SELECTION_MODE"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 222
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$action:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    sget-object v1, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;->REMOVE_TAG:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    if-ne v0, v1, :cond_3

    .line 224
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;

    iget-object v1, v1, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$selectedPaths:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$parentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->updateOperation(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;ILjava/lang/String;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 227
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "EXIT_SELECTION_MODE"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 229
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    .line 230
    .local v6, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    invoke-virtual {v6, v3}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    goto :goto_0

    .line 231
    .end local v6    # "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$action:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    sget-object v1, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;->CONFIRM_TAG:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    if-ne v0, v1, :cond_0

    .line 232
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;

    iget-object v1, v1, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$selectedPaths:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->val$parentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    move v3, v7

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->updateOperation(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;ILjava/lang/String;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 235
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "EXIT_SELECTION_MODE"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
