.class public Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlComposeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TiltAnimation"
.end annotation


# instance fields
.field private mEnd:F

.field private mStart:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 2

    .prologue
    .line 1852
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 1853
    invoke-virtual {p1, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 1854
    const-wide/16 v0, 0x12c

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;->setDuration(J)V

    .line 1855
    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 9
    .param p1, "ratio"    # F

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    .line 1864
    sub-float v5, v8, p1

    sub-float v6, v8, p1

    mul-float/2addr v5, v6

    sub-float v3, v8, v5

    .line 1866
    .local v3, "proRatio":F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;->mStart:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;->mEnd:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;->mStart:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, v3

    add-float v4, v5, v6

    .line 1867
    .local v4, "scale":F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;->mStart:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;->mEnd:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_0

    sub-float v0, v8, v3

    .line 1868
    .local v0, "adjustedRatio":F
    :goto_0
    sget v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->QUICK_ROOT_Y:F

    mul-float v1, v5, v0

    .line 1869
    .local v1, "centerY":F
    sget v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->QUICK_ROOT_Z:F

    mul-float v2, v5, v0

    .line 1871
    .local v2, "centerZ":F
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    neg-float v6, v4

    sget v7, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->QUICK_ROOT_PITCH_RATIO:F

    div-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPitch(F)V

    .line 1872
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCenter(FFF)V

    .line 1873
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    invoke-virtual {v5, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->setDefaultPitch(F)V

    .line 1874
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    invoke-virtual {v5, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->setDefaultPitch(F)V

    .line 1875
    return-void

    .end local v0    # "adjustedRatio":F
    .end local v1    # "centerY":F
    .end local v2    # "centerZ":F
    :cond_0
    move v0, v3

    .line 1867
    goto :goto_0
.end method

.method public startScale(FF)V
    .locals 0
    .param p1, "start"    # F
    .param p2, "end"    # F

    .prologue
    .line 1858
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;->mStart:F

    .line 1859
    iput p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;->mEnd:F

    .line 1860
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;->start()V

    .line 1861
    return-void
.end method
