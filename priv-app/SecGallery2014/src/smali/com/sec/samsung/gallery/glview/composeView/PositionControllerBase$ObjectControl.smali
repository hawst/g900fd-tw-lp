.class public Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;
.super Ljava/lang/Object;
.source "PositionControllerBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ObjectControl"
.end annotation


# static fields
.field protected static final DEFAULT_COUNT:I = 0x20

.field protected static final THM_VISIBITY_LVL:I = 0x1


# instance fields
.field protected mActiveObject:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;",
            ">;"
        }
    .end annotation
.end field

.field protected mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

.field protected mCount:I

.field protected mGlObject:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

.field protected mInactiveObject:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;",
            ">;"
        }
    .end annotation
.end field

.field protected mPitch:F

.field protected mValidCount:I

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 2
    .param p2, "compView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .prologue
    .line 1535
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1528
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    .line 1529
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mInactiveObject:Ljava/util/ArrayList;

    .line 1533
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mPitch:F

    .line 1536
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .line 1537
    iget-object v0, p2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    .line 1538
    .local v0, "viewConfig":Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;
    iget v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mMaxObject:I

    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mCount:I

    .line 1539
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->createRectangles()[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mGlObject:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 1540
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->clearActiveObject()V

    .line 1541
    return-void
.end method

.method private createRectangles()[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .locals 5

    .prologue
    .line 1547
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mCount:I

    new-array v1, v3, [Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 1548
    .local v1, "glThumbSet":[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    const/16 v3, 0x20

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mCount:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mValidCount:I

    .line 1549
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mValidCount:I

    if-ge v2, v3, :cond_0

    .line 1550
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {v0, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;-><init>(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    .line 1551
    .local v0, "glThumb":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    aput-object v0, v1, v2

    .line 1552
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setObjectListeners(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;I)V

    .line 1549
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1554
    .end local v0    # "glThumb":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_0
    return-object v1
.end method


# virtual methods
.method public activateObject(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .locals 7
    .param p1, "index"    # I

    .prologue
    .line 1577
    const/4 v2, 0x0

    .line 1579
    .local v2, "object":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mInactiveObject:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1580
    new-instance v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {v3, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;-><init>(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1581
    .end local v2    # "object":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .local v3, "object":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :try_start_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mGlObject:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mValidCount:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mValidCount:I

    aput-object v3, v4, v5

    .line 1582
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setObjectListeners(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_2

    move-object v2, v3

    .line 1587
    .end local v3    # "object":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .restart local v2    # "object":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :goto_0
    :try_start_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v4, p1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1588
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mPitch:F

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setPitch(F)V

    .line 1589
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setEmptyFill(Z)V

    .line 1590
    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setVisibility(ZI)V

    .line 1591
    iget-object v4, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 1592
    iget-object v4, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 1593
    iput p1, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    .line 1594
    iget-object v4, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mAlbumTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v4, :cond_0

    .line 1595
    iget-object v4, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mAlbumTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iput p1, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 1602
    :cond_0
    :goto_1
    return-object v2

    .line 1584
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mInactiveObject:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    move-object v2, v0

    .line 1585
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mInactiveObject:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 1597
    :catch_0
    move-exception v1

    .line 1598
    .local v1, "e":Ljava/lang/NullPointerException;
    :goto_2
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 1599
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .line 1600
    .local v1, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :goto_3
    invoke-virtual {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1

    .line 1599
    .end local v1    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    .end local v2    # "object":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .restart local v3    # "object":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :catch_2
    move-exception v1

    move-object v2, v3

    .end local v3    # "object":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .restart local v2    # "object":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    goto :goto_3

    .line 1597
    .end local v2    # "object":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .restart local v3    # "object":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :catch_3
    move-exception v1

    move-object v2, v3

    .end local v3    # "object":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .restart local v2    # "object":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    goto :goto_2
.end method

.method public clearActiveObject()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 1561
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    .line 1562
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mInactiveObject:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1563
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mCount:I

    if-ge v1, v2, :cond_2

    .line 1564
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mGlObject:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    aget-object v0, v2, v1

    .line 1565
    .local v0, "glObject":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-nez v0, :cond_0

    .line 1563
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1567
    :cond_0
    iput v4, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    .line 1568
    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mAlbumTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v2, :cond_1

    .line 1569
    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mAlbumTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iput v4, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 1571
    :cond_1
    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setVisibility(ZI)V

    .line 1572
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mInactiveObject:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1574
    .end local v0    # "glObject":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_2
    return-void
.end method

.method public inActivateObject(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V
    .locals 4
    .param p1, "object"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1606
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 1607
    # getter for: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->access$1000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "activeObject is already empty"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1618
    :goto_0
    return-void

    .line 1610
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget v1, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 1611
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mInactiveObject:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1612
    const/4 v0, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setVisibility(ZI)V

    .line 1613
    iget-object v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 1614
    iget-object v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 1615
    invoke-virtual {p1, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 1616
    invoke-virtual {p1, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setDecorState(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 1617
    const/4 v0, -0x1

    iput v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    goto :goto_0
.end method

.method public setDefaultPitch(F)V
    .locals 4
    .param p1, "pitch"    # F

    .prologue
    .line 1621
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mPitch:F

    cmpl-float v3, v3, p1

    if-nez v3, :cond_1

    .line 1631
    :cond_0
    return-void

    .line 1624
    :cond_1
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mPitch:F

    .line 1626
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v1

    .line 1627
    .local v1, "itemObjSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 1628
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 1629
    .local v2, "object":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setPitch(F)V

    .line 1627
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
