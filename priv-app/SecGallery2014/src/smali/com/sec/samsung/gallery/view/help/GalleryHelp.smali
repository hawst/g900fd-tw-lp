.class public Lcom/sec/samsung/gallery/view/help/GalleryHelp;
.super Ljava/lang/Object;
.source "GalleryHelp.java"


# static fields
.field public static final AIR_BUTTON:Ljava/lang/String; = "AIR_BUTTON"

.field private static final AUTHORITY:Ljava/lang/String; = "com.sec.android.galleryhelp.provider"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final EDIT_MULTIPLE_PICTURES:Ljava/lang/String; = "EDIT_MULTIPLE_PICTURES"

.field public static final EDIT_PICTURES:Ljava/lang/String; = "EDIT_PICTURES"

.field public static final FACE_TAGGING:Ljava/lang/String; = "FACE_TAGGING"

.field public static final HELP_AIR_BUTTON:I = 0x4

.field public static final HELP_EDIT_MULTIPLE_PICTURES:I = 0x8

.field public static final HELP_EDIT_PICTURES:I = 0x7

.field public static final HELP_FACE_TAGGING:I = 0x2

.field public static final HELP_ICON_LABELS:I = 0x6

.field public static final HELP_INFORMATION_PREVIEW:I = 0x3

.field public static final HELP_LIST_SCROLLING:I = 0x5

.field public static final HELP_MODE:Ljava/lang/String; = "HelpMode"

.field public static final HELP_NONE:I = 0x0

.field private static final HELP_RESOURCE_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.helpplugin"

.field public static final HELP_VIEWING_PICTURES:I = 0x1

.field public static final ICON_LABELS:Ljava/lang/String; = "ICON_LABELS"

.field public static final INFORMATION_PREVIEW:Ljava/lang/String; = "INFORMATION_PREVIEW"

.field public static final LIST_SCROLLING:Ljava/lang/String; = "AIR_VIEW_LIST_SCROLLING"

.field private static final PREFIX_HAS_DOWNLOADABLE_HELP:Ljava/lang/String; = "downloadable-help-has-"

.field private static final PREFIX_UPDATE_DOWNLOADABLE_HELP:Ljava/lang/String; = "downloadable-help-update-"

.field public static final STORYALBUM_PICKER:Ljava/lang/String; = "help_mode"

.field public static final STORYALBUM_PICKER_FINISH:Ljava/lang/String; = "noitem_finish"

.field private static final TAG:Ljava/lang/String; = "GalleryHelp"

.field public static final VIEWING_PICTURES:Ljava/lang/String; = "VIEWING_PICTURES"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-string v0, "content://com.sec.android.galleryhelp.provider/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/help/GalleryHelp;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    return-void
.end method

.method public static finishHelpPicker(Landroid/app/Activity;)V
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 103
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 104
    .local v0, "responseIntent":Landroid/content/Intent;
    const-string v1, "help_mode"

    const-string v2, "noitem_finish"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 105
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 106
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 107
    return-void
.end method

.method public static getHelpDrawable(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resourceName"    # Ljava/lang/String;

    .prologue
    .line 162
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/help/GalleryHelp;->getHelpResources(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v0

    .line 163
    .local v0, "res":Landroid/content/res/Resources;
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 164
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/view/help/GalleryHelp;->getHelpDrawable(Landroid/content/res/Resources;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 166
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getHelpDrawable(Landroid/content/res/Resources;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "resourceName"    # Ljava/lang/String;

    .prologue
    .line 149
    const/4 v0, 0x0

    .line 150
    .local v0, "id":I
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 151
    const-string v1, "drawable"

    const-string v2, "com.samsung.helpplugin"

    invoke-virtual {p0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 152
    if-eqz v0, :cond_0

    .line 153
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 157
    :goto_0
    return-object v1

    .line 156
    :cond_0
    const-string v1, "GalleryHelp"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getHelpDrawable : not find resource!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getHelpResources(Landroid/content/Context;)Landroid/content/res/Resources;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 135
    const/4 v2, 0x0

    .line 136
    .local v2, "resources":Landroid/content/res/Resources;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 138
    .local v1, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v3, "com.samsung.helpplugin"

    invoke-virtual {v1, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 144
    :goto_0
    return-object v2

    .line 139
    :catch_0
    move-exception v0

    .line 141
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getHelpType(Ljava/lang/String;)I
    .locals 2
    .param p0, "mode"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 72
    if-nez p0, :cond_1

    .line 99
    :cond_0
    :goto_0
    return v0

    .line 75
    :cond_1
    const-string v1, "VIEWING_PICTURES"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 76
    const/4 v0, 0x1

    goto :goto_0

    .line 78
    :cond_2
    const-string v1, "FACE_TAGGING"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 79
    const/4 v0, 0x2

    goto :goto_0

    .line 81
    :cond_3
    const-string v1, "INFORMATION_PREVIEW"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 82
    const/4 v0, 0x3

    goto :goto_0

    .line 84
    :cond_4
    const-string v1, "AIR_BUTTON"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 85
    const/4 v0, 0x4

    goto :goto_0

    .line 87
    :cond_5
    const-string v1, "AIR_VIEW_LIST_SCROLLING"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 88
    const/4 v0, 0x5

    goto :goto_0

    .line 90
    :cond_6
    const-string v1, "ICON_LABELS"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    const/4 v0, 0x6

    goto :goto_0
.end method

.method public static isAvailableDownloadableHelp(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 114
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/PackagesMonitor;->getPackagesVersion(Landroid/content/Context;)I

    move-result v3

    .line 115
    .local v3, "version":I
    const-string v6, "downloadable-help-update-"

    invoke-static {p0, v6, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v2

    .line 117
    .local v2, "updateKey":I
    if-eq v2, v3, :cond_3

    .line 118
    const/4 v1, 0x0

    .line 120
    .local v1, "info":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "com.samsung.helpplugin"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 121
    if-eqz v1, :cond_1

    .line 126
    const-string v6, "downloadable-help-update-"

    invoke-static {p0, v6, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 127
    const-string v6, "downloadable-help-has-"

    if-eqz v1, :cond_0

    move v4, v5

    :cond_0
    invoke-static {p0, v6, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 130
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    :goto_0
    return v5

    .line 126
    .restart local v1    # "info":Landroid/content/pm/PackageInfo;
    :cond_1
    const-string v6, "downloadable-help-update-"

    invoke-static {p0, v6, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 127
    const-string v6, "downloadable-help-has-"

    if-eqz v1, :cond_2

    move v4, v5

    :cond_2
    invoke-static {p0, v6, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 130
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    :cond_3
    :goto_1
    const-string v4, "downloadable-help-has-"

    invoke-static {p0, v4, v5}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v5

    goto :goto_0

    .line 123
    .restart local v1    # "info":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v0

    .line 124
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    const-string v6, "GalleryHelp"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadableHelp res is not exist. "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 126
    const-string v6, "downloadable-help-update-"

    invoke-static {p0, v6, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 127
    const-string v6, "downloadable-help-has-"

    if-eqz v1, :cond_4

    move v4, v5

    :cond_4
    invoke-static {p0, v6, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_1

    .line 126
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_0
    move-exception v6

    const-string v7, "downloadable-help-update-"

    invoke-static {p0, v7, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 127
    const-string v7, "downloadable-help-has-"

    if-eqz v1, :cond_5

    :goto_2
    invoke-static {p0, v7, v5}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    throw v6

    :cond_5
    move v5, v4

    goto :goto_2
.end method

.method public static requestDownloadingResource(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 170
    move-object v0, p0

    .line 172
    .local v0, "c":Landroid/content/Context;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0e021b

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e033f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lcom/sec/samsung/gallery/view/help/GalleryHelp$1;

    invoke-direct {v3, v0}, Lcom/sec/samsung/gallery/view/help/GalleryHelp$1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 192
    return-void
.end method

.method public static requestDownloadingResourceForChn(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 194
    move-object v0, p0

    .line 196
    .local v0, "c":Landroid/content/Context;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0e021b

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e0340

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lcom/sec/samsung/gallery/view/help/GalleryHelp$2;

    invoke-direct {v3, v0}, Lcom/sec/samsung/gallery/view/help/GalleryHelp$2;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 216
    return-void
.end method
