.class Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$1;
.super Ljava/lang/Object;
.source "FaceTagFeature.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 92
    const-string v1, "FaceTagFeature"

    const-string v2, "Emailservice onServiceConnected Called !!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    invoke-static {p2}, Lcom/android/email/backgroundsender/IEmailBackgroundSender$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    move-result-object v2

    # setter for: Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mEmailServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->access$002(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;Lcom/android/email/backgroundsender/IEmailBackgroundSender;)Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    .line 96
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mEmailServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->access$000(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 97
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mEmailServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->access$000(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/email/backgroundsender/IEmailBackgroundSender;->startListening()V

    .line 98
    const-string v1, "FaceTagFeature"

    const-string v2, "Start email status listening"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->access$100(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 101
    :catch_0
    move-exception v0

    .line 102
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 108
    const-string v1, "FaceTagFeature"

    const-string v2, "Email service onServiceDisconnected Called !!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mEmailServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->access$000(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 112
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mEmailServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->access$000(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/email/backgroundsender/IEmailBackgroundSender;->stopListening()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    const/4 v2, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mEmailServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->access$002(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;Lcom/android/email/backgroundsender/IEmailBackgroundSender;)Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    .line 119
    return-void

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
