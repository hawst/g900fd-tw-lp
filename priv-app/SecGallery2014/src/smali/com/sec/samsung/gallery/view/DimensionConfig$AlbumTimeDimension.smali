.class public Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;
.super Ljava/lang/Object;
.source "DimensionConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/DimensionConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlbumTimeDimension"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;
    }
.end annotation


# static fields
.field public static final FOLDER_TYPE:I = 0x5

.field public static final MODE_CHILD_COUNT:[I

.field private static sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;


# instance fields
.field public album_time_VI_degree:[I

.field public album_time_count_font_color:I

.field public album_time_count_font_size:I

.field public album_time_font_color:I

.field public album_time_font_size:I

.field public album_time_type:[I

.field public album_time_view_gap:[I

.field public mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 561
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->MODE_CHILD_COUNT:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x3
        0x4
        0x5
        0x6
    .end array-data
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x4

    const/4 v5, 0x0

    const/4 v6, 0x2

    .line 588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 590
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 591
    .local v2, "r":Landroid/content/res/Resources;
    const v3, 0x7f0c006e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->album_time_font_color:I

    .line 592
    const v3, 0x7f0c006f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->album_time_count_font_color:I

    .line 594
    const v3, 0x7f0d038f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->album_time_font_size:I

    .line 595
    const v3, 0x7f0d0390

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->album_time_count_font_size:I

    .line 597
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->album_time_VI_degree:[I

    .line 598
    const v3, 0x7f090072

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->album_time_VI_degree:[I

    invoke-static {v3, v5, v4, v5, v6}, Ljava/lang/System;->arraycopy([II[III)V

    .line 600
    new-array v3, v7, [I

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->album_time_view_gap:[I

    .line 602
    const v3, 0x7f090088

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 604
    .local v0, "gap":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_0

    if-ge v1, v7, :cond_0

    .line 605
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->album_time_view_gap:[I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v4

    aput v4, v3, v1

    .line 604
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 607
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 609
    new-array v3, v6, [Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    .line 610
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v6, :cond_1

    .line 611
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    new-instance v4, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    invoke-direct {v4}, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;-><init>()V

    aput-object v4, v3, v1

    .line 612
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    aget-object v3, v3, v1

    const v4, 0x7f09006e

    const/16 v5, 0xa

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getFullArraryFromTypeArray(Landroid/content/res/Resources;III)[I
    invoke-static {v2, v4, v1, v5}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$100(Landroid/content/res/Resources;III)[I

    move-result-object v4

    iput-object v4, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;->album_time_screen:[I

    .line 613
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    aget-object v3, v3, v1

    const v4, 0x7f09006f

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getFullArraryFromTypeArray(Landroid/content/res/Resources;III)[I
    invoke-static {v2, v4, v1, v7}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$100(Landroid/content/res/Resources;III)[I

    move-result-object v4

    iput-object v4, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;->album_time_screen_padding:[I

    .line 614
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    aget-object v3, v3, v1

    const v4, 0x7f090070

    const/4 v5, 0x7

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getFullArraryFromTypeArray(Landroid/content/res/Resources;III)[I
    invoke-static {v2, v4, v1, v5}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$100(Landroid/content/res/Resources;III)[I

    move-result-object v4

    iput-object v4, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;->album_time_label_padding:[I

    .line 615
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    aget-object v3, v3, v1

    const v4, 0x7f090071

    const/4 v5, 0x5

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->get2DFullArraryFromTypeArray(Landroid/content/res/Resources;III)[[I
    invoke-static {v2, v4, v1, v5}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$200(Landroid/content/res/Resources;III)[[I

    move-result-object v4

    iput-object v4, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;->album_time_child_type:[[I

    .line 610
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 618
    :cond_1
    return-void
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 582
    const-class v1, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    if-nez v0, :cond_0

    .line 583
    new-instance v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    .line 585
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 582
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
