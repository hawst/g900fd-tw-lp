.class public Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;
.super Ljava/lang/Object;
.source "DimensionConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/DimensionConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EventViewDimension"
.end annotation


# static fields
.field private static sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;


# instance fields
.field public event_view_sub_title_font_color:I

.field public event_view_sub_title_font_size:I

.field public event_view_sub_title_height:I

.field public event_view_suggestion_font_color:I

.field public event_view_suggestion_font_size:I

.field public event_view_suggestion_height:I

.field public event_view_suggestion_left_padding:I

.field public event_view_suggestion_right_margin:I

.field public event_view_suggestion_top_margin:I

.field public event_view_title_font_color:I

.field public event_view_title_font_size:I

.field public event_view_title_height:I

.field public event_view_title_left_margin:I

.field public event_view_title_padding_for_ssfont:I

.field public event_view_title_top_margin:I

.field public event_view_video_icon_size:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 411
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 413
    .local v0, "r":Landroid/content/res/Resources;
    const v1, 0x7f0c004e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_title_font_color:I

    .line 414
    const v1, 0x7f0c004f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_sub_title_font_color:I

    .line 415
    const v1, 0x7f0c0050

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_suggestion_font_color:I

    .line 416
    const v1, 0x7f0d02a2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_title_font_size:I

    .line 417
    const v1, 0x7f0d02a3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_sub_title_font_size:I

    .line 418
    const v1, 0x7f0d02a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_suggestion_font_size:I

    .line 420
    const v1, 0x7f0d02a6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_title_height:I

    .line 421
    const v1, 0x7f0d02a7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_sub_title_height:I

    .line 422
    const v1, 0x7f0d02a8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_title_left_margin:I

    .line 423
    const v1, 0x7f0d02a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_title_top_margin:I

    .line 424
    const v1, 0x7f0d02aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_suggestion_height:I

    .line 425
    const v1, 0x7f0d02ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_suggestion_top_margin:I

    .line 426
    const v1, 0x7f0d02ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_suggestion_right_margin:I

    .line 427
    const v1, 0x7f0d02a5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_suggestion_left_padding:I

    .line 428
    const v1, 0x7f0d02ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_video_icon_size:I

    .line 429
    const v1, 0x7f0d02af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_title_padding_for_ssfont:I

    .line 430
    return-void
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 404
    const-class v1, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    if-nez v0, :cond_0

    .line 405
    new-instance v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    .line 407
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 404
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
