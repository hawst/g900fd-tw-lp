.class public Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;
.super Lcom/sec/samsung/gallery/view/ViewObservable;
.source "EnhancedAssistantMenu.java"


# static fields
.field public static final ACTION_CAMERA:Ljava/lang/String; = "android.intent.action.camera"

.field public static final ACTION_DELETE:Ljava/lang/String; = "android.intent.action.delete"

.field public static final ACTION_SLIDESHOW:Ljava/lang/String; = "android.intent.action.slideshow"

.field private static final ACTIVITY_NAME:Ljava/lang/String; = "ActivityName"

.field private static final ASSISTANT_MENU_ON:I = 0x1

.field private static final ICON_NAME:Ljava/lang/String; = "IconName"

.field private static final MENU_CAMERA:Ljava/lang/String; = "Camera"

.field private static final MENU_DELETE:Ljava/lang/String; = "Delete"

.field private static final MENU_DRAWER:Ljava/lang/String; = "Drawer"

.field private static final MENU_SLIDESHOW:Ljava/lang/String; = "Slideshow"

.field private static final REGISTER:Ljava/lang/String; = "register"

.field private static final TAG:Ljava/lang/String; = "EnhancedAssistantMenu"


# instance fields
.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;

.field private mRegistered:Z

.field private mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/GalleryActivity;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/ViewObservable;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mRegistered:Z

    .line 43
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;

    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 45
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 46
    return-void
.end method

.method private getIntentFilter()Landroid/content/IntentFilter;
    .locals 3

    .prologue
    .line 97
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 98
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    .line 100
    .local v1, "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v2, v1, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-eqz v2, :cond_2

    .line 101
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inDeleteSelectionMode()Z

    move-result v2

    if-nez v2, :cond_1

    .line 102
    :cond_0
    const-string v2, "android.intent.action.slideshow"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 103
    const-string v2, "android.intent.action.camera"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 113
    :cond_1
    :goto_0
    return-object v0

    .line 105
    :cond_2
    instance-of v2, v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v2, :cond_4

    .line 106
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inExpansionMode()Z

    move-result v2

    if-nez v2, :cond_1

    .line 107
    :cond_3
    const-string v2, "android.intent.action.slideshow"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 108
    const-string v2, "android.intent.action.delete"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto :goto_0

    .line 110
    :cond_4
    instance-of v2, v1, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    if-eqz v2, :cond_1

    .line 111
    const-string v2, "android.intent.action.camera"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getMenuList()Ljava/lang/String;
    .locals 2

    .prologue
    .line 83
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 84
    .local v0, "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v1, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-eqz v1, :cond_0

    .line 85
    const-string v1, "Camera;Slideshow;"

    .line 93
    :goto_0
    return-object v1

    .line 86
    :cond_0
    instance-of v1, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inExpansionMode()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 87
    const-string v1, "Delete;Slideshow;"

    goto :goto_0

    .line 88
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 89
    const-string v1, "Delete;"

    goto :goto_0

    .line 90
    :cond_3
    instance-of v1, v0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    if-eqz v1, :cond_4

    .line 91
    const-string v1, "Camera;"

    goto :goto_0

    .line 93
    :cond_4
    const-string v1, ";"

    goto :goto_0
.end method

.method private isEnabledAssistatnMenu(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 117
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "assistant_menu"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v0, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public registerEAM(Landroid/content/BroadcastReceiver;)V
    .locals 5
    .param p1, "receiver"    # Landroid/content/BroadcastReceiver;

    .prologue
    const/4 v4, 0x1

    .line 49
    const-string v1, "EnhancedAssistantMenu"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Register the EAM Receiver mRegistered : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mRegistered:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->isEnabledAssistatnMenu(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mRegistered:Z

    if-eqz v1, :cond_1

    .line 62
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 55
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "ActivityName"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v1, "IconName"

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->getMenuList()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string v1, "register"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 58
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1, v0}, Landroid/view/accessibility/AccessibilityManager;->assistantMenuUpdate(Landroid/os/Bundle;)V

    .line 60
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->getIntentFilter()Landroid/content/IntentFilter;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/gallery3d/app/GalleryActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 61
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mRegistered:Z

    goto :goto_0
.end method

.method public unregisterEAM(Landroid/content/BroadcastReceiver;)V
    .locals 5
    .param p1, "receiver"    # Landroid/content/BroadcastReceiver;

    .prologue
    const/4 v4, 0x0

    .line 65
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->isEnabledAssistatnMenu(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 80
    :goto_0
    return-void

    .line 68
    :cond_0
    const-string v2, "EnhancedAssistantMenu"

    const-string v3, "Unregister the EAM Receiver"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 76
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "ActivityName"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v2, "register"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 78
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2, v0}, Landroid/view/accessibility/AccessibilityManager;->assistantMenuUpdate(Landroid/os/Bundle;)V

    .line 79
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->mRegistered:Z

    goto :goto_0

    .line 71
    .end local v0    # "bundle":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 72
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "EnhancedAssistantMenu"

    const-string v3, "IllegalArgumentException : unregiseter EAM"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
