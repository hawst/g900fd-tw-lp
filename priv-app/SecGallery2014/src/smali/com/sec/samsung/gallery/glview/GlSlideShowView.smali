.class public Lcom/sec/samsung/gallery/glview/GlSlideShowView;
.super Lcom/sec/samsung/gallery/glview/GlAbsListView;
.source "GlSlideShowView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/GlSlideShowView$OnLoadingListener;
    }
.end annotation


# static fields
.field private static final CMD_INITIAL_LOAD:I = 0x1

.field private static final CMD_LOAD_NEXT:I = 0x2

.field public static final EFFECT_DROP:I = 0x3

.field public static final EFFECT_FADE:I = 0x1

.field public static final EFFECT_FLOW:I = 0x0

.field public static final EFFECT_PERSPECTIVE_SHUFFLE:I = 0x4

.field public static final EFFECT_ZOOM_IN:I = 0x2

.field private static final MSG_START_ANIMATION:I = 0x1


# instance fields
.field public mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

.field private final mHandler:Landroid/os/Handler;

.field private mIsBurstPlay:Z

.field private mIsShowing:Z

.field private mOnLoadingListener:Lcom/sec/samsung/gallery/glview/GlSlideShowView$OnLoadingListener;

.field private mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

.field private mStartTime:J

.field private final progressDialog:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemPos"    # I
    .param p3, "type"    # I

    .prologue
    const/16 v3, 0x5dc

    const/16 v2, 0x3e8

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 62
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;-><init>(Landroid/content/Context;)V

    .line 48
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    .line 50
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mIsBurstPlay:Z

    .line 55
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mIsShowing:Z

    .line 63
    packed-switch p3, :pswitch_data_0

    .line 81
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    invoke-virtual {v0, p2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->setStartIndex(I)V

    .line 83
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->progressDialog:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .line 85
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlSlideShowView$1;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/glview/GlSlideShowView$1;-><init>(Lcom/sec/samsung/gallery/glview/GlSlideShowView;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mHandler:Landroid/os/Handler;

    .line 95
    return-void

    .line 65
    :pswitch_1
    new-instance v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;

    invoke-direct {v0, p0, p1}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;-><init>(Lcom/sec/samsung/gallery/glview/GlSlideShowView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    .line 66
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    sget-object v1, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;->FLOW_DIRECTION_LEFT:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;

    invoke-virtual {v0, v3, v1}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->setSlideShowInfo(ILcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;)V

    goto :goto_0

    .line 69
    :pswitch_2
    new-instance v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;

    invoke-direct {v0, p0, p1}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;-><init>(Lcom/sec/samsung/gallery/glview/GlSlideShowView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    sget-object v1, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;->FADE:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;

    invoke-virtual {v0, v3, v1}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->setSlideShowInfo(ILcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;)V

    goto :goto_0

    .line 73
    :pswitch_3
    new-instance v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;

    invoke-direct {v0, p0, p1}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;-><init>(Lcom/sec/samsung/gallery/glview/GlSlideShowView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    .line 74
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    invoke-virtual {v0, v2, v1}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->setSlideShowInfo(ILcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;)V

    goto :goto_0

    .line 77
    :pswitch_4
    new-instance v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;

    invoke-direct {v0, p0, p1}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;-><init>(Lcom/sec/samsung/gallery/glview/GlSlideShowView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    .line 78
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    invoke-virtual {v0, v2, v1}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->setSlideShowInfo(ILcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;)V

    goto :goto_0

    .line 63
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/glview/GlSlideShowView;)Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->progressDialog:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/glview/GlSlideShowView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mIsBurstPlay:Z

    return v0
.end method

.method private dismissProgressDialog()V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/glview/GlSlideShowView$3;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView$3;-><init>(Lcom/sec/samsung/gallery/glview/GlSlideShowView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 181
    return-void
.end method

.method private updateConfiguration()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 241
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-nez v2, :cond_1

    .line 242
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 244
    .local v1, "winMgr":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 245
    .local v0, "orientation":I
    if-eq v0, v4, :cond_0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_2

    .line 246
    :cond_0
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mWideMode:Z

    .line 251
    .end local v0    # "orientation":I
    .end local v1    # "winMgr":Landroid/view/WindowManager;
    :cond_1
    :goto_0
    return-void

    .line 248
    .restart local v0    # "orientation":I
    .restart local v1    # "winMgr":Landroid/view/WindowManager;
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mWideMode:Z

    goto :goto_0
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 197
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 200
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public finishVideoView()V
    .locals 2

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->isPlayingVideoItem:Z

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setVisibility(I)V

    .line 280
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mSlideshowVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->finishSlideshowVideoView()V

    .line 282
    :cond_0
    return-void
.end method

.method protected getDistance()F
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->getDistance()F

    move-result v0

    return v0
.end method

.method public getInstance()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    return-object v0
.end method

.method public getIsShowing()Z
    .locals 1

    .prologue
    .line 274
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mIsShowing:Z

    return v0
.end method

.method public getSlideshowVideoView()Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mSlideshowVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    return-object v0
.end method

.method public onAnimationEnd(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 8
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    const/4 v3, 0x0

    .line 104
    const/4 v2, 0x2

    const-wide/16 v6, 0x0

    move-object v1, p0

    move v4, v3

    move v5, v3

    invoke-virtual/range {v1 .. v7}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->setCommandDelayed(IIIIJ)V

    .line 105
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mStartTime:J

    .line 106
    return-void
.end method

.method public onAnimationRepeat(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 110
    return-void
.end method

.method public onAnimationStart(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 114
    return-void
.end method

.method protected onCommand(ILjava/lang/Object;III)V
    .locals 12
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->isCreated()Z

    move-result v0

    if-nez v0, :cond_1

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_4

    .line 133
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->isInitialLoaded()Z

    move-result v0

    if-nez v0, :cond_2

    .line 134
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/glview/GlSlideShowView$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView$2;-><init>(Lcom/sec/samsung/gallery/glview/GlSlideShowView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 143
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0xa

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->setCommandDelayed(IIIIJ)V

    goto :goto_0

    .line 145
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mOnLoadingListener:Lcom/sec/samsung/gallery/glview/GlSlideShowView$OnLoadingListener;

    if-eqz v0, :cond_3

    .line 146
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mOnLoadingListener:Lcom/sec/samsung/gallery/glview/GlSlideShowView$OnLoadingListener;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView$OnLoadingListener;->onLoadingComplete()V

    .line 147
    :cond_3
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->dismissProgressDialog()V

    .line 148
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->getWaitDuration()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->getWaitDuration()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->startAfter(J)V

    goto :goto_0

    .line 153
    :cond_4
    const/4 v0, 0x2

    if-ne p1, v0, :cond_7

    .line 154
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->isNextLoaded()Z

    move-result v0

    if-nez v0, :cond_5

    .line 155
    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0xa

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->setCommandDelayed(IIIIJ)V

    goto :goto_0

    .line 157
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->getWaitDuration()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 158
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    .line 160
    .local v10, "now":J
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->getWaitDuration()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mStartTime:J

    sub-long v2, v10, v2

    sub-long v8, v0, v2

    .line 161
    .local v8, "newWaitDuration":J
    const-wide/16 v0, 0x0

    cmp-long v0, v8, v0

    if-gtz v0, :cond_6

    .line 162
    const-wide/16 v8, 0x0

    .line 165
    :cond_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 169
    .end local v8    # "newWaitDuration":J
    .end local v10    # "now":J
    :cond_7
    invoke-super/range {p0 .. p5}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onCommand(ILjava/lang/Object;III)V

    goto/16 :goto_0
.end method

.method protected onCreate()V
    .locals 2

    .prologue
    .line 185
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->setClearByColor(Z)V

    .line 186
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->resetLayout()V

    .line 187
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->updateConfiguration()V

    .line 188
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 189
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    instance-of v0, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xb71

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->createView()V

    .line 192
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 193
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 205
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->dismissProgressDialog()V

    .line 206
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xb71

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    .line 207
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->stop()V

    .line 208
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->destoryView()V

    .line 209
    return-void
.end method

.method protected onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mOnKeyListenner:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mOnKeyListenner:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;->onKeyEvent(II)Z

    move-result v0

    .line 216
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onTouch(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 224
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected resetLayout()V
    .locals 1

    .prologue
    .line 229
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->resetLayout()V

    .line 230
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->updateConfiguration()V

    .line 231
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->reLayoutView()V

    .line 232
    return-void
.end method

.method public setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .prologue
    const/4 v1, 0x0

    .line 236
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    check-cast p1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    .end local p1    # "adapter":Lcom/sec/samsung/gallery/glview/GlBaseAdapter;
    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->setAdapter(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;)V

    .line 237
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1, v1, v1}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->setCommand(IIII)V

    .line 238
    return-void
.end method

.method public setBurstShotMode(Z)V
    .locals 3
    .param p1, "isBurstPlay"    # Z

    .prologue
    .line 254
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mIsBurstPlay:Z

    .line 255
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mIsBurstPlay:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;->FLOW_DIRECTION_LEFT:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->setSlideShowInfo(ILcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;)V

    .line 258
    :cond_0
    return-void
.end method

.method public setInstance(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V
    .locals 0
    .param p1, "slideShowViewState"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 266
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .line 267
    return-void
.end method

.method public setIsShowing(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 262
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mIsShowing:Z

    .line 263
    return-void
.end method

.method public setOnLoadingListener(Lcom/sec/samsung/gallery/glview/GlSlideShowView$OnLoadingListener;)V
    .locals 0
    .param p1, "onLoadingListener"    # Lcom/sec/samsung/gallery/glview/GlSlideShowView$OnLoadingListener;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mOnLoadingListener:Lcom/sec/samsung/gallery/glview/GlSlideShowView$OnLoadingListener;

    .line 124
    return-void
.end method
