.class Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;
.super Ljava/lang/Object;
.source "AddUserTagEditActionbar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;-><init>(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 56
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    # invokes: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->setActionBarLayout()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$000(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)V

    .line 58
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mUsingSelectionDropDown:Z

    if-eqz v1, :cond_1

    .line 59
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$100(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03000f

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->customView:Landroid/view/View;

    .line 61
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$200(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 62
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$200(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$100(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200f3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 71
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$200(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->removeAllTabs()V

    .line 72
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$200(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 73
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$200(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Landroid/app/ActionBar;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->customView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 75
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->initDisplayOptions()V

    .line 76
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mUsingSelectionDropDown:Z

    if-eqz v1, :cond_0

    .line 77
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$100(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "buttonTitle":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    # invokes: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->setPopUpMenu(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$300(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;Ljava/lang/String;)V

    .line 81
    .end local v0    # "buttonTitle":Ljava/lang/String;
    :cond_0
    return-void

    .line 65
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$100(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030003

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->customView:Landroid/view/View;

    .line 67
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$200(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 69
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$200(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Landroid/app/ActionBar;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
