.class public Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;
.super Ljava/lang/Object;
.source "DimensionConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/DimensionConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DetailViewDimension"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;


# instance fields
.field public detail_view_default_mode:I

.field public detail_view_mode_count:I

.field public mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 360
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 362
    .local v1, "r":Landroid/content/res/Resources;
    const v2, 0x7f0c0074

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->detail_view_default_mode:I

    .line 363
    const v2, 0x7f0c0075

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->detail_view_mode_count:I

    .line 364
    iget v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->detail_view_mode_count:I

    mul-int/lit8 v2, v2, 0x2

    new-array v2, v2, [Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    .line 365
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->detail_view_mode_count:I

    mul-int/lit8 v2, v2, 0x2

    if-ge v0, v2, :cond_0

    .line 366
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    new-instance v3, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    invoke-direct {v3}, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;-><init>()V

    aput-object v3, v2, v0

    .line 367
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f09007a

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_hpadding_left:I

    .line 368
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f09007b

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_hgap:I

    .line 369
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f09007c

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_thumbnail_width:I

    .line 370
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f09007d

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_thumbnail_height:I

    .line 371
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f09007e

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_icon_hpadding_left:I

    .line 372
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f09007f

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_icon_vpadding_top:I

    .line 374
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090080

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_strip_hpadding_left:I

    .line 375
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090081

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_strip_vpadding_top:I

    .line 376
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090082

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_strip_thumbnail_width:I

    .line 377
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090083

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_strip_thumbnail_height:I

    .line 378
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090084

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_strip_icon_hpadding_left:I

    .line 379
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090085

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_strip_icon_vpadding_bottom:I

    .line 365
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 381
    :cond_0
    return-void
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 353
    const-class v1, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    if-nez v0, :cond_0

    .line 354
    new-instance v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    .line 356
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 353
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
