.class Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController$1;
.super Ljava/lang/Object;
.source "EditTextController.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 14
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "spanned"    # Landroid/text/Spanned;
    .param p5, "dstStart"    # I
    .param p6, "dstEnd"    # I

    .prologue
    .line 36
    sub-int v10, p3, p2

    const/16 v11, 0x80

    if-le v10, v11, :cond_0

    .line 37
    move/from16 v0, p2

    add-int/lit16 v0, v0, 0x80

    move/from16 p3, v0

    .line 39
    :cond_0
    invoke-interface/range {p1 .. p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    .line 40
    .local v7, "origTxt":Ljava/lang/String;
    move-object v8, v7

    .line 41
    .local v8, "validTxt":Ljava/lang/String;
    const/4 v5, 0x0

    .line 42
    .local v5, "invalidFlag":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v10, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->INVALID_CHAR:[Ljava/lang/String;

    array-length v10, v10

    if-ge v2, v10, :cond_4

    .line 43
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    .line 44
    .local v9, "validTxtLength":I
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_1
    if-ge v6, v9, :cond_3

    .line 45
    sget-object v10, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->INVALID_CHAR:[Ljava/lang/String;

    aget-object v10, v10, v2

    invoke-virtual {v8, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 46
    .local v4, "index":I
    if-ltz v4, :cond_2

    .line 47
    const/4 v5, 0x1

    .line 48
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    if-ge v4, v10, :cond_1

    .line 49
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v11, 0x0

    invoke-virtual {v8, v11, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    add-int/lit8 v11, v4, 0x1

    invoke-virtual {v8, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 44
    :cond_1
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 52
    :cond_2
    sget-object v10, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->INVALID_CHAR:[Ljava/lang/String;

    aget-object v10, v10, v2

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 53
    .local v1, "c":C
    const/16 v10, 0x21

    if-lt v1, v10, :cond_1

    const/16 v10, 0x7e

    if-ge v1, v10, :cond_1

    const/16 v10, 0x3f

    if-eq v1, v10, :cond_1

    .line 54
    const v10, 0xfee0

    add-int/2addr v10, v1

    int-to-char v1, v10

    .line 55
    invoke-virtual {v8, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 56
    .local v3, "iDBC":I
    if-ltz v3, :cond_1

    .line 57
    const/4 v5, 0x1

    .line 59
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    if-ge v3, v10, :cond_1

    .line 60
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v11, 0x0

    invoke-virtual {v8, v11, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    add-int/lit8 v11, v3, 0x1

    invoke-virtual {v8, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    .line 42
    .end local v1    # "c":C
    .end local v3    # "iDBC":I
    .end local v4    # "index":I
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 68
    .end local v6    # "j":I
    .end local v9    # "validTxtLength":I
    :cond_4
    if-eqz v5, :cond_6

    .line 69
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mInvalidToast:Landroid/widget/Toast;
    invoke-static {v10}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;)Landroid/widget/Toast;

    move-result-object v10

    if-nez v10, :cond_5

    .line 70
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;)Landroid/content/Context;

    move-result-object v11

    const v12, 0x7f0e0111

    const/4 v13, 0x0

    invoke-static {v11, v12, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v11

    # setter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mInvalidToast:Landroid/widget/Toast;
    invoke-static {v10, v11}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->access$002(Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 72
    :cond_5
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mInvalidToast:Landroid/widget/Toast;
    invoke-static {v10}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    .line 75
    .end local v8    # "validTxt":Ljava/lang/String;
    :goto_3
    return-object v8

    .restart local v8    # "validTxt":Ljava/lang/String;
    :cond_6
    const/4 v8, 0x0

    goto :goto_3
.end method
