.class Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;
.super Lorg/puremvc/java/patterns/mediator/Mediator;
.source "AlbumViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/Object;

    .prologue
    .line 197
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-direct {p0, p2, p3}, Lorg/puremvc/java/patterns/mediator/Mediator;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public handleNotification(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 5
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 215
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getName()Ljava/lang/String;

    move-result-object v1

    .line 216
    .local v1, "notiName":Ljava/lang/String;
    const-string v3, "VIEW_MODE_SWITCH"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    const-string v3, "MEDIA_EJECT"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 219
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v3

    if-lez v3, :cond_0

    .line 220
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$100(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->dismissDialogs()V

    .line 221
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->exitSelectionMode()V
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$200(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    goto :goto_0

    .line 223
    :cond_2
    const-string v3, "UPDATE_CONFIRM_MENU"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 224
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->updateConfirmMenu()V
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    goto :goto_0

    .line 225
    :cond_3
    const-string v3, "PREPARE_SLIDE_SHOW_DATA"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 226
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->addToSlideshowProxy()V
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$400(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    goto :goto_0

    .line 227
    :cond_4
    const-string v3, "VIEW_BY_TYPE_UPDATED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 228
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlexibleCloudAlbumFormat:Z

    if-eqz v3, :cond_5

    .line 229
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$500(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v2

    .line 230
    .local v2, "viewType":I
    sget-object v3, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUDALBUM_FORMAT:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 231
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->changeCloudAlbumFormat(Z)V

    .line 236
    .end local v2    # "viewType":I
    :cond_5
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->handleFilter()V

    goto :goto_0

    .line 233
    .restart local v2    # "viewType":I
    :cond_6
    const/4 v3, 0x1

    invoke-static {v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->changeCloudAlbumFormat(Z)V

    goto :goto_1

    .line 244
    .end local v2    # "viewType":I
    :cond_7
    const-string v3, "DOWNLOAD_NEARBY"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 246
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->downloadNearby(Lorg/puremvc/java/interfaces/INotification;)V
    invoke-static {v3, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$600(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Lorg/puremvc/java/interfaces/INotification;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 247
    :catch_0
    move-exception v0

    .line 248
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v3, "AlbumViewState"

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 250
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_8
    const-string v3, "REORDER_CANCEL"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 251
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->ResetReorderAlbums()V
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    .line 252
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->exitReorderAlbums()V
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$800(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    goto/16 :goto_0

    .line 253
    :cond_9
    const-string v3, "REORDER_DONE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 254
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$900(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    const v4, 0x7f0e0363

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 255
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->exitReorderAlbums()V
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$800(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    goto/16 :goto_0

    .line 256
    :cond_a
    const-string v3, "REFRESH_SELECTION"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 257
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->updateCountOnActionBar()V
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$1000(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    .line 258
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->refreshCheckState()V

    goto/16 :goto_0
.end method

.method public listNotificationInterests()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 200
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "VIEW_MODE_SWITCH"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "UPDATE_CONFIRM_MENU"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "PREPARE_SLIDE_SHOW_DATA"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "VIEW_BY_TYPE_UPDATED"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "DOWNLOAD_NEARBY"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "SECRET_MODE_CHANGED"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "REORDER_DONE"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "REORDER_CANCEL"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "REFRESH_SELECTION"

    aput-object v2, v0, v1

    return-object v0
.end method
