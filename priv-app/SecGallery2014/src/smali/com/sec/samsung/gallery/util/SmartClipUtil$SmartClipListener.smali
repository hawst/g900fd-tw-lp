.class public Lcom/sec/samsung/gallery/util/SmartClipUtil$SmartClipListener;
.super Ljava/lang/Object;
.source "SmartClipUtil.java"

# interfaces
.implements Lcom/samsung/android/smartclip/SmartClipDataExtractionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/util/SmartClipUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SmartClipListener"
.end annotation


# instance fields
.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mDetailViewState:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

.field private mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "photoView"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p3, "detailViewState"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-object p1, p0, Lcom/sec/samsung/gallery/util/SmartClipUtil$SmartClipListener;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 90
    iput-object p2, p0, Lcom/sec/samsung/gallery/util/SmartClipUtil$SmartClipListener;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    .line 91
    iput-object p3, p0, Lcom/sec/samsung/gallery/util/SmartClipUtil$SmartClipListener;->mDetailViewState:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .line 92
    return-void
.end method


# virtual methods
.method public onExtractSmartClipData(Landroid/view/View;Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;)I
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "element"    # Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;
    .param p3, "croppedArea"    # Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;

    .prologue
    .line 97
    invoke-interface {p3, p1}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;->intersects(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 98
    invoke-static {p1, p2, p3}, Lcom/samsung/android/smartclip/SmartClipMetaUtils;->extractDefaultSmartClipData(Landroid/view/View;Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;)I

    .line 99
    iget-object v4, p0, Lcom/sec/samsung/gallery/util/SmartClipUtil$SmartClipListener;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    .line 100
    .local v1, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    iget-object v4, p0, Lcom/sec/samsung/gallery/util/SmartClipUtil$SmartClipListener;->mDetailViewState:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->getMediaForDetails()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 102
    .local v0, "currentPhoto":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v4, v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v4, :cond_0

    if-eqz v0, :cond_0

    .line 104
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    const-string v3, "file_path_video"

    .line 106
    .local v3, "tagType":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    .line 108
    .local v2, "filePath":Ljava/lang/String;
    new-instance v4, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;

    invoke-direct {v4, v3, v2}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v4}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;->addTag(Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;)V

    .line 109
    new-instance v4, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;

    const-string/jumbo v5, "title"

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v4}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;->addTag(Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;)V

    .line 111
    iget-object v4, p0, Lcom/sec/samsung/gallery/util/SmartClipUtil$SmartClipListener;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v5, p0, Lcom/sec/samsung/gallery/util/SmartClipUtil$SmartClipListener;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-interface {p3}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;->getRect()Landroid/graphics/Rect;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/samsung/gallery/util/SmartClipUtil;->getCurrentImageRect(Landroid/app/Activity;Lcom/sec/android/gallery3d/ui/PhotoView;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-interface {p2, v4}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;->setMetaAreaRect(Landroid/graphics/Rect;)V

    .line 115
    .end local v0    # "currentPhoto":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v1    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    .end local v2    # "filePath":Ljava/lang/String;
    .end local v3    # "tagType":Ljava/lang/String;
    :cond_0
    const/4 v4, 0x1

    return v4

    .line 104
    .restart local v0    # "currentPhoto":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v1    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_1
    const-string v3, "file_path_image"

    goto :goto_0
.end method
