.class public Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;
.super Ljava/lang/Object;
.source "GlDividerConfig.java"


# instance fields
.field public drawableId:I

.field public height:I

.field public horizontalAlign:I

.field public verticalAlign:I

.field public width:I


# direct methods
.method public constructor <init>(III)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "drawableId"    # I

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;->width:I

    .line 12
    iput p2, p0, Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;->height:I

    .line 13
    iput p3, p0, Lcom/sec/samsung/gallery/glview/composeView/configure/GlDividerConfig;->drawableId:I

    .line 14
    return-void
.end method
