.class Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;
.super Ljava/lang/Object;
.source "GlGallerySearchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGallerySearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EstimationInfo"
.end annotation


# instance fields
.field firstX:F

.field firstY:F

.field firstZ:F

.field gapH:F

.field gapW:F

.field hCount:I

.field objH:F

.field objW:F

.field scroll:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V
    .locals 0

    .prologue
    .line 713
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;Lcom/sec/samsung/gallery/glview/GlGallerySearchView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/glview/GlGallerySearchView;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/glview/GlGallerySearchView$1;

    .prologue
    .line 713
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;-><init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V

    return-void
.end method


# virtual methods
.method public getObject(I)Lcom/sec/android/gallery3d/glcore/GlObject;
    .locals 7
    .param p1, "index"    # I

    .prologue
    .line 724
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->objW:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->objH:F

    invoke-direct {v1, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 726
    .local v1, "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->hCount:I

    rem-int v0, p1, v3

    .line 727
    .local v0, "col":I
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->hCount:I

    div-int v2, p1, v3

    .line 728
    .local v2, "row":I
    int-to-float v3, v0

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->gapW:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->firstX:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->firstY:F

    int-to-float v5, v2

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->gapH:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->scroll:F

    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->firstZ:F

    invoke-virtual {v1, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(FFF)V

    .line 729
    return-object v1
.end method

.method public saveState()V
    .locals 1

    .prologue
    .line 733
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemW:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->objW:F

    .line 734
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemH:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->objH:F

    .line 735
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapW:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->gapW:F

    .line 736
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->gapH:F

    .line 737
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemStartX:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->firstX:F

    .line 738
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemStartY:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->firstY:F

    .line 739
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemStartZ:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->firstZ:F

    .line 740
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->hCount:I

    .line 741
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;->scroll:F

    .line 742
    return-void
.end method
