.class public Lcom/sec/samsung/gallery/mapfragment/MarkerData;
.super Ljava/lang/Object;
.source "MarkerData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final TwoToasters:Lcom/sec/samsung/gallery/mapfragment/MarkerData;


# instance fields
.field private final label:Ljava/lang/String;

.field private final latLng:Lcom/google/android/gms/maps/model/LatLng;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 16
    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/MarkerData;

    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    const-wide v2, 0x4041ff9804c6b095L    # 35.996826741234294

    const-wide v4, -0x3fac465e82380000L    # -78.90048164874315

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    const-string v2, "Two Toasters"

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/mapfragment/MarkerData;-><init>(Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/samsung/gallery/mapfragment/MarkerData;->TwoToasters:Lcom/sec/samsung/gallery/mapfragment/MarkerData;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/String;)V
    .locals 0
    .param p1, "latLng"    # Lcom/google/android/gms/maps/model/LatLng;
    .param p2, "label"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/MarkerData;->latLng:Lcom/google/android/gms/maps/model/LatLng;

    .line 20
    iput-object p2, p0, Lcom/sec/samsung/gallery/mapfragment/MarkerData;->label:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MarkerData;->label:Ljava/lang/String;

    return-object v0
.end method

.method public getLatLng()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MarkerData;->latLng:Lcom/google/android/gms/maps/model/LatLng;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "arg1"    # I

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MarkerData;->latLng:Lcom/google/android/gms/maps/model/LatLng;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MarkerData;->label:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    return-void
.end method
