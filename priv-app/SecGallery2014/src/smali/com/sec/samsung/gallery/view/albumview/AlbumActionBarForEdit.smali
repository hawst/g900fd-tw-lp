.class public Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;
.source "AlbumActionBarForEdit.java"


# instance fields
.field private mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field private mMenu:Landroid/view/Menu;

.field protected mQuantitySelectedAlbum:I

.field protected mQuantitySelectedItem:I

.field private mSelectionText:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v0, 0x1

    .line 44
    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;I)V

    .line 39
    iput v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mQuantitySelectedAlbum:I

    .line 40
    iput v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mQuantitySelectedItem:I

    .line 45
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-direct {v0, p1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->setSelectAllButtonTitle(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;ZI)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;
    .param p1, "x1"    # Z
    .param p2, "x2"    # I

    .prologue
    .line 35
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->updatePopupMenuItemsVisibility(ZI)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;)Landroid/view/Menu;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mMenu:Landroid/view/Menu;

    return-object v0
.end method

.method private getOptionMenuId()I
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    const v0, 0x7f120020

    .line 308
    :goto_0
    return v0

    .line 303
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 304
    const v0, 0x7f120009

    goto :goto_0

    .line 305
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 306
    const v0, 0x7f12000b

    goto :goto_0

    .line 308
    :cond_2
    const v0, 0x7f12000a

    goto :goto_0
.end method

.method private isValidSelection()Z
    .locals 7

    .prologue
    .line 313
    const/4 v0, 0x1

    .line 315
    .local v0, "isValid":Z
    iget v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mQuantitySelectedAlbum:I

    if-nez v2, :cond_1

    .line 316
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0e0113

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 317
    const/4 v0, 0x0

    .line 324
    :cond_0
    :goto_0
    return v0

    .line 318
    :cond_1
    iget v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mQuantitySelectedItem:I

    sget v3, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    if-le v2, v3, :cond_0

    .line 319
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0114

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget v6, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 321
    .local v1, "text":Ljava/lang/CharSequence;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 322
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 50
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 51
    .local v0, "inflater":Landroid/view/MenuInflater;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->getOptionMenuId()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 52
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 156
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 158
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->isValidSelection()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 159
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->chooseShareDialog()V

    goto :goto_0

    .line 163
    :sswitch_1
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_SLIDESHOW_SETTING:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 167
    :sswitch_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AMSD"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1, v2, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 168
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_DELETE_DIALOG:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 171
    :sswitch_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showRenameDialog(Z)V

    goto :goto_0

    .line 174
    :sswitch_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDetailsDialog(Z)V

    goto :goto_0

    .line 177
    :sswitch_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3, v2, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->hideAlbums(ZZZ)V

    goto :goto_0

    .line 180
    :sswitch_6
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v1, v0, v2

    const/4 v1, 0x0

    aput-object v1, v0, v3

    .line 181
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "S_STUDIO"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 184
    .end local v0    # "params":[Ljava/lang/Object;
    :sswitch_7
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->moveToSecretbox(ZZ)V

    goto :goto_0

    .line 187
    :sswitch_8
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->moveToSecretbox(ZZ)V

    goto :goto_0

    .line 190
    :sswitch_9
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->moveToKNOX(ZZ)V

    goto :goto_0

    .line 193
    :sswitch_a
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->moveToKNOX(ZZ)V

    goto :goto_0

    .line 196
    :sswitch_b
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    sget-object v2, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;->CONFIRM_TAG:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    invoke-virtual {v1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showRemoveConfirmTagDialog(ZLcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;)V

    goto/16 :goto_0

    .line 199
    :sswitch_c
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/16 v2, 0x904

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->startContactActivity(I)V

    goto/16 :goto_0

    .line 202
    :sswitch_d
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    sget-object v2, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;->REMOVE_TAG:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    invoke-virtual {v1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showRemoveConfirmTagDialog(ZLcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;)V

    goto/16 :goto_0

    .line 205
    :sswitch_e
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->isValidSelection()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAdvancedSoundSceneShare:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isASoundSceneFileSelected(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/golf/GolfMgr;->isGolfFileSelected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 208
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showImageVideoConversionShareDialog(ZZ)V

    goto/16 :goto_0

    .line 210
    :cond_3
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_LAST_SHARE_APP:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 215
    :sswitch_f
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->download()V

    goto/16 :goto_0

    .line 218
    :sswitch_10
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->CopyToEvent(Z)V

    goto/16 :goto_0

    .line 221
    :sswitch_11
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/16 v2, 0x905

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAddUserTagDialog(IZ)V

    goto/16 :goto_0

    .line 156
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f026c -> :sswitch_0
        0x7f0f026d -> :sswitch_e
        0x7f0f026e -> :sswitch_2
        0x7f0f026f -> :sswitch_1
        0x7f0f0270 -> :sswitch_3
        0x7f0f0271 -> :sswitch_5
        0x7f0f0272 -> :sswitch_b
        0x7f0f0273 -> :sswitch_c
        0x7f0f0274 -> :sswitch_d
        0x7f0f0276 -> :sswitch_f
        0x7f0f0277 -> :sswitch_7
        0x7f0f0278 -> :sswitch_8
        0x7f0f0279 -> :sswitch_9
        0x7f0f027a -> :sswitch_a
        0x7f0f027b -> :sswitch_2
        0x7f0f027f -> :sswitch_10
        0x7f0f0282 -> :sswitch_11
        0x7f0f0283 -> :sswitch_6
        0x7f0f029b -> :sswitch_4
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 13
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v12, 0x7f0f0279

    const v11, 0x7f0f0278

    const v10, 0x7f0f0277

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 56
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mMenu:Landroid/view/Menu;

    .line 57
    const v1, 0x7f0f026b

    invoke-interface {p1, v1, v9}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 58
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->getSupportedOperationForSelectedAlbumItem()J

    move-result-wide v2

    .line 59
    .local v2, "supportOperation":J
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mMenu:Landroid/view/Menu;

    invoke-static {v1, v4, v2, v3, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkLastShareAppSupport(Landroid/content/Context;Landroid/view/Menu;JZ)J

    move-result-wide v2

    .line 60
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkAddUserTagSupport(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;J)J

    move-result-wide v2

    .line 61
    invoke-static {p1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->updateMenuOperation(Landroid/view/Menu;J)V

    .line 64
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v9, :cond_a

    .line 65
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-nez v1, :cond_0

    .line 66
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v4, 0x7f0f026e

    const/4 v5, 0x2

    invoke-static {v1, v4, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 68
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 69
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v4, 0x7f0f026c

    invoke-static {v1, v4, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 70
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v4, 0x7f0f026c

    const v5, 0x7f020030

    invoke-static {v1, v4, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    .line 78
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 79
    const v1, 0x7f0f0271

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 80
    const v1, 0x7f0f0283

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 83
    :cond_2
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v1, :cond_d

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/samsung/android/privatemode/PrivateModeManager;->isPrivateStorageMounted(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isReadyPrivateMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 85
    :cond_4
    const-wide/32 v4, 0x40000000

    and-long/2addr v4, v2

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_b

    .line 86
    invoke-static {p1, v10, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 90
    :goto_1
    const-wide/32 v4, 0x20000000

    and-long/2addr v4, v2

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_c

    .line 91
    invoke-static {p1, v11, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 92
    const v1, 0x7f0f0271

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 101
    :goto_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCS03lteEnabled()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 102
    invoke-static {p1, v10, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 103
    invoke-static {p1, v11, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 105
    :cond_5
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuHide:Z

    if-eqz v1, :cond_6

    .line 106
    const v1, 0x7f0f0271

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 108
    :cond_6
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsGuestMode:Z

    if-nez v1, :cond_11

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOXInstallMode(Landroid/content/Context;)Z

    move-result v1

    if-ne v1, v9, :cond_11

    .line 109
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 110
    invoke-static {p1, v12, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 111
    const-wide/32 v4, 0x1000000

    and-long/2addr v4, v2

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_e

    .line 112
    const v1, 0x7f0f027a

    invoke-static {p1, v1, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 135
    :goto_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/SStudioCmd;->isSStudioAvailable(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 136
    const v1, 0x7f0f0283

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 143
    :cond_7
    :goto_4
    const v1, 0x7f0f0275

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 144
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLastShare:Z

    if-nez v1, :cond_8

    .line 145
    const v1, 0x7f0f026d

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 147
    :cond_8
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-nez v1, :cond_9

    .line 148
    const v1, 0x7f0f0282

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 151
    :cond_9
    const v1, 0x7f0f027b

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 152
    return-void

    .line 73
    :cond_a
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 74
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v4, 0x7f0f026e

    const/4 v5, 0x2

    invoke-static {v1, v4, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    goto/16 :goto_0

    .line 88
    :cond_b
    invoke-static {p1, v10, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_1

    .line 94
    :cond_c
    invoke-static {p1, v11, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_2

    .line 97
    :cond_d
    invoke-static {p1, v10, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 98
    invoke-static {p1, v11, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_2

    .line 114
    :cond_e
    const v1, 0x7f0f027a

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_3

    .line 117
    :cond_f
    const v1, 0x7f0f027a

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 118
    const-wide/32 v4, 0x2000000

    and-long/2addr v4, v2

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_10

    .line 119
    invoke-static {p1, v12, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_3

    .line 121
    :cond_10
    invoke-static {p1, v12, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_3

    .line 125
    :cond_11
    invoke-static {p1, v12, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 126
    const v1, 0x7f0f027a

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_3

    .line 137
    :cond_12
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableStudioShowAsAction:Z

    if-eqz v1, :cond_7

    .line 138
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 139
    .local v0, "locale":Ljava/lang/String;
    const-string/jumbo v1, "vi"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 140
    const v1, 0x7f0f0283

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    goto/16 :goto_4
.end method

.method protected setSelectedItemCount(I)V
    .locals 0
    .param p1, "numberOfItemsSelected"    # I

    .prologue
    .line 259
    iput p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mQuantitySelectedItem:I

    .line 260
    return-void
.end method

.method protected setTitle(I)V
    .locals 2
    .param p1, "numberOfItemsSelected"    # I

    .prologue
    .line 228
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit$1;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 255
    return-void
.end method

.method protected updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V
    .locals 2
    .param p1, "button"    # Lcom/sec/samsung/gallery/util/Consts$ButtonType;
    .param p2, "visible"    # I
    .param p3, "toggleImage"    # Z

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit$2;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit$2;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;Lcom/sec/samsung/gallery/util/Consts$ButtonType;ZI)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 287
    return-void
.end method

.method public updateConfirm(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "format"    # Ljava/lang/String;

    .prologue
    .line 291
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mMenu:Landroid/view/Menu;

    if-eqz v1, :cond_0

    .line 292
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v2, 0x7f0f0272

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 294
    .local v0, "confirmAsMenu":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 295
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 298
    .end local v0    # "confirmAsMenu":Landroid/view/MenuItem;
    :cond_0
    return-void
.end method
