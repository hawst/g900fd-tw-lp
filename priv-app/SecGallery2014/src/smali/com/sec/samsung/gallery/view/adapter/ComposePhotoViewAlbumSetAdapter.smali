.class public Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;
.super Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;
.source "ComposePhotoViewAlbumSetAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ComposePhotoViewAlbumSetAdapter"


# instance fields
.field private mNewAlbumDimDrawable:Landroid/graphics/drawable/Drawable;

.field private mNewAlbumName:Ljava/lang/String;

.field private mSplitViewTitleHeight:I

.field private mSplitViewTitleMarginLeft:I

.field private mSplitViewTitleTextColor:I

.field private mSplitViewTitleTextSize:I

.field private mSplitViewTitleWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "config"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;
    .param p4, "modeOption"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;I)V

    .line 41
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v1, 0x59000000

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mNewAlbumDimDrawable:Landroid/graphics/drawable/Drawable;

    .line 46
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_0

    const/high16 v0, -0x1000000

    :goto_0
    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mSplitViewTitleTextColor:I

    .line 50
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00ec

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mSplitViewTitleWidth:I

    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00eb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mSplitViewTitleHeight:I

    .line 52
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00ef

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mSplitViewTitleTextSize:I

    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0389

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mSplitViewTitleMarginLeft:I

    .line 54
    return-void

    .line 46
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private drawAlbumLabel(Lcom/sec/android/gallery3d/glcore/GlImageView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "count"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 180
    invoke-virtual {p1, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 182
    .local v2, "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mSplitViewTitleTextSize:I

    int-to-float v4, v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getDefaultPaintForLabel(F)Landroid/text/TextPaint;

    move-result-object v1

    .line 183
    .local v1, "paint":Landroid/text/TextPaint;
    const/4 v0, 0x0

    .line 184
    .local v0, "label":Ljava/lang/String;
    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mSplitViewTitleWidth:I

    iget v5, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mSplitViewTitleMarginLeft:I

    sub-int v3, v4, v5

    .line 186
    .local v3, "titleViewWidth":I
    int-to-float v4, v3

    invoke-static {p2, p3, v4, v1}, Lcom/sec/android/gallery3d/glcore/GlTextView;->ellipsizeString(Ljava/lang/String;Ljava/lang/String;FLandroid/text/TextPaint;)Ljava/lang/String;

    move-result-object v0

    .line 187
    if-nez v2, :cond_1

    .line 188
    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mSplitViewTitleTextSize:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mSplitViewTitleTextColor:I

    invoke-static {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v2

    .line 189
    invoke-virtual {v2, v6, v6}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 191
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v4

    invoke-virtual {p0, v4, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setTextViewPaintForAlbum(Landroid/text/TextPaint;Z)V

    .line 192
    invoke-virtual {p1, v2, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 200
    :goto_0
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mIsRTL:Z

    if-eqz v4, :cond_0

    .line 201
    invoke-virtual {v2, v6}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setIsRTL(Z)V

    .line 202
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v4

    sget-object v5, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 204
    :cond_0
    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mSplitViewTitleWidth:I

    iget v5, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mSplitViewTitleHeight:I

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setSize(II)V

    .line 205
    return-void

    .line 194
    :cond_1
    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mSplitViewTitleTextSize:I

    int-to-float v4, v4

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v5

    invoke-virtual {v5}, Landroid/text/TextPaint;->getTextSize()F

    move-result v5

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_2

    .line 195
    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mSplitViewTitleTextSize:I

    int-to-float v4, v4

    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setTextSize(F)V

    .line 196
    :cond_2
    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setText(Ljava/lang/String;)V

    .line 198
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v4

    invoke-virtual {p0, v4, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setTextViewPaintForAlbum(Landroid/text/TextPaint;Z)V

    goto :goto_0
.end method

.method private drawDragAndHereText(Lcom/sec/android/gallery3d/glcore/GlImageView;)V
    .locals 4
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;

    .prologue
    const/16 v3, 0x64

    .line 280
    invoke-virtual {p1, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 281
    .local v0, "dragTextView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    if-nez v0, :cond_0

    .line 282
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getDragHereTextView(I)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v0

    .line 283
    invoke-virtual {p1, v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 289
    :goto_0
    return-void

    .line 285
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextView()Landroid/widget/TextView;

    move-result-object v1

    .line 286
    .local v1, "textView":Landroid/widget/TextView;
    invoke-virtual {v1}, Landroid/widget/TextView;->invalidate()V

    .line 287
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setTextView(Landroid/widget/TextView;)V

    goto :goto_0
.end method

.method private drawFrame(Lcom/sec/android/gallery3d/glcore/GlImageView;)V
    .locals 7
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;

    .prologue
    const/16 v6, 0x65

    const/4 v5, 0x1

    .line 140
    const/4 v1, 0x0

    .line 141
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-nez v2, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v2, :cond_5

    .line 142
    :cond_1
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumBackgroundColor()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 146
    .restart local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_0
    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 147
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v2, :cond_4

    .line 148
    :cond_2
    invoke-virtual {p1, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 149
    .local v0, "dimmer":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-nez v0, :cond_6

    .line 150
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "dimmer":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 151
    .restart local v0    # "dimmer":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mNewAlbumDimDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 152
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v2, :cond_3

    .line 153
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 155
    :cond_3
    invoke-virtual {p1, v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 160
    .end local v0    # "dimmer":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_4
    :goto_1
    return-void

    .line 144
    :cond_5
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02026d

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_0

    .line 157
    .restart local v0    # "dimmer":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_6
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private drawNewAlbumHand(Lcom/sec/android/gallery3d/glcore/GlImageView;)V
    .locals 7
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;

    .prologue
    const/16 v6, 0x66

    const/4 v5, 0x0

    .line 261
    invoke-virtual {p1, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 262
    .local v0, "hand":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-nez v0, :cond_0

    .line 263
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "hand":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 264
    .restart local v0    # "hand":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumHandSize()I

    move-result v2

    .line 265
    .local v2, "handSize":I
    invoke-virtual {v0, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 266
    const/4 v3, 0x2

    const/4 v4, 0x3

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 267
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumHandPadding()I

    move-result v1

    .line 268
    .local v1, "handPadding":I
    invoke-virtual {v0, v5, v5, v5, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 269
    invoke-virtual {p1, v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 271
    .end local v1    # "handPadding":I
    .end local v2    # "handSize":I
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v3

    if-eqz v3, :cond_1

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v3, :cond_2

    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mHeaderItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-nez v3, :cond_3

    .line 272
    :cond_2
    const v3, 0x7f020269

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 276
    :goto_0
    return-void

    .line 274
    :cond_3
    const v3, 0x7f020268

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private drawNewAlbumImage(Lcom/sec/android/gallery3d/glcore/GlImageView;)V
    .locals 14
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;

    .prologue
    const/16 v6, 0x65

    .line 208
    iget-object v13, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mHeaderItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 209
    .local v13, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mHeaderImage:Landroid/graphics/Bitmap;

    .line 212
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 239
    .end local v13    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    :goto_0
    return-void

    .line 214
    .restart local v13    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_4

    .line 215
    :cond_2
    invoke-virtual {p1, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 216
    .local v12, "dimmer":Lcom/sec/android/gallery3d/glcore/GlImageView;
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_3

    .line 217
    if-nez v12, :cond_3

    .line 218
    new-instance v12, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v12    # "dimmer":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v12, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 219
    .restart local v12    # "dimmer":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mNewAlbumDimDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v12, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 220
    invoke-virtual {p1, v12, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 223
    :cond_3
    if-eqz v12, :cond_4

    .line 224
    const/4 v0, 0x0

    invoke-virtual {v12, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 227
    .end local v12    # "dimmer":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_4
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v4

    .line 228
    .local v4, "rotation":I
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/LocalImage;->getTargetSize(I)I

    move-result v2

    .line 229
    .local v2, "targetW":I
    move v3, v2

    .line 230
    .local v3, "targetH":I
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceThumbnail:Z

    if-eqz v0, :cond_5

    instance-of v0, v13, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v0, :cond_5

    invoke-static {v13}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 232
    check-cast v13, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v13    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/LocalImage;->getRectOfAllFaces()Landroid/graphics/RectF;

    move-result-object v5

    .local v5, "faceRect":Landroid/graphics/RectF;
    move-object v0, p1

    .line 233
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setCroppedBitmap(Landroid/graphics/Bitmap;IIILandroid/graphics/RectF;)V

    goto :goto_0

    .line 236
    .end local v5    # "faceRect":Landroid/graphics/RectF;
    .restart local v13    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_5
    const/4 v11, 0x0

    move-object v6, p1

    move-object v7, v1

    move v8, v2

    move v9, v3

    move v10, v4

    invoke-virtual/range {v6 .. v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setCroppedBitmap(Landroid/graphics/Bitmap;IIILandroid/graphics/RectF;)V

    goto :goto_0
.end method

.method private drawNewAlbumLabel(Lcom/sec/android/gallery3d/glcore/GlImageView;)V
    .locals 3
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;

    .prologue
    .line 395
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 396
    .local v0, "count":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getAlbumNumber(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    .line 397
    .local v1, "photoNumlabel":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mNewAlbumName:Ljava/lang/String;

    invoke-direct {p0, p1, v2, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->drawAlbumLabel(Lcom/sec/android/gallery3d/glcore/GlImageView;Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    return-void
.end method

.method private drawNewAlbumPlayIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;)V
    .locals 6
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    .line 242
    invoke-virtual {p1, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 243
    .local v1, "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mHeaderItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getNewAlbumPlayTypeIconRsrc(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v2

    .line 245
    .local v2, "rsrcPlayType":I
    if-nez v1, :cond_0

    .line 246
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v1    # "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 247
    .restart local v1    # "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v1, v4, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 248
    invoke-virtual {p1, v1, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 251
    :cond_0
    if-eqz v2, :cond_1

    .line 252
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 253
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 254
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 258
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-void

    .line 256
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private getDragHereTextView(I)Lcom/sec/android/gallery3d/glcore/GlTextView;
    .locals 13
    .param p1, "index"    # I

    .prologue
    const/4 v12, 0x1

    const/4 v10, 0x2

    const/4 v11, 0x0

    .line 292
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumFontSize()I

    move-result v4

    .line 293
    .local v4, "textSize":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v9, v11}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumThumbnailWidthPixel(I)I

    move-result v7

    .line 294
    .local v7, "textboxW":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v9, v11}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumThumbnailHeightPixel(I)I

    move-result v6

    .line 295
    .local v6, "textboxH":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumTextViewPadding()I

    move-result v1

    .line 296
    .local v1, "padding":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumTextViewTopMargin()I

    move-result v8

    .line 298
    .local v8, "topMargin":I
    invoke-static {v7, v6}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(II)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v0

    .line 299
    .local v0, "dragTextView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    invoke-virtual {v0, v10, v10}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 301
    const-string v2, ""

    .line 302
    .local v2, "str":Ljava/lang/String;
    if-nez p1, :cond_2

    .line 303
    neg-int v9, v8

    invoke-virtual {v0, v11, v9, v11, v11}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 304
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    const v10, 0x7f0e024d

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 313
    :cond_0
    :goto_0
    new-instance v5, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v5, p0, v9}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;-><init>(Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;Landroid/content/Context;)V

    .line 314
    .local v5, "textView":Landroid/widget/TextView;
    invoke-virtual {v5, v11, v11, v7, v6}, Landroid/widget/TextView;->layout(IIII)V

    .line 315
    invoke-virtual {v5, v1, v11, v1, v11}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 316
    const/16 v9, 0x11

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setGravity(I)V

    .line 317
    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setTextColor(I)V

    .line 319
    int-to-float v9, v4

    invoke-virtual {v5, v11, v9}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 320
    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 321
    const/4 v9, 0x0

    const v10, 0x3f666666    # 0.9f

    invoke-virtual {v5, v9, v10}, Landroid/widget/TextView;->setLineSpacing(FF)V

    .line 323
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewDragHere:Z

    if-eqz v9, :cond_1

    .line 324
    invoke-virtual {v5}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    .line 325
    .local v3, "textPaint":Landroid/text/TextPaint;
    invoke-virtual {v3, v12}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 326
    sget-object v9, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v3, v9}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 329
    .end local v3    # "textPaint":Landroid/text/TextPaint;
    :cond_1
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setTextView(Landroid/widget/TextView;)V

    .line 330
    return-object v0

    .line 305
    .end local v5    # "textView":Landroid/widget/TextView;
    :cond_2
    if-ne p1, v12, :cond_3

    .line 306
    const/16 v9, 0xd2

    invoke-virtual {v0, v11, v11, v11, v9}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 307
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    const v10, 0x7f0e024e

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 308
    :cond_3
    if-ne p1, v10, :cond_0

    .line 309
    const/16 v9, -0xa

    invoke-virtual {v0, v11, v9, v11, v11}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 310
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    const v10, 0x7f0e024f

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public drawAlbumLabel(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;)V
    .locals 6
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "sizeType"    # I
    .param p3, "album"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    .prologue
    .line 163
    iget-object v2, p3, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 164
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget v1, p3, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemCount:I

    .line 165
    .local v1, "count":I
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v0

    .line 167
    .local v0, "albumName":Ljava/lang/String;
    :goto_0
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v5, :cond_0

    instance-of v5, v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    if-nez v5, :cond_0

    instance-of v5, v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    if-nez v5, :cond_0

    .line 169
    if-eqz v0, :cond_0

    const-string v5, "/"

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 170
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 171
    .local v4, "values":[Ljava/lang/String;
    const/4 v5, 0x1

    aget-object v0, v4, v5

    .line 175
    .end local v4    # "values":[Ljava/lang/String;
    :cond_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getAlbumNumber(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 176
    .local v3, "photoNumlabel":Ljava/lang/String;
    invoke-direct {p0, p1, v0, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->drawAlbumLabel(Lcom/sec/android/gallery3d/glcore/GlImageView;Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    return-void

    .line 165
    .end local v0    # "albumName":Ljava/lang/String;
    .end local v3    # "photoNumlabel":Ljava/lang/String;
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public getView(IILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 8
    .param p1, "position"    # I
    .param p2, "subPosition"    # I
    .param p3, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p4, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p5, "ext"    # I
    .param p6, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 72
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mRetLevel:I

    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-lt p1, v0, :cond_1

    .line 112
    .end local p6    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v1

    .line 76
    .restart local p6    # "obj":Ljava/lang/Object;
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v6, v0, p1

    .line 77
    .local v6, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-eqz v6, :cond_0

    iget-object v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_0

    iget v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-eqz v0, :cond_0

    .line 79
    if-gez p2, :cond_4

    .line 80
    const/16 v0, -0xa

    if-ne p2, v0, :cond_2

    .line 81
    invoke-virtual {p0, p3, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->drawCheckBox(Lcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;)Lcom/sec/android/gallery3d/glcore/GlImageView;

    move-result-object v1

    .line 88
    .local v1, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_1
    instance-of v0, p6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_0

    .line 89
    check-cast p6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .end local p6    # "obj":Ljava/lang/Object;
    invoke-virtual {p6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getFocusBorderVisible()Z

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->drawBorder(Lcom/sec/android/gallery3d/glcore/GlView;Z)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v1

    .end local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_0

    .line 82
    .end local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .restart local p6    # "obj":Ljava/lang/Object;
    :cond_2
    const/4 v0, -0x4

    if-ne p2, v0, :cond_3

    .line 83
    invoke-virtual {p0, p3, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->drawSelectedCountView(Lcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;)Lcom/sec/android/gallery3d/glcore/GlImageView;

    move-result-object v1

    .restart local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_1

    .line 85
    .end local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_3
    invoke-virtual {p0, p3, p2}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->drawAlbumLabelBG(Lcom/sec/android/gallery3d/glcore/GlView;I)Lcom/sec/android/gallery3d/glcore/GlImageView;

    move-result-object v1

    .line 86
    .restart local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {p0, v1, p2, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->drawAlbumLabel(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;)V

    goto :goto_1

    .line 93
    .end local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_4
    iget v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-ge p2, v0, :cond_0

    .line 96
    if-nez p3, :cond_5

    .line 97
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 102
    .restart local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_2
    const/4 v0, 0x1

    if-ne p5, v0, :cond_6

    .line 103
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v2, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSlideImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    .line 104
    .local v2, "imageItem":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->getItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    iget v5, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    move-object v0, p0

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->drawThumbnailImage(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;Lcom/sec/android/gallery3d/data/MediaItem;II)Z

    goto :goto_0

    .end local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v2    # "imageItem":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    :cond_5
    move-object v1, p3

    .line 99
    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_2

    .line 106
    :cond_6
    iget-object v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    aget-object v3, v0, p2

    .line 107
    .local v3, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemImage:[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    aget-object v2, v0, p2

    .line 108
    .restart local v2    # "imageItem":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    iget v5, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    move-object v0, p0

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->drawThumbnailImage(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;Lcom/sec/android/gallery3d/data/MediaItem;II)Z

    move-result v7

    .line 109
    .local v7, "result":Z
    if-eqz v7, :cond_0

    .line 110
    iget-object v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mUpdated:[B

    aget-byte v0, v0, p2

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mRetLevel:I

    goto :goto_0
.end method

.method public getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "ext"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 118
    if-nez p2, :cond_1

    .line 119
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 123
    .local v0, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_0
    const/4 v1, -0x1

    if-ne p1, v1, :cond_3

    .line 124
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mHeaderItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-nez v1, :cond_2

    .line 125
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->drawFrame(Lcom/sec/android/gallery3d/glcore/GlImageView;)V

    .line 129
    :goto_1
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->drawNewAlbumPlayIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;)V

    .line 130
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->drawNewAlbumHand(Lcom/sec/android/gallery3d/glcore/GlImageView;)V

    .line 131
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->drawDragAndHereText(Lcom/sec/android/gallery3d/glcore/GlImageView;)V

    .line 132
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->drawBorder(Lcom/sec/android/gallery3d/glcore/GlView;Z)Lcom/sec/android/gallery3d/glcore/GlView;

    .line 136
    :cond_0
    :goto_2
    return-object v0

    .end local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_1
    move-object v0, p2

    .line 121
    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_0

    .line 127
    :cond_2
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->drawNewAlbumImage(Lcom/sec/android/gallery3d/glcore/GlImageView;)V

    goto :goto_1

    .line 133
    :cond_3
    const/4 v1, -0x2

    if-ne p1, v1, :cond_0

    .line 134
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->drawNewAlbumLabel(Lcom/sec/android/gallery3d/glcore/GlImageView;)V

    goto :goto_2
.end method

.method public notifyLayoutChanged()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00ec

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mSplitViewTitleWidth:I

    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00eb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mSplitViewTitleHeight:I

    .line 60
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00ef

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mSplitViewTitleTextSize:I

    .line 61
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0389

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mSplitViewTitleMarginLeft:I

    .line 62
    return-void
.end method

.method public setNewAlbumName(Ljava/lang/String;)V
    .locals 0
    .param p1, "newAlbumName"    # Ljava/lang/String;

    .prologue
    .line 401
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mNewAlbumName:Ljava/lang/String;

    .line 402
    return-void
.end method
