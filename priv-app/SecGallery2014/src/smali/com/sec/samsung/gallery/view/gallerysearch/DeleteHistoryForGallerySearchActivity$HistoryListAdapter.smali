.class public Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;
.super Landroid/widget/BaseAdapter;
.source "DeleteHistoryForGallerySearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "HistoryListAdapter"
.end annotation


# instance fields
.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 464
    .local p2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;>;"
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 462
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->mItems:Ljava/util/ArrayList;

    .line 465
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->mItems:Ljava/util/ArrayList;

    .line 466
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->mItems:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 475
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->mItems:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 480
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 485
    const/4 v0, 0x0

    .line 486
    .local v0, "holder":Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItemViewHolder;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;

    .line 488
    .local v1, "item":Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;
    if-nez p2, :cond_0

    .line 489
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0300d0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 492
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItemViewHolder;

    .end local v0    # "holder":Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItemViewHolder;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItemViewHolder;-><init>()V

    .line 494
    .restart local v0    # "holder":Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItemViewHolder;
    const v2, 0x7f0f0121

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItemViewHolder;->tvName:Landroid/widget/TextView;

    .line 495
    const v2, 0x7f0f0210

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, v0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItemViewHolder;->cbChecked:Landroid/widget/CheckBox;

    .line 496
    const v2, 0x7f0f0211

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItemViewHolder;->tvDate:Landroid/widget/TextView;

    .line 498
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 503
    :goto_0
    iget-object v2, v0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItemViewHolder;->tvName:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->getKeyword()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 504
    iget-object v2, v0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItemViewHolder;->tvDate:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->getDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 505
    iget-object v2, v0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItemViewHolder;->cbChecked:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->getChecked()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 507
    return-object p2

    .line 500
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "holder":Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItemViewHolder;
    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItemViewHolder;

    .restart local v0    # "holder":Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItemViewHolder;
    goto :goto_0
.end method
