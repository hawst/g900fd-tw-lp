.class Lcom/sec/samsung/gallery/view/allview/AllViewState$1;
.super Landroid/database/ContentObserver;
.source "AllViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/allview/AllViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$1;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 5
    .param p1, "selfChange"    # Z

    .prologue
    .line 152
    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "onChange"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$1;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mTopSetPath:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$100(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "/slink"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 154
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$1;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$200(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 155
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_1

    .line 156
    monitor-enter v2

    .line 157
    :try_start_0
    instance-of v3, v2, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    if-eqz v3, :cond_0

    move-object v0, v2

    check-cast v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    move-object v3, v0

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->isConnect()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$1;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->bShowDisconnectDialog:Z
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$300(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$1;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$400(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v3

    instance-of v3, v3, Lcom/sec/samsung/gallery/view/allview/AllViewState;

    if-eqz v3, :cond_0

    .line 161
    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Slink device is disconnect by onContentDirty"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$1;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/sec/samsung/gallery/view/allview/AllViewState;->showDisconnectedDialog(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$500(Lcom/sec/samsung/gallery/view/allview/AllViewState;Ljava/lang/String;)V

    .line 163
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$1;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$600(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->getLastestActivityState(Landroid/content/Context;)Ljava/lang/Class;

    move-result-object v1

    .line 165
    .local v1, "activityState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$1;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$700(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v3, v1, v4}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 167
    .end local v1    # "activityState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    :cond_0
    monitor-exit v2

    .line 170
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    return-void

    .line 167
    .restart local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method
