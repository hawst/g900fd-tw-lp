.class Lcom/sec/samsung/gallery/glview/GlPhotoView$11;
.super Ljava/lang/Object;
.source "GlPhotoView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 1877
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFlingEnd(F)V
    .locals 4
    .param p1, "delta"    # F

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1901
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSx:F
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1302(Lcom/sec/samsung/gallery/glview/GlPhotoView;F)F

    .line 1902
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPitchFact:F
    invoke-static {v0, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1402(Lcom/sec/samsung/gallery/glview/GlPhotoView;F)F

    .line 1903
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->resetPitch()V

    .line 1904
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSx:F
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1300(Lcom/sec/samsung/gallery/glview/GlPhotoView;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->setScroll(F)V

    .line 1905
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->setThumbnailPitchRate(F)V

    .line 1906
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->hideScrollBar()V

    .line 1907
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    .line 1908
    return-void
.end method

.method public onFlingProcess(FF)V
    .locals 5
    .param p1, "delta"    # F
    .param p2, "elastic"    # F

    .prologue
    const/high16 v4, 0x41f00000    # 30.0f

    const/high16 v3, -0x3e100000    # -30.0f

    .line 1879
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSx:F
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1302(Lcom/sec/samsung/gallery/glview/GlPhotoView;F)F

    .line 1886
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->getSpeedRatio()F

    move-result v1

    neg-float v1, v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPitchFact:F
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1402(Lcom/sec/samsung/gallery/glview/GlPhotoView;F)F

    .line 1887
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPitchFact:F
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1400(Lcom/sec/samsung/gallery/glview/GlPhotoView;)F

    move-result v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_2

    .line 1888
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPitchFact:F
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1402(Lcom/sec/samsung/gallery/glview/GlPhotoView;F)F

    .line 1891
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScrollRange:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 1892
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSx:F
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1300(Lcom/sec/samsung/gallery/glview/GlPhotoView;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->setScroll(F)V

    .line 1893
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    neg-float v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->setThumbnailPitchRate(F)V

    .line 1894
    return-void

    .line 1889
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPitchFact:F
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1400(Lcom/sec/samsung/gallery/glview/GlPhotoView;)F

    move-result v0

    cmpg-float v0, v0, v3

    if-gez v0, :cond_0

    .line 1890
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPitchFact:F
    invoke-static {v0, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1402(Lcom/sec/samsung/gallery/glview/GlPhotoView;F)F

    goto :goto_0
.end method
