.class Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$9;
.super Ljava/lang/Object;
.source "HiddenPhotoViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$9;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenericMotionCancel(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 1
    .param p1, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 260
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$9;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # invokes: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->genericMotionCancel()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$1600(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V

    .line 261
    return-void
.end method

.method public onGenericMotionEnter(Lcom/sec/android/gallery3d/glcore/GlLayer;I)V
    .locals 3
    .param p1, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "position"    # I

    .prologue
    .line 254
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$9;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # invokes: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->genericMotionEnter(I)V
    invoke-static {v0, p2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$1500(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;I)V

    .line 255
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$9;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$9;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$300(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v0, v1, p2, v2}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(Ljava/lang/Object;ILcom/sec/android/gallery3d/ui/SelectionManager;)V

    .line 256
    return-void
.end method
