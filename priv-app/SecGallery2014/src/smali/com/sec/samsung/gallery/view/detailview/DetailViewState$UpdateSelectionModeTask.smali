.class Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;
.super Landroid/os/AsyncTask;
.source "DetailViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateSelectionModeTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0

    .prologue
    .line 5705
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;

    .prologue
    .line 5705
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    return-void
.end method

.method private updateSelectionMode()Ljava/lang/Boolean;
    .locals 10

    .prologue
    .line 5713
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$12800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCloneMediaList()Ljava/util/LinkedList;

    move-result-object v4

    .line 5714
    .local v4, "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v2, 0x0

    .line 5715
    .local v2, "isSelectedDataChanged":Z
    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 5716
    .local v6, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->isCancelled()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 5741
    .end local v6    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    return-object v8

    .line 5718
    .restart local v6    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    instance-of v8, v6, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v8, :cond_0

    instance-of v8, v6, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;

    if-nez v8, :cond_0

    move-object v3, v6

    .line 5719
    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 5720
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v5, 0x0

    .line 5721
    .local v5, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v7, 0x0

    .line 5722
    .local v7, "parentPath":Lcom/sec/android/gallery3d/data/Path;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$19300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiPickMode(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$15300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z

    move-result v8

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v8

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->name()Ljava/lang/String;

    move-result-object v8

    const-string v9, "IMAGE"

    if-eq v8, v9, :cond_3

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->name()Ljava/lang/String;

    move-result-object v8

    const-string v9, "VIDEO"

    if-ne v8, v9, :cond_4

    .line 5724
    :cond_3
    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/data/MediaItem;->getParentSetPathInMultiPick(Lcom/sec/android/gallery3d/data/MediaObject;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    .line 5726
    :cond_4
    if-nez v7, :cond_5

    .line 5727
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getParentSetPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    .line 5728
    :cond_5
    if-eqz v7, :cond_6

    .line 5729
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v8

    invoke-virtual {v8, v7}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v5

    .line 5732
    :cond_6
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 5733
    new-instance v0, Ljava/io/File;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5734
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_0

    .line 5735
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$12800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v8

    invoke-virtual {v8, v5, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 5736
    const/4 v2, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 5709
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->updateSelectionMode()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 5705
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 5746
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 5748
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5749
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateCountOnActionBar()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$9300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 5750
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$12800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/FilmStripView;->selectAll(I)V

    .line 5751
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateMenuOperations()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$16400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 5752
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->invalidate()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5758
    :cond_0
    :goto_0
    return-void

    .line 5754
    :catch_0
    move-exception v0

    .line 5755
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 5756
    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$5000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onPostExecute : NullPointerException!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 5705
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
