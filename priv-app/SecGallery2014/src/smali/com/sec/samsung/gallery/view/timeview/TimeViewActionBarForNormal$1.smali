.class Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;
.super Ljava/lang/Object;
.source "TimeViewActionBarForNormal.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v6, -0x1

    const/4 v7, 0x1

    .line 43
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$000(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030002

    invoke-virtual {v4, v5, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 46
    .local v1, "customView":Landroid/view/View;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 47
    new-instance v3, Landroid/app/ActionBar$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    .line 49
    .local v3, "lp":Landroid/app/ActionBar$LayoutParams;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$100(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0042

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    iput v4, v3, Landroid/app/ActionBar$LayoutParams;->leftMargin:I

    .line 51
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$200(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v1, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 63
    .end local v3    # "lp":Landroid/app/ActionBar$LayoutParams;
    :goto_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$1200(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 64
    const/16 v2, 0xc

    .line 66
    .local v2, "flags":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$1300(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$1400(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->isFestivalMode()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 67
    const/16 v2, 0x8

    .line 68
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$1500(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 70
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$1600(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ActionBar;->getDisplayOptions()I

    move-result v4

    or-int v0, v4, v2

    .line 71
    .local v0, "change":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$1700(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 73
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # invokes: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->isReadyToEnableHomeButton()Z
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$1800(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 74
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$1900(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 76
    :cond_1
    return-void

    .line 52
    .end local v0    # "change":I
    .end local v2    # "flags":I
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$300(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v4

    sget-object v5, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v5, :cond_3

    .line 54
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$500(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$400(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e00a7

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 56
    :cond_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$600(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 57
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$700(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 58
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$800(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 59
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$900(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f11002a

    invoke-virtual {v4, v5}, Landroid/content/Context;->setTheme(I)V

    .line 60
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$1000(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 61
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->access$1100(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method
