.class public Lcom/sec/samsung/gallery/view/utils/SoundUtils;
.super Ljava/lang/Object;
.source "SoundUtils.java"


# static fields
.field private static final MAX_SOUND_POOL:I = 0xa

.field public static final PLAY_AIRVIEW:I = 0x4

.field public static final PLAY_CHOICE:I = 0x0

.field public static final PLAY_SPREAD:I = 0x1

.field public static final PLAY_TOGETHER:I = 0x2

.field public static final PLAY_UP:I = 0x3

.field private static mSoundUtils:Lcom/sec/samsung/gallery/view/utils/SoundUtils;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurSoundRes:[I

.field private mSoundPool:Landroid/media/SoundPool;

.field private mSoundPoolId:[I

.field private mSoundResId:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mSoundUtils:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mSoundPool:Landroid/media/SoundPool;

    .line 24
    const/16 v0, 0xa

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mSoundPoolId:[I

    .line 25
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mSoundResId:[I

    .line 33
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mCurSoundRes:[I

    .line 36
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mContext:Landroid/content/Context;

    .line 37
    return-void

    .line 25
    nop

    :array_0
    .array-data 4
        0x7f080000
        0x7f080002
        0x7f080002
        0x7f080002
        0x7f080001
    .end array-data
.end method


# virtual methods
.method public playSoundResource(I)V
    .locals 8
    .param p1, "i"    # I

    .prologue
    .line 55
    :try_start_0
    const-string/jumbo v0, "situation=1;device=0"

    invoke-static {v0}, Landroid/media/AudioSystem;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 58
    .local v2, "volume":F
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "sound_effects_enabled"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 67
    .end local v2    # "volume":F
    :goto_0
    return-void

    .line 61
    .restart local v2    # "volume":F
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mSoundPool:Landroid/media/SoundPool;

    if-nez v0, :cond_1

    .line 62
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->soundSet(I)V

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mSoundPool:Landroid/media/SoundPool;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mSoundPoolId:[I

    aget v1, v1, p1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    move v3, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 64
    .end local v2    # "volume":F
    :catch_0
    move-exception v7

    .line 65
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public releaseSoundSet()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mSoundPool:Landroid/media/SoundPool;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mSoundPool:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mSoundPool:Landroid/media/SoundPool;

    .line 74
    :cond_0
    return-void
.end method

.method public soundSet(I)V
    .locals 6
    .param p1, "set"    # I

    .prologue
    const/4 v5, 0x0

    .line 40
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->releaseSoundSet()V

    .line 41
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mSoundResId:[I

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mCurSoundRes:[I

    .line 43
    new-instance v1, Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mCurSoundRes:[I

    array-length v2, v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3, v5}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mSoundPool:Landroid/media/SoundPool;

    .line 44
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mCurSoundRes:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 45
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mSoundPool:Landroid/media/SoundPool;

    if-eqz v1, :cond_0

    .line 46
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mSoundPoolId:[I

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mSoundPool:Landroid/media/SoundPool;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->mCurSoundRes:[I

    aget v4, v4, v0

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v1, v0

    .line 44
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51
    :cond_1
    return-void
.end method
