.class Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$4;
.super Ljava/lang/Object;
.source "MoreInfoItemLocation.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAddressAvailable(Ljava/lang/String;)V
    .locals 2
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mEditMode:Z

    if-eqz v0, :cond_1

    .line 126
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    # setter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInterimAddress:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->access$302(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;Ljava/lang/String;)Ljava/lang/String;

    .line 132
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->invalidate()V

    .line 133
    return-void

    .line 129
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    # setter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInterimAddress:Ljava/lang/String;
    invoke-static {v1, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->access$302(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mAddress:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->access$402(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method
