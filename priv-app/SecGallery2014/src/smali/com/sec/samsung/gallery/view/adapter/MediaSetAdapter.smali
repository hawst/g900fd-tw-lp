.class public Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;
.super Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;
.source "MediaSetAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$1;,
        Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;,
        Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;
    }
.end annotation


# static fields
.field private static FOCUS_TOP_BOTTOM_PADDING:I = 0x0

.field private static final RATIO_FOR_EASY_MODE:F = 1.5f

.field private static final TAG:Ljava/lang/String; = "MediaSetAdapter"


# instance fields
.field private drawableDim:Landroid/graphics/drawable/Drawable;

.field public mAspectRatioCorrection:F

.field private mBadgeRatio:F

.field private mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

.field protected final mContext:Landroid/content/Context;

.field private mCoverData:[Lcom/sec/android/gallery3d/data/MediaItem;

.field private mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

.field public mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

.field private mFocusAlbumFilePath:Ljava/lang/String;

.field protected final mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

.field private mIconLeft:I

.field private mIndexInfo:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;

.field private mIsChangingSource:Z

.field private mIsEasyMode:Z

.field private mIsEnabledSelectionMode:Z

.field private mIsPhotoViewState:Z

.field private mIsReorderAlbum:Z

.field private mItemCount:[I

.field private mMediaSelectionCallback:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;

.field private mNewAlbumIndex:I

.field protected final mNewAlbumSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field public mRatio:F

.field protected final mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field protected mSource:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

.field private mUpdateSet:[Lcom/sec/android/gallery3d/data/MediaSet;

.field private mViewMode:I

.field protected final mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field private titleDrawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x7

    sput v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->FOCUS_TOP_BOTTOM_PADDING:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const/16 v5, 0x3e8

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 135
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;-><init>(Landroid/content/Context;)V

    .line 90
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsChangingSource:Z

    .line 93
    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIconLeft:I

    .line 94
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mFocusAlbumFilePath:Ljava/lang/String;

    .line 95
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    .line 96
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsEnabledSelectionMode:Z

    .line 99
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mNewAlbumIndex:I

    .line 100
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsPhotoViewState:Z

    .line 104
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsReorderAlbum:Z

    .line 107
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v1, -0x67000000

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawableDim:Landroid/graphics/drawable/Drawable;

    .line 108
    new-array v0, v5, [Lcom/sec/android/gallery3d/data/MediaSet;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mUpdateSet:[Lcom/sec/android/gallery3d/data/MediaSet;

    .line 109
    new-instance v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;

    invoke-direct {v0, p0, v3}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;-><init>(Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIndexInfo:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;

    .line 110
    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mViewMode:I

    .line 116
    iput v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mRatio:F

    .line 117
    iput v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mBadgeRatio:F

    .line 124
    iput v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mAspectRatioCorrection:F

    .line 137
    new-array v0, v5, [Lcom/sec/android/gallery3d/data/MediaItem;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mCoverData:[Lcom/sec/android/gallery3d/data/MediaItem;

    .line 138
    new-array v0, v5, [I

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mItemCount:[I

    .line 140
    invoke-static {p2}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 141
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    .line 142
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    .line 143
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-object v0, p1

    .line 145
    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 146
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 147
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 148
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getNewAlbumSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mNewAlbumSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 150
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    .line 151
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0b0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->titleDrawable:Landroid/graphics/drawable/Drawable;

    .line 154
    return-void
.end method

.method private drawBadge(ILcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;I)V
    .locals 4
    .param p1, "position"    # I
    .param p2, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p3, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p4, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p5, "itemCount"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 474
    if-nez p4, :cond_3

    move v0, v1

    .line 475
    .local v0, "showIcon":Z
    :goto_0
    invoke-static {p4}, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->isPicasaImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 476
    const/4 v0, 0x0

    .line 477
    :cond_0
    invoke-virtual {p0, p2, p4, p5, v0}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawVideoPlayIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;IZ)V

    .line 478
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mFocusOnText:Z

    if-nez v3, :cond_5

    :goto_1
    invoke-virtual {p0, p2, p1, p5, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawFocusBox(Lcom/sec/android/gallery3d/glcore/GlImageView;IIZ)V

    .line 479
    invoke-virtual {p0, p2, p3, p5}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawUnconfirmIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 480
    invoke-direct {p0, p2, p1, p4}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->setBitmap(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/android/gallery3d/data/MediaItem;)V

    .line 481
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawMediaTypeIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;I)V

    .line 482
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v1, :cond_1

    .line 483
    invoke-virtual {p0, p2, p3}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawSecretboxIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 484
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawSelectedCountView(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 485
    invoke-virtual {p0, p2, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawCheckboxAndMark(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V

    .line 486
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 487
    invoke-virtual {p0, p2, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawReorderIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V

    .line 489
    :cond_2
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawNewMark(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 490
    return-void

    .line 474
    .end local v0    # "showIcon":Z
    :cond_3
    invoke-virtual {p4}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v3

    if-nez v3, :cond_4

    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0

    .restart local v0    # "showIcon":Z
    :cond_5
    move v2, v1

    .line 478
    goto :goto_1
.end method

.method private drawFocusBox(Lcom/sec/android/gallery3d/glcore/GlImageView;II)V
    .locals 7
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I
    .param p3, "itemCount"    # I

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 990
    const/4 v1, 0x0

    .line 991
    .local v1, "padding":I
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 992
    sget v1, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->FOCUS_TOP_BOTTOM_PADDING:I

    .line 994
    :cond_0
    invoke-virtual {p1, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 995
    .local v0, "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mGenericMotionFocus:I

    if-ne v2, p2, :cond_6

    .line 996
    if-nez v0, :cond_1

    .line 997
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 998
    .restart local v0    # "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const v2, 0x7f020291

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 999
    invoke-virtual {p1, v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 1001
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isJEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1002
    const/16 v1, -0x12

    .line 1003
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframe_bottom_padding:I

    add-int/2addr v2, v1

    invoke-virtual {v0, v6, v6, v6, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    .line 1022
    :goto_0
    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 1026
    :cond_2
    :goto_1
    return-void

    .line 1005
    :cond_3
    if-gt p3, v3, :cond_4

    .line 1006
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframe_left_padding:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframe_top_padding:I

    add-int/2addr v3, v1

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v4, v4, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframe_right_padding:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v5, v5, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframe_bottom_padding:I

    add-int/2addr v5, v1

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    goto :goto_0

    .line 1010
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v2, 0xf

    if-ge p3, v2, :cond_5

    .line 1011
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_2_left_padding:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_2_top_padding:I

    add-int/2addr v3, v1

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v4, v4, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_2_right_padding:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v5, v5, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_2_bottom_padding:I

    add-int/2addr v5, v1

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    goto :goto_0

    .line 1016
    :cond_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_3_left_padding:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_3_top_padding:I

    add-int/2addr v3, v1

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v4, v4, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_3_right_padding:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v5, v5, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_3_bottom_padding:I

    add-int/2addr v5, v1

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    goto :goto_0

    .line 1023
    :cond_6
    if-eqz v0, :cond_2

    .line 1024
    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private drawFrame(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 5
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "itemCount"    # I

    .prologue
    const/4 v2, 0x1

    .line 880
    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 882
    .local v0, "frameView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-nez v0, :cond_0

    .line 883
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "frameView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 884
    .restart local v0    # "frameView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {p1, v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 887
    :cond_0
    if-gt p3, v2, :cond_1

    .line 888
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframe_left_padding:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframe_top_padding:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframe_right_padding:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v4, v4, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframe_bottom_padding:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    .line 890
    const v1, 0x7f0201cc

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 906
    :goto_0
    return-void

    .line 892
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0xf

    if-ge p3, v1, :cond_2

    .line 893
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_2_left_padding:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_2_top_padding:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_2_right_padding:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v4, v4, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_2_bottom_padding:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    .line 896
    const v1, 0x7f0201cd

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    goto :goto_0

    .line 899
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_3_left_padding:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_3_top_padding:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_3_right_padding:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v4, v4, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_3_bottom_padding:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    .line 902
    const v1, 0x7f0201ce

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private drawLabel(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;II)V
    .locals 18
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "position"    # I
    .param p4, "itemCount"    # I

    .prologue
    .line 805
    if-eqz p2, :cond_3

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v8

    .line 806
    .local v8, "albumName":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    move/from16 v0, p4

    invoke-static {v7, v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getAlbumNumber(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 809
    .local v3, "albumNumber":Ljava/lang/String;
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v7, :cond_0

    move-object/from16 v0, p2

    instance-of v7, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    if-nez v7, :cond_0

    move-object/from16 v0, p2

    instance-of v7, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    if-nez v7, :cond_0

    .line 811
    const-string v7, "/"

    invoke-virtual {v8, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 812
    invoke-static {v8}, Lcom/sec/android/gallery3d/util/GalleryUtils;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 813
    .local v13, "values":[Ljava/lang/String;
    const/4 v7, 0x1

    aget-object v8, v13, v7

    .line 817
    .end local v13    # "values":[Ljava/lang/String;
    :cond_0
    const/high16 v6, 0x435c0000    # 220.0f

    .line 818
    .local v6, "textW":F
    const/4 v5, -0x1

    .line 819
    .local v5, "textColor":I
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v9, v7, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawlabel_bottom_margin:I

    .line 820
    .local v9, "bottomMargin":I
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v7, v7, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawlabel_font:I

    int-to-float v4, v7

    .line 821
    .local v4, "textSize":F
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsPhotoViewState:Z

    if-eqz v7, :cond_4

    .line 822
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v15, 0x7f0c003f

    invoke-virtual {v7, v15}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    int-to-float v4, v7

    .line 824
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v15, 0x7f0c0040

    invoke-virtual {v7, v15}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    .line 826
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    sget-object v15, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALL:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v7, v15, :cond_1

    .line 827
    const/high16 v7, 0x40800000    # 4.0f

    sub-float/2addr v4, v7

    .line 829
    const v7, 0x3f8ccccd    # 1.1f

    mul-float/2addr v6, v7

    .line 831
    :cond_1
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mFocusedIndex:I

    move/from16 v0, p3

    if-eq v0, v7, :cond_2

    .line 832
    const v5, -0x777778

    .line 839
    :cond_2
    :goto_1
    const/4 v7, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 840
    .local v12, "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    invoke-static {v4}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getDefaultPaintForLabel(F)Landroid/text/TextPaint;

    move-result-object v11

    .line 841
    .local v11, "paint":Landroid/text/TextPaint;
    const/4 v2, 0x0

    .line 842
    .local v2, "label":Ljava/lang/String;
    invoke-static {v8, v3, v6, v11}, Lcom/sec/android/gallery3d/glcore/GlTextView;->ellipsizeString(Ljava/lang/String;Ljava/lang/String;FLandroid/text/TextPaint;)Ljava/lang/String;

    move-result-object v2

    .line 844
    if-nez v12, :cond_5

    .line 845
    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;Ljava/lang/String;FIFZ)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v12

    .line 847
    const/4 v7, 0x1

    const/4 v15, 0x3

    invoke-virtual {v12, v7, v15}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 848
    const/4 v7, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v12, v7, v15, v0, v9}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 849
    const/4 v7, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 856
    :goto_2
    invoke-virtual {v12}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getHeight()I

    move-result v10

    .line 857
    .local v10, "hight":I
    invoke-virtual {v11, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v7

    float-to-double v0, v7

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-int v14, v0

    .line 858
    .local v14, "width":I
    invoke-virtual {v12, v14, v10}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setSize(II)V

    .line 859
    return-void

    .line 805
    .end local v2    # "label":Ljava/lang/String;
    .end local v3    # "albumNumber":Ljava/lang/String;
    .end local v4    # "textSize":F
    .end local v5    # "textColor":I
    .end local v6    # "textW":F
    .end local v8    # "albumName":Ljava/lang/String;
    .end local v9    # "bottomMargin":I
    .end local v10    # "hight":I
    .end local v11    # "paint":Landroid/text/TextPaint;
    .end local v12    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    .end local v14    # "width":I
    :cond_3
    const-string v8, ""

    goto/16 :goto_0

    .line 835
    .restart local v3    # "albumNumber":Ljava/lang/String;
    .restart local v4    # "textSize":F
    .restart local v5    # "textColor":I
    .restart local v6    # "textW":F
    .restart local v8    # "albumName":Ljava/lang/String;
    .restart local v9    # "bottomMargin":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v7, v7, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawlabel_font:I

    int-to-float v4, v7

    .line 836
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v9, v7, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawlabel_bottom_margin:I

    goto :goto_1

    .line 851
    .restart local v2    # "label":Ljava/lang/String;
    .restart local v11    # "paint":Landroid/text/TextPaint;
    .restart local v12    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    :cond_5
    const/4 v7, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v12, v7, v15, v0, v9}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 852
    invoke-virtual {v12, v4}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setTextSize(F)V

    .line 853
    invoke-virtual {v12, v5}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setColor(I)V

    .line 854
    invoke-virtual {v12, v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setText(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private drawLabel(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;Z)V
    .locals 32
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "selected"    # Z

    .prologue
    .line 493
    if-eqz p2, :cond_3

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v13

    .line 494
    .local v13, "itemCount":I
    :goto_0
    if-eqz p2, :cond_4

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v6

    .line 496
    .local v6, "albumName":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-static {v0, v13}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getAlbumNumber(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    .line 500
    .local v7, "albumNumber":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v8, v0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawlabel_bottom_margin:I

    .line 501
    .local v8, "bottomMargin":I
    const/16 v18, 0x0

    .line 504
    .local v18, "postfixSize":F
    sget-boolean v28, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v28, :cond_0

    move-object/from16 v0, p2

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    move/from16 v28, v0

    if-nez v28, :cond_0

    move-object/from16 v0, p2

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    move/from16 v28, v0

    if-nez v28, :cond_0

    .line 506
    if-eqz v6, :cond_0

    const-string v28, "/"

    move-object/from16 v0, v28

    invoke-virtual {v6, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v28

    if-eqz v28, :cond_0

    .line 507
    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v26

    .line 508
    .local v26, "values":[Ljava/lang/String;
    const/16 v28, 0x1

    aget-object v6, v26, v28

    .line 514
    .end local v26    # "values":[Ljava/lang/String;
    :cond_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 515
    .local v9, "dimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawlabel_top_margin:I

    move/from16 v25, v0

    .line 516
    .local v25, "topMargin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v15, v0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawlabel_left_margin:I

    .line 517
    .local v15, "leftMargin":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsPhotoViewState:Z

    move/from16 v28, v0

    if-eqz v28, :cond_6

    .line 518
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumFontSize()I

    move-result v28

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v22, v0

    .line 519
    .local v22, "textSize":F
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumCountFontSize()I

    move-result v28

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v18, v0

    .line 520
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    const-string/jumbo v29, "thumbnailViewMode"

    const/16 v30, 0x0

    invoke-static/range {v28 .. v30}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v28

    move/from16 v0, v28

    invoke-virtual {v9, v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxWidthPixel(I)I

    move-result v28

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v24, v0

    .line 522
    .local v24, "textW":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v28

    const v29, 0x7f0c0041

    invoke-virtual/range {v28 .. v29}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v25

    .line 523
    if-eqz p3, :cond_5

    .line 524
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumSelectedFontColor()I

    move-result v20

    .line 536
    .local v20, "textColor":I
    :goto_2
    const/16 v19, 0x0

    .line 537
    .local v19, "sPath":Ljava/lang/String;
    const/4 v11, 0x0

    .line 538
    .local v11, "fontType":Ljava/lang/String;
    const/16 v16, 0x0

    .line 540
    .local v16, "mKaiti":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    const/16 v29, 0x1

    invoke-static/range {v28 .. v29}, Landroid/graphics/Typeface;->getFontPathFlipFont(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v19

    .line 541
    sget-boolean v28, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v28, :cond_1

    .line 543
    :try_start_1
    const-string v28, "default"

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v28

    if-eqz v28, :cond_7

    .line 544
    const/16 v16, 0x0

    .line 556
    :cond_1
    :goto_3
    :try_start_2
    invoke-static/range {v22 .. v22}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getDefaultPaintForLabel(F)Landroid/text/TextPaint;

    move-result-object v17

    .line 557
    .local v17, "paint":Landroid/text/TextPaint;
    const/4 v14, 0x0

    .line 559
    .local v14, "label":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsEasyMode:Z

    move/from16 v28, v0

    if-nez v28, :cond_9

    .line 560
    int-to-float v0, v15

    move/from16 v28, v0

    const/high16 v29, 0x40000000    # 2.0f

    div-float v28, v28, v29

    sub-float v28, v24, v28

    move/from16 v0, v28

    move-object/from16 v1, v17

    invoke-static {v6, v7, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlTextView;->ellipsizeString(Ljava/lang/String;Ljava/lang/String;FLandroid/text/TextPaint;)Ljava/lang/String;

    move-result-object v14

    .line 564
    :goto_4
    const/16 v28, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v23

    check-cast v23, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 566
    .local v23, "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    if-nez v23, :cond_b

    .line 567
    move/from16 v0, v22

    move/from16 v1, v20

    invoke-static {v14, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v23

    .line 568
    const/16 v28, 0x1

    const/16 v29, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 569
    const/16 v28, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v25

    move/from16 v2, v28

    invoke-virtual {v0, v15, v1, v2, v8}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 570
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v21

    .line 571
    .local v21, "textPaint":Landroid/text/TextPaint;
    move-object/from16 v0, v21

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 572
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v28

    if-eqz v28, :cond_a

    .line 573
    invoke-static {}, Lcom/sec/samsung/gallery/util/FontUtil;->getRegularFont()Landroid/graphics/Typeface;

    move-result-object v28

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 576
    :goto_5
    const/16 v28, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 577
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v28

    if-nez v28, :cond_2

    .line 578
    const/high16 v28, 0x3f800000    # 1.0f

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, -0x1

    move-object/from16 v0, v21

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    move/from16 v4, v31

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 595
    :cond_2
    :goto_6
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getHeight()I

    move-result v12

    .line 598
    .local v12, "hight":I
    sget-boolean v28, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v28, :cond_e

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsEasyMode:Z

    move/from16 v28, v0

    if-eqz v28, :cond_e

    .line 599
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v28

    move/from16 v0, v28

    float-to-double v0, v0

    move-wide/from16 v28, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-int v0, v0

    move/from16 v28, v0

    add-int/lit8 v27, v28, 0x14

    .line 604
    .local v27, "width":I
    :goto_7
    move-object/from16 v0, v23

    move/from16 v1, v27

    invoke-virtual {v0, v1, v12}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setSize(II)V

    .line 647
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->titleDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v28, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 654
    .end local v9    # "dimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    .end local v11    # "fontType":Ljava/lang/String;
    .end local v12    # "hight":I
    .end local v14    # "label":Ljava/lang/String;
    .end local v15    # "leftMargin":I
    .end local v16    # "mKaiti":Z
    .end local v17    # "paint":Landroid/text/TextPaint;
    .end local v19    # "sPath":Ljava/lang/String;
    .end local v20    # "textColor":I
    .end local v21    # "textPaint":Landroid/text/TextPaint;
    .end local v22    # "textSize":F
    .end local v23    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    .end local v24    # "textW":F
    .end local v25    # "topMargin":I
    .end local v27    # "width":I
    :goto_8
    return-void

    .line 493
    .end local v6    # "albumName":Ljava/lang/String;
    .end local v7    # "albumNumber":Ljava/lang/String;
    .end local v8    # "bottomMargin":I
    .end local v13    # "itemCount":I
    .end local v18    # "postfixSize":F
    :cond_3
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 494
    .restart local v13    # "itemCount":I
    :cond_4
    const-string v6, ""

    goto/16 :goto_1

    .line 526
    .restart local v6    # "albumName":Ljava/lang/String;
    .restart local v7    # "albumNumber":Ljava/lang/String;
    .restart local v8    # "bottomMargin":I
    .restart local v9    # "dimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    .restart local v15    # "leftMargin":I
    .restart local v18    # "postfixSize":F
    .restart local v22    # "textSize":F
    .restart local v24    # "textW":F
    .restart local v25    # "topMargin":I
    :cond_5
    :try_start_3
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumFontColor()I

    move-result v20

    .restart local v20    # "textColor":I
    goto/16 :goto_2

    .line 529
    .end local v20    # "textColor":I
    .end local v22    # "textSize":F
    .end local v24    # "textW":F
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mViewMode:I

    move/from16 v28, v0

    move/from16 v0, v28

    invoke-virtual {v9, v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewAlbumFontSize(I)I

    move-result v28

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v22, v0

    .line 530
    .restart local v22    # "textSize":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mViewMode:I

    move/from16 v28, v0

    move/from16 v0, v28

    invoke-virtual {v9, v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewAlbumCountFontSize(I)I

    move-result v28

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v18, v0

    .line 531
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    const-string v29, "albumViewMode"

    const/16 v30, 0x0

    invoke-static/range {v28 .. v30}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v28

    move/from16 v0, v28

    invoke-virtual {v9, v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxWidthPixel(I)I

    move-result v28

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v24, v0

    .line 533
    .restart local v24    # "textW":F
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewAlbumFontColor()I
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    move-result v20

    .restart local v20    # "textColor":I
    goto/16 :goto_2

    .line 545
    .restart local v11    # "fontType":Ljava/lang/String;
    .restart local v16    # "mKaiti":Z
    .restart local v19    # "sPath":Ljava/lang/String;
    :cond_7
    :try_start_4
    const-string v28, "/"

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v28

    move-object/from16 v0, v19

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v28

    const-string v29, "/Kaiti"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_1

    move-result v28

    if-eqz v28, :cond_8

    .line 546
    const/16 v16, 0x1

    .line 547
    const/high16 v18, 0x42080000    # 34.0f

    goto/16 :goto_3

    .line 549
    :cond_8
    const/16 v16, 0x0

    goto/16 :goto_3

    .line 550
    :catch_0
    move-exception v10

    .line 551
    .local v10, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    .line 552
    const/16 v16, 0x0

    goto/16 :goto_3

    .line 562
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v14    # "label":Ljava/lang/String;
    .restart local v17    # "paint":Landroid/text/TextPaint;
    :cond_9
    const/high16 v28, 0x430c0000    # 140.0f

    add-float v28, v28, v24

    move/from16 v0, v28

    move-object/from16 v1, v17

    invoke-static {v6, v7, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlTextView;->ellipsizeString(Ljava/lang/String;Ljava/lang/String;FLandroid/text/TextPaint;)Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_4

    .line 575
    .restart local v21    # "textPaint":Landroid/text/TextPaint;
    .restart local v23    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    :cond_a
    invoke-static {}, Lcom/sec/samsung/gallery/util/FontUtil;->getRobotoLightFont()Landroid/graphics/Typeface;

    move-result-object v28

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_5

    .line 649
    .end local v9    # "dimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    .end local v11    # "fontType":Ljava/lang/String;
    .end local v14    # "label":Ljava/lang/String;
    .end local v15    # "leftMargin":I
    .end local v16    # "mKaiti":Z
    .end local v17    # "paint":Landroid/text/TextPaint;
    .end local v19    # "sPath":Ljava/lang/String;
    .end local v20    # "textColor":I
    .end local v21    # "textPaint":Landroid/text/TextPaint;
    .end local v22    # "textSize":F
    .end local v23    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    .end local v24    # "textW":F
    .end local v25    # "topMargin":I
    :catch_1
    move-exception v10

    .line 650
    .local v10, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v10}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_8

    .line 581
    .end local v10    # "e":Ljava/lang/NullPointerException;
    .restart local v9    # "dimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    .restart local v11    # "fontType":Ljava/lang/String;
    .restart local v14    # "label":Ljava/lang/String;
    .restart local v15    # "leftMargin":I
    .restart local v16    # "mKaiti":Z
    .restart local v17    # "paint":Landroid/text/TextPaint;
    .restart local v19    # "sPath":Ljava/lang/String;
    .restart local v20    # "textColor":I
    .restart local v22    # "textSize":F
    .restart local v23    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    .restart local v24    # "textW":F
    .restart local v25    # "topMargin":I
    :cond_b
    :try_start_6
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v21

    .line 582
    .restart local v21    # "textPaint":Landroid/text/TextPaint;
    move-object/from16 v0, v21

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 583
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v28

    if-eqz v28, :cond_d

    .line 584
    invoke-static {}, Lcom/sec/samsung/gallery/util/FontUtil;->getRegularFont()Landroid/graphics/Typeface;

    move-result-object v28

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 587
    :goto_9
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v28

    if-nez v28, :cond_c

    .line 588
    const/high16 v28, 0x3f800000    # 1.0f

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, -0x1

    move-object/from16 v0, v21

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    move/from16 v4, v31

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 590
    :cond_c
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setTextSize(F)V

    .line 591
    move-object/from16 v0, v23

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setColor(I)V

    .line 592
    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setText(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_6

    .line 651
    .end local v9    # "dimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    .end local v11    # "fontType":Ljava/lang/String;
    .end local v14    # "label":Ljava/lang/String;
    .end local v15    # "leftMargin":I
    .end local v16    # "mKaiti":Z
    .end local v17    # "paint":Landroid/text/TextPaint;
    .end local v19    # "sPath":Ljava/lang/String;
    .end local v20    # "textColor":I
    .end local v21    # "textPaint":Landroid/text/TextPaint;
    .end local v22    # "textSize":F
    .end local v23    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    .end local v24    # "textW":F
    .end local v25    # "topMargin":I
    :catch_2
    move-exception v10

    .line 652
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_8

    .line 586
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v9    # "dimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    .restart local v11    # "fontType":Ljava/lang/String;
    .restart local v14    # "label":Ljava/lang/String;
    .restart local v15    # "leftMargin":I
    .restart local v16    # "mKaiti":Z
    .restart local v17    # "paint":Landroid/text/TextPaint;
    .restart local v19    # "sPath":Ljava/lang/String;
    .restart local v20    # "textColor":I
    .restart local v21    # "textPaint":Landroid/text/TextPaint;
    .restart local v22    # "textSize":F
    .restart local v23    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    .restart local v24    # "textW":F
    .restart local v25    # "topMargin":I
    :cond_d
    :try_start_7
    invoke-static {}, Lcom/sec/samsung/gallery/util/FontUtil;->getRobotoLightFont()Landroid/graphics/Typeface;

    move-result-object v28

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_9

    .line 600
    .restart local v12    # "hight":I
    :cond_e
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v28

    if-nez v28, :cond_f

    .line 601
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v28

    move/from16 v0, v28

    float-to-double v0, v0

    move-wide/from16 v28, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-int v0, v0

    move/from16 v28, v0

    add-int/lit8 v27, v28, 0x5

    .restart local v27    # "width":I
    goto/16 :goto_7

    .line 603
    .end local v27    # "width":I
    :cond_f
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v28

    move/from16 v0, v28

    float-to-double v0, v0

    move-wide/from16 v28, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->ceil(D)D
    :try_end_7
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-int v0, v0

    move/from16 v27, v0

    .restart local v27    # "width":I
    goto/16 :goto_7
.end method

.method private drawNewMark(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 27
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 365
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 417
    :cond_0
    :goto_0
    return-void

    .line 368
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v9

    .line 371
    .local v9, "bucketId":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getUserSelectedAlbum()[Ljava/lang/String;

    move-result-object v26

    .line 372
    .local v26, "userSelect":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getLatestAlbumInfo()I

    move-result v16

    .line 373
    .local v16, "latestAlbum":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 375
    .local v11, "dimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    const/4 v2, 0x0

    aget-object v2, v26, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    .line 376
    .local v22, "userBucketId":I
    const/16 v2, 0x2d

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v19

    check-cast v19, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 377
    .local v19, "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    const/16 v2, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 378
    .local v8, "NewBG":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e04ad

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 379
    .local v17, "newLabel":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNewLabelFontSize()I

    move-result v18

    .line 380
    .local v18, "textSize":I
    invoke-virtual {v11}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewAlbumFontColor()I

    move-result v5

    .line 381
    .local v5, "textColor":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    const-string v3, "albumViewMode"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v11, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxWidthPixel(I)I

    move-result v20

    .line 383
    .local v20, "textW":I
    move/from16 v0, v16

    if-ne v9, v0, :cond_5

    .line 384
    if-nez v8, :cond_2

    .line 385
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020110

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v14

    .line 387
    .local v14, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move/from16 v0, v18

    int-to-float v4, v0

    move/from16 v0, v20

    int-to-float v6, v0

    const/4 v7, 0x1

    invoke-static/range {v2 .. v7}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;Ljava/lang/String;FIFZ)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v19

    .line 388
    const/4 v2, 0x2

    const/4 v3, 0x1

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 389
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v15

    .line 390
    .local v15, "fm":Landroid/graphics/Paint$FontMetrics;
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget v3, v15, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget v4, v15, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    float-to-int v0, v2

    move/from16 v21, v0

    .line 391
    .local v21, "topMargin":I
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 392
    new-instance v8, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v8    # "NewBG":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v8, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 393
    .restart local v8    # "NewBG":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v8, v14}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 394
    const/4 v2, 0x3

    const/4 v3, 0x1

    invoke-virtual {v8, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 396
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getWidth()I

    move-result v2

    invoke-virtual {v11}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNewLabelPadding()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    invoke-virtual {v11}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNewLabelHeight()I

    move-result v3

    invoke-virtual {v8, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 397
    const/4 v2, 0x0

    invoke-virtual {v11}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNewLabelTopMargin()I

    move-result v3

    invoke-virtual {v11}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNewLabelRightMargin()I

    move-result v4

    const/4 v6, 0x0

    invoke-virtual {v8, v2, v3, v4, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 398
    const/16 v2, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 399
    const/16 v2, 0x2d

    move-object/from16 v0, v19

    invoke-virtual {v8, v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 401
    .end local v14    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v15    # "fm":Landroid/graphics/Paint$FontMetrics;
    .end local v21    # "topMargin":I
    :cond_2
    move/from16 v0, v22

    move/from16 v1, v16

    if-ne v0, v1, :cond_4

    .line 402
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v10

    .line 403
    .local v10, "currentItemId":I
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v12

    .line 404
    .local v12, "currentItemTakenTime":J
    const/4 v2, 0x1

    aget-object v2, v26, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v23

    .line 405
    .local v23, "userItemId":I
    const/4 v2, 0x2

    aget-object v2, v26, v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v24

    .line 406
    .local v24, "userItemTakenTime":J
    move/from16 v0, v23

    if-le v10, v0, :cond_3

    cmp-long v2, v12, v24

    if-gtz v2, :cond_4

    .line 407
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {v8, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 411
    .end local v10    # "currentItemId":I
    .end local v12    # "currentItemTakenTime":J
    .end local v23    # "userItemId":I
    .end local v24    # "userItemTakenTime":J
    :cond_4
    const/4 v2, 0x0

    invoke-virtual {v8, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 413
    :cond_5
    if-eqz v8, :cond_0

    .line 414
    const/4 v2, 0x1

    invoke-virtual {v8, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private drawSelectedCountView(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 23
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 1114
    const/16 v19, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v14

    check-cast v14, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 1115
    .local v14, "selectedTextView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    const/16 v19, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v11

    check-cast v11, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 1117
    .local v11, "selectedBgView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const/4 v12, 0x0

    .line 1118
    .local v12, "selectedCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumSelectCountFontColor()I

    move-result v6

    .line 1119
    .local v6, "fontColor":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumSelectCountFontSize()I

    move-result v7

    .line 1121
    .local v7, "fontSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    move-object/from16 v19, v0

    if-eqz v19, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v19, v0

    if-eqz v19, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v19

    if-eqz v19, :cond_1

    .line 1124
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v19

    move-object/from16 v0, v19

    instance-of v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    move/from16 v19, v0

    if-eqz v19, :cond_0

    .line 1125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v12

    .line 1126
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setCurrentMediaSetSelectedCount(I)V

    .line 1129
    :cond_1
    if-nez v12, :cond_4

    .line 1130
    if-eqz v14, :cond_2

    .line 1131
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setVisibility(I)V

    .line 1134
    :cond_2
    if-eqz v11, :cond_3

    .line 1135
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 1194
    :cond_3
    :goto_0
    return-void

    .line 1139
    :cond_4
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v8

    .line 1140
    .local v8, "num":Ljava/text/NumberFormat;
    int-to-long v0, v12

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    .line 1141
    .local v15, "selectedstr1":Ljava/lang/Long;
    invoke-virtual {v8, v15}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 1142
    .local v13, "selectedStr":Ljava/lang/String;
    int-to-float v0, v7

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-static {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getDefaultPaint(FI)Landroid/text/TextPaint;

    move-result-object v9

    .line 1143
    .local v9, "paint":Landroid/text/TextPaint;
    invoke-virtual {v9, v13}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v19

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v0, v0

    move/from16 v17, v0

    .line 1144
    .local v17, "textboxW":I
    const-string v19, "  "

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v19

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v4, v0

    .line 1145
    .local v4, "bgWidth":I
    const/4 v5, 0x0

    .line 1146
    .local v5, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v19

    if-eqz v19, :cond_5

    .line 1147
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f020209

    invoke-virtual/range {v19 .. v21}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 1151
    :goto_1
    add-int v19, v17, v4

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_6

    .line 1152
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    .line 1157
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumSelectCountTopPaddingPixel()I

    move-result v18

    .line 1158
    .local v18, "topPadding":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumSelectCountRightPaddingPixel()I

    move-result v10

    .line 1159
    .local v10, "rightPadding":I
    if-nez v11, :cond_7

    .line 1160
    new-instance v11, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v11    # "selectedBgView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-direct {v11, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 1161
    .restart local v11    # "selectedBgView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const/16 v19, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v11, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 1162
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 1163
    const/16 v19, 0x3

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v11, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 1164
    const/16 v19, 0x0

    const/16 v20, 0x0

    move/from16 v0, v19

    move/from16 v1, v18

    move/from16 v2, v20

    invoke-virtual {v11, v0, v1, v10, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 1165
    invoke-virtual {v11, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1167
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v11, v4, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 1168
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 1174
    :goto_3
    invoke-virtual {v9}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->descent:F

    move/from16 v19, v0

    invoke-virtual {v9}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v20

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    move/from16 v20, v0

    sub-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v16, v0

    .line 1175
    .local v16, "textHeight":I
    if-nez v14, :cond_8

    .line 1176
    int-to-float v0, v7

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-static {v13, v0, v6}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v14

    .line 1177
    invoke-virtual {v14}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v19

    invoke-virtual {v9}, Landroid/text/TextPaint;->getFlags()I

    move-result v20

    or-int/lit8 v20, v20, 0x20

    invoke-virtual/range {v19 .. v20}, Landroid/text/TextPaint;->setFlags(I)V

    .line 1178
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v19

    move/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setSize(II)V

    .line 1179
    const/16 v19, 0x3

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v14, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 1180
    const/16 v19, 0x0

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v20

    sub-int v20, v20, v16

    div-int/lit8 v20, v20, 0x2

    add-int v20, v20, v18

    sub-int v21, v4, v17

    div-int/lit8 v21, v21, 0x2

    add-int v21, v21, v10

    const/16 v22, 0x0

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v14, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 1183
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setVisibility(I)V

    .line 1184
    const/16 v19, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v14, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    goto/16 :goto_0

    .line 1149
    .end local v10    # "rightPadding":I
    .end local v16    # "textHeight":I
    .end local v18    # "topPadding":I
    :cond_5
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f020208

    invoke-virtual/range {v19 .. v21}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    goto/16 :goto_1

    .line 1154
    :cond_6
    add-int v4, v4, v17

    goto/16 :goto_2

    .line 1170
    .restart local v10    # "rightPadding":I
    .restart local v18    # "topPadding":I
    :cond_7
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v11, v4, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 1171
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 1186
    .restart local v16    # "textHeight":I
    :cond_8
    const/16 v19, 0x0

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v20

    sub-int v20, v20, v16

    div-int/lit8 v20, v20, 0x2

    add-int v20, v20, v18

    sub-int v21, v4, v17

    div-int/lit8 v21, v21, 0x2

    add-int v21, v21, v10

    const/16 v22, 0x0

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v14, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 1189
    int-to-float v0, v7

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setTextSize(F)V

    .line 1190
    invoke-virtual {v14, v6}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setColor(I)V

    .line 1191
    invoke-virtual {v14, v13}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setText(Ljava/lang/String;)V

    .line 1192
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public static getImageResourceId(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)I
    .locals 5
    .param p0, "media"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const v3, 0x7f020119

    const v1, 0x7f020103

    const v2, 0x7f020102

    .line 940
    instance-of v4, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;

    if-nez v4, :cond_0

    instance-of v4, p1, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-eqz v4, :cond_2

    .line 941
    :cond_0
    const v1, 0x7f0200da

    .line 985
    .end local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    :goto_0
    return v1

    .line 942
    .restart local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    instance-of v4, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;

    if-nez v4, :cond_3

    instance-of v4, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;

    if-nez v4, :cond_3

    instance-of v4, p1, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-nez v4, :cond_3

    instance-of v4, p1, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-eqz v4, :cond_5

    .line 944
    :cond_3
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaidu:Z

    if-nez v1, :cond_4

    .line 945
    const v1, 0x7f0200d9

    goto :goto_0

    .line 947
    :cond_4
    const v1, 0x7f0200d8

    goto :goto_0

    .line 948
    :cond_5
    instance-of v4, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    if-nez v4, :cond_6

    instance-of v4, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v4, :cond_7

    .line 949
    :cond_6
    const v1, 0x7f0201e2

    goto :goto_0

    .line 952
    :cond_7
    instance-of v4, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;

    if-nez v4, :cond_8

    instance-of v4, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-nez v4, :cond_8

    instance-of v4, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-nez v4, :cond_8

    instance-of v4, p1, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-eqz v4, :cond_11

    .line 954
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v0

    .line 955
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_9

    const-string v4, ""

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 956
    :cond_9
    if-eqz p1, :cond_d

    .line 957
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 962
    :cond_a
    :goto_1
    instance-of v4, p0, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v4, :cond_b

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->isCameraAlbum()Z

    move-result v4

    if-nez v4, :cond_c

    :cond_b
    if-eqz p1, :cond_e

    instance-of v4, p1, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v4, :cond_e

    check-cast p1, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .end local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->isCameraItem()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 965
    :cond_c
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardCameraPath(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    move v1, v2

    goto :goto_0

    .line 959
    .restart local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_d
    const/4 v0, 0x0

    goto :goto_1

    .line 967
    .end local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_e
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardPath(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    move v1, v3

    .line 968
    goto :goto_0

    .line 970
    :cond_f
    instance-of v4, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;

    if-eqz v4, :cond_13

    .line 971
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isCameraPath(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10

    move v1, v2

    .line 972
    goto/16 :goto_0

    .line 973
    :cond_10
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardCameraPath(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 975
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardPath(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    move v1, v3

    .line 976
    goto/16 :goto_0

    .line 980
    .end local v0    # "path":Ljava/lang/String;
    .restart local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_11
    instance-of v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;

    if-nez v1, :cond_12

    instance-of v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;

    if-nez v1, :cond_12

    instance-of v1, p1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    if-nez v1, :cond_12

    instance-of v1, p1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    if-eqz v1, :cond_13

    .line 982
    :cond_12
    const v1, 0x7f0200dd

    goto/16 :goto_0

    .line 985
    .end local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_13
    const/4 v1, -0x1

    goto/16 :goto_0
.end method

.method private setBitmap(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 12
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/16 v2, 0x140

    .line 1029
    const/4 v1, 0x0

    .line 1031
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    if-ltz p2, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-eqz v0, :cond_0

    .line 1032
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0, p2}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1035
    :cond_0
    if-nez v1, :cond_1

    .line 1036
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->readyDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1050
    .end local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_0
    return-void

    .line 1038
    .restart local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p3, :cond_2

    invoke-virtual {p3}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1039
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->getBrokenPictureThumbnail(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1041
    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v4

    .line 1042
    .local v4, "rotation":I
    :goto_1
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceThumbnail:Z

    if-eqz v0, :cond_4

    if-eqz p3, :cond_4

    instance-of v0, p3, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v0, :cond_4

    invoke-static {p3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1044
    check-cast p3, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/data/LocalImage;->getRectOfAllFaces()Landroid/graphics/RectF;

    move-result-object v5

    .local v5, "faceRect":Landroid/graphics/RectF;
    move-object v0, p1

    move v3, v2

    .line 1045
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setCroppedBitmap(Landroid/graphics/Bitmap;IIILandroid/graphics/RectF;)V

    goto :goto_0

    .line 1041
    .end local v4    # "rotation":I
    .end local v5    # "faceRect":Landroid/graphics/RectF;
    .restart local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_3
    const/4 v4, 0x0

    goto :goto_1

    .line 1047
    .restart local v4    # "rotation":I
    :cond_4
    const/4 v11, 0x0

    move-object v6, p1

    move-object v7, v1

    move v8, v2

    move v9, v2

    move v10, v4

    invoke-virtual/range {v6 .. v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setCroppedBitmap(Landroid/graphics/Bitmap;IIILandroid/graphics/RectF;)V

    goto :goto_0
.end method

.method private setImageViewPadding(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V
    .locals 4
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "itemCount"    # I

    .prologue
    const/4 v1, 0x0

    .line 863
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isJEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsPhotoViewState:Z

    if-eqz v0, :cond_0

    .line 864
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v0, v0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->photoview_image_bottom_margine:I

    invoke-virtual {p1, v1, v1, v1, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    .line 877
    :goto_0
    return-void

    .line 865
    :cond_0
    const/4 v0, 0x1

    if-gt p2, v0, :cond_1

    .line 866
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v0, v0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_left_margin:I

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_top_margin:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_right_margin:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_bottom_margin:I

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    goto :goto_0

    .line 869
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v0, 0xf

    if-ge p2, v0, :cond_2

    .line 870
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v0, v0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_2_left_margin:I

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_2_top_margin:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_2_right_margin:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_2_bottom_margin:I

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    goto :goto_0

    .line 874
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v0, v0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_3_left_margin:I

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_3_top_margin:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_3_right_margin:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_3_bottom_margin:I

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    goto :goto_0
.end method

.method private updateCurrentFocus()Z
    .locals 7

    .prologue
    .line 1400
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mFocusedMedia:Lcom/sec/android/gallery3d/data/MediaObject;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mFocusAlbumFilePath:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 1401
    :cond_0
    const/4 v1, 0x1

    .line 1419
    :goto_0
    return v1

    .line 1404
    :cond_1
    const/4 v1, 0x0

    .line 1405
    .local v1, "findCurrentMediaItem":Z
    sget-object v6, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v6

    .line 1406
    :try_start_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v0

    .line 1407
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    move v4, v0

    .local v4, "n":I
    :goto_1
    if-ge v2, v4, :cond_2

    .line 1408
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v5, v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 1409
    .local v3, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v3, :cond_3

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mFocusedMedia:Lcom/sec/android/gallery3d/data/MediaObject;

    if-ne v3, v5, :cond_3

    .line 1410
    const/4 v1, 0x1

    .line 1411
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getFocusedIndex()I

    move-result v5

    if-eq v5, v2, :cond_2

    .line 1412
    const/4 v5, 0x0

    invoke-virtual {p0, v2, v5}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->setFocusedIndex(IZ)V

    .line 1417
    .end local v3    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_2
    monitor-exit v6

    goto :goto_0

    .end local v0    # "count":I
    .end local v2    # "i":I
    .end local v4    # "n":I
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 1407
    .restart local v0    # "count":I
    .restart local v2    # "i":I
    .restart local v3    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .restart local v4    # "n":I
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private updateNewAlbumFocus()V
    .locals 6

    .prologue
    .line 1423
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mFocusAlbumFilePath:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 1448
    :goto_0
    return-void

    .line 1426
    :cond_0
    sget-object v5, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v5

    .line 1427
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1428
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 1430
    const/4 v3, 0x0

    .line 1431
    .local v3, "path":Ljava/lang/String;
    :try_start_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v4, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 1432
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_1

    .line 1433
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v3

    .line 1435
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mFocusAlbumFilePath:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1437
    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mNewAlbumIndex:I

    .line 1438
    const/4 v4, 0x0

    invoke-virtual {p0, v1, v4}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->setFocusedIndex(IZ)V

    .line 1439
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mFocusedMedia:Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1440
    const/4 v4, 0x2

    invoke-virtual {p0, v1, v4}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->notifyDataSetChanged(II)V

    .line 1441
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mFocusAlbumFilePath:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1447
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v3    # "path":Ljava/lang/String;
    :cond_2
    :try_start_2
    monitor-exit v5

    goto :goto_0

    .end local v0    # "count":I
    .end local v1    # "i":I
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 1444
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    .restart local v3    # "path":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 1428
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method protected addContentListener()V
    .locals 2

    .prologue
    .line 1452
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 1453
    return-void
.end method

.method public changeSource()V
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsChangingSource:Z

    .line 158
    return-void
.end method

.method public changeSource(Lcom/sec/android/gallery3d/data/MediaSet;Z)V
    .locals 1
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "reload"    # Z

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eq p1, v0, :cond_0

    .line 168
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsChangingSource:Z

    .line 169
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->notifyDataSetChanged()V

    .line 172
    :cond_0
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 173
    if-eqz p2, :cond_1

    .line 174
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->reloadData()V

    .line 175
    :cond_1
    return-void
.end method

.method protected clearSlot(I)V
    .locals 2
    .param p1, "slotIndex"    # I

    .prologue
    .line 1216
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->clearSlot(I)V

    .line 1217
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mCoverData:[Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    .line 1218
    return-void
.end method

.method public drawCheckboxAndMark(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V
    .locals 7
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I

    .prologue
    const/4 v3, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 672
    if-nez p1, :cond_1

    .line 706
    :cond_0
    :goto_0
    return-void

    .line 675
    :cond_1
    invoke-virtual {p1, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 678
    .local v0, "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 680
    if-nez v0, :cond_2

    .line 681
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 682
    .restart local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {p1, v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 684
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 685
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v2, :cond_4

    const v2, 0x7f020122

    :goto_1
    invoke-virtual {v3, v4, v2}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 693
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    :goto_2
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 694
    invoke-virtual {v0, v6, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 695
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 696
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 697
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->select_mode_checkbox_left_margin:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->select_mode_checkbox_top_margin:I

    invoke-virtual {v0, v2, v3, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 699
    :cond_3
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 700
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0

    .line 685
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_4
    const v2, 0x7f0204ad

    goto :goto_1

    .line 689
    :cond_5
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v2, :cond_6

    const v2, 0x7f020121

    :goto_3
    invoke-virtual {v3, v4, v2}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .restart local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    goto :goto_2

    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_6
    const v2, 0x7f0204a6

    goto :goto_3

    .line 702
    :cond_7
    if-eqz v0, :cond_0

    .line 703
    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public drawDragMark(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V
    .locals 8
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I

    .prologue
    const/4 v5, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 739
    if-nez p1, :cond_1

    .line 775
    :cond_0
    :goto_0
    return-void

    .line 742
    :cond_1
    invoke-virtual {p1, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 745
    .local v0, "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsEnabledSelectionMode:Z

    if-eqz v4, :cond_4

    .line 748
    const v2, 0x7f02002b

    .line 749
    .local v2, "checkOnId":I
    const v1, 0x7f02002b

    .line 751
    .local v1, "checkOffId":I
    if-nez v0, :cond_2

    .line 752
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 753
    .restart local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {p1, v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 756
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 757
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 763
    .local v3, "drawable":Landroid/graphics/drawable/Drawable;
    :goto_1
    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 764
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 765
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v4, v4, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->edit_mode_checkbox_top_margine:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v5, v5, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->edit_mode_checkbox_right_margine:I

    invoke-virtual {v0, v6, v4, v5, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 767
    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 768
    const/4 v4, 0x3

    invoke-virtual {v0, v4, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 769
    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0

    .line 760
    .end local v3    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_3
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .restart local v3    # "drawable":Landroid/graphics/drawable/Drawable;
    goto :goto_1

    .line 771
    .end local v1    # "checkOffId":I
    .end local v2    # "checkOnId":I
    .end local v3    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_4
    if-eqz v0, :cond_0

    .line 772
    invoke-virtual {v0, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public declared-synchronized drawFocusBox(Lcom/sec/android/gallery3d/glcore/GlImageView;IIZ)V
    .locals 3
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I
    .param p3, "itemCount"    # I
    .param p4, "show"    # Z

    .prologue
    .line 657
    monitor-enter p0

    const/4 v2, 0x5

    :try_start_0
    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 658
    .local v0, "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mGenericMotionFocus:I

    if-ne v2, p2, :cond_2

    if-eqz p4, :cond_2

    .line 659
    if-nez v0, :cond_0

    .line 660
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 661
    .restart local v0    # "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const v1, 0x7f020545

    .line 662
    .local v1, "selectedThumbnailImageId":I
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 663
    const/4 v2, 0x5

    invoke-virtual {p1, v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 665
    .end local v1    # "selectedThumbnailImageId":I
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 669
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 666
    :cond_2
    if-eqz v0, :cond_1

    .line 667
    const/4 v2, 0x1

    :try_start_1
    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 657
    .end local v0    # "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public drawMediaTypeIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;I)V
    .locals 10
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p4, "itemCount"    # I

    .prologue
    const/16 v7, 0xe

    const/4 v5, -0x1

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1054
    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 1055
    .local v4, "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_3

    invoke-static {p2, p3}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getImageResourceId(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v3

    .line 1057
    .local v3, "resId":I
    :goto_0
    const/4 v2, 0x0

    .line 1058
    .local v2, "leftMargin":I
    const/4 v0, 0x0

    .line 1060
    .local v0, "bottomMargin":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    if-eqz v6, :cond_0

    .line 1061
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewMediaTypeIconLeftMargin()I

    move-result v2

    .line 1062
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewMediaTypeIconBottomMargin()I

    move-result v0

    .line 1064
    :cond_0
    if-eq v3, v5, :cond_4

    .line 1065
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1067
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v4, :cond_1

    .line 1068
    new-instance v4, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v4    # "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 1069
    .restart local v4    # "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const/4 v5, 0x3

    invoke-virtual {v4, v9, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 1070
    invoke-virtual {p1, v4, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 1072
    :cond_1
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    int-to-float v5, v5

    iget v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mBadgeRatio:F

    mul-float/2addr v5, v6

    float-to-int v5, v5

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    int-to-float v6, v6

    iget v7, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mBadgeRatio:F

    mul-float/2addr v6, v7

    float-to-int v6, v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 1073
    invoke-virtual {v4, v2, v8, v8, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 1074
    invoke-virtual {v4, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 1075
    iget v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIconLeft:I

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    add-int/2addr v5, v6

    iput v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIconLeft:I

    .line 1076
    invoke-virtual {v4, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1077
    invoke-virtual {v4, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 1081
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_2
    :goto_1
    return-void

    .end local v0    # "bottomMargin":I
    .end local v2    # "leftMargin":I
    .end local v3    # "resId":I
    :cond_3
    move v3, v5

    .line 1055
    goto :goto_0

    .line 1078
    .restart local v0    # "bottomMargin":I
    .restart local v2    # "leftMargin":I
    .restart local v3    # "resId":I
    :cond_4
    if-eqz v4, :cond_2

    .line 1079
    invoke-virtual {v4, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public drawReorderIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V
    .locals 6
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I

    .prologue
    const/16 v3, 0x2e

    const/4 v5, 0x1

    .line 709
    if-nez p1, :cond_1

    .line 736
    :cond_0
    :goto_0
    return-void

    .line 712
    :cond_1
    invoke-virtual {p1, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 715
    .local v1, "reorderIcon":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsReorderAlbum:Z

    if-eqz v2, :cond_3

    .line 716
    if-nez v1, :cond_2

    .line 717
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v1    # "reorderIcon":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 718
    .restart local v1    # "reorderIcon":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {p1, v1, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 720
    :cond_2
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0204c6

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 723
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 724
    invoke-virtual {v1, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 725
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 729
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 730
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0

    .line 732
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_3
    if-eqz v1, :cond_0

    .line 733
    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public drawSecretboxIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 8
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const/16 v7, 0x1e

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1084
    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 1085
    .local v1, "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_2

    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->isSecretbox(Lcom/sec/android/gallery3d/data/MediaSet;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1086
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200d7

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1088
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v1, :cond_0

    .line 1089
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v1    # "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 1090
    .restart local v1    # "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const/4 v2, 0x3

    invoke-virtual {v1, v6, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 1091
    invoke-virtual {p1, v1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 1093
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mBadgeRatio:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mBadgeRatio:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 1094
    invoke-virtual {v1, v5, v5, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 1095
    invoke-virtual {v1, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 1096
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1097
    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 1103
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    :goto_0
    return-void

    .line 1100
    :cond_2
    if-eqz v1, :cond_1

    .line 1101
    invoke-virtual {v1, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public drawSelectedAlbumFrame(Lcom/sec/android/gallery3d/glcore/GlImageView;IIZ)V
    .locals 3
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I
    .param p3, "itemCount"    # I
    .param p4, "bSelected"    # Z

    .prologue
    const/16 v2, 0x21

    .line 458
    if-nez p1, :cond_1

    .line 469
    :cond_0
    :goto_0
    return-void

    .line 460
    :cond_1
    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 462
    .local v0, "selectedFrameView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-nez v0, :cond_2

    .line 463
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "selectedFrameView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 464
    .restart local v0    # "selectedFrameView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const v1, 0x7f02020a

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 465
    invoke-virtual {p1, v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 467
    :cond_2
    if-eqz v0, :cond_0

    .line 468
    if-eqz p4, :cond_3

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public drawUnconfirmIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 9
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "itemCount"    # I

    .prologue
    const/4 v8, 0x7

    const/4 v7, 0x0

    const/4 v6, 0x3

    const/4 v5, 0x1

    .line 909
    invoke-virtual {p1, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 910
    .local v1, "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_4

    instance-of v2, p2, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    if-eqz v2, :cond_4

    .line 911
    check-cast p2, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    .end local p2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getMediaSetType()I

    move-result v2

    if-nez v2, :cond_3

    .line 912
    if-nez v1, :cond_0

    .line 913
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200de

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 915
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v1    # "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 916
    .restart local v1    # "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 917
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 918
    invoke-virtual {v1, v6, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 923
    :goto_0
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 925
    invoke-virtual {v1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 926
    invoke-virtual {p1, v1, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 928
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIconLeft:I

    add-int/lit8 v2, v2, 0x3f

    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIconLeft:I

    .line 929
    invoke-virtual {v1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 937
    :cond_1
    :goto_1
    return-void

    .line 920
    .restart local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_2
    invoke-virtual {v1, v5, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    goto :goto_0

    .line 930
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_3
    if-eqz v1, :cond_1

    .line 931
    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_1

    .line 934
    .restart local p2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_4
    if-eqz v1, :cond_1

    .line 935
    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public drawVideoPlayIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;IZ)V
    .locals 11
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p3, "itemCount"    # I
    .param p4, "show"    # Z

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 778
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v7

    const/4 v8, 0x4

    if-ne v7, v8, :cond_1

    instance-of v7, p2, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v7, :cond_1

    move v1, v5

    .line 780
    .local v1, "isVideo":Z
    :goto_0
    invoke-virtual {p1, v10}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 782
    .local v4, "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p4, :cond_3

    if-eqz v1, :cond_3

    .line 783
    if-nez v4, :cond_2

    .line 784
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0200f7

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 786
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v4, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v4    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v4, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 787
    .restart local v4    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v4, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 788
    invoke-virtual {v4, v9, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 789
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    int-to-float v6, v6

    iget v7, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mBadgeRatio:F

    mul-float/2addr v6, v7

    float-to-int v6, v6

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    int-to-float v7, v7

    iget v8, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mBadgeRatio:F

    mul-float/2addr v7, v8

    float-to-int v7, v7

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 790
    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 791
    invoke-virtual {p1, v4, v10}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 802
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    :goto_1
    return-void

    .end local v1    # "isVideo":Z
    .end local v4    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_1
    move v1, v6

    .line 778
    goto :goto_0

    .line 793
    .restart local v1    # "isVideo":Z
    .restart local v4    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_2
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 794
    .restart local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    .line 795
    .local v3, "toW":I
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    .line 796
    .local v2, "toH":I
    int-to-float v5, v3

    iget v7, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mRatio:F

    mul-float/2addr v5, v7

    iget v7, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mBadgeRatio:F

    mul-float/2addr v5, v7

    float-to-int v5, v5

    int-to-float v7, v2

    iget v8, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mRatio:F

    mul-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mAspectRatioCorrection:F

    mul-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mBadgeRatio:F

    mul-float/2addr v7, v8

    float-to-int v7, v7

    invoke-virtual {v4, v5, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 797
    invoke-virtual {v4, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_1

    .line 799
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v2    # "toH":I
    .end local v3    # "toW":I
    :cond_3
    if-eqz v4, :cond_0

    .line 800
    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 210
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsChangingSource:Z

    if-eqz v0, :cond_0

    .line 211
    const/4 v0, 0x0

    .line 213
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSize:I

    goto :goto_0
.end method

.method public getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 218
    const/4 v1, 0x0

    .line 221
    .local v1, "result":Lcom/sec/android/gallery3d/data/MediaItem;
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mThumbnailActiveStart:I

    if-ge p1, v2, :cond_0

    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mThumbnailActiveEnd:I

    if-lt p1, v2, :cond_0

    .line 222
    const-string v2, "MediaSetAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " getItem index ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mActiveStart ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mThumbnailActiveStart:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mActiveEnd ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mThumbnailActiveEnd:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    const/4 v2, 0x0

    .line 233
    :goto_0
    return-object v2

    .line 227
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mCoverData:[Lcom/sec/android/gallery3d/data/MediaItem;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    array-length v3, v3

    rem-int v3, p1, v3

    aget-object v1, v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move-object v2, v1

    .line 233
    goto :goto_0

    .line 228
    :catch_0
    move-exception v0

    .line 229
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 230
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemImage(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "position"    # I
    .param p2, "type"    # I

    .prologue
    .line 277
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSize:I

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-nez v0, :cond_1

    .line 278
    :cond_0
    const/4 v0, 0x0

    .line 280
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0, p1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getLatestAlbumInfo()I
    .locals 3

    .prologue
    .line 420
    const/4 v0, 0x0

    .line 421
    .local v0, "id":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    const-string v2, "latest_update_album"

    invoke-static {v1, v2, v0}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 422
    return v0
.end method

.method public getPriviousAlbum()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 445
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 446
    .local v3, "bucketids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v2, ""

    .line 449
    .local v2, "bucketString":Ljava/lang/String;
    const-string v7, ";"

    invoke-virtual {v2, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 450
    .local v1, "bucket":[Ljava/lang/String;
    move-object v0, v1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v6, v0, v4

    .line 451
    .local v6, "val":Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 450
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 453
    .end local v6    # "val":Ljava/lang/String;
    :cond_0
    return-object v3
.end method

.method public getSource()Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1

    .prologue
    .line 1461
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 238
    const/4 v2, 0x0

    .line 241
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mThumbnailActiveStart:I

    if-ge p1, v3, :cond_0

    iget v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mThumbnailActiveEnd:I

    if-lt p1, v3, :cond_0

    .line 242
    const-string v3, "MediaSetAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " getSubMediaSet index ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] mActiveStart ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mThumbnailActiveStart:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] mActiveEnd ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mThumbnailActiveEnd:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    const/4 v3, 0x0

    .line 255
    :goto_0
    return-object v3

    .line 247
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    array-length v4, v4

    rem-int v4, p1, v4

    aget-object v3, v3, v4

    move-object v0, v3

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    move-object v2, v0

    .line 248
    if-nez v2, :cond_1

    .line 249
    const-string v3, "MediaSetAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MediaSet is NULL: index ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    move-object v3, v2

    .line 255
    goto :goto_0

    .line 251
    :catch_0
    move-exception v1

    .line 252
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 253
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getSubMediaSetIndex(Lcom/sec/android/gallery3d/data/MediaSet;)I
    .locals 7
    .param p1, "set"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 259
    const/4 v3, -0x1

    .line 260
    .local v3, "index":I
    const/4 v4, 0x0

    .line 262
    .local v4, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    array-length v5, v5

    if-ge v2, v5, :cond_0

    .line 263
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    array-length v6, v6

    rem-int v6, v2, v6

    aget-object v5, v5, v6

    move-object v0, v5

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    move-object v4, v0

    .line 264
    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_1

    .line 265
    move v3, v2

    .line 272
    :cond_0
    :goto_1
    return v3

    .line 262
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 269
    :catch_0
    move-exception v1

    .line 270
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public getThumbSize()I
    .locals 1

    .prologue
    .line 1497
    const/4 v0, 0x2

    return v0
.end method

.method public getUserSelectedAlbum()[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 426
    const/4 v3, 0x3

    new-array v1, v3, [Ljava/lang/String;

    const-string v3, "-1"

    aput-object v3, v1, v5

    const/4 v3, 0x1

    const-string v4, "-1"

    aput-object v4, v1, v3

    const-string v3, "-1"

    aput-object v3, v1, v6

    .line 429
    .local v1, "info":[Ljava/lang/String;
    invoke-virtual {v1}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 431
    .local v2, "tempInfo":[Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    const-string/jumbo v4, "user_selected_album"

    invoke-static {v3, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadStringKey(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 433
    .local v0, "Albuminfo":Ljava/lang/String;
    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 434
    const-string v3, ";"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 436
    :cond_0
    array-length v3, v2

    if-ne v3, v6, :cond_1

    .line 437
    array-length v3, v2

    invoke-static {v2, v5, v1, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 441
    :goto_0
    return-object v1

    .line 439
    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "parentLayer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/4 v5, 0x0

    .line 285
    iget v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSize:I

    if-lt p1, v6, :cond_0

    .line 286
    const-string v5, "MediaSetAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getView position = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", size = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSize:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    const/4 v0, 0x0

    .line 321
    :goto_0
    return-object v0

    .line 290
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 291
    .local v3, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 292
    .local v2, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v1

    .line 295
    .local v1, "itemCount":I
    :goto_1
    const/4 v4, 0x0

    .line 297
    .local v4, "showIcon":Z
    iput v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIconLeft:I

    .line 298
    if-nez p2, :cond_5

    .line 299
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 304
    .local v0, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_2
    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->setImageViewPadding(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V

    .line 305
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isJEnabled()Z

    move-result v6

    if-nez v6, :cond_1

    .line 306
    invoke-direct {p0, v0, v3, v1}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawFrame(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 308
    :cond_1
    invoke-direct {p0, v0, v3, p1, v1}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawLabel(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;II)V

    .line 309
    if-eqz v2, :cond_2

    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->isPicasaImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 310
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v6

    if-nez v6, :cond_6

    const/4 v4, 0x1

    .line 312
    :cond_2
    :goto_3
    invoke-virtual {p0, v0, v2, v1, v4}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawVideoPlayIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;IZ)V

    .line 313
    invoke-direct {p0, v0, p1, v1}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawFocusBox(Lcom/sec/android/gallery3d/glcore/GlImageView;II)V

    .line 314
    invoke-virtual {p0, v0, v3, v1}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawUnconfirmIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 315
    invoke-direct {p0, v0, p1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->setBitmap(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/android/gallery3d/data/MediaItem;)V

    .line 316
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v5, :cond_3

    .line 317
    invoke-virtual {p0, v0, v3}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawSecretboxIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 318
    :cond_3
    invoke-virtual {p0, v0, v3, v2, v1}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawMediaTypeIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;I)V

    .line 319
    invoke-direct {p0, v0, v3}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawSelectedCountView(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0

    .end local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v1    # "itemCount":I
    .end local v4    # "showIcon":Z
    :cond_4
    move v1, v5

    .line 292
    goto :goto_1

    .restart local v1    # "itemCount":I
    .restart local v4    # "showIcon":Z
    :cond_5
    move-object v0, p2

    .line 301
    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_2

    :cond_6
    move v4, v5

    .line 310
    goto :goto_3
.end method

.method public getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p4, "ext"    # I
    .param p5, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 328
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSize:I

    if-lt p1, v1, :cond_1

    .line 329
    const-string v0, "MediaSetAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getView position = "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ", size = "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v7, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSize:I

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    const/4 v2, 0x0

    .line 361
    :cond_0
    :goto_0
    return-object v2

    .line 333
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 334
    .local v3, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    .line 335
    .local v4, "mediaItems":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v5

    .line 338
    .local v5, "itemCount":I
    :goto_1
    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIconLeft:I

    .line 339
    if-nez p2, :cond_6

    .line 340
    new-instance v2, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v2, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 345
    .local v2, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_2
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsPhotoViewState:Z

    if-eqz v1, :cond_8

    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mFocusedIndex:I

    if-ne p1, v1, :cond_7

    const/4 v6, 0x1

    .line 348
    .local v6, "bSelected":Z
    :goto_3
    invoke-virtual {v2, v0, v0, v0, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    .line 350
    instance-of v0, p5, Lcom/sec/samsung/gallery/glview/GlTextObject;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsPhotoViewState:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_NORMAL:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_3

    .line 353
    :cond_2
    invoke-direct {p0, v2, v3, v6}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawLabel(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;Z)V

    .line 354
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mFocusOnText:Z

    invoke-virtual {p0, v2, p1, v5, v0}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawFocusBox(Lcom/sec/android/gallery3d/glcore/GlImageView;IIZ)V

    .line 356
    :cond_3
    instance-of v0, p5, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-nez v0, :cond_4

    instance-of v0, p5, Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v0, :cond_0

    :cond_4
    move-object v0, p0

    move v1, p1

    .line 357
    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawBadge(ILcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;I)V

    .line 358
    instance-of v0, p5, Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v0, :cond_0

    .line 359
    invoke-virtual {p0, v2, p1, v5, v6}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->drawSelectedAlbumFrame(Lcom/sec/android/gallery3d/glcore/GlImageView;IIZ)V

    goto :goto_0

    .end local v2    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v5    # "itemCount":I
    .end local v6    # "bSelected":Z
    :cond_5
    move v5, v0

    .line 335
    goto :goto_1

    .restart local v5    # "itemCount":I
    :cond_6
    move-object v2, p2

    .line 342
    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v2    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_2

    :cond_7
    move v6, v0

    .line 345
    goto :goto_3

    :cond_8
    move v6, v0

    goto :goto_3
.end method

.method public hasLocalMediaSet()Z
    .locals 5

    .prologue
    .line 1479
    const/4 v1, 0x0

    .line 1480
    .local v1, "set":Lcom/sec/android/gallery3d/data/MediaObject;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    monitor-enter v4

    .line 1481
    const/4 v2, 0x0

    .local v2, "slotIndex":I
    :goto_0
    :try_start_0
    iget v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSize:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ge v2, v3, :cond_1

    .line 1483
    :try_start_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    aget-object v1, v3, v2

    .line 1484
    instance-of v3, v1, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v3, :cond_0

    .line 1485
    const/4 v3, 0x1

    :try_start_2
    monitor-exit v4

    .line 1492
    :goto_1
    return v3

    .line 1487
    :catch_0
    move-exception v0

    .line 1488
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1481
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1491
    :cond_1
    monitor-exit v4

    .line 1492
    const/4 v3, 0x0

    goto :goto_1

    .line 1491
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public isSecretbox(Lcom/sec/android/gallery3d/data/MediaSet;)Z
    .locals 1
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 1106
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/sec/android/gallery3d/data/FilterFolderSet;

    if-eqz v0, :cond_1

    .line 1108
    :cond_0
    const/4 v0, 0x1

    .line 1110
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadData()Z
    .locals 24

    .prologue
    .line 1299
    sget-object v21, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v21

    .line 1300
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v18

    .line 1301
    .local v18, "version":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v17

    .line 1302
    .local v17, "size":I
    monitor-exit v21
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1303
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    .line 1304
    .local v14, "modelListener":Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSourceVersion:J

    move-wide/from16 v20, v0

    const-wide/16 v22, -0x1

    cmp-long v20, v20, v22

    if-nez v20, :cond_1

    const/4 v7, 0x1

    .line 1305
    .local v7, "firstLoading":Z
    :goto_0
    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSourceVersion:J

    .line 1306
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsChangingSource:Z

    move/from16 v20, v0

    if-nez v20, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSize:I

    move/from16 v20, v0

    move/from16 v0, v20

    move/from16 v1, v17

    if-ne v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSize:I

    move/from16 v20, v0

    if-nez v20, :cond_2

    .line 1307
    :cond_0
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsChangingSource:Z

    .line 1308
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1, v7}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->setContentRange(IZ)V

    .line 1309
    if-eqz v14, :cond_2

    .line 1310
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->updateCurrentFocus()Z

    .line 1311
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSize:I

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-interface {v14, v0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->onSizeChanged(I)V

    .line 1312
    if-nez v17, :cond_2

    const-wide/16 v20, -0x1

    cmp-long v20, v18, v20

    if-nez v20, :cond_2

    .line 1313
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->resetAdapter()V

    .line 1314
    const/16 v20, -0x1

    move/from16 v0, v20

    invoke-interface {v14, v0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->onWindowContentChanged(I)V

    .line 1315
    const/16 v20, 0x1

    .line 1396
    :goto_1
    return v20

    .line 1302
    .end local v7    # "firstLoading":Z
    .end local v14    # "modelListener":Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;
    .end local v17    # "size":I
    .end local v18    # "version":J
    :catchall_0
    move-exception v20

    :try_start_1
    monitor-exit v21
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v20

    .line 1304
    .restart local v14    # "modelListener":Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;
    .restart local v17    # "size":I
    .restart local v18    # "version":J
    :cond_1
    const/4 v7, 0x0

    goto :goto_0

    .line 1320
    .restart local v7    # "firstLoading":Z
    :cond_2
    const/4 v4, 0x0

    .line 1321
    .local v4, "count":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIndexInfo:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->reset()V

    .line 1322
    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIndexInfo:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->getNextIndex()Z

    move-result v20

    if-eqz v20, :cond_a

    .line 1323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIndexInfo:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v9, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->index:I

    .line 1324
    .local v9, "index":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mCoverData:[Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v20, v0

    rem-int v16, v9, v20

    .line 1325
    .local v16, "pos":I
    if-ltz v16, :cond_3

    const/16 v20, 0x3e8

    move/from16 v0, v16

    move/from16 v1, v20

    if-ge v0, v1, :cond_3

    .line 1328
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSetVersion:[J

    move-object/from16 v20, v0

    aget-wide v20, v20, v16

    cmp-long v20, v20, v18

    if-eqz v20, :cond_3

    .line 1330
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSetVersion:[J

    move-object/from16 v20, v0

    aput-wide v18, v20, v16

    .line 1331
    sget-object v21, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v21

    .line 1332
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v13

    .line 1333
    .local v13, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v13, :cond_4

    .line 1334
    monitor-exit v21

    goto :goto_2

    .line 1343
    .end local v13    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :catchall_1
    move-exception v20

    monitor-exit v21
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v20

    .line 1336
    .restart local v13    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_4
    :try_start_3
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaSet;->getDataVersion()J

    move-result-wide v10

    .line 1337
    .local v10, "itemVersion":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mItemVersion:[J

    move-object/from16 v20, v0

    aget-wide v22, v20, v16

    cmp-long v20, v22, v10

    if-nez v20, :cond_5

    .line 1338
    monitor-exit v21

    goto :goto_2

    .line 1339
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mItemVersion:[J

    move-object/from16 v20, v0

    aput-wide v10, v20, v16

    .line 1340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    move-object/from16 v20, v0

    aput-object v13, v20, v16

    .line 1341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mItemCount:[I

    move-object/from16 v20, v0

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v22

    aput v22, v20, v16

    .line 1342
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaSet;->getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v6

    .line 1343
    .local v6, "cover":Lcom/sec/android/gallery3d/data/MediaItem;
    monitor-exit v21
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1348
    instance-of v0, v6, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    move/from16 v20, v0

    if-eqz v20, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mCoverData:[Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v20, v0

    aget-object v20, v20, v16

    move-object/from16 v0, v20

    if-eq v0, v6, :cond_3

    .line 1351
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mUpdateSet:[Lcom/sec/android/gallery3d/data/MediaSet;

    move-object/from16 v20, v0

    add-int/lit8 v5, v4, 0x1

    .end local v4    # "count":I
    .local v5, "count":I
    aput-object v13, v20, v4

    .line 1352
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mCoverData:[Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v20, v0

    aput-object v6, v20, v16

    .line 1353
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mNewAlbumIndex:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-ne v9, v0, :cond_8

    .line 1354
    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v9, v1}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->notifyDataSetChanged(II)V

    .line 1355
    const/16 v20, -0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mNewAlbumIndex:I

    .line 1367
    :cond_7
    :goto_3
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->updateNewAlbumFocus()V

    .line 1368
    if-eqz v14, :cond_f

    .line 1369
    invoke-interface {v14, v9}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->onWindowContentChanged(I)V

    move v4, v5

    .end local v5    # "count":I
    .restart local v4    # "count":I
    goto/16 :goto_2

    .line 1356
    .end local v4    # "count":I
    .restart local v5    # "count":I
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mNewAlbumIndex:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_7

    .line 1357
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mNewAlbumIndex:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mCoverData:[Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    rem-int v15, v20, v21

    .line 1358
    .local v15, "newAlbumPos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    move-object/from16 v20, v0

    aget-object v20, v20, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mFocusedMedia:Lcom/sec/android/gallery3d/data/MediaObject;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_7

    .line 1359
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mNewAlbumIndex:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->notifyDataSetChanged(II)V

    .line 1360
    if-eqz v14, :cond_9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mNewAlbumIndex:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mThumbnailActiveStart:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mNewAlbumIndex:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mThumbnailActiveEnd:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_9

    .line 1362
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mNewAlbumIndex:I

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-interface {v14, v0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->onWindowContentChanged(I)V

    .line 1364
    :cond_9
    const/16 v20, -0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mNewAlbumIndex:I

    goto/16 :goto_3

    .line 1373
    .end local v5    # "count":I
    .end local v6    # "cover":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v9    # "index":I
    .end local v10    # "itemVersion":J
    .end local v13    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v15    # "newAlbumPos":I
    .end local v16    # "pos":I
    .restart local v4    # "count":I
    :cond_a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mNewAlbumIndex:I

    move/from16 v20, v0

    if-ltz v20, :cond_c

    .line 1374
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mNewAlbumIndex:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mCoverData:[Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    rem-int v15, v20, v21

    .line 1375
    .restart local v15    # "newAlbumPos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    move-object/from16 v20, v0

    aget-object v20, v20, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mFocusedMedia:Lcom/sec/android/gallery3d/data/MediaObject;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_b

    .line 1376
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mNewAlbumIndex:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->notifyDataSetChanged(II)V

    .line 1377
    if-eqz v14, :cond_b

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mNewAlbumIndex:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mThumbnailActiveStart:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_b

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mNewAlbumIndex:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mThumbnailActiveEnd:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_b

    .line 1379
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mNewAlbumIndex:I

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-interface {v14, v0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->onWindowContentChanged(I)V

    .line 1383
    :cond_b
    const/16 v20, -0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mNewAlbumIndex:I

    .line 1386
    .end local v15    # "newAlbumPos":I
    :cond_c
    if-lez v4, :cond_e

    .line 1387
    new-instance v12, Ljava/util/LinkedHashSet;

    invoke-direct {v12}, Ljava/util/LinkedHashSet;-><init>()V

    .line 1388
    .local v12, "list":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_4
    if-ge v8, v4, :cond_d

    .line 1389
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mUpdateSet:[Lcom/sec/android/gallery3d/data/MediaSet;

    move-object/from16 v20, v0

    aget-object v13, v20, v8

    .line 1390
    .restart local v13    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v12, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1388
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 1392
    .end local v13    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mMediaSelectionCallback:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;

    move-object/from16 v20, v0

    if-eqz v20, :cond_e

    .line 1393
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mMediaSelectionCallback:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-interface {v0, v12}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;->handleMediaList(Ljava/util/Set;)V

    .line 1396
    .end local v8    # "i":I
    .end local v12    # "list":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_e
    const/16 v20, 0x1

    goto/16 :goto_1

    .end local v4    # "count":I
    .restart local v5    # "count":I
    .restart local v6    # "cover":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v9    # "index":I
    .restart local v10    # "itemVersion":J
    .restart local v13    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .restart local v16    # "pos":I
    :cond_f
    move v4, v5

    .end local v5    # "count":I
    .restart local v4    # "count":I
    goto/16 :goto_2
.end method

.method public declared-synchronized onPause()V
    .locals 1

    .prologue
    .line 1207
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->onPause()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1208
    monitor-exit p0

    return-void

    .line 1207
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onResume()V
    .locals 1

    .prologue
    .line 1198
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->onResume()V

    .line 1200
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsEasyMode:Z

    .line 1201
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsEasyMode:Z

    if-eqz v0, :cond_0

    .line 1202
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mBadgeRatio:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1203
    :cond_0
    monitor-exit p0

    return-void

    .line 1198
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 1212
    return-void
.end method

.method protected removeContentListener()V
    .locals 2

    .prologue
    .line 1457
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->removeContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 1458
    return-void
.end method

.method public resetAdapter()V
    .locals 6

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 182
    iput-wide v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSourceVersion:J

    .line 183
    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContentStart:I

    .line 184
    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContentEnd:I

    .line 185
    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mThumbnailActiveStart:I

    .line 186
    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mThumbnailActiveEnd:I

    .line 187
    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSize:I

    .line 189
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    monitor-enter v2

    .line 190
    const/4 v0, 0x0

    .local v0, "slotIndex":I
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 191
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    const/4 v3, 0x0

    aput-object v3, v1, v0

    .line 192
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mCoverData:[Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v3, 0x0

    aput-object v3, v1, v0

    .line 193
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mItemVersion:[J

    const-wide/16 v4, -0x1

    aput-wide v4, v1, v0

    .line 194
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSetVersion:[J

    const-wide/16 v4, -0x1

    aput-wide v4, v1, v0

    .line 190
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 196
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 198
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-eqz v1, :cond_1

    .line 199
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->resetContentWindow()V

    .line 200
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->resetAllThumbnails()V

    .line 202
    :cond_1
    return-void

    .line 196
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public setAlbumViewMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 1475
    iput p1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mViewMode:I

    .line 1476
    return-void
.end method

.method public setData(ILcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 5
    .param p1, "position"    # I
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const/4 v4, 0x0

    .line 1221
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mCoverData:[Lcom/sec/android/gallery3d/data/MediaItem;

    if-nez v2, :cond_1

    .line 1229
    :cond_0
    :goto_0
    return-void

    .line 1224
    :cond_1
    rem-int/lit16 v0, p1, 0x3e8

    .line 1225
    .local v0, "index":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    aput-object p2, v2, v0

    .line 1226
    const/4 v2, 0x1

    invoke-virtual {p2, v4, v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v1

    .line 1227
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mCoverData:[Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    .line 1228
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mCoverData:[Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    aput-object v2, v3, v0

    goto :goto_0
.end method

.method public setFocusAlbumFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mFocusAlbumFilePath:Ljava/lang/String;

    .line 206
    return-void
.end method

.method public setIsPhotoViewState(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 1465
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsPhotoViewState:Z

    .line 1466
    return-void
.end method

.method public setMediaSelectionCallback(Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mMediaSelectionCallback:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;

    .line 179
    return-void
.end method

.method public setReorderAlbum(Z)V
    .locals 0
    .param p1, "setReorder"    # Z

    .prologue
    .line 1501
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mIsReorderAlbum:Z

    .line 1502
    return-void
.end method

.method public setTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V
    .locals 0
    .param p1, "type"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    .line 1469
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 1470
    return-void
.end method

.method public updateSource(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 1
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eq p1, v0, :cond_0

    .line 162
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 164
    :cond_0
    return-void
.end method
