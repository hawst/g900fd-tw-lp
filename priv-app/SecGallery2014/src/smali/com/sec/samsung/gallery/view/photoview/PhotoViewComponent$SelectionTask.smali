.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;
.super Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;
.source "PhotoViewComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectionTask"
.end annotation


# instance fields
.field private final mCurrentSelectedCount:I

.field private final mMaxAddCount:I

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;III)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "onProgressListener"    # Lcom/sec/android/gallery3d/data/OnProgressListener;
    .param p4, "maxAddCount"    # I
    .param p5, "currentSelectedCount"    # I
    .param p6, "maxCount"    # I

    .prologue
    .line 3228
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .line 3229
    invoke-direct {p0, p2, p3, p6}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;I)V

    .line 3230
    iput p4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->mMaxAddCount:I

    .line 3231
    iput p5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->mCurrentSelectedCount:I

    .line 3232
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 3222
    check-cast p1, [Lcom/sec/android/gallery3d/data/MediaSet;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->doInBackground([Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/Void;
    .locals 5
    .param p1, "arg0"    # [Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const/4 v4, 0x0

    .line 3236
    const/4 v1, 0x0

    aget-object v0, p1, v1

    .line 3237
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    if-eqz v1, :cond_0

    .line 3238
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->mMaxAddCount:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->mCurrentSelectedCount:I

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectItemPickMode(Lcom/sec/android/gallery3d/data/MediaSet;IILcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;)V
    invoke-static {v1, v0, v2, v3, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$3600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/android/gallery3d/data/MediaSet;IILcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;)V

    .line 3248
    :goto_0
    return-object v4

    .line 3240
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->misFilteredForStoryAlbum:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFilteredItems:Z

    if-eqz v1, :cond_2

    .line 3241
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectItemFilterMode(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;)V
    invoke-static {v1, v0, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$3700(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;)V

    goto :goto_0

    .line 3243
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    if-eqz v1, :cond_3

    .line 3244
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectItemNewAlbumMode(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;)V
    invoke-static {v1, v0, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$3800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;)V

    goto :goto_0

    .line 3247
    :cond_3
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->doInBackground([Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/Void;

    goto :goto_0
.end method
