.class Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal$1;
.super Ljava/lang/Object;
.source "EventActionBarForNormal.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    const/4 v5, 0x1

    .line 49
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->access$000(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030002

    invoke-virtual {v3, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 52
    .local v0, "customView":Landroid/view/View;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 53
    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    .line 55
    .local v2, "lp":Landroid/app/ActionBar$LayoutParams;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->access$100(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0042

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    iput v3, v2, Landroid/app/ActionBar$LayoutParams;->leftMargin:I

    .line 57
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->access$200(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v0, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 65
    .end local v2    # "lp":Landroid/app/ActionBar$LayoutParams;
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->access$800(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 66
    const/16 v1, 0xc

    .line 68
    .local v1, "flags":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->access$900(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 69
    return-void

    .line 59
    .end local v1    # "flags":I
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->access$300(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 60
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->access$400(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 61
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->access$500(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 62
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->access$600(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 63
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;->access$700(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
