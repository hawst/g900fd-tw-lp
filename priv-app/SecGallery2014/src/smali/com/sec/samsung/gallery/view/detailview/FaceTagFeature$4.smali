.class Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;
.super Ljava/lang/Object;
.source "FaceTagFeature.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->showAlertDialog(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field curPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

.field final synthetic val$lookupkey:Ljava/lang/String;

.field final synthetic val$rectScaled:Landroid/graphics/RectF;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;Landroid/graphics/RectF;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 546
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->val$rectScaled:Landroid/graphics/RectF;

    iput-object p3, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->val$lookupkey:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 547
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->getCurrentPhoto()Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->access$300(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->curPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 551
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->curPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mIsClicked:Z
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->access$400(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 581
    :cond_0
    :goto_0
    return-void

    .line 553
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    # setter for: Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mIsClicked:Z
    invoke-static {v3, v6}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->access$402(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;Z)Z

    .line 555
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.android.camera.action.CROP"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 556
    .local v1, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->access$500(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 557
    const-string v3, "is-caller-id"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 558
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->val$rectScaled:Landroid/graphics/RectF;

    if-eqz v3, :cond_2

    .line 559
    const/4 v3, 0x4

    new-array v2, v3, [F

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->val$rectScaled:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    aput v3, v2, v6

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->val$rectScaled:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    aput v3, v2, v5

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->val$rectScaled:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    aput v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->val$rectScaled:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    aput v4, v2, v3

    .line 563
    .local v2, "values":[F
    const-string v3, "face-position"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[F)Landroid/content/Intent;

    .line 565
    .end local v2    # "values":[F
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->curPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    .line 567
    .local v0, "data":Landroid/net/Uri;
    const-string v3, "aspectX"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 568
    const-string v3, "aspectY"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 569
    const-string v3, "outputX"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->access$200(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getCropSizeforSetCallerID(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 570
    const-string v3, "outputY"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->access$200(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getCropSizeforSetCallerID(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 571
    const-string v3, "return-data"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 572
    const-string v3, "output"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->access$200(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/app/CropImage;->getCallerIdSavePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 573
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->val$lookupkey:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 574
    const-string v3, "lookupKeyFromContactPopup"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->val$lookupkey:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 578
    :goto_1
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 579
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->access$500(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    const/16 v4, 0x305

    invoke-virtual {v3, v1, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 576
    :cond_3
    const-string v3, "lookupKeyFromContactPopup"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->access$600(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->getCurContactLookupKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method
