.class Lcom/sec/samsung/gallery/mapfragment/MapViewState$Options;
.super Ljava/lang/Object;
.source "MapViewState.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/mapfragment/MapViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Options"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x67fb771a62725058L


# instance fields
.field clusterClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterClickBehavior;

.field clusterInfoWindowClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

.field dipDistanceToJoinCluster:I

.field expandBoundsFactor:D

.field showInfoWindowAnimationDuration:I

.field singlePointClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

.field transitionDuration:I

.field transitionInterpolator:Ljava/lang/String;

.field zoomToBoundsAnimationDuration:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x1f4

    .line 410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 419
    iput v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$Options;->transitionDuration:I

    .line 420
    const-class v0, Landroid/view/animation/LinearInterpolator;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$Options;->transitionInterpolator:Ljava/lang/String;

    .line 421
    const/16 v0, 0xaf

    iput v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$Options;->dipDistanceToJoinCluster:I

    .line 422
    iput v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$Options;->zoomToBoundsAnimationDuration:I

    .line 423
    iput v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$Options;->showInfoWindowAnimationDuration:I

    .line 424
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    iput-wide v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$Options;->expandBoundsFactor:D

    .line 425
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;->NO_OP:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$Options;->singlePointClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    .line 426
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterClickBehavior;->NO_OP:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterClickBehavior;

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$Options;->clusterClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterClickBehavior;

    .line 427
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;->NO_OP:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$Options;->clusterInfoWindowClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    return-void
.end method
