.class public Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory;
.super Ljava/lang/Object;
.source "ViewHelperFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelperDefault;,
        Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ViewHelper"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method public static final create(Landroid/view/View;)Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 50
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 52
    .local v0, "version":I
    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 54
    new-instance v1, Lcom/sec/samsung/gallery/hlistview/util/ViewHelper16;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/hlistview/util/ViewHelper16;-><init>(Landroid/view/View;)V

    .line 60
    :goto_0
    return-object v1

    .line 55
    :cond_0
    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 57
    new-instance v1, Lcom/sec/samsung/gallery/hlistview/util/ViewHelper14;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/hlistview/util/ViewHelper14;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 60
    :cond_1
    new-instance v1, Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelperDefault;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelperDefault;-><init>(Landroid/view/View;)V

    goto :goto_0
.end method
