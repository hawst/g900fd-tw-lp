.class Lcom/sec/samsung/gallery/view/timeview/TimeViewState$24;
.super Ljava/lang/Object;
.source "TimeViewState.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->runSelectionTask(Lcom/sec/android/gallery3d/data/MediaSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field onProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

.field final synthetic val$mediaSet:Lcom/sec/android/gallery3d/data/MediaSet;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 1

    .prologue
    .line 2963
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$24;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$24;->val$mediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2965
    new-instance v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$24$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$24$1;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState$24;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$24;->onProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2988
    new-instance v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$SelectionTask;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$24;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$24;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$7300(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$24;->onProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$24;->val$mediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$SelectionTask;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;I)V

    .line 2990
    .local v0, "selectionTask":Lcom/sec/samsung/gallery/view/timeview/TimeViewState$SelectionTask;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    .line 2991
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v6, [Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$24;->val$mediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$SelectionTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2994
    :goto_0
    return-void

    .line 2993
    :cond_0
    new-array v1, v6, [Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$24;->val$mediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$SelectionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
