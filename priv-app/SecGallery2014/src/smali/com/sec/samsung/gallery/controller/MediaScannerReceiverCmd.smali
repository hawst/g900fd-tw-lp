.class public Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "MediaScannerReceiverCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd$MediaScanner;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMediaScannerReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 59
    new-instance v0, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;->mMediaScannerReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private registerMediaScannerReceiver()V
    .locals 3

    .prologue
    .line 40
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 41
    .local v0, "intentMediaScannerFilter":Landroid/content/IntentFilter;
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 42
    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 43
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;->mMediaScannerReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 44
    return-void
.end method

.method private unregisterMediaScannerReceiver()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 47
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;->mMediaScannerReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 49
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;->mMediaScannerReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;->mMediaScannerReceiver:Landroid/content/BroadcastReceiver;

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 50
    :catch_0
    move-exception v0

    .line 51
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    sget-object v1, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;->TAG:Ljava/lang/String;

    const-string v2, "catch IllegalArgumentException and ignore it"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 54
    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;->mMediaScannerReceiver:Landroid/content/BroadcastReceiver;

    goto :goto_0

    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;->mMediaScannerReceiver:Landroid/content/BroadcastReceiver;

    throw v1
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 3
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 28
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Object;

    move-object v1, v2

    check-cast v1, [Ljava/lang/Object;

    .line 29
    .local v1, "params":[Ljava/lang/Object;
    const/4 v2, 0x0

    aget-object v2, v1, v2

    check-cast v2, Landroid/content/Context;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;->mContext:Landroid/content/Context;

    .line 30
    const/4 v2, 0x1

    aget-object v0, v1, v2

    check-cast v0, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd$MediaScanner;

    .line 32
    .local v0, "mode":Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd$MediaScanner;
    sget-object v2, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd$MediaScanner;->REGISTER:Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd$MediaScanner;

    if-ne v0, v2, :cond_0

    .line 33
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;->registerMediaScannerReceiver()V

    .line 37
    :goto_0
    return-void

    .line 35
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;->unregisterMediaScannerReceiver()V

    goto :goto_0
.end method
