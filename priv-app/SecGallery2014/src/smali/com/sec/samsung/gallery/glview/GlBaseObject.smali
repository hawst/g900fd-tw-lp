.class public Lcom/sec/samsung/gallery/glview/GlBaseObject;
.super Lcom/sec/android/gallery3d/glcore/GlObject;
.source "GlBaseObject.java"


# static fields
.field public static FADE_IN_DURATION:I

.field public static FADE_OUT_DURATION:I

.field public static GL_CANVAS_LARGE:I

.field public static GL_CANVAS_MIDDLE:I

.field public static GL_CANVAS_NORMAL:I

.field public static GL_CANVAS_SMALL:I

.field public static GL_CANVAS_TINY:I

.field public static GL_OBJ_ACTIVE:I

.field public static GL_OBJ_EXT:I

.field public static GL_OBJ_USED:I


# instance fields
.field public mAttr:I

.field public mCol:I

.field public mDh:F

.field public mDw:F

.field public mDx:F

.field public mDy:F

.field public mDz:F

.field public mExpansion:Z

.field private mFadeInAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

.field private mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

.field public mHndAlpha:I

.field public mHndDim:I

.field public mIndex:I

.field public mLayoutH:F

.field public mLayoutW:F

.field public mLx:F

.field public mLy:F

.field public mRow:I

.field public mSourceHeight:F

.field protected mSourceRoll:F

.field public mSourceWidth:F

.field public mTargetHeight:F

.field protected mTargetRoll:F

.field public mTargetWidth:F

.field public msx:F

.field public msy:F

.field public msz:F

.field public mtx:F

.field public mty:F

.field public mtz:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x1f4

    .line 9
    const/4 v0, -0x1

    sput v0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_CANVAS_TINY:I

    .line 10
    const/4 v0, -0x2

    sput v0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_CANVAS_SMALL:I

    .line 11
    const/4 v0, -0x3

    sput v0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_CANVAS_MIDDLE:I

    .line 12
    const/4 v0, -0x4

    sput v0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_CANVAS_NORMAL:I

    .line 13
    const/4 v0, -0x5

    sput v0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_CANVAS_LARGE:I

    .line 15
    const/4 v0, 0x1

    sput v0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_USED:I

    .line 16
    const/4 v0, 0x2

    sput v0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    .line 17
    const/16 v0, 0x100

    sput v0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_EXT:I

    .line 212
    sput v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->FADE_IN_DURATION:I

    .line 213
    sput v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->FADE_OUT_DURATION:I

    return-void
.end method

.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V
    .locals 3
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "width"    # F
    .param p3, "height"    # F

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 19
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mHndAlpha:I

    .line 20
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mHndDim:I

    .line 21
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 33
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mLayoutW:F

    .line 34
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mLayoutH:F

    .line 35
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mRow:I

    .line 36
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mCol:I

    .line 37
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mExpansion:Z

    .line 43
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFI)V
    .locals 4
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "width"    # F
    .param p3, "height"    # F
    .param p4, "textureSize"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 19
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mHndAlpha:I

    .line 20
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mHndDim:I

    .line 21
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    .line 22
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 33
    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mLayoutW:F

    .line 34
    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mLayoutH:F

    .line 35
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mRow:I

    .line 36
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mCol:I

    .line 37
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mExpansion:Z

    .line 48
    if-nez p4, :cond_0

    .line 53
    :goto_0
    return-void

    .line 51
    :cond_0
    if-lez p4, :cond_1

    move v0, p4

    .line 52
    .local v0, "texSize":I
    :goto_1
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v2, p1, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v1, v2, v0, v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    goto :goto_0

    .line 51
    .end local v0    # "texSize":I
    :cond_1
    invoke-direct {p0, p4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getSize(I)I

    move-result v0

    goto :goto_1
.end method

.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V
    .locals 5
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "width"    # F
    .param p3, "height"    # F
    .param p4, "textureWidth"    # I
    .param p5, "textureHeight"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 56
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 19
    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mHndAlpha:I

    .line 20
    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mHndDim:I

    .line 21
    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    .line 22
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 33
    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mLayoutW:F

    .line 34
    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mLayoutH:F

    .line 35
    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mRow:I

    .line 36
    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mCol:I

    .line 37
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mExpansion:Z

    .line 58
    if-eqz p4, :cond_0

    if-nez p5, :cond_1

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    if-lez p4, :cond_2

    move v1, p4

    .line 62
    .local v1, "texWidth":I
    :goto_1
    if-lez p5, :cond_3

    move v0, p5

    .line 63
    .local v0, "texHeight":I
    :goto_2
    new-instance v2, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v3, p1, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v2, v3, v1, v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    goto :goto_0

    .line 61
    .end local v0    # "texHeight":I
    .end local v1    # "texWidth":I
    :cond_2
    invoke-direct {p0, p4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getSize(I)I

    move-result v1

    goto :goto_1

    .line 62
    .restart local v1    # "texWidth":I
    :cond_3
    invoke-direct {p0, p5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getSize(I)I

    move-result v0

    goto :goto_2
.end method

.method private getSize(I)I
    .locals 2
    .param p1, "textureSize"    # I

    .prologue
    .line 69
    sget v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_CANVAS_TINY:I

    if-ne p1, v1, :cond_0

    .line 70
    const/16 v0, 0x40

    .line 81
    .local v0, "size":I
    :goto_0
    return v0

    .line 71
    .end local v0    # "size":I
    :cond_0
    sget v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_CANVAS_SMALL:I

    if-ne p1, v1, :cond_1

    .line 72
    const/16 v0, 0x80

    .restart local v0    # "size":I
    goto :goto_0

    .line 73
    .end local v0    # "size":I
    :cond_1
    sget v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_CANVAS_MIDDLE:I

    if-ne p1, v1, :cond_2

    .line 74
    const/16 v0, 0xc4

    .restart local v0    # "size":I
    goto :goto_0

    .line 75
    .end local v0    # "size":I
    :cond_2
    sget v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_CANVAS_NORMAL:I

    if-ne p1, v1, :cond_3

    .line 76
    const/16 v0, 0x100

    .restart local v0    # "size":I
    goto :goto_0

    .line 77
    .end local v0    # "size":I
    :cond_3
    sget v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_CANVAS_LARGE:I

    if-ne p1, v1, :cond_4

    .line 78
    const/16 v0, 0x200

    .restart local v0    # "size":I
    goto :goto_0

    .line 80
    .end local v0    # "size":I
    :cond_4
    const/16 v0, 0x100

    .restart local v0    # "size":I
    goto :goto_0
.end method


# virtual methods
.method public cancelPendingAmin()V
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->isReady()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->stop()V

    .line 262
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeInAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeInAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->isRunning()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeInAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->isReady()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 263
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeInAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->stop()V

    .line 265
    :cond_3
    return-void
.end method

.method public fadeIn()V
    .locals 4

    .prologue
    .line 218
    const-wide/16 v0, 0x0

    sget v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->FADE_IN_DURATION:I

    int-to-long v2, v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->fadeIn(JJ)V

    .line 219
    return-void
.end method

.method public fadeIn(JJ)V
    .locals 3
    .param p1, "delay"    # J
    .param p3, "duration"    # J

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->isIdle()Z

    move-result v0

    if-nez v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->stop()V

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeInAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    if-eqz v0, :cond_1

    .line 226
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeInAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->stop()V

    .line 231
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeInAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->setDuration(J)V

    .line 232
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeInAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0, p3, p4}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->startAfter(J)V

    .line 233
    return-void

    .line 228
    :cond_1
    new-instance v0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getAlpha()F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;-><init>(Lcom/sec/android/gallery3d/glcore/GlObject;FF)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeInAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    .line 229
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeInAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    goto :goto_0
.end method

.method public fadeOut()V
    .locals 4

    .prologue
    .line 236
    const-wide/16 v0, 0x0

    sget v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->FADE_OUT_DURATION:I

    int-to-long v2, v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->fadeOut(JJ)V

    .line 237
    return-void
.end method

.method public fadeOut(J)V
    .locals 3
    .param p1, "delay"    # J

    .prologue
    .line 240
    sget v0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->FADE_OUT_DURATION:I

    int-to-long v0, v0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->fadeOut(JJ)V

    .line 241
    return-void
.end method

.method public fadeOut(JJ)V
    .locals 3
    .param p1, "delay"    # J
    .param p3, "duration"    # J

    .prologue
    const/4 v2, 0x0

    .line 244
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    if-nez v0, :cond_0

    .line 245
    new-instance v0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getAlpha()F

    move-result v1

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;-><init>(Lcom/sec/android/gallery3d/glcore/GlObject;FF)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    .line 246
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeInAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeInAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 250
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeInAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->stop()V

    .line 253
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getAlpha()F

    move-result v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->setParam(FF)V

    .line 254
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0, p3, p4}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->setDuration(J)V

    .line 255
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->startAfter(J)V

    .line 256
    return-void
.end method

.method public getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;
    .locals 2

    .prologue
    .line 115
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mChildCount:I

    if-ge v0, v1, :cond_1

    .line 116
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    instance-of v1, v1, Lcom/sec/samsung/gallery/glview/GlTextObject;

    if-eqz v1, :cond_0

    .line 117
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    check-cast v1, Lcom/sec/samsung/gallery/glview/GlTextObject;

    .line 119
    :goto_1
    return-object v1

    .line 115
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getMainObject()Lcom/sec/android/gallery3d/glcore/GlObject;
    .locals 0

    .prologue
    .line 111
    return-object p0
.end method

.method public getSourceX()F
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msx:F

    return v0
.end method

.method public getSourceY()F
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msy:F

    return v0
.end method

.method public getSourceZ()F
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msz:F

    return v0
.end method

.method public getTargetX()F
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mtx:F

    return v0
.end method

.method public getTargetY()F
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mty:F

    return v0
.end method

.method public getTargetZ()F
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mtz:F

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0}, Lcom/sec/android/gallery3d/glcore/GlObject;->onDestroy()V

    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->recycle()V

    .line 106
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 108
    :cond_0
    return-void
.end method

.method public resetDim()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 97
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mHndDim:I

    if-nez v0, :cond_1

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mHndDim:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getDim(I)F

    move-result v0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 99
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setDimEx(F)V

    goto :goto_0
.end method

.method public setAlphaEx(F)V
    .locals 1
    .param p1, "alpha"    # F

    .prologue
    .line 85
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mHndAlpha:I

    if-nez v0, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getAlphaHnd()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mHndAlpha:I

    .line 87
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mHndAlpha:I

    invoke-virtual {p0, p1, v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlpha(FI)V

    .line 88
    return-void
.end method

.method public setDimEx(F)V
    .locals 1
    .param p1, "dim"    # F

    .prologue
    .line 91
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mHndDim:I

    if-nez v0, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getDimHnd()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mHndDim:I

    .line 93
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mHndDim:I

    invoke-virtual {p0, p1, v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setDim(FI)V

    .line 94
    return-void
.end method

.method public setIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 268
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 269
    return-void
.end method

.method public setPosAsSource()V
    .locals 1

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msx:F

    .line 170
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msy:F

    .line 171
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getZ()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msz:F

    .line 172
    return-void
.end method

.method public setPosAsTarget()V
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mtx:F

    .line 176
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mty:F

    .line 177
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getZ()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mtz:F

    .line 178
    return-void
.end method

.method public setSourcePos(FFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 133
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msx:F

    .line 134
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msy:F

    .line 135
    iput p3, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msz:F

    .line 136
    return-void
.end method

.method public setSourceRoll(F)V
    .locals 0
    .param p1, "roll"    # F

    .prologue
    .line 205
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mSourceRoll:F

    .line 206
    return-void
.end method

.method public setSourceSize(FF)V
    .locals 0
    .param p1, "width"    # F
    .param p2, "height"    # F

    .prologue
    .line 123
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mSourceWidth:F

    .line 124
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mSourceHeight:F

    .line 125
    return-void
.end method

.method public setTargetPos(FFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 151
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mtx:F

    .line 152
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mty:F

    .line 153
    iput p3, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mtz:F

    .line 154
    return-void
.end method

.method public setTargetRoll(F)V
    .locals 0
    .param p1, "roll"    # F

    .prologue
    .line 209
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mTargetRoll:F

    .line 210
    return-void
.end method

.method public setTargetSize(FF)V
    .locals 0
    .param p1, "width"    # F
    .param p2, "height"    # F

    .prologue
    .line 128
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mTargetWidth:F

    .line 129
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mTargetHeight:F

    .line 130
    return-void
.end method

.method public switchSourceAndTarget()V
    .locals 6

    .prologue
    .line 181
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mtx:F

    .line 182
    .local v2, "tempX":F
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mty:F

    .line 183
    .local v3, "tempY":F
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mtz:F

    .line 185
    .local v4, "tempZ":F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msx:F

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mtx:F

    .line 186
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msy:F

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mty:F

    .line 187
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msz:F

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mtz:F

    .line 189
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msx:F

    .line 190
    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msy:F

    .line 191
    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msz:F

    .line 193
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mTargetWidth:F

    .line 194
    .local v1, "tempW":F
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mTargetHeight:F

    .line 196
    .local v0, "tempH":F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mSourceWidth:F

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mTargetWidth:F

    .line 197
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mSourceHeight:F

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mTargetHeight:F

    .line 199
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mSourceWidth:F

    .line 200
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mSourceHeight:F

    .line 202
    return-void
.end method
