.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;
.super Ljava/lang/Object;
.source "GallerySearchTagFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    }
.end annotation


# static fields
.field public static final EVENT:I = 0x1

.field public static final LOCATION_CITY:I = 0x2

.field private static final LOG_TAG:Ljava/lang/String;

.field public static final PEOPLE:I = 0x3

.field public static final TIME:I = 0x0

.field public static final TOTAL_SIZE:I = 0x5

.field public static final USERTAG:I = 0x4


# instance fields
.field private EXIST_FILTER:I

.field private mContext:Landroid/content/Context;

.field public mFilterList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;",
            ">;"
        }
    .end annotation
.end field

.field public mKeyword:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->EXIST_FILTER:I

    .line 27
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mContext:Landroid/content/Context;

    .line 28
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mKeyword:Ljava/lang/String;

    .line 29
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->initFilterList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    .line 30
    return-void
.end method

.method private getFilterIndex(Ljava/lang/String;)I
    .locals 1
    .param p1, "filterType"    # Ljava/lang/String;

    .prologue
    .line 55
    const-string v0, "Time"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 60
    :goto_0
    return v0

    .line 56
    :cond_0
    const-string v0, "Event"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    .line 57
    :cond_1
    const-string v0, "Location"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    .line 58
    :cond_2
    const-string v0, "People"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    goto :goto_0

    .line 59
    :cond_3
    const-string v0, "UserTag"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x4

    goto :goto_0

    .line 60
    :cond_4
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private initFilterList()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 34
    .local v0, "filter":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;>;"
    const/4 v1, 0x0

    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;

    const-string v3, "Time"

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 35
    const/4 v1, 0x1

    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;

    const-string v3, "Event"

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 36
    const/4 v1, 0x2

    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;

    const-string v3, "Location"

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 37
    const/4 v1, 0x3

    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;

    const-string v3, "People"

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 38
    const/4 v1, 0x4

    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;

    const-string v3, "UserTag"

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 39
    return-object v0
.end method


# virtual methods
.method public addTag(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1, "filterType"    # Ljava/lang/String;
    .param p2, "tagType"    # Ljava/lang/String;
    .param p3, "categoryType"    # I

    .prologue
    const/4 v3, 0x1

    .line 64
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 65
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->initFilterList()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    .line 67
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->getFilterIndex(Ljava/lang/String;)I

    move-result v0

    .line 68
    .local v0, "type":I
    packed-switch v0, :pswitch_data_0

    .line 86
    :goto_0
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->EXIST_FILTER:I

    shl-int v2, v3, v0

    or-int/2addr v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->EXIST_FILTER:I

    .line 87
    return-void

    .line 70
    :pswitch_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;

    invoke-virtual {v1, p2, p3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->addTag(Ljava/lang/String;I)V

    goto :goto_0

    .line 73
    :pswitch_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;

    invoke-virtual {v1, p2, p3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->addTag(Ljava/lang/String;I)V

    goto :goto_0

    .line 76
    :pswitch_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;

    invoke-virtual {v1, p2, p3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->addTag(Ljava/lang/String;I)V

    goto :goto_0

    .line 79
    :pswitch_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;

    invoke-virtual {v1, p2, p3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->addTag(Ljava/lang/String;I)V

    goto :goto_0

    .line 82
    :pswitch_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;

    invoke-virtual {v1, p2, p3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->addTag(Ljava/lang/String;I)V

    goto :goto_0

    .line 68
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public freeFilter()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->EXIST_FILTER:I

    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 49
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method public getExistFilterMask()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->EXIST_FILTER:I

    return v0
.end method

.method public removeTag(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "filterType"    # Ljava/lang/String;
    .param p2, "tagType"    # Ljava/lang/String;

    .prologue
    .line 89
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    if-nez v3, :cond_0

    .line 90
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->initFilterList()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    .line 92
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->getFilterIndex(Ljava/lang/String;)I

    move-result v2

    .line 93
    .local v2, "type":I
    packed-switch v2, :pswitch_data_0

    .line 113
    :cond_1
    :goto_0
    return-void

    .line 100
    :pswitch_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;

    .line 101
    .local v1, "filter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    if-eqz v1, :cond_2

    .line 102
    invoke-virtual {v1, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->removeTag(Ljava/lang/String;)V

    .line 104
    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->getCount()I

    move-result v3

    if-nez v3, :cond_1

    .line 105
    :cond_3
    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->EXIST_FILTER:I

    const/4 v4, 0x1

    shl-int/2addr v4, v2

    xor-int/lit8 v4, v4, -0x1

    and-int/2addr v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->EXIST_FILTER:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 107
    .end local v1    # "filter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 93
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setKeyword(Ljava/lang/String;)V
    .locals 0
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mKeyword:Ljava/lang/String;

    .line 52
    return-void
.end method
