.class Lcom/sec/samsung/gallery/view/detailview/DetailViewState$11;
.super Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$SimpleOnGestureListener;
.source "DetailViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0

    .prologue
    .line 3038
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$11;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onMove(III)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "direction"    # I

    .prologue
    .line 3080
    const/4 v0, 0x3

    if-eq p3, v0, :cond_0

    const/4 v0, 0x4

    if-ne p3, v0, :cond_1

    .line 3082
    :cond_0
    invoke-static {p3}, Lcom/sec/samsung/gallery/util/HoverIconUtil;->setHoveringIconTo(I)V

    .line 3084
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$SimpleOnGestureListener;->onMove(III)Z

    move-result v0

    return v0
.end method

.method public onNext()Z
    .locals 3

    .prologue
    .line 3041
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$11;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isFocusable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3043
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$11;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetToFullView()V

    .line 3044
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$11;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->previousImage()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3045
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$11;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$11800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 3050
    :goto_0
    const/4 v1, 0x1

    .line 3052
    :goto_1
    return v1

    .line 3047
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$11;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    .line 3048
    .local v0, "posCtrl":Lcom/sec/android/gallery3d/ui/PositionController;
    const/16 v1, 0x3e8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->scrollPage(II)V

    goto :goto_0

    .line 3052
    .end local v0    # "posCtrl":Lcom/sec/android/gallery3d/ui/PositionController;
    :cond_1
    invoke-super {p0}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$SimpleOnGestureListener;->onNext()Z

    move-result v1

    goto :goto_1
.end method

.method public onPress(II)Z
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 3074
    invoke-static {}, Lcom/sec/samsung/gallery/util/HoverIconUtil;->setHoveringIconToDefault()V

    .line 3075
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$SimpleOnGestureListener;->onPress(II)Z

    move-result v0

    return v0
.end method

.method public onPrevious()Z
    .locals 3

    .prologue
    .line 3058
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$11;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isFocusable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3059
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$11;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetToFullView()V

    .line 3060
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$11;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->nextImage()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3061
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$11;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$11900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 3066
    :goto_0
    const/4 v1, 0x1

    .line 3068
    :goto_1
    return v1

    .line 3063
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$11;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    .line 3064
    .local v0, "posCtrl":Lcom/sec/android/gallery3d/ui/PositionController;
    const/16 v1, -0x3e8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->scrollPage(II)V

    goto :goto_0

    .line 3068
    .end local v0    # "posCtrl":Lcom/sec/android/gallery3d/ui/PositionController;
    :cond_1
    invoke-super {p0}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$SimpleOnGestureListener;->onPrevious()Z

    move-result v1

    goto :goto_1
.end method

.method public onRelease(IIII)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    .line 3089
    invoke-static {}, Lcom/sec/samsung/gallery/util/HoverIconUtil;->setHoveringIconToDefault()V

    .line 3090
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$SimpleOnGestureListener;->onRelease(IIII)Z

    move-result v0

    return v0
.end method
