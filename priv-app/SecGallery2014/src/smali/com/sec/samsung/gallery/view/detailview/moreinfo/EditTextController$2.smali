.class Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController$2;
.super Landroid/text/InputFilter$LengthFilter;
.source "EditTextController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;I)V
    .locals 0
    .param p2, "x0"    # I

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;

    invoke-direct {p0, p2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 8
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    const/4 v7, 0x0

    .line 83
    invoke-super/range {p0 .. p6}, Landroid/text/InputFilter$LengthFilter;->filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 85
    .local v0, "rst":Ljava/lang/CharSequence;
    if-eqz v0, :cond_1

    .line 86
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mLongTextToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->access$200(Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;)Landroid/widget/Toast;

    move-result-object v1

    if-nez v1, :cond_0

    .line 87
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e01cc

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/16 v6, 0x40

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    # setter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mLongTextToast:Landroid/widget/Toast;
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->access$202(Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 91
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mLongTextToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->access$200(Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 93
    :cond_1
    return-object v0
.end method
