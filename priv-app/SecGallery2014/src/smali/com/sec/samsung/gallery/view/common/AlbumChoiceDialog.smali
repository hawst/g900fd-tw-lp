.class public Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;
.super Lcom/sec/samsung/gallery/view/ViewObservable;
.source "AlbumChoiceDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog$1;,
        Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog$AlbumChoiceDialogFragment;
    }
.end annotation


# instance fields
.field private list:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mAlbumList:[Lcom/sec/android/gallery3d/data/MediaSet;

.field private final mCtx:Landroid/content/Context;

.field private final mDialogFragment:Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog$AlbumChoiceDialogFragment;

.field private final mEvent:Lcom/sec/samsung/gallery/core/Event;


# direct methods
.method public constructor <init>(Landroid/content/Context;[Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/core/Event;)V
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "mediaSetArray"    # [Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "event"    # Lcom/sec/samsung/gallery/core/Event;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/ViewObservable;-><init>()V

    .line 38
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->list:Ljava/util/ArrayList;

    .line 41
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->mCtx:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->mAlbumList:[Lcom/sec/android/gallery3d/data/MediaSet;

    .line 43
    iput-object p3, p0, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->mEvent:Lcom/sec/samsung/gallery/core/Event;

    .line 44
    new-instance v2, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog$AlbumChoiceDialogFragment;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->mCtx:Landroid/content/Context;

    invoke-direct {v2, p0, v3}, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog$AlbumChoiceDialogFragment;-><init>(Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog$AlbumChoiceDialogFragment;

    .line 46
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->mAlbumList:[Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->mAlbumList:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v2, v2

    if-lez v2, :cond_0

    .line 47
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->mAlbumList:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 48
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->list:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->mAlbumList:[Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 51
    .end local v1    # "i":I
    :catch_0
    move-exception v0

    .line 52
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 54
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    return-void
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;)[Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->mAlbumList:[Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;)Lcom/sec/samsung/gallery/core/Event;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->mEvent:Lcom/sec/samsung/gallery/core/Event;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->list:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;)Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog$AlbumChoiceDialogFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog$AlbumChoiceDialogFragment;

    return-object v0
.end method


# virtual methods
.method public dismissDialog()V
    .locals 2

    .prologue
    .line 194
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog$AlbumChoiceDialogFragment;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog$AlbumChoiceDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 195
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 196
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 198
    :cond_0
    return-void
.end method

.method public setTitle(I)V
    .locals 1
    .param p1, "titleId"    # I

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog$AlbumChoiceDialogFragment;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog$AlbumChoiceDialogFragment;->setTitle(I)V

    .line 202
    return-void
.end method

.method public showDialog()V
    .locals 4

    .prologue
    .line 187
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog$AlbumChoiceDialogFragment;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->mCtx:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v3, "dialog"

    invoke-virtual {v2, v1, v3}, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog$AlbumChoiceDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    :goto_0
    return-void

    .line 188
    :catch_0
    move-exception v0

    .line 189
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method
