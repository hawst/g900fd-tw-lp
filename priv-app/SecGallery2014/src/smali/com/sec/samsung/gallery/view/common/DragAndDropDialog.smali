.class public Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;
.super Ljava/lang/Object;
.source "DragAndDropDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$OkButtonClickListener;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDragDialog:Landroid/app/Dialog;

.field private mOkButtonListener:Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$OkButtonClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->setDoNotShowAgain(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;)Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$OkButtonClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->mOkButtonListener:Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$OkButtonClickListener;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->mDragDialog:Landroid/app/Dialog;

    return-object p1
.end method

.method private setDoNotShowAgain(Z)V
    .locals 2
    .param p1, "doNotShowAgain"    # Z

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->mContext:Landroid/content/Context;

    const-string v1, "DragAndDropDialogOff"

    invoke-static {v0, v1, p1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 88
    return-void
.end method


# virtual methods
.method public getValueOfDoNotShowAgain()Z
    .locals 3

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->mContext:Landroid/content/Context;

    const-string v1, "DragAndDropDialogOff"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public setOkButtonClickListener(Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$OkButtonClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$OkButtonClickListener;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->mOkButtonListener:Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$OkButtonClickListener;

    .line 96
    return-void
.end method

.method public show(II)V
    .locals 8
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v7, 0x1

    .line 36
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->mDragDialog:Landroid/app/Dialog;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->mDragDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->isShowing()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->getValueOfDoNotShowAgain()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 84
    :cond_1
    :goto_0
    return-void

    .line 39
    :cond_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 40
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 41
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f030053

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 43
    .local v3, "layout":Landroid/view/View;
    const v5, 0x7f0f0050

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 44
    .local v4, "message1":Landroid/widget/TextView;
    const v5, 0x7f0f001d

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 46
    .local v1, "checkBoxDoNotShowAgain":Landroid/widget/CheckBox;
    const v5, 0x7f0e02e3

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 48
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 49
    const v5, 0x7f0e02e2

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 50
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 51
    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->setDoNotShowAgain(Z)V

    .line 53
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 54
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 55
    new-instance v5, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$1;

    invoke-direct {v5, p0}, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$1;-><init>(Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;)V

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    new-instance v5, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$2;

    invoke-direct {v5, p0}, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$2;-><init>(Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;)V

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 67
    const v5, 0x7f0e00db

    new-instance v6, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$3;

    invoke-direct {v6, p0, v1}, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$3;-><init>(Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 78
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->mContext:Landroid/content/Context;

    check-cast v5, Landroid/app/Activity;

    new-instance v6, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$4;

    invoke-direct {v6, p0, v0}, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$4;-><init>(Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;Landroid/app/AlertDialog$Builder;)V

    invoke-virtual {v5, v6}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
