.class public Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ShowBargeInNotificationCmd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;
    }
.end annotation


# static fields
.field private static final VOICE_NOTI_STATUS:I = 0x7010004

.field private static mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 31
    return-void
.end method

.method private static hideNotification(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 112
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 114
    .local v0, "manager":Landroid/app/NotificationManager;
    const v1, 0x7010004

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 115
    return-void
.end method

.method private showNotification(Landroid/content/Context;)V
    .locals 19
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 54
    .local v11, "res":Landroid/content/res/Resources;
    invoke-virtual {v11}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    .line 55
    .local v3, "config":Landroid/content/res/Configuration;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    .line 56
    .local v4, "globalLocale":Ljava/util/Locale;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    const-string/jumbo v16, "voicetalk_language"

    invoke-static/range {v15 .. v16}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 58
    .local v13, "voiceLocale":Ljava/lang/String;
    if-nez v13, :cond_1

    .line 59
    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v13

    .line 65
    :cond_0
    :goto_0
    const-string/jumbo v15, "zh"

    invoke-virtual {v13, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 66
    const/4 v14, 0x0

    .line 68
    .local v14, "voiceLocaleArray":[Ljava/lang/String;
    :try_start_0
    const-string v15, "-"

    invoke-virtual {v13, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 71
    new-instance v15, Ljava/util/Locale;

    const/16 v16, 0x0

    aget-object v16, v14, v16

    const/16 v17, 0x1

    aget-object v17, v14, v17

    invoke-direct/range {v15 .. v17}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v15, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    .end local v14    # "voiceLocaleArray":[Ljava/lang/String;
    :goto_1
    const/4 v15, 0x0

    invoke-virtual {v11, v3, v15}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 79
    const v15, 0x7f090042

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 82
    .local v2, "bargeInCommands":[Ljava/lang/String;
    iput-object v4, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 83
    const/4 v15, 0x0

    invoke-virtual {v11, v3, v15}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 85
    const v15, 0x7f0e025d

    const/16 v16, 0x7

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    aget-object v18, v2, v18

    aput-object v18, v16, v17

    const/16 v17, 0x1

    const/16 v18, 0x1

    aget-object v18, v2, v18

    aput-object v18, v16, v17

    const/16 v17, 0x2

    const/16 v18, 0x2

    aget-object v18, v2, v18

    aput-object v18, v16, v17

    const/16 v17, 0x3

    const/16 v18, 0x3

    aget-object v18, v2, v18

    aput-object v18, v16, v17

    const/16 v17, 0x4

    const/16 v18, 0x4

    aget-object v18, v2, v18

    aput-object v18, v16, v17

    const/16 v17, 0x5

    const/16 v18, 0x5

    aget-object v18, v2, v18

    aput-object v18, v16, v17

    const/16 v17, 0x6

    const/16 v18, 0x6

    aget-object v18, v2, v18

    aput-object v18, v16, v17

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 89
    .local v7, "notiString":Ljava/lang/String;
    const-string v15, "notification"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/NotificationManager;

    .line 91
    .local v6, "manager":Landroid/app/NotificationManager;
    new-instance v8, Landroid/app/Notification;

    const v15, 0x7f0203a9

    const-wide/16 v16, 0x0

    move-wide/from16 v0, v16

    invoke-direct {v8, v15, v7, v0, v1}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 92
    .local v8, "notification":Landroid/app/Notification;
    iget v15, v8, Landroid/app/Notification;->flags:I

    or-int/lit8 v15, v15, 0x10

    iput v15, v8, Landroid/app/Notification;->flags:I

    .line 94
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 95
    .local v5, "intent":Landroid/content/Intent;
    const-string v15, "android.intent.action.VOICE_SETTING_BARGEIN"

    invoke-virtual {v5, v15}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 97
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    .line 98
    .local v10, "pm":Landroid/content/pm/PackageManager;
    const/4 v15, 0x0

    invoke-virtual {v10, v5, v15}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v12

    .line 100
    .local v12, "ri":Landroid/content/pm/ResolveInfo;
    if-nez v12, :cond_3

    .line 101
    const v15, 0x7f0e002b

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v8, v0, v15, v7, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 108
    :goto_2
    const v15, 0x7010004

    invoke-virtual {v6, v15, v8}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 109
    return-void

    .line 60
    .end local v2    # "bargeInCommands":[Ljava/lang/String;
    .end local v5    # "intent":Landroid/content/Intent;
    .end local v6    # "manager":Landroid/app/NotificationManager;
    .end local v7    # "notiString":Ljava/lang/String;
    .end local v8    # "notification":Landroid/app/Notification;
    .end local v10    # "pm":Landroid/content/pm/PackageManager;
    .end local v12    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_1
    const-string/jumbo v15, "v-es-LA"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 61
    const-string v13, "es"

    goto/16 :goto_0

    .line 75
    :cond_2
    new-instance v15, Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v16

    invoke-virtual {v4}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v15, v13, v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v15, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    goto/16 :goto_1

    .line 104
    .restart local v2    # "bargeInCommands":[Ljava/lang/String;
    .restart local v5    # "intent":Landroid/content/Intent;
    .restart local v6    # "manager":Landroid/app/NotificationManager;
    .restart local v7    # "notiString":Ljava/lang/String;
    .restart local v8    # "notification":Landroid/app/Notification;
    .restart local v10    # "pm":Landroid/content/pm/PackageManager;
    .restart local v12    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_3
    const/4 v15, 0x1

    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-static {v0, v15, v5, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    .line 105
    .local v9, "pendingIntent":Landroid/app/PendingIntent;
    const v15, 0x7f0e002b

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v8, v0, v15, v7, v9}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    goto :goto_2

    .line 72
    .end local v2    # "bargeInCommands":[Ljava/lang/String;
    .end local v5    # "intent":Landroid/content/Intent;
    .end local v6    # "manager":Landroid/app/NotificationManager;
    .end local v7    # "notiString":Ljava/lang/String;
    .end local v8    # "notification":Landroid/app/Notification;
    .end local v9    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v10    # "pm":Landroid/content/pm/PackageManager;
    .end local v12    # "ri":Landroid/content/pm/ResolveInfo;
    .restart local v14    # "voiceLocaleArray":[Ljava/lang/String;
    :catch_0
    move-exception v15

    goto/16 :goto_1
.end method

.method private showStatusBarForVoice(Landroid/app/Activity;)V
    .locals 8
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 118
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    .line 119
    .local v1, "decorView":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    .line 120
    .local v2, "flag":I
    or-int/lit16 v2, v2, 0x400

    .line 121
    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 123
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 124
    .local v0, "attrs":Landroid/view/WindowManager$LayoutParams;
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v3, v3, -0x401

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 125
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 127
    sget-object v3, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$1;

    invoke-direct {v4, p0, p1}, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd;Landroid/app/Activity;)V

    const-wide/16 v6, 0xfa0

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 135
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 4
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 39
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Object;

    move-object v2, v3

    check-cast v2, [Ljava/lang/Object;

    .line 40
    .local v2, "params":[Ljava/lang/Object;
    const/4 v3, 0x0

    aget-object v0, v2, v3

    check-cast v0, Landroid/content/Context;

    .line 41
    .local v0, "context":Landroid/content/Context;
    const/4 v3, 0x1

    aget-object v1, v2, v3

    check-cast v1, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;

    .line 43
    .local v1, "isShow":Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;
    sget-object v3, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;->SHOW:Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;

    if-ne v1, v3, :cond_0

    move-object v3, v0

    .line 44
    check-cast v3, Landroid/app/Activity;

    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd;->showStatusBarForVoice(Landroid/app/Activity;)V

    .line 45
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd;->showNotification(Landroid/content/Context;)V

    .line 49
    :goto_0
    return-void

    .line 47
    :cond_0
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd;->hideNotification(Landroid/content/Context;)V

    goto :goto_0
.end method
