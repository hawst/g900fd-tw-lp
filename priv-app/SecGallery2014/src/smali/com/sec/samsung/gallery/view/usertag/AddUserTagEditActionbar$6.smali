.class Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$6;
.super Ljava/lang/Object;
.source "AddUserTagEditActionbar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->setTitle(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

.field final synthetic val$title:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$6;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$6;->val$title:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 322
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$6;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mUsingSelectionDropDown:Z

    if-eqz v0, :cond_1

    .line 323
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$6;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$6;->val$title:Ljava/lang/String;

    # setter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mTitle:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$502(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;Ljava/lang/String;)Ljava/lang/String;

    .line 324
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$6;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$6;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$100(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f0f0018

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    # setter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mSelectionButton:Landroid/widget/Button;
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$602(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;Landroid/widget/Button;)Landroid/widget/Button;

    .line 325
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$6;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mSelectionButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$600(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$6;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mSelectionButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$600(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$6;->val$title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 327
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$6;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$200(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 330
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$6;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$6;->val$title:Ljava/lang/String;

    # setter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mTitle:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$502(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;Ljava/lang/String;)Ljava/lang/String;

    .line 331
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$6;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->access$200(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Landroid/app/ActionBar;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$6;->val$title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
