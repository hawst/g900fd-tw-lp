.class public Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;
.super Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;
.source "GallerySearchItemAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final DAY_MSEC:J

.field private final MONTH_MSEC:J

.field private mCheckboxLength:I

.field private mChildSetPath:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

.field private mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

.field private mDataVersion:J

.field private mFilterTagViewHeightPixel:I

.field private mGenericMotionKeywordFocus:I

.field private mIsChangeSearchingCondition:Z

.field private mIsFilterVisible:Z

.field private mIsTagCloudViewListMode:Z

.field private mLeftMargin:I

.field private mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mRefreshTagListListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnRefreshTagListListener;

.field private mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

.field private mSelectedFilterItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedItemsListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnSelectedItemsListener;

.field protected final mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mSetPath:Ljava/lang/String;

.field private mTagList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "setPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 89
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;-><init>(Landroid/content/Context;)V

    .line 61
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    .line 62
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 67
    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mFilterTagViewHeightPixel:I

    .line 68
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mIsFilterVisible:Z

    .line 69
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mIsTagCloudViewListMode:Z

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mGenericMotionKeywordFocus:I

    .line 75
    const-wide/32 v0, 0x5265c00

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->DAY_MSEC:J

    .line 76
    const-wide v0, 0x9a7ec800L

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->MONTH_MSEC:J

    .line 77
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mTagList:Ljava/util/ArrayList;

    .line 78
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mIsChangeSearchingCondition:Z

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSelectedFilterItems:Ljava/util/ArrayList;

    .line 81
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSelectedItemsListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnSelectedItemsListener;

    .line 82
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mRefreshTagListListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnRefreshTagListListener;

    .line 90
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSetPath:Ljava/lang/String;

    .line 91
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    .line 93
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    .line 94
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSetPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 95
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSetPath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    const-string v1, "children"

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mChildSetPath:Ljava/lang/String;

    .line 96
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mChildSetPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    .line 97
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 98
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    .line 100
    const/high16 v0, 0x40800000    # 4.0f

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->dpToPixel(F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mLeftMargin:I

    .line 101
    const/high16 v0, 0x42700000    # 60.0f

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->dpToPixel(F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mCheckboxLength:I

    .line 102
    return-void
.end method

.method private drawDecorView(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 25
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 345
    if-nez p3, :cond_1

    .line 432
    :cond_0
    :goto_0
    return-void

    .line 347
    :cond_1
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v22

    const/16 v23, 0x4

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_6

    move-object/from16 v0, p3

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    move/from16 v22, v0

    if-nez v22, :cond_6

    const/16 v17, 0x1

    .line 349
    .local v17, "isPlayable":Z
    :goto_1
    invoke-static/range {p3 .. p3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v15

    .line 350
    .local v15, "isPanorama":Z
    const-wide/16 v22, 0x20

    move-object/from16 v0, p3

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v22

    if-eqz v22, :cond_7

    const-wide/32 v22, 0x40000

    move-object/from16 v0, p3

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v22

    if-nez v22, :cond_7

    const/4 v10, 0x1

    .line 351
    .local v10, "isCinePic":Z
    :goto_2
    const-wide/16 v22, 0x10

    move-object/from16 v0, p3

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v19

    .line 352
    .local v19, "isSoundScene":Z
    invoke-static/range {p3 .. p3}, Lcom/sec/samsung/gallery/util/Panorama3DUtil;->is3DPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v22

    if-nez v22, :cond_2

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/data/MediaItem;->is3DPanorama()Z

    move-result v22

    if-eqz v22, :cond_8

    :cond_2
    const/4 v6, 0x1

    .line 353
    .local v6, "is3DPanorama":Z
    :goto_3
    const-wide/16 v22, 0x800

    move-object/from16 v0, p3

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v13

    .line 354
    .local v13, "isMagicShot":Z
    const-wide/16 v22, 0x1000

    move-object/from16 v0, p3

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v9

    .line 355
    .local v9, "isBestPhoto":Z
    const-wide/16 v22, 0x2000

    move-object/from16 v0, p3

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v8

    .line 356
    .local v8, "isBestFace":Z
    const-wide/16 v22, 0x4000

    move-object/from16 v0, p3

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v12

    .line 357
    .local v12, "isEraser":Z
    const-wide/32 v22, 0x8000

    move-object/from16 v0, p3

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v11

    .line 358
    .local v11, "isDramaShot":Z
    const-wide/32 v22, 0x10000

    move-object/from16 v0, p3

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v16

    .line 359
    .local v16, "isPicMotion":Z
    const-wide/16 v22, 0x400

    move-object/from16 v0, p3

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v7

    .line 360
    .local v7, "is3DTour":Z
    const-wide/32 v22, 0x20000

    move-object/from16 v0, p3

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v14

    .line 361
    .local v14, "isOutOfFocus":Z
    const-wide/32 v22, 0x40000

    move-object/from16 v0, p3

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v22

    if-nez v22, :cond_3

    sget-boolean v22, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsGolfIconSupported:Z

    if-eqz v22, :cond_9

    const-wide/16 v22, 0x80

    move-object/from16 v0, p3

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v22

    if-eqz v22, :cond_9

    :cond_3
    const/16 v18, 0x1

    .line 367
    .local v18, "isSequence":Z
    :goto_4
    if-nez p1, :cond_4

    .line 368
    new-instance p1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local p1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 370
    .restart local p1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_4
    const/16 v22, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v20

    check-cast v20, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 371
    .local v20, "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v17, :cond_a

    .line 372
    if-nez v20, :cond_5

    .line 373
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f020308

    invoke-virtual/range {v22 .. v24}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 375
    .local v5, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v20, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v20    # "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 376
    .restart local v20    # "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 377
    const/16 v22, 0x2

    const/16 v23, 0x2

    move-object/from16 v0, v20

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 378
    const/16 v22, 0x15e

    const/16 v23, 0x15e

    move-object/from16 v0, v20

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 379
    const/16 v22, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 380
    const/16 v22, 0x3

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 385
    .end local v5    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_5
    :goto_5
    if-eqz v6, :cond_b

    .line 386
    const v21, 0x7f0200fb

    .line 414
    .local v21, "rsrcId":I
    :goto_6
    const/16 v22, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 415
    .local v4, "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v21, :cond_19

    .line 416
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 417
    .restart local v5    # "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v4, :cond_18

    .line 418
    new-instance v4, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v4    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v4, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 419
    .restart local v4    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 420
    const/16 v22, 0x1

    const/16 v23, 0x3

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 421
    const/16 v22, 0x82

    const/16 v23, 0x82

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 422
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 423
    const/16 v22, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 427
    :goto_7
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 347
    .end local v4    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v5    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v6    # "is3DPanorama":Z
    .end local v7    # "is3DTour":Z
    .end local v8    # "isBestFace":Z
    .end local v9    # "isBestPhoto":Z
    .end local v10    # "isCinePic":Z
    .end local v11    # "isDramaShot":Z
    .end local v12    # "isEraser":Z
    .end local v13    # "isMagicShot":Z
    .end local v14    # "isOutOfFocus":Z
    .end local v15    # "isPanorama":Z
    .end local v16    # "isPicMotion":Z
    .end local v17    # "isPlayable":Z
    .end local v18    # "isSequence":Z
    .end local v19    # "isSoundScene":Z
    .end local v20    # "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v21    # "rsrcId":I
    :cond_6
    const/16 v17, 0x0

    goto/16 :goto_1

    .line 350
    .restart local v15    # "isPanorama":Z
    .restart local v17    # "isPlayable":Z
    :cond_7
    const/4 v10, 0x0

    goto/16 :goto_2

    .line 352
    .restart local v10    # "isCinePic":Z
    .restart local v19    # "isSoundScene":Z
    :cond_8
    const/4 v6, 0x0

    goto/16 :goto_3

    .line 361
    .restart local v6    # "is3DPanorama":Z
    .restart local v7    # "is3DTour":Z
    .restart local v8    # "isBestFace":Z
    .restart local v9    # "isBestPhoto":Z
    .restart local v11    # "isDramaShot":Z
    .restart local v12    # "isEraser":Z
    .restart local v13    # "isMagicShot":Z
    .restart local v14    # "isOutOfFocus":Z
    .restart local v16    # "isPicMotion":Z
    :cond_9
    const/16 v18, 0x0

    goto/16 :goto_4

    .line 382
    .restart local v18    # "isSequence":Z
    .restart local v20    # "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_a
    if-eqz v20, :cond_5

    .line 383
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto/16 :goto_5

    .line 387
    :cond_b
    if-eqz v15, :cond_c

    .line 388
    const v21, 0x7f020111

    .restart local v21    # "rsrcId":I
    goto/16 :goto_6

    .line 389
    .end local v21    # "rsrcId":I
    :cond_c
    if-eqz v10, :cond_d

    .line 390
    const v21, 0x7f02010e

    .restart local v21    # "rsrcId":I
    goto/16 :goto_6

    .line 391
    .end local v21    # "rsrcId":I
    :cond_d
    if-eqz v19, :cond_e

    .line 392
    const v21, 0x7f02011c

    .restart local v21    # "rsrcId":I
    goto/16 :goto_6

    .line 393
    .end local v21    # "rsrcId":I
    :cond_e
    if-eqz v13, :cond_f

    .line 394
    const v21, 0x7f020118

    .restart local v21    # "rsrcId":I
    goto/16 :goto_6

    .line 395
    .end local v21    # "rsrcId":I
    :cond_f
    if-eqz v9, :cond_10

    .line 396
    const v21, 0x7f0200ff

    .restart local v21    # "rsrcId":I
    goto/16 :goto_6

    .line 397
    .end local v21    # "rsrcId":I
    :cond_10
    if-eqz v8, :cond_11

    .line 398
    const v21, 0x7f0200fe

    .restart local v21    # "rsrcId":I
    goto/16 :goto_6

    .line 399
    .end local v21    # "rsrcId":I
    :cond_11
    if-eqz v12, :cond_12

    .line 400
    const v21, 0x7f020109

    .restart local v21    # "rsrcId":I
    goto/16 :goto_6

    .line 401
    .end local v21    # "rsrcId":I
    :cond_12
    if-eqz v11, :cond_13

    .line 402
    const v21, 0x7f020107

    .restart local v21    # "rsrcId":I
    goto/16 :goto_6

    .line 403
    .end local v21    # "rsrcId":I
    :cond_13
    if-eqz v16, :cond_14

    .line 404
    const v21, 0x7f020105

    .restart local v21    # "rsrcId":I
    goto/16 :goto_6

    .line 405
    .end local v21    # "rsrcId":I
    :cond_14
    if-eqz v7, :cond_15

    .line 406
    const v21, 0x7f0200fc

    .restart local v21    # "rsrcId":I
    goto/16 :goto_6

    .line 407
    .end local v21    # "rsrcId":I
    :cond_15
    if-eqz v14, :cond_16

    .line 408
    const v21, 0x7f02010b

    .restart local v21    # "rsrcId":I
    goto/16 :goto_6

    .line 409
    .end local v21    # "rsrcId":I
    :cond_16
    if-eqz v18, :cond_17

    .line 410
    const v21, 0x7f020117

    .restart local v21    # "rsrcId":I
    goto/16 :goto_6

    .line 412
    .end local v21    # "rsrcId":I
    :cond_17
    const/16 v21, 0x0

    .restart local v21    # "rsrcId":I
    goto/16 :goto_6

    .line 425
    .restart local v4    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .restart local v5    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_18
    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_7

    .line 429
    .end local v5    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_19
    if-eqz v4, :cond_0

    .line 430
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private drawFocusBox(Lcom/sec/android/gallery3d/glcore/GlImageView;IZ)V
    .locals 4
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I
    .param p3, "keyword"    # Z

    .prologue
    const/4 v3, 0x5

    .line 487
    invoke-virtual {p1, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 488
    .local v0, "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-nez v0, :cond_0

    .line 489
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 490
    .restart local v0    # "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const v2, 0x7f02026e

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 491
    invoke-virtual {p1, v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 494
    :cond_0
    if-eqz p3, :cond_1

    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mGenericMotionKeywordFocus:I

    .line 495
    .local v1, "genericMotionKeywordFocus":I
    :goto_0
    if-ne v1, p2, :cond_2

    .line 496
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 500
    :goto_1
    return-void

    .line 494
    .end local v1    # "genericMotionKeywordFocus":I
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mGenericMotionFocus:I

    goto :goto_0

    .line 498
    .restart local v1    # "genericMotionKeywordFocus":I
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private drawThumbnailImage(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 14
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 589
    if-nez p1, :cond_0

    .line 614
    .end local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_0
    return-void

    .line 592
    .restart local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    if-nez p3, :cond_1

    .line 593
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->readyDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 597
    :cond_1
    :try_start_0
    move/from16 v0, p2

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getThumbnailBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 598
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    if-nez v2, :cond_2

    .line 599
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->readyDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 611
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    .end local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :catch_0
    move-exception v13

    .line 612
    .local v13, "e":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 601
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v2    # "bitmap":Landroid/graphics/Bitmap;
    .restart local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    :try_start_1
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v5

    .line 602
    .local v5, "rotation":I
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceThumbnail:Z

    if-eqz v1, :cond_3

    move-object/from16 v0, p3

    instance-of v1, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v1, :cond_3

    invoke-static/range {p3 .. p3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 604
    check-cast p3, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/data/LocalImage;->getRectOfAllFaces()Landroid/graphics/RectF;

    move-result-object v6

    .line 605
    .local v6, "faceRect":Landroid/graphics/RectF;
    const/16 v3, 0x140

    const/16 v4, 0x140

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setCroppedBitmap(Landroid/graphics/Bitmap;IIILandroid/graphics/RectF;)V

    goto :goto_0

    .line 608
    .end local v6    # "faceRect":Landroid/graphics/RectF;
    .restart local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_3
    const/16 v9, 0x140

    const/16 v10, 0x140

    const/4 v12, 0x0

    move-object v7, p1

    move-object v8, v2

    move v11, v5

    invoke-virtual/range {v7 .. v12}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setCroppedBitmap(Landroid/graphics/Bitmap;IIILandroid/graphics/RectF;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private drawUnconfirmIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 7
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/16 v6, 0x30

    const/4 v5, 0x7

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x3

    .line 568
    invoke-virtual {p1, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 569
    .local v0, "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemType()I

    move-result v1

    if-nez v1, :cond_3

    .line 570
    if-nez v0, :cond_0

    .line 571
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 572
    .restart local v0    # "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const v1, 0x7f0200de

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 573
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 574
    invoke-virtual {v0, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 578
    :goto_0
    invoke-virtual {v0, v6, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 579
    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 580
    invoke-virtual {p1, v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 582
    :cond_0
    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 586
    :cond_1
    :goto_1
    return-void

    .line 576
    :cond_2
    invoke-virtual {v0, v4, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    goto :goto_0

    .line 583
    :cond_3
    if-eqz v0, :cond_1

    .line 584
    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private getKeyView(ILcom/sec/android/gallery3d/glcore/GlView;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 13
    .param p1, "position"    # I
    .param p2, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 505
    if-nez p2, :cond_2

    .line 506
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 511
    .local v0, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 514
    .local v5, "res":Landroid/content/res/Resources;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 515
    const v8, 0x7f0d027f

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    int-to-float v4, v8

    .line 519
    .local v4, "photoLabelTextSize":F
    :goto_1
    const v8, 0x7f0e0474

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-array v9, v11, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->getMediaItemCount()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 520
    .local v2, "photoLabel":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->getMediaItemCount()I

    move-result v8

    if-ne v8, v11, :cond_0

    .line 522
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e0475

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 524
    :cond_0
    const/4 v3, 0x0

    .line 525
    .local v3, "photoLabelColor":I
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v8, :cond_4

    .line 526
    const/high16 v3, -0x1000000

    .line 529
    :goto_2
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-eqz v8, :cond_1

    .line 530
    const v8, 0x7f0e002b

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 531
    const v8, 0x7f0b0084

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 533
    :cond_1
    invoke-virtual {v0, v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 535
    .local v7, "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    if-nez v7, :cond_5

    .line 536
    invoke-static {v2, v4, v3}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v7

    .line 538
    const/4 v8, 0x3

    invoke-virtual {v7, v11, v8}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 539
    const/16 v8, 0x14

    invoke-virtual {v7, v8, v12, v12, v12}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 540
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v6

    .line 541
    .local v6, "textPaint":Landroid/text/TextPaint;
    invoke-static {}, Lcom/sec/samsung/gallery/util/FontUtil;->getRobotoLightFont()Landroid/graphics/Typeface;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 543
    invoke-direct {p0, v6, v11}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->setTextViewPaint(Landroid/text/TextPaint;Z)V

    .line 545
    const/4 v8, 0x1

    :try_start_0
    invoke-virtual {v0, v7, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 554
    .end local v6    # "textPaint":Landroid/text/TextPaint;
    :goto_3
    return-object v0

    .end local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v2    # "photoLabel":Ljava/lang/String;
    .end local v3    # "photoLabelColor":I
    .end local v4    # "photoLabelTextSize":F
    .end local v5    # "res":Landroid/content/res/Resources;
    .end local v7    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    :cond_2
    move-object v0, p2

    .line 508
    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto/16 :goto_0

    .line 517
    .restart local v5    # "res":Landroid/content/res/Resources;
    :cond_3
    const v8, 0x7f0d027e

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    int-to-float v4, v8

    .restart local v4    # "photoLabelTextSize":F
    goto :goto_1

    .line 528
    .restart local v2    # "photoLabel":Ljava/lang/String;
    .restart local v3    # "photoLabelColor":I
    :cond_4
    const/4 v3, -0x1

    goto :goto_2

    .line 546
    .restart local v6    # "textPaint":Landroid/text/TextPaint;
    .restart local v7    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    :catch_0
    move-exception v1

    .line 547
    .local v1, "ise":Ljava/lang/IllegalStateException;
    sget-object v8, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Cannot add child, "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 551
    .end local v1    # "ise":Ljava/lang/IllegalStateException;
    .end local v6    # "textPaint":Landroid/text/TextPaint;
    :cond_5
    invoke-virtual {v7, v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setText(Ljava/lang/String;)V

    goto :goto_3
.end method

.method private resizeNinePatch(III)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "id"    # I

    .prologue
    const/4 v8, 0x0

    .line 435
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 436
    .local v1, "res":Landroid/content/res/Resources;
    invoke-static {v1, p3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 437
    .local v2, "bmp":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v3

    .line 439
    .local v3, "npChunk":[B
    new-instance v0, Landroid/graphics/drawable/NinePatchDrawable;

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/NinePatchDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;[BLandroid/graphics/Rect;Ljava/lang/String;)V

    .line 440
    .local v0, "ninepatchDrawable":Landroid/graphics/drawable/NinePatchDrawable;
    invoke-virtual {v0, v8, v8, p1, p2}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 441
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 442
    .local v7, "output_bitmap":Landroid/graphics/Bitmap;
    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 443
    .local v6, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v0, v6}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 445
    return-object v7
.end method

.method private setTextViewPaint(Landroid/text/TextPaint;Z)V
    .locals 1
    .param p1, "textPaint"    # Landroid/text/TextPaint;
    .param p2, "forTitle"    # Z

    .prologue
    .line 561
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 565
    return-void
.end method


# virtual methods
.method protected addContentListener()V
    .locals 2

    .prologue
    .line 646
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 647
    return-void
.end method

.method public addKeyword(Ljava/lang/String;)V
    .locals 8
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 132
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    if-nez v4, :cond_1

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 136
    .local v3, "values":Landroid/content/ContentValues;
    const-string/jumbo v4, "title"

    invoke-virtual {v3, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v4, "date_added"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 138
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->deleteHistory(Ljava/lang/String;)V

    .line 140
    const-string v4, ""

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 141
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string v5, "search_history"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6, v3}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 142
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mTagList:Ljava/util/ArrayList;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mTagList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 143
    const/4 v0, 0x0

    .line 144
    .local v0, "convertKeyword":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mTagList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 145
    .local v2, "tag":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    const/4 v0, 0x0

    .line 146
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getName(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 147
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 148
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object p1

    .line 153
    .end local v0    # "convertKeyword":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "tag":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    invoke-virtual {v4, p1}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->addKeyword(Ljava/lang/String;)V

    .line 154
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mIsChangeSearchingCondition:Z

    .line 155
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    if-eqz v4, :cond_0

    .line 156
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->notifyDirty()V

    goto :goto_0
.end method

.method public addSelectedItem(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V
    .locals 3
    .param p1, "data"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .prologue
    .line 737
    const/4 v0, 0x0

    .line 738
    .local v0, "tagDataString":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTagFilterType()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Time"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 739
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getRawTagData()Ljava/lang/String;

    move-result-object v0

    .line 743
    :goto_0
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTagFilterType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getCategoryType()I

    move-result v2

    invoke-virtual {p0, v1, v0, v2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->addTag(Ljava/lang/String;Ljava/lang/String;I)V

    .line 745
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSelectedFilterItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 746
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSelectedFilterItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 747
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSelectedItemsListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnSelectedItemsListener;

    invoke-interface {v1, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnSelectedItemsListener;->setSelectedItemsLists(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V

    .line 749
    :cond_0
    return-void

    .line 741
    :cond_1
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public addTag(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "filterType"    # Ljava/lang/String;
    .param p2, "tagType"    # Ljava/lang/String;
    .param p3, "categoryType"    # I

    .prologue
    .line 683
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    if-nez v0, :cond_1

    .line 689
    :cond_0
    :goto_0
    return-void

    .line 685
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->addTag(Ljava/lang/String;Ljava/lang/String;I)V

    .line 686
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mIsChangeSearchingCondition:Z

    .line 687
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    if-eqz v0, :cond_0

    .line 688
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->notifyDirty()V

    goto :goto_0
.end method

.method public changeKeyword(Ljava/lang/String;)V
    .locals 5
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 160
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mTagList:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mTagList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 161
    const/4 v0, 0x0

    .line 162
    .local v0, "convertKeyword":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mTagList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 163
    .local v2, "tag":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    const/4 v0, 0x0

    .line 164
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getName(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 165
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 166
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object p1

    .line 172
    .end local v0    # "convertKeyword":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "tag":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    if-eqz v3, :cond_2

    .line 173
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    invoke-virtual {v3, p1}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->changeKeyword(Ljava/lang/String;)Z

    .line 174
    :cond_2
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mIsChangeSearchingCondition:Z

    .line 175
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    if-eqz v3, :cond_3

    .line 176
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->notifyDirty()V

    .line 177
    :cond_3
    return-void
.end method

.method public checkSearchConditionChange()Z
    .locals 1

    .prologue
    .line 692
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mIsChangeSearchingCondition:Z

    return v0
.end method

.method public clearFilterList()V
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    if-eqz v0, :cond_0

    .line 710
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->freeFilterList()V

    .line 711
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mIsChangeSearchingCondition:Z

    .line 712
    return-void
.end method

.method public contentDirty()V
    .locals 1

    .prologue
    .line 725
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    if-eqz v0, :cond_0

    .line 726
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->notifyDirty()V

    .line 727
    :cond_0
    return-void
.end method

.method public drawCheckboxAndMark(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 7
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v3, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 449
    if-nez p1, :cond_1

    .line 482
    :cond_0
    :goto_0
    return-void

    .line 452
    :cond_1
    invoke-virtual {p1, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 456
    .local v0, "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 458
    if-nez v0, :cond_2

    .line 459
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 460
    .restart local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {p1, v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 462
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2, p3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 463
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v2, :cond_3

    const v2, 0x7f020122

    :goto_1
    invoke-virtual {v3, v4, v2}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 471
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    :goto_2
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 472
    invoke-virtual {v0, v6, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 473
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 474
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mLeftMargin:I

    invoke-virtual {v0, v2, v5, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 475
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mCheckboxLength:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mCheckboxLength:I

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 476
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0

    .line 463
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_3
    const v2, 0x7f0204ad

    goto :goto_1

    .line 467
    :cond_4
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v2, :cond_5

    const v2, 0x7f020121

    :goto_3
    invoke-virtual {v3, v4, v2}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .restart local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    goto :goto_2

    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_5
    const v2, 0x7f0204a6

    goto :goto_3

    .line 478
    :cond_6
    if-eqz v0, :cond_0

    .line 479
    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public getAlbumPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mChildSetPath:Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->getMediaItemCount()I

    move-result v0

    return v0
.end method

.method public getFilterHeight(Z)I
    .locals 1
    .param p1, "careVisible"    # Z

    .prologue
    .line 663
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mIsFilterVisible:Z

    if-nez v0, :cond_0

    .line 664
    const/4 v0, 0x0

    .line 665
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mFilterTagViewHeightPixel:I

    goto :goto_0
.end method

.method public getGenericMotionKeywordFocus()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mGenericMotionKeywordFocus:I

    return v0
.end method

.method public getHistoryList(Ljava/util/List;)V
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 217
    .local p1, "nameList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;>;"
    const/4 v14, 0x0

    .line 218
    .local v14, "cursor":Landroid/database/Cursor;
    const-string v9, "date_added DESC"

    .line 219
    .local v9, "orderClause":Ljava/lang/String;
    const/16 v18, 0x0

    .line 220
    .local v18, "name":Ljava/lang/String;
    const-wide/16 v20, 0x0

    .line 221
    .local v20, "time":J
    const/4 v11, 0x0

    .line 222
    .local v11, "count":I
    const/16 v10, 0x32

    .line 223
    .local v10, "MAX":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 224
    .local v12, "currentTime":J
    const-wide v4, 0x9a7ec800L

    sub-long v16, v12, v4

    .line 227
    .local v16, "limit_30_Day":J
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string v5, "search_history"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string/jumbo v8, "title"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "date_added"

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v4 .. v9}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 232
    if-eqz v14, :cond_4

    .line 233
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 235
    :cond_0
    const/4 v4, 0x0

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 236
    const/4 v4, 0x1

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 237
    cmp-long v4, v20, v16

    if-ltz v4, :cond_1

    if-lt v11, v10, :cond_2

    .line 238
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->deleteHistory(Ljava/lang/String;)V

    .line 240
    :cond_2
    new-instance v4, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-direct {v4, v0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;-><init>(Ljava/lang/String;J)V

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    add-int/lit8 v11, v11, 0x1

    .line 242
    const/16 v4, 0x31

    if-lt v11, v4, :cond_6

    .line 246
    :cond_3
    :goto_0
    invoke-interface {v14}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 251
    :cond_4
    if-eqz v14, :cond_5

    .line 252
    invoke-static {v14}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 254
    :cond_5
    :goto_1
    return-void

    .line 244
    :cond_6
    :try_start_1
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-nez v4, :cond_0

    goto :goto_0

    .line 248
    :catch_0
    move-exception v15

    .line 249
    .local v15, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 251
    if-eqz v14, :cond_5

    .line 252
    invoke-static {v14}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .line 251
    .end local v15    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-eqz v14, :cond_7

    .line 252
    invoke-static {v14}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :cond_7
    throw v4
.end method

.method public getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getCount()I

    move-result v0

    if-gtz v0, :cond_0

    .line 304
    const/4 v0, 0x0

    .line 306
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemImage(II)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "position"    # I
    .param p2, "type"    # I

    .prologue
    const/4 v0, 0x0

    .line 311
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getCount()I

    move-result v1

    if-ge p1, v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-nez v1, :cond_1

    .line 317
    :cond_0
    :goto_0
    return-object v0

    .line 314
    :cond_1
    const/4 v1, 0x2

    if-ne p2, v1, :cond_0

    .line 315
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0, p1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getSelectedItem()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 752
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSelectedFilterItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSource()Lcom/sec/android/gallery3d/data/GallerySearchAlbum;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    return-object v0
.end method

.method public getSuggestionHistoryTime(J)Ljava/lang/String;
    .locals 13
    .param p1, "time"    # J

    .prologue
    .line 257
    const/4 v11, 0x0

    .line 258
    .local v11, "result":Ljava/lang/String;
    const-wide/16 v4, -0x1

    cmp-long v4, p1, v4

    if-nez v4, :cond_0

    .line 259
    const/4 v4, 0x0

    .line 289
    :goto_0
    return-object v4

    .line 261
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 262
    .local v6, "currentDate":Ljava/util/Calendar;
    const/4 v4, 0x2

    invoke-virtual {v6, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 263
    .local v2, "todayMonth":I
    const/4 v4, 0x1

    invoke-virtual {v6, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 264
    .local v1, "todayYear":I
    const/4 v4, 0x5

    invoke-virtual {v6, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 266
    .local v3, "todayDay":I
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 267
    .local v0, "today":Ljava/util/Calendar;
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 268
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    .line 270
    .local v8, "currentTime":J
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "date_format"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 272
    .local v12, "sysDate":Ljava/lang/String;
    const/4 v10, 0x0

    .line 274
    .local v10, "dateStr":Ljava/lang/String;
    const-string v4, "dd-MM-yyyy"

    invoke-virtual {v4, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 275
    const-string v10, "dd/MM"

    .line 279
    :goto_1
    new-instance v7, Ljava/text/SimpleDateFormat;

    invoke-direct {v7, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 281
    .local v7, "dateFormat":Ljava/text/SimpleDateFormat;
    cmp-long v4, p1, v8

    if-ltz v4, :cond_2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v4, v8

    cmp-long v4, v4, p1

    if-lez v4, :cond_2

    .line 282
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-static {v4, p1, p2, v5}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v11

    :goto_2
    move-object v4, v11

    .line 289
    goto :goto_0

    .line 277
    .end local v7    # "dateFormat":Ljava/text/SimpleDateFormat;
    :cond_1
    const-string v10, "MM/dd"

    goto :goto_1

    .line 286
    .restart local v7    # "dateFormat":Ljava/text/SimpleDateFormat;
    :cond_2
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    goto :goto_2
.end method

.method public getSuggestionNames(Z)Ljava/util/List;
    .locals 6
    .param p1, "isOnlyHistory"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 204
    .local v1, "nameList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;>;"
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getHistoryList(Ljava/util/List;)V

    .line 206
    if-nez p1, :cond_0

    .line 207
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mTagList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mTagList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 208
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mTagList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 209
    .local v2, "tag":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    new-instance v3, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getName(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 213
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "tag":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    :cond_0
    return-object v1
.end method

.method public getTagFilter()Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;
    .locals 1

    .prologue
    .line 677
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    if-nez v0, :cond_0

    .line 678
    const/4 v0, 0x0

    .line 679
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->getTagFilter()Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    move-result-object v0

    goto :goto_0
.end method

.method public getThumbnailBitmap(I)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 617
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    .line 618
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0, p1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 620
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "parentLayer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/4 v3, 0x0

    .line 322
    if-nez p3, :cond_0

    .line 323
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getKeyView(ILcom/sec/android/gallery3d/glcore/GlView;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    .line 341
    :goto_0
    return-object v0

    .line 326
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 329
    .local v1, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    iput v3, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mViewResult:I

    .line 330
    if-nez p2, :cond_1

    .line 331
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 336
    .local v0, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_1
    invoke-direct {p0, v0, p1, v1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->drawThumbnailImage(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/android/gallery3d/data/MediaItem;)V

    .line 337
    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->drawUnconfirmIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 338
    invoke-direct {p0, v0, p1, v3}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->drawFocusBox(Lcom/sec/android/gallery3d/glcore/GlImageView;IZ)V

    .line 339
    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->drawCheckboxAndMark(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/android/gallery3d/data/MediaItem;)V

    .line 340
    invoke-direct {p0, v0, p1, v1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->drawDecorView(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/android/gallery3d/data/MediaItem;)V

    goto :goto_0

    .end local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_1
    move-object v0, p2

    .line 333
    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_1
.end method

.method public isSearchKeyEmpty()Z
    .locals 1

    .prologue
    .line 730
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    if-eqz v0, :cond_0

    .line 731
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->isSearchKeyEmpty()Z

    move-result v0

    .line 733
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isTagCloudListVisible()Z
    .locals 1

    .prologue
    .line 669
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mIsTagCloudViewListMode:Z

    return v0
.end method

.method protected loadData()Z
    .locals 2

    .prologue
    .line 640
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->reload()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mDataVersion:J

    .line 641
    const/4 v0, 0x1

    return v0
.end method

.method public declared-synchronized onPause()V
    .locals 1

    .prologue
    .line 630
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->onPause()V

    .line 631
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->removeContentListener()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 632
    monitor-exit p0

    return-void

    .line 630
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onResume()V
    .locals 1

    .prologue
    .line 625
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->onResume()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 626
    monitor-exit p0

    return-void

    .line 625
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 636
    return-void
.end method

.method public refreshTagList()V
    .locals 1

    .prologue
    .line 777
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mRefreshTagListListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnRefreshTagListListener;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnRefreshTagListListener;->refreshTagList()V

    .line 778
    return-void
.end method

.method protected removeContentListener()V
    .locals 2

    .prologue
    .line 651
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->removeContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 652
    return-void
.end method

.method public removeSelectedItem(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V
    .locals 3
    .param p1, "data"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .prologue
    .line 762
    const/4 v0, 0x0

    .line 763
    .local v0, "tagDataString":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTagFilterType()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Time"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 764
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getRawTagData()Ljava/lang/String;

    move-result-object v0

    .line 768
    :goto_0
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTagFilterType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->removeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSelectedFilterItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 771
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSelectedFilterItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 772
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSelectedItemsListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnSelectedItemsListener;

    invoke-interface {v1, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnSelectedItemsListener;->setSelectedItemsLists(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V

    .line 774
    :cond_0
    return-void

    .line 766
    :cond_1
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public removeTag(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "filterType"    # Ljava/lang/String;
    .param p2, "tagType"    # Ljava/lang/String;

    .prologue
    .line 700
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    if-nez v0, :cond_1

    .line 706
    :cond_0
    :goto_0
    return-void

    .line 702
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->removeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mIsChangeSearchingCondition:Z

    .line 704
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    if-eqz v0, :cond_0

    .line 705
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->notifyDirty()V

    goto :goto_0
.end method

.method public resetFilterList()V
    .locals 1

    .prologue
    .line 715
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    if-eqz v0, :cond_0

    .line 716
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->freeFilterList()V

    .line 717
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->clusteringSearchResult()Z

    .line 719
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    if-eqz v0, :cond_1

    .line 720
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->notifyDirty()V

    .line 721
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mIsChangeSearchingCondition:Z

    .line 722
    return-void
.end method

.method public resetSearchConditionChange()V
    .locals 1

    .prologue
    .line 696
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mIsChangeSearchingCondition:Z

    .line 697
    return-void
.end method

.method public resetSelectedItem()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 756
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSelectedFilterItems:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 757
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSelectedFilterItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 758
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSelectedFilterItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setFilterTagViewHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 673
    iput p1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mFilterTagViewHeightPixel:I

    .line 674
    return-void
.end method

.method public setFilterVisiblility(Z)V
    .locals 0
    .param p1, "isShow"    # Z

    .prologue
    .line 655
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mIsFilterVisible:Z

    .line 656
    return-void
.end method

.method public setGenericMotionKeywordFocus(I)V
    .locals 0
    .param p1, "focus"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mGenericMotionKeywordFocus:I

    .line 106
    return-void
.end method

.method public setRefreshTagListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnRefreshTagListListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnRefreshTagListListener;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mRefreshTagListListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnRefreshTagListListener;

    .line 129
    return-void
.end method

.method public setSelectedItemListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnSelectedItemsListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnSelectedItemsListener;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mSelectedItemsListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnSelectedItemsListener;

    .line 125
    return-void
.end method

.method public setTagCloudViewListMode(Z)V
    .locals 0
    .param p1, "isList"    # Z

    .prologue
    .line 659
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mIsTagCloudViewListMode:Z

    .line 660
    return-void
.end method

.method public setTagList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 199
    .local p1, "tagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mTagList:Ljava/util/ArrayList;

    .line 200
    return-void
.end method

.method public updateContentWindow()V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->resetContentWindow()V

    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getCount()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->onSizeChanged(I)V

    .line 121
    :cond_0
    return-void
.end method
