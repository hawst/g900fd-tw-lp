.class Lcom/sec/samsung/gallery/util/FileUtil$2;
.super Ljava/lang/Object;
.source "FileUtil.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/OnProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/util/FileUtil;->operateMediaList(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/OnProgressListener;)Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/util/FileUtil;

.field final synthetic val$onProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/util/FileUtil;Lcom/sec/android/gallery3d/data/OnProgressListener;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lcom/sec/samsung/gallery/util/FileUtil$2;->this$0:Lcom/sec/samsung/gallery/util/FileUtil;

    iput-object p2, p0, Lcom/sec/samsung/gallery/util/FileUtil$2;->val$onProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 290
    const/4 v0, 0x0

    return v0
.end method

.method public handleOperation(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 295
    .local p1, "objectList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/FileUtil$2;->val$onProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/FileUtil$2;->val$onProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    invoke-interface {v0, p1}, Lcom/sec/android/gallery3d/data/OnProgressListener;->handleOperation(Ljava/util/List;)Z

    move-result v0

    .line 297
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCompleted(Z)V
    .locals 1
    .param p1, "result"    # Z

    .prologue
    .line 302
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/FileUtil$2;->val$onProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/FileUtil$2;->val$onProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    invoke-interface {v0, p1}, Lcom/sec/android/gallery3d/data/OnProgressListener;->onCompleted(Z)V

    .line 304
    :cond_0
    # getter for: Lcom/sec/samsung/gallery/util/FileUtil;->mTasksForMultiple:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/samsung/gallery/util/FileUtil;->access$100()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    return-void
.end method

.method public onProgress(II)V
    .locals 1
    .param p1, "progress"    # I
    .param p2, "total"    # I

    .prologue
    .line 284
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/FileUtil$2;->val$onProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/FileUtil$2;->val$onProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/gallery3d/data/OnProgressListener;->onProgress(II)V

    .line 286
    :cond_0
    return-void
.end method
