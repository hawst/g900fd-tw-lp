.class Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "LocalDatabaseManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LocalDatabaseHelper"
.end annotation


# instance fields
.field private mDatabase:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 600
    const-string v0, "local.db"

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 601
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 605
    iput-object p1, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    .line 606
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE images_hidden_album (_id INTEGER, title TEXT, mime_type TEXT, latitude REAL, longitude REAL, datetaken INTEGER, date_added INTEGER, date_modified INTEGER, _data TEXT UNIQUE, orientation INTEGER, bucket_id INTEGER, _size INTEGER, width INTEGER, height INTEGER, group_id INTEGER, spherical_mosaic INTEGER, sef_file_type INTEGER, album_name TEXT, hidden_album TEXT, source_album TEXT, faces_rect BLOB );"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 607
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE video_hidden_album (_id INTEGER, title TEXT, mime_type TEXT, latitude REAL, longitude REAL, datetaken INTEGER, date_added INTEGER, date_modified INTEGER, _data TEXT UNIQUE, duration INTEGER, bucket_id INTEGER, _size INTEGER, album_name TEXT, hidden_album TEXT, source_album TEXT, resolution TEXT);"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 608
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE hidden_album_info (bucket_id INTEGER UNIQUE);"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 609
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE album_display_info_table (_id INTEGER, bucketid_order TEXT);"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 610
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE suggest_event_info (_id INTEGER, latitude REAL, longitude REAL, _data TEXT UNIQUE, datetaken INTEGER, locality TEXT, type INTEGER);"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 611
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE images_event_album (pkey INTEGER UNIQUE, _id INTEGER, title TEXT, mime_type TEXT, latitude REAL, longitude REAL, datetaken INTEGER, date_added INTEGER, date_modified INTEGER, _data TEXT, orientation INTEGER, bucket_id INTEGER, _size INTEGER, width INTEGER, height INTEGER, group_id INTEGER, spherical_mosaic INTEGER, sef_file_type INTEGER, bucket_display_name TEXT, suggestion INTEGER, album_create_time INTEGER);"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 612
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE video_event_album (pkey INTEGER UNIQUE, _id INTEGER, title TEXT, mime_type TEXT, latitude REAL, longitude REAL, datetaken INTEGER, date_added INTEGER, date_modified INTEGER, _data TEXT, duration INTEGER, bucket_id INTEGER, _size INTEGER, resolution TEXT, bucket_display_name TEXT, suggestion INTEGER, album_create_time INTEGER);"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 613
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE search_history (title TEXT UNIQUE NOT NULL, date_added INTEGER);"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 614
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I

    .prologue
    .line 618
    const-string v0, "DROP TABLE IF EXISTS images_hidden_album"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 619
    const-string v0, "DROP TABLE IF EXISTS video_hidden_album"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 620
    const-string v0, "DROP TABLE IF EXISTS hidden_album_info"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 621
    const-string v0, "DROP TABLE IF EXISTS album_display_info_table"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 622
    const-string v0, "DROP TABLE IF EXISTS suggest_event_info"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 623
    const-string v0, "DROP TABLE IF EXISTS images_event_album"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 624
    const-string v0, "DROP TABLE IF EXISTS video_event_album"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 625
    const-string v0, "DROP TABLE IF EXISTS search_history"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 627
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 628
    return-void
.end method
