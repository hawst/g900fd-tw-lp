.class Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$2;
.super Ljava/lang/Object;
.source "PhotoActionBarForNormal.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->setupButtons()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 75
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    const v2, 0x7f0f0012

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    # setter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mCameraButton:Landroid/widget/ImageButton;
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->access$802(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;Landroid/widget/ImageButton;)Landroid/widget/ImageButton;

    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mCameraButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->access$800(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Landroid/widget/ImageButton;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mCameraButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->access$800(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Landroid/widget/ImageButton;

    move-result-object v0

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$2$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$2$1;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$2;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mCameraButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->access$800(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Landroid/widget/ImageButton;

    move-result-object v0

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$2$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$2$2;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$2;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 91
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->access$1300(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->showCameraButton(Z)V

    .line 92
    return-void

    .line 91
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
