.class Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;
.super Ljava/lang/Object;
.source "ShowNewAlbumCopyMoveDialogCmd.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/OnProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->handleSelectedOperation(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mErrorReason:Ljava/lang/String;

.field mMediaSize:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

.field final synthetic val$dataProxy:Lcom/sec/android/gallery3d/data/DataManager;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;Lcom/sec/android/gallery3d/data/DataManager;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 166
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    iput-object p2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->val$dataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->mErrorReason:Ljava/lang/String;

    .line 168
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->mMediaSize:Ljava/util/HashMap;

    return-void
.end method

.method private exitSelectionMode(Z)V
    .locals 6
    .param p1, "result"    # Z

    .prologue
    .line 196
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mEventType:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$100(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)I

    move-result v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_MOVE_FILES:I

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mEventType:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$100(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)I

    move-result v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_FILES:I

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mEventType:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$100(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)I

    move-result v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_FROM_SECRETBOX:I

    if-ne v1, v2, :cond_2

    .line 197
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$300(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MEDIA_SCAN"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "file://"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->EXTERNAL_SD_DIRECTORY:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$200()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 198
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$300(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "EXIT_SELECTION_MODE"

    if-eqz p1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mNewAlbumPath:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$700(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 207
    :goto_1
    return-void

    .line 198
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 201
    :cond_2
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 204
    .local v0, "body":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$300(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "EXIT_NEW_ALBUM_MODE"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private operation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 12
    .param p1, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 251
    const/4 v1, 0x0

    .line 252
    .local v1, "result":Z
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v7

    .line 253
    .local v7, "uri":Landroid/net/Uri;
    if-nez v7, :cond_0

    .line 254
    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$800()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Wrong uri = NULL , object = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    const/4 v8, 0x1

    .line 276
    :goto_0
    return v8

    .line 257
    :cond_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->mMediaSize:Ljava/util/HashMap;

    invoke-virtual {v7}, Landroid/net/Uri;->hashCode()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 258
    .local v2, "fileSize":J
    const-wide/16 v4, 0x0

    .line 260
    .local v4, "freeSize":J
    :try_start_0
    new-instance v6, Landroid/os/StatFs;

    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mNewAlbumPath:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$700(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v8}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 261
    .local v6, "statFs":Landroid/os/StatFs;
    invoke-virtual {v6}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v6}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    int-to-long v10, v10

    mul-long v4, v8, v10

    .line 267
    .end local v6    # "statFs":Landroid/os/StatFs;
    :goto_1
    cmp-long v8, v2, v4

    if-lez v8, :cond_1

    .line 268
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$300(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0e0139

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->mErrorReason:Ljava/lang/String;

    .line 269
    const/4 v8, 0x1

    goto :goto_0

    .line 262
    :catch_0
    move-exception v0

    .line 263
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$800()Ljava/lang/String;

    move-result-object v8

    const-string v9, "error while initiating statFs, path : "

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 271
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mOperationId:I
    invoke-static {v8}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)I

    move-result v8

    if-nez v8, :cond_2

    .line 272
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->val$dataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mNewAlbumPath:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$700(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v8, p1, v9, v10}, Lcom/sec/android/gallery3d/data/DataManager;->copyItem(Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    :goto_2
    move v8, v1

    .line 276
    goto :goto_0

    .line 274
    :cond_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->val$dataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mNewAlbumPath:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$700(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v8, p1, v9, v10}, Lcom/sec/android/gallery3d/data/DataManager;->moveItem(Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    goto :goto_2
.end method


# virtual methods
.method public handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 10
    .param p1, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 223
    const/4 v3, 0x0

    .line 224
    .local v3, "result":Z
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->mMediaSize:Ljava/util/HashMap;

    if-nez v5, :cond_0

    .line 225
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;
    invoke-static {v5}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Lcom/sec/samsung/gallery/util/FileUtil;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/util/FileUtil;->getSelectedMediaSize()Ljava/util/HashMap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->mMediaSize:Ljava/util/HashMap;

    .line 226
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v5}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$400(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v6

    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->mMediaSize:Ljava/util/HashMap;

    sget-object v7, Lcom/sec/samsung/gallery/util/FileUtil;->TOTAL_FILE_SIZE_KEY:Ljava/lang/Integer;

    invoke-virtual {v5, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalSize(J)V

    .line 228
    :cond_0
    instance-of v5, p1, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v5, :cond_3

    move-object v4, p1

    .line 229
    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 230
    .local v4, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v2, 0x0

    .line 231
    .local v2, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    sget-object v6, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v6

    .line 232
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v7

    invoke-virtual {v4, v5, v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v2

    .line 233
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 235
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->operation(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v3

    .line 236
    if-nez v3, :cond_1

    .line 242
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v2    # "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_2
    :goto_0
    return v3

    .line 233
    .restart local v2    # "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 240
    .end local v2    # "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_3
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->operation(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v3

    goto :goto_0
.end method

.method public handleOperation(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 247
    .local p1, "objectList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public onCompleted(Z)V
    .locals 4
    .param p1, "result"    # Z

    .prologue
    const/4 v2, 0x0

    .line 171
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$400(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->closeCustomProgressDialog()V

    .line 173
    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->mMediaSize:Ljava/util/HashMap;

    .line 174
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # setter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$002(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;Lcom/sec/samsung/gallery/util/FileUtil;)Lcom/sec/samsung/gallery/util/FileUtil;

    .line 175
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->exitSelectionMode(Z)V

    .line 176
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mOperationTask:Lcom/sec/samsung/gallery/util/MediaOperations;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$500(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Lcom/sec/samsung/gallery/util/MediaOperations;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mOperationTask:Lcom/sec/samsung/gallery/util/MediaOperations;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$500(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Lcom/sec/samsung/gallery/util/MediaOperations;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/MediaOperations;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 177
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # setter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mOperationTask:Lcom/sec/samsung/gallery/util/MediaOperations;
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$502(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;Lcom/sec/samsung/gallery/util/MediaOperations;)Lcom/sec/samsung/gallery/util/MediaOperations;

    .line 193
    :goto_0
    return-void

    .line 180
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # setter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mOperationTask:Lcom/sec/samsung/gallery/util/MediaOperations;
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$502(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;Lcom/sec/samsung/gallery/util/MediaOperations;)Lcom/sec/samsung/gallery/util/MediaOperations;

    .line 182
    if-eqz p1, :cond_2

    .line 183
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$300(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->mErrorReason:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->mErrorReason:Ljava/lang/String;

    :goto_1
    invoke-static {v2, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$300(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0e01f8

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 185
    :cond_2
    const-string v0, ""

    .line 186
    .local v0, "errorMsg":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mOperationId:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 187
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$300(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01c9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 191
    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$300(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 188
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mOperationId:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)I

    move-result v1

    if-nez v1, :cond_3

    .line 189
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$300(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01c8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public onProgress(II)V
    .locals 4
    .param p1, "progress"    # I
    .param p2, "flag"    # I

    .prologue
    .line 217
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    const/4 v0, 0x1

    .line 218
    .local v0, "isUpdateFileSize":Z
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->access$400(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v1

    int-to-long v2, p1

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->increaseProgress(JZ)V

    .line 219
    return-void

    .line 217
    .end local v0    # "isUpdateFileSize":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
