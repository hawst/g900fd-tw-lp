.class public Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;
.super Lcom/sec/samsung/gallery/view/ViewObservable;
.source "RecommendDropboxDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;
    }
.end annotation


# static fields
.field private static final UNIQUE_REQUEST_CODE_OF_YOUR_CHOOSING:I = 0x3039


# instance fields
.field private final AFTER_TWO_MONTH_POPUP:I

.field private final ALREADY_DROPBOX_OOBE_POPUP_SHOW:I

.field private final CAMERA_UPLOAD_POPUP:I

.field private final DISABLE_DROPBOX_OOBE_POPUP_SHOW:I

.field private final STORAGE_FULL_POPUP:I

.field private final mContext:Landroid/content/Context;

.field private mDialogFragment:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

.field private mTimeUtils:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;

.field private mTriggerType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/ViewObservable;-><init>()V

    .line 35
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mTriggerType:I

    .line 36
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mTimeUtils:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;

    .line 41
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->AFTER_TWO_MONTH_POPUP:I

    .line 42
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->STORAGE_FULL_POPUP:I

    .line 43
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->CAMERA_UPLOAD_POPUP:I

    .line 44
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->ALREADY_DROPBOX_OOBE_POPUP_SHOW:I

    .line 45
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->DISABLE_DROPBOX_OOBE_POPUP_SHOW:I

    .line 51
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mContext:Landroid/content/Context;

    .line 52
    new-instance v0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;-><init>(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mTriggerType:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->setDropboxFlag(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->setNextTime(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->getDropboxFlag()I

    move-result v0

    return v0
.end method

.method private getDropboxFlag()I
    .locals 3

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mContext:Landroid/content/Context;

    const-string v1, "recommand_dropbox_show_value"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private setDropboxFlag(I)V
    .locals 3
    .param p1, "isSet"    # I

    .prologue
    .line 160
    move v0, p1

    .line 161
    .local v0, "flag":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mContext:Landroid/content/Context;

    const-string v2, "recommand_dropbox_show_value"

    invoke-static {v1, v2, v0}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 162
    return-void
.end method

.method private setNextTime(I)V
    .locals 4
    .param p1, "nMonths"    # I

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mContext:Landroid/content/Context;

    const-string v1, "recommand_dropbox_time_value"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mTimeUtils:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month_after_N(I)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;J)V

    .line 170
    return-void
.end method


# virtual methods
.method public dismissRecommendDropboxDialog()V
    .locals 2

    .prologue
    .line 151
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 152
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 153
    .local v0, "dlg":Landroid/app/Dialog;
    if-eqz v0, :cond_0

    .line 154
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->dismiss()V

    .line 157
    .end local v0    # "dlg":Landroid/app/Dialog;
    :cond_0
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->setMessage(Ljava/lang/String;)V

    .line 178
    return-void
.end method

.method public setRequestCode(I)V
    .locals 0
    .param p1, "requestCode"    # I

    .prologue
    .line 147
    iput p1, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mTriggerType:I

    .line 148
    return-void
.end method

.method public setTitle(I)V
    .locals 1
    .param p1, "title"    # I

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->setTitle(I)V

    .line 174
    return-void
.end method

.method public showRecommendDropboxDialog()V
    .locals 4

    .prologue
    .line 140
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v3, "dialog"

    invoke-virtual {v2, v1, v3}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    :goto_0
    return-void

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method
