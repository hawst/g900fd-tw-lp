.class public Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;
.super Ljava/lang/Object;
.source "LargeImageTilePool.java"


# instance fields
.field private POOL_LIMIT:I

.field public volatile mIsTilePoolRecycled:Z

.field private final mPool:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/sec/samsung/gallery/decoder/LargeImageTile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/16 v0, 0x3c

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;->POOL_LIMIT:I

    .line 8
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;->mIsTilePoolRecycled:Z

    .line 9
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;->mPool:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 13
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;->mIsTilePoolRecycled:Z

    .line 14
    return-void
.end method


# virtual methods
.method public add(Lcom/sec/samsung/gallery/decoder/LargeImageTile;)Z
    .locals 3
    .param p1, "tile"    # Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    .prologue
    const/4 v0, 0x0

    .line 25
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;->mPool:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v1

    iget v2, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;->POOL_LIMIT:I

    if-lt v1, v2, :cond_1

    .line 26
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->recycle()V

    .line 27
    const/4 p1, 0x0

    .line 30
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;->mPool:Ljava/util/concurrent/ConcurrentLinkedQueue;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;->mPool:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public clearLargeImageTilePool()V
    .locals 2

    .prologue
    .line 42
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;->mIsTilePoolRecycled:Z

    .line 43
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 44
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;->get()Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    move-result-object v0

    .line 45
    .local v0, "tile":Lcom/sec/samsung/gallery/decoder/LargeImageTile;
    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->recycle()V

    goto :goto_0

    .line 49
    .end local v0    # "tile":Lcom/sec/samsung/gallery/decoder/LargeImageTile;
    :cond_1
    return-void
.end method

.method public get()Lcom/sec/samsung/gallery/decoder/LargeImageTile;
    .locals 2

    .prologue
    .line 19
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;->mPool:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    .line 20
    .local v0, "tile":Lcom/sec/samsung/gallery/decoder/LargeImageTile;
    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;->mPool:Ljava/util/concurrent/ConcurrentLinkedQueue;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;->mPool:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;->mPool:Ljava/util/concurrent/ConcurrentLinkedQueue;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;->mPool:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
