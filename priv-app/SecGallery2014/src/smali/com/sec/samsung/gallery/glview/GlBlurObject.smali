.class public Lcom/sec/samsung/gallery/glview/GlBlurObject;
.super Ljava/lang/Object;
.source "GlBlurObject.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/GlBlurObject$1;,
        Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;,
        Lcom/sec/samsung/gallery/glview/GlBlurObject$foregndDrawer;
    }
.end annotation


# static fields
.field public static final ALIGN_BOTTOM:I = 0x18

.field public static final ALIGN_CENTER:I = 0x2

.field private static final ALIGN_HFLAG:I = 0x7

.field public static final ALIGN_HPOS:I = 0x4

.field public static final ALIGN_LEFT:I = 0x1

.field public static final ALIGN_MIDDLE:I = 0x10

.field public static final ALIGN_RIGHT:I = 0x3

.field public static final ALIGN_TOP:I = 0x8

.field private static final ALIGN_VFLAG:I = 0x38

.field public static final ALIGN_VPOS:I = 0x20

.field public static final ROTATE_180:I = 0x80

.field public static final ROTATE_270:I = 0xc0

.field public static final ROTATE_90:I = 0x40

.field private static final ROTATE_FLAG:I = 0x1c0

.field private static final SHL_ROTATE:I = 0x6

.field private static final SHL_VALIGN:I = 0x3


# instance fields
.field protected mContext:Landroid/content/Context;

.field private mCx:F

.field private mCy:F

.field private mGL:Ljavax/microedition/khronos/opengles/GL11;

.field private mH:F

.field private mLevel:F

.field private mNeedUpdate:Z

.field private mPadB:F

.field private mPadL:F

.field private mPadR:F

.field private mPadT:F

.field private mPreset:[Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;

.field protected mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mRotate:F

.field private mRsrcId:I

.field private mSetCount:I

.field private mSetCurrent:I

.field private mTextureId:I

.field private mUploaded:Z

.field private mW:F

.field private final mfndDrawer:Lcom/sec/samsung/gallery/glview/GlBlurObject$foregndDrawer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlLayer;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p3, "setCount"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 44
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mSetCurrent:I

    .line 48
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadL:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadR:F

    .line 49
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadT:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadB:F

    .line 53
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mRsrcId:I

    .line 54
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mUploaded:Z

    .line 55
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mNeedUpdate:Z

    .line 56
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlBlurObject$foregndDrawer;

    invoke-direct {v0, p0, v2}, Lcom/sec/samsung/gallery/glview/GlBlurObject$foregndDrawer;-><init>(Lcom/sec/samsung/gallery/glview/GlBlurObject;Lcom/sec/samsung/gallery/glview/GlBlurObject$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mfndDrawer:Lcom/sec/samsung/gallery/glview/GlBlurObject$foregndDrawer;

    .line 59
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mContext:Landroid/content/Context;

    .line 60
    iput p3, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mSetCount:I

    .line 61
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mSetCurrent:I

    .line 62
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mSetCount:I

    new-array v0, v0, [Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPreset:[Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;

    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPreset:[Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 64
    if-eqz p2, :cond_0

    .line 65
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->getGlRoot()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 66
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mfndDrawer:Lcom/sec/samsung/gallery/glview/GlBlurObject$foregndDrawer;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->addOnForegroundDrawer(Lcom/sec/android/gallery3d/glcore/GlRootView$OnForeground;)V

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGLUtilInstance()Lcom/sec/android/gallery3d/glcore/TUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/TUtils;->getAvailable()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mTextureId:I

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlLayer;IZ)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p3, "setCount"    # I
    .param p4, "isUpdateUpperBorder"    # Z

    .prologue
    .line 72
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/glview/GlBlurObject;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlLayer;I)V

    .line 73
    return-void
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/glview/GlBlurObject;Ljavax/microedition/khronos/opengles/GL11;)Ljavax/microedition/khronos/opengles/GL11;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlBlurObject;
    .param p1, "x1"    # Ljavax/microedition/khronos/opengles/GL11;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/glview/GlBlurObject;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlBlurObject;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mUploaded:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/samsung/gallery/glview/GlBlurObject;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlBlurObject;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mUploaded:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/glview/GlBlurObject;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlBlurObject;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->setBitmapTexture()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/glview/GlBlurObject;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlBlurObject;

    .prologue
    .line 18
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mLevel:F

    return v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/glview/GlBlurObject;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlBlurObject;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->draw()V

    return-void
.end method

.method private draw()V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 173
    const/high16 v0, 0x3f800000    # 1.0f

    .line 174
    .local v0, "colorWeight":F
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v2, 0x1701

    invoke-interface {v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    .line 175
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL11;->glLoadIdentity()V

    .line 176
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v2, 0x1700

    invoke-interface {v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    .line 178
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v2, 0xde1

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mTextureId:I

    invoke-interface {v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBindTexture(II)V

    .line 179
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL11;->glLoadIdentity()V

    .line 180
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mCx:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mCy:F

    invoke-interface {v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL11;->glTranslatef(FFF)V

    .line 181
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mRotate:F

    invoke-interface {v1, v2, v4, v4, v5}, Ljavax/microedition/khronos/opengles/GL11;->glRotatef(FFFF)V

    .line 182
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mW:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mH:F

    invoke-interface {v1, v2, v3, v5}, Ljavax/microedition/khronos/opengles/GL11;->glScalef(FFF)V

    .line 183
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePremultipleAlphaBlend:Z

    if-eqz v1, :cond_0

    .line 184
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mLevel:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mLevel:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mLevel:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mLevel:F

    invoke-interface {v1, v2, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL11;->glColor4f(FFFF)V

    .line 189
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/4 v2, 0x6

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-interface {v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    .line 190
    return-void

    .line 186
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mLevel:F

    invoke-interface {v1, v0, v0, v0, v2}, Ljavax/microedition/khronos/opengles/GL11;->glColor4f(FFFF)V

    goto :goto_0
.end method

.method private getAlignedPos()V
    .locals 13

    .prologue
    .line 134
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->getPaddingBottom()F

    move-result v2

    .line 136
    .local v2, "paddingBottom":F
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPreset:[Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;

    iget v11, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mSetCurrent:I

    aget-object v4, v10, v11

    .line 137
    .local v4, "preset":Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;
    iget v10, v4, Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;->mAlign:I

    and-int/lit16 v10, v10, 0x1c0

    shr-int/lit8 v6, v10, 0x6

    .line 138
    .local v6, "rotate":I
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v10, v10, Lcom/sec/android/gallery3d/glcore/GlRootView;->mWidth:I

    int-to-float v8, v10

    .line 139
    .local v8, "vw":F
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v10, v10, Lcom/sec/android/gallery3d/glcore/GlRootView;->mHeight:I

    int-to-float v7, v10

    .line 140
    .local v7, "vh":F
    iget v5, v4, Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;->mIWidth:F

    .line 141
    .local v5, "pw":F
    iget v3, v4, Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;->mIHeight:F

    .line 142
    .local v3, "ph":F
    const/high16 v10, 0x42b40000    # 90.0f

    int-to-float v11, v6

    mul-float/2addr v10, v11

    iput v10, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mRotate:F

    .line 143
    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mRotate:F

    const/high16 v11, 0x42b40000    # 90.0f

    cmpl-float v10, v10, v11

    if-eqz v10, :cond_0

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mRotate:F

    const/high16 v11, 0x43870000    # 270.0f

    cmpl-float v10, v10, v11

    if-nez v10, :cond_1

    .line 144
    :cond_0
    div-float v9, v5, v8

    .line 145
    .local v9, "w":F
    div-float v1, v3, v7

    .line 146
    .local v1, "h":F
    const/high16 v10, 0x40000000    # 2.0f

    mul-float/2addr v10, v1

    iput v10, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mW:F

    .line 147
    const/high16 v10, 0x40000000    # 2.0f

    mul-float/2addr v10, v9

    iput v10, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mH:F

    .line 155
    :goto_0
    iget v10, v4, Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;->mAlign:I

    and-int/lit8 v0, v10, 0x7

    .line 156
    .local v0, "alignFlag":I
    const/4 v10, 0x1

    if-ne v0, v10, :cond_2

    .line 157
    const/high16 v10, -0x40800000    # -1.0f

    add-float/2addr v10, v9

    const/high16 v11, 0x40000000    # 2.0f

    iget v12, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadL:F

    mul-float/2addr v11, v12

    div-float/2addr v11, v8

    add-float/2addr v10, v11

    iput v10, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mCx:F

    .line 163
    :goto_1
    iget v10, v4, Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;->mAlign:I

    and-int/lit8 v0, v10, 0x38

    .line 164
    const/16 v10, 0x18

    if-ne v0, v10, :cond_4

    .line 165
    const/high16 v10, -0x40800000    # -1.0f

    add-float/2addr v10, v1

    const/high16 v11, 0x40000000    # 2.0f

    mul-float/2addr v11, v2

    div-float/2addr v11, v7

    add-float/2addr v10, v11

    iput v10, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mCy:F

    .line 170
    :goto_2
    return-void

    .line 149
    .end local v0    # "alignFlag":I
    .end local v1    # "h":F
    .end local v9    # "w":F
    :cond_1
    div-float v9, v5, v8

    .line 150
    .restart local v9    # "w":F
    div-float v1, v3, v7

    .line 151
    .restart local v1    # "h":F
    const/high16 v10, 0x40000000    # 2.0f

    mul-float/2addr v10, v9

    iput v10, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mW:F

    .line 152
    const/high16 v10, 0x40000000    # 2.0f

    mul-float/2addr v10, v1

    iput v10, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mH:F

    goto :goto_0

    .line 158
    .restart local v0    # "alignFlag":I
    :cond_2
    const/4 v10, 0x3

    if-ne v0, v10, :cond_3

    .line 159
    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, v9

    const/high16 v11, 0x40000000    # 2.0f

    iget v12, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadR:F

    mul-float/2addr v11, v12

    div-float/2addr v11, v8

    sub-float/2addr v10, v11

    iput v10, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mCx:F

    goto :goto_1

    .line 161
    :cond_3
    const/4 v10, 0x0

    iput v10, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mCx:F

    goto :goto_1

    .line 166
    :cond_4
    const/16 v10, 0x8

    if-ne v0, v10, :cond_5

    .line 167
    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, v1

    const/high16 v11, 0x40000000    # 2.0f

    iget v12, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadT:F

    mul-float/2addr v11, v12

    div-float/2addr v11, v7

    sub-float/2addr v10, v11

    iput v10, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mCy:F

    goto :goto_2

    .line 169
    :cond_5
    const/4 v10, 0x0

    iput v10, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mCy:F

    goto :goto_2
.end method

.method private getPaddingBottom()F
    .locals 3

    .prologue
    .line 214
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadB:F

    .line 215
    .local v0, "paddingBottom":F
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 217
    const/high16 v1, 0x41200000    # 10.0f

    add-float/2addr v0, v1

    .line 219
    :cond_0
    return v0
.end method

.method private setBitmapTexture()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const v6, 0x47012f00    # 33071.0f

    const/high16 v5, 0x46180000    # 9728.0f

    const/16 v4, 0xde1

    .line 198
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mRsrcId:I

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 199
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 200
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const v2, 0x84c0

    invoke-interface {v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glActiveTexture(I)V

    .line 201
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v1, v4}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    .line 203
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mTextureId:I

    invoke-interface {v1, v4, v2}, Ljavax/microedition/khronos/opengles/GL11;->glBindTexture(II)V

    .line 204
    invoke-static {v4, v7, v0, v7}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 205
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 206
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v2, 0x2802

    invoke-interface {v1, v4, v2, v6}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 207
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v2, 0x2803

    invoke-interface {v1, v4, v2, v6}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 208
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v2, 0x2801

    invoke-interface {v1, v4, v2, v5}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 209
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v2, 0x2800

    invoke-interface {v1, v4, v2, v5}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 211
    :cond_0
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mfndDrawer:Lcom/sec/samsung/gallery/glview/GlBlurObject$foregndDrawer;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeForegroundDrawer(Lcom/sec/android/gallery3d/glcore/GlRootView$OnForeground;)V

    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGLUtilInstance()Lcom/sec/android/gallery3d/glcore/TUtils;

    move-result-object v0

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mTextureId:I

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/TUtils;->freeId(I)V

    .line 81
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mTextureId:I

    .line 82
    return-void
.end method

.method public setAlignAndScale(IIII)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "align"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPreset:[Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPreset:[Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;

    new-instance v1, Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;

    invoke-direct {v1, p0, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;-><init>(Lcom/sec/samsung/gallery/glview/GlBlurObject;III)V

    aput-object v1, v0, p1

    .line 89
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mNeedUpdate:Z

    .line 90
    return-void

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPreset:[Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;->set(III)V

    goto :goto_0
.end method

.method public setPadding(FFFF)V
    .locals 1
    .param p1, "left"    # F
    .param p2, "right"    # F
    .param p3, "top"    # F
    .param p4, "bottom"    # F

    .prologue
    .line 103
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadL:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadR:F

    cmpl-float v0, p2, v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadT:F

    cmpl-float v0, p3, v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadB:F

    cmpl-float v0, p4, v0

    if-eqz v0, :cond_1

    .line 104
    :cond_0
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadL:F

    .line 105
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadR:F

    .line 106
    iput p3, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadT:F

    .line 107
    iput p4, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadB:F

    .line 108
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mNeedUpdate:Z

    .line 110
    :cond_1
    return-void
.end method

.method public setPadding(IIII)V
    .locals 2
    .param p1, "left"    # I
    .param p2, "right"    # I
    .param p3, "top"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 93
    int-to-float v0, p1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadL:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    int-to-float v0, p2

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadR:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    int-to-float v0, p3

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadT:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    int-to-float v0, p4

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadB:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 94
    :cond_0
    int-to-float v0, p1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadL:F

    .line 95
    int-to-float v0, p2

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadR:F

    .line 96
    int-to-float v0, p3

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadT:F

    .line 97
    int-to-float v0, p4

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mPadB:F

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mNeedUpdate:Z

    .line 100
    :cond_1
    return-void
.end method

.method public setResourceImage(I)V
    .locals 1
    .param p1, "resourceID"    # I

    .prologue
    .line 193
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mRsrcId:I

    .line 194
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mUploaded:Z

    .line 195
    return-void
.end method

.method public show(IF)V
    .locals 2
    .param p1, "current"    # I
    .param p2, "level"    # F

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 113
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mSetCurrent:I

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mNeedUpdate:Z

    if-eqz v0, :cond_1

    .line 114
    :cond_0
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mSetCurrent:I

    .line 115
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mNeedUpdate:Z

    .line 116
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->getAlignedPos()V

    .line 119
    :cond_1
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mLevel:F

    .line 120
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mLevel:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2

    .line 121
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mLevel:F

    .line 123
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_3

    .line 124
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 126
    :cond_3
    return-void
.end method
