.class Lcom/sec/samsung/gallery/glview/GlSearchView$2;
.super Ljava/lang/Object;
.source "GlSearchView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlSearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V
    .locals 0

    .prologue
    .line 359
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFlingEnd(F)V
    .locals 3
    .param p1, "delta"    # F

    .prologue
    const/4 v2, 0x0

    .line 384
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iput p1, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordScroll:F

    .line 385
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$200(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordScroll:F

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->setScroll(F)V

    .line 386
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setCommand(IIII)V

    .line 387
    return-void
.end method

.method public onFlingProcess(FF)V
    .locals 6
    .param p1, "delta"    # F
    .param p2, "elastic"    # F

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x41f00000    # 30.0f

    const/high16 v3, -0x3e100000    # -30.0f

    const/4 v2, 0x0

    .line 362
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iput p1, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordScroll:F

    .line 363
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$200(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordScroll:F

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->setScroll(F)V

    .line 365
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mUsePitchBoundary:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$300(Lcom/sec/samsung/gallery/glview/GlSearchView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 366
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$500(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->getSpeedRatio()F

    move-result v1

    neg-float v1, v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    # setter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mPitchFact:F
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$402(Lcom/sec/samsung/gallery/glview/GlSearchView;F)F

    .line 367
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mPitchFact:F
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$400(Lcom/sec/samsung/gallery/glview/GlSearchView;)F

    move-result v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_1

    .line 368
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mPitchFact:F
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$402(Lcom/sec/samsung/gallery/glview/GlSearchView;F)F

    .line 371
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    neg-float v1, p2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$200(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setThumbnailPitchRate(FLcom/sec/samsung/gallery/glview/GlScroller;)V

    .line 380
    :goto_1
    return-void

    .line 369
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mPitchFact:F
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$400(Lcom/sec/samsung/gallery/glview/GlSearchView;)F

    move-result v0

    cmpg-float v0, v0, v3

    if-gez v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mPitchFact:F
    invoke-static {v0, v3}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$402(Lcom/sec/samsung/gallery/glview/GlSearchView;F)F

    goto :goto_0

    .line 373
    :cond_2
    cmpl-float v0, p2, v2

    if-lez v0, :cond_3

    .line 374
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$600(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p2}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    goto :goto_1

    .line 375
    :cond_3
    cmpg-float v0, p2, v2

    if-gez v0, :cond_4

    .line 376
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$600(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;

    move-result-object v0

    invoke-virtual {v0, v5, p2}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    goto :goto_1

    .line 378
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$600(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;

    move-result-object v0

    invoke-virtual {v0, v5, v2}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    goto :goto_1
.end method
