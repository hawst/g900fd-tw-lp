.class Lcom/sec/samsung/gallery/view/allview/AllViewState$10;
.super Ljava/lang/Object;
.source "AllViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/allview/AllViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V
    .locals 0

    .prologue
    .line 433
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$10;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenericMotionCancel(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 3
    .param p1, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/4 v2, -0x1

    .line 446
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$10;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # invokes: Lcom/sec/samsung/gallery/view/allview/AllViewState;->setItemFocus(I)V
    invoke-static {v0, v2}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2600(Lcom/sec/samsung/gallery/view/allview/AllViewState;I)V

    .line 447
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$10;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1600(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/glview/GlPhotoView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setFocusIndex(IZ)V

    .line 448
    return-void
.end method

.method public onGenericMotionEnter(Lcom/sec/android/gallery3d/glcore/GlLayer;I)V
    .locals 3
    .param p1, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "position"    # I

    .prologue
    .line 437
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$10;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1600(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/glview/GlPhotoView;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemFocused:I

    if-eq v0, p2, :cond_0

    .line 438
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$10;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # invokes: Lcom/sec/samsung/gallery/view/allview/AllViewState;->setItemFocus(I)V
    invoke-static {v0, p2}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2600(Lcom/sec/samsung/gallery/view/allview/AllViewState;I)V

    .line 439
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$10;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1600(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/glview/GlPhotoView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setFocusIndex(IZ)V

    .line 440
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$10;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$200(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$10;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2000(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v0, v1, p2, v2}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(Ljava/lang/Object;ILcom/sec/android/gallery3d/ui/SelectionManager;)V

    .line 442
    :cond_0
    return-void
.end method
