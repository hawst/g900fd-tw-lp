.class public Lcom/sec/samsung/gallery/controller/PlayOutOfFocusCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "PlayOutOfFocusCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final COMPONENT_NAME:Ljava/lang/String; = "com.sec.android.ofviewer.SEFViewerActivity"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.ofviewer"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method private startOutOfFocusImage(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 6
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 48
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    .line 49
    .local v2, "filePath":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 61
    :goto_0
    return-void

    .line 52
    :cond_0
    :try_start_0
    const-string v4, "PlayOutOfFocusCmd"

    const-string v5, "play outoffocus"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    new-instance v0, Landroid/content/ComponentName;

    const-string v4, "com.sec.android.ofviewer"

    const-string v5, "com.sec.android.ofviewer.SEFViewerActivity"

    invoke-direct {v0, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .local v0, "component":Landroid/content/ComponentName;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 55
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v3, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 56
    const-string v4, "inputfile"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 57
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/PlayOutOfFocusCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 58
    .end local v0    # "component":Landroid/content/ComponentName;
    .end local v3    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 59
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 4
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 37
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Object;

    move-object v2, v3

    check-cast v2, [Ljava/lang/Object;

    .line 38
    .local v2, "params":[Ljava/lang/Object;
    const/4 v3, 0x0

    aget-object v0, v2, v3

    check-cast v0, Landroid/app/Activity;

    .line 39
    .local v0, "activity":Landroid/app/Activity;
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/PlayOutOfFocusCmd;->mContext:Landroid/content/Context;

    .line 40
    const/4 v3, 0x1

    aget-object v1, v2, v3

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 42
    .local v1, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v1, :cond_0

    .line 45
    :goto_0
    return-void

    .line 44
    :cond_0
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/PlayOutOfFocusCmd;->startOutOfFocusImage(Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto :goto_0
.end method
