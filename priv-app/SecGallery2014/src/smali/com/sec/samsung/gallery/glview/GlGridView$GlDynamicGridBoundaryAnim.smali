.class public Lcom/sec/samsung/gallery/glview/GlGridView$GlDynamicGridBoundaryAnim;
.super Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
.source "GlGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GlDynamicGridBoundaryAnim"
.end annotation


# instance fields
.field public mReleaseDist:F

.field public mReleaseSpeed:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGridView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlGridView;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 2007
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlDynamicGridBoundaryAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    .line 2008
    invoke-direct {p0, p2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;-><init>(Landroid/content/Context;)V

    .line 2011
    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 0
    .param p1, "ratio"    # F

    .prologue
    .line 2030
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->applyTransform(F)V

    .line 2036
    return-void
.end method

.method public setInitMovement(F)V
    .locals 0
    .param p1, "movX"    # F

    .prologue
    .line 2022
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 2023
    return-void
.end method

.method public startFling(F)V
    .locals 1
    .param p1, "speed"    # F

    .prologue
    .line 2015
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->startFling(F)V

    .line 2016
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView$GlDynamicGridBoundaryAnim;->getSpeed()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlDynamicGridBoundaryAnim;->mReleaseSpeed:F

    .line 2018
    return-void
.end method
