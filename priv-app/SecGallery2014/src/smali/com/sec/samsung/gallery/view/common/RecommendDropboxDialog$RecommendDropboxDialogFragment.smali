.class Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;
.super Landroid/app/DialogFragment;
.source "RecommendDropboxDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RecommendDropboxDialogFragment"
.end annotation


# static fields
.field protected static final TAG:Ljava/lang/String; = "RecommendDropboxDialogFragment"


# instance fields
.field mAlertDialog:Landroid/app/AlertDialog;

.field private final mContext:Landroid/content/Context;

.field private mMessage:Ljava/lang/String;

.field private mTitleId:I

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;Landroid/content/Context;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 62
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 58
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    .line 60
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->mMessage:Ljava/lang/String;

    .line 63
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->mContext:Landroid/content/Context;

    .line 64
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 65
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 66
    return-void
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f0300c3

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 79
    .local v0, "alertDialogView":Landroid/view/View;
    const v3, 0x7f0f0203

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 80
    .local v2, "tv":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->mMessage:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    const v3, 0x7f0f0124

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 82
    .local v1, "iv":Landroid/widget/ImageView;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mTriggerType:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->access$000(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;)I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mTriggerType:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->access$000(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;)I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_1

    .line 83
    :cond_0
    const v3, 0x7f020193

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 84
    :cond_1
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget v4, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->mTitleId:I

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0e04b1

    new-instance v5, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$2;

    invoke-direct {v5, p0}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$2;-><init>(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0e025e

    new-instance v5, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$1;

    invoke-direct {v5, p0}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$1;-><init>(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    .line 123
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 124
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    return-object v3
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 71
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 72
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->mMessage:Ljava/lang/String;

    .line 133
    return-void
.end method

.method public setTitle(I)V
    .locals 0
    .param p1, "titleId"    # I

    .prologue
    .line 128
    iput p1, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->mTitleId:I

    .line 129
    return-void
.end method
