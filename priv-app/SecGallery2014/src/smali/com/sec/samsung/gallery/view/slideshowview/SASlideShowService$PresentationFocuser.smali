.class Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;
.super Landroid/content/BroadcastReceiver;
.source "SASlideShowService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PresentationFocuser"
.end annotation


# static fields
.field private static final ACTION_PRESENTATION_FOCUS_CHANGED:Ljava/lang/String; = "com.sec.android.app.PRESENTATION_FOCUS_CHANGED"

.field private static final IMEDIA_PLAYER_VIDEO_EXIST:Ljava/lang/String; = "android.media.IMediaPlayer.videoexist"

.field private static final KEY_APP_NAME:Ljava/lang/String; = "app_name"

.field private static final VALUE_GALLERY:Ljava/lang/String; = "gallery"

.field private static final VALUE_VIDEO:Ljava/lang/String; = "video"


# instance fields
.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mIsRegistered:Z

.field private mKnoxModeSwitchFilter:Landroid/content/IntentFilter;

.field private mVideoExistIntentFilter:Landroid/content/IntentFilter;

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)V
    .locals 2

    .prologue
    .line 354
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 366
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.app.PRESENTATION_FOCUS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->mIntentFilter:Landroid/content/IntentFilter;

    .line 367
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.media.IMediaPlayer.videoexist"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->mVideoExistIntentFilter:Landroid/content/IntentFilter;

    .line 368
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.USER_SWITCHED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->mKnoxModeSwitchFilter:Landroid/content/IntentFilter;

    .line 370
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->mIsRegistered:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;

    .prologue
    .line 354
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 397
    const-string v2, "app_name"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 398
    .local v1, "appName":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 399
    .local v0, "action":Ljava/lang/String;
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$500()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "presentation focus changed : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    const-string v2, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 402
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 403
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->dismiss()V

    .line 407
    :cond_0
    const-string v2, "gallery"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "android.media.IMediaPlayer.videoexist"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 408
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->isSlideshowVideoPlaying:Z
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$400(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 409
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->dismiss()V

    .line 412
    :cond_2
    const-string/jumbo v2, "video"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "com.sec.android.app.PRESENTATION_FOCUS_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 413
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 414
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->dismiss()V

    .line 417
    :cond_4
    return-void
.end method

.method public registerReceiver(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 380
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->mIsRegistered:Z

    if-nez v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 382
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->mVideoExistIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 383
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->mKnoxModeSwitchFilter:Landroid/content/IntentFilter;

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 384
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->mIsRegistered:Z

    .line 386
    :cond_0
    return-void
.end method

.method public sendBroadcast(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 373
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "send broadcast : gallery"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.PRESENTATION_FOCUS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 375
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "app_name"

    const-string v2, "gallery"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 376
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 377
    return-void
.end method

.method public unRegisterReceiver(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 389
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->mIsRegistered:Z

    if-eqz v0, :cond_0

    .line 390
    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 391
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->mIsRegistered:Z

    .line 393
    :cond_0
    return-void
.end method
