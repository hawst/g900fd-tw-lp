.class public abstract Lcom/sec/samsung/gallery/drawer/DrawerItem;
.super Ljava/lang/Object;
.source "DrawerItem.java"


# static fields
.field public static final EMPTY_ITEM:Ljava/lang/String; = "EMPTY_ITEM"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mGroupId:I

.field private mIconRes:I

.field private mIsEmptyItem:Z

.field private mIsGroupLastChild:Z

.field protected mMediaSetPath:Ljava/lang/String;

.field private mName:Ljava/lang/CharSequence;

.field protected mNameStringId:I

.field private mTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

.field protected mTtsStringId:I

.field private mbIsGroupNameItem:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/sec/samsung/gallery/drawer/DrawerItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->TAG:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(IIILcom/sec/samsung/gallery/core/TabTagType;)V
    .locals 2
    .param p1, "groupid"    # I
    .param p2, "stringId"    # I
    .param p3, "ttsId"    # I
    .param p4, "tagType"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    const/4 v1, 0x0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIconRes:I

    .line 21
    iput v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mNameStringId:I

    .line 23
    iput v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mTtsStringId:I

    .line 29
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mbIsGroupNameItem:Z

    .line 31
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIsGroupLastChild:Z

    .line 33
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIsEmptyItem:Z

    .line 93
    iput p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mGroupId:I

    .line 94
    iput p2, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mNameStringId:I

    .line 95
    iput p3, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mTtsStringId:I

    .line 96
    iput-object p4, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 98
    if-nez p4, :cond_0

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mbIsGroupNameItem:Z

    .line 101
    :cond_0
    return-void
.end method

.method protected constructor <init>(IILcom/sec/samsung/gallery/core/TabTagType;)V
    .locals 2
    .param p1, "groupid"    # I
    .param p2, "stringId"    # I
    .param p3, "tagType"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIconRes:I

    .line 21
    iput v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mNameStringId:I

    .line 23
    iput v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mTtsStringId:I

    .line 29
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mbIsGroupNameItem:Z

    .line 31
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIsGroupLastChild:Z

    .line 33
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIsEmptyItem:Z

    .line 82
    iput p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mGroupId:I

    .line 83
    iput p2, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mNameStringId:I

    .line 84
    iput p2, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mTtsStringId:I

    .line 85
    iput-object p3, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 87
    if-nez p3, :cond_0

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mbIsGroupNameItem:Z

    .line 90
    :cond_0
    return-void
.end method

.method protected constructor <init>(ILcom/sec/samsung/gallery/core/TabTagType;)V
    .locals 3
    .param p1, "groupid"    # I
    .param p2, "tagType"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIconRes:I

    .line 21
    iput v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mNameStringId:I

    .line 23
    iput v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mTtsStringId:I

    .line 29
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mbIsGroupNameItem:Z

    .line 31
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIsGroupLastChild:Z

    .line 33
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIsEmptyItem:Z

    .line 51
    iput p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mGroupId:I

    .line 52
    const-string v0, "EMPTY_ITEM"

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mName:Ljava/lang/CharSequence;

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 54
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIsEmptyItem:Z

    .line 56
    if-nez p2, :cond_0

    .line 57
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mbIsGroupNameItem:Z

    .line 59
    :cond_0
    return-void
.end method

.method protected constructor <init>(ILjava/lang/CharSequence;ILcom/sec/samsung/gallery/core/TabTagType;)V
    .locals 2
    .param p1, "groupid"    # I
    .param p2, "name"    # Ljava/lang/CharSequence;
    .param p3, "ttsId"    # I
    .param p4, "tagType"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIconRes:I

    .line 21
    iput v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mNameStringId:I

    .line 23
    iput v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mTtsStringId:I

    .line 29
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mbIsGroupNameItem:Z

    .line 31
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIsGroupLastChild:Z

    .line 33
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIsEmptyItem:Z

    .line 71
    iput p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mGroupId:I

    .line 72
    iput-object p2, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mName:Ljava/lang/CharSequence;

    .line 73
    iput-object p4, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 74
    iput p3, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mTtsStringId:I

    .line 76
    if-nez p4, :cond_0

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mbIsGroupNameItem:Z

    .line 79
    :cond_0
    return-void
.end method

.method protected constructor <init>(ILjava/lang/CharSequence;Lcom/sec/samsung/gallery/core/TabTagType;)V
    .locals 2
    .param p1, "groupid"    # I
    .param p2, "name"    # Ljava/lang/CharSequence;
    .param p3, "tagType"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIconRes:I

    .line 21
    iput v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mNameStringId:I

    .line 23
    iput v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mTtsStringId:I

    .line 29
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mbIsGroupNameItem:Z

    .line 31
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIsGroupLastChild:Z

    .line 33
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIsEmptyItem:Z

    .line 61
    iput p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mGroupId:I

    .line 62
    iput-object p2, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mName:Ljava/lang/CharSequence;

    .line 63
    iput-object p3, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 65
    if-nez p3, :cond_0

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mbIsGroupNameItem:Z

    .line 68
    :cond_0
    return-void
.end method


# virtual methods
.method public getGroupId()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mGroupId:I

    return v0
.end method

.method public getName()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mName:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getNameStringId()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mNameStringId:I

    return v0
.end method

.method public getTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    return-object v0
.end method

.method public getTtsId()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mTtsStringId:I

    return v0
.end method

.method public isEmptyItem()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIsEmptyItem:Z

    return v0
.end method

.method public isGroupLastItem()Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIsGroupLastChild:Z

    return v0
.end method

.method public isGroupNameItem()Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mbIsGroupNameItem:Z

    return v0
.end method

.method public abstract selectItem(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/StateManager;IZ)V
.end method

.method public setGroupLastItem(Z)Z
    .locals 0
    .param p1, "isGroupLastChild"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIsGroupLastChild:Z

    return p1
.end method

.method public setIcon(Landroid/widget/ImageView;)V
    .locals 3
    .param p1, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 128
    if-nez p1, :cond_1

    .line 129
    sget-object v0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "imageView is NULL. mName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mName:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , mNameStringId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mNameStringId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 134
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIconRes:I

    if-eqz v0, :cond_0

    .line 135
    iget v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIconRes:I

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public setIconId(I)V
    .locals 0
    .param p1, "iconId"    # I

    .prologue
    .line 124
    iput p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mIconRes:I

    .line 125
    return-void
.end method

.method public setTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V
    .locals 0
    .param p1, "type"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItem;->mTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 141
    return-void
.end method
