.class final Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$1;
.super Ljava/lang/Object;
.source "MusicItemBuilder.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final collator:Ljava/text/Collator;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$1;->collator:Ljava/text/Collator;

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;)I
    .locals 3
    .param p1, "object1"    # Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;
    .param p2, "object2"    # Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$1;->collator:Ljava/text/Collator;

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 107
    check-cast p1, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$1;->compare(Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;)I

    move-result v0

    return v0
.end method
