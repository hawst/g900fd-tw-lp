.class public Lcom/sec/samsung/gallery/controller/CpuBoostCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "CpuBoostCmd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;
    }
.end annotation


# static fields
.field private static final CPUBOOST_DEMAND_DURATION_200MS:I = 0xc8

.field private static final CPUBOOST_DEMAND_DURATION_600MS:I = 0x258

.field private static final CPUBOOST_DEMAND_DURATION_MS:I = 0x1f4

.field private static final CPUBOOST_DEMAND_FREQUENCY_KHZ:I = 0x16e360

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBusBooster:Landroid/os/DVFSHelper;

.field private mCpuBooster:Landroid/os/DVFSHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 25
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    .line 26
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mBusBooster:Landroid/os/DVFSHelper;

    return-void
.end method

.method private boostCpu()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUFrequency()[I

    move-result-object v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    const-string v1, "Gallery_touch"

    invoke-virtual {v0, v1}, Landroid/os/DVFSHelper;->acquire(Ljava/lang/String;)V

    .line 64
    sget-object v0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->TAG:Ljava/lang/String;

    const-string v1, "Boost CPU Clock"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mBusBooster:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_1

    .line 68
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mBusBooster:Landroid/os/DVFSHelper;

    const-string v1, "Gallery_touch"

    invoke-virtual {v0, v1}, Landroid/os/DVFSHelper;->acquire(Ljava/lang/String;)V

    .line 70
    :cond_1
    return-void
.end method

.method private boostCpu200ms()V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUFrequency()[I

    move-result-object v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Landroid/os/DVFSHelper;->acquire(I)V

    .line 76
    sget-object v0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->TAG:Ljava/lang/String;

    const-string v1, "Boost CPU Clock"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    :cond_0
    return-void
.end method

.method private boostCpu600ms()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUFrequency()[I

    move-result-object v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    const/16 v1, 0x258

    invoke-virtual {v0, v1}, Landroid/os/DVFSHelper;->acquire(I)V

    .line 85
    sget-object v0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->TAG:Ljava/lang/String;

    const-string v1, "Boost CPU Clock"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :cond_0
    return-void
.end method

.method private boostCpuCancel()V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUFrequency()[I

    move-result-object v0

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    const-string v1, "Gallery_touch_tail"

    invoke-virtual {v0, v1}, Landroid/os/DVFSHelper;->acquire(Ljava/lang/String;)V

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mBusBooster:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mBusBooster:Landroid/os/DVFSHelper;

    const-string v1, "Gallery_touch_tail"

    invoke-virtual {v0, v1}, Landroid/os/DVFSHelper;->acquire(Ljava/lang/String;)V

    .line 99
    :cond_1
    return-void
.end method

.method private boostRelease()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mBusBooster:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mBusBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    .line 108
    :cond_1
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v4, 0x0

    .line 50
    new-instance v0, Landroid/os/DVFSHelper;

    const-string v2, "com.sec.android.gallery3d"

    const/16 v3, 0xc

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    .line 51
    new-instance v0, Landroid/os/DVFSHelper;

    const-string v2, "com.sec.android.gallery3d"

    const/16 v3, 0x13

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mBusBooster:Landroid/os/DVFSHelper;

    .line 53
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    const-string v1, "Gallery_touch"

    invoke-virtual {v0, v1}, Landroid/os/DVFSHelper;->addExtraOptionsByDefaultPolicy(Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->mBusBooster:Landroid/os/DVFSHelper;

    const-string v1, "Gallery_touch"

    invoke-virtual {v0, v1}, Landroid/os/DVFSHelper;->addExtraOptionsByDefaultPolicy(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :goto_0
    return-void

    .line 55
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 4
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 30
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Object;

    move-object v2, v3

    check-cast v2, [Ljava/lang/Object;

    .line 31
    .local v2, "params":[Ljava/lang/Object;
    const/4 v3, 0x0

    aget-object v1, v2, v3

    check-cast v1, Landroid/content/Context;

    .line 32
    .local v1, "context":Landroid/content/Context;
    const/4 v3, 0x1

    aget-object v0, v2, v3

    check-cast v0, Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;

    .line 34
    .local v0, "cmdType":Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;
    sget-object v3, Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;->INITIALIZE:Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;

    if-ne v0, v3, :cond_1

    .line 35
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->init(Landroid/content/Context;)V

    .line 47
    :cond_0
    :goto_0
    return-void

    .line 36
    :cond_1
    sget-object v3, Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;->BOOST_CPU:Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;

    if-ne v0, v3, :cond_2

    .line 37
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->boostCpu()V

    goto :goto_0

    .line 38
    :cond_2
    sget-object v3, Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;->BOOST_CPU_200MS:Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;

    if-ne v0, v3, :cond_3

    .line 39
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->boostCpu200ms()V

    goto :goto_0

    .line 40
    :cond_3
    sget-object v3, Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;->BOOST_CPU_600MS:Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;

    if-ne v0, v3, :cond_4

    .line 41
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->boostCpu600ms()V

    goto :goto_0

    .line 42
    :cond_4
    sget-object v3, Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;->BOOST_CPU_CANCEL:Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;

    if-ne v0, v3, :cond_5

    .line 43
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->boostCpuCancel()V

    goto :goto_0

    .line 44
    :cond_5
    sget-object v3, Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;->BOOST_RELEASE:Lcom/sec/samsung/gallery/controller/CpuBoostCmd$CpuBoostCmdType;

    if-ne v0, v3, :cond_0

    .line 45
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;->boostRelease()V

    goto :goto_0
.end method
