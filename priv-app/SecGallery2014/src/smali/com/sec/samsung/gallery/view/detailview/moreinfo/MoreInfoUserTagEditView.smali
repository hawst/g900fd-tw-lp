.class public Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;
.super Ljava/lang/Object;
.source "MoreInfoUserTagEditView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;,
        Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$Result;
    }
.end annotation


# static fields
.field private static final LIST_TEXT_COLOR:I = -0xa0a0b

.field private static final MSG_UPDATE_ACTIONBAR:I


# instance fields
.field private mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

.field private mComposeAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;

.field private mContext:Landroid/content/Context;

.field private mDeleteToast:Landroid/widget/Toast;

.field private mHandler:Landroid/os/Handler;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mOriginalUserTag:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mResult:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$Result;

.field private mRootView:Landroid/view/View;

.field private mTagListView:Landroid/widget/ListView;

.field private mUri:Ljava/lang/String;

.field private mUserTagList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Ljava/util/ArrayList;Ljava/lang/String;Lcom/sec/samsung/gallery/view/ActionBarManager;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rootView"    # Landroid/view/View;
    .param p4, "uri"    # Ljava/lang/String;
    .param p5, "manager"    # Lcom/sec/samsung/gallery/view/ActionBarManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/View;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/sec/samsung/gallery/view/ActionBarManager;",
            ")V"
        }
    .end annotation

    .prologue
    .line 74
    .local p3, "userTagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mUserTagList:Ljava/util/ArrayList;

    .line 40
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mOriginalUserTag:Ljava/util/ArrayList;

    .line 55
    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$1;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mHandler:Landroid/os/Handler;

    .line 75
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mContext:Landroid/content/Context;

    .line 76
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mRootView:Landroid/view/View;

    .line 77
    iput-object p4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mUri:Ljava/lang/String;

    .line 78
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mUserTagList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 79
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mUserTagList:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 80
    iput-object p5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 81
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v1, :cond_0

    .line 82
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mContext:Landroid/content/Context;

    const v2, 0x7f110039

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 83
    .local v0, "ct":Landroid/view/ContextThemeWrapper;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mInflater:Landroid/view/LayoutInflater;

    .line 87
    .end local v0    # "ct":Landroid/view/ContextThemeWrapper;
    :goto_0
    invoke-direct {p0, p1, p4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->initOriginalUserTag(Landroid/content/Context;Ljava/lang/String;)V

    .line 88
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->initView()V

    .line 89
    return-void

    .line 85
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mInflater:Landroid/view/LayoutInflater;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mComposeAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mUserTagList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mUri:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Landroid/view/LayoutInflater;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mOriginalUserTag:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mDeleteToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mDeleteToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private initOriginalUserTag(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    .line 96
    invoke-static {p1, p2}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getUserTag(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 97
    .local v4, "tagString":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 98
    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 99
    .local v5, "tags":[Ljava/lang/String;
    move-object v0, v5

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 100
    .local v3, "tag":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_0

    .line 101
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mOriginalUserTag:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 104
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "tag":Ljava/lang/String;
    .end local v5    # "tags":[Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private initView()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    const/4 v10, -0x1

    .line 107
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mRootView:Landroid/view/View;

    const v9, 0x7f0f0195

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    .line 108
    .local v5, "subContainer":Landroid/widget/RelativeLayout;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getAllUserTag(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 109
    const v8, 0x7f0f01b5

    invoke-virtual {v5, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 110
    .local v1, "noItemsView":Landroid/view/View;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mInflater:Landroid/view/LayoutInflater;

    const v9, 0x7f03009f

    invoke-virtual {v8, v9, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 111
    new-instance v8, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v8, v10, v10}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 113
    invoke-virtual {v5, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 114
    invoke-direct {p0, v11}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->setVisibility(Z)V

    .line 115
    const v8, 0x7f0f01b7

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 117
    .local v0, "mNoItemBg":Landroid/widget/ImageView;
    const/16 v8, 0x8

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 159
    .end local v0    # "mNoItemBg":Landroid/widget/ImageView;
    .end local v1    # "noItemsView":Landroid/view/View;
    :goto_0
    return-void

    .line 119
    :cond_0
    const v8, 0x7f0f01a8

    invoke-virtual {v5, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 120
    .local v7, "userTagEditView":Landroid/view/View;
    if-nez v7, :cond_1

    .line 121
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mInflater:Landroid/view/LayoutInflater;

    const v9, 0x7f03009c

    invoke-virtual {v8, v9, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 122
    new-instance v8, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v8, v10, v10}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v7, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 124
    invoke-virtual {v5, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 127
    :cond_1
    new-instance v8, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mUserTagList:Ljava/util/ArrayList;

    invoke-direct {v8, p0, v9, v10}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mComposeAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;

    .line 128
    const v8, 0x7f0f01a9

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ListView;

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mTagListView:Landroid/widget/ListView;

    .line 130
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mTagListView:Landroid/widget/ListView;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mComposeAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;

    invoke-virtual {v8, v9}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 131
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mTagListView:Landroid/widget/ListView;

    new-instance v9, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$2;

    invoke-direct {v9, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$2;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)V

    invoke-virtual {v8, v9}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 149
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 150
    .local v4, "resources":Landroid/content/res/Resources;
    const v8, 0x7f0d009f

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 152
    .local v6, "tagPaddingTop":I
    const v8, 0x7f0d007f

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 153
    .local v2, "paddingLeft":I
    const v8, 0x7f0d007e

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 154
    .local v3, "paddingRight":I
    const/4 v8, 0x0

    invoke-virtual {v7, v2, v6, v3, v8}, Landroid/view/View;->setPadding(IIII)V

    .line 156
    invoke-direct {p0, v11}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->setVisibility(Z)V

    .line 157
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->updateActionBar()V

    goto :goto_0
.end method

.method private setVisibility(Z)V
    .locals 8
    .param p1, "userTagEdit"    # Z

    .prologue
    const/16 v5, 0x8

    const/4 v6, 0x0

    .line 181
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mRootView:Landroid/view/View;

    const v7, 0x7f0f0195

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 182
    .local v2, "subContainer":Landroid/widget/RelativeLayout;
    const v4, 0x7f0f0196

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 183
    .local v1, "scrollView":Landroid/view/View;
    if-eqz p1, :cond_0

    move v4, v5

    :goto_0
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 184
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getAllUserTag(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 185
    const v4, 0x7f0f01b5

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 186
    .local v0, "noItemsView":Landroid/view/View;
    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 191
    .end local v0    # "noItemsView":Landroid/view/View;
    :goto_2
    return-void

    :cond_0
    move v4, v6

    .line 183
    goto :goto_0

    .restart local v0    # "noItemsView":Landroid/view/View;
    :cond_1
    move v6, v5

    .line 186
    goto :goto_1

    .line 188
    .end local v0    # "noItemsView":Landroid/view/View;
    :cond_2
    const v4, 0x7f0f01a8

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 189
    .local v3, "userTagEditView":Landroid/view/View;
    if-eqz p1, :cond_3

    :goto_3
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_3
    move v6, v5

    goto :goto_3
.end method


# virtual methods
.method public editActionCancel()V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mResult:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$Result;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mResult:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$Result;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$Result;->cancel()V

    .line 177
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->setVisibility(Z)V

    .line 178
    return-void
.end method

.method public editActionDone()V
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mResult:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$Result;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mResult:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$Result;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mComposeAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->getData()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$Result;->done(Ljava/util/ArrayList;)V

    .line 170
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->setVisibility(Z)V

    .line 171
    return-void
.end method

.method public registerResultCallBack(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$Result;)V
    .locals 0
    .param p1, "result"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$Result;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mResult:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$Result;

    .line 93
    return-void
.end method

.method public updateActionBar()V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getAllUserTag(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 164
    :cond_0
    return-void
.end method
