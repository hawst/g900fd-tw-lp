.class Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview$1;
.super Ljava/lang/Object;
.source "DetailActionBarForMotionPreview.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->setTitle()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 91
    const-string v1, ""

    .line 92
    .local v1, "title":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryMotion()Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v0

    .line 93
    .local v0, "motionType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v0, v2, :cond_1

    .line 94
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->access$100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e00df

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 100
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;)Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 108
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 109
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->access$600(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;)Landroid/app/ActionBar;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->access$500(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0200e6

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 111
    :cond_0
    return-void

    .line 95
    :cond_1
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v0, v2, :cond_2

    .line 96
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->access$200(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e00de

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 98
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->access$300(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e00e0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
