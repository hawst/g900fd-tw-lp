.class Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$2;
.super Ljava/lang/Object;
.source "SlideShowSettingView.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->setupSpeedView(Landroid/content/Context;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/sec/android/touchwiz/widget/TwAdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/touchwiz/widget/TwAdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 129
    .local p1, "parent":Lcom/sec/android/touchwiz/widget/TwAdapterView;, "Lcom/sec/android/touchwiz/widget/TwAdapterView<*>;"
    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$000()Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->setSpeed(I)V

    .line 130
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$100(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)Landroid/content/Context;

    move-result-object v0

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$000()Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->save(Landroid/content/Context;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;)V

    .line 131
    return-void
.end method
