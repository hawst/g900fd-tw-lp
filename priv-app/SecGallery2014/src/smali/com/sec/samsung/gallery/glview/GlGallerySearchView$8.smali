.class Lcom/sec/samsung/gallery/glview/GlGallerySearchView$8;
.super Ljava/lang/Object;
.source "GlGallerySearchView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGallerySearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V
    .locals 0

    .prologue
    .line 503
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 5
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 508
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mOnItemClickListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListenerEx;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDeletingIndex:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$700(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)I

    move-result v3

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListenerEx;->onItemClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;II)V

    .line 509
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->loadItems()V

    .line 510
    return-void
.end method

.method public onAnimationRepeat(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 505
    return-void
.end method

.method public onAnimationStart(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 504
    return-void
.end method
