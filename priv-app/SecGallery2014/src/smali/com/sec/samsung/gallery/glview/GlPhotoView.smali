.class public Lcom/sec/samsung/gallery/glview/GlPhotoView;
.super Lcom/sec/samsung/gallery/glview/GlAbsListView;
.source "GlPhotoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/GlPhotoView$OnChangeThumbSizeListener;,
        Lcom/sec/samsung/gallery/glview/GlPhotoView$GlPhotoViewEventListener;,
        Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;,
        Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;,
        Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;,
        Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFixedAlbumSeparatorView;,
        Lcom/sec/samsung/gallery/glview/GlPhotoView$OnGetViewListener;,
        Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;,
        Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;,
        Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;,
        Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;,
        Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;,
        Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;,
        Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;
    }
.end annotation


# static fields
.field private static ALBUM_ITEM_MAX:I = 0x0

.field private static ALBUM_YAW:F = 0.0f

.field static ANIM_PRECOND_ALBUM:I = 0x0

.field static ANIM_PRECOND_THUMB:I = 0x0

.field private static ANIM_STATUS_IDLE:I = 0x0

.field private static ANIM_STATUS_RUN:I = 0x0

.field private static ANIM_STATUS_WAIT:I = 0x0

.field static ANIM_TYPE_ALBUM:I = 0x0

.field static ANIM_TYPE_NONE:I = 0x0

.field static ANIM_TYPE_TIME:I = 0x0

.field static ANIM_TYPE_VIEW:I = 0x0

.field static ARROW_VISIBLE_EXPAND:I = 0x0

.field static ARROW_VISIBLE_RANGE:I = 0x0

.field public static CLICK_ALBUM_SET:I = 0x0

.field static CMD_APPLY_POS:I = 0x0

.field static CMD_CONV_ALBUM:I = 0x0

.field static CMD_NOTIFY_FOLDER_UPDATE:I = 0x0

.field static CMD_REQ_LOAD_FOLDER:I = 0x0

.field static CMD_SWITCH_ALBUM:I = 0x0

.field static CMD_UPDATE_FOLDER:I = 0x0

.field private static FIXEDABLUM_VISIABLE_ADJUST_VALUE:F = 0.0f

.field private static final FOLDER_CHANGE_SPREAD:I = 0x2

.field private static final FOLDER_CHANGE_TURN:I = 0x1

.field public static final MODE_NORMAL:I = 0x0

.field static SPLIT_RATIO_LAND:F = 0.0f

.field static SPLIT_RATIO_PORT:F = 0.0f

.field public static final STATUS_ALBUM_UPDATED:I = 0x3

.field public static final STATUS_COLUMN_PORT:I = 0x2

.field public static final STATUS_COLUMN_WIDE:I = 0x1

.field public static final STATUS_ITEM_ADD_TO_NEW_ALBUM:I = 0x5

.field public static final STATUS_ITEM_MOVE:I = 0x4

.field private static final TAG:Ljava/lang/String; = "GlPhotoView"

.field private static THUMB_DRAG_MAX:I = 0x0

.field private static THUMB_ITEM_MAX:I = 0x0

.field public static final VIEW_MODE_EXPAND_H12_V8:I = 0x5

.field public static final VIEW_MODE_SPLIT_H4_V2:I = 0x1

.field private static mFilteredItems:Z

.field private static misFilteredForStoryAlbum:Z

.field private static misFilteredForVideoAlbum:Z


# instance fields
.field private final DEFAULT_ANIMATION_DURATION:J

.field private final SPLIT_THUMB_TYPE_COUNT:I

.field private final VIEW_MODE_COUNT:I

.field private final VIEW_MODE_EXPAND_H3_V2:I

.field private final VIEW_MODE_EXPAND_H4_V3:I

.field private final VIEW_MODE_EXPAND_H6_V4:I

.field private final VIEW_MODE_SPLIT_H2_V1:I

.field private mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

.field private mAlbumDropEnabled:Z

.field public mAlbumFocused:I

.field private mAnimCondDone:I

.field private mAnimCondNeed:I

.field private mBgExpandAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;

.field private mChangeThumbSizeListner:Lcom/sec/samsung/gallery/glview/GlPhotoView$OnChangeThumbSizeListener;

.field private mClipEnable:Z

.field private mClipRect:Landroid/graphics/Rect;

.field private mClipRectForAlbums:Landroid/graphics/Rect;

.field public mColCnt:I

.field mConvProgressListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$GlAnimationProgressListener;

.field private mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

.field private mConvSetListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

.field private mConvSetListenerThm:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

.field public mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

.field private mDragAnim:Lcom/sec/samsung/gallery/glview/GlDragAnimation;

.field private mDragInfo:Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;

.field private mEventListener:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlPhotoViewEventListener;

.field private mExpandViewMode:I

.field private mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;

.field private mFixedAlbumH:F

.field private mFixedAlbumHpx:I

.field public mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

.field private mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

.field mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

.field public mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlSplitObject;

.field public mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

.field private mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

.field private mFolderChangeAnimType:I

.field private mFolderChgAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;

.field private mFolderChgAnimCurrent:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

.field mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

.field public mFolderObjecSel:Lcom/sec/samsung/gallery/glview/GlSplitObject;

.field private mFolderSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

.field private mFolderSpreadAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;

.field private mFromTimeView:Z

.field private mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

.field private mGlConfig:Lcom/sec/android/gallery3d/glcore/GlConfig;

.field mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

.field private mInitAnimListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

.field private mIsCurrentExpandable:Z

.field private mIsDragEnabled:Z

.field private mIsEasyMode:Z

.field private mIsExpandable:Z

.field public mIsExpanded:Z

.field mIsFixedAlbumAtTop:Z

.field mIsFixedAlbumVisible:Z

.field private mIsNoSplitMode:Z

.field private mIsPressed:Z

.field private mIsResizing:Z

.field public mItemFocused:I

.field public mKeyboardRightFocused:Z

.field private mLClickable:Z

.field private mListenerAlbumClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

.field private mListenerAlbumMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

.field private mListenerDrop:Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;

.field private mListenerHover:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

.field private mListenerItemClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

.field private mListenerItemLongClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

.field private mListenerItemMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

.field public mLockTimeAnim:Z

.field public mMaintainScroll:Z

.field public mNewAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

.field private mNewAlbumMode:Z

.field private mObjEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

.field private mObjEstimatorThm:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

.field private mObjEstimatorThm2:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

.field private mOnConvAnim:Z

.field mOnGetViewListener:Lcom/sec/samsung/gallery/glview/GlPhotoView$OnGetViewListener;

.field private mPitchFact:F

.field private mPressedLeft:Z

.field private mPressedUp:Z

.field private mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

.field private mRClickable:Z

.field public mRowCnt:I

.field public mSPKeyString:Ljava/lang/String;

.field private mSeparatorY:F

.field private mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

.field private mSplitSx:F

.field mStartDragListener:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field private mSx:F

.field public mThisListView:Lcom/sec/samsung/gallery/glview/GlAbsListView;

.field public mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

.field mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

.field mThumbMoveTo:Lcom/sec/android/gallery3d/glcore/GlObject;

.field mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

.field private mViewMode:I

.field private mWAvailRatio:F

.field private mlistenerFolders:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

.field private mlistenerItems:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

.field private newAlbumHeaderHeight:F


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x1b

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 73
    sput v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CLICK_ALBUM_SET:I

    .line 90
    const/16 v0, 0x64

    sput v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_NOTIFY_FOLDER_UPDATE:I

    .line 91
    const/16 v0, 0x65

    sput v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_UPDATE_FOLDER:I

    .line 92
    const/16 v0, 0x66

    sput v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_REQ_LOAD_FOLDER:I

    .line 93
    const/16 v0, 0x67

    sput v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_SWITCH_ALBUM:I

    .line 94
    const/16 v0, 0x68

    sput v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_CONV_ALBUM:I

    .line 95
    const/16 v0, 0x69

    sput v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_APPLY_POS:I

    .line 103
    const/4 v0, 0x0

    sput v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ALBUM_YAW:F

    .line 105
    const/16 v0, 0xa

    sput v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ALBUM_ITEM_MAX:I

    .line 106
    sput v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->THUMB_ITEM_MAX:I

    .line 107
    sput v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->THUMB_DRAG_MAX:I

    .line 109
    sput v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_TYPE_NONE:I

    .line 110
    sput v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_TYPE_VIEW:I

    .line 111
    sput v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_TYPE_ALBUM:I

    .line 112
    const/4 v0, 0x3

    sput v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_TYPE_TIME:I

    .line 114
    sput v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_STATUS_IDLE:I

    .line 115
    sput v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_STATUS_WAIT:I

    .line 116
    sput v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_STATUS_RUN:I

    .line 118
    sput v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_PRECOND_ALBUM:I

    .line 119
    sput v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_PRECOND_THUMB:I

    .line 121
    sput v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ARROW_VISIBLE_RANGE:I

    .line 122
    sput v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ARROW_VISIBLE_EXPAND:I

    .line 126
    const/high16 v0, 0x40400000    # 3.0f

    sput v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->FIXEDABLUM_VISIABLE_ADJUST_VALUE:F

    .line 219
    sput-boolean v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFilteredItems:Z

    .line 220
    sput-boolean v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->misFilteredForVideoAlbum:Z

    .line 221
    sput-boolean v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->misFilteredForStoryAlbum:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IIFF)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "setPos"    # I
    .param p3, "itemPos"    # I
    .param p4, "splitScroll"    # F
    .param p5, "thumbScroll"    # F

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 228
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;-><init>(Landroid/content/Context;)V

    .line 81
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->VIEW_MODE_SPLIT_H2_V1:I

    .line 83
    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->VIEW_MODE_EXPAND_H3_V2:I

    .line 84
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->VIEW_MODE_EXPAND_H4_V3:I

    .line 85
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->VIEW_MODE_EXPAND_H6_V4:I

    .line 87
    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SPLIT_THUMB_TYPE_COUNT:I

    .line 88
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->VIEW_MODE_COUNT:I

    .line 99
    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderChangeAnimType:I

    .line 129
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .line 130
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .line 131
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mNewAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .line 132
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    .line 139
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    .line 140
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDragAnim:Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    .line 141
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;

    .line 143
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimCondNeed:I

    .line 144
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimCondDone:I

    .line 145
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    .line 146
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mRowCnt:I

    .line 147
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnConvAnim:Z

    .line 149
    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSx:F

    .line 150
    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSplitSx:F

    .line 151
    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPitchFact:F

    .line 152
    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSeparatorY:F

    .line 153
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mLClickable:Z

    .line 154
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mRClickable:Z

    .line 156
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .line 157
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    .line 158
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mItemFocused:I

    .line 162
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    .line 164
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    .line 165
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    .line 168
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 169
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 170
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbMoveTo:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 171
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDragInfo:Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;

    .line 173
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    .line 174
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipEnable:Z

    .line 176
    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->newAlbumHeaderHeight:F

    .line 177
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumHpx:I

    .line 178
    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumH:F

    .line 180
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRectForAlbums:Landroid/graphics/Rect;

    .line 181
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsFixedAlbumVisible:Z

    .line 182
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsFixedAlbumAtTop:Z

    .line 185
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnGetViewListener:Lcom/sec/samsung/gallery/glview/GlPhotoView$OnGetViewListener;

    .line 186
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mStartDragListener:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    .line 189
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 193
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mNewAlbumMode:Z

    .line 196
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumDropEnabled:Z

    .line 197
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsEasyMode:Z

    .line 198
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsNoSplitMode:Z

    .line 200
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpandable:Z

    .line 201
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsCurrentExpandable:Z

    .line 202
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsDragEnabled:Z

    .line 203
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWAvailRatio:F

    .line 209
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mExpandViewMode:I

    .line 210
    const-string/jumbo v0, "thumbnailViewMode"

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSPKeyString:Ljava/lang/String;

    .line 214
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsPressed:Z

    .line 215
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mMaintainScroll:Z

    .line 222
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mLockTimeAnim:Z

    .line 224
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFromTimeView:Z

    .line 225
    const-wide/16 v4, 0x1f4

    iput-wide v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->DEFAULT_ANIMATION_DURATION:J

    .line 767
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$2;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvProgressListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$GlAnimationProgressListener;

    .line 794
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$3;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mObjEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    .line 808
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$4;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

    .line 894
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$5;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetListenerThm:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

    .line 905
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$6;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$6;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mObjEstimatorThm:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    .line 919
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$7;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$7;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mObjEstimatorThm2:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    .line 1046
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$8;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$8;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mInitAnimListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 1877
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$11;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mlistenerItems:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

    .line 1911
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$12;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$12;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mlistenerFolders:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

    .line 1941
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerItemMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    .line 1988
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerItemClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 2042
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerItemLongClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

    .line 2100
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$16;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$16;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerDrop:Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;

    .line 2133
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$17;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$17;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerHover:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

    .line 2162
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$18;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$18;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerAlbumMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    .line 2189
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$19;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$19;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerAlbumClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 230
    iput-object p0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThisListView:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    .line 231
    iput p4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSplitSx:F

    .line 232
    iput p5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSx:F

    .line 233
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    .line 234
    iput p3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mItemFocused:I

    .line 236
    const v0, 0x7f0e0017

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getResource(I)F

    move-result v0

    sput v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SPLIT_RATIO_PORT:F

    .line 237
    const v0, 0x7f0e0018

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getResource(I)F

    move-result v0

    sput v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SPLIT_RATIO_LAND:F

    .line 238
    const/16 v0, 0x80

    sput v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->THUMB_ITEM_MAX:I

    .line 239
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsEasyMode:Z

    .line 240
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$1;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerGenericMotionEx:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    .line 258
    return-void

    :cond_1
    move v0, v1

    .line 239
    goto :goto_0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;)Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;)Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/glview/GlPhotoView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSx:F

    return v0
.end method

.method static synthetic access$1302(Lcom/sec/samsung/gallery/glview/GlPhotoView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .param p1, "x1"    # F

    .prologue
    .line 70
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSx:F

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/glview/GlPhotoView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPitchFact:F

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/samsung/gallery/glview/GlPhotoView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .param p1, "x1"    # F

    .prologue
    .line 70
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPitchFact:F

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/glview/GlPhotoView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSplitSx:F

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/samsung/gallery/glview/GlPhotoView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .param p1, "x1"    # F

    .prologue
    .line 70
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSplitSx:F

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mRClickable:Z

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/view/utils/SoundUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    return-object v0
.end method

.method static synthetic access$1800()Z
    .locals 1

    .prologue
    .line 70
    sget-boolean v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFilteredItems:Z

    return v0
.end method

.method static synthetic access$1900()Z
    .locals 1

    .prologue
    .line 70
    sget-boolean v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->misFilteredForVideoAlbum:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    return-object v0
.end method

.method static synthetic access$2000()Z
    .locals 1

    .prologue
    .line 70
    sget-boolean v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->misFilteredForStoryAlbum:Z

    return v0
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/android/gallery3d/app/StateManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsDragEnabled:Z

    return v0
.end method

.method static synthetic access$2300(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mNewAlbumMode:Z

    return v0
.end method

.method static synthetic access$2400(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/android/gallery3d/glcore/GlObject;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 70
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->startDragAnim(Lcom/sec/android/gallery3d/glcore/GlObject;II)V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerAlbumClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mLClickable:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/sec/samsung/gallery/glview/GlPhotoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mLClickable:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/sec/samsung/gallery/glview/GlPhotoView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSeparatorY:F

    return v0
.end method

.method static synthetic access$2802(Lcom/sec/samsung/gallery/glview/GlPhotoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPressedUp:Z

    return p1
.end method

.method static synthetic access$2902(Lcom/sec/samsung/gallery/glview/GlPhotoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPressedLeft:Z

    return p1
.end method

.method static synthetic access$3000(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$GlPhotoViewEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mEventListener:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlPhotoViewEventListener;

    return-object v0
.end method

.method static synthetic access$3100()I
    .locals 1

    .prologue
    .line 70
    sget v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_STATUS_IDLE:I

    return v0
.end method

.method static synthetic access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    return v0
.end method

.method static synthetic access$3300(Lcom/sec/samsung/gallery/glview/GlPhotoView;I)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .param p1, "x1"    # I

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getResource(I)F

    move-result v0

    return v0
.end method

.method static synthetic access$3400(Lcom/sec/samsung/gallery/glview/GlPhotoView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWAvailRatio:F

    return v0
.end method

.method static synthetic access$3500()I
    .locals 1

    .prologue
    .line 70
    sget v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->THUMB_ITEM_MAX:I

    return v0
.end method

.method static synthetic access$3600(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/samsung/gallery/glview/GlPhotoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnConvAnim:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;)Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->startExpandAnimation()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/glview/GlPhotoView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .param p1, "x1"    # I

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->checkStartAnimation(I)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsResizing:Z

    return v0
.end method

.method static synthetic access$802(Lcom/sec/samsung/gallery/glview/GlPhotoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsResizing:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->clearBoundarEffect()V

    return-void
.end method

.method private changeFolder()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 2776
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    if-eqz v0, :cond_0

    .line 2777
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    .line 2778
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderChangeAnimType:I

    if-ne v0, v2, :cond_2

    .line 2779
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderChgAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderChgAnimCurrent:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 2780
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderChgAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->startChange()V

    .line 2809
    :cond_1
    :goto_0
    return-void

    .line 2781
    :cond_2
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderChangeAnimType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 2782
    const-string v0, "GlPhotoView"

    const-string v1, "changeFolder, starting fold change animation"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2783
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iput-boolean v2, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mInitVisible:Z

    .line 2784
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderSpreadAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderChgAnimCurrent:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 2785
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnConvAnim:Z

    .line 2786
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderSpreadAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderObjecSel:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->startAnimation(Lcom/sec/android/gallery3d/glcore/GlRootView;[Lcom/sec/samsung/gallery/glview/GlThumbObject;Lcom/sec/samsung/gallery/glview/GlScroller;Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 2787
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderSpreadAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;

    new-instance v1, Lcom/sec/samsung/gallery/glview/GlPhotoView$20;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$20;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    goto :goto_0
.end method

.method private checkStartAnimation(I)V
    .locals 3
    .param p1, "readyOpt"    # I

    .prologue
    const/4 v2, 0x0

    .line 977
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimStatus:I

    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_STATUS_WAIT:I

    if-eq v0, v1, :cond_1

    .line 990
    :cond_0
    :goto_0
    return-void

    .line 980
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimCondDone:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimCondDone:I

    .line 981
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimCondNeed:I

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimCondDone:I

    and-int/2addr v0, v1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimCondNeed:I

    if-ne v0, v1, :cond_0

    .line 985
    sget v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_STATUS_RUN:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimStatus:I

    .line 986
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimType:I

    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_TYPE_ALBUM:I

    if-ne v0, v1, :cond_2

    .line 987
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->changeFolder()V

    goto :goto_0

    .line 988
    :cond_2
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimType:I

    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_TYPE_VIEW:I

    if-ne v0, v1, :cond_0

    .line 989
    sget v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_CONV_ALBUM:I

    invoke-virtual {p0, v0, v2, v2, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    goto :goto_0
.end method

.method private clearBoundarEffect()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3612
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPitchFact:F

    .line 3613
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    if-eqz v0, :cond_0

    .line 3614
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->setAlbumPitchRate(F)V

    .line 3615
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    if-eqz v0, :cond_1

    .line 3616
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->resetPitch()V

    .line 3617
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->setThumbnailPitchRate(F)V

    .line 3619
    :cond_1
    return-void
.end method

.method private controlByDrag(Z)Z
    .locals 12
    .param p1, "active"    # Z

    .prologue
    .line 1421
    const/4 v4, 0x0

    .line 1422
    .local v4, "moveToFolder":Z
    if-nez p1, :cond_2

    .line 1423
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMoveToward(F)V

    .line 1424
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v8, :cond_1

    .line 1425
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    if-eqz v8, :cond_0

    .line 1427
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v10, 0x4

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iget v11, v11, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mIndex:I

    invoke-interface {v8, v9, v10, v11}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;->onStatusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    .line 1430
    :cond_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iput-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbMoveTo:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 1431
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setFocused(Z)V

    .line 1432
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(Z)V

    .line 1433
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .line 1434
    const/4 v4, 0x1

    .line 1436
    :cond_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDragInfo:Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;

    const/4 v9, 0x0

    iput-boolean v9, v8, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->mActive:Z

    move v8, v4

    .line 1499
    :goto_0
    return v8

    .line 1439
    :cond_2
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mNewAlbumMode:Z

    if-eqz v8, :cond_3

    .line 1440
    const/4 v8, 0x0

    goto :goto_0

    .line 1442
    :cond_3
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getX()F

    move-result v6

    .line 1443
    .local v6, "x":F
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getY()F

    move-result v7

    .line 1444
    .local v7, "y":F
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDragInfo:Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;

    invoke-virtual {v8, v6, v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->getIntensity(FF)F

    move-result v1

    .line 1446
    .local v1, "intensity":F
    const/4 v8, 0x0

    cmpl-float v8, v1, v8

    if-eqz v8, :cond_5

    const/4 v8, 0x0

    cmpl-float v8, v1, v8

    if-lez v8, :cond_4

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScroll:F

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-eqz v8, :cond_5

    :cond_4
    const/4 v8, 0x0

    cmpg-float v8, v1, v8

    if-gez v8, :cond_c

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScroll:F

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScrollRange:F

    cmpl-float v8, v8, v9

    if-nez v8, :cond_c

    .line 1448
    :cond_5
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDragInfo:Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;

    iget-boolean v8, v8, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->mActive:Z

    if-eqz v8, :cond_7

    .line 1449
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMoveToward(F)V

    .line 1450
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDragInfo:Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;

    const/4 v9, 0x0

    iput-boolean v9, v8, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->mActive:Z

    .line 1499
    :cond_6
    :goto_1
    const/4 v8, 0x0

    goto :goto_0

    .line 1452
    :cond_7
    invoke-direct {p0, v6, v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->findFocusAlbum(FF)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .line 1453
    .local v0, "focusedObj":Lcom/sec/samsung/gallery/glview/GlSplitObject;
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eq v0, v8, :cond_6

    .line 1454
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v8, :cond_8

    .line 1455
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setFocused(Z)V

    .line 1456
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(Z)V

    .line 1458
    :cond_8
    if-eqz v0, :cond_b

    .line 1459
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderObjecSel:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v0, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_9

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v0, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 1460
    :cond_9
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(Z)V

    .line 1462
    :cond_a
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setFocused(Z)V

    .line 1463
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 1464
    iget v2, v0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mIndex:I

    .line 1465
    .local v2, "itemIndex":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    check-cast v8, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    invoke-virtual {v8, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 1466
    .local v3, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v5, 0x0

    .line 1467
    .local v5, "name":Ljava/lang/String;
    if-eqz v3, :cond_b

    .line 1468
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v5

    .line 1469
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v8

    const v9, 0x7f0e0296

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-virtual {v8, v9, v10}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 1474
    .end local v2    # "itemIndex":I
    .end local v3    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v5    # "name":Ljava/lang/String;
    :cond_b
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    goto :goto_1

    .line 1478
    .end local v0    # "focusedObj":Lcom/sec/samsung/gallery/glview/GlSplitObject;
    :cond_c
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v8, :cond_d

    .line 1479
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setFocused(Z)V

    .line 1480
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(Z)V

    .line 1481
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .line 1483
    :cond_d
    invoke-direct {p0, v6, v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->findFocusAlbum(FF)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .line 1484
    .restart local v0    # "focusedObj":Lcom/sec/samsung/gallery/glview/GlSplitObject;
    if-eqz v0, :cond_f

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v0, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_f

    .line 1485
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v8, :cond_e

    .line 1486
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setFocused(Z)V

    .line 1487
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(Z)V

    .line 1489
    :cond_e
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(Z)V

    .line 1490
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setFocused(Z)V

    .line 1492
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .line 1494
    :cond_f
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    neg-float v9, v1

    const/high16 v10, 0x41200000    # 10.0f

    mul-float/2addr v9, v10

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMoveToward(F)V

    .line 1495
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDragInfo:Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;

    const/4 v9, 0x1

    iput-boolean v9, v8, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->mActive:Z

    .line 1496
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->showScrollBar()V

    goto/16 :goto_1
.end method

.method private convAnim()V
    .locals 15

    .prologue
    .line 719
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-nez v10, :cond_0

    .line 765
    :goto_0
    return-void

    .line 722
    :cond_0
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    array-length v7, v10

    .line 723
    .local v7, "objCount":I
    new-array v2, v7, [Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 724
    .local v2, "glObject":[Lcom/sec/android/gallery3d/glcore/GlObject;
    new-array v5, v7, [I

    .line 725
    .local v5, "indexes":[I
    const/4 v9, 0x0

    .line 726
    .local v9, "validCount":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v7, :cond_2

    .line 727
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    aget-object v3, v10, v4

    .line 728
    .local v3, "glObjectCur":Lcom/sec/samsung/gallery/glview/GlSplitObject;
    iget v10, v3, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mIndex:I

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v11, v11, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemCount:I

    if-ge v10, v11, :cond_1

    .line 729
    const/4 v10, 0x1

    invoke-virtual {v3, v10}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setVisibility(Z)V

    .line 730
    aput-object v3, v2, v9

    .line 731
    iget v10, v3, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mIndex:I

    aput v10, v5, v9

    .line 732
    add-int/lit8 v9, v9, 0x1

    .line 726
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 736
    .end local v3    # "glObjectCur":Lcom/sec/samsung/gallery/glview/GlSplitObject;
    :cond_2
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    if-eqz v10, :cond_6

    .line 737
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mObjEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    invoke-interface {v10}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;->saveState()V

    .line 738
    new-instance v10, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-direct {v10, v11}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;-><init>(Landroid/content/Context;)V

    iput-object v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    .line 739
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v11, v11, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v12, v12, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mIndexs:[I

    iget-object v13, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v13, v13, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    iget-object v14, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget v14, v14, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCount:I

    invoke-virtual {v10, v11, v12, v13, v14}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setSrc([Lcom/sec/android/gallery3d/glcore/GlObject;[ILcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;I)V

    .line 741
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mObjEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    invoke-virtual {v10, v2, v5, v11, v9}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setDst([Lcom/sec/android/gallery3d/glcore/GlObject;[ILcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;I)V

    .line 742
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

    invoke-virtual {v10, v11}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setAnimationDoneListener(Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;)V

    .line 743
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    invoke-virtual {v10}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->start()V

    .line 744
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v8, v10, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemW:F

    .line 745
    .local v8, "splitW":F
    move v1, v8

    .line 746
    .local v1, "albumW":F
    const/4 v4, 0x0

    :goto_2
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v10, v10, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    array-length v10, v10

    if-ge v4, v10, :cond_3

    .line 747
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v10, v10, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v6, v10, v4

    check-cast v6, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 748
    .local v6, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-eqz v6, :cond_5

    .line 749
    const/4 v10, 0x1

    invoke-virtual {v6, v10}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getWidth(Z)F

    move-result v1

    .line 750
    iget v10, v6, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-nez v10, :cond_5

    .line 755
    .end local v6    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    check-cast v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    .line 756
    .local v0, "adapter":Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;
    if-eqz v0, :cond_4

    .line 757
    div-float v10, v8, v1

    iput v10, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mRatio:F

    .line 759
    :cond_4
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnConvAnim:Z

    .line 760
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvProgressListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$GlAnimationProgressListener;

    iput-object v11, v10, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mProgressListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$GlAnimationProgressListener;

    .line 761
    const-string v10, "GlPhotoView"

    const-string v11, "convAnim started"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 746
    .end local v0    # "adapter":Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;
    .restart local v6    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 763
    .end local v1    # "albumW":F
    .end local v6    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .end local v8    # "splitW":F
    :cond_6
    const-string v10, "GlPhotoView"

    const-string v11, "convAnim : No PrevListInfo"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private convAnimEx()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 868
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    array-length v4, v6

    .line 869
    .local v4, "objCount":I
    new-array v0, v4, [Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 870
    .local v0, "glObject":[Lcom/sec/android/gallery3d/glcore/GlObject;
    new-array v3, v4, [I

    .line 871
    .local v3, "indexes":[I
    const/4 v5, 0x0

    .line 872
    .local v5, "validCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_0

    .line 873
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v1, v6, v2

    .line 874
    .local v1, "glObjectCur":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemVisibleCnt:I

    if-lt v2, v6, :cond_1

    .line 884
    .end local v1    # "glObjectCur":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mObjEstimatorThm2:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    invoke-interface {v6}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;->saveState()V

    .line 886
    new-instance v6, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    .line 887
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mIndexs:[I

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget v10, v10, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCount:I

    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setSrc([Lcom/sec/android/gallery3d/glcore/GlObject;[ILcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;I)V

    .line 888
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mObjEstimatorThm2:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    invoke-virtual {v6, v0, v3, v7, v5}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setDst([Lcom/sec/android/gallery3d/glcore/GlObject;[ILcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;I)V

    .line 889
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetListenerThm:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setAnimationDoneListener(Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;)V

    .line 890
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->start()V

    .line 891
    iput-boolean v11, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnConvAnim:Z

    .line 892
    return-void

    .line 876
    .restart local v1    # "glObjectCur":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_1
    iget v6, v1, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemCount:I

    if-ge v6, v7, :cond_2

    .line 877
    invoke-virtual {v1, v11}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setVisibility(Z)V

    .line 878
    aput-object v1, v0, v5

    .line 879
    iget v6, v1, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    aput v6, v3, v5

    .line 880
    add-int/lit8 v5, v5, 0x1

    .line 872
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private convAnimGrid()V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    .line 1132
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mLockTimeAnim:Z

    .line 1133
    new-instance v1, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getGlRoot()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-direct {v1, v0, v2}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/samsung/gallery/glview/GlScroller;)V

    .line 1134
    .local v1, "anim":Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;
    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->setDuration(J)V

    .line 1135
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mInitAnimListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 1136
    new-instance v0, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;-><init>()V

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->setInterpolator(Lcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    .line 1137
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScroll:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_0

    .line 1138
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v2, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScroll:F

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScroll:F

    invoke-virtual/range {v1 .. v6}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->startTranlateScrollAnimation(FFJZ)V

    .line 1141
    :goto_0
    return-void

    .line 1140
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScroll:F

    const/high16 v2, 0x42700000    # 60.0f

    sub-float v2, v0, v2

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScroll:F

    invoke-virtual/range {v1 .. v6}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->startTranlateScrollAnimation(FFJZ)V

    goto :goto_0
.end method

.method private createAlbumItems()[Lcom/sec/samsung/gallery/glview/GlSplitObject;
    .locals 11

    .prologue
    .line 542
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v1, v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumThumbnailWidthPixel(I)I

    move-result v2

    .line 543
    .local v2, "thumbWidth":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v1, v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumThumbnailHeightPixel(I)I

    move-result v3

    .line 544
    .local v3, "thumbHeight":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v1, v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxWidthPixel(I)I

    move-result v4

    .line 545
    .local v4, "textboxWidth":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v1, v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxHeightPixel(I)I

    move-result v5

    .line 547
    .local v5, "textboxHeight":I
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isJEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 551
    add-int/lit8 v5, v5, 0x1

    .line 554
    :cond_0
    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ALBUM_ITEM_MAX:I

    new-array v6, v1, [Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .line 556
    .local v6, "glAlbumSet":[Lcom/sec/samsung/gallery/glview/GlSplitObject;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ALBUM_ITEM_MAX:I

    if-ge v7, v1, :cond_3

    .line 557
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlSplitObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;IIII)V

    .line 558
    .local v0, "glAlbum":Lcom/sec/samsung/gallery/glview/GlSplitObject;
    aput-object v0, v6, v7

    .line 559
    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ALBUM_YAW:F

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setYaw(F)V

    .line 560
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setAlpha(F)V

    .line 561
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerAlbumMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 562
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerAlbumClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)V

    .line 563
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerDrop:Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setDragListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;)V

    .line 564
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerGenericMotionEx:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setGenericMotionListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;)V

    .line 565
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipEnable:Z

    if-eqz v1, :cond_1

    .line 566
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipEnable:Z

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setClippingEnabled(Z)V

    .line 567
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v8, v9, v10}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setClipRect(IIII)V

    .line 570
    :cond_1
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-eqz v1, :cond_2

    .line 571
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setVisibility(Z)V

    .line 556
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 574
    .end local v0    # "glAlbum":Lcom/sec/samsung/gallery/glview/GlSplitObject;
    :cond_3
    return-object v6
.end method

.method private findFocusAlbum(FF)Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .locals 13
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v8, 0x0

    const/4 v12, 0x1

    const/high16 v11, 0x40000000    # 2.0f

    .line 1376
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v9, :cond_0

    iget-boolean v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumDropEnabled:Z

    if-nez v9, :cond_1

    :cond_0
    move-object v4, v8

    .line 1406
    :goto_0
    return-object v4

    .line 1379
    :cond_1
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFixScrollingAlbum:Z

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v9, :cond_2

    iget-boolean v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsFixedAlbumVisible:Z

    if-eqz v9, :cond_2

    .line 1380
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v9, v12}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getWidth(Z)F

    move-result v7

    .line 1381
    .local v7, "w":F
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v9, v12}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getHeight(Z)F

    move-result v2

    .line 1382
    .local v2, "h":F
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getX()F

    move-result v9

    div-float v10, v7, v11

    sub-float v5, v9, v10

    .line 1383
    .local v5, "sx":F
    add-float v0, v5, v7

    .line 1384
    .local v0, "ex":F
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getY()F

    move-result v9

    div-float v10, v2, v11

    add-float v6, v9, v10

    .line 1385
    .local v6, "sy":F
    sub-float v1, v6, v2

    .line 1386
    .local v1, "ey":F
    cmpg-float v9, v5, p1

    if-gtz v9, :cond_2

    cmpg-float v9, p1, v0

    if-gtz v9, :cond_2

    cmpl-float v9, v6, p2

    if-ltz v9, :cond_2

    cmpl-float v9, p2, v1

    if-ltz v9, :cond_2

    .line 1387
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    goto :goto_0

    .line 1390
    .end local v0    # "ex":F
    .end local v1    # "ey":F
    .end local v2    # "h":F
    .end local v5    # "sx":F
    .end local v6    # "sy":F
    .end local v7    # "w":F
    :cond_2
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v9, v9

    if-ge v3, v9, :cond_5

    .line 1391
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v4, v9, v3

    .line 1392
    .local v4, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-nez v4, :cond_4

    .line 1390
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1394
    :cond_4
    iget v9, v4, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v10, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    and-int/2addr v9, v10

    if-eqz v9, :cond_3

    .line 1397
    invoke-virtual {v4, v12}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getWidth(Z)F

    move-result v7

    .line 1398
    .restart local v7    # "w":F
    invoke-virtual {v4, v12}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getHeight(Z)F

    move-result v2

    .line 1399
    .restart local v2    # "h":F
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getX()F

    move-result v9

    div-float v10, v7, v11

    sub-float v5, v9, v10

    .line 1400
    .restart local v5    # "sx":F
    add-float v0, v5, v7

    .line 1401
    .restart local v0    # "ex":F
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getY()F

    move-result v9

    div-float v10, v2, v11

    add-float v6, v9, v10

    .line 1402
    .restart local v6    # "sy":F
    sub-float v1, v6, v2

    .line 1403
    .restart local v1    # "ey":F
    cmpg-float v9, v5, p1

    if-gtz v9, :cond_3

    cmpg-float v9, p1, v0

    if-gtz v9, :cond_3

    cmpl-float v9, v6, p2

    if-ltz v9, :cond_3

    cmpl-float v9, p2, v1

    if-ltz v9, :cond_3

    goto/16 :goto_0

    .end local v0    # "ex":F
    .end local v1    # "ey":F
    .end local v2    # "h":F
    .end local v4    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .end local v5    # "sx":F
    .end local v6    # "sy":F
    .end local v7    # "w":F
    :cond_5
    move-object v4, v8

    .line 1406
    goto/16 :goto_0
.end method

.method private freeFolderItems()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 578
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v1, :cond_0

    .line 579
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 580
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->remove()V

    .line 581
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    aput-object v2, v1, v0

    .line 579
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 584
    .end local v0    # "i":I
    :cond_0
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .line 585
    return-void
.end method

.method private getCurrentInfo(Lcom/sec/samsung/gallery/glview/GlScroller;Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;)Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;
    .locals 7
    .param p1, "ctrlInfo"    # Lcom/sec/samsung/gallery/glview/GlScroller;
    .param p2, "objEstimator"    # Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    .prologue
    .line 637
    iget-object v3, p1, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 638
    .local v3, "objSet":[Lcom/sec/samsung/gallery/glview/GlBaseObject;
    iget v4, p1, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    iget v5, p1, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    sub-int/2addr v4, v5

    add-int/lit8 v2, v4, 0x1

    .line 639
    .local v2, "objCount":I
    if-eqz v3, :cond_0

    if-gtz v2, :cond_1

    .line 640
    :cond_0
    const-string v4, "GlPhotoView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getCurrentInfo : No visible items first = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", last = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    const/4 v1, 0x0

    .line 660
    :goto_0
    return-object v1

    .line 644
    :cond_1
    new-instance v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    invoke-direct {v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;-><init>()V

    .line 645
    .local v1, "listInfo":Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;
    iget v4, p1, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    iput v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mFirst:I

    .line 646
    iget v4, p1, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    iget v5, p1, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    sub-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x1

    iput v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCount:I

    .line 647
    iget v4, p1, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    iput v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mSelected:I

    .line 648
    iget v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCount:I

    new-array v4, v4, [I

    iput-object v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mIndexs:[I

    .line 649
    iget v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCount:I

    new-array v4, v4, [Lcom/sec/android/gallery3d/glcore/GlObject;

    iput-object v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 650
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCount:I

    if-ge v0, v4, :cond_2

    .line 651
    iget-object v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mIndexs:[I

    iget v5, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mFirst:I

    add-int/2addr v5, v0

    aput v5, v4, v0

    .line 652
    iget-object v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v5, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mFirst:I

    add-int/2addr v5, v0

    invoke-virtual {p0, v3, v5}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getObjectIndex([Lcom/sec/samsung/gallery/glview/GlBaseObject;I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v5

    aput-object v5, v4, v0

    .line 650
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 654
    :cond_2
    const/4 v0, 0x0

    :goto_2
    array-length v4, v3

    if-ge v0, v4, :cond_4

    .line 655
    aget-object v4, v3, v0

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    and-int/2addr v4, v5

    if-nez v4, :cond_3

    .line 656
    aget-object v4, v3, v0

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->remove()V

    .line 654
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 658
    :cond_4
    iput-object p2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    .line 659
    iget-object v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    invoke-interface {v4}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;->saveState()V

    goto :goto_0
.end method

.method private getHorizontalCount()V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v1, 0x1

    const/4 v3, 0x4

    const/4 v4, 0x3

    const/4 v2, 0x2

    .line 2220
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    if-nez v5, :cond_1

    .line 2250
    :cond_0
    :goto_0
    return-void

    .line 2224
    :cond_1
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    if-nez v5, :cond_5

    .line 2225
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWideMode:Z

    if-eqz v3, :cond_2

    move v1, v2

    :cond_2
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    .line 2237
    :cond_3
    :goto_1
    const/4 v0, 0x0

    .line 2238
    .local v0, "adapter":Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    instance-of v1, v1, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    if-eqz v1, :cond_4

    .line 2239
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .end local v0    # "adapter":Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    check-cast v0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    .line 2240
    .restart local v0    # "adapter":Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    if-ne v1, v6, :cond_f

    .line 2241
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v1, :cond_4

    .line 2242
    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->setThumbSize(I)V

    .line 2248
    :cond_4
    :goto_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mChangeThumbSizeListner:Lcom/sec/samsung/gallery/glview/GlPhotoView$OnChangeThumbSizeListener;

    if-eqz v1, :cond_0

    .line 2249
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mChangeThumbSizeListner:Lcom/sec/samsung/gallery/glview/GlPhotoView$OnChangeThumbSizeListener;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-interface {v1, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$OnChangeThumbSizeListener;->OnChangeThumbSize(I)V

    goto :goto_0

    .line 2226
    .end local v0    # "adapter":Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    :cond_5
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    if-ne v5, v1, :cond_7

    .line 2227
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWideMode:Z

    if-eqz v1, :cond_6

    move v1, v3

    :goto_3
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    goto :goto_1

    :cond_6
    move v1, v2

    goto :goto_3

    .line 2228
    :cond_7
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    if-ne v1, v2, :cond_9

    .line 2229
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWideMode:Z

    if-eqz v1, :cond_8

    move v1, v4

    :goto_4
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    goto :goto_1

    :cond_8
    move v1, v2

    goto :goto_4

    .line 2230
    :cond_9
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    if-ne v1, v4, :cond_b

    .line 2231
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWideMode:Z

    if-eqz v1, :cond_a

    :goto_5
    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    goto :goto_1

    :cond_a
    move v3, v4

    goto :goto_5

    .line 2232
    :cond_b
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    if-ne v1, v3, :cond_d

    .line 2233
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWideMode:Z

    if-eqz v1, :cond_c

    const/4 v3, 0x6

    :cond_c
    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    goto :goto_1

    .line 2234
    :cond_d
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    if-ne v1, v6, :cond_3

    .line 2235
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWideMode:Z

    if-eqz v1, :cond_e

    const/16 v1, 0xc

    :goto_6
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    goto :goto_1

    :cond_e
    const/16 v1, 0x8

    goto :goto_6

    .line 2244
    .restart local v0    # "adapter":Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    :cond_f
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v1, :cond_4

    .line 2245
    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->setThumbSize(I)V

    goto :goto_2
.end method

.method private getResource(I)F
    .locals 2
    .param p1, "resID"    # I

    .prologue
    .line 261
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 262
    .local v0, "str":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    return v1
.end method

.method private hideBgAndArrow(F)V
    .locals 4
    .param p1, "widthDelta"    # F

    .prologue
    .line 3767
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    if-nez v0, :cond_1

    .line 3806
    :cond_0
    :goto_0
    return-void

    .line 3770
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    const/4 v1, 0x0

    sget v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ARROW_VISIBLE_EXPAND:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 3771
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPosAsSource()V

    .line 3772
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getX()F

    move-result v1

    sub-float/2addr v1, p1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getZ()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setTargetPos(FFF)V

    .line 3774
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v0, :cond_2

    .line 3775
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setPosAsSource()V

    .line 3776
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getX()F

    move-result v1

    sub-float/2addr v1, p1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getZ()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setTargetPos(FFF)V

    .line 3778
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPosAsSource()V

    .line 3779
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getX()F

    move-result v1

    sub-float/2addr v1, p1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getZ()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setTargetPos(FFF)V

    .line 3783
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->setPosAsSource()V

    .line 3784
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->getX()F

    move-result v1

    sub-float/2addr v1, p1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->getZ()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->setTargetPos(FFF)V

    .line 3786
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mBgExpandAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;

    new-instance v1, Lcom/sec/samsung/gallery/glview/GlPhotoView$23;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$23;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 3805
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mBgExpandAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->start()V

    goto/16 :goto_0
.end method

.method private hideFolder(Z)V
    .locals 10
    .param p1, "playAnimation"    # Z

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 3642
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-nez v5, :cond_1

    .line 3683
    :cond_0
    return-void

    .line 3644
    :cond_1
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWideMode:Z

    if-eqz v5, :cond_2

    sget v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SPLIT_RATIO_LAND:F

    .line 3645
    .local v3, "ratio":F
    :goto_0
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidthSpace:F

    mul-float v4, v3, v5

    .line 3646
    .local v4, "widthL":F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    array-length v5, v5

    if-ge v1, v5, :cond_0

    .line 3647
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    aget-object v2, v5, v1

    .line 3649
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/GlSplitObject;
    if-nez v2, :cond_3

    .line 3646
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3644
    .end local v1    # "i":I
    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/GlSplitObject;
    .end local v3    # "ratio":F
    .end local v4    # "widthL":F
    :cond_2
    sget v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SPLIT_RATIO_PORT:F

    goto :goto_0

    .line 3650
    .restart local v1    # "i":I
    .restart local v2    # "obj":Lcom/sec/samsung/gallery/glview/GlSplitObject;
    .restart local v3    # "ratio":F
    .restart local v4    # "widthL":F
    :cond_3
    iget v5, v2, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mAttr:I

    sget v6, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    and-int/2addr v5, v6

    if-nez v5, :cond_4

    .line 3651
    const-string v5, "GlPhotoView"

    const-string v6, "obj is not active, hide immediately"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3652
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getX()F

    move-result v5

    sub-float/2addr v5, v4

    invoke-virtual {v2, v5, v8, v8, v9}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setPos(FFFI)V

    goto :goto_2

    .line 3656
    :cond_4
    if-eqz p1, :cond_5

    .line 3657
    new-instance v0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    neg-float v5, v4

    invoke-direct {v0, v2, v5, v8, v8}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;-><init>(Lcom/sec/android/gallery3d/glcore/GlObject;FFF)V

    .line 3658
    .local v0, "ani":Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 3659
    const-wide/16 v6, 0x1f4

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->setDuration(J)V

    .line 3660
    new-instance v5, Lcom/sec/samsung/gallery/glview/GlPhotoView$21;

    invoke-direct {v5, p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$21;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/samsung/gallery/glview/GlSplitObject;)V

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 3677
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->start()V

    .line 3678
    iput-boolean v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mLockPos:Z

    goto :goto_2

    .line 3680
    .end local v0    # "ani":Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;
    :cond_5
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setVisibility(Z)V

    goto :goto_2
.end method

.method private initArrowObject()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 2819
    const v0, 0x7f02026a

    .line 2820
    .local v0, "arrorId":I
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    sget v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_CANVAS_SMALL:I

    invoke-direct {v2, p0, v4, v4, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFI)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 2821
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 2822
    .local v1, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 2823
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 2824
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-eqz v2, :cond_0

    .line 2825
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    const/4 v3, 0x0

    sget v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ARROW_VISIBLE_EXPAND:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 2827
    :cond_0
    return-void
.end method

.method private initFixedAlbum()V
    .locals 10

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    .line 2830
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-nez v0, :cond_0

    .line 2831
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    sget v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_CANVAS_TINY:I

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFI)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 2832
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    new-instance v1, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFixedAlbumSeparatorView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-direct {v1, p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFixedAlbumSeparatorView;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 2833
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v0, v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(Z)V

    .line 2836
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-nez v0, :cond_1

    .line 2837
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumThumbnailWidthPixel(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumThumbnailHeightPixel(I)I

    move-result v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxWidthPixel(I)I

    move-result v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v5, v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxHeightPixel(I)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlSplitObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;IIII)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .line 2844
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_2

    .line 2845
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v3, v3, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    move v4, v9

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 2847
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v7

    .line 2848
    .local v7, "textObj":Lcom/sec/samsung/gallery/glview/GlTextObject;
    if-eqz v7, :cond_2

    .line 2849
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/glview/GlTextObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v5

    move v4, v9

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v6

    .line 2851
    .local v6, "glTextView":Lcom/sec/android/gallery3d/glcore/GlView;
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2852
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 2856
    .end local v6    # "glTextView":Lcom/sec/android/gallery3d/glcore/GlView;
    .end local v7    # "textObj":Lcom/sec/samsung/gallery/glview/GlTextObject;
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerAlbumMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 2857
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerAlbumClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)V

    .line 2858
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerDrop:Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setDragListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;)V

    .line 2859
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerGenericMotionEx:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setGenericMotionListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;)V

    .line 2860
    return-void
.end method

.method private loadFolders()V
    .locals 1

    .prologue
    .line 1144
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->loadFolders(Z)V

    .line 1145
    return-void
.end method

.method private loadFolders(Z)V
    .locals 6
    .param p1, "doAnimation"    # Z

    .prologue
    const/4 v5, 0x1

    .line 1149
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v2

    if-nez v2, :cond_2

    .line 1150
    :cond_0
    const-string v2, "GlPhotoView"

    const-string v3, "mFolderAdapter is empty"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1194
    :cond_1
    :goto_0
    return-void

    .line 1154
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    if-nez v2, :cond_3

    .line 1155
    const-string v2, "GlPhotoView"

    const-string v3, "mAlbumCtrl is null"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1159
    :cond_3
    const-string v2, "GlPhotoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadFolders ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimStatus:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", type = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1160
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimStatus:I

    sget v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_STATUS_IDLE:I

    if-eq v2, v3, :cond_4

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimType:I

    sget v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_TYPE_VIEW:I

    if-eq v2, v3, :cond_9

    .line 1161
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iput-boolean v5, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mInitVisible:Z

    .line 1166
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v2, :cond_5

    .line 1167
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    check-cast v2, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getFocusedIndex()I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    .line 1170
    :cond_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v0

    .line 1171
    .local v0, "count":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mFullVisibleCnt:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->access$1200(Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;)I

    move-result v2

    if-ge v0, v2, :cond_a

    .line 1172
    const/4 v1, 0x0

    .line 1180
    .local v1, "first":I
    :cond_6
    :goto_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v2, v1, v0, v5}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->resetSet(IIZ)V

    .line 1181
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScroll:F

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScrollRange:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_7

    .line 1182
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScrollRange:F

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->setScroll(F)V

    .line 1183
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->hideScrollBarImmediately()V

    .line 1185
    :cond_7
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScroll:F

    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSplitSx:F

    .line 1186
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScrollRange:F

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setRange(FF)V

    .line 1187
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSplitSx:F

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 1188
    if-eqz p1, :cond_8

    .line 1189
    sget v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_PRECOND_ALBUM:I

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->checkStartAnimation(I)V

    .line 1191
    :cond_8
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimStatus:I

    sget v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_STATUS_IDLE:I

    if-ne v2, v3, :cond_1

    .line 1192
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->updateArrowObject()V

    goto/16 :goto_0

    .line 1163
    .end local v0    # "count":I
    .end local v1    # "first":I
    :cond_9
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mInitVisible:Z

    goto :goto_1

    .line 1174
    .restart local v0    # "count":I
    :cond_a
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mFullVisibleCnt:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->access$1200(Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int v1, v2, v3

    .line 1175
    .restart local v1    # "first":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mFullVisibleCnt:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->access$1200(Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;)I

    move-result v2

    add-int/2addr v2, v1

    if-le v2, v0, :cond_b

    .line 1176
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mFullVisibleCnt:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->access$1200(Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;)I

    move-result v2

    sub-int v1, v0, v2

    .line 1177
    :cond_b
    if-gez v1, :cond_6

    const/4 v1, 0x0

    goto :goto_2
.end method

.method private loadThumbs()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1007
    const/4 v0, 0x0

    .line 1009
    .local v0, "count":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    if-nez v1, :cond_1

    .line 1044
    :cond_0
    :goto_0
    return-void

    .line 1012
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v0

    .line 1013
    const-string v1, "GlPhotoView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loadThumbs animState ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimStatus:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", count = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-nez v1, :cond_2

    .line 1016
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->createThumbItems()[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 1017
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->register([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 1018
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->moveToLast()V

    .line 1020
    :cond_2
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimStatus:I

    sget v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_STATUS_IDLE:I

    if-ne v1, v2, :cond_6

    .line 1021
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mInitVisible:Z

    .line 1026
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mItemFocused:I

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->resetSet(II)V

    .line 1027
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mMaintainScroll:Z

    if-eqz v1, :cond_3

    .line 1028
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSx:F

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->setScroll(F)V

    .line 1029
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mMaintainScroll:Z

    .line 1032
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->applyThumbPosition()V

    .line 1033
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    if-eqz v1, :cond_4

    .line 1034
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScrollRange:F

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setRange(FF)V

    .line 1035
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScroll:F

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 1037
    :cond_4
    if-lez v0, :cond_5

    .line 1038
    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_PRECOND_THUMB:I

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->checkStartAnimation(I)V

    .line 1039
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mVisibleFirst:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mVisibleLast:I

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->updateVisiableRange(II)V

    .line 1041
    :cond_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimType:I

    sget v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_TYPE_TIME:I

    if-ne v1, v2, :cond_0

    .line 1042
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->convAnimGrid()V

    goto/16 :goto_0

    .line 1023
    :cond_6
    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mItemFocused:I

    goto :goto_1
.end method

.method private needToShowFixedAlbum()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2983
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    const/16 v5, -0x64

    if-ne v4, v5, :cond_1

    .line 3003
    :cond_0
    :goto_0
    return v2

    .line 2986
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemGapH:F

    const/high16 v5, 0x40400000    # 3.0f

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScrollVisibleH:F

    const/high16 v6, 0x40000000    # 2.0f

    mul-float/2addr v5, v6

    cmpl-float v4, v4, v5

    if-gtz v4, :cond_0

    .line 2989
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-nez v4, :cond_0

    .line 2992
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemGapH:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    int-to-float v5, v5

    mul-float v1, v4, v5

    .line 2993
    .local v1, "topScrollBorder":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScrollVisibleH:F

    sub-float v4, v1, v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemH:F

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxHeight(I)F

    move-result v5

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumTopPadding(I)F

    move-result v5

    add-float/2addr v4, v5

    sget v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->FIXEDABLUM_VISIABLE_ADJUST_VALUE:F

    add-float v0, v4, v5

    .line 2998
    .local v0, "botScrollBorder":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScroll:F

    cmpl-float v4, v4, v1

    if-gtz v4, :cond_2

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScroll:F

    cmpg-float v4, v4, v0

    if-gez v4, :cond_0

    .line 2999
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScroll:F

    cmpl-float v4, v4, v1

    if-lez v4, :cond_3

    move v2, v3

    :cond_3
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsFixedAlbumAtTop:Z

    move v2, v3

    .line 3000
    goto :goto_0
.end method

.method private pushCurrentListInfo()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 588
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-nez v2, :cond_1

    .line 634
    :cond_0
    :goto_0
    return-void

    .line 591
    :cond_1
    new-instance v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    invoke-direct {v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;-><init>()V

    .line 592
    .local v1, "listInfo":Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;
    const-class v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mViewType:Ljava/lang/String;

    .line 593
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mVisibleFirst:I

    iput v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mFirst:I

    .line 594
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mVisibleLast:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mVisibleFirst:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCount:I

    .line 595
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCount:I

    if-gtz v2, :cond_2

    .line 596
    iput v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCount:I

    .line 598
    :cond_2
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCount:I

    new-array v2, v2, [I

    iput-object v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mIndexs:[I

    .line 599
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCount:I

    new-array v2, v2, [Lcom/sec/android/gallery3d/glcore/GlObject;

    iput-object v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 600
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCount:I

    if-ge v0, v2, :cond_3

    .line 601
    iget-object v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mIndexs:[I

    iget v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mFirst:I

    add-int/2addr v3, v0

    aput v3, v2, v0

    .line 602
    iget-object v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mFirst:I

    add-int/2addr v3, v0

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getObjectIndex(I)Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v3

    aput-object v3, v2, v0

    .line 600
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 604
    :cond_3
    const/4 v0, 0x0

    :goto_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 605
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v2, v2, v0

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAttr:I

    sget v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    and-int/2addr v2, v3

    if-nez v2, :cond_4

    .line 606
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->remove()V

    .line 604
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 609
    :cond_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleFirst:I

    iput v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mFirstEx:I

    .line 610
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleLast:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleFirst:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCountEx:I

    .line 611
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCountEx:I

    if-gtz v2, :cond_6

    .line 612
    iput v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCountEx:I

    .line 614
    :cond_6
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    iput v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mSelectedEx:I

    .line 615
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCountEx:I

    new-array v2, v2, [I

    iput-object v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mIndexsEx:[I

    .line 616
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCountEx:I

    new-array v2, v2, [Lcom/sec/android/gallery3d/glcore/GlObject;

    iput-object v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectsEx:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 617
    const/4 v0, 0x0

    :goto_3
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCountEx:I

    if-ge v0, v2, :cond_8

    .line 618
    iget-object v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mIndexsEx:[I

    iget v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mFirstEx:I

    add-int/2addr v3, v0

    aput v3, v2, v0

    .line 619
    iget-object v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectsEx:[Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mFirstEx:I

    add-int/2addr v3, v0

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getObjectIndexLeft(I)Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v3

    aput-object v3, v2, v0

    .line 620
    iget-object v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectsEx:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v2, v2, v0

    if-eqz v2, :cond_7

    iget-object v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectsEx:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->getChildCount()I

    move-result v2

    if-lez v2, :cond_7

    .line 621
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectsEx:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAllChildObject()[Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->addObjectNotToDetach([Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 617
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 624
    :cond_8
    const/4 v0, 0x0

    :goto_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    array-length v2, v2

    if-ge v0, v2, :cond_a

    .line 625
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    aget-object v2, v2, v0

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mAttr:I

    sget v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    and-int/2addr v2, v3

    if-nez v2, :cond_9

    .line 626
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->remove()V

    .line 624
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 629
    :cond_a
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mObjEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    iput-object v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    .line 630
    iget-object v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    invoke-interface {v2}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;->saveState()V

    .line 631
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->addObjectNotToDetach([Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 632
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectsEx:[Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->addObjectNotToDetach([Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 633
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->saveStatus(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private reloadFolder()V
    .locals 3

    .prologue
    .line 993
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->loadFolders()V

    .line 995
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-nez v0, :cond_0

    .line 996
    const-string v0, "GlPhotoView"

    const-string v1, "reload folder cancelled : folder adapter is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1004
    :goto_0
    return-void

    .line 999
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 1000
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleFirst:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleLast:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->updateVisiableRange(II)V

    goto :goto_0

    .line 1002
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->updateVisiableRange(II)V

    goto :goto_0
.end method

.method private removeAllObject(Lcom/sec/samsung/gallery/glview/GlScroller;)V
    .locals 3
    .param p1, "ctrlInfo"    # Lcom/sec/samsung/gallery/glview/GlScroller;

    .prologue
    .line 672
    iget-object v1, p1, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 673
    .local v1, "objSet":[Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-nez v1, :cond_1

    .line 678
    :cond_0
    return-void

    .line 675
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p1, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 676
    aget-object v2, v1, v0

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->remove()V

    .line 675
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private removeUnusedObject(Lcom/sec/samsung/gallery/glview/GlScroller;)V
    .locals 4
    .param p1, "ctrlInfo"    # Lcom/sec/samsung/gallery/glview/GlScroller;

    .prologue
    .line 664
    iget-object v1, p1, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 665
    .local v1, "objSet":[Lcom/sec/samsung/gallery/glview/GlBaseObject;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p1, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 666
    aget-object v2, v1, v0

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_USED:I

    and-int/2addr v2, v3

    if-nez v2, :cond_0

    .line 667
    aget-object v2, v1, v0

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->remove()V

    .line 665
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 669
    :cond_1
    return-void
.end method

.method private setFolderFlingSpeed(I)V
    .locals 2
    .param p1, "speed"    # I

    .prologue
    .line 2364
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->startFling(F)V

    .line 2365
    return-void
.end method

.method private setPreviousInfo()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 399
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->restoreStatus()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    .line 400
    sget v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_TYPE_NONE:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimType:I

    .line 401
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    if-eqz v0, :cond_2

    .line 408
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 409
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mSelected:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    .line 410
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mViewType:Ljava/lang/String;

    const-class v1, Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 411
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnConvAnim:Z

    .line 413
    :cond_1
    sget v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_TYPE_VIEW:I

    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_PRECOND_ALBUM:I

    sget v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_PRECOND_THUMB:I

    or-int/2addr v1, v2

    invoke-virtual {p0, v0, v1, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->requestAnimation(IIZ)V

    .line 415
    :cond_2
    return-void
.end method

.method private setScaleFactor(ZI)V
    .locals 0
    .param p1, "wide"    # Z
    .param p2, "colCount"    # I

    .prologue
    .line 2273
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getHorizontalCount()V

    .line 2274
    return-void
.end method

.method private setThumbImageFlingSpeed(I)V
    .locals 2
    .param p1, "speed"    # I

    .prologue
    .line 2355
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->startFling(F)V

    .line 2356
    return-void
.end method

.method private setThumbnailViewMode(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    const/4 v3, 0x2

    .line 2390
    if-ltz p1, :cond_0

    const/4 v0, 0x5

    if-lt p1, v0, :cond_1

    .line 2410
    :cond_0
    :goto_0
    return-void

    .line 2394
    :cond_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsEasyMode:Z

    if-eqz v0, :cond_3

    .line 2395
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-eqz v0, :cond_2

    .line 2396
    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    goto :goto_0

    .line 2398
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    goto :goto_0

    .line 2400
    :cond_3
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    .line 2402
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsNoSplitMode:Z

    if-eqz v0, :cond_4

    .line 2403
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    const-string v1, "eventViewMode"

    invoke-static {v0, v1, p1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0

    .line 2405
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSPKeyString:Ljava/lang/String;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 2406
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    if-le v0, v3, :cond_0

    .line 2407
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mExpandViewMode:I

    goto :goto_0
.end method

.method private showBgAndArrow(F)V
    .locals 4
    .param p1, "widthDelta"    # F

    .prologue
    .line 3809
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    if-nez v0, :cond_1

    .line 3852
    :cond_0
    :goto_0
    return-void

    .line 3811
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->setVisibility(Z)V

    .line 3813
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->alignBgPosition()V

    .line 3814
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->updateArrowObject()V

    .line 3816
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFixScrollingAlbum:Z

    if-eqz v0, :cond_2

    .line 3817
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->updateFixedAlbum()V

    .line 3818
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v0, :cond_0

    .line 3821
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v0, :cond_2

    .line 3822
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setPosAsTarget()V

    .line 3823
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getX()F

    move-result v1

    sub-float/2addr v1, p1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getZ()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setSourcePos(FFF)V

    .line 3825
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPosAsTarget()V

    .line 3826
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getX()F

    move-result v1

    sub-float/2addr v1, p1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getZ()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setSourcePos(FFF)V

    .line 3831
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPosAsTarget()V

    .line 3832
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getX()F

    move-result v1

    sub-float/2addr v1, p1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getZ()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setSourcePos(FFF)V

    .line 3834
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->setPosAsTarget()V

    .line 3835
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->getX()F

    move-result v1

    sub-float/2addr v1, p1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->getZ()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->setSourcePos(FFF)V

    .line 3836
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mBgExpandAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;

    new-instance v1, Lcom/sec/samsung/gallery/glview/GlPhotoView$24;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$24;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 3851
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mBgExpandAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->start()V

    goto/16 :goto_0
.end method

.method private showFixedAlbum()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/high16 v11, 0x40000000    # 2.0f

    const/4 v8, 0x0

    .line 2863
    iput-boolean v12, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsFixedAlbumVisible:Z

    .line 2864
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->showFixedAlbumBorder()Z

    move-result v9

    invoke-virtual {v7, v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(Z)V

    .line 2866
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v7, v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumThumbnailHeightPixel(I)I

    move-result v7

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumTopPaddingPixel(I)I

    move-result v9

    add-int/2addr v7, v9

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxHeightPixel(I)I

    move-result v9

    add-int/2addr v7, v9

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxYOffsetPixel(I)I

    move-result v9

    add-int/2addr v7, v9

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumDividerHeightPixel(I)I

    move-result v9

    add-int/2addr v7, v9

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumHpx:I

    .line 2872
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v7, :cond_0

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    check-cast v7, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-ne v9, v7, :cond_0

    .line 2873
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumHpx:I

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumDividerHeightPixel(I)I

    move-result v9

    sub-int/2addr v7, v9

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumHpx:I

    .line 2875
    :cond_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v7, v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumThumbnailHeight(I)F

    move-result v7

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumTopPadding(I)F

    move-result v9

    add-float/2addr v7, v9

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxHeight(I)F

    move-result v9

    add-float/2addr v7, v9

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxYOffset(I)F

    move-result v9

    add-float/2addr v7, v9

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumDividerHeight(I)F

    move-result v9

    add-float/2addr v7, v9

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumH:F

    .line 2881
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeight:I

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeightPixel()I

    move-result v9

    sub-int v9, v7, v9

    iget-boolean v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mNewAlbumMode:Z

    if-eqz v7, :cond_1

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->newAlbumHeaderHeight:F

    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v10}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v10

    div-float/2addr v7, v10

    float-to-int v7, v7

    :goto_0
    sub-int v7, v9, v7

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumHpx:I

    sub-int v5, v7, v9

    .line 2884
    .local v5, "visibleAreaMaxY":I
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsFixedAlbumAtTop:Z

    if-eqz v7, :cond_2

    move v6, v8

    .line 2886
    .local v6, "visibleAreaMinY":I
    :goto_1
    const/4 v4, 0x0

    .line 2887
    .local v4, "separatorY":F
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsFixedAlbumAtTop:Z

    if-eqz v7, :cond_4

    .line 2888
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    div-float/2addr v7, v11

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v9

    sub-float v9, v7, v9

    iget-boolean v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mNewAlbumMode:Z

    if-eqz v7, :cond_3

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->newAlbumHeaderHeight:F

    :goto_2
    sub-float v7, v9, v7

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumH:F

    sub-float v4, v7, v9

    .line 2895
    :goto_3
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getX()F

    move-result v9

    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v10, v10, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemStartZ:F

    const v11, 0x3a83126f    # 0.001f

    add-float/2addr v10, v11

    invoke-virtual {v7, v9, v4, v10}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 2896
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRectForAlbums:Landroid/graphics/Rect;

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidth:I

    invoke-virtual {v7, v8, v6, v9, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 2898
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-nez v7, :cond_5

    .line 2906
    :goto_4
    return-void

    .end local v4    # "separatorY":F
    .end local v5    # "visibleAreaMaxY":I
    .end local v6    # "visibleAreaMinY":I
    :cond_1
    move v7, v8

    .line 2881
    goto :goto_0

    .line 2884
    .restart local v5    # "visibleAreaMaxY":I
    :cond_2
    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumHpx:I

    goto :goto_1

    .line 2888
    .restart local v4    # "separatorY":F
    .restart local v6    # "visibleAreaMinY":I
    :cond_3
    const/4 v7, 0x0

    goto :goto_2

    .line 2891
    :cond_4
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    neg-float v7, v7

    div-float/2addr v7, v11

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumH:F

    add-float v4, v7, v9

    .line 2892
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v7, v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumTopPadding(I)F

    move-result v7

    sub-float/2addr v4, v7

    goto :goto_3

    .line 2900
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .local v0, "arr$":[Lcom/sec/samsung/gallery/glview/GlSplitObject;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_5
    if-ge v1, v2, :cond_6

    aget-object v3, v0, v1

    .line 2901
    .local v3, "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    invoke-virtual {v3, v12}, Lcom/sec/android/gallery3d/glcore/GlObject;->setClippingEnabled(Z)V

    .line 2902
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRectForAlbums:Landroid/graphics/Rect;

    iput-object v7, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mClipRect:Landroid/graphics/Rect;

    .line 2900
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 2904
    .end local v3    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_6
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->updateArrowObject()V

    .line 2905
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v7, v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setClippingEnabled(Z)V

    goto :goto_4
.end method

.method private showFixedAlbumBorder()Z
    .locals 4

    .prologue
    .line 3007
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScroll:F

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemGapH:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScrollVisibleH:F

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemH:F

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxHeight(I)F

    move-result v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumTopPadding(I)F

    move-result v2

    add-float/2addr v1, v2

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->FIXEDABLUM_VISIABLE_ADJUST_VALUE:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showFolder(Z)V
    .locals 14
    .param p1, "playAnimation"    # Z

    .prologue
    .line 3686
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-nez v2, :cond_1

    .line 3728
    :cond_0
    :goto_0
    return-void

    .line 3688
    :cond_1
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWideMode:Z

    if-eqz v2, :cond_3

    sget v9, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SPLIT_RATIO_LAND:F

    .line 3689
    .local v9, "ratio":F
    :goto_1
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidthSpace:F

    mul-float v10, v9, v2

    .line 3690
    .local v10, "widthL":F
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    array-length v2, v2

    if-ge v8, v2, :cond_5

    .line 3691
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    aget-object v1, v2, v8

    .line 3693
    .local v1, "obj":Lcom/sec/samsung/gallery/glview/GlSplitObject;
    if-eqz v1, :cond_2

    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mAttr:I

    sget v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    and-int/2addr v2, v5

    if-nez v2, :cond_4

    .line 3690
    :cond_2
    :goto_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 3688
    .end local v1    # "obj":Lcom/sec/samsung/gallery/glview/GlSplitObject;
    .end local v8    # "i":I
    .end local v9    # "ratio":F
    .end local v10    # "widthL":F
    :cond_3
    sget v9, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SPLIT_RATIO_PORT:F

    goto :goto_1

    .line 3694
    .restart local v1    # "obj":Lcom/sec/samsung/gallery/glview/GlSplitObject;
    .restart local v8    # "i":I
    .restart local v9    # "ratio":F
    .restart local v10    # "widthL":F
    :cond_4
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setVisibility(Z)V

    .line 3695
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 3696
    if-eqz p1, :cond_2

    .line 3697
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getX()F

    move-result v3

    .line 3698
    .local v3, "x":F
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getY()F

    move-result v4

    .line 3699
    .local v4, "y":F
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getZ()F

    move-result v6

    .line 3700
    .local v6, "z":F
    new-instance v0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    sub-float v2, v3, v10

    move v5, v4

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;-><init>(Lcom/sec/android/gallery3d/glcore/GlObject;FFFFFF)V

    .line 3701
    .local v0, "ani":Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 3702
    const-wide/16 v12, 0x1f4

    invoke-virtual {v0, v12, v13}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->setDuration(J)V

    .line 3703
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$22;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$22;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 3717
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->start()V

    goto :goto_3

    .line 3721
    .end local v0    # "ani":Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;
    .end local v1    # "obj":Lcom/sec/samsung/gallery/glview/GlSplitObject;
    .end local v3    # "x":F
    .end local v4    # "y":F
    .end local v6    # "z":F
    :cond_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v2, :cond_0

    .line 3722
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v2

    if-lez v2, :cond_6

    .line 3723
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleFirst:I

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleLast:I

    invoke-virtual {v2, v5, v7}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->updateVisiableRange(II)V

    goto :goto_0

    .line 3726
    :cond_6
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    const/4 v5, 0x0

    const/4 v7, -0x1

    invoke-virtual {v2, v5, v7}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->updateVisiableRange(II)V

    goto :goto_0
.end method

.method private startDragAnim(Lcom/sec/android/gallery3d/glcore/GlObject;II)V
    .locals 24
    .param p1, "srcObject"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 2668
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    check-cast v2, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    .local v2, "editAdapter":Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    move-object/from16 v3, p1

    .line 2669
    check-cast v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget v0, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    move/from16 v21, v0

    .line 2674
    .local v21, "selected":I
    const/4 v12, 0x0

    .line 2675
    .local v12, "added":I
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->selectedCount()I

    move-result v16

    .line 2676
    .local v16, "count":I
    if-gtz v16, :cond_1

    .line 2677
    const-string v3, "GlPhotoView"

    const-string v4, "no item selected"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2742
    :cond_0
    :goto_0
    return-void

    .line 2680
    :cond_1
    sget v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->THUMB_DRAG_MAX:I

    move/from16 v0, v16

    if-le v0, v3, :cond_2

    .line 2681
    sget v16, Lcom/sec/samsung/gallery/glview/GlPhotoView;->THUMB_DRAG_MAX:I

    .line 2682
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2683
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 2684
    :cond_3
    move/from16 v0, v16

    new-array v10, v0, [Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 2685
    .local v10, "thumbSet":[Lcom/sec/samsung/gallery/glview/GlThumbObject;
    move/from16 v0, v16

    new-array v11, v0, [Lcom/sec/android/gallery3d/glcore/GlView;

    .line 2686
    .local v11, "viewSet":[Lcom/sec/android/gallery3d/glcore/GlView;
    const/16 v17, 0x0

    .local v17, "i":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    array-length v0, v3

    move/from16 v18, v0

    .local v18, "n":I
    :goto_1
    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_6

    .line 2687
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v22, v3, v17

    .line 2688
    .local v22, "thumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    if-eqz v22, :cond_4

    move-object/from16 v0, v22

    iget v3, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    if-ltz v3, :cond_4

    move-object/from16 v0, v22

    iget v3, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemCount:I

    if-lt v3, v4, :cond_5

    .line 2686
    :cond_4
    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    .line 2690
    :cond_5
    move-object/from16 v0, v22

    iget v3, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->isSelected(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2691
    move-object/from16 v0, v22

    iget v3, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    move/from16 v0, v21

    if-ne v3, v0, :cond_9

    if-lez v12, :cond_9

    .line 2693
    const/4 v3, 0x0

    aget-object v3, v10, v3

    aput-object v3, v10, v12

    .line 2694
    const/16 v23, 0x0

    .line 2697
    .local v23, "toAdd":I
    :goto_2
    aput-object v22, v10, v23

    .line 2698
    add-int/lit8 v12, v12, 0x1

    .line 2699
    move/from16 v0, v16

    if-lt v12, v0, :cond_4

    .line 2706
    .end local v22    # "thumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .end local v23    # "toAdd":I
    :cond_6
    const/4 v3, 0x0

    aget-object v3, v10, v3

    if-eqz v3, :cond_7

    const/4 v3, 0x0

    aget-object v3, v10, v3

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    move/from16 v0, v21

    if-eq v3, v0, :cond_7

    .line 2707
    const/4 v4, 0x0

    move-object/from16 v3, p1

    check-cast v3, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aput-object v3, v10, v4

    .line 2711
    :cond_7
    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    array-length v0, v3

    move/from16 v18, v0

    :goto_3
    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_b

    .line 2712
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v22, v3, v17

    .line 2713
    .restart local v22    # "thumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    if-eqz v22, :cond_8

    move-object/from16 v0, v22

    iget v3, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    if-ltz v3, :cond_8

    move-object/from16 v0, v22

    iget v3, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemCount:I

    if-lt v3, v4, :cond_a

    .line 2711
    :cond_8
    :goto_4
    add-int/lit8 v17, v17, 0x1

    goto :goto_3

    .line 2696
    :cond_9
    move/from16 v23, v12

    .restart local v23    # "toAdd":I
    goto :goto_2

    .line 2715
    .end local v23    # "toAdd":I
    :cond_a
    move-object/from16 v0, v22

    iget v3, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->isSelected(I)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2716
    invoke-virtual/range {v22 .. v22}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->dim()V

    goto :goto_4

    .line 2721
    .end local v22    # "thumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_b
    if-lez v12, :cond_0

    .line 2722
    const/16 v6, 0x100

    .line 2723
    .local v6, "viewExt":I
    const/16 v17, 0x0

    :goto_5
    move/from16 v0, v17

    if-ge v0, v12, :cond_c

    .line 2724
    aget-object v3, v10, v17

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThisListView:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    aput-object v3, v11, v17

    .line 2723
    add-int/lit8 v17, v17, 0x1

    goto :goto_5

    .line 2726
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumThumbnailWidth(I)F

    move-result v20

    .line 2727
    .local v20, "objectWidth":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumThumbnailHeight(I)F

    move-result v19

    .line 2728
    .local v19, "objectHeight":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDragAnim:Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    move/from16 v0, v20

    move/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->setObjectSize(FF)V

    .line 2730
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidthSpace:F

    move/from16 v0, p2

    int-to-float v4, v0

    mul-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidth:I

    int-to-float v4, v4

    div-float v14, v3, v4

    .line 2731
    .local v14, "sx":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    move/from16 v0, p3

    int-to-float v4, v0

    mul-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeight:I

    int-to-float v4, v4

    div-float v15, v3, v4

    .line 2732
    .local v15, "sy":F
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDragAnim:Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-object/from16 v13, p1

    invoke-virtual/range {v7 .. v15}, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->setThumb(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/samsung/gallery/view/utils/SoundUtils;[Lcom/sec/samsung/gallery/glview/GlThumbObject;[Lcom/sec/android/gallery3d/glcore/GlView;ILcom/sec/android/gallery3d/glcore/GlObject;FF)V

    .line 2734
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDragAnim:Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->getHeaderObject()Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 2735
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setPosIndex()I

    move-result v4

    iput v4, v3, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHndDispmnt:I

    .line 2736
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDragNDropInMultiwindow:Z

    if-eqz v3, :cond_d

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v3, :cond_d

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v3

    if-nez v3, :cond_e

    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isConnetedSideSync(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCallWindow(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_e

    .line 2738
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDragAnim:Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->startDragAnimation()V

    .line 2740
    :cond_e
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbMoveTo:Lcom/sec/android/gallery3d/glcore/GlObject;

    goto/16 :goto_0
.end method

.method private startExpandAnimation()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 3742
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mBgExpandAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;

    if-nez v2, :cond_0

    .line 3743
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/samsung/gallery/glview/GlPhotoView$1;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mBgExpandAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;

    .line 3744
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mBgExpandAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 3745
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mBgExpandAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v4, v5}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->setDuration(J)V

    .line 3747
    :cond_0
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWideMode:Z

    if-eqz v2, :cond_1

    sget v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SPLIT_RATIO_LAND:F

    .line 3748
    .local v0, "ratio":F
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidthSpace:F

    mul-float v1, v0, v2

    .line 3750
    .local v1, "widthL":F
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-eqz v2, :cond_2

    .line 3751
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 3752
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->setVisibility(Z)V

    .line 3753
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->hideBgAndArrow(F)V

    .line 3754
    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->hideFolder(Z)V

    .line 3761
    :goto_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->clearBoundarEffect()V

    .line 3762
    iput-boolean v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsResizing:Z

    .line 3763
    const/4 v2, 0x6

    invoke-virtual {p0, v2, v6, v6, v6}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    .line 3764
    return-void

    .line 3747
    .end local v0    # "ratio":F
    .end local v1    # "widthL":F
    :cond_1
    sget v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SPLIT_RATIO_PORT:F

    goto :goto_0

    .line 3756
    .restart local v0    # "ratio":F
    .restart local v1    # "widthL":F
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->setVisibility(Z)V

    .line 3757
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->showBgAndArrow(F)V

    .line 3758
    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->showFolder(Z)V

    goto :goto_1
.end method

.method private startExpandNoAnimation()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3731
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v0, :cond_0

    .line 3732
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    sget v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ARROW_VISIBLE_EXPAND:I

    invoke-virtual {v2, v0, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 3734
    :cond_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-eqz v0, :cond_2

    .line 3735
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->hideFolder(Z)V

    .line 3739
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 3732
    goto :goto_0

    .line 3737
    :cond_2
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->showFolder(Z)V

    goto :goto_1
.end method

.method private switchAlbum(I)V
    .locals 7
    .param p1, "index"    # I

    .prologue
    const/4 v6, 0x1

    .line 1800
    const-string v2, "GlPhotoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "switch album, index = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1804
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->scrollToVisible(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v0

    .line 1805
    .local v0, "object":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-nez v0, :cond_0

    .line 1831
    :goto_0
    return-void

    .line 1808
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 1809
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 1810
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderSpreadAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->stop()V

    .line 1812
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderObjecSel:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .local v1, "prevSelObject":Lcom/sec/samsung/gallery/glview/GlSplitObject;
    move-object v2, v0

    .line 1813
    check-cast v2, Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderObjecSel:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .line 1814
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    .line 1815
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnItemClickListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListenerEx;

    if-eqz v2, :cond_1

    .line 1816
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnItemClickListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListenerEx;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v4

    sget v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CLICK_ALBUM_SET:I

    invoke-interface {v2, v3, v4, p1, v5}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListenerEx;->onItemClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;II)V

    .line 1818
    :cond_1
    if-eqz v1, :cond_2

    if-eq v1, v0, :cond_2

    .line 1820
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v6}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setSelected(ZZ)V

    .line 1821
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 1824
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderObjecSel:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v2, v6, v6}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setSelected(ZZ)V

    .line 1825
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderObjecSel:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 1826
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFixScrollingAlbum:Z

    if-eqz v2, :cond_3

    .line 1827
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->updateFixedAlbum()V

    .line 1829
    :cond_3
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->updateArrowObject()V

    .line 1830
    sget v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_TYPE_ALBUM:I

    sget v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_PRECOND_THUMB:I

    invoke-virtual {p0, v2, v3, v6}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->requestAnimation(IIZ)V

    goto :goto_0
.end method

.method private switchKeyboardFocus(Z)V
    .locals 4
    .param p1, "toRight"    # Z

    .prologue
    const/4 v3, 0x0

    .line 2371
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mKeyboardRightFocused:Z

    .line 2372
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mKeyboardRightFocused:Z

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPressedLeft:Z

    .line 2373
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mKeyboardRightFocused:Z

    if-eqz v2, :cond_2

    .line 2374
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemFocused:I

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2375
    .local v0, "focusedIndex":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iput v0, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemFocused:I

    .line 2376
    const/4 v1, 0x1

    .line 2382
    .local v1, "focusedSide":I
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    if-eqz v2, :cond_0

    .line 2383
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-interface {v2, v3, v0, v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;->onFocusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    .line 2385
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemFocused:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->update(I)V

    .line 2386
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemFocused:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->update(I)V

    .line 2387
    return-void

    .end local v0    # "focusedIndex":I
    .end local v1    # "focusedSide":I
    :cond_1
    move v2, v3

    .line 2372
    goto :goto_0

    .line 2378
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemFocused:I

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2379
    .restart local v0    # "focusedIndex":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iput v0, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemFocused:I

    .line 2380
    const/4 v1, 0x0

    .restart local v1    # "focusedSide":I
    goto :goto_1
.end method


# virtual methods
.method public alignBgPosition()V
    .locals 11

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    .line 3579
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v7, v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumBackgroundWidth(I)F

    move-result v4

    .line 3580
    .local v4, "width":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v7, v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumDividerHeight(I)F

    move-result v0

    .line 3581
    .local v0, "height":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v7, :cond_0

    .line 3582
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v7, v4, v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setSize(FF)V

    .line 3584
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v1, v7, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemStartX:F

    .line 3587
    .local v1, "sx":F
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsFixedAlbumAtTop:Z

    if-eqz v7, :cond_1

    .line 3588
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    div-float/2addr v7, v10

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v8

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemH:F

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumTopPadding(I)F

    move-result v8

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxHeight(I)F

    move-result v8

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxYOffset(I)F

    move-result v8

    sub-float v2, v7, v8

    .line 3598
    .local v2, "sy":F
    :goto_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getDistance()F

    move-result v7

    neg-float v7, v7

    const/high16 v8, 0x3f800000    # 1.0f

    add-float v3, v7, v8

    .line 3600
    .local v3, "sz":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v7, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 3602
    .end local v1    # "sx":F
    .end local v2    # "sy":F
    .end local v3    # "sz":F
    :cond_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v7, v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumBackgroundWidth(I)F

    move-result v4

    .line 3603
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v8

    sub-float v0, v7, v8

    .line 3604
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidthSpace:F

    neg-float v7, v7

    div-float/2addr v7, v10

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumBackgroundWidth(I)F

    move-result v8

    div-float/2addr v8, v10

    add-float v5, v7, v8

    .line 3605
    .local v5, "x":F
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    div-float/2addr v7, v10

    div-float v8, v0, v10

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v8

    sub-float v6, v7, v8

    .line 3607
    .local v6, "y":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    invoke-virtual {v7, v4, v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->setSize(FF)V

    .line 3608
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getDistance()F

    move-result v8

    neg-float v8, v8

    invoke-virtual {v7, v5, v6, v8}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->setPos(FFF)V

    .line 3609
    return-void

    .line 3593
    .end local v5    # "x":F
    .end local v6    # "y":F
    .restart local v1    # "sx":F
    :cond_1
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    neg-float v7, v7

    div-float/2addr v7, v10

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemH:F

    add-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumTopPadding(I)F

    move-result v8

    add-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxHeight(I)F

    move-result v8

    add-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxYOffset(I)F

    move-result v8

    add-float v2, v7, v8

    .restart local v2    # "sy":F
    goto :goto_0
.end method

.method protected createThumbItems()[Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .locals 9

    .prologue
    const/16 v4, 0x140

    const/high16 v2, 0x3f800000    # 1.0f

    .line 518
    const-string v1, "GlPhotoView"

    const-string v3, "createThumbItems"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->THUMB_ITEM_MAX:I

    new-array v6, v1, [Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 522
    .local v6, "glThumbSet":[Lcom/sec/samsung/gallery/glview/GlThumbObject;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->THUMB_ITEM_MAX:I

    if-ge v7, v1, :cond_1

    .line 523
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    move v3, v2

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V

    .line 524
    .local v0, "glThumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    aput-object v0, v6, v7

    .line 525
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerItemMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 526
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerItemClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)V

    .line 527
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerItemLongClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setLongClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;)V

    .line 528
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerHover:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setHoverListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;)V

    .line 529
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerGenericMotion:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setGenericMotionListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;)V

    .line 530
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerDrop:Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setDragListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;)V

    .line 532
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipEnable:Z

    if-eqz v1, :cond_0

    .line 533
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipEnable:Z

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setClippingEnabled(Z)V

    .line 534
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v3, v5, v8}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setClipRect(IIII)V

    .line 522
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 538
    .end local v0    # "glThumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_1
    return-object v6
.end method

.method protected doResize()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2277
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    if-eqz v1, :cond_0

    .line 2278
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    sget v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SCROLL_STATE_IDLE:I

    invoke-interface {v1, p0, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;->onScrollStateChanged(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V

    .line 2279
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    if-eqz v1, :cond_1

    .line 2280
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    sget v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SCROLL_STATE_IDLE:I

    invoke-interface {v1, p0, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;->onScrollStateChanged(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V

    .line 2281
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    if-eqz v1, :cond_2

    .line 2282
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 2283
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mObjEstimatorThm:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getCurrentInfo(Lcom/sec/samsung/gallery/glview/GlScroller;Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;)Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    .line 2284
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    if-nez v1, :cond_5

    .line 2285
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->removeAllObject(Lcom/sec/samsung/gallery/glview/GlScroller;)V

    .line 2286
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mLockPos:Z

    .line 2292
    :goto_0
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-eqz v1, :cond_6

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    add-int/lit8 v0, v1, -0x1

    .line 2293
    .local v0, "colcnt":I
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    if-eqz v1, :cond_3

    .line 2294
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWideMode:Z

    if-eqz v1, :cond_7

    .line 2295
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-interface {v1, v2, v3, v0}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;->onStatusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    .line 2299
    :cond_3
    :goto_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->createThumbItems()[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 2300
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->reset()V

    .line 2301
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->register([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 2302
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iput v4, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScrollStep:I

    .line 2303
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->loadThumbs()V

    .line 2304
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->moveToLast()V

    .line 2307
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->hideScrollBarImmediately()V

    .line 2308
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    if-eqz v1, :cond_4

    .line 2309
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->convAnimEx()V

    .line 2311
    :cond_4
    return-void

    .line 2288
    .end local v0    # "colcnt":I
    :cond_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->removeUnusedObject(Lcom/sec/samsung/gallery/glview/GlScroller;)V

    .line 2289
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScrollStep:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemHVisibleCnt:I

    mul-int/2addr v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mItemFocused:I

    .line 2290
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mLockPos:Z

    goto :goto_0

    .line 2292
    :cond_6
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    goto :goto_1

    .line 2297
    .restart local v0    # "colcnt":I
    :cond_7
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3, v0}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;->onStatusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    goto :goto_2
.end method

.method protected getColCntThreshold()[F
    .locals 2

    .prologue
    .line 3996
    const/4 v1, 0x4

    new-array v0, v1, [F

    fill-array-data v0, :array_0

    .line 3997
    .local v0, "threshold":[F
    return-object v0

    .line 3996
    nop

    :array_0
    .array-data 4
        0x3e4ccccd    # 0.2f
        0x3ecccccd    # 0.4f
        0x3f19999a    # 0.6f
        0x3f4ccccd    # 0.8f
    .end array-data
.end method

.method public getFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 2572
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPressedLeft:Z

    if-eqz v0, :cond_0

    .line 2573
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleFirst:I

    .line 2575
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mVisibleFirst:I

    goto :goto_0
.end method

.method public getItemViewPos(I)I
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 2593
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlPos;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/GlPos;-><init>()V

    .line 2594
    .local v0, "pos":Lcom/sec/android/gallery3d/glcore/GlPos;
    new-instance v2, Lcom/sec/android/gallery3d/glcore/GlPos2D;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/glcore/GlPos2D;-><init>()V

    .line 2596
    .local v2, "viewPos":Lcom/sec/android/gallery3d/glcore/GlPos2D;
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->getThumbByIndex(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 2597
    .local v1, "thumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    if-eqz v1, :cond_0

    .line 2598
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getX()F

    move-result v3

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getY()F

    move-result v4

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getZ()F

    move-result v5

    invoke-virtual {v0, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlPos;->set(FFF)V

    .line 2600
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlConfig:Lcom/sec/android/gallery3d/glcore/GlConfig;

    sget v4, Lcom/sec/android/gallery3d/glcore/GlConfig;->CRIT_LEFT_TOP:I

    invoke-virtual {v3, v2, v0, v4}, Lcom/sec/android/gallery3d/glcore/GlConfig;->getViewPos(Lcom/sec/android/gallery3d/glcore/GlPos2D;Lcom/sec/android/gallery3d/glcore/GlPos;I)V

    .line 2601
    iget v3, v2, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mX:F

    float-to-int v3, v3

    const v4, 0xffff

    and-int/2addr v3, v4

    iget v4, v2, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mY:F

    float-to-int v4, v4

    shl-int/lit8 v4, v4, 0x10

    const/high16 v5, -0x10000

    and-int/2addr v4, v5

    or-int/2addr v3, v4

    return v3
.end method

.method public getLastVisiblePosition()I
    .locals 1

    .prologue
    .line 2580
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPressedLeft:Z

    if-eqz v0, :cond_0

    .line 2581
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleLast:I

    .line 2583
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mVisibleLast:I

    goto :goto_0
.end method

.method public getLinePatternPeriod()I
    .locals 1

    .prologue
    .line 2260
    const/4 v0, 0x1

    return v0
.end method

.method public getMaxVisibleCount()I
    .locals 1

    .prologue
    .line 2588
    sget v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->THUMB_ITEM_MAX:I

    return v0
.end method

.method public getObjectIndex(I)Lcom/sec/android/gallery3d/glcore/GlObject;
    .locals 6
    .param p1, "index"    # I

    .prologue
    const/4 v3, 0x0

    .line 681
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    if-nez v4, :cond_2

    :cond_0
    move-object v1, v3

    .line 690
    :cond_1
    :goto_0
    return-object v1

    .line 684
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemVisibleCnt:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    array-length v5, v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 685
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_3

    .line 686
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v1, v4, v0

    .line 687
    .local v1, "obj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    iget v4, v1, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    if-eq v4, p1, :cond_1

    .line 685
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v1    # "obj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_3
    move-object v1, v3

    .line 690
    goto :goto_0
.end method

.method public getObjectIndex([Lcom/sec/samsung/gallery/glview/GlBaseObject;I)Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .locals 4
    .param p1, "objSet"    # [Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .param p2, "index"    # I

    .prologue
    const/4 v2, 0x0

    .line 707
    if-nez p1, :cond_1

    move-object v0, v2

    .line 715
    :cond_0
    :goto_0
    return-object v0

    .line 710
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, p1

    if-ge v1, v3, :cond_2

    .line 711
    aget-object v0, p1, v1

    .line 712
    .local v0, "glObject":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    iget v3, v0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-eq v3, p2, :cond_0

    .line 710
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v0    # "glObject":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_2
    move-object v0, v2

    .line 715
    goto :goto_0
.end method

.method public getObjectIndexLeft(I)Lcom/sec/android/gallery3d/glcore/GlObject;
    .locals 6
    .param p1, "index"    # I

    .prologue
    const/4 v3, 0x0

    .line 694
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    if-nez v4, :cond_2

    :cond_0
    move-object v0, v3

    .line 703
    :cond_1
    :goto_0
    return-object v0

    .line 697
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemVisibleCnt:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    array-length v5, v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 698
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_3

    .line 699
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    aget-object v0, v4, v1

    .line 700
    .local v0, "glObject":Lcom/sec/samsung/gallery/glview/GlSplitObject;
    iget v4, v0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mIndex:I

    if-eq v4, p1, :cond_1

    .line 698
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v0    # "glObject":Lcom/sec/samsung/gallery/glview/GlSplitObject;
    :cond_3
    move-object v0, v3

    .line 703
    goto :goto_0
.end method

.method public getSelectedItemPosition()I
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScrollStep:I

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemHVisibleCnt:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mItemFocused:I

    .line 283
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mItemFocused:I

    return v0
.end method

.method public getSplitScroll()F
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScroll:F

    .line 269
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSplitSx:F

    goto :goto_0
.end method

.method public getThumbScroll()F
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScroll:F

    .line 276
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSx:F

    goto :goto_0
.end method

.method public getThumbnailViewMode()I
    .locals 1

    .prologue
    .line 2413
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    return v0
.end method

.method public hideFixedAlbum()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 2909
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsFixedAlbumVisible:Z

    .line 2910
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(Z)V

    .line 2911
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setVisibility(Z)V

    .line 2913
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRectForAlbums:Landroid/graphics/Rect;

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidth:I

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeight:I

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mNewAlbumMode:Z

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->newAlbumHeaderHeight:F

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v9

    div-float/2addr v4, v9

    float-to-int v4, v4

    :goto_0
    sub-int v4, v8, v4

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeightPixel()I

    move-result v8

    sub-int/2addr v4, v8

    invoke-virtual {v6, v5, v5, v7, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2917
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v4, :cond_1

    .line 2918
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .local v0, "arr$":[Lcom/sec/samsung/gallery/glview/GlSplitObject;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 2919
    .local v3, "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/glcore/GlObject;->setClippingEnabled(Z)V

    .line 2920
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRectForAlbums:Landroid/graphics/Rect;

    iput-object v4, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mClipRect:Landroid/graphics/Rect;

    .line 2918
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v0    # "arr$":[Lcom/sec/samsung/gallery/glview/GlSplitObject;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_0
    move v4, v5

    .line 2913
    goto :goto_0

    .line 2923
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRectForAlbums:Landroid/graphics/Rect;

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setArrowClipRect(Landroid/graphics/Rect;)V

    .line 2924
    return-void
.end method

.method public isDragEnabled()Z
    .locals 1

    .prologue
    .line 4019
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsDragEnabled:Z

    return v0
.end method

.method protected isScreenLocked()Z
    .locals 1

    .prologue
    .line 2813
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderSpreadAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderSpreadAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2814
    const/4 v0, 0x1

    .line 2815
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->isScreenLocked()Z

    move-result v0

    goto :goto_0
.end method

.method public localDrag(I)V
    .locals 3
    .param p1, "mSelectedAlbumItemIndex"    # I

    .prologue
    .line 1410
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    if-eqz v0, :cond_0

    .line 1411
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v2, 0x4

    invoke-interface {v0, v1, v2, p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;->onStatusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    .line 1414
    :cond_0
    return-void
.end method

.method protected onCommand(ILjava/lang/Object;III)V
    .locals 9
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1278
    if-ne p1, v2, :cond_2

    .line 1279
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPressedLeft:Z

    if-eqz v1, :cond_1

    .line 1280
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    if-eqz v1, :cond_0

    .line 1281
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    sget v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SCROLL_STATE_IDLE:I

    invoke-interface {v1, p0, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;->onScrollStateChanged(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V

    .line 1336
    :cond_0
    :goto_0
    return-void

    .line 1283
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    if-eqz v1, :cond_0

    .line 1284
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    sget v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SCROLL_STATE_IDLE:I

    invoke-interface {v1, p0, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;->onScrollStateChanged(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V

    goto :goto_0

    .line 1286
    :cond_2
    const/4 v1, 0x3

    if-ne p1, v1, :cond_3

    .line 1287
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->updateThumbs()V

    .line 1288
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->doResize()V

    goto :goto_0

    .line 1289
    :cond_3
    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_NOTIFY_FOLDER_UPDATE:I

    if-ne p1, v1, :cond_5

    .line 1290
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimType:I

    sget v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_TYPE_VIEW:I

    if-ne v1, v2, :cond_4

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimStatus:I

    sget v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_STATUS_RUN:I

    if-ne v1, v2, :cond_4

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnConvAnim:Z

    if-eqz v1, :cond_4

    .line 1291
    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_NOTIFY_FOLDER_UPDATE:I

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->removeCommand(I)V

    .line 1292
    sget v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_NOTIFY_FOLDER_UPDATE:I

    const-wide/16 v6, 0xc8

    move-object v1, p0

    move v4, v3

    move v5, v3

    invoke-virtual/range {v1 .. v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommandDelayed(IIIIJ)V

    .line 1293
    const-string v1, "GlPhotoView"

    const-string v2, "onCommand: CMD_NOTIFY_FOLDER_UPDATE: while album animation running, delay folder update noti"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1296
    :cond_4
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->reloadFolder()V

    goto :goto_0

    .line 1297
    :cond_5
    if-ne p1, v4, :cond_6

    .line 1298
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->loadThumbs()V

    goto :goto_0

    .line 1299
    :cond_6
    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_REQ_LOAD_FOLDER:I

    if-ne p1, v1, :cond_7

    .line 1300
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->loadFolders()V

    goto :goto_0

    .line 1301
    :cond_7
    const/4 v1, 0x4

    if-ne p1, v1, :cond_8

    .line 1302
    invoke-virtual {p0, p3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->updateItem(I)V

    goto :goto_0

    .line 1303
    :cond_8
    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_UPDATE_FOLDER:I

    if-ne p1, v1, :cond_c

    .line 1304
    if-ne p4, v2, :cond_a

    .line 1305
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v8, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleFirst:I

    .line 1306
    .local v8, "prevVisibleFirst":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v0

    .line 1307
    .local v0, "count":I
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v1, p3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->calulateVisibleRange(I)V

    .line 1308
    if-lez v0, :cond_0

    .line 1309
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleFirst:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleLast:I

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->updateVisiableRange(II)V

    .line 1311
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleFirst:I

    if-eq v8, v1, :cond_0

    .line 1312
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleFirst:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemCount:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->resetSet(IIZ)V

    goto/16 :goto_0

    .end local v0    # "count":I
    :cond_9
    move v0, v3

    .line 1306
    goto :goto_1

    .line 1315
    .end local v8    # "prevVisibleFirst":I
    :cond_a
    if-ne p4, v4, :cond_b

    .line 1316
    iput p3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    .line 1317
    invoke-direct {p0, p3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->switchAlbum(I)V

    .line 1318
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 1319
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleFirst:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleLast:I

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->updateVisiableRange(II)V

    goto/16 :goto_0

    .line 1323
    :cond_b
    invoke-virtual {p0, p3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->updateFolderItem(I)V

    goto/16 :goto_0

    .line 1325
    :cond_c
    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_SWITCH_ALBUM:I

    if-ne p1, v1, :cond_d

    .line 1326
    invoke-direct {p0, p3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->switchAlbum(I)V

    goto/16 :goto_0

    .line 1327
    :cond_d
    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_CONV_ALBUM:I

    if-ne p1, v1, :cond_e

    .line 1328
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->convAnim()V

    goto/16 :goto_0

    .line 1329
    :cond_e
    const/4 v1, 0x6

    if-ne p1, v1, :cond_f

    .line 1330
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->doResize()V

    goto/16 :goto_0

    .line 1331
    :cond_f
    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_APPLY_POS:I

    if-ne p1, v1, :cond_10

    .line 1332
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->applyThumbPosition()V

    goto/16 :goto_0

    .line 1334
    :cond_10
    invoke-super/range {p0 .. p5}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onCommand(ILjava/lang/Object;III)V

    goto/16 :goto_0
.end method

.method protected onCreate()V
    .locals 13

    .prologue
    const v12, -0x777778

    const-wide/16 v10, 0xbb8

    const v9, 0x3f19999a    # 0.6f

    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 419
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setClearByColor(Z)V

    .line 420
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getGlRoot()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGlConfig()Lcom/sec/android/gallery3d/glcore/GlConfig;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlConfig:Lcom/sec/android/gallery3d/glcore/GlConfig;

    .line 421
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setPreviousInfo()V

    .line 422
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->resetLayout()V

    .line 423
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 424
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidthSpace:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getHeight()I

    move-result v7

    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/sec/samsung/gallery/util/DimensionUtil;->setDimensionUtil(FFII)V

    .line 426
    const/4 v1, 0x0

    .line 427
    .local v1, "mode":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 428
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsNoSplitMode:Z

    if-eqz v2, :cond_4

    .line 429
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    const-string v4, "eventViewMode"

    invoke-static {v2, v4, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    .line 433
    :goto_0
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setThumbnailViewMode(I)V

    .line 434
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    const/4 v4, 0x2

    if-ge v2, v4, :cond_0

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-eqz v2, :cond_5

    .line 435
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setExpanded(ZZ)V

    .line 439
    :goto_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getHorizontalCount()V

    .line 441
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    .line 442
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v2, v10, v11}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setDuration(J)V

    .line 444
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 445
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v2, v9, v9}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setFactors(FF)V

    .line 451
    :goto_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const v4, 0x465ac000    # 14000.0f

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMaxSpeed(F)V

    .line 452
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mlistenerItems:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setBoundaryAnimationListener(Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;)V

    .line 453
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 455
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    .line 456
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v2, v10, v11}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setDuration(J)V

    .line 457
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mlistenerFolders:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setBoundaryAnimationListener(Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;)V

    .line 458
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 460
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderChgAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;

    .line 461
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderChgAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;

    invoke-virtual {v2, v10, v11}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->setDuration(J)V

    .line 462
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderChgAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 464
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    invoke-direct {v2}, Lcom/sec/samsung/gallery/glview/GlDragAnimation;-><init>()V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDragAnim:Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    .line 466
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->initArrowObject()V

    .line 468
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->freeFolderItems()V

    .line 469
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->createAlbumItems()[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .line 470
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    .line 471
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->enableScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;)Lcom/sec/samsung/gallery/glview/GlScrollBar;

    .line 472
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->setScrollBarEnable(Z)V

    .line 473
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v2

    invoke-virtual {v2, v12}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->setCanvasColor(I)V

    .line 475
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->reset()V

    .line 476
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->register([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 477
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    iput v4, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemFocused:I

    .line 478
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iput-boolean v3, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mIsFocusInited:Z

    .line 479
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v0

    .line 480
    .local v0, "albumCount":I
    :goto_3
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->loadFolders()V

    .line 482
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    invoke-direct {v2, p0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    .line 483
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-eqz v2, :cond_9

    .line 484
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->setVisibility(Z)V

    .line 491
    :cond_1
    :goto_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->moveToFirst()V

    .line 493
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-direct {v2, p0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/samsung/gallery/glview/GlAbsListView;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    .line 494
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->enableScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;)Lcom/sec/samsung/gallery/glview/GlScrollBar;

    .line 495
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v2

    const/high16 v4, 0x41200000    # 10.0f

    invoke-virtual {v2, v8, v8, v4}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->setOffset(FFF)V

    .line 496
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v2

    invoke-virtual {v2, v12}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->setCanvasColor(I)V

    .line 497
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iput-boolean v3, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mIsFocusInited:Z

    .line 498
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->reset()V

    .line 500
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFixScrollingAlbum:Z

    if-eqz v2, :cond_2

    .line 501
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->initFixedAlbum()V

    .line 504
    :cond_2
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;

    invoke-direct {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;-><init>()V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderSpreadAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;

    .line 506
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    .line 507
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/samsung/gallery/glview/GlPhotoView$1;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDragInfo:Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;

    .line 508
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDragInfo:Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->reset()V

    .line 509
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->alignBgPosition()V

    .line 510
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v2, :cond_3

    if-lez v0, :cond_3

    .line 511
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleFirst:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleLast:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->updateVisiableRange(II)V

    .line 513
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;->initMultiCore(Landroid/content/Context;)V

    .line 514
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getGlRoot()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 515
    return-void

    .line 431
    .end local v0    # "albumCount":I
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSPKeyString:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewDefaultMode()I

    move-result v5

    invoke-static {v2, v4, v5}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    goto/16 :goto_0

    .line 437
    :cond_5
    invoke-virtual {p0, v3, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setExpanded(ZZ)V

    goto/16 :goto_1

    .line 446
    :cond_6
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isOverHD(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 447
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const v4, 0x3ecccccd    # 0.4f

    const v5, 0x3f666666    # 0.9f

    invoke-virtual {v2, v4, v5}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setFactors(FF)V

    goto/16 :goto_2

    .line 449
    :cond_7
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const v4, 0x3e99999a    # 0.3f

    invoke-virtual {v2, v9, v4}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setFactors(FF)V

    goto/16 :goto_2

    :cond_8
    move v0, v3

    .line 479
    goto/16 :goto_3

    .line 485
    .restart local v0    # "albumCount":I
    :cond_9
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimType:I

    sget v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_TYPE_NONE:I

    if-eq v2, v4, :cond_1

    .line 486
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    invoke-virtual {v2, v8}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->setAlpha(F)V

    .line 487
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    const-wide/16 v4, 0x1f4

    const-wide/16 v6, 0xc8

    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->fadeIn(JJ)V

    .line 488
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v2, v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlpha(F)V

    .line 489
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    const-wide/16 v4, 0x1f4

    const-wide/16 v6, 0xc8

    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->fadeIn(JJ)V

    goto/16 :goto_4
.end method

.method protected onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 358
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlags:I

    sget v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SAVE_OBJINFO_FORANIM:I

    and-int/2addr v2, v3

    if-lez v2, :cond_7

    .line 359
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->pushCurrentListInfo()V

    .line 364
    :goto_0
    iput-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlFolderObject:[Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .line 365
    iput-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 367
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v2, :cond_0

    .line 368
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->remove()V

    .line 371
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    if-eqz v2, :cond_1

    .line 372
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->remove()V

    .line 375
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v2, :cond_2

    .line 376
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->remove()V

    .line 379
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v2, :cond_3

    .line 380
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->remove()V

    .line 383
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mBgExpandAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;

    if-eqz v2, :cond_4

    .line 384
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mBgExpandAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 387
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;)Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v0

    .line 388
    .local v0, "sba":Lcom/sec/samsung/gallery/glview/GlScrollBar;
    if-eqz v0, :cond_5

    .line 389
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->remove()V

    .line 390
    :cond_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;)Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v1

    .line 391
    .local v1, "sbt":Lcom/sec/samsung/gallery/glview/GlScrollBar;
    if-eqz v1, :cond_6

    .line 392
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->remove()V

    .line 394
    :cond_6
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V

    .line 395
    const/4 v2, 0x0

    invoke-virtual {p0, v4, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;I)V

    .line 396
    return-void

    .line 361
    .end local v0    # "sba":Lcom/sec/samsung/gallery/glview/GlScrollBar;
    .end local v1    # "sbt":Lcom/sec/samsung/gallery/glview/GlScrollBar;
    :cond_7
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->removeAllObject(Lcom/sec/samsung/gallery/glview/GlScroller;)V

    .line 362
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->removeAllObject(Lcom/sec/samsung/gallery/glview/GlScroller;)V

    goto :goto_0
.end method

.method protected onDrag(IIZ)Z
    .locals 7
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "onDragging"    # Z

    .prologue
    const/4 v6, 0x1

    .line 1548
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsPressed:Z

    .line 1550
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->isScreenLocked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1561
    :goto_0
    return v6

    .line 1554
    :cond_0
    if-eqz p3, :cond_1

    .line 1555
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidthSpace:F

    int-to-float v3, p1

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidth:I

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 1556
    .local v0, "sx":F
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    int-to-float v3, p2

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeight:I

    int-to-float v3, v3

    div-float v1, v2, v3

    .line 1557
    .local v1, "sy":F
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHndDispmnt:I

    neg-float v4, v1

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v0, v4, v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setPos(IFFF)V

    .line 1558
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDragAnim:Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->setTargetPos(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    .line 1560
    .end local v0    # "sx":F
    .end local v1    # "sy":F
    :cond_1
    invoke-direct {p0, p3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->controlByDrag(Z)Z

    goto :goto_0
.end method

.method public onDragEvent(Landroid/view/DragEvent;)Z
    .locals 11
    .param p1, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v5, 0x0

    .line 1771
    :try_start_0
    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v0

    .line 1772
    .local v0, "clipData":Landroid/content/ClipData;
    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v6

    const-string v7, "cropUri"

    invoke-virtual {v6, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1773
    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    move-result v1

    .line 1774
    .local v1, "count":I
    if-lez v1, :cond_0

    .line 1775
    const/4 v3, 0x0

    .line 1776
    .local v3, "index":I
    invoke-virtual {v0, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    .line 1777
    .local v4, "item":Landroid/content/ClipData$Item;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnDropItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnDropItemClickListener;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v8, 0x0

    const/4 v9, -0x1

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v10

    invoke-interface {v6, v7, v8, v9, v10}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnDropItemClickListener;->onDropItemClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;ILandroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1778
    const/4 v5, 0x1

    .line 1784
    .end local v0    # "clipData":Landroid/content/ClipData;
    .end local v1    # "count":I
    .end local v3    # "index":I
    .end local v4    # "item":Landroid/content/ClipData$Item;
    :cond_0
    :goto_0
    return v5

    .line 1781
    :catch_0
    move-exception v2

    .line 1782
    .local v2, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method protected onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x3

    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1642
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->isSlideShowMode:Z

    if-eqz v3, :cond_1

    move v4, v5

    .line 1733
    :cond_0
    :goto_0
    return v4

    .line 1644
    :cond_1
    const/16 v3, 0xa8

    if-eq p1, v3, :cond_2

    const/16 v3, 0xa9

    if-eq p1, v3, :cond_2

    const/16 v3, 0x118

    if-eq p1, v3, :cond_2

    const/16 v3, 0x117

    if-ne p1, v3, :cond_6

    :cond_2
    move v1, v4

    .line 1646
    .local v1, "isZoomKey":Z
    :goto_1
    if-ne p1, v6, :cond_7

    move v0, v4

    .line 1647
    .local v0, "isBackKey":Z
    :goto_2
    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->isScreenLocked()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    if-nez v3, :cond_0

    .line 1651
    :cond_3
    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->isScreenLocked()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1654
    :cond_4
    const/16 v3, 0x42

    if-eq p1, v3, :cond_5

    const/16 v3, 0x17

    if-ne p1, v3, :cond_8

    .line 1655
    :cond_5
    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    goto :goto_0

    .end local v0    # "isBackKey":Z
    .end local v1    # "isZoomKey":Z
    :cond_6
    move v1, v5

    .line 1644
    goto :goto_1

    .restart local v1    # "isZoomKey":Z
    :cond_7
    move v0, v5

    .line 1646
    goto :goto_2

    .line 1659
    .restart local v0    # "isBackKey":Z
    :cond_8
    const/16 v3, 0x15

    if-eq p1, v3, :cond_9

    const/16 v3, 0x16

    if-eq p1, v3, :cond_9

    const/16 v3, 0x13

    if-eq p1, v3, :cond_9

    const/16 v3, 0x14

    if-eq p1, v3, :cond_9

    const/16 v3, 0x17

    if-eq p1, v3, :cond_9

    const/16 v3, 0x42

    if-eq p1, v3, :cond_9

    if-ne p1, v6, :cond_b

    .line 1662
    :cond_9
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    if-eqz v3, :cond_a

    .line 1663
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 1664
    :cond_a
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    if-eqz v3, :cond_b

    .line 1665
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 1667
    :cond_b
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    if-eqz v3, :cond_0

    .line 1671
    const/16 v3, 0x15

    if-eq p1, v3, :cond_c

    const/16 v3, 0x16

    if-eq p1, v3, :cond_c

    const/16 v3, 0x13

    if-eq p1, v3, :cond_c

    const/16 v3, 0x14

    if-ne p1, v3, :cond_d

    .line 1673
    :cond_c
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mKeyboardRightFocused:Z

    if-nez v3, :cond_e

    move v3, v4

    :goto_3
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPressedLeft:Z

    .line 1676
    :cond_d
    sparse-switch p1, :sswitch_data_0

    .line 1729
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnKeyListenner:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;

    if-eqz v3, :cond_18

    .line 1730
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnKeyListenner:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    invoke-interface {v3, p1, v4}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;->onKeyEvent(II)Z

    move-result v4

    goto/16 :goto_0

    :cond_e
    move v3, v5

    .line 1673
    goto :goto_3

    .line 1678
    :sswitch_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemFocused:I

    if-eqz v3, :cond_f

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->jumpTo(I)Z

    move-result v3

    if-nez v3, :cond_10

    :cond_f
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mKeyboardRightFocused:Z

    if-eqz v3, :cond_10

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-nez v3, :cond_10

    .line 1680
    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->switchKeyboardFocus(Z)V

    .line 1682
    :cond_10
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1685
    :sswitch_1
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mKeyboardRightFocused:Z

    if-nez v3, :cond_12

    .line 1686
    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->switchKeyboardFocus(Z)V

    .line 1690
    :cond_11
    :goto_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1687
    :cond_12
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->lastColFocused()Z

    move-result v3

    if-nez v3, :cond_11

    .line 1688
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v3, v8}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->jumpTo(I)Z

    goto :goto_4

    .line 1693
    :sswitch_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 1694
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mKeyboardRightFocused:Z

    if-eqz v3, :cond_14

    .line 1695
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemFocused:I

    if-nez v3, :cond_13

    .line 1696
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    const/4 v4, -0x1

    iput v4, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemFocused:I

    move v4, v5

    .line 1697
    goto/16 :goto_0

    .line 1699
    :cond_13
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v3, v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->jumpTo(I)Z

    move-result v4

    goto/16 :goto_0

    .line 1701
    :cond_14
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v3, v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->moveTo(I)Z

    move-result v2

    .line 1702
    .local v2, "result":Z
    if-nez v2, :cond_15

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    if-eqz v3, :cond_15

    .line 1703
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-interface {v3, v4, v5, v8}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;->onFocusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    :cond_15
    move v4, v2

    .line 1705
    goto/16 :goto_0

    .line 1708
    .end local v2    # "result":Z
    :sswitch_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 1709
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mKeyboardRightFocused:Z

    if-eqz v3, :cond_16

    .line 1710
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->jumpTo(I)Z

    move-result v4

    goto/16 :goto_0

    .line 1712
    :cond_16
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->moveTo(I)Z

    move-result v4

    goto/16 :goto_0

    .line 1716
    :sswitch_4
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    if-le v3, v7, :cond_0

    .line 1717
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    add-int/lit8 v3, v3, -0x1

    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setThumbnailViewMode(I)V

    .line 1718
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setColumn()V

    goto/16 :goto_0

    .line 1723
    :sswitch_5
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-nez v3, :cond_17

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-nez v3, :cond_0

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    if-ge v3, v4, :cond_0

    .line 1724
    :cond_17
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    add-int/lit8 v3, v3, 0x1

    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setThumbnailViewMode(I)V

    .line 1725
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setColumn()V

    goto/16 :goto_0

    :cond_18
    move v4, v5

    .line 1733
    goto/16 :goto_0

    .line 1676
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_2
        0x14 -> :sswitch_3
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0xa8 -> :sswitch_4
        0xa9 -> :sswitch_5
        0x117 -> :sswitch_5
        0x118 -> :sswitch_4
    .end sparse-switch
.end method

.method protected onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1789
    const/16 v0, 0xa8

    if-eq p1, v0, :cond_0

    const/16 v0, 0xa9

    if-eq p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->isScreenLocked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1791
    :cond_0
    const/4 v0, 0x1

    .line 1796
    :goto_0
    return v0

    .line 1793
    :cond_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mKeyboardRightFocused:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnKeyListenner:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;

    if-eqz v0, :cond_2

    .line 1794
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnKeyListenner:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;

    const/16 v1, 0x80

    invoke-interface {v0, p1, v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;->onKeyEvent(II)Z

    move-result v0

    goto :goto_0

    .line 1796
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v6, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1738
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->isSlideShowMode:Z

    if-eqz v3, :cond_1

    .line 1765
    :cond_0
    :goto_0
    return v1

    .line 1740
    :cond_1
    const/16 v3, 0xa8

    if-eq p1, v3, :cond_2

    const/16 v3, 0xa9

    if-eq p1, v3, :cond_2

    const/16 v3, 0x118

    if-eq p1, v3, :cond_2

    const/16 v3, 0x117

    if-eq p1, v3, :cond_2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->isScreenLocked()Z

    move-result v3

    if-eqz v3, :cond_3

    if-eq p1, v6, :cond_3

    :cond_2
    move v1, v2

    .line 1743
    goto :goto_0

    .line 1745
    :cond_3
    const/16 v3, 0x42

    if-eq p1, v3, :cond_4

    const/16 v3, 0x17

    if-ne p1, v3, :cond_7

    .line 1746
    :cond_4
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mKeyboardRightFocused:Z

    if-eqz v3, :cond_6

    .line 1747
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 1748
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemFocused:I

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mItemFocused:I

    .line 1749
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mItemFocused:I

    if-ltz v3, :cond_5

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mItemFocused:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemCount:I

    if-ge v3, v4, :cond_5

    .line 1750
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mItemFocused:I

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->getThumbByIndex(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v0

    .line 1751
    .local v0, "object":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    if-eqz v3, :cond_5

    if-eqz v0, :cond_5

    .line 1752
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v4

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mItemFocused:I

    invoke-interface {v3, p0, v4, v5}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 1753
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mItemFocused:I

    invoke-virtual {p0, v6, v3, v1, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    .end local v0    # "object":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_5
    :goto_1
    move v1, v2

    .line 1763
    goto :goto_0

    .line 1757
    :cond_6
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemFocused:I

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    .line 1758
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->getThumbByIndex(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderObjecSel:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eq v3, v4, :cond_0

    .line 1761
    sget v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_SWITCH_ALBUM:I

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    invoke-virtual {p0, v3, v4, v1, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    goto :goto_1

    .line 1765
    :cond_7
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method protected onMoved(II)Z
    .locals 6
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    const/16 v4, 0x2d

    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 1566
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->isScreenLocked()Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mLockFlingAtMaxZoomLevel:Z

    if-eqz v2, :cond_1

    .line 1595
    :cond_0
    :goto_0
    return v5

    .line 1570
    :cond_1
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpandable:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsCurrentExpandable:Z

    if-eqz v2, :cond_2

    .line 1571
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 1572
    .local v0, "absDx":I
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 1574
    .local v1, "absDy":I
    const/16 v2, 0x32

    if-le v1, v2, :cond_4

    .line 1575
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsCurrentExpandable:Z

    .line 1585
    .end local v0    # "absDx":I
    .end local v1    # "absDy":I
    :cond_2
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPressedLeft:Z

    if-eqz v2, :cond_6

    .line 1586
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPressedUp:Z

    if-eqz v2, :cond_0

    .line 1587
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-le v2, v4, :cond_3

    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mLClickable:Z

    .line 1588
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    neg-int v3, p2

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMovement(F)V

    goto :goto_0

    .line 1576
    .restart local v0    # "absDx":I
    .restart local v1    # "absDy":I
    :cond_4
    const/16 v2, 0x64

    if-le v0, v2, :cond_2

    .line 1577
    if-gez p1, :cond_5

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-nez v2, :cond_5

    .line 1578
    invoke-virtual {p0, v5, v5}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setExpanded(ZZ)V

    goto :goto_0

    .line 1579
    :cond_5
    if-lez p1, :cond_0

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-eqz v2, :cond_0

    .line 1580
    invoke-virtual {p0, v3, v5}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setExpanded(ZZ)V

    goto :goto_0

    .line 1591
    .end local v0    # "absDx":I
    .end local v1    # "absDy":I
    :cond_6
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-le v2, v4, :cond_7

    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mRClickable:Z

    .line 1592
    :cond_7
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderChgAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderChgAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->isRunning()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1593
    :cond_8
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    neg-int v3, p2

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMovement(F)V

    goto :goto_0
.end method

.method protected onPressed(II)Z
    .locals 6
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1504
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWideMode:Z

    if-eqz v3, :cond_4

    sget v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SPLIT_RATIO_LAND:F

    .line 1505
    .local v0, "ratio":F
    :goto_0
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsPressed:Z

    .line 1506
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsCurrentExpandable:Z

    .line 1507
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;->useMultiCore(Landroid/content/Context;)V

    .line 1509
    int-to-float v3, p1

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidth:I

    int-to-float v4, v4

    mul-float/2addr v4, v0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_6

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-nez v3, :cond_6

    .line 1510
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1511
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 1513
    :cond_0
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPressedLeft:Z

    .line 1514
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mNewAlbumMode:Z

    if-eqz v3, :cond_5

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    int-to-float v4, p2

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeight:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSeparatorY:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_5

    .line 1515
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPressedUp:Z

    .line 1519
    :goto_1
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mLClickable:Z

    .line 1520
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    if-eqz v1, :cond_1

    .line 1521
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    sget v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SCROLL_STATE_TOUCH_SCROLL:I

    invoke-interface {v1, p0, v3}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;->onScrollStateChanged(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V

    .line 1523
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->isScreenLocked()Z

    move-result v1

    if-nez v1, :cond_2

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mLClickable:Z

    :cond_2
    :goto_2
    move v1, v2

    .line 1544
    :cond_3
    return v1

    .line 1504
    .end local v0    # "ratio":F
    :cond_4
    sget v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SPLIT_RATIO_PORT:F

    goto :goto_0

    .line 1517
    .restart local v0    # "ratio":F
    :cond_5
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPressedUp:Z

    goto :goto_1

    .line 1524
    :cond_6
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderChgAnimCurrent:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderChgAnimCurrent:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->isRunning()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1527
    :cond_7
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1528
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 1530
    :cond_8
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPressedLeft:Z

    .line 1531
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mRClickable:Z

    .line 1532
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    if-eqz v1, :cond_9

    .line 1533
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    sget v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SCROLL_STATE_TOUCH_SCROLL:I

    invoke-interface {v1, p0, v3}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;->onScrollStateChanged(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V

    .line 1535
    :cond_9
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->isScreenLocked()Z

    move-result v1

    if-nez v1, :cond_2

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mRClickable:Z

    goto :goto_2
.end method

.method protected onReleased(IIII)Z
    .locals 3
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 1605
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsPressed:Z

    .line 1606
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsCurrentExpandable:Z

    .line 1607
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->isScreenLocked()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mLockFlingAtMaxZoomLevel:Z

    if-eqz v0, :cond_1

    .line 1624
    :cond_0
    :goto_0
    return v2

    .line 1610
    :cond_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPressedLeft:Z

    if-eqz v0, :cond_2

    .line 1611
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPressedUp:Z

    if-eqz v0, :cond_0

    .line 1612
    neg-int v0, p4

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setFolderFlingSpeed(I)V

    .line 1613
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 1614
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SCROLL_STATE_TOUCH_SCROLL:I

    invoke-interface {v0, p0, v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;->onScrollStateChanged(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V

    goto :goto_0

    .line 1618
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderChgAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderChgAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1619
    :cond_3
    neg-int v0, p4

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setThumbImageFlingSpeed(I)V

    .line 1620
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 1621
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->SCROLL_STATE_TOUCH_SCROLL:I

    invoke-interface {v0, p0, v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;->onScrollStateChanged(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V

    goto :goto_0
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)V
    .locals 9
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/16 v2, 0x8

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 2323
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnConvAnim:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->isScreenLocked()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPressedLeft:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsEasyMode:Z

    if-eqz v1, :cond_1

    .line 2352
    :cond_0
    :goto_0
    return-void

    .line 2326
    :cond_1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v8

    .line 2327
    .local v8, "scaleFactor":F
    const v1, 0x3f8147ae    # 1.01f

    cmpl-float v1, v8, v1

    if-lez v1, :cond_3

    .line 2328
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    const/4 v4, 0x3

    if-le v1, v4, :cond_2

    .line 2329
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setThumbnailViewMode(I)V

    .line 2339
    :cond_2
    :goto_1
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    .line 2340
    .local v0, "oldColCount":I
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getHorizontalCount()V

    .line 2341
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    if-eq v1, v0, :cond_5

    .line 2342
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mLockPos:Z

    .line 2343
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->clearBoundarEffect()V

    .line 2344
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsResizing:Z

    .line 2345
    const-string v1, "GlPhotoView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mScaleFactor= "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", newCount = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2346
    const/4 v1, 0x6

    invoke-virtual {p0, v1, v3, v3, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    goto :goto_0

    .line 2331
    .end local v0    # "oldColCount":I
    :cond_3
    const v1, 0x3f7d70a4    # 0.99f

    cmpg-float v1, v8, v1

    if-gez v1, :cond_0

    .line 2332
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    if-ge v1, v5, :cond_2

    .line 2333
    :cond_4
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setThumbnailViewMode(I)V

    goto :goto_1

    .line 2348
    .restart local v0    # "oldColCount":I
    :cond_5
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mLockFlingAtMaxZoomLevel:Z

    .line 2349
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->removeCommand(I)V

    .line 2350
    sget-wide v6, Lcom/sec/samsung/gallery/glview/GlPhotoView;->LOCK_FLING_TIMEOUT:J

    move-object v1, p0

    move v4, v3

    move v5, v3

    invoke-virtual/range {v1 .. v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommandDelayed(IIIIJ)V

    goto :goto_0
.end method

.method protected onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 1
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 2315
    const/4 v0, 0x1

    return v0
.end method

.method protected onScrolled(IIII)Z
    .locals 2
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "x"    # I
    .param p4, "y"    # I

    .prologue
    const/4 v1, 0x0

    .line 1600
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->onMoved(II)Z

    move-result v0

    invoke-virtual {p0, v1, v1, v1, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->onReleased(IIII)Z

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method protected onTouch(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, -0x1

    .line 1629
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mKeyboardRightFocused:Z

    if-nez v0, :cond_1

    .line 1630
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->onFocusChange(I)V

    .line 1631
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    if-eqz v0, :cond_0

    .line 1632
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v3, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;->onFocusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    .line 1637
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onTouch(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 1635
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->onFocusChange(I)V

    goto :goto_0
.end method

.method public refreshAllView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2566
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v1, v1, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    .line 2567
    sget v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_NOTIFY_FOLDER_UPDATE:I

    invoke-virtual {p0, v0, v1, v1, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    .line 2568
    return-void
.end method

.method public refreshView(Z)V
    .locals 2
    .param p1, "reload"    # Z

    .prologue
    const/4 v1, 0x0

    .line 2559
    if-eqz p1, :cond_0

    .line 2560
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1, v1, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    .line 2563
    :goto_0
    return-void

    .line 2562
    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v1, v1, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    goto :goto_0
.end method

.method requestAnimation(IIZ)V
    .locals 2
    .param p1, "animType"    # I
    .param p2, "condition"    # I
    .param p3, "resetCondition"    # Z

    .prologue
    .line 966
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimType:I

    .line 967
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimCondNeed:I

    .line 968
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimType:I

    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_TYPE_NONE:I

    if-ne v0, v1, :cond_1

    .line 969
    sget v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_STATUS_IDLE:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimStatus:I

    .line 972
    :goto_0
    if-eqz p3, :cond_0

    .line 973
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimCondDone:I

    .line 974
    :cond_0
    return-void

    .line 971
    :cond_1
    sget v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_STATUS_WAIT:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimStatus:I

    goto :goto_0
.end method

.method protected resetLayout()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 289
    :try_start_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->resetLayout()V

    .line 290
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidthSpace:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getHeight()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->setDimensionUtil(FFII)V

    .line 292
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-nez v2, :cond_1

    .line 293
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 295
    .local v1, "orientation":I
    if-eq v1, v7, :cond_0

    const/4 v2, 0x3

    if-ne v1, v2, :cond_a

    .line 296
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWideMode:Z

    .line 301
    .end local v1    # "orientation":I
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getHorizontalCount()V

    .line 303
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    if-eqz v2, :cond_2

    .line 304
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 306
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    if-eqz v2, :cond_3

    .line 307
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 309
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderChgAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;

    if-eqz v2, :cond_4

    .line 310
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderChgAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->stop(Z)V

    .line 312
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    if-eqz v2, :cond_5

    .line 313
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->stop()V

    .line 314
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnConvAnim:Z

    .line 317
    :cond_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderSpreadAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;

    if-eqz v2, :cond_6

    .line 318
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderSpreadAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->stop()V

    .line 320
    :cond_6
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDragInfo:Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$DragInfo;->reset()V

    .line 321
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->stopDragAnim(Z)V

    .line 323
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScrollStep:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemHVisibleCnt:I

    mul-int/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mItemFocused:I

    .line 324
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->reset()V

    .line 325
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->resetRoll()V

    .line 326
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->hideScrollBarImmediately()V

    .line 327
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->loadThumbs()V

    .line 329
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->reset()V

    .line 330
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->loadFolders(Z)V

    .line 332
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFixScrollingAlbum:Z

    if-eqz v2, :cond_7

    .line 333
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->updateFixedAlbum()V

    .line 336
    :cond_7
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->alignBgPosition()V

    .line 337
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setExpanded(ZZ)V

    .line 339
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->updateArrowObject()V

    .line 341
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v2, :cond_8

    .line 342
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v2

    if-lez v2, :cond_b

    .line 343
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleFirst:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleLast:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->updateVisiableRange(II)V

    .line 348
    :cond_8
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v2, :cond_9

    .line 349
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 354
    :cond_9
    :goto_2
    return-void

    .line 298
    .restart local v1    # "orientation":I
    :cond_a
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWideMode:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 351
    .end local v1    # "orientation":I
    :catch_0
    move-exception v0

    .line 352
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 345
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_b
    :try_start_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->updateVisiableRange(II)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V
    .locals 5
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .prologue
    const/4 v4, 0x0

    .line 1198
    const-string v1, "GlPhotoView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAdapter = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", new = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1199
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v1, :cond_1

    .line 1233
    :cond_0
    :goto_0
    return-void

    .line 1201
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 1202
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    if-eqz v1, :cond_2

    .line 1204
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->unregisterObserver(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1209
    :cond_2
    :goto_1
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .line 1210
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 1211
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v1, :cond_0

    .line 1214
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    if-nez v1, :cond_3

    .line 1215
    new-instance v1, Lcom/sec/samsung/gallery/glview/GlPhotoView$9;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$9;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    .line 1231
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->registerObserver(Lcom/sec/samsung/gallery/glview/GContentObserver;)V

    .line 1232
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v4, v4, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    goto :goto_0

    .line 1205
    :catch_0
    move-exception v0

    .line 1206
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1
.end method

.method public setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;I)V
    .locals 4
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/glview/GlBaseAdapter;
    .param p2, "option"    # I

    .prologue
    const/4 v3, 0x0

    .line 1238
    const-string v0, "GlPhotoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAdapterFolder = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", new = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1239
    if-eqz p2, :cond_1

    .line 1274
    :cond_0
    :goto_0
    return-void

    .line 1242
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 1243
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    if-eqz v0, :cond_2

    .line 1244
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->unregisterObserver(Ljava/lang/Object;)V

    .line 1246
    :cond_2
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .line 1247
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 1248
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_0

    .line 1251
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    if-nez v0, :cond_3

    .line 1252
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$10;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$10;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    .line 1272
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->registerObserver(Lcom/sec/samsung/gallery/glview/GContentObserver;)V

    .line 1273
    sget v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_NOTIFY_FOLDER_UPDATE:I

    invoke-virtual {p0, v0, v3, v3, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    goto :goto_0
.end method

.method public setAirMotionImageFling(I)V
    .locals 2
    .param p1, "speed"    # I

    .prologue
    .line 2359
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsPressed:Z

    if-nez v0, :cond_0

    .line 2360
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->startFlingtoEnd(F)V

    .line 2361
    :cond_0
    return-void
.end method

.method public setArrowClipRect(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 4001
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setClippingEnabled(Z)V

    .line 4002
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iput-object p1, v0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mClipRect:Landroid/graphics/Rect;

    .line 4003
    return-void
.end method

.method public setAttrs(II)V
    .locals 1
    .param p1, "attr"    # I
    .param p2, "parm"    # I

    .prologue
    const/4 v0, 0x1

    .line 2618
    if-ne p1, v0, :cond_1

    .line 2619
    invoke-direct {p0, v0, p2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setScaleFactor(ZI)V

    .line 2622
    :cond_0
    :goto_0
    return-void

    .line 2620
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 2621
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setScaleFactor(ZI)V

    goto :goto_0
.end method

.method public setColumn()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2264
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    .line 2265
    .local v0, "oldColCount":I
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getHorizontalCount()V

    .line 2266
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    if-eq v1, v0, :cond_0

    .line 2267
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mVisibleFirst:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScrollOffset:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemHVisibleCnt:I

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mItemFocused:I

    .line 2268
    const/4 v1, 0x6

    invoke-virtual {p0, v1, v4, v4, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    .line 2270
    :cond_0
    return-void
.end method

.method public setDragEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 4015
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsDragEnabled:Z

    .line 4016
    return-void
.end method

.method public setExpandable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 4011
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpandable:Z

    .line 4012
    return-void
.end method

.method public setExpanded(ZZ)V
    .locals 3
    .param p1, "expand"    # Z
    .param p2, "playAnim"    # Z

    .prologue
    const/4 v2, 0x1

    .line 3970
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsNoSplitMode:Z

    if-eqz v0, :cond_1

    .line 3971
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    .line 3972
    const/4 p2, 0x0

    .line 3978
    :goto_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-eqz v0, :cond_3

    .line 3979
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq v0, v1, :cond_0

    .line 3980
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mExpandViewMode:I

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setThumbnailViewMode(I)V

    .line 3986
    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getHorizontalCount()V

    .line 3988
    if-eqz p2, :cond_4

    .line 3989
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->startExpandAnimation()V

    .line 3993
    :goto_2
    return-void

    .line 3973
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsEasyMode:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFromTimeView:Z

    if-eqz v0, :cond_2

    .line 3974
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    goto :goto_0

    .line 3976
    :cond_2
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    goto :goto_0

    .line 3983
    :cond_3
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setThumbnailViewMode(I)V

    goto :goto_1

    .line 3991
    :cond_4
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->startExpandNoAnimation()V

    goto :goto_2
.end method

.method public setFocusIndex(IZ)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "isAlbum"    # Z

    .prologue
    const/4 v1, -0x1

    .line 3622
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    if-nez v0, :cond_1

    .line 3639
    :cond_0
    :goto_0
    return-void

    .line 3625
    :cond_1
    if-ne p1, v1, :cond_3

    .line 3626
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    if-eqz v0, :cond_2

    .line 3627
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iput v1, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemFocused:I

    .line 3629
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    if-eqz v0, :cond_0

    .line 3630
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iput v1, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemFocused:I

    goto :goto_0

    .line 3635
    :cond_3
    if-eqz p2, :cond_4

    .line 3636
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iput p1, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemFocused:I

    goto :goto_0

    .line 3638
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iput p1, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemFocused:I

    goto :goto_0
.end method

.method public setMode(IILjava/lang/Object;)V
    .locals 3
    .param p1, "mode"    # I
    .param p2, "parm"    # I
    .param p3, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 2606
    invoke-super {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->setMode(IILjava/lang/Object;)V

    .line 2607
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_0

    .line 2608
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemCount:I

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 2609
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2, v2, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    .line 2614
    :cond_0
    :goto_0
    return-void

    .line 2611
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v2, v2, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    goto :goto_0
.end method

.method public setVisibleRect(Landroid/graphics/Rect;)V
    .locals 7
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    const/4 v6, 0x0

    .line 1837
    if-eqz p1, :cond_2

    .line 1838
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    .line 1839
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    if-nez v2, :cond_1

    .line 1840
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipEnable:Z

    .line 1847
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 1848
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v1, v2, v0

    .line 1849
    .local v1, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipEnable:Z

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setClippingEnabled(Z)V

    .line 1850
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipEnable:Z

    if-eqz v2, :cond_0

    .line 1851
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setClipRect(IIII)V

    .line 1847
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1842
    .end local v0    # "i":I
    .end local v1    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipEnable:Z

    goto :goto_0

    .line 1844
    :cond_2
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    .line 1845
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipEnable:Z

    goto :goto_0

    .line 1854
    .restart local v0    # "i":I
    :cond_3
    const/4 v0, 0x0

    :goto_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 1855
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v1, v2, v0

    .line 1856
    .restart local v1    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipEnable:Z

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setClippingEnabled(Z)V

    .line 1857
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipEnable:Z

    if-eqz v2, :cond_4

    .line 1858
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setClipRect(IIII)V

    .line 1854
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1861
    .end local v1    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->reset()V

    .line 1862
    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->loadFolders(Z)V

    .line 1864
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->reset()V

    .line 1865
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mVisibleFirst:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemCount:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->resetSet(II)V

    .line 1866
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScrollRange:F

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setRange(FF)V

    .line 1868
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipEnable:Z

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setClippingEnabled(Z)V

    .line 1869
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipEnable:Z

    if-eqz v2, :cond_6

    .line 1870
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setClipRect(IIII)V

    .line 1872
    :cond_6
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipEnable:Z

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->setClippingEnabled(Z)V

    .line 1873
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipEnable:Z

    if-eqz v2, :cond_7

    .line 1874
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mClipRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->setClipRect(IIII)V

    .line 1875
    :cond_7
    return-void
.end method

.method public stopDragAnim(Z)V
    .locals 6
    .param p1, "doAnimation"    # Z

    .prologue
    const/4 v5, 0x0

    .line 2749
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2750
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 2751
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    array-length v1, v3

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_4

    .line 2752
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v2, v3, v0

    .line 2753
    .local v2, "thumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    if-eqz v2, :cond_1

    iget v3, v2, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    if-ltz v3, :cond_1

    iget v3, v2, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemCount:I

    if-lt v3, v4, :cond_2

    .line 2751
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2755
    :cond_2
    if-eqz p1, :cond_3

    .line 2756
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->undim()V

    goto :goto_1

    .line 2758
    :cond_3
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->resetDim()V

    goto :goto_1

    .line 2760
    .end local v2    # "thumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-eqz v3, :cond_5

    .line 2761
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHndDispmnt:I

    invoke-virtual {v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->addPos(II)V

    .line 2762
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHndDispmnt:I

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->releasePosIndex(I)V

    .line 2764
    :cond_5
    if-eqz p1, :cond_7

    .line 2765
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbMoveTo:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v3, :cond_6

    .line 2766
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDragAnim:Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbMoveTo:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->startMoveToAnimation(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 2772
    :goto_2
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 2773
    return-void

    .line 2768
    :cond_6
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDragAnim:Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->startInverseDragAnimation()V

    goto :goto_2

    .line 2770
    :cond_7
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDragAnim:Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->resetDrag()V

    goto :goto_2
.end method

.method updateArrowObject()V
    .locals 12

    .prologue
    const v11, 0x7f02026a

    const/4 v10, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    .line 3013
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderObjecSel:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .line 3014
    .local v2, "selectedObj":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    if-eqz v6, :cond_2

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsFixedAlbumVisible:Z

    if-eqz v6, :cond_2

    .line 3015
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .line 3026
    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    const/4 v7, 0x1

    sget v8, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ARROW_VISIBLE_RANGE:I

    invoke-virtual {v6, v7, v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 3027
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v6, v7, v11}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    int-to-float v1, v6

    .line 3028
    .local v1, "arrowWidth":F
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v6, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWorldCoordinateValue(F)F

    move-result v1

    .line 3029
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v6, v7, v11}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    int-to-float v0, v6

    .line 3030
    .local v0, "arrowHeight":F
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v6, v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWorldCoordinateValue(F)F

    move-result v0

    .line 3031
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v6, v1, v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setSize(FF)V

    .line 3032
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->getX()F

    move-result v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    invoke-virtual {v7, v10}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->getWidth(Z)F

    move-result v7

    div-float/2addr v7, v9

    add-float/2addr v6, v7

    div-float v7, v1, v9

    sub-float v3, v6, v7

    .line 3033
    .local v3, "x":F
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->getY()F

    move-result v4

    .line 3034
    .local v4, "y":F
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->getZ()F

    move-result v6

    const v7, 0x3c23d70a    # 0.01f

    add-float v5, v6, v7

    .line 3035
    .local v5, "z":F
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v6, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 3036
    .end local v0    # "arrowHeight":F
    .end local v1    # "arrowWidth":F
    .end local v3    # "x":F
    .end local v4    # "y":F
    .end local v5    # "z":F
    :cond_1
    :goto_0
    return-void

    .line 3017
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderObjecSel:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v6, :cond_1

    .line 3021
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderObjecSel:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mIndex:I

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    if-ne v6, v7, :cond_3

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    const/16 v7, -0x64

    if-ne v6, v7, :cond_0

    .line 3022
    :cond_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    sget v7, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ARROW_VISIBLE_RANGE:I

    invoke-virtual {v6, v10, v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    goto :goto_0
.end method

.method public updateFixedAlbum()V
    .locals 14

    .prologue
    const/4 v4, 0x0

    const/high16 v13, 0x40000000    # 2.0f

    .line 2927
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumTopPadding(I)F

    move-result v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumThumbnailHeight(I)F

    move-result v1

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxHeight(I)F

    move-result v1

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumBottomPadding(I)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->newAlbumHeaderHeight:F

    .line 2932
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-nez v0, :cond_1

    .line 2933
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->initFixedAlbum()V

    .line 2935
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->needToShowFixedAlbum()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2936
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->showFixedAlbum()V

    .line 2940
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsFixedAlbumVisible:Z

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setVisibility(Z)V

    .line 2942
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsFixedAlbumAtTop:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v8, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemStartY:F

    .line 2947
    .local v8, "posY":F
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemStartX:F

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemStartZ:F

    const v3, 0x3a83126f    # 0.001f

    add-float/2addr v2, v3

    invoke-virtual {v0, v1, v8, v2}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setPos(FFF)V

    .line 2949
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    if-ltz v0, :cond_3

    .line 2950
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v3, v3, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    invoke-virtual {v12, v0}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 2952
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    iput v1, v0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mIndex:I

    .line 2953
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v9

    .line 2955
    .local v9, "textObj":Lcom/sec/samsung/gallery/glview/GlTextObject;
    if-eqz v9, :cond_3

    .line 2956
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/glview/GlTextObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v6

    .line 2958
    .local v6, "glTextView":Lcom/sec/android/gallery3d/glcore/GlView;
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2959
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 2963
    .end local v6    # "glTextView":Lcom/sec/android/gallery3d/glcore/GlView;
    .end local v9    # "textObj":Lcom/sec/samsung/gallery/glview/GlTextObject;
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemW:F

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemH:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setSize(FF)V

    .line 2965
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v7

    .line 2966
    .local v7, "mTextObj":Lcom/sec/samsung/gallery/glview/GlTextObject;
    if-eqz v7, :cond_4

    .line 2969
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getWidth(Z)F

    move-result v0

    neg-float v0, v0

    div-float v10, v0, v13

    .line 2970
    .local v10, "x":F
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxXOffset(I)F

    move-result v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxWidth(I)F

    move-result v1

    div-float/2addr v1, v13

    add-float/2addr v0, v1

    add-float/2addr v10, v0

    .line 2972
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getHeight(Z)F

    move-result v0

    neg-float v0, v0

    div-float v11, v0, v13

    .line 2973
    .local v11, "y":F
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxYOffset(I)F

    move-result v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxHeight(I)F

    move-result v1

    div-float/2addr v1, v13

    add-float/2addr v0, v1

    sub-float/2addr v11, v0

    .line 2975
    const/4 v0, 0x0

    invoke-virtual {v7, v10, v11, v0}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setPos(FFF)V

    .line 2976
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxWidth(I)F

    move-result v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxHeight(I)F

    move-result v1

    invoke-virtual {v7, v0, v1}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setSize(FF)V

    .line 2979
    .end local v10    # "x":F
    .end local v11    # "y":F
    :cond_4
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->updateArrowObject()V

    .line 2980
    return-void

    .line 2937
    .end local v7    # "mTextObj":Lcom/sec/samsung/gallery/glview/GlTextObject;
    .end local v8    # "posY":F
    :cond_5
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->needToShowFixedAlbum()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2938
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->hideFixedAlbum()V

    goto/16 :goto_0

    .line 2942
    :cond_6
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    div-float/2addr v0, v13

    neg-float v0, v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemH:F

    div-float/2addr v1, v13

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxHeight(I)F

    move-result v1

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxYOffset(I)F

    move-result v1

    add-float v8, v0, v1

    goto/16 :goto_1
.end method

.method public updateFolderItem(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 1349
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_0

    .line 1361
    :goto_0
    return-void

    .line 1351
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 1353
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    if-eqz v0, :cond_1

    .line 1354
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->update(I)V

    .line 1356
    :cond_1
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFixScrollingAlbum:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mIndex:I

    if-ne v0, p1, :cond_2

    const/4 v0, -0x1

    if-eq p1, v0, :cond_2

    .line 1357
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->updateFixedAlbum()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1359
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    throw v0
.end method

.method public updateFolders()V
    .locals 1

    .prologue
    .line 1364
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    if-eqz v0, :cond_0

    .line 1365
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->update()V

    .line 1366
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFixScrollingAlbum:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v0, :cond_2

    .line 1367
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_1

    .line 1368
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    check-cast v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getFocusedIndex()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    .line 1369
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->updateFixedAlbum()V

    .line 1371
    :cond_2
    return-void
.end method

.method protected updateItem(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1339
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    if-eqz v0, :cond_0

    .line 1340
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->update(I)V

    .line 1341
    :cond_0
    return-void
.end method

.method public updateThumbs()V
    .locals 1

    .prologue
    .line 1344
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    if-eqz v0, :cond_0

    .line 1345
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->update()V

    .line 1346
    :cond_0
    return-void
.end method
