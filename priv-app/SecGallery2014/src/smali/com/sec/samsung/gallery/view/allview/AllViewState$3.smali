.class Lcom/sec/samsung/gallery/view/allview/AllViewState$3;
.super Lorg/puremvc/java/patterns/mediator/Mediator;
.source "AllViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/allview/AllViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/Object;

    .prologue
    .line 212
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$3;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-direct {p0, p2, p3}, Lorg/puremvc/java/patterns/mediator/Mediator;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public handleNotification(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 7
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 223
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getName()Ljava/lang/String;

    move-result-object v3

    .line 224
    .local v3, "notiName":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$3;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1000(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    .line 225
    .local v1, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v4, v1, Lcom/sec/samsung/gallery/view/allview/AllViewState;

    if-eqz v4, :cond_0

    .line 226
    const-string v4, "MEDIA_EJECT"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 227
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$3;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # invokes: Lcom/sec/samsung/gallery/view/allview/AllViewState;->handleLeaveMode()V
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1100(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 228
    :cond_1
    const-string v4, "DEVICE_DISCONNECTED"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 229
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$3;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1200(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$3;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$900(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$3;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mTopSetPath:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$100(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->isDeviceRemoved(Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 230
    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "top mediaset is removed"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    move-object v2, v4

    check-cast v2, [Ljava/lang/String;

    .line 232
    .local v2, "deviceInfo":[Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$3;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$900(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v5, v2, v5

    const/4 v6, 0x1

    aget-object v6, v2, v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->showDeviceRemovedDialog(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$3;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # invokes: Lcom/sec/samsung/gallery/view/allview/AllViewState;->refreshNearbyData()V
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1300(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V

    .line 234
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$3;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1400(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->getLastestActivityState(Landroid/content/Context;)Ljava/lang/Class;

    move-result-object v0

    .line 235
    .local v0, "activityState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$3;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$700(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v4, v0, v5}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public listNotificationInterests()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 215
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "MEDIA_EJECT"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "DEVICE_DISCONNECTED"

    aput-object v2, v0, v1

    return-object v0
.end method
