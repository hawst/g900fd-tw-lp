.class Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;
.super Ljava/lang/Object;
.source "AlbumViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 0

    .prologue
    .line 368
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAllContentReady()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 395
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mNeedIdleProcess:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 408
    :goto_0
    return-void

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # setter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mNeedIdleProcess:Z
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2302(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Z)Z

    .line 400
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$1700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "START_IDLE_PROCESS"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2400(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 406
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2500(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    .line 407
    new-instance v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2600(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;->setBeamListener()V

    goto :goto_0
.end method

.method public onContentChanged(II)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 391
    return-void
.end method

.method public onSizeChanged(I)V
    .locals 5
    .param p1, "size"    # I

    .prologue
    .line 371
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->checkMediaAvailability()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2000(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    .line 373
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    if-lez v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2100(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2100(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1

    .line 387
    :cond_0
    :goto_0
    return-void

    .line 379
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->tempTask:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2200(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;

    move-result-object v1

    monitor-enter v1

    .line 380
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    new-instance v2, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;)V

    # setter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;
    invoke-static {v0, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2102(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;)Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;

    .line 381
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2100(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 382
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->updateCountOnActionBar()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$1000(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    goto :goto_0

    .line 382
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
