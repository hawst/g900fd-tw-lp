.class Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;
.super Ljava/lang/Object;
.source "AlbumViewBeam.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;->toItemList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;Ljava/util/List;)[Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;->toUriPathArray(Ljava/util/List;)[Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private toItemList(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "mediaSetList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 57
    .local v1, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .local v3, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    move-object v2, v3

    .line 58
    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 59
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v4, 0x0

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 61
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v3    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    return-object v1
.end method

.method private toUriPathArray(Ljava/util/List;)[Landroid/net/Uri;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)[",
            "Landroid/net/Uri;"
        }
    .end annotation

    .prologue
    .local p1, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 65
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 66
    .local v4, "uriPathList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    .line 68
    .local v0, "context":Landroid/content/Context;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 69
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    .line 70
    .local v3, "uri":Landroid/net/Uri;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$AndroidBeamUtils;->isBeamSupported(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 71
    new-array v5, v6, [Landroid/net/Uri;

    invoke-static {}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$AndroidBeamUtils;->getDummyCloudUri()Landroid/net/Uri;

    move-result-object v6

    aput-object v6, v5, v7

    .line 81
    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v3    # "uri":Landroid/net/Uri;
    :goto_1
    return-object v5

    .line 72
    .restart local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v3    # "uri":Landroid/net/Uri;
    :cond_0
    invoke-static {v3}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$AndroidBeamUtils;->isRemoteContents(Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 74
    new-array v5, v6, [Landroid/net/Uri;

    aput-object v3, v5, v7

    goto :goto_1

    .line 76
    :cond_1
    invoke-static {v0, v3}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$AndroidBeamUtils;->convertToFileUri(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    .line 77
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 81
    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v5, 0x0

    goto :goto_1

    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Landroid/net/Uri;

    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Landroid/net/Uri;

    goto :goto_1
.end method


# virtual methods
.method public setBeamListener()V
    .locals 4

    .prologue
    .line 32
    new-instance v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam$1;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;)V

    .line 46
    .local v0, "onGetFilePathListener":Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    .line 50
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "ANDROID_BEAM"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 52
    return-void
.end method
