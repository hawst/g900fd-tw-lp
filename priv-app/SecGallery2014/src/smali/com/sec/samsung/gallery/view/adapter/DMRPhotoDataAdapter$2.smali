.class Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$2;
.super Ljava/lang/Object;
.source "DMRPhotoDataAdapter.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/FutureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/FutureListener",
        "<",
        "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$2;->this$0:Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFutureDone(Lcom/sec/android/gallery3d/util/Future;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "future":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;>;"
    const/4 v7, 0x0

    const/4 v9, 0x1

    .line 129
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .line 130
    .local v1, "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    if-nez v1, :cond_0

    .line 131
    # getter for: Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->access$000()Ljava/lang/String;

    move-result-object v5

    const-string v6, "decoder is null"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$2;->this$0:Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->access$600(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$2;->this$0:Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->access$600(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;)Landroid/os/Handler;

    move-result-object v6

    new-instance v7, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;

    const/4 v8, 0x0

    invoke-direct {v7, v1, v8}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;-><init>(Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;Landroid/graphics/Bitmap;)V

    invoke-virtual {v6, v9, v7}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 145
    :goto_0
    return-void

    .line 136
    :cond_0
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->getWidth()I

    move-result v4

    .line 137
    .local v4, "width":I
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->getHeight()I

    move-result v2

    .line 138
    .local v2, "height":I
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 139
    .local v3, "options":Landroid/graphics/BitmapFactory$Options;
    const/high16 v5, 0x44800000    # 1024.0f

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-static {v5}, Lcom/sec/android/gallery3d/common/BitmapUtils;->computeSampleSize(F)I

    move-result v5

    iput v5, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 141
    iput-boolean v9, v3, Landroid/graphics/BitmapFactory$Options;->isPreview:Z

    .line 142
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v7, v7, v4, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v1, v5, v3}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 143
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$2;->this$0:Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->access$600(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$2;->this$0:Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->access$600(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;)Landroid/os/Handler;

    move-result-object v6

    new-instance v7, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;

    invoke-direct {v7, v1, v0}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;-><init>(Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;Landroid/graphics/Bitmap;)V

    invoke-virtual {v6, v9, v7}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
