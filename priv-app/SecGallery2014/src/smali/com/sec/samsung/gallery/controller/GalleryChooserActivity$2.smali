.class Lcom/sec/samsung/gallery/controller/GalleryChooserActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "GalleryChooserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity$2;->this$0:Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const-wide/16 v4, 0x32

    const/4 v3, 0x0

    .line 108
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v0

    .line 109
    .local v0, "secretMode":Z
    const-string v1, "com.samsung.android.intent.action.PRIVATE_MODE_ON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity$2;->this$0:Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;

    # getter for: Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->privateMode:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->access$000(Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;)Z

    move-result v1

    if-eq v1, v0, :cond_1

    .line 110
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity$2;->this$0:Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;

    # setter for: Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->privateMode:Z
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->access$002(Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;Z)Z

    .line 111
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity$2;->this$0:Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;

    # getter for: Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->access$100(Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    const-string v1, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity$2;->this$0:Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;

    # getter for: Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->privateMode:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->access$000(Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    .line 113
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity$2;->this$0:Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;

    # setter for: Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->privateMode:Z
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->access$002(Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;Z)Z

    .line 114
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity$2;->this$0:Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;

    # getter for: Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->access$100(Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    goto :goto_0
.end method
