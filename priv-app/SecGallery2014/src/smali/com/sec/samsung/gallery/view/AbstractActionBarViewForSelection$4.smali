.class Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$4;
.super Ljava/lang/Object;
.source "AbstractActionBarViewForSelection.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->setSelectAllButtonTitle(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$4;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$4;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$200(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$4;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$4;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$300(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$4;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$300(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$4;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenuItemWidth:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$400(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->setTextWidth(I)V

    .line 265
    :cond_0
    return-void
.end method
