.class public Lcom/sec/samsung/gallery/controller/DetailModel;
.super Ljava/lang/Object;
.source "DetailModel.java"


# static fields
.field private static final GB:I = 0x40000000

.field private static final KB:J = 0x400L

.field private static final MB:I = 0x100000

.field private static final TAG:Ljava/lang/String; = "DetailModel"


# instance fields
.field addressResolvingListener:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;

.field private contentsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAddressResolver:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;

.field private final mContext:Landroid/content/Context;

.field private final mDetails:Lcom/sec/android/gallery3d/data/MediaDetails;

.field private mIgnoreList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mLocationIndex:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaDetails;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "details"    # Lcom/sec/android/gallery3d/data/MediaDetails;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 362
    new-instance v0, Lcom/sec/samsung/gallery/controller/DetailModel$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/controller/DetailModel$1;-><init>(Lcom/sec/samsung/gallery/controller/DetailModel;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->addressResolvingListener:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;

    .line 55
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mContext:Landroid/content/Context;

    .line 56
    iput-object p2, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mDetails:Lcom/sec/android/gallery3d/data/MediaDetails;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mDetails:Lcom/sec/android/gallery3d/data/MediaDetails;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaDetails;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->contentsList:Ljava/util/ArrayList;

    .line 58
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mLocationIndex:I

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/DetailModel;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/DetailModel;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mLocationIndex:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/DetailModel;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/DetailModel;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/controller/DetailModel;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/DetailModel;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->contentsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private getDetailValue(ILjava/lang/Object;)Ljava/lang/String;
    .locals 6
    .param p1, "key"    # I
    .param p2, "valueObj"    # Ljava/lang/Object;

    .prologue
    .line 99
    const/16 v0, 0x9

    if-ne p1, v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->contentsList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mLocationIndex:I

    .line 101
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mAddressResolver:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;

    if-nez v0, :cond_1

    .line 102
    new-instance v1, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v0}, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mAddressResolver:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;

    .line 107
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mAddressResolver:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;

    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->addressResolvingListener:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;

    move v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/sec/samsung/gallery/controller/DetailModel;->getDetailValue(Landroid/content/Context;ILjava/lang/Object;ZLcom/sec/android/gallery3d/ui/DetailsAddressResolver;Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mAddressResolver:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;->cancel()V

    goto :goto_0
.end method

.method public static getDetailValue(Landroid/content/Context;ILjava/lang/Object;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # I
    .param p2, "valueObj"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 111
    const/4 v3, 0x1

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Lcom/sec/samsung/gallery/controller/DetailModel;->getDetailValue(Landroid/content/Context;ILjava/lang/Object;ZLcom/sec/android/gallery3d/ui/DetailsAddressResolver;Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getDetailValue(Landroid/content/Context;ILjava/lang/Object;ZLcom/sec/android/gallery3d/ui/DetailsAddressResolver;Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;)Ljava/lang/String;
    .locals 32
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # I
    .param p2, "valueObj"    # Ljava/lang/Object;
    .param p3, "isSimpleMode"    # Z
    .param p4, "addrResolver"    # Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;
    .param p5, "addrReolveingListener"    # Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;

    .prologue
    .line 117
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v24

    move-object/from16 v0, v24

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v10

    .line 118
    .local v10, "language":Ljava/lang/String;
    const/16 v22, 0x0

    .line 119
    .local v22, "value":Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 352
    if-nez p2, :cond_1f

    .line 353
    const/16 v22, 0x0

    .line 359
    .end local p2    # "valueObj":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v22

    .line 123
    .restart local p2    # "valueObj":Ljava/lang/Object;
    :sswitch_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v12

    .line 124
    .local v12, "locale":Ljava/lang/String;
    const-string v24, "ar"

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_1

    const-string v24, "fa"

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_1

    const-string/jumbo v24, "ur"

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_1

    const-string v24, "iw"

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_2

    .line 125
    :cond_1
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 126
    .local v16, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v24, "\u200f\u200e"

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 132
    .end local v16    # "sb":Ljava/lang/StringBuilder;
    :goto_1
    const-string v24, "DetailModel"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v26, "value "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 129
    :cond_2
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v22

    goto :goto_1

    .line 136
    .end local v12    # "locale":Ljava/lang/String;
    :sswitch_1
    sget-boolean v24, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLocationInfo:Z

    if-eqz v24, :cond_0

    .line 137
    if-eqz p2, :cond_0

    .line 140
    if-eqz p4, :cond_0

    .line 141
    check-cast p2, [D

    .end local p2    # "valueObj":Ljava/lang/Object;
    move-object/from16 v11, p2

    check-cast v11, [D

    .line 142
    .local v11, "latlng":[D
    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-virtual {v0, v11, v1}, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;->resolveAddress([DLcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;)Ljava/lang/String;

    move-result-object v22

    .line 143
    goto/16 :goto_0

    .line 149
    .end local v11    # "latlng":[D
    .restart local p2    # "valueObj":Ljava/lang/Object;
    :sswitch_2
    const/16 v22, 0x0

    .line 151
    goto/16 :goto_0

    .line 154
    :sswitch_3
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v22

    .line 155
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v15

    .line 156
    .local v15, "orientation_value":I
    const-string v24, "ar"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_3

    const-string v24, "ar-rIL"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_3

    const-string v24, "fa"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_3

    const-string/jumbo v24, "ur"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_4

    .line 158
    :cond_3
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v25, 0xb0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "%d"

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-static/range {v25 .. v26}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_0

    .line 161
    :cond_4
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "%d"

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-static/range {v25 .. v26}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const/16 v25, 0xb0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 163
    goto/16 :goto_0

    .line 166
    .end local v15    # "orientation_value":I
    :sswitch_4
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v22

    .line 167
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 168
    .local v9, "iso_value":I
    const-string v24, "ar"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_5

    const-string v24, "ar-rIL"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_5

    const-string v24, "fa"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_5

    const-string/jumbo v24, "ur"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_6

    .line 169
    :cond_5
    const-string v24, "%d"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_0

    .line 171
    :cond_6
    const-string v24, "%d"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    .line 173
    goto/16 :goto_0

    .line 177
    .end local v9    # "iso_value":I
    :sswitch_5
    sget-boolean v24, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLocationInfo:Z

    if-eqz v24, :cond_0

    .line 178
    if-eqz p2, :cond_0

    .line 181
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_0

    .line 186
    :sswitch_6
    const/4 v13, 0x0

    .line 187
    .local v13, "mimeType":Ljava/lang/String;
    if-eqz p2, :cond_7

    .line 188
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    .line 190
    :cond_7
    const/4 v4, 0x0

    .line 191
    .local v4, "displayMimeType":Ljava/lang/String;
    if-eqz v13, :cond_8

    .line 192
    const/16 v24, 0x2f

    move/from16 v0, v24

    invoke-virtual {v13, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v17

    .line 193
    .local v17, "slashPos":I
    const/16 v24, -0x1

    move/from16 v0, v17

    move/from16 v1, v24

    if-eq v0, v1, :cond_9

    add-int/lit8 v24, v17, 0x1

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-ge v0, v1, :cond_9

    .line 194
    add-int/lit8 v24, v17, 0x1

    move/from16 v0, v24

    invoke-virtual {v13, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    .line 199
    .end local v17    # "slashPos":I
    :cond_8
    :goto_2
    if-nez v4, :cond_a

    const v24, 0x7f0e00ff

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 202
    :goto_3
    goto/16 :goto_0

    .line 196
    .restart local v17    # "slashPos":I
    :cond_9
    invoke-virtual {v13}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .end local v17    # "slashPos":I
    :cond_a
    move-object/from16 v22, v4

    .line 199
    goto :goto_3

    .line 204
    .end local v4    # "displayMimeType":Ljava/lang/String;
    .end local v13    # "mimeType":Ljava/lang/String;
    :sswitch_7
    check-cast p2, Ljava/lang/Long;

    .end local p2    # "valueObj":Ljava/lang/Object;
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->doubleValue()D

    move-result-wide v18

    .line 205
    .local v18, "size":D
    const-string v24, "ar"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_b

    const-string v24, "ar-rIL"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_b

    const-string v24, "fa"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_b

    const-string/jumbo v24, "ur"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_b

    const-string v24, "ko"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_f

    .line 206
    :cond_b
    const-wide/high16 v24, 0x41d0000000000000L    # 1.073741824E9

    cmpl-double v24, v18, v24

    if-ltz v24, :cond_c

    .line 207
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "%.2f"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const v25, 0x7f0e0341

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const-wide/high16 v28, 0x41d0000000000000L    # 1.073741824E9

    div-double v28, v18, v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_0

    .line 208
    :cond_c
    const-wide/high16 v24, 0x4130000000000000L    # 1048576.0

    cmpl-double v24, v18, v24

    if-ltz v24, :cond_d

    .line 209
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "%.1f"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const v25, 0x7f0e0342

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const-wide/high16 v28, 0x4130000000000000L    # 1048576.0

    div-double v28, v18, v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_0

    .line 210
    :cond_d
    const-wide/high16 v24, 0x4090000000000000L    # 1024.0

    cmpl-double v24, v18, v24

    if-ltz v24, :cond_e

    .line 211
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "%d"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const v25, 0x7f0e0343

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x400

    div-long v28, v28, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_0

    .line 213
    :cond_e
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "%d"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const v25, 0x7f0e0344

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_0

    .line 216
    :cond_f
    const-wide/high16 v24, 0x41d0000000000000L    # 1.073741824E9

    cmpl-double v24, v18, v24

    if-ltz v24, :cond_10

    .line 217
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "%.2f "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const v25, 0x7f0e0341

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const-wide/high16 v28, 0x41d0000000000000L    # 1.073741824E9

    div-double v28, v18, v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_0

    .line 218
    :cond_10
    const-wide/high16 v24, 0x4130000000000000L    # 1048576.0

    cmpl-double v24, v18, v24

    if-ltz v24, :cond_11

    .line 219
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "%.1f "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const v25, 0x7f0e0342

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const-wide/high16 v28, 0x4130000000000000L    # 1048576.0

    div-double v28, v18, v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_0

    .line 220
    :cond_11
    const-wide/high16 v24, 0x4090000000000000L    # 1024.0

    cmpl-double v24, v18, v24

    if-ltz v24, :cond_12

    .line 221
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "%d "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const v25, 0x7f0e0343

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x400

    div-long v28, v28, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_0

    .line 223
    :cond_12
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "%d "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const v25, 0x7f0e0344

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    .line 226
    goto/16 :goto_0

    .line 229
    .end local v18    # "size":D
    .restart local p2    # "valueObj":Ljava/lang/Object;
    :sswitch_8
    const-string v24, "1"

    move-object/from16 v0, v24

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_13

    const v24, 0x7f0e0083

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 265
    :goto_4
    goto/16 :goto_0

    .line 229
    :cond_13
    const v24, 0x7f0e0090

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    goto :goto_4

    :sswitch_9
    move-object/from16 v6, p2

    .line 268
    check-cast v6, Lcom/sec/android/gallery3d/data/MediaDetails$FlashState;

    .line 269
    .local v6, "flash":Lcom/sec/android/gallery3d/data/MediaDetails$FlashState;
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaDetails$FlashState;->isFlashFired()Z

    move-result v24

    if-eqz v24, :cond_15

    .line 270
    if-eqz p3, :cond_14

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaDetails$FlashState;->isFlashPresent()Z

    move-result v24

    if-eqz v24, :cond_14

    .line 271
    const v24, 0x7f0e00bc

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_0

    .line 273
    :cond_14
    const v24, 0x7f0e0091

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_0

    .line 276
    :cond_15
    if-eqz p3, :cond_16

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaDetails$FlashState;->isFlashPresent()Z

    move-result v24

    if-eqz v24, :cond_16

    .line 277
    const v24, 0x7f0e0187

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_0

    .line 279
    :cond_16
    const v24, 0x7f0e0092

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 282
    goto/16 :goto_0

    .line 285
    .end local v6    # "flash":Lcom/sec/android/gallery3d/data/MediaDetails$FlashState;
    :sswitch_a
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v22

    .line 286
    if-nez p3, :cond_0

    .line 287
    const-string v24, "F %s"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    aput-object v22, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_0

    .line 292
    :sswitch_b
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v22

    .line 293
    const-string v24, "DetailModel"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Exposure value "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v24

    if-nez v24, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isMEnabled()Z

    move-result v24

    if-nez v24, :cond_0

    .line 297
    const-string v24, "/"

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_18

    .line 298
    const-string v24, "/"

    const/16 v25, 0x2

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v14

    .line 299
    .local v14, "num":[Ljava/lang/String;
    const/16 v24, 0x0

    aget-object v24, v14, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v24

    const/16 v26, 0x1

    aget-object v26, v14, v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v26

    div-double v20, v24, v26

    .line 303
    .end local v14    # "num":[Ljava/lang/String;
    .local v20, "time":D
    :goto_5
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    cmpg-double v24, v20, v24

    if-gez v24, :cond_1e

    .line 304
    const-wide/high16 v24, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    div-double v26, v26, v20

    add-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-int v0, v0

    move/from16 v23, v0

    .line 306
    .local v23, "xtime":I
    const/16 v24, 0x3b

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_19

    .line 307
    const/16 v23, 0x3c

    .line 319
    :cond_17
    :goto_6
    const-string v24, "%d/%d"

    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x1

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x1

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    .line 320
    goto/16 :goto_0

    .line 301
    .end local v20    # "time":D
    .end local v23    # "xtime":I
    :cond_18
    invoke-static/range {v22 .. v22}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v20

    .restart local v20    # "time":D
    goto :goto_5

    .line 308
    .restart local v23    # "xtime":I
    :cond_19
    const/16 v24, 0x5b

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_1a

    .line 309
    const/16 v23, 0x5a

    goto :goto_6

    .line 310
    :cond_1a
    const/16 v24, 0xb3

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_1b

    .line 311
    const/16 v23, 0xb4

    goto :goto_6

    .line 312
    :cond_1b
    const/16 v24, 0x159

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_1c

    .line 313
    const/16 v23, 0x15e

    goto :goto_6

    .line 314
    :cond_1c
    const/16 v24, 0x301

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_1d

    .line 315
    const/16 v23, 0x2ee

    goto :goto_6

    .line 316
    :cond_1d
    const/16 v24, 0x595

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_17

    .line 317
    const/16 v23, 0x5dc

    goto :goto_6

    .line 321
    .end local v23    # "xtime":I
    :cond_1e
    move-wide/from16 v0, v20

    double-to-int v8, v0

    .line 322
    .local v8, "integer":I
    int-to-double v0, v8

    move-wide/from16 v24, v0

    sub-double v20, v20, v24

    .line 323
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "\'\'"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 324
    const-wide v24, 0x3f1a36e2eb1c432dL    # 1.0E-4

    cmpl-double v24, v20, v24

    if-lez v24, :cond_0

    .line 325
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " %d/%d"

    const/16 v26, 0x2

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    const/16 v28, 0x1

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    const/16 v27, 0x1

    const-wide/high16 v28, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v30, 0x3ff0000000000000L    # 1.0

    div-double v30, v30, v20

    add-double v28, v28, v30

    move-wide/from16 v0, v28

    double-to-int v0, v0

    move/from16 v28, v0

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-static/range {v25 .. v26}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_0

    .line 333
    .end local v8    # "integer":I
    .end local v20    # "time":D
    :sswitch_c
    const-string v22, ""

    .line 334
    goto/16 :goto_0

    .line 337
    :sswitch_d
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v22

    .line 338
    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Float;->floatValue()F

    move-result v5

    .line 339
    .local v5, "exposure_value":F
    const-string v24, "%.1f"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    .line 340
    goto/16 :goto_0

    .line 343
    .end local v5    # "exposure_value":F
    :sswitch_e
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v22

    .line 344
    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 345
    .local v7, "focalLength_value":F
    const-string v24, "%.2f"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    .line 346
    goto/16 :goto_0

    .line 356
    .end local v7    # "focalLength_value":F
    :cond_1f
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_0

    .line 119
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_6
        0x4 -> :sswitch_0
        0x9 -> :sswitch_1
        0xa -> :sswitch_5
        0xb -> :sswitch_5
        0xc -> :sswitch_2
        0xd -> :sswitch_2
        0xe -> :sswitch_7
        0x10 -> :sswitch_3
        0x12 -> :sswitch_0
        0x66 -> :sswitch_9
        0x67 -> :sswitch_e
        0x68 -> :sswitch_8
        0x6a -> :sswitch_a
        0x6b -> :sswitch_b
        0x6c -> :sswitch_4
        0x6d -> :sswitch_d
        0x12c -> :sswitch_c
    .end sparse-switch
.end method

.method private getName(Landroid/content/Context;I)Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # I

    .prologue
    const/4 v0, 0x0

    const v3, 0x7f0e00fe

    const v2, 0x7f0e00fd

    const v1, 0x7f0e00f8

    .line 375
    sparse-switch p2, :sswitch_data_0

    .line 452
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown key"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    .line 377
    :sswitch_0
    const v0, 0x7f0e0071

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 379
    :sswitch_1
    const v0, 0x7f0e0072

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 381
    :sswitch_2
    const v0, 0x7f0e01d3

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 383
    :sswitch_3
    const v0, 0x7f0e01d4

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 385
    :sswitch_4
    const v0, 0x7f0e0074

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 387
    :sswitch_5
    const v0, 0x7f0e0075

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 389
    :sswitch_6
    const v0, 0x7f0e0076

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 391
    :sswitch_7
    const v0, 0x7f0e0077

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 393
    :sswitch_8
    const v0, 0x7f0e0078

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 395
    :sswitch_9
    const v0, 0x7f0e0079

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 397
    :sswitch_a
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 399
    :sswitch_b
    const v0, 0x7f0e01d1

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 401
    :sswitch_c
    const v0, 0x7f0e007a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 403
    :sswitch_d
    const v0, 0x7f0e007b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 405
    :sswitch_e
    const v0, 0x7f0e007c

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 407
    :sswitch_f
    const v0, 0x7f0e007d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 409
    :sswitch_10
    const v0, 0x7f0e007e

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 411
    :sswitch_11
    const v0, 0x7f0e007f

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 413
    :sswitch_12
    const v0, 0x7f0e0080

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 415
    :sswitch_13
    const v0, 0x7f0e0081

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 417
    :sswitch_14
    const v0, 0x7f0e01d2

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 419
    :sswitch_15
    const v0, 0x7f0e01d7

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 421
    :sswitch_16
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLocationInfo:Z

    if-eqz v1, :cond_0

    .line 422
    const v0, 0x7f0e01d5

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 427
    :sswitch_17
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLocationInfo:Z

    if-eqz v1, :cond_0

    .line 428
    const v0, 0x7f0e01d6

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 434
    :sswitch_18
    const-string v0, "[%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const v3, 0x7f0e00fa

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 436
    :sswitch_19
    const v0, 0x7f0e00f7

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 438
    :sswitch_1a
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 440
    :sswitch_1b
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 442
    :sswitch_1c
    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 444
    :sswitch_1d
    const v0, 0x7f0e00f9

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 446
    :sswitch_1e
    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 448
    :sswitch_1f
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 450
    :sswitch_20
    const v0, 0x7f0e00fc

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 375
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_a
        0x4 -> :sswitch_2
        0x5 -> :sswitch_15
        0x9 -> :sswitch_4
        0xa -> :sswitch_16
        0xb -> :sswitch_17
        0xc -> :sswitch_6
        0xd -> :sswitch_7
        0xe -> :sswitch_b
        0xf -> :sswitch_14
        0x10 -> :sswitch_8
        0x11 -> :sswitch_9
        0x12 -> :sswitch_3
        0x64 -> :sswitch_c
        0x65 -> :sswitch_d
        0x66 -> :sswitch_e
        0x67 -> :sswitch_10
        0x68 -> :sswitch_11
        0x6a -> :sswitch_f
        0x6b -> :sswitch_12
        0x6c -> :sswitch_13
        0xc8 -> :sswitch_5
        0x12c -> :sswitch_18
        0x12d -> :sswitch_19
        0x12e -> :sswitch_1a
        0x12f -> :sswitch_1f
        0x130 -> :sswitch_1c
        0x131 -> :sswitch_1b
        0x132 -> :sswitch_1e
        0x134 -> :sswitch_1d
        0x135 -> :sswitch_20
    .end sparse-switch
.end method


# virtual methods
.method public addIgnores([I)V
    .locals 6
    .param p1, "list"    # [I

    .prologue
    .line 62
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mIgnoreList:Ljava/util/Set;

    if-nez v4, :cond_0

    .line 63
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mIgnoreList:Ljava/util/Set;

    .line 65
    :cond_0
    move-object v0, p1

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget v2, v0, v1

    .line 66
    .local v2, "key":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mIgnoreList:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 65
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 68
    .end local v2    # "key":I
    :cond_1
    return-void
.end method

.method public toStringList()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mDetails:Lcom/sec/android/gallery3d/data/MediaDetails;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaDetails;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 73
    .local v0, "detail":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Object;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 74
    .local v2, "key":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mIgnoreList:Ljava/util/Set;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mIgnoreList:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 78
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    invoke-direct {p0, v2, v5}, Lcom/sec/samsung/gallery/controller/DetailModel;->getDetailValue(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 79
    .local v3, "value":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 81
    const/16 v5, 0x12c

    if-ne v2, v5, :cond_3

    .line 82
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mContext:Landroid/content/Context;

    invoke-direct {p0, v5, v2}, Lcom/sec/samsung/gallery/controller/DetailModel;->getName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    .line 91
    .local v4, "valueData":Ljava/lang/String;
    :cond_2
    :goto_1
    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->contentsList:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 83
    .end local v4    # "valueData":Ljava/lang/String;
    :cond_3
    const/16 v5, 0x6d

    if-ne v2, v5, :cond_4

    .line 84
    const/4 v4, 0x0

    .restart local v4    # "valueData":Ljava/lang/String;
    goto :goto_1

    .line 86
    .end local v4    # "valueData":Ljava/lang/String;
    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mContext:Landroid/content/Context;

    invoke-direct {p0, v6, v2}, Lcom/sec/samsung/gallery/controller/DetailModel;->getName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 87
    .restart local v4    # "valueData":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mDetails:Lcom/sec/android/gallery3d/data/MediaDetails;

    invoke-virtual {v5, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->hasUnit(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 88
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->mDetails:Lcom/sec/android/gallery3d/data/MediaDetails;

    invoke-virtual {v7, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->getUnit(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 95
    .end local v0    # "detail":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Object;>;"
    .end local v2    # "key":I
    .end local v3    # "value":Ljava/lang/String;
    .end local v4    # "valueData":Ljava/lang/String;
    :cond_5
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/DetailModel;->contentsList:Ljava/util/ArrayList;

    return-object v5
.end method
