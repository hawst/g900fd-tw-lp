.class Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog$3;
.super Ljava/lang/Object;
.source "ShareLocationConfirmDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->createAndShowDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog$3;->this$0:Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog$3;->val$checkBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog$3;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog$3;->this$0:Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;

    const/4 v1, 0x1

    # invokes: Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->setShareActionDialogOff(Z)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->access$000(Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;Z)V

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog$3;->this$0:Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog$3;->this$0:Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->mEvent:Lcom/sec/samsung/gallery/core/Event;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->access$100(Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->notifyObservers(Ljava/lang/Object;)V

    .line 74
    return-void
.end method
