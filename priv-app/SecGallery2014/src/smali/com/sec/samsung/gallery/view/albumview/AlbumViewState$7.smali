.class Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;
.super Ljava/lang/Object;
.source "AlbumViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 0

    .prologue
    .line 545
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)Z
    .locals 7
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "albumIndex"    # I
    .param p4, "itemIndex"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 547
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$3400(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "AMLP"

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$3500(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 548
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$3600(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setMultiWindow(Landroid/content/Context;)V

    .line 549
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v4

    if-eqz v4, :cond_1

    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReOrderAlbumsSlectionMode:Z

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$3700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isConnetedSideSync(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$3800(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCallWindow(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 551
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v4, p3}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 552
    .local v1, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    instance-of v4, v1, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-nez v4, :cond_0

    .line 553
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->exitSelectionMode()V
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$200(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    .line 554
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$3900(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    const v4, 0x7f0e04c2

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 580
    .end local v1    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_0
    return v2

    .line 557
    .restart local v1    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->exitSelectionMode()V
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$200(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    .line 558
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v5

    # setter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mBackupMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$4002(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Lcom/sec/android/gallery3d/data/MediaSet;)Lcom/sec/android/gallery3d/data/MediaSet;

    .line 559
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->enterReorderAlbums(Z)V
    invoke-static {v4, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$4100(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Z)V

    move v2, v3

    .line 560
    goto :goto_0

    .line 562
    .end local v1    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v4

    sget-object v5, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_NORMAL:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v4, v5, :cond_2

    move v2, v3

    .line 563
    goto :goto_0

    .line 564
    :cond_2
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReOrderAlbumsSlectionMode:Z

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsReOrderState:Z
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$4200(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v2, v3

    .line 565
    goto :goto_0

    .line 566
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v3

    if-nez v3, :cond_4

    .line 567
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->enterSelectionMode(Z)V
    invoke-static {v3, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$4300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Z)V

    .line 569
    :cond_4
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDragNDropInMultiwindow:Z

    if-eqz v3, :cond_5

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v3, :cond_5

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v3

    if-nez v3, :cond_6

    :cond_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$4400(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isConnetedSideSync(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$4500(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCallWindow(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 571
    :cond_6
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v4, p3}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 572
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-virtual {v3, p3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->selectAlbum(I)V

    goto/16 :goto_0

    .line 574
    :cond_7
    new-instance v0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$4600(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;-><init>(Landroid/content/Context;)V

    .line 575
    .local v0, "drag":Lcom/sec/samsung/gallery/view/common/DragAndDrop;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0, p3, v3, v4, v5}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->startAlbumDrag(ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    goto/16 :goto_0

    .line 578
    .end local v0    # "drag":Lcom/sec/samsung/gallery/view/common/DragAndDrop;
    :cond_8
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-virtual {v3, p3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->selectAlbum(I)V

    goto/16 :goto_0
.end method
