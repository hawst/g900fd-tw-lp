.class public Lcom/sec/samsung/gallery/glview/GlSplitObject;
.super Lcom/sec/samsung/gallery/glview/GlBaseObject;
.source "GlSplitObject.java"


# instance fields
.field public mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

.field protected mClickAvailable:Z

.field protected mFocused:Z

.field protected mSelected:Z

.field protected mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFI)V
    .locals 5
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "width"    # F
    .param p3, "height"    # F
    .param p4, "textureSize"    # I

    .prologue
    const/4 v4, 0x0

    .line 16
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFI)V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    .line 18
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;-><init>(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->setDuration(J)V

    .line 20
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->setSelectScale(F)V

    .line 21
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 22
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mSelected:Z

    .line 23
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mFocused:Z

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;II)V
    .locals 5
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v4, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 40
    invoke-direct {p0, p1, v1, v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    .line 41
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;-><init>(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    .line 42
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->setDuration(J)V

    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->setSelectScale(F)V

    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 45
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mSelected:Z

    .line 46
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mFocused:Z

    .line 47
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v1, p1, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v0, v1, p2, p3}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;IIII)V
    .locals 6
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "childWidth"    # I
    .param p5, "childHeight"    # I

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 27
    invoke-direct {p0, p1, v4, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 13
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    .line 28
    new-instance v1, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;-><init>(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    .line 29
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->setDuration(J)V

    .line 30
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->setSelectScale(F)V

    .line 31
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 32
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mSelected:Z

    .line 33
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mFocused:Z

    .line 34
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v2, p1, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v1, v2, p2, p3}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 35
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlTextObject;

    invoke-direct {v0, p1, p0, p4, p5}, Lcom/sec/samsung/gallery/glview/GlTextObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/samsung/gallery/glview/GlBaseObject;II)V

    .line 36
    .local v0, "childTextObj":Lcom/sec/samsung/gallery/glview/GlTextObject;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 37
    return-void
.end method


# virtual methods
.method public cancelClick()V
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mSelected:Z

    if-nez v0, :cond_0

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mClickAvailable:Z

    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->cancelPressAnim()V

    .line 79
    :cond_0
    return-void
.end method

.method public getFocused()Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mFocused:Z

    return v0
.end method

.method public getMainObject()Lcom/sec/android/gallery3d/glcore/GlObject;
    .locals 0

    .prologue
    .line 124
    return-object p0
.end method

.method public getSelected()Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mSelected:Z

    return v0
.end method

.method protected onMoved(II)Z
    .locals 2
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 59
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0x2d

    if-le v0, v1, :cond_0

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mClickAvailable:Z

    .line 61
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->cancelPressAnim()V

    .line 63
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onPressed(II)Z
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v1, 0x1

    .line 51
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mClickAvailable:Z

    .line 52
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mSelected:Z

    if-nez v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->startPressAnim()V

    .line 55
    :cond_0
    return v1
.end method

.method protected onReleased(IIII)Z
    .locals 2
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    const/4 v1, 0x1

    .line 67
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mClickAvailable:Z

    if-eqz v0, :cond_0

    .line 68
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mSelected:Z

    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->startSelectAnim()V

    .line 71
    :cond_0
    return v1
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 132
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->remove()V

    .line 133
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlTextObject;->remove()V

    .line 136
    :cond_0
    return-void
.end method

.method public setFocused(Z)V
    .locals 2
    .param p1, "focused"    # Z

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mFocused:Z

    if-ne v0, p1, :cond_0

    .line 108
    :goto_0
    return-void

    .line 102
    :cond_0
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mFocused:Z

    .line 103
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mFocused:Z

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->startFocusAnim()V

    goto :goto_0

    .line 106
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mSelected:Z

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->startUnfocusAnim(Z)V

    goto :goto_0
.end method

.method public setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    .prologue
    .line 165
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 166
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 169
    :cond_0
    return-void
.end method

.method public setSelected(ZZ)V
    .locals 2
    .param p1, "selected"    # Z
    .param p2, "doAnim"    # Z

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mSelected:Z

    if-ne v0, p1, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mSelected:Z

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->setSelected(Z)V

    .line 97
    :goto_0
    return-void

    .line 87
    :cond_0
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mSelected:Z

    .line 88
    if-eqz p2, :cond_2

    .line 89
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mSelected:Z

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->startSelectAnim()V

    goto :goto_0

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->cancelPressAnim()V

    goto :goto_0

    .line 95
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mSelected:Z

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->setSelected(Z)V

    goto :goto_0
.end method

.method public setVisibility(Z)V
    .locals 1
    .param p1, "visibility"    # Z

    .prologue
    .line 143
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(Z)V

    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setVisibility(Z)V

    .line 147
    :cond_0
    return-void
.end method

.method public setVisibility(ZI)V
    .locals 1
    .param p1, "visibility"    # Z
    .param p2, "level"    # I

    .prologue
    .line 154
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 155
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setVisibility(ZI)V

    .line 158
    :cond_0
    return-void
.end method

.method public startAppearAnim()V
    .locals 4

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mTouchAnimation:Lcom/sec/samsung/gallery/glview/GlTouchAnimation;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->startAppearAnim(J)V

    .line 112
    return-void
.end method
