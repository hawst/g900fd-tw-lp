.class Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$SelectionNotifier;
.super Ljava/lang/Object;
.source "AdapterView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectionNotifier"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 861
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$SelectionNotifier;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>.SelectionNotifier;"
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$SelectionNotifier;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$1;

    .prologue
    .line 861
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$SelectionNotifier;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>.SelectionNotifier;"
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$SelectionNotifier;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 865
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$SelectionNotifier;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>.SelectionNotifier;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$SelectionNotifier;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mDataChanged:Z

    if-eqz v0, :cond_1

    .line 869
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$SelectionNotifier;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 870
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$SelectionNotifier;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->post(Ljava/lang/Runnable;)Z

    .line 876
    :cond_0
    :goto_0
    return-void

    .line 873
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$SelectionNotifier;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    # invokes: Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->fireOnSelected()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->access$200(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;)V

    .line 874
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$SelectionNotifier;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    # invokes: Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->performAccessibilityActionsOnSelected()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->access$300(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;)V

    goto :goto_0
.end method
