.class Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;
.super Ljava/lang/Object;
.source "DMRPhotoDataAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ImageBundle"
.end annotation


# instance fields
.field public final backupImage:Landroid/graphics/Bitmap;

.field public final decoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "decoder"    # Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .param p2, "backupImage"    # Landroid/graphics/Bitmap;

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;->decoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .line 122
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;->backupImage:Landroid/graphics/Bitmap;

    .line 123
    return-void
.end method
