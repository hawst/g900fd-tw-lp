.class public final Lcom/sec/samsung/gallery/access/dcm/DCMInterface$CategoryType;
.super Ljava/lang/Object;
.source "DCMInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/access/dcm/DCMInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CategoryType"
.end annotation


# static fields
.field public static final DOCUMENTS:Ljava/lang/String; = "Documents"

.field public static final FLOWER:Ljava/lang/String; = "Flower"

.field public static final FOOD:Ljava/lang/String; = "Food"

.field public static final PEOPLE:Ljava/lang/String; = "People"

.field public static final PETS:Ljava/lang/String; = "Pets"

.field public static final SCENERY:Ljava/lang/String; = "Scenery"

.field public static final UNClASSIFIED:Ljava/lang/String; = "unclassified"

.field public static final VEHICLES:Ljava/lang/String; = "Vehicles"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
