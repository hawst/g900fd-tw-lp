.class Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;
.super Ljava/lang/Object;
.source "SlideShowViewState.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->addListenersOnButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V
    .locals 0

    .prologue
    .line 1088
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v11, 0xf

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1090
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    iget-object v9, v9, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->slideShowsettings:Landroid/widget/Button;

    invoke-virtual {p1, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1092
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setSlideshowPauseViewEnable(Z)V
    invoke-static {v8, v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1600(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Z)V

    .line 1093
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->finishCurrentViewState()V

    .line 1094
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$2000(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v8

    const-string v9, "SHOW_SLIDESHOW_SETTINGS"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v8, v9, v7}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1161
    :cond_0
    :goto_0
    return-void

    .line 1096
    :cond_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    iget-object v9, v9, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->slideShowStop:Landroid/widget/Button;

    invoke-virtual {p1, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1097
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setSlideshowPauseViewEnable(Z)V
    invoke-static {v8, v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1600(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Z)V

    .line 1098
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->finishCurrentViewState()V

    goto :goto_0

    .line 1099
    :cond_2
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    iget-object v9, v9, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->createVideoClip:Landroid/widget/Button;

    invoke-virtual {p1, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1100
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSelectedItems:Ljava/util/List;
    invoke-static {v10}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$2100(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Ljava/util/List;

    move-result-object v10

    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->getUriList(Ljava/util/List;)Ljava/util/ArrayList;
    invoke-static {v9, v10}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$2200(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v6

    .line 1101
    .local v6, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-le v9, v11, :cond_4

    move v0, v7

    .line 1102
    .local v0, "directSend":Z
    :goto_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsEditmode:Z
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$2300(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Z

    move-result v9

    if-eqz v9, :cond_5

    if-eqz v0, :cond_5

    .line 1103
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setSlideshowPauseViewEnable(Z)V
    invoke-static {v8, v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1600(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Z)V

    .line 1104
    sput-boolean v7, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->misSlideshowRunning:Z

    .line 1105
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 1106
    .local v3, "intent":Landroid/content/Intent;
    const-string v7, "com.sec.android.app.storycam"

    const-string v8, "com.sec.android.app.storycam.activity.VideoEditorLiteActivity"

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1108
    const-string v4, "*/*"

    .line 1109
    .local v4, "mimeType":Ljava/lang/String;
    invoke-virtual {v3, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1110
    const-string v7, "android.intent.action.EDIT"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1111
    const-string v7, "android.intent.extra.STREAM"

    invoke-virtual {v3, v7, v6}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1113
    if-eqz v3, :cond_3

    .line 1115
    :try_start_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$2400(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 1116
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1100(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1117
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1100(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->minimizeWindow()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1140
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "mimeType":Ljava/lang/String;
    :cond_3
    :goto_2
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsPresentationStarted:Z
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$2800(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1141
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isMusicEnabled()Z
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$500(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$600(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 1142
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$600(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->destroy()V

    .line 1143
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    const/4 v8, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    invoke-static {v7, v8}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$602(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    goto/16 :goto_0

    .end local v0    # "directSend":Z
    :cond_4
    move v0, v8

    .line 1101
    goto :goto_1

    .line 1118
    .restart local v0    # "directSend":Z
    .restart local v3    # "intent":Landroid/content/Intent;
    .restart local v4    # "mimeType":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 1119
    .local v2, "ex":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v2}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 1120
    .end local v2    # "ex":Landroid/content/ActivityNotFoundException;
    :catch_1
    move-exception v1

    .line 1121
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_2

    .line 1125
    .end local v1    # "e":Ljava/lang/NullPointerException;
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "mimeType":Ljava/lang/String;
    :cond_5
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setSlideshowPauseViewEnable(Z)V
    invoke-static {v9, v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1600(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Z)V

    .line 1126
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 1127
    .local v5, "request":Landroid/content/Intent;
    const-string v9, "android.intent.action.GET_CONTENT"

    invoke-virtual {v5, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1128
    const-string v9, "multi-pick"

    invoke-virtual {v5, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1129
    const-string v9, "VIDEO_CLIP_MULTIPIC"

    invoke-virtual {v5, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1130
    const-string v9, "pick-max-item"

    invoke-virtual {v5, v9, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1131
    const-string v9, "pick-min-item"

    invoke-virtual {v5, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1132
    const-string v9, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {v5, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1133
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$2500(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getPackageName()Ljava/lang/String;

    move-result-object v9

    const-class v10, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1135
    const-string v9, "*/*"

    invoke-virtual {v5, v9}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1136
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$2600(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v9

    invoke-virtual {v9, v5, v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1138
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsWaitingResponse:Z
    invoke-static {v7, v8}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$2702(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Z)Z

    goto/16 :goto_2

    .line 1147
    .end local v0    # "directSend":Z
    .end local v5    # "request":Landroid/content/Intent;
    .end local v6    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_6
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    iget-object v9, v9, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->slideShowResume:Landroid/widget/ImageButton;

    invoke-virtual {p1, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1148
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isMusicEnabled()Z
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$500(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Z

    move-result v9

    if-eqz v9, :cond_7

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$600(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    move-result-object v9

    if-eqz v9, :cond_7

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    iget-boolean v9, v9, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsMusicPaused:Z

    if-nez v9, :cond_7

    .line 1149
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$600(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    move-result-object v9

    invoke-virtual {v9, v8}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->setMusicLooping(Z)V

    .line 1150
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$600(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->playMusic()V

    .line 1152
    :cond_7
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$900(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getSlideshowVideoView()Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    move-result-object v8

    if-eqz v8, :cond_8

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$900(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getSlideshowVideoView()Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    move-result-object v8

    iget-boolean v8, v8, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->misVideoPlaying:Z

    if-eqz v8, :cond_8

    .line 1154
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$900(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getSlideshowVideoView()Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->resumeVideoPlay()V

    .line 1155
    :cond_8
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setSlideshowPauseViewEnable(Z)V
    invoke-static {v8, v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1600(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Z)V

    .line 1156
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$900(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 1157
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$900(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v8

    invoke-virtual {v8, v7}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->setIsShowing(Z)V

    goto/16 :goto_0
.end method
