.class public Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;
.super Landroid/widget/AutoCompleteTextView;
.source "CameraSearchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SearchAutoComplete"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 308
    invoke-direct {p0, p1}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 309
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 312
    invoke-direct {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 313
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 316
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 317
    return-void
.end method


# virtual methods
.method public enoughToFilter()Z
    .locals 1

    .prologue
    .line 330
    const/4 v0, 0x1

    return v0
.end method

.method public forceQuery()V
    .locals 0

    .prologue
    .line 334
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->showDropDown()V

    .line 335
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 339
    invoke-super/range {p0 .. p5}, Landroid/widget/AutoCompleteTextView;->onLayout(ZIIII)V

    .line 340
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->getWidth()I

    move-result v1

    .line 341
    .local v1, "width":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 342
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 343
    .local v0, "view":Landroid/view/ViewParent;
    if-eqz v0, :cond_0

    .line 344
    check-cast v0, Landroid/view/View;

    .end local v0    # "view":Landroid/view/ViewParent;
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 347
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->setDropDownWidth(I)V

    .line 348
    return-void
.end method

.method protected replaceText(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 325
    return-void
.end method
