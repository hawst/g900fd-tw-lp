.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLoaderConfig;
.super Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;
.source "PhotoViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlbumLoaderConfig"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 823
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLoaderConfig;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;-><init>()V

    .line 824
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLoaderConfig;->mScanFirstOnly:Z

    .line 825
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLoaderConfig;->mCheckSetVersion:Z

    .line 826
    return-void
.end method


# virtual methods
.method public setAlbumAttribute(Lcom/sec/android/gallery3d/data/MediaSet;I)I
    .locals 2
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "count"    # I

    .prologue
    const/4 v0, 0x0

    .line 830
    const/16 v1, 0x20

    iput-byte v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLoaderConfig;->mRetThmType:B

    .line 831
    iput v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLoaderConfig;->mRetLineCount:I

    .line 832
    if-lez p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
