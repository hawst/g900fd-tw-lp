.class public Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;
.super Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;
.source "MoreInfoItemCategory.java"


# instance fields
.field private mAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

.field private mExpandableLayoutContainer:Landroid/widget/FrameLayout;

.field private mListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter$DataSetChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rootView"    # Landroid/view/View;
    .param p3, "entry"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;
    .param p4, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p4, p3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;)V

    .line 22
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory$1;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter$DataSetChangeListener;

    .line 31
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCategoryAdapter;

    invoke-direct {v0, p1, p4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCategoryAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter$DataSetChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->registerDataSetChangeListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter$DataSetChangeListener;)V

    .line 33
    return-void
.end method


# virtual methods
.method public actionEditCancel()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->editCancel()V

    .line 104
    return-void
.end method

.method public actionEditDone()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->editDone()V

    .line 99
    return-void
.end method

.method public dataEdited()Z
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->isDataSetChanged()Z

    move-result v0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 50
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->getView()Landroid/view/View;

    .line 52
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mView:Landroid/view/View;

    const v9, 0x7f0f018a

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 53
    .local v7, "view":Landroid/view/View;
    if-nez v7, :cond_0

    .line 54
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mView:Landroid/view/View;

    const v9, 0x7f0f019c

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/FrameLayout;

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mExpandableLayoutContainer:Landroid/widget/FrameLayout;

    .line 56
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mContext:Landroid/content/Context;

    check-cast v8, Landroid/app/Activity;

    invoke-virtual {v8}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f03008d

    invoke-virtual {v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    .line 58
    .local v2, "expandableLayout":Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mExpandableLayoutContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v8, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 64
    :goto_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->getCount()I

    move-result v5

    .line 65
    .local v5, "size":I
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->getChildCount()I

    move-result v1

    .line 66
    .local v1, "childSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v5, :cond_2

    .line 67
    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 68
    .local v0, "child":Landroid/view/View;
    if-nez v0, :cond_1

    .line 69
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v8, v3, v10, v10}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->addView(Landroid/view/View;)V

    .line 66
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "childSize":I
    .end local v2    # "expandableLayout":Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;
    .end local v3    # "i":I
    .end local v5    # "size":I
    :cond_0
    move-object v2, v7

    .line 61
    check-cast v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    .restart local v2    # "expandableLayout":Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;
    goto :goto_0

    .line 71
    .restart local v0    # "child":Landroid/view/View;
    .restart local v1    # "childSize":I
    .restart local v3    # "i":I
    .restart local v5    # "size":I
    :cond_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v8, v3, v0, v10}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    goto :goto_2

    .line 74
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    if-le v1, v5, :cond_3

    .line 75
    sub-int v8, v1, v5

    invoke-virtual {v2, v5, v8}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->removeViews(II)V

    .line 78
    :cond_3
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mView:Landroid/view/View;

    const v9, 0x7f0f018e

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    .line 80
    .local v4, "imageButton":Landroid/widget/ImageButton;
    invoke-virtual {v4}, Landroid/widget/ImageButton;->hasOnClickListeners()Z

    move-result v8

    if-nez v8, :cond_4

    .line 81
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->getAddBtnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    :cond_4
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mView:Landroid/view/View;

    const v9, 0x7f0f018b

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 85
    .local v6, "textView":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->getTags()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->setVisibilityByViewMode()V

    .line 87
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mView:Landroid/view/View;

    return-object v8
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->getView()Landroid/view/View;

    .line 93
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mRootView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 94
    return-void
.end method

.method public setEditListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->setEditListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;)V

    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->setEditListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;)V

    .line 39
    return-void
.end method

.method public setMode(Z)V
    .locals 1
    .param p1, "editMode"    # Z

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->setMode(Z)V

    .line 44
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->setVisibilityByViewMode()V

    .line 45
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mRootView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 46
    return-void
.end method

.method protected setVisibilityByViewMode()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 108
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mView:Landroid/view/View;

    const v3, 0x7f0f018b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 109
    .local v1, "textView":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mView:Landroid/view/View;

    const v3, 0x7f0f018e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 111
    .local v0, "addButton":Landroid/widget/ImageButton;
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mEditMode:Z

    if-eqz v2, :cond_1

    .line 112
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 113
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mExpandableLayoutContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 114
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 119
    :goto_0
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 125
    :goto_1
    return-void

    .line 116
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mExpandableLayoutContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 117
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 121
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemCategory;->mExpandableLayoutContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 122
    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 123
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method
