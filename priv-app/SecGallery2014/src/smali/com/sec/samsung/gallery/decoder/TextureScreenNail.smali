.class public Lcom/sec/samsung/gallery/decoder/TextureScreenNail;
.super Ljava/lang/Object;
.source "TextureScreenNail.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/ScreenNail;


# static fields
.field private static final ANIMATION_DONE:J = -0x3L

.field private static final ANIMATION_NEEDED:J = -0x2L

.field private static final ANIMATION_NOT_NEEDED:J = -0x1L

.field private static final DURATION:I = 0xb4

.field private static final MAX_SIDE:I = 0x280

.field private static final PLACEHOLDER_COLOR:I = -0x1000000

.field private static final TAG:Ljava/lang/String; = "TextureScreenNail"


# instance fields
.field private mAnimationStartTime:J

.field private mBuffer:Ljava/nio/ByteBuffer;

.field private mCi:Lcom/sec/samsung/gallery/decoder/CacheInterface;

.field private mHeight:I

.field public mTexture:Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;

.field private mType:I

.field private mWidth:I


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/decoder/CacheInterface;III)V
    .locals 2
    .param p1, "ci"    # Lcom/sec/samsung/gallery/decoder/CacheInterface;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "type"    # I

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mAnimationStartTime:J

    .line 63
    iput-object p1, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mCi:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    .line 64
    iput p4, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mType:I

    .line 65
    invoke-direct {p0, p2, p3}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->setSize(II)V

    .line 66
    return-void
.end method

.method public constructor <init>(Lcom/sec/samsung/gallery/decoder/CacheInterface;Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;I)V
    .locals 2
    .param p1, "ci"    # Lcom/sec/samsung/gallery/decoder/CacheInterface;
    .param p2, "info"    # Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;
    .param p3, "type"    # I

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mAnimationStartTime:J

    .line 53
    iput-object p1, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mCi:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    .line 54
    iget v0, p2, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;->width:I

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mWidth:I

    .line 55
    iget v0, p2, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;->height:I

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mHeight:I

    .line 56
    iget-object v0, p2, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;->buffer:Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mBuffer:Ljava/nio/ByteBuffer;

    .line 57
    iput p3, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mType:I

    .line 60
    return-void
.end method

.method private getRatio()F
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 199
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->now()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mAnimationStartTime:J

    sub-long/2addr v2, v4

    long-to-float v1, v2

    const/high16 v2, 0x43340000    # 180.0f

    div-float v0, v1, v2

    .line 200
    .local v0, "r":F
    sub-float v1, v6, v0

    const/4 v2, 0x0

    invoke-static {v1, v2, v6}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v1

    return v1
.end method

.method private static now()J
    .locals 2

    .prologue
    .line 195
    invoke-static {}, Lcom/sec/android/gallery3d/ui/AnimationTime;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method private setSize(II)V
    .locals 4
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 69
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 70
    :cond_0
    const/16 p1, 0x280

    .line 71
    const/16 p2, 0x1e0

    .line 73
    :cond_1
    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x44200000    # 640.0f

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 74
    .local v0, "scale":F
    int-to-float v1, p1

    mul-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mWidth:I

    .line 75
    int-to-float v1, p2

    mul-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mHeight:I

    .line 76
    return-void
.end method


# virtual methods
.method public combine(Lcom/sec/android/gallery3d/ui/ScreenNail;)Lcom/sec/android/gallery3d/ui/ScreenNail;
    .locals 5
    .param p1, "other"    # Lcom/sec/android/gallery3d/ui/ScreenNail;

    .prologue
    const/4 v4, 0x0

    .line 81
    if-nez p1, :cond_0

    .line 109
    .end local p0    # "this":Lcom/sec/samsung/gallery/decoder/TextureScreenNail;
    :goto_0
    return-object p0

    .line 85
    .restart local p0    # "this":Lcom/sec/samsung/gallery/decoder/TextureScreenNail;
    :cond_0
    instance-of v1, p1, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    if-nez v1, :cond_1

    .line 86
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->recycle()V

    move-object p0, p1

    .line 87
    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 92
    check-cast v0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    .line 93
    .local v0, "newer":Lcom/sec/samsung/gallery/decoder/TextureScreenNail;
    iget v1, v0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mWidth:I

    iput v1, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mWidth:I

    .line 94
    iget v1, v0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mHeight:I

    iput v1, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mHeight:I

    .line 95
    iget-object v1, v0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mBuffer:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_3

    .line 96
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mBuffer:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_2

    .line 97
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mCi:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    iget v2, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mType:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->freeBuffer(ILjava/nio/ByteBuffer;)V

    .line 99
    :cond_2
    iget-object v1, v0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mBuffer:Ljava/nio/ByteBuffer;

    iput-object v1, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mBuffer:Ljava/nio/ByteBuffer;

    .line 100
    iput-object v4, v0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mBuffer:Ljava/nio/ByteBuffer;

    .line 102
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;

    if-eqz v1, :cond_3

    .line 103
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->recycle()V

    .line 104
    iput-object v4, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;

    .line 108
    :cond_3
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->recycle()V

    goto :goto_0
.end method

.method public draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V
    .locals 8
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mBuffer:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_1

    .line 147
    iget-wide v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mAnimationStartTime:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 150
    :cond_0
    int-to-float v1, p2

    int-to-float v2, p3

    int-to-float v3, p4

    int-to-float v4, p5

    const/high16 v5, -0x1000000

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->fillRect(FFFFI)V

    .line 168
    :goto_0
    return-void

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;

    if-nez v0, :cond_2

    .line 155
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mCi:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    new-instance v1, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;

    iget v2, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mWidth:I

    iget v3, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mHeight:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;-><init>(IILjava/nio/ByteBuffer;)V

    iget v2, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mType:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->createBufferReusableTexture(Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;IZ)Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;

    .line 158
    :cond_2
    iget-wide v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mAnimationStartTime:J

    const-wide/16 v2, -0x2

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    .line 159
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mAnimationStartTime:J

    .line 162
    :cond_3
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->isAnimating()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 163
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;

    const/high16 v2, -0x1000000

    invoke-direct {p0}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->getRatio()F

    move-result v3

    move-object v0, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    invoke-interface/range {v0 .. v7}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawMixed(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IFIIII)V

    goto :goto_0

    .line 166
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    goto :goto_0
.end method

.method public draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "source"    # Landroid/graphics/RectF;
    .param p3, "dest"    # Landroid/graphics/RectF;

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mBuffer:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_0

    .line 173
    iget v1, p3, Landroid/graphics/RectF;->left:F

    iget v2, p3, Landroid/graphics/RectF;->top:F

    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v4

    const/high16 v5, -0x1000000

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->fillRect(FFFFI)V

    .line 183
    :goto_0
    return-void

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;

    if-nez v0, :cond_1

    .line 179
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mCi:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    new-instance v1, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;

    iget v2, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mWidth:I

    iget v3, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mHeight:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;-><init>(IILjava/nio/ByteBuffer;)V

    iget v2, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mType:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->createBufferReusableTexture(Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;IZ)Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;

    .line 182
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;

    invoke-interface {p1, v0, p2, p3}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    goto :goto_0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mHeight:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mWidth:I

    return v0
.end method

.method public isAnimating()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 186
    iget-wide v2, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mAnimationStartTime:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 191
    :goto_0
    return v0

    .line 187
    :cond_0
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->now()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mAnimationStartTime:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0xb4

    cmp-long v1, v2, v4

    if-ltz v1, :cond_1

    .line 188
    const-wide/16 v2, -0x3

    iput-wide v2, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mAnimationStartTime:J

    goto :goto_0

    .line 191
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isShowingPlaceholder()Z
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mBuffer:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->isAnimating()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public noDraw()V
    .locals 0

    .prologue
    .line 130
    return-void
.end method

.method public recycle()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 134
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;->recycle()V

    .line 136
    iput-object v3, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mBuffer:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_1

    .line 139
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mCi:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    iget v1, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mType:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->freeBuffer(ILjava/nio/ByteBuffer;)V

    .line 140
    iput-object v3, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mBuffer:Ljava/nio/ByteBuffer;

    .line 142
    :cond_1
    return-void
.end method

.method public updatePlaceholderSize(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mBuffer:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 115
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->setSize(II)V

    goto :goto_0
.end method
