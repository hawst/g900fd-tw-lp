.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
.super Ljava/lang/Object;
.source "GallerySearchTagFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Filter"
.end annotation


# instance fields
.field public mCategoryType:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public mFilterType:Ljava/lang/String;

.field public mTag:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "filterType"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mFilterType:Ljava/lang/String;

    .line 121
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    .line 122
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mCategoryType:Ljava/util/ArrayList;

    .line 125
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mFilterType:Ljava/lang/String;

    .line 126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    .line 127
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mCategoryType:Ljava/util/ArrayList;

    .line 129
    return-void
.end method


# virtual methods
.method public addTag(Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    :cond_0
    return-void
.end method

.method public addTag(Ljava/lang/String;I)V
    .locals 3
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "categoryType"    # I

    .prologue
    .line 136
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 137
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 139
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mCategoryType:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 141
    .end local v0    # "index":I
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 152
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public removeTag(Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 144
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 145
    .local v0, "index":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 146
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 147
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mCategoryType:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 149
    :cond_0
    return-void
.end method
