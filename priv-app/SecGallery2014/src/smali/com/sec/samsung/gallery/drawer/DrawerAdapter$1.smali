.class Lcom/sec/samsung/gallery/drawer/DrawerAdapter$1;
.super Ljava/lang/Object;
.source "DrawerAdapter.java"

# interfaces
.implements Lcom/sec/samsung/gallery/drawer/IDrawerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/drawer/DrawerAdapter;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$1;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdate()V
    .locals 2

    .prologue
    .line 60
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$1;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupViewBy:Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$000(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;->getGroup()Lcom/sec/samsung/gallery/drawer/Group;

    move-result-object v0

    .line 61
    .local v0, "groupViewBy":Lcom/sec/samsung/gallery/drawer/Group;
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$1;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 62
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$1;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$1;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->notifyDataSetChanged()V

    .line 65
    return-void
.end method
