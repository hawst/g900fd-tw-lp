.class Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$2;
.super Ljava/lang/Object;
.source "FaceTagFeature.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 127
    const-string v1, "FaceTagFeature"

    const-string v2, "service onServiceConnected Called !!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    invoke-static {p2}, Lcom/android/mms/transaction/IMessageBackgroundSender$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/mms/transaction/IMessageBackgroundSender;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mMMSServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    .line 131
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mMMSServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    if-eqz v1, :cond_0

    .line 132
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mMMSServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    invoke-interface {v1}, Lcom/android/mms/transaction/IMessageBackgroundSender;->startListening()V

    .line 133
    const-string v1, "FaceTagFeature"

    const-string v2, "Start message status listening"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 135
    :catch_0
    move-exception v0

    .line 136
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 142
    const-string v1, "FaceTagFeature"

    const-string v2, "service onServiceDisconnected Called !!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mMMSServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    if-eqz v1, :cond_0

    .line 146
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mMMSServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    invoke-interface {v1}, Lcom/android/mms/transaction/IMessageBackgroundSender;->stopListening()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mMMSServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    .line 153
    return-void

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
