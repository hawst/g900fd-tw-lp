.class public Lcom/sec/samsung/gallery/haptic/HapticUtils;
.super Ljava/lang/Object;
.source "HapticUtils.java"


# static fields
.field public static final HAPTIC_DESTINATION:I = 0x1

.field public static final HAPTIC_GATHERED:I


# instance fields
.field mVibrator:Landroid/os/SystemVibrator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/haptic/HapticUtils;->mVibrator:Landroid/os/SystemVibrator;

    .line 16
    invoke-direct {p0}, Lcom/sec/samsung/gallery/haptic/HapticUtils;->initHaptic()V

    .line 17
    return-void
.end method

.method private initHaptic()V
    .locals 2

    .prologue
    .line 22
    :try_start_0
    new-instance v1, Landroid/os/SystemVibrator;

    invoke-direct {v1}, Landroid/os/SystemVibrator;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/haptic/HapticUtils;->mVibrator:Landroid/os/SystemVibrator;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    :goto_0
    return-void

    .line 23
    :catch_0
    move-exception v0

    .line 24
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public closeHaptic()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/haptic/HapticUtils;->mVibrator:Landroid/os/SystemVibrator;

    if-eqz v0, :cond_0

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/haptic/HapticUtils;->mVibrator:Landroid/os/SystemVibrator;

    .line 52
    :cond_0
    return-void
.end method

.method public playHaptic(I)V
    .locals 3
    .param p1, "num"    # I

    .prologue
    .line 29
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHaptic:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/haptic/HapticUtils;->mVibrator:Landroid/os/SystemVibrator;

    if-nez v1, :cond_1

    .line 46
    :cond_0
    :goto_0
    return-void

    .line 33
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 35
    :pswitch_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/haptic/HapticUtils;->mVibrator:Landroid/os/SystemVibrator;

    sget-object v2, Lcom/sec/samsung/gallery/haptic/HapticFileGathered;->ivt:[B

    invoke-virtual {v1, v2}, Landroid/os/SystemVibrator;->vibrateImmVibe([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 38
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/haptic/HapticUtils;->mVibrator:Landroid/os/SystemVibrator;

    sget-object v2, Lcom/sec/samsung/gallery/haptic/HapticFileMoveOnDestination;->ivt:[B

    invoke-virtual {v1, v2}, Landroid/os/SystemVibrator;->vibrateImmVibe([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 33
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
