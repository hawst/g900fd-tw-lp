.class public Lcom/sec/samsung/gallery/view/ActionBarManager;
.super Ljava/lang/Object;
.source "ActionBarManager.java"


# instance fields
.field private mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

.field private final mContext:Landroid/content/Context;

.field private final mViewState:Lcom/sec/android/gallery3d/app/ActivityState;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/ActivityState;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "viewState"    # Lcom/sec/android/gallery3d/app/ActivityState;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mContext:Landroid/content/Context;

    .line 29
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mViewState:Lcom/sec/android/gallery3d/app/ActivityState;

    .line 30
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/ActionBarManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/ActionBarManager;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    return-object v0
.end method

.method public getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    return-object v0
.end method

.method public getPopUpMenu()Landroid/widget/ListPopupWindow;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v0, :cond_0

    .line 52
    const/4 v0, 0x0

    .line 54
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->getPopUpMenu()Landroid/widget/ListPopupWindow;

    move-result-object v0

    goto :goto_0
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v0, :cond_0

    .line 44
    :goto_0
    return-void

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->hide()V

    goto :goto_0
.end method

.method public invalidateOptionsMenu()V
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/ActionBarManager$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/ActionBarManager$2;-><init>(Lcom/sec/samsung/gallery/view/ActionBarManager;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 227
    return-void
.end method

.method public onConfigChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "configuration"    # Landroid/content/res/Configuration;

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v0, :cond_0

    .line 250
    :goto_0
    return-void

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->onConfigChanged(Landroid/content/res/Configuration;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v0, :cond_0

    .line 87
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->onCreateOptionsMenu(Landroid/view/Menu;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v0, :cond_0

    .line 127
    :goto_0
    return-void

    .line 122
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0f0284

    if-ne v0, v1, :cond_1

    .line 123
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->goToHome(Landroid/content/Context;)V

    goto :goto_0

    .line 126
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->onOptionsItemSelected(Landroid/view/MenuItem;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v0, :cond_0

    .line 212
    :goto_0
    return-void

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->onPause()V

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v2, 0x7f0f0289

    const/4 v1, 0x0

    .line 90
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v0, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 93
    const v0, 0x7f0f0284

    invoke-static {p1, v0, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 95
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 97
    const v0, 0x7f0f0287

    invoke-static {p1, v0, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 98
    invoke-static {p1, v2, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 99
    const v0, 0x7f0f0257

    invoke-static {p1, v0, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 100
    const v0, 0x7f0f0271

    invoke-static {p1, v0, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 102
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->isHelpMenuAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 103
    const v0, 0x7f0f028a

    invoke-static {p1, v0, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 106
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 107
    invoke-static {p1, v2, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 108
    const v0, 0x7f0f0285

    invoke-static {p1, v0, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 110
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/StartCameraCmd;->isCameraAppEnabed(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 111
    const v0, 0x7f0f0251

    invoke-static {p1, v0, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 113
    :cond_5
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFunctionSimplification:Z

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->updateFuntionSimplificationMenu(Landroid/view/Menu;)Z

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v0, :cond_0

    .line 218
    :goto_0
    return-void

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->onResume()V

    goto :goto_0
.end method

.method public setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V
    .locals 1
    .param p1, "actionBarView"    # Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    .prologue
    .line 47
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;Z)V

    .line 48
    return-void
.end method

.method public setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;Z)V
    .locals 2
    .param p1, "actionBarView"    # Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    .param p2, "autoShow"    # Z

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mViewState:Lcom/sec/android/gallery3d/app/ActivityState;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->addObserver(Ljava/util/Observer;)V

    .line 60
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->setupButtons()V

    .line 63
    if-eqz p2, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/ActionBarManager$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/ActionBarManager$1;-><init>(Lcom/sec/samsung/gallery/view/ActionBarManager;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 72
    :cond_0
    return-void
.end method

.method public setDisplayOptions(ZZ)V
    .locals 1
    .param p1, "displayHomeAsUp"    # Z
    .param p2, "showTitle"    # Z

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->setDisplayOptions(ZZ)V

    .line 243
    return-void
.end method

.method public setOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v0, :cond_0

    .line 134
    :goto_0
    return-void

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->onOptionsItemSelected(Landroid/view/MenuItem;)V

    goto :goto_0
.end method

.method public setSelectedItemCount(I)V
    .locals 1
    .param p1, "numberOfSelectedItems"    # I

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v0, :cond_0

    .line 162
    :goto_0
    return-void

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->setSelectedItemCount(I)V

    goto :goto_0
.end method

.method public setSuggestionAdapter()V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v0, :cond_0

    .line 184
    :goto_0
    return-void

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->setSuggestionAdapter()V

    goto :goto_0
.end method

.method public setTitle(I)V
    .locals 1
    .param p1, "numberOfSelectedItems"    # I

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v0, :cond_0

    .line 148
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->setTitle(I)V

    goto :goto_0
.end method

.method public setTitle(II)V
    .locals 1
    .param p1, "numberOfSelectedItems"    # I
    .param p2, "countOfMediaItem"    # I

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v0, :cond_0

    .line 155
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->setTitle(II)V

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "albumName"    # Ljava/lang/String;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v0, :cond_0

    .line 141
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;I)V
    .locals 1
    .param p1, "albumName"    # Ljava/lang/String;
    .param p2, "numberOfItemsInAlbum"    # I

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v0, :cond_0

    .line 169
    :goto_0
    return-void

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->setTitle(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;II)V
    .locals 1
    .param p1, "itemName"    # Ljava/lang/String;
    .param p2, "currentPosition"    # I
    .param p3, "numberOfItemsInAlbum"    # I

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v0, :cond_0

    .line 177
    :goto_0
    return-void

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->setTitle(Ljava/lang/String;II)V

    goto :goto_0
.end method

.method public show()V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v0, :cond_0

    .line 37
    :goto_0
    return-void

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->show()V

    goto :goto_0
.end method

.method public updateActionbarButton()V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->updateActionbarButton()V

    .line 239
    return-void
.end method

.method public updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V
    .locals 1
    .param p1, "button"    # Lcom/sec/samsung/gallery/util/Consts$ButtonType;
    .param p2, "visible"    # I
    .param p3, "toggleImage"    # Z

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v0, :cond_0

    .line 191
    :goto_0
    return-void

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    goto :goto_0
.end method

.method public updateConfirm(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 194
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v1, :cond_0

    .line 205
    :goto_0
    return-void

    .line 198
    :cond_0
    if-nez p1, :cond_1

    .line 199
    const-string p1, ""

    .line 200
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e005a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 204
    .local v0, "format":Ljava/lang/String;
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v1, p1, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->updateConfirm(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 202
    .end local v0    # "format":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "format":Ljava/lang/String;
    goto :goto_1
.end method

.method public updateDoneButton(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v0, :cond_0

    .line 234
    :goto_0
    return-void

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ActionBarManager;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->updateDoneButton(Z)V

    goto :goto_0
.end method
