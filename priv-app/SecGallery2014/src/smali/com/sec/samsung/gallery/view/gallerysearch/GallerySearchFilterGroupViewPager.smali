.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;
.super Landroid/support/v4/view/ViewPager;
.source "GallerySearchFilterGroupViewPager.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnRefreshTagListListener;,
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;,
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;,
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnVisibilityChangeListener;,
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;
    }
.end annotation


# static fields
.field public static final FILTER_PAGE_INDEX_EVENT:I = 0x1

.field public static final FILTER_PAGE_INDEX_LOCATION:I = 0x2

.field public static final FILTER_PAGE_INDEX_MAX:I = 0x5

.field public static final FILTER_PAGE_INDEX_PERSON:I = 0x3

.field public static final FILTER_PAGE_INDEX_TIME:I = 0x0

.field public static final FILTER_PAGE_INDEX_USERTAG:I = 0x4

.field private static final LOCATION_MODE_CHANGED:Ljava/lang/String; = "android.location.MODE_CHANGED"

.field private static final MSG_TAG_LIST_RECEIVE:I = 0x64

.field private static final MSG_TAG_LIST_REFRESH:I = 0x65


# instance fields
.field protected eventTagListActive:Z

.field protected locationTagListActive:Z

.field protected mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

.field private mAnchorView:Landroid/view/View;

.field private mArrowView:Landroid/view/View;

.field protected mEventTagList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation
.end field

.field mFilterClickListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;

.field final mHandler:Landroid/os/Handler;

.field private mIsFinishInit:Z

.field protected mLocationTagList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation
.end field

.field private mNeedToRedrawArrowIcon:Z

.field private mNeedToRestorePanel:Z

.field private mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private mParentFrame:Landroid/view/View;

.field protected mPersonTagList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation
.end field

.field private mRefreshTagListListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnRefreshTagListListener;

.field protected mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field protected mTimeTagList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation
.end field

.field protected mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;

.field private mUserTag:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;

.field protected mUserTagTagList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation
.end field

.field private mVisibilityChangeListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnVisibilityChangeListener;

.field protected personTagListActive:Z

.field protected userTagListActive:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 121
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 68
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUserTag:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;

    .line 70
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mAnchorView:Landroid/view/View;

    .line 72
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mParentFrame:Landroid/view/View;

    .line 74
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mArrowView:Landroid/view/View;

    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mNeedToRedrawArrowIcon:Z

    .line 78
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 80
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mVisibilityChangeListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnVisibilityChangeListener;

    .line 84
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mNeedToRestorePanel:Z

    .line 86
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mLocationTagList:Ljava/util/ArrayList;

    .line 87
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mPersonTagList:Ljava/util/ArrayList;

    .line 88
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mEventTagList:Ljava/util/ArrayList;

    .line 89
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUserTagTagList:Ljava/util/ArrayList;

    .line 90
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mTimeTagList:Ljava/util/ArrayList;

    .line 92
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->locationTagListActive:Z

    .line 93
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->personTagListActive:Z

    .line 94
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->eventTagListActive:Z

    .line 95
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->userTagListActive:Z

    .line 96
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mIsFinishInit:Z

    .line 97
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;

    .line 100
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    .line 102
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$1;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mHandler:Landroid/os/Handler;

    .line 664
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$4;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mFilterClickListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;

    .line 680
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$5;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mRefreshTagListListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnRefreshTagListListener;

    .line 122
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->initTagTask()V

    .line 123
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 126
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUserTag:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;

    .line 70
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mAnchorView:Landroid/view/View;

    .line 72
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mParentFrame:Landroid/view/View;

    .line 74
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mArrowView:Landroid/view/View;

    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mNeedToRedrawArrowIcon:Z

    .line 78
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 80
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mVisibilityChangeListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnVisibilityChangeListener;

    .line 84
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mNeedToRestorePanel:Z

    .line 86
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mLocationTagList:Ljava/util/ArrayList;

    .line 87
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mPersonTagList:Ljava/util/ArrayList;

    .line 88
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mEventTagList:Ljava/util/ArrayList;

    .line 89
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUserTagTagList:Ljava/util/ArrayList;

    .line 90
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mTimeTagList:Ljava/util/ArrayList;

    .line 92
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->locationTagListActive:Z

    .line 93
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->personTagListActive:Z

    .line 94
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->eventTagListActive:Z

    .line 95
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->userTagListActive:Z

    .line 96
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mIsFinishInit:Z

    .line 97
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;

    .line 100
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    .line 102
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$1;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mHandler:Landroid/os/Handler;

    .line 664
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$4;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mFilterClickListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;

    .line 680
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$5;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mRefreshTagListListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnRefreshTagListListener;

    .line 127
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->initTagTask()V

    .line 128
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->setTagDataList()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->updateItemsInDetailPagerView()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;
    .param p1, "x1"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->setArrowPosition(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUserTag:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;

    return-object v0
.end method

.method private findViewAndSetState(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;Z)V
    .locals 2
    .param p1, "parent"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    .param p2, "data"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    .param p3, "selected"    # Z

    .prologue
    .line 402
    invoke-virtual {p1, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->findViewByTagData(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)Landroid/view/View;

    move-result-object v0

    .line 404
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 405
    invoke-virtual {v0, p3}, Landroid/view/View;->setSelected(Z)V

    .line 407
    :cond_0
    return-void
.end method

.method private initTagTask()V
    .locals 3

    .prologue
    .line 141
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUserTag:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;

    if-nez v1, :cond_0

    .line 142
    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUserTag:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;

    .line 143
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;

    if-nez v1, :cond_1

    .line 144
    const-class v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 145
    .local v0, "threadName":Ljava/lang/String;
    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;

    invoke-direct {v1, p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;

    .line 146
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;->start()V

    .line 147
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;->notifyDirty()V

    .line 149
    .end local v0    # "threadName":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private onNotifyPageShowing(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 610
    const/4 v0, 0x5

    if-ge p1, v0, :cond_0

    .line 611
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 615
    :cond_0
    return-void
.end method

.method private setArrowPosition(I)V
    .locals 10
    .param p1, "item"    # I

    .prologue
    .line 251
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mAnchorView:Landroid/view/View;

    instance-of v8, v8, Landroid/view/ViewGroup;

    if-eqz v8, :cond_0

    .line 252
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mAnchorView:Landroid/view/View;

    check-cast v8, Landroid/view/ViewGroup;

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 254
    .local v2, "count":I
    if-lez v2, :cond_0

    if-ge p1, v2, :cond_0

    .line 255
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mAnchorView:Landroid/view/View;

    check-cast v8, Landroid/view/ViewGroup;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 257
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 258
    .local v1, "childWidth":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mParentFrame:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    .line 259
    .local v6, "parentWidth":I
    mul-int v8, v1, v2

    sub-int v8, v6, v8

    div-int v7, v8, v2

    .line 260
    .local v7, "spacing":I
    div-int/lit8 v8, v1, 0x2

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mArrowView:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    add-int v4, v8, v9

    .line 261
    .local v4, "offset":I
    if-nez p1, :cond_1

    sub-int v3, v1, v4

    .line 264
    .local v3, "marginLeft":I
    :goto_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mArrowView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 266
    .local v5, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iput v3, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 267
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mArrowView:Landroid/view/View;

    invoke-virtual {v8, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 271
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "childWidth":I
    .end local v2    # "count":I
    .end local v3    # "marginLeft":I
    .end local v4    # "offset":I
    .end local v5    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v6    # "parentWidth":I
    .end local v7    # "spacing":I
    :cond_0
    return-void

    .line 261
    .restart local v0    # "child":Landroid/view/View;
    .restart local v1    # "childWidth":I
    .restart local v2    # "count":I
    .restart local v4    # "offset":I
    .restart local v6    # "parentWidth":I
    .restart local v7    # "spacing":I
    :cond_1
    add-int v8, v1, v7

    add-int/lit8 v9, p1, 0x1

    mul-int/2addr v8, v9

    sub-int v3, v8, v4

    goto :goto_0
.end method

.method private setTagDataList()V
    .locals 1

    .prologue
    .line 697
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUserTag:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->getLocationTagData()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mLocationTagList:Ljava/util/ArrayList;

    .line 698
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUserTag:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->getPersonTagData()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mPersonTagList:Ljava/util/ArrayList;

    .line 699
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUserTag:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->getEventTagData()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mEventTagList:Ljava/util/ArrayList;

    .line 700
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUserTag:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->getUserTagData()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUserTagTagList:Ljava/util/ArrayList;

    .line 701
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUserTag:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->getTimeTagData()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mTimeTagList:Ljava/util/ArrayList;

    .line 702
    return-void
.end method

.method private setVisibilityByCurrentItem(I)Z
    .locals 6
    .param p1, "item"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 181
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mParentFrame:Landroid/view/View;

    if-eqz v4, :cond_2

    const/4 v4, 0x5

    if-ge p1, v4, :cond_2

    .line 182
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5, p1}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 185
    .local v1, "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mNeedToRestorePanel:Z

    .line 187
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getCurrentItem()I

    move-result v4

    if-ne v4, p1, :cond_1

    .line 191
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->isDisplaying()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 193
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f04000a

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 194
    .local v0, "ani":Landroid/view/animation/Animation;
    new-instance v4, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$2;

    invoke-direct {v4, p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$2;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;)V

    invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 214
    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setDisplay(Z)V

    .line 220
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mParentFrame:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 221
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mParentFrame:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 233
    .end local v0    # "ani":Landroid/view/animation/Animation;
    .end local v1    # "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    :goto_1
    return v2

    .line 216
    .restart local v1    # "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    :cond_0
    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setDisplay(Z)V

    .line 217
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f040009

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 218
    .restart local v0    # "ani":Landroid/view/animation/Animation;
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->setVisibility(I)V

    goto :goto_0

    .line 224
    .end local v0    # "ani":Landroid/view/animation/Animation;
    :cond_1
    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setDisplay(Z)V

    .line 225
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f040008

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 226
    .restart local v0    # "ani":Landroid/view/animation/Animation;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mParentFrame:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 227
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->setVisibility(I)V

    goto :goto_1

    .end local v0    # "ani":Landroid/view/animation/Animation;
    .end local v1    # "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    :cond_2
    move v2, v3

    .line 233
    goto :goto_1
.end method

.method private updateAnchorView()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 365
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mAnchorView:Landroid/view/View;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mAnchorView:Landroid/view/View;

    instance-of v6, v6, Landroid/view/ViewGroup;

    if-eqz v6, :cond_3

    .line 366
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mAnchorView:Landroid/view/View;

    check-cast v3, Landroid/view/ViewGroup;

    .line 368
    .local v3, "parent":Landroid/view/ViewGroup;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v6, 0x5

    if-ge v1, v6, :cond_3

    .line 369
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v1}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 373
    .local v2, "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 374
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    move v0, v4

    .line 378
    .local v0, "enabled":Z
    :goto_1
    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 380
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getCurrentItem()I

    move-result v6

    if-ne v6, v1, :cond_0

    .line 381
    if-nez v0, :cond_2

    .line 382
    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/view/View;->setSelected(Z)V

    .line 384
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getVisibility()I

    move-result v6

    if-nez v6, :cond_0

    .line 385
    const/16 v6, 0x8

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->setVisibility(I)V

    .line 387
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mNeedToRestorePanel:Z

    .line 368
    .end local v0    # "enabled":Z
    :cond_0
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v0, v5

    .line 374
    goto :goto_1

    .line 389
    .restart local v0    # "enabled":Z
    :cond_2
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mNeedToRestorePanel:Z

    if-eqz v6, :cond_0

    .line 390
    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/view/View;->setSelected(Z)V

    .line 391
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->setVisibility(I)V

    .line 393
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mNeedToRestorePanel:Z

    goto :goto_2

    .line 399
    .end local v0    # "enabled":Z
    .end local v1    # "i":I
    .end local v2    # "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    .end local v3    # "parent":Landroid/view/ViewGroup;
    :cond_3
    return-void
.end method

.method private updateItemsInDetailPagerView()V
    .locals 2

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->reloadAllItems()V

    .line 157
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->updateItemState(Ljava/util/ArrayList;Z)V

    .line 158
    return-void
.end method


# virtual methods
.method public getVisibility()I
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mParentFrame:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mParentFrame:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    .line 415
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/support/v4/view/ViewPager;->getVisibility()I

    move-result v0

    goto :goto_0
.end method

.method public init()V
    .locals 2

    .prologue
    .line 131
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)V

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 132
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 133
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->getAdapter()Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    .line 134
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mRefreshTagListListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnRefreshTagListListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->setRefreshTagListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnRefreshTagListListener;)V

    .line 135
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mParentFrame:Landroid/view/View;

    .line 136
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mParentFrame:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 137
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mParentFrame:Landroid/view/View;

    const v1, 0x7f0f0108

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mArrowView:Landroid/view/View;

    .line 138
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 548
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 550
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mNeedToRedrawArrowIcon:Z

    .line 551
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    .line 620
    const/4 v0, 0x0

    return v0
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "oldLeft"    # I
    .param p7, "oldTop"    # I
    .param p8, "oldRight"    # I
    .param p9, "oldBottom"    # I

    .prologue
    .line 556
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mNeedToRedrawArrowIcon:Z

    if-eqz v0, :cond_0

    .line 557
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getCurrentItem()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->setArrowPosition(I)V

    .line 558
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mNeedToRedrawArrowIcon:Z

    .line 560
    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 541
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v4/view/ViewPager;->onSizeChanged(IIII)V

    .line 543
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mNeedToRedrawArrowIcon:Z

    .line 544
    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 3
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 597
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/ViewPager;->onVisibilityChanged(Landroid/view/View;I)V

    .line 599
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mVisibilityChangeListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnVisibilityChangeListener;

    if-eqz v0, :cond_0

    .line 600
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mParentFrame:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 601
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mVisibilityChangeListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnVisibilityChangeListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mParentFrame:Landroid/view/View;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mParentFrame:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnVisibilityChangeListener;->onVisibilityChange(Landroid/view/View;I)V

    .line 607
    :cond_0
    :goto_0
    return-void

    .line 604
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mVisibilityChangeListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnVisibilityChangeListener;

    invoke-interface {v0, p1, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnVisibilityChangeListener;->onVisibilityChange(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public reloadAllItems()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 274
    const/4 v0, 0x0

    .line 277
    .local v0, "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v2}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 278
    .restart local v0    # "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mTimeTagList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 281
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v3, v2}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 282
    .restart local v0    # "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mEventTagList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 285
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v3, v2}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 286
    .restart local v0    # "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mLocationTagList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 289
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v3, v2}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 290
    .restart local v0    # "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mPersonTagList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 293
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v3, v2}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 294
    .restart local v0    # "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUserTagTagList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 297
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->updateAnchorView()V

    .line 298
    return-void
.end method

.method public setAnchorView(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mAnchorView:Landroid/view/View;

    .line 153
    return-void
.end method

.method public setCurrentItem(I)V
    .locals 0
    .param p1, "item"    # I

    .prologue
    .line 172
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->setVisibilityByCurrentItem(I)Z

    .line 174
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 176
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->setArrowPosition(I)V

    .line 177
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->onNotifyPageShowing(I)V

    .line 178
    return-void
.end method

.method public setCurrentItem(IZ)V
    .locals 0
    .param p1, "item"    # I
    .param p2, "smoothScroll"    # Z

    .prologue
    .line 162
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->setVisibilityByCurrentItem(I)Z

    .line 164
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 166
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->setArrowPosition(I)V

    .line 167
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->onNotifyPageShowing(I)V

    .line 168
    return-void
.end method

.method public setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .prologue
    .line 568
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 570
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$3;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)V

    invoke-super {p0, v0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 593
    return-void
.end method

.method public setOnVisibilityChangeListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnVisibilityChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnVisibilityChangeListener;

    .prologue
    .line 563
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mVisibilityChangeListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnVisibilityChangeListener;

    .line 564
    return-void
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 420
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mParentFrame:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mParentFrame:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 426
    :goto_0
    return-void

    .line 425
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateItemState(Ljava/util/ArrayList;Z)V
    .locals 13
    .param p2, "selected"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    const/4 v12, 0x0

    .line 301
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_5

    .line 303
    const/4 v7, 0x0

    .local v7, "pagePersonTag":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    const/4 v8, 0x0

    .local v8, "pageTime":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    const/4 v9, 0x0

    .local v9, "pageUsertag":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    const/4 v6, 0x0

    .local v6, "pageLocation":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    const/4 v5, 0x0

    .line 305
    .local v5, "pageEventTag":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v12, v11}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "pageTime":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    check-cast v8, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 306
    .restart local v8    # "pageTime":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setUnselectedState()V

    .line 307
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v12, v11}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "pageEventTag":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    check-cast v5, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 308
    .restart local v5    # "pageEventTag":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setUnselectedState()V

    .line 309
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v10

    const/4 v11, 0x2

    invoke-virtual {v10, v12, v11}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "pageLocation":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    check-cast v6, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 310
    .restart local v6    # "pageLocation":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setUnselectedState()V

    .line 311
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v10

    const/4 v11, 0x3

    invoke-virtual {v10, v12, v11}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "pagePersonTag":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    check-cast v7, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 312
    .restart local v7    # "pagePersonTag":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setUnselectedState()V

    .line 313
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v10

    const/4 v11, 0x4

    invoke-virtual {v10, v12, v11}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "pageUsertag":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    check-cast v9, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 314
    .restart local v9    # "pageUsertag":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setUnselectedState()V

    .line 316
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 317
    .local v1, "data":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isPersonTag()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 319
    invoke-direct {p0, v7, v1, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->findViewAndSetState(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;Z)V

    .line 322
    :cond_1
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isTimeTag()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 324
    invoke-direct {p0, v8, v1, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->findViewAndSetState(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;Z)V

    .line 327
    :cond_2
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isUserTag()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 329
    invoke-direct {p0, v9, v1, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->findViewAndSetState(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;Z)V

    .line 332
    :cond_3
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isLocationTag()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 334
    invoke-direct {p0, v6, v1, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->findViewAndSetState(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;Z)V

    .line 337
    :cond_4
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isEventTag()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 339
    invoke-direct {p0, v5, v1, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->findViewAndSetState(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;Z)V

    goto :goto_0

    .line 351
    .end local v1    # "data":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "pageEventTag":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    .end local v6    # "pageLocation":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    .end local v7    # "pagePersonTag":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    .end local v8    # "pageTime":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    .end local v9    # "pageUsertag":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    :cond_5
    const/4 v4, 0x0

    .line 352
    .local v4, "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v0

    .line 354
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_6

    .line 355
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v10

    invoke-virtual {v10, v12, v2}, Landroid/support/v4/view/PagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    check-cast v4, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 356
    .restart local v4    # "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setUnselectedState()V

    .line 354
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 361
    .end local v0    # "count":I
    .end local v2    # "i":I
    .end local v4    # "page":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
    :cond_6
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->updateAnchorView()V

    .line 362
    return-void
.end method
