.class Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$2;
.super Ljava/lang/Object;
.source "AbstractActionBarViewForSelection.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->setSelectionModeBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$2;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$2;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$100(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$2;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$100(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$2;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$100(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)Landroid/widget/CheckBox;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 130
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$2;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_UNSELECT_ALL:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->notifyObservers(Ljava/lang/Object;)V

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$2;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$100(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)Landroid/widget/CheckBox;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 133
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$2;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_SELECT_ALL:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0
.end method
