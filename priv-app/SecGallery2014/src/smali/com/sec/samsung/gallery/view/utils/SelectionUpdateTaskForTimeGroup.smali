.class public Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;
.super Landroid/os/AsyncTask;
.source "SelectionUpdateTaskForTimeGroup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field protected mContext:Landroid/content/Context;

.field private mFinishingState:Z

.field protected mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field private mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

.field protected mPosition:I

.field private mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;ILcom/sec/android/gallery3d/data/OnProgressListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "position"    # I
    .param p4, "onProgressListener"    # Lcom/sec/android/gallery3d/data/OnProgressListener;

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->mFinishingState:Z

    .line 25
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup$1;-><init>(Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 41
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->mContext:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 43
    iput p3, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->mPosition:I

    .line 44
    iput-object p4, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->mFinishingState:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;)Lcom/sec/android/gallery3d/data/OnProgressListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    return-object v0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 16
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 49
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->closeProgressDialog()V

    .line 55
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 56
    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 16
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->onCancelled(Ljava/lang/Void;)V

    return-void
.end method

.method protected onCancelled(Ljava/lang/Void;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->closeProgressDialog()V

    .line 61
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onCancelled(Ljava/lang/Object;)V

    .line 62
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 16
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/data/OnProgressListener;->onCompleted(Z)V

    .line 67
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->closeProgressDialog()V

    .line 68
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 69
    return-void
.end method

.method protected onPreExecute()V
    .locals 6

    .prologue
    .line 73
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .line 74
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->mContext:Landroid/content/Context;

    const v4, 0x7f0e0032

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->showProgressDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 75
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 76
    return-void
.end method

.method public setFinishingState()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;->mFinishingState:Z

    .line 38
    return-void
.end method
