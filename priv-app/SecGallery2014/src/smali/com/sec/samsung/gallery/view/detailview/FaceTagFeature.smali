.class public Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;
.super Ljava/lang/Object;
.source "FaceTagFeature.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;


# static fields
.field private static final ME_NAME:Ljava/lang/String; = "profile/Me"

.field private static final MSG_CHECK_SEND_BTN:I = 0x3

.field private static final MSG_EMAIL_SEND_RESPONSE:I = 0x4

.field private static final MSG_MMS_SEND_RESPONSE:I = 0x5

.field private static final ManualFaceDataIndexOffset:I = 0x64

.field private static final RAW_SQL_MAIN_DB:Landroid/net/Uri;

.field private static final SENDING_FAIL:I = 0x0

.field private static final SENDING_SUCCESS:I = 0x1

.field private static SEND_EMAIL_JOB_ID_END:I = 0x0

.field private static SEND_EMAIL_JOB_ID_START:I = 0x0

.field private static SEND_MMS_APP_ID:I = 0x0

.field private static SEND_MMS_MSG_ID_START:I = 0x0

.field private static final TAG:Ljava/lang/String; = "FaceTagFeature"

.field private static final UNKOWN_PERSON_ID:I = 0x1


# instance fields
.field private final mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mAddContactsDialog:Landroid/app/AlertDialog;

.field private final mContext:Landroid/content/Context;

.field private mEmailServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;

.field private mEmailServiceConn:Landroid/content/ServiceConnection;

.field private mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

.field private mIsClicked:Z

.field mMMSServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

.field private mMMSServiceConn:Landroid/content/ServiceConnection;

.field private mManualFaceTagUri:Landroid/net/Uri;

.field private final mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

.field private final mSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 63
    const/16 v0, 0x3e8

    sput v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->SEND_MMS_APP_ID:I

    .line 64
    const/16 v0, 0x64

    sput v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->SEND_MMS_MSG_ID_START:I

    .line 65
    const/16 v0, 0x7d0

    sput v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->SEND_EMAIL_JOB_ID_START:I

    .line 66
    const/16 v0, 0xbb7

    sput v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->SEND_EMAIL_JOB_ID_END:I

    .line 267
    sget-object v0, Lcom/sec/samsung/gallery/access/face/FaceData;->RAW_SQL_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "main"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->RAW_SQL_MAIN_DB:Landroid/net/Uri;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "photoView"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    const/4 v1, 0x0

    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mManualFaceTagUri:Landroid/net/Uri;

    .line 88
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mEmailServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    .line 89
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$1;-><init>(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mEmailServiceConn:Landroid/content/ServiceConnection;

    .line 122
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mMMSServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    .line 124
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$2;-><init>(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mMMSServiceConn:Landroid/content/ServiceConnection;

    .line 508
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mAddContactsDialog:Landroid/app/AlertDialog;

    .line 510
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mIsClicked:Z

    .line 157
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mContext:Landroid/content/Context;

    .line 158
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    .line 159
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 160
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 162
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$3;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$3;-><init>(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;Lcom/sec/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    .line 188
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Lcom/android/email/backgroundsender/IEmailBackgroundSender;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mEmailServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;Lcom/android/email/backgroundsender/IEmailBackgroundSender;)Lcom/android/email/backgroundsender/IEmailBackgroundSender;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;
    .param p1, "x1"    # Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mEmailServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->getCurrentPhoto()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mIsClicked:Z

    return v0
.end method

.method static synthetic access$402(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mIsClicked:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Lcom/sec/android/gallery3d/ui/PhotoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mAddContactsDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mAddContactsDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method private assignName(Ljava/lang/String;IZ)I
    .locals 12
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "faceId"    # I
    .param p3, "recommeded"    # Z

    .prologue
    const/4 v11, 0x0

    const/4 v2, 0x1

    .line 473
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 475
    .local v0, "cr":Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1, p1}, Lcom/sec/samsung/gallery/access/face/PersonList;->addPerson(Landroid/content/Context;Ljava/lang/String;)I

    move-result v9

    .line 479
    .local v9, "personId":I
    if-le v9, v2, :cond_1

    .line 480
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaObject;->setArcVersionNumber()V

    .line 481
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1, p2, v9}, Lcom/sec/samsung/gallery/access/face/FaceList;->setPerson(Landroid/content/Context;II)V

    .line 482
    const/4 v6, 0x0

    .line 483
    .local v6, "autoGroup":I
    const/4 v7, 0x0

    .line 485
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "auto_group"

    aput-object v4, v2, v3

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 490
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 491
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    .line 496
    :cond_0
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 499
    :goto_0
    if-lez v6, :cond_1

    .line 500
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v11, p2, v9}, Lcom/sec/samsung/gallery/access/face/FaceList;->recommendFaces(Landroid/content/Context;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;II)V

    .line 505
    .end local v6    # "autoGroup":I
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_1
    return v9

    .line 493
    .restart local v6    # "autoGroup":I
    .restart local v7    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v8

    .line 494
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 496
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1
.end method

.method private bindServiceEmail()V
    .locals 5

    .prologue
    .line 642
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mEmailServiceConn:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 648
    :goto_0
    return-void

    .line 645
    :catch_0
    move-exception v0

    .line 646
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0
.end method

.method private bindServiceMMS()V
    .locals 5

    .prologue
    .line 632
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/android/mms/transaction/IMessageBackgroundSender;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mMMSServiceConn:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 638
    :goto_0
    return-void

    .line 635
    :catch_0
    move-exception v0

    .line 636
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0
.end method

.method private getCurrentPhoto()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 3

    .prologue
    .line 621
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    if-lez v1, :cond_0

    .line 622
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 623
    .local v0, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v1, v0, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v1, :cond_0

    .line 624
    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 627
    .end local v0    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getFaceFreeDataIndexNoFeature(Landroid/net/Uri;Landroid/content/Context;)I
    .locals 10
    .param p1, "uriDB"    # Landroid/net/Uri;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 321
    const/4 v6, -0x1

    .line 322
    .local v6, "begin":I
    const/4 v8, -0x1

    .line 323
    .local v8, "faceDataIndex":I
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 324
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v9, "select face_data from faces  where face_data<0 order by face_data asc"

    .line 325
    .local v9, "sql":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .local v1, "uri":Landroid/net/Uri;
    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 326
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 328
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_1

    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 329
    const/4 v6, -0x1

    .line 331
    :cond_0
    const/4 v2, 0x0

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 332
    if-gt v8, v6, :cond_3

    .line 340
    :cond_1
    :goto_0
    if-eqz v7, :cond_2

    .line 341
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 343
    :cond_2
    invoke-static {v6, v8}, Ljava/lang/Math;->min(II)I

    move-result v2

    return v2

    .line 335
    :cond_3
    add-int/lit8 v6, v6, -0x1

    .line 336
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 340
    :catchall_0
    move-exception v2

    if-eqz v7, :cond_4

    .line 341
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2
.end method

.method private insertManualFDFace(Landroid/graphics/Rect;I)Landroid/net/Uri;
    .locals 10
    .param p1, "faceRect"    # Landroid/graphics/Rect;
    .param p2, "faceDataIndex"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x1

    .line 347
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->getCurrentPhoto()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 348
    .local v1, "curPhoto":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move-object v4, v6

    .line 376
    :goto_0
    return-object v4

    .line 352
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    .line 353
    .local v4, "uri":Landroid/net/Uri;
    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "content"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    move-object v4, v6

    .line 354
    goto :goto_0

    .line 357
    :cond_2
    invoke-virtual {v4}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    .line 358
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 360
    .local v2, "id":I
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 362
    .local v5, "values":Landroid/content/ContentValues;
    const-string v6, "image_id"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 363
    const-string v6, "person_id"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 364
    const-string v6, "recommended_id"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 365
    const-string v6, "group_id"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 366
    const-string v6, "pos_left"

    iget v7, p1, Landroid/graphics/Rect;->left:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 367
    const-string v6, "pos_top"

    iget v7, p1, Landroid/graphics/Rect;->top:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 368
    const-string v6, "pos_right"

    iget v7, p1, Landroid/graphics/Rect;->right:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 369
    const-string v6, "pos_bottom"

    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 370
    const-string v6, "face_data"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 371
    const-string v6, "auto_group"

    const/4 v7, -0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 373
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 375
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v6, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    invoke-virtual {v0, v6, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v4

    .line 376
    goto/16 :goto_0
.end method


# virtual methods
.method public assignName(Landroid/net/Uri;Z)V
    .locals 25
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "checkUserId"    # Z

    .prologue
    .line 385
    const/16 v20, 0x0

    .local v20, "name":Ljava/lang/String;
    const/16 v16, 0x0

    .line 386
    .local v16, "joinedName":Ljava/lang/String;
    const/16 v17, 0x0

    .local v17, "lookupKey":Ljava/lang/String;
    const/4 v15, 0x0

    .line 387
    .local v15, "joinedLookupKey":Ljava/lang/String;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    .line 388
    .local v21, "nameBuffer":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_8

    .line 389
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->getCurFaceId()I

    move-result v13

    .line 390
    .local v13, "faceId":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 391
    .local v4, "cr":Landroid/content/ContentResolver;
    const/4 v10, 0x0

    .line 393
    .local v10, "c":Landroid/database/Cursor;
    const/4 v5, 0x2

    :try_start_0
    new-array v6, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v7, "lookup"

    aput-object v7, v6, v5

    const/4 v5, 0x1

    const-string v7, "display_name"

    aput-object v7, v6, v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v5, p1

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 397
    if-eqz v10, :cond_0

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 398
    const/4 v5, 0x0

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 399
    const/4 v5, 0x1

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 400
    const-string v5, "Assign Name"

    const-string v6, "Assgin Name : "

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 403
    :cond_0
    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 406
    if-eqz v17, :cond_1

    const-string v5, "."

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 407
    move-object/from16 v15, v17

    .line 408
    const-string v5, "\\."

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v17, v5, v6

    .line 411
    :cond_1
    if-eqz v20, :cond_7

    if-lez v13, :cond_7

    if-eqz v17, :cond_7

    .line 413
    move-object/from16 v12, v20

    .line 414
    .local v12, "dsn":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getContactProvider()Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getProfileLookupKey()Ljava/lang/String;

    move-result-object v18

    .line 415
    .local v18, "lookupKeyMe":Ljava/lang/String;
    if-eqz v18, :cond_4

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 416
    const-string v20, "profile/Me"

    .line 421
    :goto_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/ui/PhotoView;->nameChanged(Z)V

    .line 422
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->clearCurFace()V

    .line 424
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v13, v5}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->assignName(Ljava/lang/String;IZ)I

    move-result v22

    .line 425
    .local v22, "personId":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v20

    invoke-virtual {v5, v0, v6}, Lcom/sec/android/gallery3d/ui/PhotoView;->setAssignedName(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    if-eqz v15, :cond_5

    .line 428
    new-instance v21, Ljava/lang/StringBuilder;

    .end local v21    # "nameBuffer":Ljava/lang/StringBuilder;
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    .line 429
    .restart local v21    # "nameBuffer":Ljava/lang/StringBuilder;
    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 435
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->getCurrentPhoto()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v11

    .line 436
    .local v11, "curPhoto":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz p2, :cond_3

    if-eqz v17, :cond_3

    if-eqz v11, :cond_3

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v5

    if-nez v5, :cond_3

    .line 437
    new-instance v23, Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, v23

    invoke-direct {v0, v5}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;-><init>(Landroid/content/Context;)V

    .line 439
    .local v23, "provider":Lcom/sec/samsung/gallery/access/contact/ContactProvider;
    if-nez v15, :cond_6

    move-object/from16 v5, v17

    :goto_2
    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->checkCallerIdPhoto(Ljava/lang/String;)Z

    move-result v14

    .line 442
    .local v14, "isPhotoNeeded":Z
    if-eqz v14, :cond_3

    .line 443
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v6, 0x7f0e00b2

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v12, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 445
    .local v19, "message":Ljava/lang/String;
    const v24, 0x7f0e00b4

    .line 446
    .local v24, "title":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPhone(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 447
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v6, 0x7f0e00b3

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v12, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 449
    const v24, 0x7f0e00b6

    .line 451
    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, p0

    move/from16 v1, v24

    move-object/from16 v2, v19

    move-object/from16 v3, v17

    invoke-virtual {v0, v5, v1, v2, v3}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->showAlertDialog(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 456
    .end local v14    # "isPhotoNeeded":Z
    .end local v19    # "message":Ljava/lang/String;
    .end local v23    # "provider":Lcom/sec/samsung/gallery/access/contact/ContactProvider;
    .end local v24    # "title":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    move/from16 v0, v22

    move-object/from16 v1, v16

    invoke-static {v5, v0, v1}, Lcom/sec/samsung/gallery/access/face/PersonList;->updateJoinedName(Landroid/content/Context;ILjava/lang/String;)V

    .line 458
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->setCurContactLookupKey(Ljava/lang/String;)V

    .line 467
    .end local v4    # "cr":Landroid/content/ContentResolver;
    .end local v10    # "c":Landroid/database/Cursor;
    .end local v11    # "curPhoto":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v12    # "dsn":Ljava/lang/String;
    .end local v13    # "faceId":I
    .end local v18    # "lookupKeyMe":Ljava/lang/String;
    .end local v22    # "personId":I
    :goto_3
    const-string v5, "FaceTagFeature"

    const-string v6, "Assign Name : "

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    return-void

    .line 403
    .restart local v4    # "cr":Landroid/content/ContentResolver;
    .restart local v10    # "c":Landroid/database/Cursor;
    .restart local v13    # "faceId":I
    :catchall_0
    move-exception v5

    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v5

    .line 418
    .restart local v12    # "dsn":Ljava/lang/String;
    .restart local v18    # "lookupKeyMe":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 419
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    goto/16 :goto_0

    .line 432
    .restart local v22    # "personId":I
    :cond_5
    const-string v16, ""

    goto/16 :goto_1

    .restart local v11    # "curPhoto":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v23    # "provider":Lcom/sec/samsung/gallery/access/contact/ContactProvider;
    :cond_6
    move-object v5, v15

    .line 439
    goto/16 :goto_2

    .line 460
    .end local v11    # "curPhoto":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v12    # "dsn":Ljava/lang/String;
    .end local v18    # "lookupKeyMe":Ljava/lang/String;
    .end local v22    # "personId":I
    .end local v23    # "provider":Lcom/sec/samsung/gallery/access/contact/ContactProvider;
    :cond_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0e005e

    const/4 v7, 0x1

    invoke-static {v5, v6, v7}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;II)V

    goto :goto_3

    .line 463
    .end local v4    # "cr":Landroid/content/ContentResolver;
    .end local v10    # "c":Landroid/database/Cursor;
    .end local v13    # "faceId":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0e0068

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v20

    .line 464
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/ui/PhotoView;->nameChanged(Z)V

    goto :goto_3
.end method

.method public cancelFaceScan()V
    .locals 7

    .prologue
    .line 240
    const/4 v6, 0x0

    .line 242
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/access/face/FaceData;->FACE_SCANNER_REQUEST_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "cancel"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 244
    .local v1, "faceUri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 247
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 249
    return-void

    .line 247
    .end local v1    # "faceUri":Landroid/net/Uri;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public cropManualFD(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
    .locals 11
    .param p1, "bmp"    # Landroid/graphics/Bitmap;
    .param p2, "croppedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 275
    if-nez p2, :cond_1

    .line 309
    :cond_0
    :goto_0
    return-void

    .line 277
    :cond_1
    const-string v6, "FaceTagFeature"

    const-string v7, "cropped rect is (l-%1$s, t-%2$s, r-%3$s, b-%4$s)"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget v10, p2, Landroid/graphics/Rect;->left:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget v10, p2, Landroid/graphics/Rect;->top:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    iget v10, p2, Landroid/graphics/Rect;->right:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    iget v10, p2, Landroid/graphics/Rect;->bottom:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    .line 283
    .local v0, "externalStorageDir":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 285
    sget-object v6, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->RAW_SQL_MAIN_DB:Landroid/net/Uri;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->getFaceFreeDataIndexNoFeature(Landroid/net/Uri;Landroid/content/Context;)I

    move-result v1

    .line 287
    .local v1, "facedataIndex":I
    const/4 v6, -0x1

    if-ne v1, v6, :cond_2

    .line 288
    add-int/lit8 v1, v1, -0x64

    .line 294
    :goto_1
    invoke-direct {p0, p2, v1}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->insertManualFDFace(Landroid/graphics/Rect;I)Landroid/net/Uri;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mManualFaceTagUri:Landroid/net/Uri;

    .line 297
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/.face/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 300
    .local v2, "filePath":Ljava/lang/String;
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 301
    .local v4, "fos":Ljava/io/FileOutputStream;
    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v7, 0x64

    invoke-virtual {p1, v6, v7, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 302
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 303
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v3

    .line 304
    .local v3, "fnfe":Ljava/io/FileNotFoundException;
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 290
    .end local v2    # "filePath":Ljava/lang/String;
    .end local v3    # "fnfe":Ljava/io/FileNotFoundException;
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 305
    .restart local v2    # "filePath":Ljava/lang/String;
    :catch_1
    move-exception v5

    .line 306
    .local v5, "ioe":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public disableBuddyShare()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 214
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mMMSServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    if-eqz v1, :cond_0

    .line 215
    const-string v1, "FaceTagFeature"

    const-string v2, "PhotoPage onDestroy unBind service"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mMMSServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    invoke-interface {v1}, Lcom/android/mms/transaction/IMessageBackgroundSender;->stopListening()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mMMSServiceConn:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 222
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mMMSServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    .line 224
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mEmailServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    if-eqz v1, :cond_1

    .line 226
    :try_start_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mEmailServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    invoke-interface {v1}, Lcom/android/email/backgroundsender/IEmailBackgroundSender;->stopListening()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 230
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mEmailServiceConn:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 231
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mEmailServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    .line 233
    :cond_1
    return-void

    .line 218
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 227
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 228
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public dismissAlertDialog()V
    .locals 1

    .prologue
    .line 517
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mAddContactsDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mAddContactsDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 518
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mAddContactsDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 519
    :cond_0
    return-void
.end method

.method public enableBuddyShare()V
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSmsIntentRegistered(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mMMSServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    if-nez v0, :cond_0

    .line 198
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->bindServiceMMS()V

    .line 199
    const-string v0, "FaceTagFeature"

    const-string v1, "PhotoPage enableBuddyShare MMS service"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mEmailServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    if-nez v0, :cond_1

    .line 204
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->bindServiceEmail()V

    .line 205
    const-string v0, "FaceTagFeature"

    const-string v1, "PhotoPage enableBuddyShare Email service"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    :cond_1
    return-void
.end method

.method public getManualFaceTagUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mManualFaceTagUri:Landroid/net/Uri;

    return-object v0
.end method

.method public setAsCallerId(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 256
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getCurContactLookupKey()Ljava/lang/String;

    move-result-object v0

    .line 257
    .local v0, "lookupkey":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1, p1, v0}, Lcom/sec/samsung/gallery/access/face/PersonList;->setContactPhotoEx(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 258
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPhone(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 259
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mContext:Landroid/content/Context;

    const v2, 0x7f0e00b6

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 263
    :goto_0
    return-void

    .line 261
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mContext:Landroid/content/Context;

    const v2, 0x7f0e00b5

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public setManualFaceTagUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "manualFaceTagUri"    # Landroid/net/Uri;

    .prologue
    .line 312
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mManualFaceTagUri:Landroid/net/Uri;

    .line 313
    return-void
.end method

.method public setScanPriority(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 6
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 602
    if-nez p1, :cond_1

    .line 618
    :cond_0
    :goto_0
    return-void

    .line 605
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    .line 606
    .local v2, "itemUri":Landroid/net/Uri;
    const-wide/16 v0, 0x0

    .line 608
    .local v0, "id":J
    :try_start_0
    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 612
    :goto_1
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    .line 615
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 616
    .local v3, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-virtual {v3, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 617
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-static {v4, v3, v5}, Lcom/sec/samsung/gallery/access/face/FaceInterface;->requestFaceScan(Landroid/content/Context;Ljava/util/List;Z)V

    goto :goto_0

    .line 609
    .end local v3    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :catch_0
    move-exception v4

    goto :goto_1
.end method

.method public showAlertDialog(Landroid/content/Context;ILjava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "titleId"    # I
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 526
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->showAlertDialog(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 527
    return-void
.end method

.method public showAlertDialog(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "titleId"    # I
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "lookupkey"    # Ljava/lang/String;

    .prologue
    .line 534
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v1, :cond_0

    .line 535
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getScaledFaceRectForCrop()Landroid/graphics/RectF;

    move-result-object v0

    .line 536
    .local v0, "rectScaled":Landroid/graphics/RectF;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->dismissAlertDialog()V

    .line 537
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mAddContactsDialog:Landroid/app/AlertDialog;

    .line 538
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mIsClicked:Z

    .line 539
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e0046

    new-instance v3, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$5;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$5;-><init>(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e00db

    new-instance v3, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;

    invoke-direct {v3, p0, v0, p4}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$4;-><init>(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;Landroid/graphics/RectF;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mAddContactsDialog:Landroid/app/AlertDialog;

    .line 583
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mAddContactsDialog:Landroid/app/AlertDialog;

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$6;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature$6;-><init>(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 593
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeature;->mAddContactsDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 595
    .end local v0    # "rectScaled":Landroid/graphics/RectF;
    :cond_0
    return-void
.end method
