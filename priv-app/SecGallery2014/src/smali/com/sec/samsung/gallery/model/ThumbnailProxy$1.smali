.class Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;
.super Ljava/lang/Object;
.source "ThumbnailProxy.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/model/ThumbnailProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBitmap(I)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 63
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    invoke-static {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    move-result-object v2

    array-length v2, v2

    rem-int v2, p1, v2

    aget-object v0, v1, v2

    .line 65
    .local v0, "displayMediaItem":Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->isBitmapReady()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    iget v1, v0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mIndex:I

    if-ne p1, v1, :cond_0

    .line 67
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 71
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onSizeChanged(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # invokes: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->contentSizeChanged(I)V
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$1200(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)V

    .line 141
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;
    invoke-static {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$1300(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->notifyDataSetChanged()V

    .line 142
    return-void
.end method

.method public onWindowContentChanged(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 123
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mIsActive:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$100(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mWindowContentChangedListener:Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;
    invoke-static {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$1000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 128
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mWindowContentChangedListener:Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;
    invoke-static {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$1000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;->onWindowContentChanged(I)V

    .line 130
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$200(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)I

    move-result v1

    if-lt p1, v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$300(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 131
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # invokes: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->updateSlotContent(I)V
    invoke-static {v1, p1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$1100(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 133
    :catch_0
    move-exception v0

    .line 134
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public resetAllThumbnails()V
    .locals 3

    .prologue
    .line 155
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    invoke-static {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    move-result-object v1

    if-nez v1, :cond_1

    .line 164
    :cond_0
    return-void

    .line 157
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    invoke-static {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 158
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    invoke-static {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    move-result-object v1

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    .line 159
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    invoke-static {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    move-result-object v1

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->cancelImageRequest()V

    .line 160
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    invoke-static {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    move-result-object v1

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->recycle()V

    .line 161
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    invoke-static {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 157
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public resetContentWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 146
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # setter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$202(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)I

    .line 147
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # setter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$302(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)I

    .line 148
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # setter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$402(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)I

    .line 149
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # setter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$502(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)I

    .line 150
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # setter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveRequestCount:I
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$1402(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)I

    .line 151
    return-void
.end method

.method public resetOutOfActivieThumbnail(II)V
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 168
    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    move-result-object v2

    if-nez v2, :cond_1

    .line 181
    :cond_0
    return-void

    .line 171
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    move-result-object v2

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 172
    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    move-result-object v2

    aget-object v0, v2, v1

    .line 173
    .local v0, "dispItem":Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    if-nez v0, :cond_3

    .line 171
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 175
    :cond_3
    iget v2, v0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mIndex:I

    if-lt v2, p1, :cond_4

    iget v2, v0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mIndex:I

    if-lt v2, p2, :cond_2

    .line 176
    :cond_4
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->cancelImageRequest()V

    .line 177
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->recycle()V

    .line 178
    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v3, v2, v1

    goto :goto_1
.end method

.method public setActiveWindow(II)V
    .locals 7
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v6, 0x0

    .line 76
    iget-object v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mIsActive:Z
    invoke-static {v3}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$100(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    const/4 v3, -0x1

    if-ne p2, v3, :cond_2

    .line 81
    iget-object v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # setter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I
    invoke-static {v3, v6}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$202(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)I

    .line 82
    iget-object v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # setter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I
    invoke-static {v3, v6}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$302(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)I

    .line 83
    iget-object v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # setter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I
    invoke-static {v3, v6}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$402(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)I

    .line 84
    iget-object v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # setter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I
    invoke-static {v3, v6}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$502(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)I

    goto :goto_0

    .line 92
    :cond_2
    add-int/lit8 v2, p2, 0x1

    .line 95
    .local v2, "rangeEnd":I
    if-eq p1, v2, :cond_0

    .line 98
    iget-object v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mCacheSize:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$600(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)I

    move-result v3

    add-int/2addr v3, p1

    if-lt p2, v3, :cond_3

    .line 99
    const-string v3, "ThumbnailProxy"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setActiveWindow : error (max ActiveWindow = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mCacheSize:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$600(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "), start = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", end = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 103
    :cond_3
    add-int v3, p1, v2

    div-int/lit8 v3, v3, 0x2

    iget-object v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    invoke-static {v4}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    move-result-object v4

    array-length v4, v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mSize:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$700(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    invoke-static {v5}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    move-result-object v5

    array-length v5, v5

    sub-int/2addr v4, v5

    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v3, v6, v4}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v1

    .line 106
    .local v1, "contentStart":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    invoke-static {v3}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    move-result-object v3

    array-length v3, v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mSize:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$700(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 109
    .local v0, "contentEnd":I
    if-lt p1, v1, :cond_0

    if-gt v2, v0, :cond_0

    .line 113
    iget-object v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # setter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I
    invoke-static {v3, p1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$402(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)I

    .line 114
    iget-object v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # setter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I
    invoke-static {v3, v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$502(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)I

    .line 116
    iget-object v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    const/4 v4, 0x1

    # invokes: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setContentWindow(IIZ)V
    invoke-static {v3, v1, v0, v4}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$800(Lcom/sec/samsung/gallery/model/ThumbnailProxy;IIZ)V

    .line 117
    iget-object v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mIsActive:Z
    invoke-static {v3}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$100(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 118
    iget-object v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # invokes: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->updateAllImageRequests()V
    invoke-static {v3}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$900(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)V

    goto/16 :goto_0
.end method
