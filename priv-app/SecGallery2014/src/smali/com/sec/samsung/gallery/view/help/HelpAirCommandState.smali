.class public Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;
.super Lcom/sec/samsung/gallery/view/help/HelpViewState;
.source "HelpAirCommandState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;


# static fields
.field private static final TAG:Ljava/lang/String; = "HelpAirCommandState"


# instance fields
.field private mAirButtonHoverListener:Landroid/view/View$OnHoverListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;-><init>()V

    .line 88
    new-instance v0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$3;-><init>(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mAirButtonHoverListener:Landroid/view/View$OnHoverListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->setViewAirButton()V

    return-void
.end method

.method private setAirButtonListener()V
    .locals 4

    .prologue
    .line 48
    new-instance v0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 50
    .local v0, "abv":Lcom/sec/samsung/gallery/view/common/AirButtonViewer;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->getAirButtonImpl()Lcom/samsung/android/airbutton/AirButtonImpl;

    move-result-object v1

    new-instance v2, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$1;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$1;-><init>(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/airbutton/AirButtonImpl;->setOnStateChangedListener(Lcom/samsung/android/airbutton/AirButtonImpl$OnStateChangedListener;)V

    .line 80
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->getAirButtonImpl()Lcom/samsung/android/airbutton/AirButtonImpl;

    move-result-object v1

    new-instance v2, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$2;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$2;-><init>(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/airbutton/AirButtonImpl;->setOnItemSelectedListener(Lcom/samsung/android/airbutton/AirButtonImpl$OnItemSelectedListener;)V

    .line 86
    return-void
.end method

.method private setViewAirButton()V
    .locals 15

    .prologue
    const/16 v14, 0x11

    const-wide/high16 v12, 0x3fe8000000000000L    # 0.75

    const v11, 0x7f0e02d5

    const/4 v10, 0x3

    const/high16 v9, 0x40000000    # 2.0f

    .line 107
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    const v8, 0x7f0202da

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 108
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 109
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    const v8, 0x7f0202d2

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 113
    :goto_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget v7, v7, Landroid/content/res/Configuration;->orientation:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_4

    .line 114
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 115
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mBG:Landroid/widget/ImageView;

    const v8, 0x7f0202d1

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 118
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 120
    .local v6, "res":Landroid/content/res/Resources;
    const v7, 0x7f0d01d0

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    .line 121
    .local v4, "posX":F
    const v7, 0x7f0d01d1

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 122
    .local v5, "posY":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setX(F)V

    .line 123
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v7, v5}, Landroid/widget/ImageView;->setY(F)V

    .line 125
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getVisibility()I

    move-result v7

    if-nez v7, :cond_0

    .line 126
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mText:Landroid/widget/TextView;

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setText(I)V

    .line 127
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mText:Landroid/widget/TextView;

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setGravity(I)V

    .line 130
    :cond_0
    const v7, 0x7f0d01d2

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    .line 131
    const v7, 0x7f0d01d3

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v8}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getActionBarSize(Landroid/content/Context;)I

    move-result v8

    int-to-float v8, v8

    sub-float v5, v7, v8

    .line 133
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHelpPopup:Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v4}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 134
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHelpPopup:Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v5}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 136
    neg-float v7, v4

    const v8, 0x7f0d01d0

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    add-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v8, v9

    sub-float v4, v7, v8

    .line 138
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setX(F)V

    .line 140
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    new-instance v8, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$4;

    invoke-direct {v8, p0, v6}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$4;-><init>(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;Landroid/content/res/Resources;)V

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    .line 263
    .end local v4    # "posX":F
    .end local v5    # "posY":F
    :goto_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getVisibility()I

    move-result v7

    if-nez v7, :cond_8

    .line 264
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHelpPopup:Landroid/widget/RelativeLayout;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 265
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->startBubbleAnimation()V

    .line 269
    :goto_2
    return-void

    .line 111
    .end local v6    # "res":Landroid/content/res/Resources;
    :cond_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    const v8, 0x7f0202ca

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 154
    :cond_2
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mBG:Landroid/widget/ImageView;

    const v8, 0x7f0202e3

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 155
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 156
    .restart local v6    # "res":Landroid/content/res/Resources;
    const v7, 0x7f0d01d8

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 157
    .local v3, "mTapY":I
    iget v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mWidth:I

    int-to-float v7, v7

    const/high16 v8, 0x40a00000    # 5.0f

    div-float/2addr v7, v8

    const/high16 v8, 0x40600000    # 3.5f

    mul-float/2addr v7, v8

    float-to-int v2, v7

    .line 162
    .local v2, "mTapX":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    int-to-float v8, v3

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setY(F)V

    .line 163
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int v8, v2, v8

    int-to-float v8, v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setX(F)V

    .line 164
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getVisibility()I

    move-result v7

    if-nez v7, :cond_3

    .line 165
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mText:Landroid/widget/TextView;

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setText(I)V

    .line 166
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mText:Landroid/widget/TextView;

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setGravity(I)V

    .line 168
    :cond_3
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHelpPopup:Landroid/widget/RelativeLayout;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v3

    int-to-float v8, v8

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 169
    iget v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mWidth:I

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHelpPopup:Landroid/widget/RelativeLayout;

    invoke-virtual {v8}, Landroid/widget/RelativeLayout;->getMeasuredWidth()I

    move-result v8

    sub-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHelpPopup:Landroid/widget/RelativeLayout;

    invoke-virtual {v8}, Landroid/widget/RelativeLayout;->getMeasuredWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    sub-int v1, v2, v7

    .line 172
    .local v1, "mHelpPopupOffset":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHelpPopup:Landroid/widget/RelativeLayout;

    int-to-float v8, v1

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 173
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHead:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int v8, v2, v8

    sub-int/2addr v8, v1

    int-to-float v8, v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setX(F)V

    .line 175
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    iget v8, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mWidth:I

    int-to-double v8, v8

    mul-double/2addr v8, v12

    double-to-int v8, v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setMaxWidth(I)V

    .line 176
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    iget v8, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mWidth:I

    int-to-double v8, v8

    mul-double/2addr v8, v12

    double-to-int v8, v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setMaxHeight(I)V

    .line 177
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    new-instance v8, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$5;

    invoke-direct {v8, p0, v3, v2}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$5;-><init>(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;II)V

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_1

    .line 194
    .end local v1    # "mHelpPopupOffset":I
    .end local v2    # "mTapX":I
    .end local v3    # "mTapY":I
    .end local v6    # "res":Landroid/content/res/Resources;
    :cond_4
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 195
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mBG:Landroid/widget/ImageView;

    const v8, 0x7f0202d3

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 198
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 200
    .restart local v6    # "res":Landroid/content/res/Resources;
    const v7, 0x7f0d01c6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    .line 201
    .restart local v4    # "posX":F
    const v7, 0x7f0d01c7

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 202
    .restart local v5    # "posY":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setX(F)V

    .line 203
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v7, v5}, Landroid/widget/ImageView;->setY(F)V

    .line 205
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mText:Landroid/widget/TextView;

    invoke-virtual {v7, v14}, Landroid/widget/TextView;->setGravity(I)V

    .line 207
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getVisibility()I

    move-result v7

    if-nez v7, :cond_5

    .line 208
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mText:Landroid/widget/TextView;

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setText(I)V

    .line 209
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mText:Landroid/widget/TextView;

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setGravity(I)V

    .line 212
    :cond_5
    const v7, 0x7f0d01c8

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    .line 213
    const v7, 0x7f0d01c9

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v8}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getActionBarSize(Landroid/content/Context;)I

    move-result v8

    int-to-float v8, v8

    sub-float v5, v7, v8

    .line 215
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHelpPopup:Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v4}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 216
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHelpPopup:Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v5}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 218
    neg-float v7, v4

    const v8, 0x7f0d01c6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    add-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v8, v9

    sub-float v4, v7, v8

    .line 220
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setX(F)V

    .line 222
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    new-instance v8, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$6;

    invoke-direct {v8, p0, v6}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$6;-><init>(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;Landroid/content/res/Resources;)V

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_1

    .line 234
    .end local v4    # "posX":F
    .end local v5    # "posY":F
    .end local v6    # "res":Landroid/content/res/Resources;
    :cond_6
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mBG:Landroid/widget/ImageView;

    const v8, 0x7f0202e2

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 235
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 236
    .restart local v6    # "res":Landroid/content/res/Resources;
    const v7, 0x7f0d01d9

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 237
    .restart local v3    # "mTapY":I
    iget v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mWidth:I

    div-int/lit8 v2, v7, 0x2

    .line 238
    .restart local v2    # "mTapX":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    int-to-float v8, v3

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setY(F)V

    .line 239
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int v8, v2, v8

    int-to-float v8, v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setX(F)V

    .line 241
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mText:Landroid/widget/TextView;

    invoke-virtual {v7, v14}, Landroid/widget/TextView;->setGravity(I)V

    .line 242
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getVisibility()I

    move-result v7

    if-nez v7, :cond_7

    .line 243
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mText:Landroid/widget/TextView;

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setText(I)V

    .line 244
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mText:Landroid/widget/TextView;

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setGravity(I)V

    .line 246
    :cond_7
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v7

    add-int v0, v3, v7

    .line 247
    .local v0, "helpY":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHelpPopup:Landroid/widget/RelativeLayout;

    int-to-float v8, v0

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 248
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHelpPopup:Landroid/widget/RelativeLayout;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 249
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHead:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int v8, v2, v8

    int-to-float v8, v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setX(F)V

    .line 251
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    new-instance v8, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$7;

    invoke-direct {v8, p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$7;-><init>(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;I)V

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_1

    .line 267
    .end local v0    # "helpY":I
    .end local v2    # "mTapX":I
    .end local v3    # "mTapY":I
    :cond_8
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHelpPopup:Landroid/widget/RelativeLayout;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_2
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    .line 29
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 31
    const-string v0, "Help"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHelpMode:I

    .line 32
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mViewMode:I

    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpPhotoActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/help/HelpPhotoActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mAirButtonHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 35
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->setAirButtonListener()V

    .line 37
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->setHelpView()V

    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->animationInit(Landroid/content/Context;)V

    .line 39
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->setOnClickListener()V

    .line 40
    return-void
.end method

.method protected setHelpView()V
    .locals 0

    .prologue
    .line 43
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->setHelpView()V

    .line 44
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->setViewAirButton()V

    .line 45
    return-void
.end method
