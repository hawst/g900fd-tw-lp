.class Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$5;
.super Landroid/os/Handler;
.source "SlideshowPresentationVideoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 284
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 287
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 304
    :cond_0
    :goto_0
    return-void

    .line 290
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "ACTION_ADD_GLSURFACE_VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 291
    .local v0, "i":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$800(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 294
    .end local v0    # "i":Landroid/content/Intent;
    :pswitch_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->abandonAudioFocus()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$900(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)V

    .line 295
    new-instance v1, Landroid/content/Intent;

    const-string v2, "ACTION_REMOVE_PRESENTATION_VIDEO_VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 297
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$800(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 298
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mTelephonyManager:Landroid/telephony/TelephonyManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$1000(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/telephony/TelephonyManager;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 299
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mTelephonyManager:Landroid/telephony/TelephonyManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$1000(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/telephony/TelephonyManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 300
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    const/4 v3, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mTelephonyManager:Landroid/telephony/TelephonyManager;
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$1002(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;Landroid/telephony/TelephonyManager;)Landroid/telephony/TelephonyManager;

    goto :goto_0

    .line 287
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
