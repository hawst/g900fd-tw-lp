.class Lcom/sec/samsung/gallery/view/AbstractActionBarView$4;
.super Ljava/lang/Object;
.source "AbstractActionBarView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/AbstractActionBarView;->showHomeButtonOnTopLeft()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView$4;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 160
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView$4;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f0011

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 161
    .local v0, "homeButton":Landroid/widget/ImageButton;
    if-eqz v0, :cond_0

    .line 162
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 163
    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView$4$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView$4$1;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarView$4;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView$4$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView$4$2;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarView$4;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 180
    :cond_0
    return-void
.end method
