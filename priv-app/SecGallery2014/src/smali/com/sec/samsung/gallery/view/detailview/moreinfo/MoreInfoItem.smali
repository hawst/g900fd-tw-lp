.class public Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;
.super Ljava/lang/Object;
.source "MoreInfoItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;
    }
.end annotation


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mEditListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;

.field protected mEditMode:Z

.field protected mIcon:I

.field protected mIndex:I

.field protected mInflater:Landroid/view/LayoutInflater;

.field protected mLayout:I

.field protected mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field protected mRootView:Landroid/view/View;

.field protected mTitle:I

.field protected mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rootView"    # Landroid/view/View;
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p4, "entry"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mContext:Landroid/content/Context;

    .line 36
    iget v2, p4, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;->index:I

    iput v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mIndex:I

    .line 37
    iget v2, p4, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;->layout:I

    iput v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mLayout:I

    .line 38
    iget v2, p4, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;->icon:I

    iput v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mIcon:I

    .line 39
    iget v2, p4, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;->title:I

    iput v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mTitle:I

    .line 40
    iput-object p3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 41
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mRootView:Landroid/view/View;

    .line 43
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v2, :cond_0

    .line 44
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mContext:Landroid/content/Context;

    const v3, 0x7f110039

    invoke-direct {v0, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 45
    .local v0, "ct":Landroid/view/ContextThemeWrapper;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 49
    .end local v0    # "ct":Landroid/view/ContextThemeWrapper;
    .local v1, "inflater":Landroid/view/LayoutInflater;
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mLayout:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mView:Landroid/view/View;

    .line 50
    return-void

    .line 47
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .restart local v1    # "inflater":Landroid/view/LayoutInflater;
    goto :goto_0
.end method


# virtual methods
.method public actionEditCancel()V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method public actionEditDone()V
    .locals 0

    .prologue
    .line 75
    return-void
.end method

.method public dataEdited()Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 4

    .prologue
    .line 53
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mView:Landroid/view/View;

    const v3, 0x7f0f018c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 54
    .local v0, "icon":Landroid/widget/ImageView;
    iget v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mIcon:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 55
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mView:Landroid/view/View;

    const v3, 0x7f0f018d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 56
    .local v1, "title":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mTitle:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mView:Landroid/view/View;

    return-object v2
.end method

.method public invalidate()V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public resetEditMode()V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method public setEditListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mEditListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;

    .line 66
    return-void
.end method

.method public setMode(Z)V
    .locals 0
    .param p1, "editMode"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->mEditMode:Z

    .line 62
    return-void
.end method

.method protected setVisibilityByViewMode()V
    .locals 0

    .prologue
    .line 72
    return-void
.end method
