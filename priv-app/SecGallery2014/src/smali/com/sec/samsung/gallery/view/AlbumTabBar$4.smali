.class Lcom/sec/samsung/gallery/view/AlbumTabBar$4;
.super Ljava/lang/Object;
.source "AlbumTabBar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/AlbumTabBar;->setupButtons()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/AlbumTabBar;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$4;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 286
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$4;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f0011

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 287
    .local v0, "homeButton":Landroid/widget/ImageButton;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 288
    new-instance v1, Lcom/sec/samsung/gallery/view/AlbumTabBar$4$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar$4$1;-><init>(Lcom/sec/samsung/gallery/view/AlbumTabBar$4;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 294
    new-instance v1, Lcom/sec/samsung/gallery/view/AlbumTabBar$4$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar$4$2;-><init>(Lcom/sec/samsung/gallery/view/AlbumTabBar$4;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 301
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$4;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$4;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0f0012

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    # setter for: Lcom/sec/samsung/gallery/view/AlbumTabBar;->mCameraButton:Landroid/widget/ImageButton;
    invoke-static {v2, v1}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$602(Lcom/sec/samsung/gallery/view/AlbumTabBar;Landroid/widget/ImageButton;)Landroid/widget/ImageButton;

    .line 302
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$4;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # getter for: Lcom/sec/samsung/gallery/view/AlbumTabBar;->mCameraButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$600(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Landroid/widget/ImageButton;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 303
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$4;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # getter for: Lcom/sec/samsung/gallery/view/AlbumTabBar;->mCameraButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$600(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Landroid/widget/ImageButton;

    move-result-object v1

    new-instance v2, Lcom/sec/samsung/gallery/view/AlbumTabBar$4$3;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar$4$3;-><init>(Lcom/sec/samsung/gallery/view/AlbumTabBar$4;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 310
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$4;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # getter for: Lcom/sec/samsung/gallery/view/AlbumTabBar;->mCameraButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$600(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Landroid/widget/ImageButton;

    move-result-object v1

    new-instance v2, Lcom/sec/samsung/gallery/view/AlbumTabBar$4$4;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar$4$4;-><init>(Lcom/sec/samsung/gallery/view/AlbumTabBar$4;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 317
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$4;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->showCameraButton(Z)V

    .line 318
    return-void
.end method
