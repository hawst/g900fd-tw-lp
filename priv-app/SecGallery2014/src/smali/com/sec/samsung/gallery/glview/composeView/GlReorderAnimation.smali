.class public Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlReorderAnimation.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mAddIndex:I

.field mDragIndex:I

.field mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

.field mEnd:I

.field mFocusIndex:I

.field private mObject:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;",
            ">;"
        }
    .end annotation
.end field

.field mSrcPos:Lcom/sec/android/gallery3d/glcore/GlPos;

.field mStart:I

.field private mTmpObject:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 16
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mObject:Ljava/util/HashSet;

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mTmpObject:Ljava/util/ArrayList;

    .line 18
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlPos;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/GlPos;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mSrcPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    .line 19
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlPos;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/GlPos;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 9
    .param p1, "ratio"    # F

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 137
    sub-float v4, v7, p1

    .line 139
    .local v4, "invRatio":F
    mul-float v6, v4, v4

    sub-float v4, v7, v6

    .line 141
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mObject:Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 142
    .local v5, "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    iget v6, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->msx:F

    iget v7, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mtx:F

    iget v8, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->msx:F

    sub-float/2addr v7, v8

    mul-float/2addr v7, v4

    add-float v0, v6, v7

    .line 143
    .local v0, "dx":F
    iget v6, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->msy:F

    iget v7, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mty:F

    iget v8, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->msy:F

    sub-float/2addr v7, v8

    mul-float/2addr v7, v4

    add-float v1, v6, v7

    .line 144
    .local v1, "dy":F
    iget v6, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->msz:F

    iget v7, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mtz:F

    iget v8, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->msz:F

    sub-float/2addr v7, v8

    mul-float/2addr v7, v4

    add-float v2, v6, v7

    .line 145
    .local v2, "dz":F
    iget v6, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mHndDispmnt:I

    invoke-virtual {v5, v6, v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setPos(IFFF)V

    goto :goto_0

    .line 148
    .end local v0    # "dx":F
    .end local v1    # "dy":F
    .end local v2    # "dz":F
    .end local v5    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_0
    return-void
.end method

.method public changeIndex(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;)V
    .locals 7
    .param p1, "posCtrl"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;

    .prologue
    const/4 v6, -0x1

    .line 151
    sget-object v2, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "startReorder:changeIndex focusIndex-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mFocusIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", dragIndex-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mDragIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    sget-object v2, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "startReorder:changeIndex mStart-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mStart:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mEnd-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mEnd:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mFocusIndex:I

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mDragIndex:I

    if-eq v2, v3, :cond_0

    .line 154
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mObject:Ljava/util/HashSet;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mAddIndex:I

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mFocusIndex:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mDragIndex:I

    invoke-virtual {p1, v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->changeGroupIndex(Ljava/util/HashSet;III)V

    .line 156
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mObject:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 157
    .local v1, "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    iget v2, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mHndDispmnt:I

    if-eq v2, v6, :cond_1

    .line 158
    iget v2, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mHndDispmnt:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->addPos(II)V

    .line 159
    iget v2, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mHndDispmnt:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->releasePosIndex(I)V

    .line 160
    iput v6, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mHndDispmnt:I

    goto :goto_0

    .line 163
    .end local v1    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mObject:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    .line 164
    return-void
.end method

.method protected getFocusIndex()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mFocusIndex:I

    return v0
.end method

.method protected onStop()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 115
    sget-object v2, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "startReorder onStop+"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->getLastRatio()F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    .line 133
    :goto_0
    return-void

    .line 119
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mTmpObject:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 120
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mObject:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 121
    .local v1, "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    iget v2, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mtx:F

    cmpl-float v2, v2, v4

    if-nez v2, :cond_1

    iget v2, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mty:F

    cmpl-float v2, v2, v4

    if-nez v2, :cond_1

    iget v2, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mtz:F

    cmpl-float v2, v2, v4

    if-nez v2, :cond_1

    .line 122
    iget v2, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mHndDispmnt:I

    if-ltz v2, :cond_2

    .line 123
    iget v2, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mHndDispmnt:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->releasePosIndex(I)V

    .line 125
    :cond_2
    const/4 v2, -0x1

    iput v2, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mHndDispmnt:I

    .line 126
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mTmpObject:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 129
    .end local v1    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mTmpObject:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 130
    .restart local v1    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mObject:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    .line 132
    .end local v1    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mTmpObject:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method public startReorder(IILcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;)V
    .locals 18
    .param p1, "focusIndex"    # I
    .param p2, "dragIndex"    # I
    .param p3, "posCtrl"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .prologue
    .line 28
    sget-object v14, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "startReorder: focusIndex-"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", dragIndex-"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mObject:Ljava/util/HashSet;

    invoke-virtual {v14}, Ljava/util/HashSet;->clear()V

    .line 33
    move/from16 v12, p1

    .line 34
    .local v12, "start":I
    move/from16 v11, p2

    .line 36
    .local v11, "movingIndex":I
    if-ne v12, v11, :cond_0

    .line 95
    :goto_0
    return-void

    .line 38
    :cond_0
    if-le v12, v11, :cond_2

    .line 39
    move v3, v12

    .line 40
    .local v3, "end":I
    move v12, v11

    .line 41
    const/4 v2, -0x1

    .line 47
    .local v2, "addIndex":I
    :goto_1
    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mAddIndex:I

    .line 48
    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mStart:I

    .line 49
    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mEnd:I

    .line 50
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mFocusIndex:I

    .line 51
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mDragIndex:I

    .line 53
    invoke-virtual/range {p3 .. p3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getActiveGroup()Landroid/util/SparseArray;

    move-result-object v6

    .line 57
    .local v6, "groupObjects":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;>;"
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_2
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v14

    if-ge v8, v14, :cond_4

    .line 58
    invoke-virtual {v6, v8}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 59
    .local v4, "groupObject":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    if-nez v4, :cond_3

    .line 57
    :cond_1
    :goto_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 43
    .end local v2    # "addIndex":I
    .end local v3    # "end":I
    .end local v4    # "groupObject":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    .end local v6    # "groupObjects":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;>;"
    .end local v8    # "i":I
    :cond_2
    move v3, v11

    .line 44
    .restart local v3    # "end":I
    const/4 v2, 0x1

    .restart local v2    # "addIndex":I
    goto :goto_1

    .line 61
    .restart local v4    # "groupObject":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    .restart local v6    # "groupObjects":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;>;"
    .restart local v8    # "i":I
    :cond_3
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getIndex()I

    move-result v5

    .line 62
    .local v5, "groupObjectIndex":I
    if-gt v12, v5, :cond_1

    if-gt v5, v3, :cond_1

    .line 63
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mObject:Ljava/util/HashSet;

    invoke-virtual {v14, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 67
    .end local v4    # "groupObject":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    .end local v5    # "groupObjectIndex":I
    :cond_4
    sget-object v14, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->TAG:Ljava/lang/String;

    const-string/jumbo v15, "startReorder+"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mObject:Ljava/util/HashSet;

    invoke-virtual {v14}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 69
    .local v13, "tObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    iget v14, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mHndDispmnt:I

    const/4 v15, -0x1

    if-ne v14, v15, :cond_5

    .line 70
    invoke-virtual {v13}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setPosIndex()I

    move-result v14

    iput v14, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mHndDispmnt:I

    .line 71
    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setSourcePos(FFF)V

    .line 77
    :goto_5
    invoke-virtual {v13}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getIndex()I

    move-result v14

    move/from16 v0, p2

    if-ne v14, v0, :cond_6

    .line 78
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mSrcPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-virtual {v0, v1, v14}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getGroupPosition(ILcom/sec/android/gallery3d/glcore/GlPos;)V

    .line 79
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    move-object/from16 v0, p3

    move/from16 v1, p1

    invoke-virtual {v0, v1, v14}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getGroupPosition(ILcom/sec/android/gallery3d/glcore/GlPos;)V

    .line 80
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    iget v14, v14, Lcom/sec/android/gallery3d/glcore/GlPos;->mX:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mSrcPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    iget v15, v15, Lcom/sec/android/gallery3d/glcore/GlPos;->mX:F

    sub-float/2addr v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    iget v15, v15, Lcom/sec/android/gallery3d/glcore/GlPos;->mY:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mSrcPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlPos;->mY:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlPos;->mZ:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mSrcPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlPos;->mZ:F

    move/from16 v17, v0

    sub-float v16, v16, v17

    invoke-virtual/range {v13 .. v16}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setTargetPos(FFF)V

    goto :goto_4

    .line 73
    :cond_5
    iget v7, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mHndDispmnt:I

    .line 74
    .local v7, "hnd":I
    invoke-virtual {v13, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getDX(I)F

    move-result v14

    invoke-virtual {v13, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getDY(I)F

    move-result v15

    invoke-virtual {v13, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getDZ(I)F

    move-result v16

    invoke-virtual/range {v13 .. v16}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setSourcePos(FFF)V

    goto :goto_5

    .line 84
    .end local v7    # "hnd":I
    :cond_6
    invoke-virtual {v13}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getIndex()I

    move-result v10

    .line 85
    .local v10, "index":I
    if-gt v12, v10, :cond_7

    if-gt v10, v3, :cond_7

    .line 86
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mSrcPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v14}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getGroupPosition(ILcom/sec/android/gallery3d/glcore/GlPos;)V

    .line 87
    add-int v14, v10, v2

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    move-object/from16 v0, p3

    invoke-virtual {v0, v14, v15}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getGroupPosition(ILcom/sec/android/gallery3d/glcore/GlPos;)V

    .line 88
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    iget v14, v14, Lcom/sec/android/gallery3d/glcore/GlPos;->mX:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mSrcPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    iget v15, v15, Lcom/sec/android/gallery3d/glcore/GlPos;->mX:F

    sub-float/2addr v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    iget v15, v15, Lcom/sec/android/gallery3d/glcore/GlPos;->mY:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mSrcPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlPos;->mY:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlPos;->mZ:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mSrcPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlPos;->mZ:F

    move/from16 v17, v0

    sub-float v16, v16, v17

    invoke-virtual/range {v13 .. v16}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setTargetPos(FFF)V

    goto/16 :goto_4

    .line 91
    :cond_7
    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setTargetPos(FFF)V

    goto/16 :goto_4

    .line 94
    .end local v10    # "index":I
    .end local v13    # "tObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->start()V

    goto/16 :goto_0
.end method

.method public startReturn(I)V
    .locals 7
    .param p1, "time"    # I

    .prologue
    const/4 v6, 0x0

    .line 99
    sget-object v3, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "startReturn+"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->mObject:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 101
    .local v2, "tObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    iget v3, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mHndDispmnt:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 102
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setPosIndex()I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mHndDispmnt:I

    .line 103
    invoke-virtual {v2, v6, v6, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setSourcePos(FFF)V

    .line 108
    :goto_1
    invoke-virtual {v2, v6, v6, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setTargetPos(FFF)V

    goto :goto_0

    .line 105
    :cond_0
    iget v0, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mHndDispmnt:I

    .line 106
    .local v0, "hnd":I
    invoke-virtual {v2, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getDX(I)F

    move-result v3

    invoke-virtual {v2, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getDY(I)F

    move-result v4

    invoke-virtual {v2, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getDZ(I)F

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setSourcePos(FFF)V

    goto :goto_1

    .line 110
    .end local v0    # "hnd":I
    .end local v2    # "tObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_1
    int-to-long v4, p1

    invoke-virtual {p0, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->setDuration(J)V

    .line 111
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->start()V

    .line 112
    return-void
.end method
