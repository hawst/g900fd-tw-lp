.class public Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
.super Ljava/lang/Object;
.source "MusicPlayer.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MusicPlayer"


# instance fields
.field mAFChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field mAudioManager:Landroid/media/AudioManager;

.field private mCallState:I

.field private mForcePause:Z

.field private mIsMusicAFCLoss:Z

.field private mIsMusicLoop:Z

.field private mIsMusicPausedByCall:Z

.field private mIsMusicStartPended:Z

.field private mIsOffHookOccured:Z

.field mMediaPlayer:Landroid/media/MediaPlayer;

.field mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mSlideShowPause:Z

.field mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mCallState:I

    .line 31
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsOffHookOccured:Z

    .line 33
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicStartPended:Z

    .line 35
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicPausedByCall:Z

    .line 37
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicAFCLoss:Z

    .line 39
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicLoop:Z

    .line 41
    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$1;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mAFChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 62
    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$2;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 146
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mForcePause:Z

    .line 151
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mSlideShowPause:Z

    .line 99
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 100
    const-string v1, "audio"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mAudioManager:Landroid/media/AudioManager;

    .line 101
    const-string v1, "phone"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 102
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v3, 0x20

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 105
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$3;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$3;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 116
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 117
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicLoop:Z

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setLooping(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    :goto_0
    return-void

    .line 118
    :catch_0
    move-exception v0

    .line 119
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MusicPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to play track "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 121
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mForcePause:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mSlideShowPause:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicAFCLoss:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicAFCLoss:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicStartPended:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicStartPended:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicPausedByCall:Z

    return v0
.end method

.method static synthetic access$402(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicPausedByCall:Z

    return p1
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsOffHookOccured:Z

    return p1
.end method

.method static synthetic access$602(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    .param p1, "x1"    # I

    .prologue
    .line 20
    iput p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mCallState:I

    return p1
.end method


# virtual methods
.method public destroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 191
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mAFChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 193
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 194
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 195
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mAFChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    if-eqz v0, :cond_1

    .line 199
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mAFChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 202
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_2

    .line 203
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 204
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 207
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    if-eqz v0, :cond_3

    .line 208
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 210
    :cond_3
    return-void
.end method

.method public getPlaylistUris(J)Landroid/net/Uri;
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 235
    const/4 v0, 0x0

    .line 236
    .local v0, "lUri":Landroid/net/Uri;
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 237
    return-object v0
.end method

.method public getPlidwithPlname(Landroid/content/Context;Ljava/lang/String;)J
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "plname"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 213
    const-wide/16 v8, -0x1

    .line 214
    .local v8, "thisId":J
    const/4 v7, 0x0

    .line 215
    .local v7, "thisTitle":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 217
    .local v6, "c":Landroid/database/Cursor;
    if-nez v6, :cond_2

    .line 229
    :cond_0
    :goto_0
    if-eqz v6, :cond_1

    .line 230
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 231
    :cond_1
    return-wide v8

    .line 218
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    const-string/jumbo v0, "title"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 222
    .local v10, "titleColumn":I
    :cond_3
    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 223
    if-eqz p2, :cond_4

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 224
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 225
    goto :goto_0

    .line 227
    :cond_4
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_0
.end method

.method public isAudioPathEarjack()Z
    .locals 3

    .prologue
    .line 280
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mAudioManager:Landroid/media/AudioManager;

    const-string v2, "audioParam;curDevice"

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 281
    .local v0, "path":Ljava/lang/String;
    const-string v1, "HPH"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public pauseMusic()V
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->pauseMusic(Z)V

    .line 149
    return-void
.end method

.method public pauseMusic(Z)V
    .locals 2
    .param p1, "forcePause"    # Z

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 159
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicAFCLoss:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsOffHookOccured:Z

    if-nez v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mAFChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 164
    :cond_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsOffHookOccured:Z

    if-eqz v0, :cond_2

    .line 165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicPausedByCall:Z

    .line 167
    :cond_2
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mForcePause:Z

    .line 168
    return-void
.end method

.method public playMusic()V
    .locals 4

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    .line 144
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mCallState:I

    .line 132
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mCallState:I

    if-nez v0, :cond_2

    .line 135
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mAFChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 138
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsOffHookOccured:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicAFCLoss:Z

    if-eqz v0, :cond_3

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 143
    :cond_2
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mForcePause:Z

    goto :goto_0

    .line 141
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicStartPended:Z

    goto :goto_1
.end method

.method public preparePlayWithUri(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "music"    # Ljava/lang/String;

    .prologue
    .line 246
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v4, :cond_1

    .line 247
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 248
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->stop()V

    .line 249
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->reset()V

    .line 252
    :cond_1
    const-string v4, "MusicPlayer"

    const-string v5, "music source string : "

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    const-string v4, "content://"

    invoke-virtual {p2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "android.resource://"

    invoke-virtual {p2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 255
    :cond_2
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 261
    .local v1, "musicUri":Landroid/net/Uri;
    :goto_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v4, :cond_3

    .line 262
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4, p1, v1}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 263
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 271
    :cond_3
    :goto_1
    return-void

    .line 257
    .end local v1    # "musicUri":Landroid/net/Uri;
    :cond_4
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->getPlidwithPlname(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v2

    .line 258
    .local v2, "musicId":J
    invoke-virtual {p0, v2, v3}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->getPlaylistUris(J)Landroid/net/Uri;

    move-result-object v1

    .restart local v1    # "musicUri":Landroid/net/Uri;
    goto :goto_0

    .line 265
    .end local v2    # "musicId":J
    :catch_0
    move-exception v0

    .line 266
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "MusicPlayer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "preparePlayWithUri: Unable to play track "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v4, :cond_5

    .line 268
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->release()V

    .line 269
    :cond_5
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    goto :goto_1
.end method

.method public setMusicLooping(Z)V
    .locals 1
    .param p1, "loop"    # Z

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 243
    :cond_0
    return-void
.end method

.method public slideShowPause(Z)V
    .locals 0
    .param p1, "slideShowPause"    # Z

    .prologue
    .line 153
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mSlideShowPause:Z

    .line 154
    return-void
.end method

.method public stopMusic()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mAFChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 174
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 176
    :cond_0
    return-void
.end method

.method public togglePlay()Z
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 180
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->pauseMusic()V

    .line 185
    :goto_0
    const/4 v0, 0x1

    .line 187
    :goto_1
    return v0

    .line 183
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->playMusic()V

    goto :goto_0

    .line 187
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
