.class public interface abstract Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;
.super Ljava/lang/Object;
.source "AbstractGlBaseAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ModelListener"
.end annotation


# virtual methods
.method public abstract getBitmap(I)Landroid/graphics/Bitmap;
.end method

.method public abstract onSizeChanged(I)V
.end method

.method public abstract onWindowContentChanged(I)V
.end method

.method public abstract resetAllThumbnails()V
.end method

.method public abstract resetContentWindow()V
.end method

.method public abstract resetOutOfActivieThumbnail(II)V
.end method

.method public abstract setActiveWindow(II)V
.end method
