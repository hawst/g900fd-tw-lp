.class Lcom/sec/samsung/gallery/dialog/ConfirmationDialog$2;
.super Ljava/lang/Object;
.source "ConfirmationDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->mCtx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->access$000(Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;)Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->mCtx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->access$000(Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "UNLOCK_TOUCH"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->mEvent:Lcom/sec/samsung/gallery/core/Event;
    invoke-static {v1}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->access$100(Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->notifyObservers(Ljava/lang/Object;)V

    .line 86
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->dismissDialog()V

    .line 88
    return-void
.end method
