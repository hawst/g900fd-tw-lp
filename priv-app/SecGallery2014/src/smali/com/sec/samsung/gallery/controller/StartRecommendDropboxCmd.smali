.class public Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "StartRecommendDropboxCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final ALREADY_SHOW_DROPBOX_OOBE_POPUP:I

.field private final DROPBOX_SUPPORT_PERIOD:I

.field private final DROPBOX_TOTAL_SIZE:I

.field private final TWO_MONTH_DROPBOX_POPUP_SHOW:I

.field private mContext:Landroid/content/Context;

.field final mHandler:Landroid/os/Handler;

.field private mRecommendDropboxDialog:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 20
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 25
    iput v1, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->TWO_MONTH_DROPBOX_POPUP_SHOW:I

    .line 26
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->ALREADY_SHOW_DROPBOX_OOBE_POPUP:I

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mRecommendDropboxDialog:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    .line 30
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->DROPBOX_TOTAL_SIZE:I

    .line 31
    iput v1, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->DROPBOX_SUPPORT_PERIOD:I

    .line 32
    new-instance v0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;)Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mRecommendDropboxDialog:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;
    .param p1, "x1"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->showDialog(I)V

    return-void
.end method

.method private dismissDialog()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mRecommendDropboxDialog:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mRecommendDropboxDialog:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->dismissRecommendDropboxDialog()V

    .line 118
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mRecommendDropboxDialog:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    .line 119
    return-void
.end method

.method private getValueOfRecommandDropbox()I
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mContext:Landroid/content/Context;

    const-string v1, "recommand_dropbox_show_value"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private isAfterTowMonths()Z
    .locals 7

    .prologue
    .line 84
    const-wide/16 v0, -0x1

    .line 85
    .local v0, "reservationTime":J
    const-wide/16 v4, -0x1

    .line 86
    .local v4, "todayTime":J
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mContext:Landroid/content/Context;

    const-string v6, "recommand_dropbox_time_value"

    invoke-static {v3, v6, v0, v1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadLongKey(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v0

    .line 88
    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;

    invoke-direct {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;-><init>()V

    .line 89
    .local v2, "time":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->today()J

    move-result-wide v4

    .line 90
    cmp-long v3, v4, v0

    if-ltz v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private showDialog(I)V
    .locals 7
    .param p1, "requestCode"    # I

    .prologue
    const v2, 0x7f0e04ae

    const/16 v3, 0x32

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 94
    new-instance v0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mRecommendDropboxDialog:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    .line 95
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mRecommendDropboxDialog:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->setRequestCode(I)V

    .line 96
    if-ne p1, v5, :cond_1

    .line 97
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mRecommendDropboxDialog:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->setTitle(I)V

    .line 98
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mRecommendDropboxDialog:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mContext:Landroid/content/Context;

    const v2, 0x7f0e04b2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->setMessage(Ljava/lang/String;)V

    .line 111
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mRecommendDropboxDialog:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->showRecommendDropboxDialog()V

    .line 112
    return-void

    .line 100
    :cond_1
    if-ne p1, v4, :cond_2

    .line 101
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mRecommendDropboxDialog:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    const v1, 0x7f0e04af

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->setTitle(I)V

    .line 102
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mRecommendDropboxDialog:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mContext:Landroid/content/Context;

    const v2, 0x7f0e04b3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->setMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 104
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    .line 105
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mRecommendDropboxDialog:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    const v1, 0x7f0e04b0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->setTitle(I)V

    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mRecommendDropboxDialog:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mContext:Landroid/content/Context;

    const v2, 0x7f0e04b4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->setMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 107
    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mRecommendDropboxDialog:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->setTitle(I)V

    .line 109
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mRecommendDropboxDialog:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mContext:Landroid/content/Context;

    const v2, 0x7f0e04b5

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->setMessage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showRecommandDropboxPopup()V
    .locals 6

    .prologue
    const/16 v3, 0xa

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 49
    sget-object v0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CloudIntegratorApi canShowDropboxLanding = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/cloud/integrator/CloudIntegratorApi$API;->canShowDropboxLanding(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloud/integrator/CloudIntegratorApi$API;->canShowDropboxLanding(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_1

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloud/integrator/CloudIntegratorApi$API;->canShowDropboxLanding(Landroid/content/Context;)I

    move-result v0

    if-ne v0, v4, :cond_5

    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->getValueOfRecommandDropbox()I

    move-result v0

    if-ge v0, v3, :cond_5

    .line 54
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->getValueOfRecommandDropbox()I

    move-result v0

    if-ge v0, v5, :cond_2

    .line 55
    invoke-direct {p0, v4, v4}, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->showRecommandDropboxPopup(IZ)V

    goto :goto_0

    .line 56
    :cond_2
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getStorageUsageRate()F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3fe999999999999aL    # 0.8

    cmpl-double v0, v0, v2

    if-lez v0, :cond_3

    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->getValueOfRecommandDropbox()I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    if-eqz v0, :cond_3

    .line 57
    const/4 v0, 0x3

    invoke-direct {p0, v0, v4}, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->showRecommandDropboxPopup(IZ)V

    goto :goto_0

    .line 58
    :cond_3
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->isAfterTowMonths()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->getValueOfRecommandDropbox()I

    move-result v0

    if-ne v0, v5, :cond_4

    .line 60
    invoke-direct {p0, v5, v4}, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->showRecommandDropboxPopup(IZ)V

    goto :goto_0

    .line 61
    :cond_4
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->getValueOfRecommandDropbox()I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    if-nez v0, :cond_0

    .line 62
    invoke-direct {p0, v5, v4}, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->showRecommandDropboxPopup(IZ)V

    goto :goto_0

    .line 65
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloud/integrator/CloudIntegratorApi$API;->canShowDropboxLanding(Landroid/content/Context;)I

    move-result v0

    if-ne v0, v5, :cond_0

    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->getValueOfRecommandDropbox()I

    move-result v0

    if-ge v0, v3, :cond_0

    .line 67
    const/4 v0, 0x4

    invoke-direct {p0, v0, v4}, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->showRecommandDropboxPopup(IZ)V

    goto :goto_0
.end method

.method private showRecommandDropboxPopup(IZ)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "bShow"    # Z

    .prologue
    .line 72
    if-eqz p2, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 77
    :goto_0
    return-void

    .line 75
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->dismissDialog()V

    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 2
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 42
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    .line 43
    .local v0, "params":[Ljava/lang/Object;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->mContext:Landroid/content/Context;

    .line 45
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;->showRecommandDropboxPopup()V

    .line 46
    return-void
.end method
