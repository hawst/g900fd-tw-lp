.class Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;
.super Landroid/view/OrientationEventListener;
.source "DetailViewScreenOrientation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->setRotationIcon()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;Landroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 9
    .param p1, "orientation"    # I

    .prologue
    const/16 v8, 0x32

    const/16 v7, -0x32

    const/16 v6, 0x41

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 56
    if-gez p1, :cond_1

    .line 57
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Landroid/view/MenuItem;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 58
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 59
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 65
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Landroid/view/MenuItem;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 66
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 67
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 72
    :cond_2
    const/4 v0, 0x0

    .line 73
    .local v0, "checkOrientation":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->checkAutoRotationUse()V
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$100(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)V

    .line 74
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Landroid/view/MenuItem;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 75
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mAutoRotationUse:Z
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$200(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 76
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 77
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mOrientation:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$300(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 80
    :pswitch_0
    move v0, p1

    .line 81
    if-lt v0, v6, :cond_3

    const/16 v3, 0x127

    if-gt v0, v3, :cond_3

    .line 82
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 84
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 89
    :pswitch_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mOrientation:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$300(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)I

    move-result v3

    rsub-int/lit8 v3, v3, 0x4

    mul-int/lit8 v3, v3, 0x5a

    sub-int v0, p1, v3

    .line 90
    if-ge v0, v8, :cond_4

    if-le v0, v7, :cond_4

    .line 91
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 93
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 97
    :pswitch_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mOrientation:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$300(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x5a

    sub-int v0, p1, v3

    .line 98
    if-ge v0, v6, :cond_5

    const/16 v3, -0x41

    if-le v0, v3, :cond_5

    .line 99
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 101
    :cond_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 109
    :cond_6
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v1

    .line 110
    .local v1, "isCurrentEnable":Z
    const/4 v2, 0x0

    .line 112
    .local v2, "isFutureEnable":Z
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mOrientation:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$300(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)I

    move-result v3

    packed-switch v3, :pswitch_data_1

    .line 135
    :cond_7
    :goto_1
    :pswitch_3
    if-eq v1, v2, :cond_0

    .line 136
    if-eqz v2, :cond_d

    .line 137
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 114
    :pswitch_4
    move v0, p1

    .line 115
    if-lt v0, v6, :cond_8

    const/16 v3, 0x73

    if-le v0, v3, :cond_9

    :cond_8
    const/16 v3, 0xf5

    if-lt v0, v3, :cond_7

    const/16 v3, 0x127

    if-gt v0, v3, :cond_7

    .line 117
    :cond_9
    const/4 v2, 0x1

    goto :goto_1

    .line 122
    :pswitch_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mOrientation:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$300(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)I

    move-result v3

    rsub-int/lit8 v3, v3, 0x4

    mul-int/lit8 v3, v3, 0x5a

    sub-int v0, p1, v3

    .line 123
    const/16 v3, 0x8c

    if-lt p1, v3, :cond_a

    const/16 v3, 0xdc

    if-le p1, v3, :cond_c

    .line 124
    :cond_a
    if-ge v0, v8, :cond_b

    if-gt v0, v7, :cond_7

    .line 125
    :cond_b
    const/4 v2, 0x1

    goto :goto_1

    .line 128
    :cond_c
    const/4 v2, 0x0

    .line 130
    goto :goto_1

    .line 139
    :cond_d
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 144
    .end local v1    # "isCurrentEnable":Z
    .end local v2    # "isFutureEnable":Z
    :cond_e
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 145
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 77
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 112
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method
