.class Lcom/sec/samsung/gallery/glview/GlPhotoView$20;
.super Ljava/lang/Object;
.source "GlPhotoView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;->changeFolder()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 2787
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$20;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 2
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 2795
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$20;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_TYPE_NONE:I

    iput v1, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimType:I

    .line 2796
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$20;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_STATUS_IDLE:I
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3100()I

    move-result v1

    iput v1, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimStatus:I

    .line 2797
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$20;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnConvAnim:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$402(Lcom/sec/samsung/gallery/glview/GlPhotoView;Z)Z

    .line 2798
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$20;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mEventListener:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlPhotoViewEventListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3000(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$GlPhotoViewEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2799
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$20;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mEventListener:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlPhotoViewEventListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3000(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$GlPhotoViewEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlPhotoViewEventListener;->onAlbumSwitchEnd()V

    .line 2801
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$20;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->updateArrowObject()V

    .line 2802
    return-void
.end method

.method public onAnimationRepeat(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 2806
    return-void
.end method

.method public onAnimationStart(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 2791
    return-void
.end method
