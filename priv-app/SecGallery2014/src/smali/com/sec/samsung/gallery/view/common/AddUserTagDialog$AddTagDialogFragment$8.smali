.class Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$8;
.super Ljava/lang/Object;
.source "AddUserTagDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

.field final synthetic val$alertEditText:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$8;->this$1:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$8;->val$alertEditText:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$8;->this$1:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$8;->val$alertEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mTag:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->access$102(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 301
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$8;->this$1:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    # invokes: Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->startHandleAddTag()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->access$700(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;)V

    .line 302
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$8;->this$1:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$8;->val$alertEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    # invokes: Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->hideSoftInput(Landroid/os/IBinder;)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->access$000(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;Landroid/os/IBinder;)V

    .line 304
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$8;->this$1:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 305
    return-void
.end method
