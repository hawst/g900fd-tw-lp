.class Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$4;
.super Ljava/lang/Object;
.source "SlideShowViewState.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setIndicatorVisibility(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

.field final synthetic val$visible:Z


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Z)V
    .locals 0

    .prologue
    .line 406
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$4;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    iput-boolean p2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$4;->val$visible:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 409
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$4;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$700(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 410
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 411
    .local v0, "params":Landroid/view/WindowManager$LayoutParams;
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$4;->val$visible:Z

    if-eqz v2, :cond_0

    .line 413
    const/4 v2, 0x0

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    .line 421
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 422
    return-void

    .line 415
    :cond_0
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v2, :cond_1

    .line 416
    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->getSystemUiFlagRemoveNatigation()I
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$800()I

    move-result v2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    goto :goto_0

    .line 418
    :cond_1
    const/16 v2, 0x200

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    goto :goto_0
.end method
