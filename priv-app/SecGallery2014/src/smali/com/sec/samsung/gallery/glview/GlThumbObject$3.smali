.class Lcom/sec/samsung/gallery/glview/GlThumbObject$3;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlThumbObject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlThumbObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$3;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 3
    .param p1, "ratio"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 282
    sub-float v0, v2, p1

    sub-float v1, v2, p1

    mul-float/2addr v0, v1

    sub-float p1, v2, v0

    .line 283
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$3;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    const/high16 v1, -0x41000000    # -0.5f

    mul-float/2addr v1, p1

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setDimEx(F)V

    .line 284
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$3;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 288
    return-void
.end method
