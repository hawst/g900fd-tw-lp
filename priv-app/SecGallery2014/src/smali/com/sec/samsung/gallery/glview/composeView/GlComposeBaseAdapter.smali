.class public abstract Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;
.super Ljava/lang/Object;
.source "GlComposeBaseAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;
    }
.end annotation


# static fields
.field public static final BROKEN_TYPE_IMAGE:I = 0x1

.field public static final BROKEN_TYPE_MOVIE:I = 0x2

.field public static final BROKEN_TYPE_NOT_BROKEN:I = 0x0

.field public static final FOR_ALBUMLIST:I = 0x1

.field public static final FOR_EVENTVIEW:I = 0x3

.field public static final FOR_TIMEVIEW:I = 0x2

.field public static final MAG_TYPE_LAND:I = 0x2

.field public static final MAG_TYPE_LAND_PANO:I = 0x1

.field public static final MAG_TYPE_NONE:I = 0x0

.field public static final MAG_TYPE_PORT:I = 0x3

.field public static final MAG_TYPE_SQUARE:I = 0x4

.field public static final RATIO_LAND:F = 1.333f

.field public static final RATIO_LAND_PANO:F = 2.8f

.field public static final RATIO_PORT:F = 0.666f

.field public static final RATIO_SQUARE:F = 1.0f

.field public static final REQ_DEFAULT:I = 0x0

.field public static final REQ_SHRINK:I = 0x3

.field public static final REQ_SLIDE:I = 0x1

.field public static final REQ_STRETCH:I = 0x2

.field protected static final RES_ID_ALBUM_LABEL:I = 0x1

.field protected static final RES_ID_ALBUM_LABEL_COUNT:I = 0xe

.field protected static final RES_ID_ATTR_ICON:I = 0x4

.field protected static final RES_ID_BORDER:I = 0xd

.field protected static final RES_ID_CHECKED:I = 0x9

.field protected static final RES_ID_CNT_ICON:I = 0x5

.field protected static final RES_ID_COUNT_LABEL:I = 0x2

.field protected static final RES_ID_DELETE:I = 0xb

.field protected static final RES_ID_DRAG_TEXT:I = 0x64

.field protected static final RES_ID_DRAG_TEXT_LINE1:I = 0x67

.field protected static final RES_ID_DRAG_TEXT_LINE2:I = 0x68

.field protected static final RES_ID_EVENT_LOCATION_ICON:I = 0xcb

.field protected static final RES_ID_EVENT_SUB_TITLE_TEXT:I = 0xcd

.field protected static final RES_ID_EVENT_SUGGESTION_BG:I = 0xc9

.field protected static final RES_ID_EVENT_SUGGESTION_TEXT:I = 0xca

.field protected static final RES_ID_EVENT_TITLE_TEXT:I = 0xcc

.field protected static final RES_ID_NEW_ALBUM_DIMMER:I = 0x65

.field protected static final RES_ID_NEW_ALBUM_HAND:I = 0x66

.field protected static final RES_ID_NEW_MARK:I = 0x6

.field protected static final RES_ID_NEW_TEXT:I = 0xc

.field protected static final RES_ID_PLAY_ICON:I = 0x3

.field protected static final RES_ID_SECRET:I = 0xa

.field protected static final RES_ID_SELECTED_COUNT:I = 0x7

.field protected static final RES_ID_UNKNOWN:I = 0x8

.field protected static final RES_SUBID_TEXT:I = 0x1

.field public static final ST_DISABLED:I = 0x2

.field public static final ST_SELECTED:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field public static final VIEW_ALBUM_GROUP:I = -0x1

.field public static final VIEW_ALBUM_SEL_COUNT:I = -0x4

.field public static final VIEW_ALBUM_TITLE:I = -0x2

.field public static final VIEW_ALBUM_TITLE_FIX:I = -0x3


# instance fields
.field protected mActive:Z

.field protected mAlbumMode:Z

.field protected mAllSelectMode:Z

.field public mAttribute:I

.field protected final mContext:Landroid/content/Context;

.field protected mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

.field protected mEasyMode:Z

.field protected mEventMode:Z

.field protected mGenericMotionFocus:I

.field protected mGenericMotionTitleFocus:I

.field protected mIsRTL:Z

.field protected mResources:Landroid/content/res/Resources;

.field public mRetLevel:I

.field public final mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

.field protected mTimeMode:Z

.field public mViewResult:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "config"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;
    .param p4, "modeOption"    # I

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mGenericMotionFocus:I

    .line 89
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mGenericMotionTitleFocus:I

    .line 90
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mActive:Z

    .line 91
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mAllSelectMode:Z

    .line 92
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mEasyMode:Z

    .line 93
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mAlbumMode:Z

    .line 94
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mTimeMode:Z

    .line 95
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mEventMode:Z

    .line 96
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mIsRTL:Z

    .line 112
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$1;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

    .line 120
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mContext:Landroid/content/Context;

    .line 121
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mResources:Landroid/content/res/Resources;

    .line 122
    if-ne p4, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mAlbumMode:Z

    .line 123
    const/4 v0, 0x2

    if-ne p4, v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mTimeMode:Z

    .line 124
    const/4 v0, 0x3

    if-ne p4, v0, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mEventMode:Z

    .line 125
    new-instance v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-direct {v0, p1, p2, p0, p3}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    .line 126
    return-void

    :cond_0
    move v0, v2

    .line 122
    goto :goto_0

    :cond_1
    move v0, v2

    .line 123
    goto :goto_1

    :cond_2
    move v1, v2

    .line 124
    goto :goto_2
.end method

.method protected static calcFaceCropRect(IILandroid/graphics/RectF;FFI)Landroid/graphics/Rect;
    .locals 15
    .param p0, "srcWidth"    # I
    .param p1, "srcHeight"    # I
    .param p2, "faceRect"    # Landroid/graphics/RectF;
    .param p3, "targetWidth"    # F
    .param p4, "targetHeight"    # F
    .param p5, "rotation"    # I

    .prologue
    .line 370
    if-eqz p2, :cond_3

    if-eqz p5, :cond_3

    .line 371
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v11

    .line 372
    .local v11, "w":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v7

    .line 373
    .local v7, "h":F
    move-object/from16 v0, p2

    iget v8, v0, Landroid/graphics/RectF;->left:F

    .line 374
    .local v8, "left":F
    move-object/from16 v0, p2

    iget v10, v0, Landroid/graphics/RectF;->top:F

    .line 376
    .local v10, "top":F
    const/16 v12, 0x5a

    move/from16 v0, p5

    if-eq v0, v12, :cond_0

    const/16 v12, 0xb4

    move/from16 v0, p5

    if-ne v0, v12, :cond_1

    .line 377
    :cond_0
    const/high16 v12, 0x3f800000    # 1.0f

    sub-float/2addr v12, v11

    sub-float v8, v12, v8

    .line 378
    const/high16 v12, 0x3f800000    # 1.0f

    sub-float/2addr v12, v7

    sub-float v10, v12, v10

    .line 380
    :cond_1
    move/from16 v0, p5

    rem-int/lit16 v12, v0, 0xb4

    if-eqz v12, :cond_2

    .line 382
    move v9, v10

    .line 383
    .local v9, "temp":F
    move v10, v8

    .line 384
    const/high16 v12, 0x3f800000    # 1.0f

    sub-float/2addr v12, v9

    sub-float v8, v12, v7

    .line 386
    move v9, v11

    .line 387
    move v11, v7

    .line 388
    move v7, v9

    .line 390
    .end local v9    # "temp":F
    :cond_2
    new-instance p2, Landroid/graphics/RectF;

    .end local p2    # "faceRect":Landroid/graphics/RectF;
    add-float v12, v8, v11

    add-float v13, v10, v7

    move-object/from16 v0, p2

    invoke-direct {v0, v8, v10, v12, v13}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 393
    .end local v7    # "h":F
    .end local v8    # "left":F
    .end local v10    # "top":F
    .end local v11    # "w":F
    .restart local p2    # "faceRect":Landroid/graphics/RectF;
    :cond_3
    move/from16 v0, p5

    rem-int/lit16 v12, v0, 0xb4

    if-eqz v12, :cond_4

    .line 395
    move/from16 v9, p3

    .line 396
    .restart local v9    # "temp":F
    move/from16 p3, p4

    .line 397
    float-to-int v12, v9

    int-to-float v0, v12

    move/from16 p4, v0

    .line 403
    .end local v9    # "temp":F
    :cond_4
    move/from16 v0, p1

    int-to-float v12, v0

    mul-float v12, v12, p3

    int-to-float v13, p0

    mul-float v13, v13, p4

    cmpl-float v12, v12, v13

    if-lez v12, :cond_7

    .line 405
    move v2, p0

    .line 406
    .local v2, "cropWidth":I
    int-to-float v12, p0

    mul-float v12, v12, p4

    div-float v12, v12, p3

    float-to-int v1, v12

    .line 407
    .local v1, "cropHeight":I
    const/4 v3, 0x0

    .line 408
    .local v3, "cropX":I
    sub-int v12, p1, v1

    div-int/lit8 v4, v12, 0x2

    .line 416
    .local v4, "cropY":I
    :goto_0
    if-eqz p2, :cond_6

    .line 418
    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/RectF;->left:F

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-ltz v12, :cond_5

    .line 419
    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    int-to-float v13, p0

    mul-float/2addr v12, v13

    float-to-int v5, v12

    .line 420
    .local v5, "faceCenterH":I
    div-int/lit8 v12, v2, 0x2

    sub-int v3, v5, v12

    .line 421
    if-gez v3, :cond_8

    .line 422
    const/4 v3, 0x0

    .line 427
    .end local v5    # "faceCenterH":I
    :cond_5
    :goto_1
    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/RectF;->top:F

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-ltz v12, :cond_6

    .line 428
    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    move/from16 v0, p1

    int-to-float v13, v0

    mul-float/2addr v12, v13

    float-to-int v6, v12

    .line 429
    .local v6, "faceCenterV":I
    div-int/lit8 v12, v1, 0x2

    sub-int v4, v6, v12

    .line 430
    if-gez v4, :cond_9

    .line 431
    const/4 v4, 0x0

    .line 437
    .end local v6    # "faceCenterV":I
    :cond_6
    :goto_2
    new-instance v12, Landroid/graphics/Rect;

    add-int v13, v3, v2

    add-int v14, v4, v1

    invoke-direct {v12, v3, v4, v13, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v12

    .line 411
    .end local v1    # "cropHeight":I
    .end local v2    # "cropWidth":I
    .end local v3    # "cropX":I
    .end local v4    # "cropY":I
    :cond_7
    move/from16 v1, p1

    .line 412
    .restart local v1    # "cropHeight":I
    move/from16 v0, p1

    int-to-float v12, v0

    mul-float v12, v12, p3

    div-float v12, v12, p4

    float-to-int v2, v12

    .line 413
    .restart local v2    # "cropWidth":I
    const/4 v4, 0x0

    .line 414
    .restart local v4    # "cropY":I
    sub-int v12, p0, v2

    div-int/lit8 v3, v12, 0x2

    .restart local v3    # "cropX":I
    goto :goto_0

    .line 423
    .restart local v5    # "faceCenterH":I
    :cond_8
    sub-int v12, p0, v2

    if-le v3, v12, :cond_5

    .line 424
    sub-int v3, p0, v2

    goto :goto_1

    .line 432
    .end local v5    # "faceCenterH":I
    .restart local v6    # "faceCenterV":I
    :cond_9
    sub-int v12, p1, v1

    if-le v4, v12, :cond_6

    .line 433
    sub-int v4, p1, v1

    goto :goto_2
.end method


# virtual methods
.method protected addContentListener()V
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v0, :cond_0

    .line 236
    sget-object v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->TAG:Ljava/lang/String;

    const-string v1, "addContentListener : mSource is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    :goto_0
    return-void

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    goto :goto_0
.end method

.method public applyMediaSetInfo(Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;)V
    .locals 1
    .param p1, "interInfo"    # Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;

    .prologue
    .line 326
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->applyMediaSetInfo(Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;)V

    .line 327
    return-void
.end method

.method protected calcCenterCropRect(FFFFI)Landroid/graphics/Rect;
    .locals 9
    .param p1, "width"    # F
    .param p2, "height"    # F
    .param p3, "tgtWidth"    # F
    .param p4, "tgtHeight"    # F
    .param p5, "rotation"    # I

    .prologue
    .line 472
    div-float v4, p1, p2

    .line 473
    .local v4, "srcRatio":F
    const/16 v6, 0x5a

    if-eq p5, v6, :cond_0

    const/16 v6, 0xb4

    if-ne p5, v6, :cond_1

    :cond_0
    div-float v5, p4, p3

    .line 475
    .local v5, "tgtRatio":F
    :goto_0
    cmpg-float v6, v4, v5

    if-gtz v6, :cond_2

    .line 476
    float-to-int v1, p1

    .line 477
    .local v1, "cropWidth":I
    div-float v6, p1, v5

    float-to-int v0, v6

    .line 478
    .local v0, "cropHeight":I
    const/4 v2, 0x0

    .line 479
    .local v2, "cropX":I
    int-to-float v6, v0

    sub-float v6, p2, v6

    float-to-int v6, v6

    div-int/lit8 v3, v6, 0x2

    .line 486
    .local v3, "cropY":I
    :goto_1
    new-instance v6, Landroid/graphics/Rect;

    add-int v7, v2, v1

    add-int v8, v3, v0

    invoke-direct {v6, v2, v3, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v6

    .line 473
    .end local v0    # "cropHeight":I
    .end local v1    # "cropWidth":I
    .end local v2    # "cropX":I
    .end local v3    # "cropY":I
    .end local v5    # "tgtRatio":F
    :cond_1
    div-float v5, p3, p4

    goto :goto_0

    .line 481
    .restart local v5    # "tgtRatio":F
    :cond_2
    float-to-int v0, p2

    .line 482
    .restart local v0    # "cropHeight":I
    mul-float v6, p2, v5

    float-to-int v1, v6

    .line 483
    .restart local v1    # "cropWidth":I
    const/4 v3, 0x0

    .line 484
    .restart local v3    # "cropY":I
    int-to-float v6, v1

    sub-float v6, p1, v6

    float-to-int v6, v6

    div-int/lit8 v2, v6, 0x2

    .restart local v2    # "cropX":I
    goto :goto_1
.end method

.method protected calcDynamicCropRect(II)Landroid/graphics/Rect;
    .locals 10
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v9, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    .line 443
    int-to-float v6, p1

    int-to-float v7, p2

    div-float v4, v6, v7

    .line 446
    .local v4, "ratio":F
    cmpl-float v6, v4, v8

    if-nez v6, :cond_0

    .line 447
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v9, v9, p1, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 466
    :goto_0
    return-object v6

    .line 449
    :cond_0
    cmpg-float v6, v4, v8

    if-gez v6, :cond_1

    .line 450
    const v5, 0x3f2a7efa    # 0.666f

    .line 455
    .local v5, "tgtRatio":F
    :goto_1
    cmpg-float v6, v4, v5

    if-gez v6, :cond_3

    .line 456
    move v1, p1

    .line 457
    .local v1, "cropWidth":I
    int-to-float v6, p1

    div-float/2addr v6, v5

    float-to-int v0, v6

    .line 458
    .local v0, "cropHeight":I
    const/4 v2, 0x0

    .line 459
    .local v2, "cropX":I
    sub-int v6, p2, v0

    div-int/lit8 v3, v6, 0x2

    .line 466
    .local v3, "cropY":I
    :goto_2
    new-instance v6, Landroid/graphics/Rect;

    add-int v7, v2, v1

    add-int v8, v3, v0

    invoke-direct {v6, v2, v3, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0

    .line 451
    .end local v0    # "cropHeight":I
    .end local v1    # "cropWidth":I
    .end local v2    # "cropX":I
    .end local v3    # "cropY":I
    .end local v5    # "tgtRatio":F
    :cond_1
    const v6, 0x40333333    # 2.8f

    cmpg-float v6, v4, v6

    if-gez v6, :cond_2

    .line 452
    const v5, 0x3faa9fbe    # 1.333f

    .restart local v5    # "tgtRatio":F
    goto :goto_1

    .line 454
    .end local v5    # "tgtRatio":F
    :cond_2
    const v5, 0x40333333    # 2.8f

    .restart local v5    # "tgtRatio":F
    goto :goto_1

    .line 461
    :cond_3
    move v0, p2

    .line 462
    .restart local v0    # "cropHeight":I
    int-to-float v6, p2

    mul-float/2addr v6, v5

    float-to-int v1, v6

    .line 463
    .restart local v1    # "cropWidth":I
    const/4 v3, 0x0

    .line 464
    .restart local v3    # "cropY":I
    sub-int v6, p1, v1

    div-int/lit8 v2, v6, 0x2

    .restart local v2    # "cropX":I
    goto :goto_2
.end method

.method public abstract drawDecorViewOnRequest(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/glcore/GlView;
.end method

.method public getAllCount()I
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x0

    return v0
.end method

.method public getAllItem(I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "albumIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAllItemCount(I)I
    .locals 1
    .param p1, "albumIndex"    # I

    .prologue
    .line 193
    const/4 v0, 0x0

    return v0
.end method

.method public abstract getCodeForMediaItem(Lcom/sec/android/gallery3d/data/MediaItem;)I
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x0

    return v0
.end method

.method public getCount(I)I
    .locals 1
    .param p1, "albumIndex"    # I

    .prologue
    .line 173
    const/4 v0, 0x0

    return v0
.end method

.method public getCurrentInfo(Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;)V
    .locals 1
    .param p1, "interInfo"    # Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;

    .prologue
    .line 322
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getMediaSetInfo(Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;)V

    .line 323
    return-void
.end method

.method public getCurrentState(II)I
    .locals 1
    .param p1, "position"    # I
    .param p2, "subPosition"    # I

    .prologue
    .line 212
    const/4 v0, 0x0

    return v0
.end method

.method public getFirstReloadSkipEnabled()Z
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstReloadSkip:Z

    return v0
.end method

.method public getGenericMotionFocus()I
    .locals 1

    .prologue
    .line 310
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mGenericMotionFocus:I

    return v0
.end method

.method public getGenericMotionTitleFocus()I
    .locals 1

    .prologue
    .line 318
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mGenericMotionTitleFocus:I

    return v0
.end method

.method public getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p1, "albumIndex"    # I
    .param p2, "photoIndex"    # I

    .prologue
    .line 185
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemImage(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "albumIndex"    # I
    .param p2, "photoIndex"    # I

    .prologue
    .line 197
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemRatio(I)[B
    .locals 1
    .param p1, "albumIndex"    # I

    .prologue
    .line 208
    const/4 v0, 0x0

    return-object v0
.end method

.method public getLineCount(I)I
    .locals 1
    .param p1, "albumIndex"    # I

    .prologue
    .line 181
    const/4 v0, 0x0

    return v0
.end method

.method public getMediaSetName(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 364
    const/4 v0, 0x0

    return-object v0
.end method

.method public getScreenNailImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "albumIndex"    # I

    .prologue
    .line 165
    const/4 v0, 0x0

    return-object v0
.end method

.method public getView(IILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 1
    .param p1, "position"    # I
    .param p2, "subPosition"    # I
    .param p3, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p4, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p5, "ext"    # I
    .param p6, "obj"    # Ljava/lang/Object;

    .prologue
    .line 225
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getViewInfo(IIILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;Z)Z
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 259
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mActive:Z

    return v0
.end method

.method public notifyLayoutChanged()V
    .locals 0

    .prologue
    .line 162
    return-void
.end method

.method protected onLoadData()Z
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x1

    return v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 279
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->onPause(Z)V

    .line 280
    return-void
.end method

.method public declared-synchronized onPause(Z)V
    .locals 1
    .param p1, "recycleBitmap"    # Z

    .prologue
    .line 272
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mActive:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 276
    :goto_0
    monitor-exit p0

    return-void

    .line 273
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mActive:Z

    .line 274
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->removeContentListener()V

    .line 275
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->onPause(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 272
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onResume()V
    .locals 1

    .prologue
    .line 263
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mActive:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 269
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 264
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mActive:Z

    .line 265
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->addContentListener()V

    .line 266
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->onResume()V

    .line 267
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mEasyMode:Z

    if-eqz v0, :cond_0

    .line 268
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->notifyLayoutChanged()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 263
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 283
    return-void
.end method

.method public reloadData()V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->reloadData()V

    .line 156
    return-void
.end method

.method protected removeContentListener()V
    .locals 2

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v0, :cond_0

    .line 244
    sget-object v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->TAG:Ljava/lang/String;

    const-string v1, "removeContentListener : mSource is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    :goto_0
    return-void

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->removeContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    goto :goto_0
.end method

.method public requestScreenNail(II)Z
    .locals 1
    .param p1, "code"    # I
    .param p2, "type"    # I

    .prologue
    .line 140
    const/4 v0, 0x0

    return v0
.end method

.method public requestScreenNail(Lcom/sec/android/gallery3d/data/MediaItem;I)Z
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "type"    # I

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method public requestScreenNailUrgent(II)Z
    .locals 1
    .param p1, "code"    # I
    .param p2, "type"    # I

    .prologue
    .line 148
    const/4 v0, 0x0

    return v0
.end method

.method public requestScreenNailUrgent(Lcom/sec/android/gallery3d/data/MediaItem;I)Z
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "type"    # I

    .prologue
    .line 144
    const/4 v0, 0x0

    return v0
.end method

.method public requestToNext(I)Z
    .locals 1
    .param p1, "toward"    # I

    .prologue
    .line 132
    const/4 v0, 0x0

    return v0
.end method

.method public abstract requestUpdateBitmap(III)V
.end method

.method public setActiveWindow(IIII)V
    .locals 0
    .param p1, "activeStart"    # I
    .param p2, "activeEnd"    # I
    .param p3, "contentStart"    # I
    .param p4, "contentEnd"    # I

    .prologue
    .line 129
    return-void
.end method

.method public setConfiguration(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;)V
    .locals 1
    .param p1, "config"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;

    .prologue
    .line 286
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iput-object p1, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mConfig:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;

    .line 287
    return-void
.end method

.method public setEasyMode(Z)V
    .locals 1
    .param p1, "isEasyMode"    # Z

    .prologue
    .line 294
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mEasyMode:Z

    if-eq v0, p1, :cond_0

    .line 295
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mEasyMode:Z

    .line 296
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->notifyLayoutChanged()V

    .line 298
    :cond_0
    return-void
.end method

.method public setFirstIndex(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 334
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iput p1, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstIndex:I

    .line 335
    return-void
.end method

.method public setFirstLoadingCount(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 338
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iput p1, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstLoadCount:I

    .line 339
    return-void
.end method

.method public setFirstLoadingValues(II)V
    .locals 1
    .param p1, "rowCount"    # I
    .param p2, "columnCount"    # I

    .prologue
    .line 342
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iput p1, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstLoadRowCount:I

    .line 343
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iput p2, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstLoadColumnCount:I

    .line 344
    return-void
.end method

.method public setGenericMotionFocus(I)V
    .locals 0
    .param p1, "focus"    # I

    .prologue
    .line 290
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mGenericMotionFocus:I

    .line 291
    return-void
.end method

.method public setGenericMotionTitleFocus(I)V
    .locals 0
    .param p1, "focus"    # I

    .prologue
    .line 314
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mGenericMotionTitleFocus:I

    .line 315
    return-void
.end method

.method public setGroupCheckBoxMode(Z)V
    .locals 0
    .param p1, "isAllSelect"    # Z

    .prologue
    .line 306
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mAllSelectMode:Z

    .line 307
    return-void
.end method

.method public setHeaderItem(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 152
    return-void
.end method

.method public final setModelListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->setModelListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;)V

    .line 252
    return-void
.end method

.method public setRTL(Z)V
    .locals 0
    .param p1, "isRTL"    # Z

    .prologue
    .line 301
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mIsRTL:Z

    .line 302
    return-void
.end method

.method public setScreenNailImage(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;I)V
    .locals 0
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "type"    # I

    .prologue
    .line 205
    return-void
.end method

.method public abstract setThumbReslevel(I)V
.end method

.method public final setViewListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;

    .prologue
    .line 255
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->setViewListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;)V

    .line 256
    return-void
.end method
