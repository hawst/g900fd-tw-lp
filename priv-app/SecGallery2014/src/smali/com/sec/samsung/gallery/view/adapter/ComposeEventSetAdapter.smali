.class public Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;
.super Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;
.source "ComposeEventSetAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter$MediaSelectionCallback;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ComposeMediaItemAdapter"


# instance fields
.field private mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

.field private mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private readyDrawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "config"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;
    .param p4, "modeOption"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;I)V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 46
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, -0x969697

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->readyDrawable:Landroid/graphics/drawable/Drawable;

    .line 50
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 52
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 53
    return-void
.end method

.method public static calcFaceCropRect(IILandroid/graphics/RectF;FFI)Landroid/graphics/Rect;
    .locals 16
    .param p0, "srcWidth"    # I
    .param p1, "srcHeight"    # I
    .param p2, "faceRect"    # Landroid/graphics/RectF;
    .param p3, "tgtWidth"    # F
    .param p4, "tgtHeight"    # F
    .param p5, "rotation"    # I

    .prologue
    .line 516
    const/16 v13, 0x5a

    move/from16 v0, p5

    if-eq v0, v13, :cond_0

    const/16 v13, 0x10e

    move/from16 v0, p5

    if-ne v0, v13, :cond_7

    :cond_0
    div-float v10, p4, p3

    .line 518
    .local v10, "tgtRatio":F
    :goto_0
    if-eqz p2, :cond_4

    if-eqz p5, :cond_4

    .line 519
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v12

    .line 520
    .local v12, "w":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v7

    .line 521
    .local v7, "h":F
    move-object/from16 v0, p2

    iget v8, v0, Landroid/graphics/RectF;->left:F

    .line 522
    .local v8, "left":F
    move-object/from16 v0, p2

    iget v11, v0, Landroid/graphics/RectF;->top:F

    .line 524
    .local v11, "top":F
    const/16 v13, 0x5a

    move/from16 v0, p5

    if-eq v0, v13, :cond_1

    const/16 v13, 0x10e

    move/from16 v0, p5

    if-ne v0, v13, :cond_2

    .line 525
    :cond_1
    const/high16 v13, 0x3f800000    # 1.0f

    sub-float/2addr v13, v12

    sub-float v8, v13, v8

    .line 526
    const/high16 v13, 0x3f800000    # 1.0f

    sub-float/2addr v13, v7

    sub-float v11, v13, v11

    .line 528
    :cond_2
    move/from16 v0, p5

    rem-int/lit16 v13, v0, 0xb4

    if-eqz v13, :cond_3

    .line 530
    move v9, v11

    .line 531
    .local v9, "temp":F
    move v11, v8

    .line 532
    const/high16 v13, 0x3f800000    # 1.0f

    sub-float/2addr v13, v9

    sub-float v8, v13, v7

    .line 534
    move v9, v12

    .line 535
    move v12, v7

    .line 536
    move v7, v9

    .line 538
    .end local v9    # "temp":F
    :cond_3
    new-instance p2, Landroid/graphics/RectF;

    .end local p2    # "faceRect":Landroid/graphics/RectF;
    add-float v13, v8, v12

    add-float v14, v11, v7

    move-object/from16 v0, p2

    invoke-direct {v0, v8, v11, v13, v14}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 544
    .end local v7    # "h":F
    .end local v8    # "left":F
    .end local v11    # "top":F
    .end local v12    # "w":F
    .restart local p2    # "faceRect":Landroid/graphics/RectF;
    :cond_4
    move/from16 v0, p1

    int-to-float v13, v0

    move/from16 v0, p0

    int-to-float v14, v0

    div-float/2addr v14, v10

    cmpl-float v13, v13, v14

    if-lez v13, :cond_8

    .line 546
    move/from16 v2, p0

    .line 547
    .local v2, "cropWidth":I
    move/from16 v0, p0

    int-to-float v13, v0

    div-float/2addr v13, v10

    float-to-int v1, v13

    .line 548
    .local v1, "cropHeight":I
    const/4 v3, 0x0

    .line 549
    .local v3, "cropX":I
    sub-int v13, p1, v1

    div-int/lit8 v4, v13, 0x2

    .line 557
    .local v4, "cropY":I
    :goto_1
    if-eqz p2, :cond_6

    .line 559
    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/RectF;->left:F

    const/4 v14, 0x0

    cmpl-float v13, v13, v14

    if-ltz v13, :cond_5

    .line 560
    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p2

    iget v14, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    move/from16 v0, p0

    int-to-float v14, v0

    mul-float/2addr v13, v14

    float-to-int v5, v13

    .line 561
    .local v5, "faceCenterH":I
    div-int/lit8 v13, v2, 0x2

    sub-int v3, v5, v13

    .line 562
    if-gez v3, :cond_9

    .line 563
    const/4 v3, 0x0

    .line 568
    .end local v5    # "faceCenterH":I
    :cond_5
    :goto_2
    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/RectF;->top:F

    const/4 v14, 0x0

    cmpl-float v13, v13, v14

    if-ltz v13, :cond_6

    .line 569
    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p2

    iget v14, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    move/from16 v0, p1

    int-to-float v14, v0

    mul-float/2addr v13, v14

    float-to-int v6, v13

    .line 570
    .local v6, "faceCenterV":I
    div-int/lit8 v13, v1, 0x2

    sub-int v4, v6, v13

    .line 571
    if-gez v4, :cond_a

    .line 572
    const/4 v4, 0x0

    .line 578
    .end local v6    # "faceCenterV":I
    :cond_6
    :goto_3
    new-instance v13, Landroid/graphics/Rect;

    add-int v14, v3, v2

    add-int v15, v4, v1

    invoke-direct {v13, v3, v4, v14, v15}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v13

    .line 516
    .end local v1    # "cropHeight":I
    .end local v2    # "cropWidth":I
    .end local v3    # "cropX":I
    .end local v4    # "cropY":I
    .end local v10    # "tgtRatio":F
    :cond_7
    div-float v10, p3, p4

    goto/16 :goto_0

    .line 552
    .restart local v10    # "tgtRatio":F
    :cond_8
    move/from16 v1, p1

    .line 553
    .restart local v1    # "cropHeight":I
    move/from16 v0, p1

    int-to-float v13, v0

    mul-float/2addr v13, v10

    float-to-int v2, v13

    .line 554
    .restart local v2    # "cropWidth":I
    const/4 v4, 0x0

    .line 555
    .restart local v4    # "cropY":I
    sub-int v13, p0, v2

    div-int/lit8 v3, v13, 0x2

    .restart local v3    # "cropX":I
    goto :goto_1

    .line 564
    .restart local v5    # "faceCenterH":I
    :cond_9
    sub-int v13, p0, v2

    if-le v3, v13, :cond_5

    .line 565
    sub-int v3, p0, v2

    goto :goto_2

    .line 573
    .end local v5    # "faceCenterH":I
    .restart local v6    # "faceCenterV":I
    :cond_a
    sub-int v13, p1, v1

    if-le v4, v13, :cond_6

    .line 574
    sub-int v4, p1, v1

    goto :goto_3
.end method

.method private drawAlbumLabelBG(Lcom/sec/android/gallery3d/glcore/GlView;I)Lcom/sec/android/gallery3d/glcore/GlImageView;
    .locals 2
    .param p1, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p2, "sizeType"    # I

    .prologue
    .line 388
    if-nez p1, :cond_0

    .line 389
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 393
    .local v0, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_0
    return-object v0

    .end local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_0
    move-object v0, p1

    .line 391
    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_0
.end method

.method private drawThumbnailImage(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;Lcom/sec/android/gallery3d/data/MediaItem;II)Z
    .locals 3
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "imageItem"    # Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p4, "subPosition"    # I
    .param p5, "totalCount"    # I

    .prologue
    .line 369
    const/4 v0, 0x0

    .line 371
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz p3, :cond_0

    if-nez p2, :cond_1

    .line 372
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->readyDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 373
    const/4 v2, 0x0

    .line 382
    :goto_0
    return v2

    .line 375
    :cond_1
    iget-object v0, p2, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 376
    if-nez v0, :cond_2

    .line 377
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->readyDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 382
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    .line 379
    :cond_2
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v1

    .line 380
    .local v1, "rotation":I
    invoke-virtual {p1, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageBitmap(Landroid/graphics/Bitmap;I)V

    goto :goto_1
.end method

.method private getPlayTypeIconRsrc(Lcom/sec/android/gallery3d/data/MediaItem;)I
    .locals 3
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 231
    const/4 v0, 0x0

    .line 233
    .local v0, "rsrcPlayType":I
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    instance-of v1, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v1, :cond_0

    .line 234
    const v0, 0x7f0201b6

    .line 235
    :cond_0
    return v0
.end method

.method private setTextViewPaint(Landroid/text/TextPaint;Z)V
    .locals 3
    .param p1, "textPaint"    # Landroid/text/TextPaint;
    .param p2, "forTitle"    # Z

    .prologue
    const/4 v2, 0x0

    .line 509
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 510
    invoke-static {}, Lcom/sec/samsung/gallery/util/FontUtil;->getRobotoLightFont()Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 511
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 512
    return-void
.end method


# virtual methods
.method public drawAlbumLabel(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;F)V
    .locals 30
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "sizeType"    # I
    .param p3, "album"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    .param p4, "itemW"    # F

    .prologue
    .line 397
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 398
    .local v5, "dimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    move-object/from16 v0, p3

    iget-object v10, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 399
    .local v10, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v4, 0x0

    .line 400
    .local v4, "count":I
    if-eqz v10, :cond_0

    .line 401
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v4

    .line 403
    :cond_0
    invoke-virtual {v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getEventViewTitleFontColor()I

    move-result v21

    .line 404
    .local v21, "titleTextColor":I
    invoke-virtual {v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getEventViewSubTitleFontColor()I

    move-result v16

    .line 405
    .local v16, "subTitleTextColor":I
    invoke-virtual {v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getEventViewTitleFontSize()I

    move-result v23

    .line 406
    .local v23, "titleTextSize":I
    invoke-virtual {v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getEventViewTitleHeight()I

    move-result v22

    .line 407
    .local v22, "titleTextHeight":I
    invoke-virtual {v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getEventViewSubTitleHeight()I

    move-result v17

    .line 408
    .local v17, "subTitleTextHeight":I
    invoke-virtual {v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getEventViewSubTitleFontSize()I

    move-result v18

    .line 409
    .local v18, "subTitleTextSize":I
    invoke-virtual {v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getEventViewTitleLeftMargin()I

    move-result v8

    .line 410
    .local v8, "leftMargin":I
    invoke-virtual {v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getEventViewTitleTopMargin()I

    move-result v26

    .line 411
    .local v26, "topMargin":I
    invoke-virtual {v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getEventViewTitlePaddingforSSFont()I

    move-result v12

    .line 412
    .local v12, "paddingforSamsungSansFont":I
    invoke-virtual {v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v27

    div-float v27, p4, v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v25, v0

    .line 413
    .local v25, "titleW":I
    sget-boolean v27, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v27, :cond_1

    .line 414
    const/high16 v21, -0x1000000

    .line 415
    const/high16 v16, -0x1000000

    .line 418
    :cond_1
    const/16 v27, 0xcb

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 419
    .local v9, "mapView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v10, :cond_5

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaSet;->hasLocation()Z

    move-result v27

    if-eqz v27, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v27

    if-nez v27, :cond_5

    .line 420
    if-nez v9, :cond_4

    .line 421
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v28

    const v29, 0x7f02027d

    invoke-virtual/range {v27 .. v29}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 422
    .local v6, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v27

    sub-int v25, v25, v27

    .line 423
    new-instance v9, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v9    # "mapView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v9, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 424
    .restart local v9    # "mapView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v9, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 425
    const/16 v27, 0x3

    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v9, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 426
    const/16 v27, 0x0

    const/16 v28, 0x0

    move/from16 v0, v27

    move/from16 v1, v26

    move/from16 v2, v28

    invoke-virtual {v9, v0, v1, v8, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 427
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v27

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v28

    move/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v9, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 428
    const/16 v27, 0xcb

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v9, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 433
    .end local v6    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_0
    const/16 v27, 0x0

    const/16 v28, 0x0

    move/from16 v0, v27

    move/from16 v1, v26

    move/from16 v2, v28

    invoke-virtual {v9, v0, v1, v8, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 437
    :cond_2
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-static {v0, v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getAlbumNumber(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v14

    .line 438
    .local v14, "photoNumlabel":Ljava/lang/String;
    const/16 v27, 0xcc

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v24

    check-cast v24, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 439
    .local v24, "titleTextView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    const/16 v27, 0xcd

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v19

    check-cast v19, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 441
    .local v19, "subTitleTextView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    if-eqz v10, :cond_6

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v20

    .line 442
    .local v20, "title":Ljava/lang/String;
    :goto_2
    if-eqz v10, :cond_7

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaSet;->getEventAlbumTimeInfo()Ljava/lang/String;

    move-result-object v15

    .line 444
    .local v15, "subTitle":Ljava/lang/String;
    :goto_3
    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getDefaultPaintForLabel(F)Landroid/text/TextPaint;

    move-result-object v13

    .line 445
    .local v13, "paint":Landroid/text/TextPaint;
    const/4 v7, 0x0

    .line 446
    .local v7, "label":Ljava/lang/String;
    sub-int v27, v25, v12

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, v20

    move/from16 v1, v27

    invoke-static {v0, v14, v1, v13}, Lcom/sec/android/gallery3d/glcore/GlTextView;->ellipsizeString(Ljava/lang/String;Ljava/lang/String;FLandroid/text/TextPaint;)Ljava/lang/String;

    move-result-object v7

    .line 447
    if-nez v24, :cond_8

    .line 448
    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    move/from16 v1, v21

    invoke-static {v7, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v24

    .line 449
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    move/from16 v1, v16

    invoke-static {v15, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v19

    .line 451
    const/16 v27, 0x1

    const/16 v28, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 452
    const/16 v27, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v8, v1, v8, v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 454
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v27

    const/16 v28, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->setTextViewPaint(Landroid/text/TextPaint;Z)V

    .line 455
    const/16 v27, 0xcc

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 457
    const/16 v27, 0x1

    const/16 v28, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 458
    add-int v27, v26, v22

    const/16 v28, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v8, v1, v8, v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 459
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getWidth()I

    move-result v27

    move-object/from16 v0, v19

    move/from16 v1, v27

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setSize(II)V

    .line 462
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v27

    const/16 v28, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->setTextViewPaint(Landroid/text/TextPaint;Z)V

    .line 463
    const/16 v27, 0xcd

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 477
    :cond_3
    :goto_4
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v11

    .line 478
    .local v11, "metrics":Landroid/graphics/Paint$FontMetricsInt;
    invoke-virtual {v13, v7}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v27

    move/from16 v0, v27

    float-to-double v0, v0

    move-wide/from16 v28, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-int v0, v0

    move/from16 v27, v0

    add-int v27, v27, v12

    iget v0, v11, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    move/from16 v28, v0

    iget v0, v11, Landroid/graphics/Paint$FontMetricsInt;->top:I

    move/from16 v29, v0

    sub-int v28, v28, v29

    move-object/from16 v0, v24

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setSize(II)V

    .line 479
    return-void

    .line 431
    .end local v7    # "label":Ljava/lang/String;
    .end local v11    # "metrics":Landroid/graphics/Paint$FontMetricsInt;
    .end local v13    # "paint":Landroid/text/TextPaint;
    .end local v14    # "photoNumlabel":Ljava/lang/String;
    .end local v15    # "subTitle":Ljava/lang/String;
    .end local v19    # "subTitleTextView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    .end local v20    # "title":Ljava/lang/String;
    .end local v24    # "titleTextView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    :cond_4
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v27

    sub-int v25, v25, v27

    goto/16 :goto_0

    .line 434
    :cond_5
    if-eqz v9, :cond_2

    .line 435
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto/16 :goto_1

    .line 441
    .restart local v14    # "photoNumlabel":Ljava/lang/String;
    .restart local v19    # "subTitleTextView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    .restart local v24    # "titleTextView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    :cond_6
    const-string v20, ""

    goto/16 :goto_2

    .line 442
    .restart local v20    # "title":Ljava/lang/String;
    :cond_7
    const-string v15, ""

    goto/16 :goto_3

    .line 466
    .restart local v7    # "label":Ljava/lang/String;
    .restart local v13    # "paint":Landroid/text/TextPaint;
    .restart local v15    # "subTitle":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, v24

    invoke-virtual {v0, v7}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setText(Ljava/lang/String;)V

    .line 467
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v27

    const/16 v28, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->setTextViewPaint(Landroid/text/TextPaint;Z)V

    .line 468
    const/16 v27, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v8, v1, v8, v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 470
    if-eqz v19, :cond_3

    .line 471
    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setText(Ljava/lang/String;)V

    .line 472
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v27

    const/16 v28, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->setTextViewPaint(Landroid/text/TextPaint;Z)V

    .line 473
    add-int v27, v26, v22

    const/16 v28, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v8, v1, v8, v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 474
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getWidth()I

    move-result v27

    move-object/from16 v0, v19

    move/from16 v1, v27

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setSize(II)V

    goto/16 :goto_4
.end method

.method public drawBorder(Lcom/sec/android/gallery3d/glcore/GlView;Z)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 7
    .param p1, "glView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p2, "visible"    # Z

    .prologue
    const/16 v6, 0xd

    const/4 v5, 0x2

    .line 484
    if-nez p1, :cond_0

    .line 485
    new-instance p1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local p1    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {p1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 487
    .restart local p1    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_0
    invoke-virtual {p1, v6}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 489
    .local v0, "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_2

    .line 490
    if-nez v0, :cond_1

    .line 491
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02026e

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 493
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 494
    .restart local v0    # "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 495
    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setScaleRatio(F)V

    .line 496
    invoke-virtual {v0, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 497
    invoke-virtual {p1, v0, v6}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 505
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    :goto_0
    return-object p1

    .line 500
    :cond_2
    if-eqz v0, :cond_1

    .line 501
    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto :goto_0
.end method

.method public drawDecorViewOnRequest(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 17
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 171
    const/4 v3, 0x0

    .line 173
    .local v3, "decorView":Lcom/sec/android/gallery3d/glcore/GlView;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->getPlayTypeIconRsrc(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v6

    .line 176
    .local v6, "rsrcPlayType":I
    if-nez v3, :cond_0

    .line 177
    new-instance v3, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v3    # "decorView":Lcom/sec/android/gallery3d/glcore/GlView;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v3, v13}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 179
    .restart local v3    # "decorView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_0
    const/4 v13, 0x3

    invoke-virtual {v3, v13}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 180
    .local v5, "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v6, :cond_3

    .line 181
    if-nez v5, :cond_1

    .line 182
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    invoke-virtual {v13, v14, v6}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 183
    .local v4, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v5, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v5    # "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v5, v13}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 184
    .restart local v5    # "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v5, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 185
    const/4 v13, 0x2

    const/4 v14, 0x2

    invoke-virtual {v5, v13, v14}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 186
    const/4 v13, 0x1

    invoke-virtual {v5, v13}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 187
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v13

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v14

    invoke-virtual {v5, v13, v14}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 188
    const/4 v13, 0x3

    invoke-virtual {v3, v5, v13}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 194
    .end local v4    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    :goto_0
    const/16 v13, 0xc9

    invoke-virtual {v3, v13}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 195
    .local v8, "suggestionView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/data/MediaSet;->isSuggestionEvent()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 196
    const/16 v13, 0xca

    invoke-virtual {v3, v13}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 197
    .local v12, "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0e0442

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 198
    .local v7, "suggestionLabel":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getEventViewSuggestionFontSize()I

    move-result v11

    .line 199
    .local v11, "textSize":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getEventViewSuggestionFontColor()I

    move-result v9

    .line 200
    .local v9, "textColor":I
    if-nez v8, :cond_2

    .line 201
    sget-boolean v13, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v13, :cond_4

    .line 202
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    .line 203
    .local v2, "colorDrawable":Landroid/graphics/drawable/ColorDrawable;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0b006b

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v2, v13}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 204
    move-object v4, v2

    .line 208
    .end local v2    # "colorDrawable":Landroid/graphics/drawable/ColorDrawable;
    .restart local v4    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_1
    int-to-float v13, v11

    invoke-static {v7, v13, v9}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v12

    .line 209
    invoke-virtual {v12, v7}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setText(Ljava/lang/String;)V

    .line 210
    int-to-float v13, v11

    invoke-virtual {v12, v13}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setTextSize(F)V

    .line 211
    const/4 v13, 0x2

    const/4 v14, 0x2

    invoke-virtual {v12, v13, v14}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 212
    invoke-virtual {v12}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v10

    .line 213
    .local v10, "textPaint":Landroid/text/TextPaint;
    invoke-static {}, Lcom/sec/samsung/gallery/util/FontUtil;->getRobotoLightFont()Landroid/graphics/Typeface;

    move-result-object v13

    invoke-virtual {v10, v13}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 214
    new-instance v8, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v8    # "suggestionView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v8, v13}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 215
    .restart local v8    # "suggestionView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v8, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 216
    const/4 v13, 0x3

    const/4 v14, 0x1

    invoke-virtual {v8, v13, v14}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 217
    invoke-virtual {v12}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getWidth()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v14}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getEventViewSuggestionPadding()I

    move-result v14

    mul-int/lit8 v14, v14, 0x2

    add-int/2addr v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v14}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getEventViewSuggestionHeight()I

    move-result v14

    invoke-virtual {v8, v13, v14}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 218
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v14}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getEventViewSuggestionTopMargin()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v15}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getEventViewSuggestionRightMargin()I

    move-result v15

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v8, v13, v14, v15, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 219
    const/16 v13, 0xc9

    invoke-virtual {v3, v8, v13}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 220
    const/16 v13, 0xca

    invoke-virtual {v8, v12, v13}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 226
    .end local v4    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v7    # "suggestionLabel":Ljava/lang/String;
    .end local v9    # "textColor":I
    .end local v10    # "textPaint":Landroid/text/TextPaint;
    .end local v11    # "textSize":I
    .end local v12    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    :cond_2
    :goto_2
    return-object v3

    .line 190
    .end local v8    # "suggestionView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_3
    if-eqz v5, :cond_1

    .line 191
    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto/16 :goto_0

    .line 206
    .restart local v7    # "suggestionLabel":Ljava/lang/String;
    .restart local v8    # "suggestionView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .restart local v9    # "textColor":I
    .restart local v11    # "textSize":I
    .restart local v12    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f02027c

    invoke-virtual {v13, v14, v15}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .restart local v4    # "drawable":Landroid/graphics/drawable/Drawable;
    goto/16 :goto_1

    .line 222
    .end local v4    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v7    # "suggestionLabel":Ljava/lang/String;
    .end local v9    # "textColor":I
    .end local v11    # "textSize":I
    .end local v12    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    :cond_5
    if-eqz v8, :cond_2

    .line 223
    invoke-virtual {v3, v8}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto :goto_2
.end method

.method public getAllCount()I
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getAllCount()I

    move-result v0

    return v0
.end method

.method public getAllItem(I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "albumIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getAllItem(I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getAllItemCount(I)I
    .locals 1
    .param p1, "albumIndex"    # I

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getAllItemCount(I)I

    move-result v0

    return v0
.end method

.method public getCodeForMediaItem(Lcom/sec/android/gallery3d/data/MediaItem;)I
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getCodeForMediaItem(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getCount()I

    move-result v0

    return v0
.end method

.method public getCount(I)I
    .locals 1
    .param p1, "albumIndex"    # I

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getCount(I)I

    move-result v0

    return v0
.end method

.method public getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p1, "albumIndex"    # I
    .param p2, "photoIndex"    # I

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemImage(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "albumIndex"    # I
    .param p2, "photoIndex"    # I

    .prologue
    .line 309
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getImage(II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getLineCount(I)I
    .locals 1
    .param p1, "albumIndex"    # I

    .prologue
    .line 260
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getLineCount(I)I

    move-result v0

    return v0
.end method

.method public getScreenNailImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    if-nez v0, :cond_0

    .line 315
    const/4 v0, 0x0

    .line 316
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public getSource()Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "albumIndex"    # I

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    return-object v0
.end method

.method public getView(IILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 8
    .param p1, "position"    # I
    .param p2, "subPosition"    # I
    .param p3, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p4, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p5, "ext"    # I
    .param p6, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 335
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mRetLevel:I

    .line 336
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-lt p1, v0, :cond_1

    .line 364
    .end local p4    # "parentView":Lcom/sec/android/gallery3d/glcore/GlLayer;
    .end local p6    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v1

    .line 339
    .restart local p4    # "parentView":Lcom/sec/android/gallery3d/glcore/GlLayer;
    .restart local p6    # "obj":Ljava/lang/Object;
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v6, v0, p1

    .line 340
    .local v6, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-eqz v6, :cond_0

    iget-object v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_0

    iget v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-eqz v0, :cond_0

    .line 342
    if-gez p2, :cond_2

    .line 343
    invoke-direct {p0, p3, p2}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->drawAlbumLabelBG(Lcom/sec/android/gallery3d/glcore/GlView;I)Lcom/sec/android/gallery3d/glcore/GlImageView;

    move-result-object v1

    .line 344
    .local v1, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    check-cast p4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    .end local p4    # "parentView":Lcom/sec/android/gallery3d/glcore/GlLayer;
    invoke-virtual {p4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->getItemWidth()F

    move-result v0

    invoke-virtual {p0, v1, p2, v6, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->drawAlbumLabel(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;F)V

    .line 345
    instance-of v0, p6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_0

    .line 346
    check-cast p6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .end local p6    # "obj":Ljava/lang/Object;
    invoke-virtual {p6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getFocusBorderVisible()Z

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->drawBorder(Lcom/sec/android/gallery3d/glcore/GlView;Z)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v1

    .end local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_0

    .line 349
    .end local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .restart local p4    # "parentView":Lcom/sec/android/gallery3d/glcore/GlLayer;
    .restart local p6    # "obj":Ljava/lang/Object;
    :cond_2
    iget v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-ge p2, v0, :cond_0

    .line 352
    if-nez p3, :cond_3

    .line 353
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 358
    .restart local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_1
    iget-object v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    aget-object v3, v0, p2

    .line 359
    .local v3, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemImage:[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    aget-object v2, v0, p2

    .line 360
    .local v2, "imageItem":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    iget v5, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    move-object v0, p0

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->drawThumbnailImage(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;Lcom/sec/android/gallery3d/data/MediaItem;II)Z

    move-result v7

    .line 361
    .local v7, "result":Z
    if-eqz v7, :cond_0

    .line 362
    iget-object v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mUpdated:[B

    aget-byte v0, v0, p2

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mRetLevel:I

    goto :goto_0

    .end local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v2    # "imageItem":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    .end local v3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v7    # "result":Z
    :cond_3
    move-object v1, p3

    .line 355
    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_1
.end method

.method public getViewInfo(IIILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;Z)Z
    .locals 12
    .param p1, "position"    # I
    .param p2, "subPosition"    # I
    .param p3, "option"    # I
    .param p4, "inter"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;
    .param p5, "borderVisible"    # Z

    .prologue
    .line 94
    const/4 v7, 0x0

    .line 98
    .local v7, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    const/4 v9, 0x0

    .line 101
    .local v9, "cropRect":Landroid/graphics/Rect;
    if-nez p4, :cond_0

    const/4 v4, 0x0

    .line 165
    :goto_0
    return v4

    .line 102
    :cond_0
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mBitmap:Landroid/graphics/Bitmap;

    .line 103
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-boolean v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mSelected:Z

    .line 104
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 105
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-boolean v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mReorderEnable:Z

    .line 106
    if-eqz p3, :cond_2

    .line 107
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v10, v4, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    .line 108
    .local v10, "imageItem":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    invoke-virtual {v10}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->getItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v11

    .line 109
    .local v11, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v8, v10, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 110
    .local v8, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v11}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v4

    move-object/from16 v0, p4

    iput v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mRotation:I

    .line 135
    :goto_1
    if-eqz v7, :cond_1

    .line 136
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v5, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v4

    move-object/from16 v0, p4

    iput-boolean v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mSelected:Z

    .line 138
    :cond_1
    if-nez v8, :cond_a

    .line 139
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mCropRect:Landroid/graphics/Rect;

    .line 140
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDynamicCropRect:Landroid/graphics/Rect;

    .line 141
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mBitmap:Landroid/graphics/Bitmap;

    .line 142
    const/4 v4, 0x0

    goto :goto_0

    .line 112
    .end local v8    # "bitmap":Landroid/graphics/Bitmap;
    .end local v10    # "imageItem":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    .end local v11    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    if-ltz p1, :cond_3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget v4, v4, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-lt p1, v4, :cond_4

    .line 113
    :cond_3
    const-string v4, "ComposeMediaItemAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GetViewInfo pos = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", size = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget v6, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    const/4 v4, 0x0

    goto :goto_0

    .line 117
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v7, v4, p1

    .line 118
    if-eqz v7, :cond_5

    iget v4, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-lt p2, v4, :cond_7

    .line 119
    :cond_5
    if-nez v7, :cond_6

    .line 120
    const-string v4, "ComposeMediaItemAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GetViewInfo album is null = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :goto_2
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 122
    :cond_6
    const-string v4, "ComposeMediaItemAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GetViewInfo position is over index  = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", length = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 126
    :cond_7
    iget-object v4, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    aget-object v11, v4, p2

    .line 127
    .restart local v11    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v11, :cond_8

    .line 128
    const-string v4, "ComposeMediaItemAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GetViewInfo mediaItem is null = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", sub = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 131
    :cond_8
    invoke-virtual {v11}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v4

    move-object/from16 v0, p4

    iput v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mRotation:I

    .line 132
    iget-object v4, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemImage:[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    aget-object v10, v4, p2

    .line 133
    .restart local v10    # "imageItem":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    if-nez v10, :cond_9

    const/4 v8, 0x0

    .restart local v8    # "bitmap":Landroid/graphics/Bitmap;
    :goto_3
    goto/16 :goto_1

    .end local v8    # "bitmap":Landroid/graphics/Bitmap;
    :cond_9
    iget-object v8, v10, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_3

    .line 145
    .restart local v8    # "bitmap":Landroid/graphics/Bitmap;
    :cond_a
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-boolean v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispExpansionIcon:Z

    .line 147
    move-object/from16 v0, p4

    iget-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    if-nez v4, :cond_b

    if-eqz v10, :cond_b

    iget-object v4, v10, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

    if-eqz v4, :cond_b

    .line 148
    iget-object v4, v10, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

    move-object/from16 v0, p4

    iput-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 150
    :cond_b
    move-object/from16 v0, p4

    iget-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    move/from16 v0, p5

    invoke-virtual {p0, v4, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->drawBorder(Lcom/sec/android/gallery3d/glcore/GlView;Z)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v4

    move-object/from16 v0, p4

    iput-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 152
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 153
    .local v1, "width":I
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 155
    .local v2, "height":I
    const/4 v3, 0x0

    .line 156
    .local v3, "faceRect":Landroid/graphics/RectF;
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceThumbnail:Z

    if-eqz v4, :cond_c

    instance-of v4, v11, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v4, :cond_c

    invoke-static {v11}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v4

    if-nez v4, :cond_c

    if-eqz v10, :cond_c

    iget v4, v10, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThmType:I

    const/4 v5, 0x3

    if-eq v4, v5, :cond_c

    .line 160
    check-cast v11, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v11    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v11}, Lcom/sec/android/gallery3d/data/LocalImage;->getRectOfAllFaces()Landroid/graphics/RectF;

    move-result-object v3

    .line 162
    :cond_c
    move-object/from16 v0, p4

    iget v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjWidth:F

    move-object/from16 v0, p4

    iget v5, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjHeight:F

    move-object/from16 v0, p4

    iget v6, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mRotation:I

    invoke-static/range {v1 .. v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->calcFaceCropRect(IILandroid/graphics/RectF;FFI)Landroid/graphics/Rect;

    move-result-object v9

    .line 163
    move-object/from16 v0, p4

    iput-object v9, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mCropRect:Landroid/graphics/Rect;

    .line 164
    move-object/from16 v0, p4

    iput-object v8, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mBitmap:Landroid/graphics/Bitmap;

    .line 165
    const/4 v4, 0x1

    goto/16 :goto_0
.end method

.method public requestScreenNail(II)Z
    .locals 2
    .param p1, "code"    # I
    .param p2, "type"    # I

    .prologue
    .line 290
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->requestScreenNail(IIZ)Z

    move-result v0

    return v0
.end method

.method public requestScreenNail(Lcom/sec/android/gallery3d/data/MediaItem;I)Z
    .locals 2
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "type"    # I

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->requestScreenNail(Lcom/sec/android/gallery3d/data/MediaItem;IZ)Z

    move-result v0

    return v0
.end method

.method public requestScreenNailUrgent(II)Z
    .locals 2
    .param p1, "code"    # I
    .param p2, "type"    # I

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->requestScreenNail(IIZ)Z

    move-result v0

    return v0
.end method

.method public requestScreenNailUrgent(Lcom/sec/android/gallery3d/data/MediaItem;I)Z
    .locals 2
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "type"    # I

    .prologue
    .line 295
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->requestScreenNail(Lcom/sec/android/gallery3d/data/MediaItem;IZ)Z

    move-result v0

    return v0
.end method

.method public requestToNext(I)Z
    .locals 1
    .param p1, "toward"    # I

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->requestToNext(I)Z

    move-result v0

    return v0
.end method

.method public requestUpdateBitmap(III)V
    .locals 1
    .param p1, "code"    # I
    .param p2, "level"    # I
    .param p3, "type"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->requestUpdateBitmap(III)V

    .line 89
    return-void
.end method

.method public setActiveWindow(IIII)V
    .locals 1
    .param p1, "activeStart"    # I
    .param p2, "activeEnd"    # I
    .param p3, "contentStart"    # I
    .param p4, "contentEnd"    # I

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->setActiveWindow(IIII)V

    .line 79
    return-void
.end method

.method public setInitThumbType(I)V
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    int-to-byte v1, p1

    iput-byte v1, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mLevel:B

    .line 69
    return-void
.end method

.method public setScreenNailImage(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;I)V
    .locals 8
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "type"    # I

    .prologue
    .line 321
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->recycle()V

    .line 324
    :cond_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    new-instance v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v4, 0x3

    move-object v2, p1

    move v5, p3

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;IIILandroid/graphics/Bitmap;)V

    iput-object v0, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    .line 325
    return-void
.end method

.method public setSelectionSource(Lcom/sec/android/gallery3d/ui/SelectionManager;)V
    .locals 1
    .param p1, "selectionMgr"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->setSelectionSource(Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    .line 65
    return-void
.end method

.method public setSource(Lcom/sec/android/gallery3d/data/MediaSet;)Z
    .locals 2
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->setSource(Lcom/sec/android/gallery3d/data/MediaSet;Z)Z

    move-result v0

    return v0
.end method

.method public setThumbReslevel(I)V
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->setThumbReslevel(I)V

    .line 74
    return-void
.end method
