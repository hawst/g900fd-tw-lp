.class public Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;
.super Lcom/sec/samsung/gallery/view/help/HelpViewState;
.source "HelpListScrollingState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;


# static fields
.field private static final TAG:Ljava/lang/String; = "HelpListScrollingState"


# instance fields
.field private IsShowCompletePopup:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->IsShowCompletePopup:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->IsShowCompletePopup:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->IsShowCompletePopup:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method private setViewListScrolling()V
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    .line 60
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v7, 0x7f0f0131

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/view/help/HelpScrollView;

    .line 62
    .local v4, "scroll":Lcom/sec/samsung/gallery/view/help/HelpScrollView;
    invoke-virtual {v4, v8}, Lcom/sec/samsung/gallery/view/help/HelpScrollView;->setVisibility(I)V

    .line 63
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mBG:Landroid/widget/ImageView;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 64
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v7, 0x7f0f0132

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 66
    .local v0, "im":Landroid/widget/ImageView;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_1

    .line 67
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v6, :cond_0

    .line 68
    const-string v6, "gallery_list_scroll_bg_land"

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 77
    :goto_0
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setSoundEffectsEnabled(Z)V

    .line 78
    new-instance v6, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$2;

    invoke-direct {v6, p0}, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$2;-><init>(Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;)V

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 90
    .local v3, "res":Landroid/content/res/Resources;
    iget v6, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mHeight:I

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v7

    sub-int/2addr v6, v7

    const v7, 0x7f0d01dc

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    add-int v1, v6, v7

    .line 91
    .local v1, "mTapY":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mTap:Landroid/widget/ImageView;

    const v7, 0x7f0202da

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 92
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mTap:Landroid/widget/ImageView;

    new-instance v7, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$3;

    invoke-direct {v7, p0}, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$3;-><init>(Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;)V

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mTap:Landroid/widget/ImageView;

    int-to-float v7, v1

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setY(F)V

    .line 101
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mTap:Landroid/widget/ImageView;

    iget v7, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mWidth:I

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v8

    sub-int/2addr v7, v8

    int-to-float v7, v7

    div-float/2addr v7, v9

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setX(F)V

    .line 103
    const v6, 0x7f0d01a4

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 104
    .local v5, "top_margin":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mHelpPopup:Landroid/widget/RelativeLayout;

    sub-int v7, v1, v5

    int-to-float v7, v7

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 105
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mHead:Landroid/widget/ImageView;

    const v7, 0x7f0202e4

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 106
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mHead:Landroid/widget/ImageView;

    iget v7, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mWidth:I

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v8

    sub-int/2addr v7, v8

    int-to-float v7, v7

    div-float/2addr v7, v9

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setX(F)V

    .line 108
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 109
    .local v2, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const v6, 0x7f0c0043

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    iput v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 110
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 113
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mText:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .end local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 114
    .restart local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    const v6, 0x7f0c0044

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    iput v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 115
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mText:Landroid/widget/TextView;

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 116
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mText:Landroid/widget/TextView;

    const v7, 0x7f0e02cf

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 118
    new-instance v6, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$4;

    invoke-direct {v6, p0, v4}, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$4;-><init>(Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;Lcom/sec/samsung/gallery/view/help/HelpScrollView;)V

    invoke-virtual {v4, v6}, Lcom/sec/samsung/gallery/view/help/HelpScrollView;->setOnScrollChanged(Lcom/sec/samsung/gallery/view/help/HelpScrollView$OnScrollChanged;)V

    .line 143
    return-void

    .line 70
    .end local v1    # "mTapY":I
    .end local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v3    # "res":Landroid/content/res/Resources;
    .end local v5    # "top_margin":I
    :cond_0
    const v6, 0x7f02039c

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 72
    :cond_1
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v6, :cond_2

    .line 73
    const-string v6, "gallery_list_scroll_bg"

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 75
    :cond_2
    const v6, 0x7f02039b

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 32
    const-string v0, "Help"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mHelpMode:I

    .line 33
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mViewMode:I

    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 35
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mTap:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$1;-><init>(Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 47
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->setHelpView()V

    .line 48
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->animationInit(Landroid/content/Context;)V

    .line 49
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->setOnClickListener()V

    .line 50
    return-void
.end method

.method protected setHelpView()V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->setHelpView()V

    .line 54
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->setViewListScrolling()V

    .line 55
    return-void
.end method
