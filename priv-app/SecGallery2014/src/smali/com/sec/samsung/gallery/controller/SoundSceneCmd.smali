.class public Lcom/sec/samsung/gallery/controller/SoundSceneCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "SoundSceneCmd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static isPlayingState:Z

.field private static isSoundShotAutoPlay:Z

.field private static isSoundShotAutoPlayInited:Z

.field public static mEndTime:J


# instance fields
.field private mAdditionalCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mDuration:J

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mOnCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mStartTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 37
    const-class v0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->TAG:Ljava/lang/String;

    .line 47
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mEndTime:J

    .line 49
    sput-boolean v2, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isPlayingState:Z

    .line 285
    sput-boolean v2, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isSoundShotAutoPlayInited:Z

    .line 286
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isSoundShotAutoPlay:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 36
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 43
    iput-wide v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mDuration:J

    .line 45
    iput-wide v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mStartTime:J

    .line 180
    new-instance v0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/SoundSceneCmd;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mOnCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 189
    new-instance v0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd$2;-><init>(Lcom/sec/samsung/gallery/controller/SoundSceneCmd;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 198
    new-instance v0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd$3;-><init>(Lcom/sec/samsung/gallery/controller/SoundSceneCmd;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/SoundSceneCmd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/SoundSceneCmd;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->avandonAudioFocus()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/SoundSceneCmd;)Landroid/media/MediaPlayer$OnCompletionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/SoundSceneCmd;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mAdditionalCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/controller/SoundSceneCmd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/SoundSceneCmd;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->stopPlaying()V

    return-void
.end method

.method private avandonAudioFocus()V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 219
    return-void
.end method

.method private checkQdioAndPrepare(Ljava/lang/String;)V
    .locals 12
    .param p1, "file"    # Ljava/lang/String;

    .prologue
    .line 106
    const/4 v7, 0x0

    .line 108
    .local v7, "fis":Ljava/io/FileInputStream;
    if-eqz p1, :cond_0

    .line 109
    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .local v8, "fis":Ljava/io/FileInputStream;
    move-object v7, v8

    .line 110
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :cond_0
    const/4 v1, 0x0

    .line 111
    .local v1, "fd":Ljava/io/FileDescriptor;
    if-eqz v7, :cond_1

    .line 112
    invoke-virtual {v7}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    .line 113
    :cond_1
    invoke-static {p1}, Lcom/sec/android/secvision/sef/SEF;->checkAudioInJPEG(Ljava/lang/String;)Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;

    move-result-object v10

    .line 116
    .local v10, "qdioData":Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;
    if-nez v10, :cond_2

    .line 117
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->stopPlaying()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 145
    .end local v1    # "fd":Ljava/io/FileDescriptor;
    .end local v10    # "qdioData":Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;
    :goto_0
    return-void

    .line 120
    .restart local v1    # "fd":Ljava/io/FileDescriptor;
    .restart local v10    # "qdioData":Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;
    :cond_2
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v10, v0}, Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;->getStartOffset(I)I

    move-result v11

    .line 121
    .local v11, "start_offset":I
    const/4 v0, 0x0

    invoke-virtual {v10, v0}, Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;->getLength(I)I

    move-result v9

    .line 124
    .local v9, "length":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_3

    .line 125
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->stopPlaying()V

    .line 128
    :cond_3
    sget-object v0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->TAG:Ljava/lang/String;

    const-string v2, "make mp"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 130
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_4

    .line 131
    if-eqz v11, :cond_4

    if-eqz v9, :cond_4

    .line 132
    const-string v0, "QURAM"

    const-string v2, "prepare mp"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mMediaPlayer:Landroid/media/MediaPlayer;

    int-to-long v2, v11

    int-to-long v4, v9

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    .line 134
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 143
    :cond_4
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    .line 140
    .end local v1    # "fd":Ljava/io/FileDescriptor;
    .end local v9    # "length":I
    .end local v10    # "qdioData":Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;
    .end local v11    # "start_offset":I
    :catch_0
    move-exception v6

    .line 141
    .local v6, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 143
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v0
.end method

.method public static copySoundDataToFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "srcPath"    # Ljava/lang/String;
    .param p1, "dstPath"    # Ljava/lang/String;

    .prologue
    .line 222
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSoundScene:Z

    if-nez v4, :cond_1

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-static {p0}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isSoundScene(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 230
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 231
    .local v3, "srcFile":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 232
    .local v0, "dstFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 236
    const/4 v2, 0x0

    .line 239
    .local v2, "qdioData":Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/secvision/sef/SEF;->checkAudioInJPEG(Ljava/lang/String;)Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;

    move-result-object v2

    .line 240
    if-eqz v2, :cond_0

    .line 242
    invoke-static {p0, p1}, Lcom/sec/android/secvision/sef/SEF;->copyAdioInJPEGtoPNG(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 244
    :catch_0
    move-exception v1

    .line 245
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isASoundSceneFileSelected(Landroid/content/Context;)Z
    .locals 14
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 250
    move-object v12, p0

    check-cast v12, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v11

    .line 251
    .local v11, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    .line 253
    .local v1, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v12, v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-nez v12, :cond_0

    instance-of v12, v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    if-eqz v12, :cond_4

    .line 254
    :cond_0
    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCloneMediaList()Ljava/util/LinkedList;

    move-result-object v9

    .line 255
    .local v9, "selectedItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 256
    .local v8, "selectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v8, :cond_1

    .line 259
    instance-of v12, v8, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v12, :cond_3

    move-object v10, v8

    .line 260
    check-cast v10, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 261
    .local v10, "selectedSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    .line 262
    .local v0, "count":I
    const/4 v12, 0x0

    invoke-virtual {v10, v12, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v5

    .line 264
    .local v5, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 265
    .local v6, "media":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v12, v6, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v12, :cond_2

    check-cast v6, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v6    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    const-wide/16 v12, 0x10

    invoke-virtual {v6, v12, v13}, Lcom/sec/android/gallery3d/data/LocalImage;->hasAttribute(J)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 266
    const/4 v12, 0x1

    .line 282
    .end local v0    # "count":I
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v8    # "selectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v9    # "selectedItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .end local v10    # "selectedSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_0
    return v12

    .line 268
    .restart local v8    # "selectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    .restart local v9    # "selectedItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_3
    instance-of v12, v8, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v12, :cond_1

    check-cast v8, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v8    # "selectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    const-wide/16 v12, 0x10

    invoke-virtual {v8, v12, v13}, Lcom/sec/android/gallery3d/data/LocalImage;->hasAttribute(J)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 269
    const/4 v12, 0x1

    goto :goto_0

    .line 273
    .end local v9    # "selectedItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_4
    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v7

    .line 274
    .local v7, "selectedCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v7, :cond_6

    .line 275
    invoke-virtual {v11, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v6

    .line 276
    .local v6, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v12, v6, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v12, :cond_5

    check-cast v6, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v6    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    const-wide/16 v12, 0x10

    invoke-virtual {v6, v12, v13}, Lcom/sec/android/gallery3d/data/LocalImage;->hasAttribute(J)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 277
    const/4 v12, 0x1

    goto :goto_0

    .line 274
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 282
    .end local v2    # "i":I
    .end local v7    # "selectedCount":I
    :cond_6
    const/4 v12, 0x0

    goto :goto_0
.end method

.method public static isSoundScene(Ljava/lang/String;)Z
    .locals 4
    .param p0, "file"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 87
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSoundScene:Z

    if-eqz v3, :cond_0

    if-nez p0, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v2

    .line 91
    :cond_1
    const/4 v1, 0x0

    .line 93
    .local v1, "qdioData":Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/secvision/sef/SEF;->checkAudioInJPEG(Ljava/lang/String;)Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 98
    :goto_1
    if-eqz v1, :cond_0

    .line 99
    const/4 v2, 0x1

    goto :goto_0

    .line 94
    :catch_0
    move-exception v0

    .line 95
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static isSoundShotAutoPlay(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 289
    sget-boolean v0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isSoundShotAutoPlayInited:Z

    if-nez v0, :cond_0

    .line 290
    const-string/jumbo v0, "soundshotautoplay"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isSoundShotAutoPlay:Z

    .line 291
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isSoundShotAutoPlayInited:Z

    .line 293
    :cond_0
    sget-boolean v0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isSoundShotAutoPlay:Z

    return v0
.end method

.method private requestAudioFocus()V
    .locals 4

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 215
    return-void
.end method

.method public static setSoundShotAutoPlay(Landroid/content/Context;Z)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isOn"    # Z

    .prologue
    .line 297
    const-string/jumbo v0, "soundshotautoplay"

    invoke-static {p0, v0, p1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 298
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isSoundShotAutoPlayInited:Z

    .line 299
    sput-boolean p1, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isSoundShotAutoPlay:Z

    .line 300
    return-void
.end method

.method private startPlaying()V
    .locals 6

    .prologue
    .line 149
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 150
    sget-object v1, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "start playing"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->requestAudioFocus()V

    .line 152
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mOnCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 153
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 155
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V

    .line 156
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 157
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isPlayingState:Z

    .line 158
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v1

    int-to-long v2, v1

    iput-wide v2, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mDuration:J

    .line 159
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mStartTime:J

    .line 160
    iget-wide v2, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mDuration:J

    iget-wide v4, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mStartTime:J

    add-long/2addr v2, v4

    sput-wide v2, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mEndTime:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 162
    :catch_0
    move-exception v0

    .line 163
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private stopPlaying()V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 169
    sget-object v0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stop playing"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->avandonAudioFocus()V

    .line 171
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 174
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 175
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 176
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isPlayingState:Z

    .line 178
    :cond_1
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 7
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 59
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSoundScene:Z

    if-nez v4, :cond_1

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object v3, v4

    check-cast v3, [Ljava/lang/Object;

    .line 63
    .local v3, "params":[Ljava/lang/Object;
    const/4 v4, 0x0

    aget-object v1, v3, v4

    check-cast v1, Landroid/content/Context;

    .line 64
    .local v1, "context":Landroid/content/Context;
    const/4 v4, 0x1

    aget-object v0, v3, v4

    check-cast v0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;

    .line 65
    .local v0, "cmdType":Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;
    sget-object v4, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cmdType : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mAudioManager:Landroid/media/AudioManager;

    if-nez v4, :cond_2

    .line 68
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "audio"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mAudioManager:Landroid/media/AudioManager;

    .line 71
    :cond_2
    sget-object v4, Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;->START_PLAY_SOUND:Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;

    if-ne v0, v4, :cond_4

    .line 72
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 73
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->stopPlaying()V

    goto :goto_0

    .line 75
    :cond_3
    const/4 v4, 0x2

    aget-object v2, v3, v4

    check-cast v2, Ljava/lang/String;

    .line 76
    .local v2, "filePath":Ljava/lang/String;
    const/4 v4, 0x3

    aget-object v4, v3, v4

    check-cast v4, Landroid/media/MediaPlayer$OnCompletionListener;

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mAdditionalCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 78
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->checkQdioAndPrepare(Ljava/lang/String;)V

    .line 79
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->startPlaying()V

    goto :goto_0

    .line 81
    .end local v2    # "filePath":Ljava/lang/String;
    :cond_4
    sget-object v4, Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;->STOP_PLAY_SOUND:Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;

    if-ne v0, v4, :cond_0

    .line 82
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->stopPlaying()V

    goto :goto_0
.end method
