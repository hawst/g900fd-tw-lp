.class public Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;
.super Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;
.source "MoreInfoItemDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$DetailsListAdapter;,
        Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$LoadHistogramTask;,
        Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$LoadBitmapDataTask;
    }
.end annotation


# static fields
.field public static final EXCLUDE_DETAIL_INDEX:[I


# instance fields
.field private mAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$DetailsListAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->EXCLUDE_DETAIL_INDEX:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x4
        0x9
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rootView"    # Landroid/view/View;
    .param p3, "entry"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;
    .param p4, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p4, p3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;)V

    .line 45
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->initView()V

    .line 46
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSimpleDetails:Z

    if-nez v0, :cond_0

    .line 47
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->startLoadBitmapTask()V

    .line 48
    :cond_0
    return-void
.end method

.method private hideDetailsForEdit(Z)V
    .locals 2
    .param p1, "mode"    # Z

    .prologue
    .line 105
    if-eqz p1, :cond_0

    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->mView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 110
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->mView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private initView()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 51
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->mView:Landroid/view/View;

    const v6, 0x7f0f0199

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 52
    .local v4, "view":Landroid/widget/LinearLayout;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 53
    .local v0, "exclude":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSimpleDetails:Z

    if-eqz v5, :cond_0

    .line 54
    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    const/16 v5, 0x64

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    const/16 v5, 0x65

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    const/16 v5, 0x67

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    const/16 v5, 0x68

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    :goto_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    sget-object v5, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->BASE_DETAIL_INDEX:[I

    array-length v5, v5

    if-ge v2, v5, :cond_1

    .line 66
    sget-object v5, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->BASE_DETAIL_INDEX:[I

    aget v5, v5, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 60
    .end local v2    # "i":I
    :cond_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->mContext:Landroid/content/Context;

    check-cast v5, Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f030096

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 62
    .local v1, "headerView":Landroid/widget/LinearLayout;
    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 63
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->setupHeaderView(Landroid/view/ViewGroup;)V

    goto :goto_0

    .line 68
    .end local v1    # "headerView":Landroid/widget/LinearLayout;
    .restart local v2    # "i":I
    :cond_1
    const/4 v2, 0x0

    :goto_2
    sget-object v5, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->EXCLUDE_DETAIL_INDEX:[I

    array-length v5, v5

    if-ge v2, v5, :cond_2

    .line 69
    sget-object v5, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->EXCLUDE_DETAIL_INDEX:[I

    aget v5, v5, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 71
    :cond_2
    new-instance v5, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$DetailsListAdapter;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->mContext:Landroid/content/Context;

    invoke-direct {v5, p0, v6, v0, v7}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$DetailsListAdapter;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$1;)V

    iput-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->mAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$DetailsListAdapter;

    .line 72
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->mAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$DetailsListAdapter;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$DetailsListAdapter;->getCount()I

    move-result v3

    .line 73
    .local v3, "size":I
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v3, :cond_3

    .line 74
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->mAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$DetailsListAdapter;

    invoke-virtual {v5, v2, v7, v7}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$DetailsListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 73
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 76
    :cond_3
    return-void
.end method

.method private setupHeaderView(Landroid/view/ViewGroup;)V
    .locals 6
    .param p1, "headerView"    # Landroid/view/ViewGroup;

    .prologue
    .line 113
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 114
    .local v0, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    const/4 v1, 0x0

    .line 115
    .local v1, "hasBaseInfo":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v3, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->BASE_DETAIL_INDEX:[I

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 116
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->BASE_DETAIL_INDEX:[I

    aget v4, v4, v2

    sget-object v5, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->BASE_DETAIL_RESOURCE_ID:[I

    aget v5, v5, v2

    invoke-static {v3, p1, v0, v4, v5}, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->setupBaseInfoView(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/sec/android/gallery3d/data/MediaDetails;II)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 119
    const/4 v1, 0x1

    .line 115
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 122
    :cond_1
    if-nez v1, :cond_2

    .line 123
    const v3, 0x7f0f00d2

    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 124
    :cond_2
    return-void
.end method

.method private startLoadBitmapTask()V
    .locals 3

    .prologue
    .line 143
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    .line 144
    .local v0, "threadPool":Lcom/sec/android/gallery3d/util/ThreadPool;
    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$LoadBitmapDataTask;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$LoadBitmapDataTask;-><init>(Lcom/sec/android/gallery3d/data/MediaItem;)V

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$2;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$2;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    .line 158
    return-void
.end method


# virtual methods
.method public actionEditCancel()V
    .locals 0

    .prologue
    .line 102
    return-void
.end method

.method public actionEditDone()V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->getView()Landroid/view/View;

    .line 81
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->mView:Landroid/view/View;

    return-object v0
.end method

.method public invalidate()V
    .locals 0

    .prologue
    .line 92
    return-void
.end method

.method public setHistogram(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 127
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    .line 128
    .local v0, "threadPool":Lcom/sec/android/gallery3d/util/ThreadPool;
    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$LoadHistogramTask;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$LoadHistogramTask;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;Landroid/graphics/Bitmap;)V

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$1;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails$1;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemDetails;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    .line 140
    return-void
.end method

.method public setMode(Z)V
    .locals 0
    .param p1, "editMode"    # Z

    .prologue
    .line 86
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->setMode(Z)V

    .line 88
    return-void
.end method
