.class Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;
.super Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;
.source "DetailActionBarForMotionPreview.java"


# instance fields
.field private mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x1

    .line 19
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 21
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 23
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mMainActionBar:Landroid/app/ActionBar;

    const v1, 0x7f020311

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setIcon(I)V

    .line 25
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    .line 27
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->setTitle()V

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method private setTitle()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview$1;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 113
    return-void
.end method


# virtual methods
.method protected initActionBarBg(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 85
    return-void
.end method

.method public isManualRotateIconEnabled()Z
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->isRotationMenuItemEnabled()Z

    move-result v0

    .line 120
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v2, 0x7f0f0298

    .line 32
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 33
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f120042

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 35
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v1, :cond_0

    .line 36
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->setRotationMenuItem(Landroid/view/MenuItem;)V

    .line 41
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->setCameraButtonVisibility(Landroid/view/Menu;Z)V

    .line 42
    return-void

    .line 38
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0
.end method

.method protected onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 1
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 57
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 59
    :sswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryMotion()Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->showMotionTutorialInfoDialog()V

    goto :goto_0

    .line 62
    :sswitch_1
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->handleScreenRotation()V

    goto :goto_0

    .line 57
    :sswitch_data_0
    .sparse-switch
        0x7f0f0298 -> :sswitch_1
        0x7f0f02dd -> :sswitch_0
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 71
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->onPause()V

    .line 74
    :cond_0
    return-void
.end method

.method protected onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 46
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 78
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->onResume()V

    .line 81
    :cond_0
    return-void
.end method

.method protected setupButtons()V
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;->showHomeButtonOnTopLeft()V

    .line 53
    :cond_0
    return-void
.end method
