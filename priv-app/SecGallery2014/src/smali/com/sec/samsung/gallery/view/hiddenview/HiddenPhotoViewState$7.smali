.class Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$7;
.super Ljava/lang/Object;
.source "HiddenPhotoViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$7;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;I)V
    .locals 2
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/GlAbsListView;
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "position"    # I

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$7;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # setter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mScrolledPosition:I
    invoke-static {v0, p3}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$1102(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;I)I

    .line 233
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$7;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    const/4 v1, 0x0

    # invokes: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->enterSelectionMode(Z)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$1400(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;Z)V

    .line 234
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$7;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # invokes: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->selectMedia(I)V
    invoke-static {v0, p3}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$1200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;I)V

    .line 235
    return-void
.end method
