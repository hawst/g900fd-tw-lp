.class public Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
.super Lcom/sec/android/gallery3d/app/ActivityState;
.source "SlideShowViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;
.implements Ljava/util/Observer;


# static fields
.field private static final ACTION_PALM_DOWN:Ljava/lang/String; = "android.intent.action.PALM_DOWN"

.field private static final ACTION_USB_PERMISSION:Ljava/lang/String; = "android.mtp.MtpClient.action.USB_PERMISSION"

.field private static final BURST_PLAY_DURATION_X02:I = 0x5dc

.field private static final BURST_PLAY_DURATION_X05:I = 0x258

.field private static final BURST_PLAY_DURATION_X1:I = 0x12c

.field public static final BURST_PLAY_DURATION_X2:I = 0x96

.field private static final BURST_PLAY_EFFECT_NUMBER:I = 0x3e8

.field private static final DEBUG:Z = true

.field public static final KEY_BURSTPLAY:Ljava/lang/String; = "burstplay"

.field public static final KEY_BURSTPLAYSPEED:Ljava/lang/String; = "burstplayspeed"

.field public static final KEY_EDITMODE:Ljava/lang/String; = "editmode"

.field public static final KEY_INDEX:Ljava/lang/String; = "index"

.field public static final KEY_PATH:Ljava/lang/String; = "path"

.field private static final MSG_ORIENTATION_CHANGE_DELAY:I = 0x1

.field private static final MULTI_PICK_INTENT:I = 0x0

.field private static final PAUSE_MUSIC:I = 0xa

.field public static final REQUEST_SLIDESHOW_PAUSE_ACTIVITY:I = 0xb05

.field public static final REQUEST_SLIDESHOW_RESEME:I = 0xb01

.field public static final REQUEST_SLIDESHOW_SETTINGS:I = 0xb03

.field public static final REQUEST_SLIDESHOW_STOP:I = 0xb02

.field public static final REQUEST_SLIDESHOW_VIDEO_CLIP:I = 0xb04

.field private static final TAG:Ljava/lang/String;

.field public static isGettingFinished:Z

.field public static isSlideShowPaused:Z

.field private static mDockStatePrevious:I

.field private static mSmartDockStatePrevious:I


# instance fields
.field private SLID:Ljava/lang/String;

.field createVideoClip:Landroid/widget/Button;

.field private mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

.field private mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field private mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

.field private mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private mEffectDuration:I

.field private mEffectNumber:I

.field private mGalleryId:I

.field private mGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field mHandler:Landroid/os/Handler;

.field private mHomeSyncController:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mIsBurstPlay:Z

.field private mIsEditmode:Z

.field private mIsEnabled:Z

.field private mIsFullScreen:Z

.field public mIsMusicPaused:Z

.field private mIsPresentationStarted:Z

.field private mIsWaitingResponse:Z

.field private mMotionListener:Lcom/samsung/android/motion/MRListener;

.field private mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

.field private mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

.field private mMultiWindowsStatue:Z

.field private mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

.field private mNeedToSendState:Z

.field private mOrientationChangeHandler:Landroid/os/Handler;

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mPinupState:Z

.field public mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mSelectedItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;"
        }
    .end annotation
.end field

.field private mSlideShowPauseView:Landroid/view/View;

.field private mSlideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mkeyboardConfig:I

.field slideShowResume:Landroid/widget/ImageButton;

.field slideShowStop:Landroid/widget/Button;

.field slideShowsettings:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 107
    const-class v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;

    .line 169
    sput-boolean v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isSlideShowPaused:Z

    .line 172
    sput v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mDockStatePrevious:I

    .line 174
    sput v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSmartDockStatePrevious:I

    .line 180
    sput-boolean v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isGettingFinished:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 106
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;-><init>()V

    .line 111
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    .line 113
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    .line 139
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    .line 142
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    .line 144
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mEffectDuration:I

    .line 146
    const/16 v0, 0x63

    iput v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mEffectNumber:I

    .line 148
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mGalleryId:I

    .line 152
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsFullScreen:Z

    .line 161
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsPresentationStarted:Z

    .line 163
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsMusicPaused:Z

    .line 167
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsBurstPlay:Z

    .line 168
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsEditmode:Z

    .line 176
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mHomeSyncController:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    .line 178
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mNeedToSendState:Z

    .line 198
    const-string v0, "SLID"

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->SLID:Ljava/lang/String;

    .line 202
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsWaitingResponse:Z

    .line 208
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$1;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 701
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$5;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$5;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mHandler:Landroid/os/Handler;

    .line 765
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$6;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$6;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 875
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$8;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$8;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    .line 1208
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$14;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$14;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mOrientationChangeHandler:Landroid/os/Handler;

    .line 1282
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$15;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$15;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowPauseView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsFullScreen:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 106
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsFullScreen:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setFullScreenMode(Z)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    return-object v0
.end method

.method static synthetic access$1400()I
    .locals 1

    .prologue
    .line 106
    sget v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mDockStatePrevious:I

    return v0
.end method

.method static synthetic access$1402(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 106
    sput p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mDockStatePrevious:I

    return p0
.end method

.method static synthetic access$1500()I
    .locals 1

    .prologue
    .line 106
    sget v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSmartDockStatePrevious:I

    return v0
.end method

.method static synthetic access$1502(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 106
    sput p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSmartDockStatePrevious:I

    return p0
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setSlideshowPauseViewEnable(Z)V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    .param p1, "x1"    # I

    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->printDisplayInfo(I)V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsBurstPlay:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSelectedItems:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Ljava/util/List;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->getUriList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsEditmode:Z

    return v0
.end method

.method static synthetic access$2400(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 106
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsWaitingResponse:Z

    return p1
.end method

.method static synthetic access$2800(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsPresentationStarted:Z

    return v0
.end method

.method static synthetic access$2900(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->finishSlideshowPauseView()V

    return-void
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->initSlideshowPauseView()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isMusicEnabled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$800()I
    .locals 1

    .prologue
    .line 106
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->getSystemUiFlagRemoveNatigation()I

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    return-object v0
.end method

.method private addController()V
    .locals 3

    .prologue
    .line 909
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    .line 910
    new-instance v0, Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$9;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$9;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/homesync/HomeSyncController;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mHomeSyncController:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    .line 942
    :cond_0
    return-void
.end method

.method private addListenersOnButton()V
    .locals 3

    .prologue
    .line 1083
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f0220

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->createVideoClip:Landroid/widget/Button;

    .line 1084
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f021f

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->slideShowsettings:Landroid/widget/Button;

    .line 1085
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f021d

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->slideShowStop:Landroid/widget/Button;

    .line 1086
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f021e

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->slideShowResume:Landroid/widget/ImageButton;

    .line 1088
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$13;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V

    .line 1164
    .local v0, "filterClickListener":Landroid/view/View$OnClickListener;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->createVideoClip:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1165
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->slideShowsettings:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1166
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->slideShowStop:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1167
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->slideShowResume:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1168
    return-void
.end method

.method private finishSlideshowPauseView()V
    .locals 4

    .prologue
    .line 1046
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowPauseView:Landroid/view/View;

    if-nez v1, :cond_0

    .line 1057
    :goto_0
    return-void

    .line 1049
    :cond_0
    const/4 v1, 0x0

    :try_start_0
    sput-boolean v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isSlideShowPaused:Z

    .line 1051
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowPauseView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowPauseView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/ViewManager;->removeView(Landroid/view/View;)V

    .line 1052
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->overridePendingTransition(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1054
    :catch_0
    move-exception v0

    .line 1055
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private getEffectNo(Landroid/content/Context;)I
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 653
    const/4 v1, 0x0

    .line 654
    .local v1, "effectType":I
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsBurstPlay:Z

    if-eqz v2, :cond_0

    .line 655
    const/4 v0, 0x0

    .line 656
    .local v0, "burstPlaySpeed":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget v2, v2, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->mBurstPlaySpeed:I

    packed-switch v2, :pswitch_data_0

    .line 672
    :goto_0
    const/16 v2, 0x3e8

    iput v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mEffectNumber:I

    .line 673
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    const/4 v3, 0x1

    iget v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mEffectNumber:I

    invoke-virtual {v2, v0, v3, v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->setSlideShowInfo(III)V

    .line 676
    .end local v0    # "burstPlaySpeed":I
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mEffectNumber:I

    sparse-switch v2, :sswitch_data_0

    .line 694
    const/4 v1, 0x1

    .line 698
    :goto_1
    return v1

    .line 658
    .restart local v0    # "burstPlaySpeed":I
    :pswitch_0
    const/16 v0, 0x5dc

    .line 659
    goto :goto_0

    .line 661
    :pswitch_1
    const/16 v0, 0x258

    .line 662
    goto :goto_0

    .line 664
    :pswitch_2
    const/16 v0, 0x12c

    .line 665
    goto :goto_0

    .line 667
    :pswitch_3
    const/16 v0, 0x96

    .line 668
    goto :goto_0

    .line 678
    .end local v0    # "burstPlaySpeed":I
    :sswitch_0
    const/4 v1, 0x0

    .line 679
    goto :goto_1

    .line 681
    :sswitch_1
    const/4 v1, 0x1

    .line 682
    goto :goto_1

    .line 684
    :sswitch_2
    const/4 v1, 0x2

    .line 685
    goto :goto_1

    .line 688
    :sswitch_3
    const/4 v1, 0x4

    .line 689
    goto :goto_1

    .line 691
    :sswitch_4
    const/4 v1, 0x0

    .line 692
    goto :goto_1

    .line 656
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 676
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_3
        0x3e8 -> :sswitch_4
    .end sparse-switch
.end method

.method private static getSystemUiFlagRemoveNatigation()I
    .locals 6

    .prologue
    .line 433
    const/16 v2, 0x200

    .line 435
    .local v2, "viewFlag":I
    :try_start_0
    const-class v3, Landroid/view/View;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 436
    .local v0, "clas":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v3, "SYSTEM_UI_FLAG_REMOVE_NAVIGATION"

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 440
    .end local v0    # "clas":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_0
    return v2

    .line 437
    :catch_0
    move-exception v1

    .line 438
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cannot retrieve view flag : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getUriList(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1225
    .local p1, "selectedItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1226
    .local v6, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1227
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v3, :cond_1

    .line 1228
    sget-object v7, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;

    const-string v8, "selected item\'s uri is null, item is ignored"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1229
    :cond_1
    instance-of v7, v3, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v7, :cond_2

    move-object v7, v3

    .line 1230
    check-cast v7, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    .line 1231
    .local v0, "count":I
    check-cast v3, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v3    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    const/4 v7, 0x0

    invoke-virtual {v3, v7, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v4

    .line 1232
    .local v4, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1233
    .local v5, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1236
    .end local v0    # "count":I
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v5    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v3    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1239
    .end local v3    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_3
    return-object v6
.end method

.method private initSlideshowPauseView()V
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, -0x1

    .line 1009
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1010
    .local v0, "dm":Landroid/util/DisplayMetrics;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1011
    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1012
    .local v3, "width":I
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 1013
    .local v1, "height":I
    if-ge v3, v1, :cond_5

    .line 1014
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0300db

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowPauseView:Landroid/view/View;

    .line 1016
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowPauseView:Landroid/view/View;

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v6, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5, v6}, Landroid/app/Activity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1025
    :goto_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isMusicEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    if-eqz v4, :cond_0

    .line 1026
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->pauseMusic()V

    .line 1028
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->addListenersOnButton()V

    .line 1029
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVideoClip:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1030
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->createVideoClip:Landroid/widget/Button;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 1031
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v9, :cond_2

    .line 1032
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->slideShowsettings:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 1033
    .local v2, "params":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->createVideoClip:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getMinHeight()I

    move-result v4

    invoke-virtual {v2, v8, v8, v8, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1034
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->slideShowsettings:Landroid/widget/Button;

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1036
    .end local v2    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->slideShowsettings:Landroid/widget/Button;

    invoke-virtual {v4, v9}, Landroid/widget/Button;->setGravity(I)V

    .line 1039
    :cond_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getSlideshowVideoView()Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    move-result-object v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getSlideshowVideoView()Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    move-result-object v4

    iget-boolean v4, v4, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->misVideoPlaying:Z

    if-eqz v4, :cond_4

    .line 1041
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getSlideshowVideoView()Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->pauseVideo()V

    .line 1043
    :cond_4
    return-void

    .line 1019
    :cond_5
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0300dc

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowPauseView:Landroid/view/View;

    .line 1021
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowPauseView:Landroid/view/View;

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v6, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5, v6}, Landroid/app/Activity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0
.end method

.method private isMusicEnabled()Z
    .locals 1

    .prologue
    .line 870
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsBurstPlay:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private printDisplayInfo(I)V
    .locals 5
    .param p1, "displayId"    # I

    .prologue
    .line 896
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v2, p1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v0

    .line 897
    .local v0, "display":Landroid/view/Display;
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 898
    .local v1, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 899
    sget-object v2, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Display ID] : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 900
    sget-object v2, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Name : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/view/Display;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 901
    sget-object v2, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Size : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " x "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 902
    return-void
.end method

.method private registerBroadcastReceiver()V
    .locals 6

    .prologue
    .line 781
    new-instance v4, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$7;

    invoke-direct {v4, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$7;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    .line 832
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 833
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v4, "android.intent.action.PALM_DOWN"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 834
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 837
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 838
    .local v1, "intentFilter2":Landroid/content/IntentFilter;
    const-string v4, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 839
    const-string v4, "file"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 840
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v4, v5, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 843
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 844
    .local v2, "intentFilter3":Landroid/content/IntentFilter;
    const-string v4, "android.intent.action.DOCK_EVENT"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 845
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v4, v5, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 849
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 850
    .local v3, "mtpInetentFilter":Landroid/content/IntentFilter;
    const-string v4, "android.hardware.usb.action.USB_DEVICE_ATTACHED"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 851
    const-string v4, "android.hardware.usb.action.USB_DEVICE_DETACHED"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 852
    const-string v4, "android.mtp.MtpClient.action.USB_PERMISSION"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 853
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v4, v5, v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 855
    return-void
.end method

.method private registerLayoutListener()V
    .locals 3

    .prologue
    .line 760
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 761
    .local v0, "deco":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    .line 762
    .local v1, "vto2":Landroid/view/ViewTreeObserver;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 763
    return-void
.end method

.method private setFullScreenMode(Z)V
    .locals 3
    .param p1, "isFullMode"    # Z

    .prologue
    .line 744
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHWKey:Z

    if-eqz v1, :cond_1

    .line 757
    :cond_0
    :goto_0
    return-void

    .line 748
    :cond_1
    const-class v1, Landroid/view/View;

    const-string v2, "SYSTEM_UI_FLAG_REMOVE_NAVIGATION"

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDeclaredIntField(Ljava/lang/Class;Ljava/lang/String;)I

    move-result v0

    .line 750
    .local v0, "systemUiFlagRemoveNavigation":I
    if-eqz p1, :cond_2

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 751
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    or-int/lit8 v2, v0, 0x4

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setSystemUiVisibility(I)V

    goto :goto_0

    .line 754
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v2, 0x600

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setSystemUiVisibility(I)V

    goto :goto_0
.end method

.method private setShowWhenLocked(Z)V
    .locals 1
    .param p1, "on"    # Z

    .prologue
    .line 1244
    if-eqz p1, :cond_0

    .line 1245
    iget v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mFlags:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mFlags:I

    .line 1249
    :goto_0
    return-void

    .line 1247
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mFlags:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mFlags:I

    goto :goto_0
.end method

.method private setSlideshowPauseViewEnable(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 1060
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsEnabled:Z

    if-ne v0, p1, :cond_1

    .line 1072
    :cond_0
    :goto_0
    return-void

    .line 1062
    :cond_1
    if-eqz p1, :cond_2

    .line 1063
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->initSlideshowPauseView()V

    .line 1067
    :goto_1
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsEnabled:Z

    .line 1069
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isMusicEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    if-eqz v0, :cond_0

    .line 1070
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsEnabled:Z

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->slideShowPause(Z)V

    goto :goto_0

    .line 1065
    :cond_2
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->finishSlideshowPauseView()V

    goto :goto_1
.end method


# virtual methods
.method public finishCurrentViewState()V
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$2;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 264
    return-void
.end method

.method public getGalleryId()I
    .locals 1

    .prologue
    .line 905
    iget v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mGalleryId:I

    return v0
.end method

.method public getGlSlideShowView()Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    return-object v0
.end method

.method public getMultiWindow()Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .locals 1

    .prologue
    .line 740
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    return-object v0
.end method

.method public getSlideShowAdapter()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    return-object v0
.end method

.method public declared-synchronized getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;
    .locals 1

    .prologue
    .line 736
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isSlideshowPauseViewEnable()Z
    .locals 1

    .prologue
    .line 1076
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsEnabled:Z

    return v0
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 717
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isSlideshowPauseViewEnable()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 718
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setSlideshowPauseViewEnable(Z)V

    .line 719
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    if-eqz v1, :cond_0

    .line 720
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->setIsShowing(Z)V

    .line 721
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getSlideshowVideoView()Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    move-result-object v0

    .line 722
    .local v0, "videoView":Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;
    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->misVideoPlaying:Z

    if-eqz v1, :cond_0

    .line 723
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->resumeVideoPlay()V

    .line 726
    .end local v0    # "videoView":Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isMusicEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsMusicPaused:Z

    if-nez v1, :cond_1

    .line 727
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->setMusicLooping(Z)V

    .line 728
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->playMusic()V

    .line 733
    :cond_1
    :goto_0
    return-void

    .line 732
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public onConfigChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 391
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;

    const-string v1, "call onConfigurationChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    iget v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mkeyboardConfig:I

    iget v1, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-eq v0, v1, :cond_0

    .line 393
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;

    const-string v1, "Stop slideshow by hard keyboard hidden"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->finishCurrentViewState()V

    .line 398
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mOrientationChangeHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 399
    return-void

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->onConfigChanged(Landroid/content/res/Configuration;)V

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 226
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 227
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->onConfigChanged(Landroid/content/res/Configuration;)V

    .line 228
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 12
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    const/16 v10, 0x400

    const/16 v11, 0x20

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 276
    sget-object v6, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;

    const-string v9, "onCreate()"

    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f00bf

    invoke-virtual {v6, v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 279
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFullScreen(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 280
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6, v10, v10}, Landroid/view/Window;->setFlags(II)V

    .line 284
    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->hideStatusIcon(Ljava/lang/Object;)V

    .line 286
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    .line 287
    .local v0, "app":Lcom/sec/android/gallery3d/app/GalleryAppImpl;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getSlideShowSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 288
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCloneMediaList()Ljava/util/LinkedList;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSelectedItems:Ljava/util/List;

    .line 289
    new-instance v6, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v6, v9, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;-><init>(Landroid/content/Context;Landroid/os/Bundle;)V

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    .line 290
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v6, :cond_1

    .line 291
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v6, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getMultiWindowActivity()Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 292
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v6

    if-nez v6, :cond_a

    move v6, v7

    :goto_0
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsFullScreen:Z

    .line 293
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsFullScreen:Z

    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setFullScreenMode(Z)V

    .line 294
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->registerLayoutListener()V

    .line 297
    :cond_1
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v6

    const-string v9, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_TRUN_OVER"

    invoke-virtual {v6, v9}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 299
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v9, "motion_recognition"

    invoke-virtual {v6, v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/motion/MotionRecognitionManager;

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    .line 302
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    if-eqz v6, :cond_2

    .line 303
    new-instance v6, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$3;

    invoke-direct {v6, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$3;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    .line 313
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    const/16 v10, 0xa

    invoke-virtual {v6, v9, v10}, Lcom/samsung/android/motion/MotionRecognitionManager;->registerListenerEvent(Lcom/samsung/android/motion/MRListener;I)V

    .line 317
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v9, "phone"

    invoke-virtual {v6, v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/telephony/TelephonyManager;

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 318
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v6, v9, v11}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 321
    if-eqz p1, :cond_7

    .line 322
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v4

    .line 324
    .local v4, "setting":Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getEffect()I

    move-result v2

    .line 325
    .local v2, "effectNumber":I
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getMusicUri()Ljava/lang/String;

    move-result-object v3

    .line 326
    .local v3, "music":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSpeed()I

    move-result v5

    .line 328
    .local v5, "speed":I
    const-string v6, "clusterType"

    invoke-virtual {p1, v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 332
    .local v1, "clusterType":Ljava/lang/Integer;
    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setIndicatorVisibility(Z)V

    .line 334
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->setCurrentClusterType(I)V

    .line 335
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getCurrentClusterType()I

    move-result v6

    if-eq v6, v11, :cond_3

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getCurrentClusterType()I

    move-result v6

    const/16 v9, 0x80

    if-ne v6, v9, :cond_4

    .line 337
    :cond_3
    invoke-virtual {v0, v7}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->setCleanCacheStatus(Z)V

    .line 338
    invoke-virtual {v0, v7}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->setRefreshState(Z)V

    .line 341
    :cond_4
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getEffectTimeMsec(I)I

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mEffectDuration:I

    .line 342
    iput v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mEffectNumber:I

    .line 344
    if-eqz p1, :cond_5

    .line 345
    const-string v6, "GALLERY_ACTIVITY_ID"

    const/4 v9, -0x1

    invoke-virtual {p1, v6, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mGalleryId:I

    .line 346
    const-string v6, "burstplay"

    invoke-virtual {p1, v6, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsBurstPlay:Z

    .line 347
    const-string v6, "editmode"

    invoke-virtual {p1, v6, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsEditmode:Z

    .line 350
    :cond_5
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v6

    if-eqz v6, :cond_6

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsBurstPlay:Z

    if-eqz v6, :cond_6

    .line 351
    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setShowWhenLocked(Z)V

    .line 353
    :cond_6
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isMusicEnabled()Z

    move-result v6

    if-eqz v6, :cond_7

    if-eqz v3, :cond_7

    .line 354
    new-instance v6, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v6, v8}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    .line 355
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6, v8, v3}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->preparePlayWithUri(Landroid/content/Context;Ljava/lang/String;)V

    .line 359
    .end local v1    # "clusterType":Ljava/lang/Integer;
    .end local v2    # "effectNumber":I
    .end local v3    # "music":Ljava/lang/String;
    .end local v4    # "setting":Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    .end local v5    # "speed":I
    :cond_7
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v6, :cond_8

    invoke-static {}, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->isStartSlideshowByHomeSyncController()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 360
    iput-boolean v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mNeedToSendState:Z

    .line 361
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->addController()V

    .line 364
    :cond_8
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryCoverMode()Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    move-result-object v6

    if-eqz v6, :cond_9

    .line 365
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryCoverMode()Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 368
    :cond_9
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->SLID:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6, v7, v8}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 369
    return-void

    :cond_a
    move v6, v8

    .line 292
    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1272
    return-void
.end method

.method public onDestroy()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 445
    sget-object v4, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;

    const-string v5, "onDestroy()"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    sput-boolean v8, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isGettingFinished:Z

    .line 447
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 448
    .local v0, "actionbar":Landroid/app/ActionBar;
    if-eqz v0, :cond_0

    .line 449
    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 452
    :cond_0
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setIndicatorVisibility(Z)V

    .line 454
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isSlideshowPauseViewEnable()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 455
    invoke-direct {p0, v8}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setSlideshowPauseViewEnable(Z)V

    .line 457
    :cond_1
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsPresentationStarted:Z

    if-nez v4, :cond_2

    .line 458
    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setSlideShowMode(Z)V

    .line 460
    :cond_2
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsPresentationStarted:Z

    if-nez v4, :cond_3

    .line 461
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isMusicEnabled()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    if-eqz v4, :cond_3

    .line 462
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->destroy()V

    .line 463
    iput-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    .line 468
    :cond_3
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    if-eqz v4, :cond_5

    .line 469
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    if-eqz v4, :cond_4

    .line 470
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    invoke-virtual {v4, v5}, Lcom/samsung/android/motion/MotionRecognitionManager;->unregisterListener(Lcom/samsung/android/motion/MRListener;)V

    .line 471
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    .line 473
    :cond_4
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    .line 476
    :cond_5
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    if-eqz v4, :cond_7

    .line 477
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryCoverMode()Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 478
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryCoverMode()Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 480
    :cond_6
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 483
    :cond_7
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-eqz v4, :cond_8

    .line 484
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 485
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 488
    :cond_8
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    if-eqz v4, :cond_9

    .line 489
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 496
    :cond_9
    :goto_0
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v4, :cond_a

    .line 497
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    .line 498
    .local v1, "deco":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    .line 499
    .local v3, "vto2":Landroid/view/ViewTreeObserver;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v3, v4}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 500
    iput-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 502
    .end local v1    # "deco":Landroid/view/View;
    .end local v3    # "vto2":Landroid/view/ViewTreeObserver;
    :cond_a
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    if-eqz v4, :cond_b

    .line 503
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v4, v8}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->isSlideShowActive(Z)V

    .line 504
    :cond_b
    return-void

    .line 492
    :catch_0
    move-exception v2

    .line 493
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 0
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 1280
    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 508
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/homesync/HomeSyncUtil;->fadePointer(Landroid/content/Context;Z)V

    .line 512
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->isServiceRunning(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 513
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->clearKeepScreenOnFlag(Landroid/app/Activity;)V

    .line 515
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsBurstPlay:Z

    if-eqz v0, :cond_1

    .line 516
    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setShowWhenLocked(Z)V

    .line 518
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    if-eqz v0, :cond_2

    .line 519
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->finishVideoView()V

    .line 520
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->pause()V

    .line 522
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isSlideshowPauseViewEnable()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsPresentationStarted:Z

    if-nez v0, :cond_3

    .line 523
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->onPause()V

    .line 526
    :cond_3
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setIndicatorVisibility(Z)V

    .line 528
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsPresentationStarted:Z

    if-nez v0, :cond_4

    .line 530
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isMusicEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    if-eqz v0, :cond_4

    .line 531
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->pauseMusic(Z)V

    .line 535
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_5

    .line 536
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V

    .line 539
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_6

    .line 540
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 541
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    .line 544
    :cond_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mHomeSyncController:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    if-eqz v0, :cond_7

    .line 545
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mHomeSyncController:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->unregisterBroadCastReceiver()V

    .line 546
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mNeedToSendState:Z

    if-eqz v0, :cond_7

    .line 547
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->SendSlideShowStopState(Landroid/content/Context;)V

    .line 552
    :cond_7
    sput-boolean v2, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isGettingFinished:Z

    .line 553
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isSlideshowPauseViewEnable()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 554
    sput-boolean v2, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isSlideShowPaused:Z

    .line 555
    :cond_8
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsWaitingResponse:Z

    if-nez v0, :cond_9

    .line 556
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->finishCurrentViewState()V

    .line 558
    :cond_9
    return-void
.end method

.method public onPrepareOptionMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1276
    return-void
.end method

.method public onResume()V
    .locals 14

    .prologue
    const/4 v10, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 562
    sget-object v7, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;

    const-string v11, "onResume()"

    invoke-static {v7, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 565
    .local v2, "config":Landroid/content/res/Configuration;
    iget v7, v2, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    iput v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mkeyboardConfig:I

    .line 566
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/homesync/HomeSyncUtil;->fadePointer(Landroid/content/Context;Z)V

    .line 568
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->addKeepScreenOnFlag(Landroid/app/Activity;)V

    .line 569
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 570
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v7, :cond_0

    .line 571
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v7

    if-nez v7, :cond_3

    move v7, v8

    :goto_0
    iput-boolean v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMultiWindowsStatue:Z

    .line 572
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isScaleWindow()Z

    move-result v7

    iput-boolean v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mPinupState:Z

    .line 574
    :cond_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseVideoClip(Landroid/content/pm/PackageManager;)V

    .line 576
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    if-eqz v7, :cond_1

    .line 577
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getCount()I

    move-result v7

    if-eqz v7, :cond_6

    .line 578
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->onResume()V

    .line 579
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget v11, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mEffectDuration:I

    iget v12, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mEffectDuration:I

    iget v13, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mEffectNumber:I

    invoke-virtual {v7, v11, v12, v13}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->setSlideShowInfo(III)V

    .line 580
    new-instance v7, Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v12, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v12}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getStartIndex()I

    move-result v12

    iget-object v13, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {p0, v13}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->getEffectNo(Landroid/content/Context;)I

    move-result v13

    invoke-direct {v7, v11, v12, v13}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;-><init>(Landroid/content/Context;II)V

    iput-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    .line 582
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowPauseView:Landroid/view/View;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowPauseView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-nez v7, :cond_4

    move v7, v8

    :goto_1
    invoke-virtual {v11, v7}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->setIsShowing(Z)V

    .line 584
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget-boolean v11, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsBurstPlay:Z

    invoke-virtual {v7, v11}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->setBurstShotMode(Z)V

    .line 592
    :cond_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    if-eqz v7, :cond_2

    .line 593
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v7, p0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->setInstance(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V

    .line 594
    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setSlideShowMode(Z)V

    .line 598
    :cond_2
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v11, "display"

    invoke-virtual {v7, v11}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/hardware/display/DisplayManager;

    iput-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 599
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    const-string v11, "android.hardware.display.category.PRESENTATION"

    invoke-virtual {v7, v11}, Landroid/hardware/display/DisplayManager;->getDisplays(Ljava/lang/String;)[Landroid/view/Display;

    move-result-object v3

    .line 601
    .local v3, "displays":[Landroid/view/Display;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    invoke-virtual {v7, v11, v10}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    .line 603
    const/4 v4, 0x0

    .local v4, "i":I
    array-length v6, v3

    .local v6, "n":I
    :goto_2
    if-ge v4, v6, :cond_8

    .line 605
    aget-object v7, v3, v4

    invoke-virtual {v7}, Landroid/view/Display;->getDisplayId()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->printDisplayInfo(I)V

    .line 603
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .end local v3    # "displays":[Landroid/view/Display;
    .end local v4    # "i":I
    .end local v6    # "n":I
    :cond_3
    move v7, v9

    .line 571
    goto/16 :goto_0

    :cond_4
    move v7, v9

    .line 582
    goto :goto_1

    :cond_5
    move v7, v9

    goto :goto_1

    .line 586
    :cond_6
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v8, 0x7f0e006d

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 587
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->finishCurrentViewState()V

    .line 641
    :cond_7
    :goto_3
    return-void

    .line 608
    .restart local v3    # "displays":[Landroid/view/Display;
    .restart local v4    # "i":I
    .restart local v6    # "n":I
    :cond_8
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isExternalDisplayAvailable(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_a

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isConnetedSideSync(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_a

    .line 609
    iput-boolean v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsPresentationStarted:Z

    .line 611
    sget-object v7, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;

    const-string/jumbo v11, "try to start service"

    invoke-static {v7, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    new-instance v5, Landroid/content/Intent;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-class v11, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    invoke-direct {v5, v7, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 613
    .local v5, "intent":Landroid/content/Intent;
    const-string v7, "key_display_id"

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    const-string v12, "android.hardware.display.category.PRESENTATION"

    invoke-virtual {v11, v12}, Landroid/hardware/display/DisplayManager;->getDisplays(Ljava/lang/String;)[Landroid/view/Display;

    move-result-object v11

    aget-object v9, v11, v9

    invoke-virtual {v9}, Landroid/view/Display;->getDisplayId()I

    move-result v9

    invoke-virtual {v5, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 615
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isMusicEnabled()Z

    move-result v7

    if-eqz v7, :cond_9

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    :goto_4
    invoke-static {v7, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->setSlideShowContent(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V

    .line 616
    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setSlideShowMode(Z)V

    .line 617
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7, v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 618
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->finishCurrentViewState()V

    .line 636
    .end local v5    # "intent":Landroid/content/Intent;
    :goto_5
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->registerBroadcastReceiver()V

    .line 638
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mHomeSyncController:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    if-eqz v7, :cond_7

    .line 639
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mHomeSyncController:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->registerBroadCastReceiver()V

    goto :goto_3

    .restart local v5    # "intent":Landroid/content/Intent;
    :cond_9
    move-object v7, v10

    .line 615
    goto :goto_4

    .line 620
    .end local v5    # "intent":Landroid/content/Intent;
    :cond_a
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v7, :cond_b

    .line 621
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const/high16 v10, 0x7f0b0000

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->intColorToFloatARGBArray(I)[F

    move-result-object v1

    .line 622
    .local v1, "color":[F
    if-eqz v1, :cond_b

    .line 623
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    aget v8, v1, v8

    const/4 v10, 0x2

    aget v10, v1, v10

    const/4 v11, 0x3

    aget v11, v1, v11

    aget v12, v1, v9

    invoke-virtual {v7, v8, v10, v11, v12}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setGlBackgroundColor(FFFF)V

    .line 627
    .end local v1    # "color":[F
    :cond_b
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 628
    .local v0, "actionbar":Landroid/app/ActionBar;
    if-eqz v0, :cond_c

    .line 629
    invoke-virtual {v0, v9}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 630
    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 632
    :cond_c
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v7, v8, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->attachLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V

    goto :goto_5
.end method

.method public onStateResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 1172
    if-nez p2, :cond_0

    .line 1173
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->finishSlideshowPauseView()V

    .line 1175
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 1205
    :goto_0
    return-void

    .line 1178
    :pswitch_0
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsWaitingResponse:Z

    .line 1180
    if-eqz p3, :cond_1

    .line 1182
    const-string v4, "selectedItems"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 1185
    .local v3, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1186
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "com.sec.android.app.storycam"

    const-string v5, "com.sec.android.app.storycam.activity.VideoEditorLiteActivity"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1188
    const-string v2, "*/*"

    .line 1189
    .local v2, "mimeType":Ljava/lang/String;
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1190
    const-string v4, "android.intent.action.EDIT"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1191
    const-string v4, "android.intent.extra.STREAM"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1193
    if-eqz v1, :cond_1

    .line 1195
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1202
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "mimeType":Ljava/lang/String;
    .end local v3    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->finishCurrentViewState()V

    goto :goto_0

    .line 1196
    .restart local v1    # "intent":Landroid/content/Intent;
    .restart local v2    # "mimeType":Ljava/lang/String;
    .restart local v3    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :catch_0
    move-exception v0

    .line 1197
    .local v0, "ex":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 1175
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 2
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 946
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->isSlideShowActive(Z)V

    .line 947
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V

    .line 948
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$10;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$10;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->setOnItemClickListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;)V

    .line 966
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$11;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$11;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->setOnKeyListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;)V

    .line 979
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$12;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$12;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->setOnLoadingListener(Lcom/sec/samsung/gallery/glview/GlSlideShowView$OnLoadingListener;)V

    .line 990
    return-void
.end method

.method public onWindowStatusChanged(ZZZ)V
    .locals 3
    .param p1, "isNormalWindow"    # Z
    .param p2, "isMinimized"    # Z
    .param p3, "isPinup"    # Z

    .prologue
    .line 372
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v0, :cond_0

    .line 373
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mPinupState:Z

    if-eq v0, p3, :cond_1

    .line 374
    iput-boolean p3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mPinupState:Z

    .line 375
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMultiWindowsStatue:Z

    if-eq v0, p1, :cond_0

    .line 376
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMultiWindowsStatue:Z

    .line 377
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "call onWindowStatusChanged (para 3)  ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]  ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    :cond_0
    :goto_0
    return-void

    .line 381
    :cond_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMultiWindowsStatue:Z

    if-eq v0, p1, :cond_0

    .line 382
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMultiWindowsStatue:Z

    .line 383
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "call onWindowStatusChanged (para 3)  ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]  ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public pauseSlideshowMusic()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1252
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isMusicEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    if-eqz v0, :cond_0

    .line 1253
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->pauseMusic(Z)V

    .line 1254
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsMusicPaused:Z

    .line 1256
    :cond_0
    return-void
.end method

.method public playSlideshowMusic()V
    .locals 2

    .prologue
    .line 1259
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isMusicEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isSlideshowPauseViewEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1260
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->setMusicLooping(Z)V

    .line 1261
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->playMusic()V

    .line 1263
    :cond_0
    return-void
.end method

.method public refreshSlideShowView()V
    .locals 1

    .prologue
    .line 1001
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowPauseView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1002
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowPauseView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1003
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowPauseView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 1005
    :cond_0
    return-void
.end method

.method public releaseWakeLock()V
    .locals 1

    .prologue
    .line 649
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->clearKeepScreenOnFlag(Landroid/app/Activity;)V

    .line 650
    return-void
.end method

.method public setIndicatorVisibility(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 402
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 424
    :goto_0
    return-void

    .line 406
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$4;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$4;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Z)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setSlideShowMode(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 1266
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    if-eqz v0, :cond_0

    .line 1267
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->setSlideShowMode(Z)V

    .line 1268
    :cond_0
    return-void
.end method

.method public startSlideShowPauseActivity()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 994
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    if-eqz v0, :cond_0

    .line 995
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->setIsShowing(Z)V

    .line 997
    :cond_0
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setSlideshowPauseViewEnable(Z)V

    .line 998
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 4
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 232
    move-object v0, p2

    check-cast v0, Lcom/sec/samsung/gallery/core/Event;

    .line 233
    .local v0, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v1

    .line 234
    .local v1, "eventType":I
    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_STOP_SLIDESHOW:I

    if-ne v1, v2, :cond_0

    .line 235
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setIndicatorVisibility(Z)V

    .line 236
    sget-object v2, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;

    const-string v3, "Stop slideshow by event"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->finishCurrentViewState()V

    .line 239
    :cond_0
    return-void
.end method

.method public updateLayer()V
    .locals 2

    .prologue
    .line 644
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    .line 645
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->attachLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V

    .line 646
    :cond_0
    return-void
.end method
