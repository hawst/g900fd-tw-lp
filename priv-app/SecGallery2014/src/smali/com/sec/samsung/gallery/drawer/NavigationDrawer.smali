.class public Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
.super Ljava/lang/Object;
.source "NavigationDrawer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerListOnKeyListener;,
        Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerItemClickListener;
    }
.end annotation


# static fields
.field public static final MSG_NOTIFY_DATASET_CHANGED:I = 0x2

.field public static final MSG_NOTIFY_INFLATE_DRAWER:I = 0x3

.field public static final MSG_SELECT_LIST:I = 0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

.field private mDrawerHelp:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

.field private mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

.field private mDrawerList:Landroid/widget/ListView;

.field private mDrawerMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

.field private mEnableflag:Z

.field private mHandler:Landroid/os/Handler;

.field private mIsGifMode:Z

.field private mIsNeedtoPostInflate:Z

.field private mIsOnlyAlbumModePick:Z

.field private mIsPickMode:Z

.field private mIsSignatureAlbumPickMode:Z

.field private mLinearlayout:Landroid/widget/LinearLayout;

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mSlideOffset:F

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field private mTitle:Ljava/lang/CharSequence;

.field private mToggle:Z

.field private mTouchArea:I

.field private mTouchDown:F

.field private mTouched:Z

.field private mView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const-class v0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mEnableflag:Z

    .line 89
    iput v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mSlideOffset:F

    .line 90
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsPickMode:Z

    .line 91
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsOnlyAlbumModePick:Z

    .line 92
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsSignatureAlbumPickMode:Z

    .line 94
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mToggle:Z

    .line 95
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTouched:Z

    .line 96
    iput v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTouchDown:F

    .line 97
    const/16 v0, 0x28

    iput v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTouchArea:I

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsNeedtoPostInflate:Z

    .line 100
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsGifMode:Z

    .line 103
    new-instance v0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;-><init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mHandler:Landroid/os/Handler;

    .line 150
    new-instance v0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$2;

    const-string v1, "EVENT_NAVIGATION_DRAWER"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$2;-><init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 166
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 167
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 168
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 169
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 170
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
    .param p1, "x1"    # I

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->selectItem(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/samsung/gallery/drawer/DrawerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsPickMode:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsSignatureAlbumPickMode:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mLinearlayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTouched:Z

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mToggle:Z

    return v0
.end method

.method static synthetic access$1702(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mToggle:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    iget v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mSlideOffset:F

    return v0
.end method

.method static synthetic access$1802(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
    .param p1, "x1"    # F

    .prologue
    .line 71
    iput p1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mSlideOffset:F

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->drawerClosed()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->drawerOpened()V

    return-void
.end method

.method static synthetic access$202(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsNeedtoPostInflate:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/samsung/gallery/view/common/DrawerHelpView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerHelp:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setSelectAllFocused(Z)V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Landroid/support/v4/widget/DrawerLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Ljava/lang/CharSequence;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isDrawerHideMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/android/gallery3d/app/StateManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsOnlyAlbumModePick:Z

    return v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->adjustLayout()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Landroid/support/v4/app/ActionBarDrawerToggle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    return-object v0
.end method

.method private adjustLayout()V
    .locals 3

    .prologue
    .line 387
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mLinearlayout:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 388
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mLinearlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout$LayoutParams;

    .line 389
    .local v0, "params":Landroid/support/v4/widget/DrawerLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getActionBarSize(Landroid/content/Context;)I

    move-result v1

    iput v1, v0, Landroid/support/v4/widget/DrawerLayout$LayoutParams;->topMargin:I

    .line 390
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v2, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$11;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$11;-><init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 396
    .end local v0    # "params":Landroid/support/v4/widget/DrawerLayout$LayoutParams;
    :cond_0
    return-void
.end method

.method private checkPickermode()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 744
    const-string v6, "Gallery_Performance"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "NavigationDrawer : mIsPickMode = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsPickMode:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", mIsOnlyAlbumModePick = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsOnlyAlbumModePick:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", mIsSignatureAlbumPickMode = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsSignatureAlbumPickMode:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    iget-object v6, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 746
    .local v1, "intent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v6

    sget-object v7, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v6, v7, :cond_0

    iget-object v6, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v6

    sget-object v7, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v6, v7, :cond_0

    iget-object v6, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v6

    sget-object v7, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v6, v7, :cond_3

    .line 749
    :cond_0
    iput-boolean v10, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsPickMode:Z

    .line 750
    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isSingleAlbumMode()Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsSignatureAlbumPickMode:Z

    .line 751
    iget-object v6, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    invoke-virtual {v6, v10}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->setAlbumPickMode(Z)V

    .line 753
    const-string v6, "album-pick"

    invoke-virtual {v1, v6, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 754
    .local v2, "isAlbumPick":Z
    const-string v6, "onlyMagic"

    invoke-virtual {v1, v6, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 755
    .local v4, "isMagicPick":Z
    const-string v6, "android.intent.action.PERSON_PICK"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 756
    .local v5, "isPersonPick":Z
    const-string v6, "caller_id_pick"

    invoke-virtual {v1, v6, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 758
    .local v3, "isContactPickerMode":Z
    const/4 v0, 0x0

    .line 759
    .local v0, "clusterType":I
    if-eqz v3, :cond_4

    .line 760
    const/16 v0, 0x4000

    .line 768
    :cond_1
    :goto_0
    if-nez v2, :cond_2

    if-nez v4, :cond_2

    if-nez v5, :cond_2

    if-eqz v0, :cond_3

    .line 769
    :cond_2
    iput-boolean v10, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsOnlyAlbumModePick:Z

    .line 772
    .end local v0    # "clusterType":I
    .end local v2    # "isAlbumPick":Z
    .end local v3    # "isContactPickerMode":Z
    .end local v4    # "isMagicPick":Z
    .end local v5    # "isPersonPick":Z
    :cond_3
    return-void

    .line 761
    .restart local v0    # "clusterType":I
    .restart local v2    # "isAlbumPick":Z
    .restart local v3    # "isContactPickerMode":Z
    .restart local v4    # "isMagicPick":Z
    .restart local v5    # "isPersonPick":Z
    :cond_4
    iget-object v6, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v6

    sget-object v7, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v6, v7, :cond_1

    .line 762
    const-string v6, "include-recommend"

    invoke-virtual {v1, v6, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 763
    const/high16 v0, 0x10000

    goto :goto_0

    .line 765
    :cond_5
    const v0, 0x8000

    goto :goto_0
.end method

.method private drawerClosed()V
    .locals 2

    .prologue
    .line 614
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->invalidateOptionsMenu()V

    .line 615
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerList:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 616
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mLinearlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 617
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 618
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerHelp:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerHelp:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->isShowingHelpView()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOverlayHelp:Z

    if-eqz v0, :cond_0

    .line 619
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerHelp:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->removeView()V

    .line 620
    :cond_0
    return-void
.end method

.method private drawerOpened()V
    .locals 2

    .prologue
    .line 623
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mLinearlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->openDrawer(Landroid/view/View;)V

    .line 624
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->invalidateOptionsMenu()V

    .line 625
    return-void
.end method

.method private initDrawerToggle()V
    .locals 7

    .prologue
    .line 424
    const/4 v4, -0x1

    .line 425
    .local v4, "ic_ab":I
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_0

    .line 426
    const v4, 0x7f020500

    .line 430
    :goto_0
    new-instance v0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;

    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const v5, 0x7f0e0437

    const v6, 0x7f0e0438

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;-><init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    .line 522
    return-void

    .line 428
    :cond_0
    const v4, 0x7f0204ff

    goto :goto_0
.end method

.method private isDrawerHideMode()Z
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 821
    const/4 v4, 0x0

    .line 822
    .local v4, "retValue":Z
    iget-object v6, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 823
    .local v1, "intent":Landroid/content/Intent;
    if-nez v1, :cond_0

    .line 842
    :goto_0
    return v5

    .line 826
    :cond_0
    const-string v6, "onlyMagic"

    invoke-virtual {v1, v6, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 827
    .local v3, "isMagicPick":Z
    const-string v6, "caller_id_pick"

    invoke-virtual {v1, v6, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 829
    .local v2, "isContactPickerMode":Z
    const/4 v0, 0x0

    .line 830
    .local v0, "clusterType":I
    if-eqz v2, :cond_4

    .line 831
    const/16 v0, 0x4000

    .line 839
    :cond_1
    :goto_1
    if-nez v3, :cond_2

    if-eqz v0, :cond_3

    .line 840
    :cond_2
    const/4 v4, 0x1

    :cond_3
    move v5, v4

    .line 842
    goto :goto_0

    .line 832
    :cond_4
    iget-object v6, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v6

    sget-object v7, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v6, v7, :cond_1

    .line 833
    const-string v6, "include-recommend"

    invoke-virtual {v1, v6, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 834
    const/high16 v0, 0x10000

    goto :goto_1

    .line 836
    :cond_5
    const v0, 0x8000

    goto :goto_1
.end method

.method private isSingleAlbumMode()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 776
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "single-album"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 778
    :cond_0
    :goto_0
    return v1

    .line 777
    :catch_0
    move-exception v0

    .line 778
    .local v0, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method private refreshTitle()V
    .locals 2

    .prologue
    .line 674
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$13;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$13;-><init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 680
    return-void
.end method

.method private selectItem(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 629
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->getChild(I)Lcom/sec/samsung/gallery/drawer/DrawerItem;

    move-result-object v2

    .line 631
    .local v2, "item":Lcom/sec/samsung/gallery/drawer/DrawerItem;
    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v3, Lcom/sec/android/gallery3d/app/GalleryActivity;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsInViewMode:Z

    .line 633
    add-int/lit8 v1, p1, -0x1

    .line 634
    .local v1, "index":I
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->isGroupNameItem()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 635
    const/4 v1, 0x0

    .line 636
    :cond_0
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getGroupId()I

    move-result v3

    const v4, 0x7f0e04ce

    if-eq v3, v4, :cond_1

    .line 637
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getName()Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTitle:Ljava/lang/CharSequence;

    .line 639
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    iget-boolean v5, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsPickMode:Z

    invoke-virtual {v2, v3, v4, v1, v5}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->selectItem(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/StateManager;IZ)V

    .line 640
    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->notifyDataSetChanged()V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 646
    .end local v1    # "index":I
    .end local v2    # "item":Lcom/sec/samsung/gallery/drawer/DrawerItem;
    :goto_0
    return-void

    .line 641
    :catch_0
    move-exception v0

    .line 642
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    .line 643
    sget-object v3, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "selectItem() : ignore selected item, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " drawer adapter size is changed"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setSelectAllFocused(Z)V
    .locals 4
    .param p1, "focus"    # Z

    .prologue
    .line 525
    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v2

    .line 526
    .local v2, "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v3, v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-eqz v3, :cond_0

    .line 527
    check-cast v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .end local v2    # "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->getActionBarManager()Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v0

    .line 528
    .local v0, "actionBarView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    instance-of v3, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    if-eqz v3, :cond_0

    .line 529
    check-cast v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    .end local v0    # "actionBarView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->getSelectionModeBar()Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    move-result-object v1

    .line 530
    .local v1, "mSelectionModeBar":Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    if-eqz v1, :cond_0

    .line 531
    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->setFocused(Z)V

    .line 534
    .end local v1    # "mSelectionModeBar":Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    :cond_0
    return-void
.end method


# virtual methods
.method public checkGifMode()Z
    .locals 1

    .prologue
    .line 817
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsGifMode:Z

    return v0
.end method

.method public closeDrawerMenu()V
    .locals 2

    .prologue
    .line 795
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mLinearlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 796
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)V
    .locals 7
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 712
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mEnableflag:Z

    if-eqz v3, :cond_0

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePostInflateNavigationDrawer:Z

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsNeedtoPostInflate:Z

    if-eqz v3, :cond_1

    .line 739
    :cond_0
    :goto_0
    return-void

    .line 715
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getActionBarSize(Landroid/content/Context;)I

    move-result v0

    .line 716
    .local v0, "actionbarHeight":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 717
    .local v1, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 719
    .local v2, "y":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerList:Landroid/widget/ListView;

    if-eqz v3, :cond_0

    .line 720
    int-to-float v3, v0

    cmpg-float v3, v3, v2

    if-gez v3, :cond_0

    .line 721
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_2

    iget v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTouchArea:I

    int-to-float v3, v3

    cmpg-float v3, v1, v3

    if-gez v3, :cond_2

    .line 722
    iput v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTouchDown:F

    .line 724
    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerList:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 725
    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 726
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTouched:Z

    goto :goto_0

    .line 727
    :cond_2
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTouched:Z

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isDrawerVisible()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 728
    sget-object v3, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UP********"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTouchDown:F

    sub-float/2addr v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 729
    iget v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTouchDown:F

    sub-float/2addr v3, v1

    const/high16 v4, 0x40a00000    # 5.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    .line 730
    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerList:Landroid/widget/ListView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 731
    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 732
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTouched:Z

    goto :goto_0
.end method

.method public exitSelectionMode()V
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$6;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$6;-><init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 232
    return-void
.end method

.method public exitSelectionMode(I)V
    .locals 2
    .param p1, "title"    # I

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$7;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$7;-><init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 244
    return-void
.end method

.method public getDrawerEnable()Z
    .locals 1

    .prologue
    .line 207
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mEnableflag:Z

    return v0
.end method

.method public getPostInflateStatus()Z
    .locals 1

    .prologue
    .line 809
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsNeedtoPostInflate:Z

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 670
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTitle:Ljava/lang/CharSequence;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public initDrawer()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 399
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 421
    :goto_0
    return-void

    .line 401
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 402
    .local v1, "res":Landroid/content/res/Resources;
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 404
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030013

    invoke-virtual {v0, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;

    .line 405
    const v2, 0x7f0d0303

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTouchArea:I

    .line 406
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;

    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v4, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 409
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 410
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0f0029

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mLinearlayout:Landroid/widget/LinearLayout;

    .line 411
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0f0027

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v4/widget/DrawerLayout;

    iput-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    .line 412
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0f002a

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerList:Landroid/widget/ListView;

    .line 413
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v2, :cond_1

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOverlayHelp:Z

    if-eqz v2, :cond_1

    .line 414
    new-instance v2, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerHelp:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    .line 415
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->initDrawerToggle()V

    .line 416
    new-instance v2, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    .line 417
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-virtual {v2, v3}, Landroid/support/v4/widget/DrawerLayout;->setDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 418
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerList:Landroid/widget/ListView;

    new-instance v3, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerItemClickListener;

    invoke-direct {v3, p0, v5}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerItemClickListener;-><init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 419
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerList:Landroid/widget/ListView;

    new-instance v3, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerListOnKeyListener;

    invoke-direct {v3, p0, v5}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerListOnKeyListener;-><init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 420
    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->checkPickermode()V

    goto/16 :goto_0
.end method

.method public isDrawerOpen()Z
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-nez v0, :cond_0

    .line 248
    const/4 v0, 0x0

    .line 250
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mLinearlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(Landroid/view/View;)Z

    move-result v0

    goto :goto_0
.end method

.method public isDrawerVisible()Z
    .locals 2

    .prologue
    .line 254
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-nez v0, :cond_0

    .line 255
    const/4 v0, 0x0

    .line 257
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mLinearlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->isDrawerVisible(Landroid/view/View;)Z

    move-result v0

    goto :goto_0
.end method

.method public isSelectionMode(Ljava/lang/String;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$5;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$5;-><init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 221
    return-void
.end method

.method public isVisibleState()Z
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 262
    const/4 v0, 0x1

    .line 264
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 349
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mEnableflag:Z

    if-nez v0, :cond_1

    .line 357
    :cond_0
    :goto_0
    return-void

    .line 351
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    if-eqz v0, :cond_2

    .line 352
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/ActionBarDrawerToggle;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 353
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->hasFilterBy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerHelp:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOverlayHelp:Z

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerHelp:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->resetLayout()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 361
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 342
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->onDestroy()V

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "EVENT_NAVIGATION_DRAWER"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 346
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const v4, 0x102002c

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 364
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePostInflateNavigationDrawer:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsNeedtoPostInflate:Z

    if-nez v2, :cond_4

    .line 365
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;

    if-eqz v2, :cond_2

    .line 366
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    if-ne v2, v4, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mEnableflag:Z

    if-eqz v2, :cond_2

    .line 367
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerList:Landroid/widget/ListView;

    if-eqz v2, :cond_1

    .line 368
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerList:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 370
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 373
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    if-ne v2, v4, :cond_3

    .line 374
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    iget-object v2, v2, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupFilterBy:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->updateFilterByGroup(Z)V

    .line 375
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mToggle:Z

    .line 377
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    if-eqz v2, :cond_4

    .line 378
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-virtual {v2, p1}, Landroid/support/v4/app/ActionBarDrawerToggle;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 383
    :goto_0
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 325
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePostInflateNavigationDrawer:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsNeedtoPostInflate:Z

    if-nez v0, :cond_3

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mLinearlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 327
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerList:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 328
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerList:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 330
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 331
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 335
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    if-eqz v0, :cond_3

    .line 336
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->onPause()V

    .line 339
    :cond_3
    return-void
.end method

.method public onPostCreate()V
    .locals 1

    .prologue
    .line 706
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    if-eqz v0, :cond_0

    .line 707
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-virtual {v0}, Landroid/support/v4/app/ActionBarDrawerToggle;->syncState()V

    .line 709
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x0

    .line 683
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mEnableflag:Z

    if-nez v0, :cond_1

    .line 703
    :cond_0
    :goto_0
    return-void

    .line 685
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mSlideOffset:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 686
    const v0, 0x7f0f028b

    invoke-interface {p1, v0, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 687
    const v0, 0x7f0f02bd

    invoke-interface {p1, v0, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 688
    const v0, 0x7f0f02d1

    invoke-interface {p1, v0, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 690
    const v0, 0x7f0f0286

    invoke-interface {p1, v0, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 691
    const v0, 0x7f0f02cf

    invoke-interface {p1, v0, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 692
    const v0, 0x7f0f02da

    invoke-interface {p1, v0, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 694
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$14;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$14;-><init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 700
    :cond_2
    iget v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mSlideOffset:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 701
    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->refreshTitle()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 317
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePostInflateNavigationDrawer:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsNeedtoPostInflate:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsOnlyAlbumModePick:Z

    if-nez v0, :cond_1

    .line 318
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->initDrawer()V

    .line 319
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onPostCreate()V

    .line 320
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->onResume()V

    .line 322
    :cond_1
    return-void
.end method

.method public openDrawerMenu()V
    .locals 2

    .prologue
    .line 790
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mLinearlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->openDrawer(Landroid/view/View;)V

    .line 791
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 792
    return-void
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$8;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$8;-><init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 275
    return-void
.end method

.method public requestFocus()V
    .locals 1

    .prologue
    .line 799
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerList:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 800
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    .line 801
    :cond_0
    return-void
.end method

.method public resume()V
    .locals 2

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$9;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$9;-><init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 291
    return-void
.end method

.method public resume(I)V
    .locals 2
    .param p1, "title"    # I

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$10;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$10;-><init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 308
    return-void
.end method

.method public resume(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 311
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 312
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setTitle(Ljava/lang/String;)V

    .line 313
    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->adjustLayout()V

    .line 314
    return-void
.end method

.method public setDrawerEnable(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 203
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mEnableflag:Z

    .line 204
    return-void
.end method

.method public setDrawerIcon()V
    .locals 2

    .prologue
    .line 858
    const/4 v0, -0x1

    .line 859
    .local v0, "drawerIcon":I
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v1, :cond_0

    .line 860
    const v0, 0x7f020500

    .line 864
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setHomeAsUpIndicator(I)V

    .line 865
    return-void

    .line 862
    :cond_0
    const v0, 0x7f0204ff

    goto :goto_0
.end method

.method public setDrawerIconWithAlpha(I)V
    .locals 3
    .param p1, "alphaValue"    # I

    .prologue
    .line 846
    const/4 v0, -0x1

    .line 847
    .local v0, "drawerIcon":I
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v2, :cond_0

    .line 848
    const v0, 0x7f020500

    .line 852
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 853
    .local v1, "naviDrawer":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 854
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/ActionBar;->setHomeAsUpIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 855
    return-void

    .line 850
    .end local v1    # "naviDrawer":Landroid/graphics/drawable/Drawable;
    :cond_0
    const v0, 0x7f0204ff

    goto :goto_0
.end method

.method public setGifMode(Z)V
    .locals 0
    .param p1, "isGif"    # Z

    .prologue
    .line 813
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsGifMode:Z

    .line 814
    return-void
.end method

.method public setPostInflateStatus(Z)V
    .locals 0
    .param p1, "bStatus"    # Z

    .prologue
    .line 804
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsNeedtoPostInflate:Z

    .line 805
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onResume()V

    .line 806
    return-void
.end method

.method public setStatusProxy(Lcom/sec/android/gallery3d/app/StateManager;)V
    .locals 0
    .param p1, "statusProxy"    # Lcom/sec/android/gallery3d/app/StateManager;

    .prologue
    .line 786
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 787
    return-void
.end method

.method public setTitle()V
    .locals 4

    .prologue
    .line 649
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    .line 650
    .local v1, "type":Lcom/sec/samsung/gallery/core/TabTagType;
    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 651
    .local v0, "title":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 652
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setTitle(Ljava/lang/String;)V

    .line 654
    :cond_0
    return-void
.end method

.method public setTitle(I)V
    .locals 1
    .param p1, "title"    # I

    .prologue
    .line 657
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setTitle(Ljava/lang/String;)V

    .line 658
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 661
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->isFestivalMode()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 662
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e002b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTitle:Ljava/lang/CharSequence;

    .line 666
    :goto_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->refreshTitle()V

    .line 667
    return-void

    .line 664
    :cond_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTitle:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public updateDrawerMode(Z)V
    .locals 2
    .param p1, "mode"    # Z

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    if-nez v0, :cond_1

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->isFestivalMode()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 177
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$3;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$3;-><init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 188
    :cond_2
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setDrawerEnable(Z)V

    .line 189
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePostInflateNavigationDrawer:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsNeedtoPostInflate:Z

    if-nez v0, :cond_0

    .line 190
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$4;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$4;-><init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;Z)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
