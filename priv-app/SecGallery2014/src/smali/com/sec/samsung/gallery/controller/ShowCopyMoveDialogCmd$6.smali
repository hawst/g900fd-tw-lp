.class Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$6;
.super Ljava/lang/Object;
.source "ShowCopyMoveDialogCmd.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->showLocalCloudCopyMoveChioceDialog(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

.field final synthetic val$dstAlbumPath:Ljava/lang/String;

.field final synthetic val$fileTo:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 632
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$6;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    iput p2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$6;->val$fileTo:I

    iput-object p3, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$6;->val$dstAlbumPath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 5
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    .line 635
    move-object v0, p2

    check-cast v0, Lcom/sec/samsung/gallery/core/Event;

    .line 636
    .local v0, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_FILES:I

    if-ne v1, v2, :cond_2

    .line 638
    iget v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$6;->val$fileTo:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 639
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$6;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$6;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # invokes: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->getSrcMediaLists()Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$1000(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Ljava/util/LinkedList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$6;->val$dstAlbumPath:Ljava/lang/String;

    # invokes: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->doUpload(Ljava/util/LinkedList;Ljava/lang/String;)V
    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$1100(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Ljava/util/LinkedList;Ljava/lang/String;)V

    .line 640
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$6;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0306

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 652
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$6;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "EXIT_SELECTION_MODE"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 654
    return-void

    .line 642
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$6;->val$fileTo:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 643
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$6;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$6;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # invokes: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->getSrcMediaLists()Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$1000(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Ljava/util/LinkedList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$6;->val$dstAlbumPath:Ljava/lang/String;

    # invokes: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->doDownload(Ljava/util/LinkedList;Ljava/lang/String;)V
    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$1200(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Ljava/util/LinkedList;Ljava/lang/String;)V

    .line 644
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$6;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0305

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 647
    :cond_2
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_MOVE_FILES:I

    if-ne v1, v2, :cond_0

    .line 649
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$6;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0107

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0
.end method
