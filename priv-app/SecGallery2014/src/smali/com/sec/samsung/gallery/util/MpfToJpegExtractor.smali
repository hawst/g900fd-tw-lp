.class public Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;
.super Ljava/lang/Object;
.source "MpfToJpegExtractor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$1;,
        Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;
    }
.end annotation


# static fields
.field private static final FULL_HD_HEIGHT:I = 0x438

.field private static final FULL_HD_WIDTH:I = 0x780

.field private static final INVALID_POS:I = -0x1

.field private static final MAXOUTPUTCOUNT:I = 0x4

.field private static final MAXSUPPORTMPFSIZE:I = 0xa00000

.field private static final MAX_DISP_SIZE:I = 0x3c0


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mAccessArray:[B

.field private mBitmapLeft:Landroid/graphics/Bitmap;

.field private mBitmapRight:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field private mExtBuffer:[Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

.field private mExtBufferCur:I

.field private mIdxLeft:I

.field private mIdxRight:I

.field private mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mStopReq:Z

.field private mStreamSize:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-string v0, "MpfToJpegExtractor"

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->TAG:Ljava/lang/String;

    .line 30
    iput-object v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mAccessArray:[B

    .line 34
    iput v2, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mIdxLeft:I

    .line 35
    iput v2, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mIdxRight:I

    .line 36
    iput-object v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mBitmapLeft:Landroid/graphics/Bitmap;

    .line 37
    iput-object v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mBitmapRight:Landroid/graphics/Bitmap;

    .line 41
    iput-object p1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mContext:Landroid/content/Context;

    .line 42
    iput-object v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 43
    return-void
.end method

.method private Extract()V
    .locals 8

    .prologue
    const/4 v7, -0x1

    .line 142
    iget-object v4, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBuffer:[Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    iget v5, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBufferCur:I

    aget-object v0, v4, v5

    .line 143
    .local v0, "curExtBuff":Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;
    iget-object v2, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mAccessArray:[B

    .line 144
    .local v2, "input":[B
    const/4 v1, 0x0

    .line 145
    .local v1, "idx":I
    const/4 v3, -0x1

    .line 147
    .local v3, "last":I
    :goto_0
    iget v4, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mStreamSize:I

    add-int/lit8 v4, v4, -0x4

    if-ge v1, v4, :cond_7

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mStopReq:Z

    if-nez v4, :cond_7

    .line 148
    aget-byte v4, v2, v1

    if-ne v4, v7, :cond_6

    .line 149
    add-int/lit8 v4, v1, 0x1

    aget-byte v4, v2, v4

    const/16 v5, -0x28

    if-ne v4, v5, :cond_5

    .line 150
    add-int/lit8 v4, v1, 0x2

    aget-byte v4, v2, v4

    if-ne v4, v7, :cond_4

    .line 151
    add-int/lit8 v4, v1, 0x3

    aget-byte v4, v2, v4

    const/16 v5, -0x1f

    if-ne v4, v5, :cond_3

    .line 152
    const-string v4, "MpfToJpegExtractor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "View found at offset = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    if-eq v3, v7, :cond_2

    .line 154
    sub-int v4, v1, v3

    # invokes: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->write(II)Z
    invoke-static {v0, v3, v4}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$200(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;II)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 155
    const-string v4, "MpfToJpegExtractor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Create View = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBufferCur:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", size = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-int v6, v1, v3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    iget v4, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBufferCur:I

    add-int/lit8 v4, v4, 0x1

    const/4 v5, 0x4

    if-le v4, v5, :cond_1

    .line 157
    const-string v4, "MpfToJpegExtractor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Reached max buffer = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", size = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mStreamSize:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    :cond_0
    :goto_1
    return-void

    .line 160
    :cond_1
    iget v4, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBufferCur:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBufferCur:I

    .line 161
    iget-object v4, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBuffer:[Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    iget v5, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBufferCur:I

    aget-object v0, v4, v5

    .line 164
    :cond_2
    move v3, v1

    .line 165
    add-int/lit8 v1, v1, 0x4

    goto/16 :goto_0

    .line 167
    :cond_3
    add-int/lit8 v1, v1, 0x2

    goto/16 :goto_0

    .line 170
    :cond_4
    add-int/lit8 v1, v1, 0x3

    goto/16 :goto_0

    .line 173
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 176
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 179
    :cond_7
    iget v4, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mStreamSize:I

    if-le v4, v3, :cond_0

    .line 181
    if-ne v3, v7, :cond_8

    const/4 v3, 0x0

    .line 182
    :cond_8
    iget v4, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mStreamSize:I

    sub-int/2addr v4, v3

    # invokes: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->write(II)Z
    invoke-static {v0, v3, v4}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$200(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;II)Z

    .line 183
    iget v4, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBufferCur:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBufferCur:I

    .line 184
    const-string v4, "MpfToJpegExtractor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Create View last = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBufferCur:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", size = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mStreamSize:I

    sub-int/2addr v6, v3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;)[B
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mAccessArray:[B

    return-object v0
.end method

.method private findScreenails()V
    .locals 8

    .prologue
    .line 208
    const/4 v2, -0x1

    .line 209
    .local v2, "idx1":I
    const/4 v3, -0x1

    .line 210
    .local v3, "idx2":I
    const/4 v4, 0x0

    .line 213
    .local v4, "maxFactor":F
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mStopReq:Z

    if-eqz v5, :cond_0

    .line 232
    :goto_0
    return-void

    .line 215
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v5, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBufferCur:I

    if-ge v1, v5, :cond_5

    .line 216
    iget-object v5, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBuffer:[Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    aget-object v5, v5, v1

    # getter for: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mValid:Z
    invoke-static {v5}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$300(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 215
    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 218
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBuffer:[Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    aget-object v5, v5, v1

    # getter for: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mFactor:F
    invoke-static {v5}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$600(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)F

    move-result v0

    .line 219
    .local v0, "factor":F
    const/4 v5, 0x0

    cmpl-float v5, v4, v5

    if-nez v5, :cond_3

    .line 220
    move v4, v0

    .line 221
    move v2, v1

    goto :goto_2

    .line 222
    :cond_3
    invoke-static {v4, v0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->isAlmostEquals(FF)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 223
    move v3, v1

    goto :goto_2

    .line 224
    :cond_4
    cmpg-float v5, v4, v0

    if-gez v5, :cond_1

    .line 225
    move v4, v0

    .line 226
    move v2, v1

    goto :goto_2

    .line 229
    .end local v0    # "factor":F
    :cond_5
    iput v2, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mIdxLeft:I

    .line 230
    iput v3, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mIdxRight:I

    .line 231
    const-string v5, "MpfToJpegExtractor"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "findScreenails left = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", right = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private initExtBuffer()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x4

    .line 128
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBuffer:[Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    if-nez v1, :cond_0

    .line 129
    new-array v1, v3, [Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    iput-object v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBuffer:[Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    .line 130
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBuffer:[Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    invoke-static {v1, v4}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 132
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_2

    .line 133
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBuffer:[Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    aget-object v1, v1, v0

    if-nez v1, :cond_1

    .line 134
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBuffer:[Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    new-instance v2, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    invoke-direct {v2, p0, v4}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;-><init>(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$1;)V

    aput-object v2, v1, v0

    .line 136
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBuffer:[Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    aget-object v1, v1, v0

    # invokes: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->reset()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$100(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)V

    .line 132
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 138
    :cond_2
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBufferCur:I

    .line 139
    return-void
.end method

.method protected static isAlmostEquals(FF)Z
    .locals 2
    .param p0, "a"    # F
    .param p1, "b"    # F

    .prologue
    .line 260
    sub-float v0, p0, p1

    .line 261
    .local v0, "diff":F
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    neg-float v0, v0

    .end local v0    # "diff":F
    :cond_0
    const v1, 0x3dcccccd    # 0.1f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private makeScreenails()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, -0x1

    const/high16 v4, 0x44700000    # 960.0f

    .line 235
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mStopReq:Z

    if-eqz v2, :cond_1

    .line 257
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    iget v2, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mIdxLeft:I

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mIdxRight:I

    if-eq v2, v3, :cond_0

    .line 240
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 241
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 242
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->isPreview:Z

    .line 246
    iget-object v2, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBuffer:[Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    iget v3, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mIdxLeft:I

    aget-object v0, v2, v3

    .line 247
    .local v0, "curExtBuf":Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;
    # getter for: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mWidth:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$400(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)I

    move-result v2

    # getter for: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mHeight:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$500(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    int-to-float v2, v2

    div-float v2, v4, v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/common/BitmapUtils;->computeSampleSize(F)I

    move-result v2

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 249
    # getter for: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mOutBuf:[B
    invoke-static {v0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$700(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)[B

    move-result-object v2

    # getter for: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mWrite:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$800(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)I

    move-result v3

    invoke-static {v2, v5, v3, v1}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mBitmapLeft:Landroid/graphics/Bitmap;

    .line 252
    iget-object v2, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBuffer:[Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    iget v3, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mIdxRight:I

    aget-object v0, v2, v3

    .line 253
    # getter for: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mWidth:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$400(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)I

    move-result v2

    # getter for: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mHeight:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$500(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    int-to-float v2, v2

    div-float v2, v4, v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/common/BitmapUtils;->computeSampleSize(F)I

    move-result v2

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 255
    # getter for: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mOutBuf:[B
    invoke-static {v0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$700(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)[B

    move-result-object v2

    # getter for: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mWrite:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$800(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)I

    move-result v3

    invoke-static {v2, v5, v3, v1}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mBitmapRight:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private openFile()Z
    .locals 5

    .prologue
    .line 103
    const/4 v2, 0x0

    .line 104
    .local v2, "uriInputStream":Ljava/io/InputStream;
    const/4 v1, 0x0

    .line 107
    .local v1, "success":Z
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    .line 109
    if-nez v2, :cond_0

    .line 110
    const-string v3, "MpfToJpegExtractor"

    const-string v4, "InputStream is null!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    const/4 v3, 0x0

    .line 122
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 124
    :goto_0
    return v3

    .line 113
    :cond_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->available()I

    move-result v0

    .line 114
    .local v0, "streamSize":I
    const/high16 v3, 0xa00000

    if-le v0, v3, :cond_1

    .line 115
    const/high16 v0, 0xa00000

    .line 118
    :cond_1
    iput v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mStreamSize:I

    .line 119
    invoke-direct {p0, v2, v0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->streamToBytes(Ljava/io/InputStream;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 122
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .end local v0    # "streamSize":I
    :goto_1
    move v3, v1

    .line 124
    goto :goto_0

    .line 120
    :catch_0
    move-exception v3

    .line 122
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_0
    move-exception v3

    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v3
.end method

.method private prepareAttribute()V
    .locals 6

    .prologue
    .line 191
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 193
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mStopReq:Z

    if-eqz v3, :cond_1

    .line 205
    :cond_0
    return-void

    .line 195
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v3, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBufferCur:I

    if-ge v1, v3, :cond_0

    .line 196
    iget-object v3, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mExtBuffer:[Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    aget-object v0, v3, v1

    .line 197
    .local v0, "extBuf":Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;
    # getter for: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mValid:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$300(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 195
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 200
    :cond_2
    invoke-direct {p0, v2, v0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->setExtractBuffer(Landroid/graphics/BitmapFactory$Options;Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)V

    .line 201
    const-string v3, "MpfToJpegExtractor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "prepareAttribute index = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " width = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mWidth:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$400(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", height = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mHeight:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$500(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", near = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mFactor:F
    invoke-static {v0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$600(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private setExtractBuffer(Landroid/graphics/BitmapFactory$Options;Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)V
    .locals 3
    .param p1, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p2, "extract"    # Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    .prologue
    .line 265
    const/4 v0, 0x1

    iput-boolean v0, p1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 266
    # getter for: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mOutBuf:[B
    invoke-static {p2}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$700(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)[B

    move-result-object v0

    const/4 v1, 0x0

    # getter for: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mWrite:I
    invoke-static {p2}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$800(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)I

    move-result v2

    invoke-static {v0, v1, v2, p1}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 268
    iget v0, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    # setter for: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mWidth:I
    invoke-static {p2, v0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$402(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;I)I

    .line 269
    iget v0, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    # setter for: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mHeight:I
    invoke-static {p2, v0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$502(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;I)I

    .line 270
    # invokes: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->determineFactor()V
    invoke-static {p2}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->access$900(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)V

    .line 271
    return-void
.end method

.method private streamToBytes(Ljava/io/InputStream;I)Z
    .locals 7
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "streamSize"    # I

    .prologue
    const/4 v3, 0x0

    .line 83
    iget-object v4, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mAccessArray:[B

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mAccessArray:[B

    array-length v4, v4

    if-le p2, v4, :cond_1

    .line 84
    :cond_0
    new-array v4, p2, [B

    iput-object v4, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mAccessArray:[B

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mAccessArray:[B

    .line 87
    .local v0, "buffer":[B
    const/4 v2, -0x1

    .line 89
    .local v2, "readsize":I
    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p1, v0, v4, p2}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 94
    :goto_0
    if-eq v2, p2, :cond_2

    .line 95
    const-string v4, "MpfToJpegExtractor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "streamToBytes size mismatch read = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", streamSize = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    :goto_1
    return v3

    .line 91
    :catch_0
    move-exception v1

    .line 92
    .local v1, "e1":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 99
    .end local v1    # "e1":Ljava/io/IOException;
    :cond_2
    const/4 v3, 0x1

    goto :goto_1
.end method


# virtual methods
.method public getBitmap(Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "isLeft"    # Z

    .prologue
    .line 76
    if-eqz p1, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mBitmapLeft:Landroid/graphics/Bitmap;

    .line 79
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mBitmapRight:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public isAvailable()Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mBitmapLeft:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mBitmapRight:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 71
    const/4 v0, 0x1

    .line 72
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMediaItem(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 3
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-ne v0, p1, :cond_1

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 49
    :cond_1
    iput-object p1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 50
    iput v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mIdxLeft:I

    .line 51
    iput v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mIdxRight:I

    .line 52
    iput-object v2, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mBitmapLeft:Landroid/graphics/Bitmap;

    .line 53
    iput-object v2, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mBitmapRight:Landroid/graphics/Bitmap;

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mStopReq:Z

    .line 55
    invoke-direct {p0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->openFile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    invoke-direct {p0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->initExtBuffer()V

    .line 59
    invoke-direct {p0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->Extract()V

    .line 60
    invoke-direct {p0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->prepareAttribute()V

    .line 61
    invoke-direct {p0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->findScreenails()V

    .line 62
    invoke-direct {p0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->makeScreenails()V

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mStopReq:Z

    .line 67
    return-void
.end method
