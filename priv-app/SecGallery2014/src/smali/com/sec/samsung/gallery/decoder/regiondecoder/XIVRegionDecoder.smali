.class public Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
.super Ljava/lang/Object;
.source "XIVRegionDecoder.java"


# instance fields
.field private mRegionDecoderInstance:Ljava/lang/Object;

.field private mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cancelAllNewInstances()V
    .locals 0

    .prologue
    .line 102
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->cancelAllNewInstances()V

    .line 103
    return-void
.end method

.method public static getNativeInstance(Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;)Ljava/lang/Object;
    .locals 1
    .param p0, "regionDecoder"    # Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mRegionDecoderInstance:Ljava/lang/Object;

    return-object v0
.end method

.method public static isUseRDEx()Z
    .locals 1

    .prologue
    .line 39
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->isUseRDEx()Z

    move-result v0

    return v0
.end method

.method public static newInstance(Ljava/io/FileDescriptor;ZLcom/sec/samsung/gallery/decoder/XIVInterface;)Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    .locals 2
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "isShareable"    # Z
    .param p2, "xivInterface"    # Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .prologue
    .line 71
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;-><init>()V

    .line 72
    .local v0, "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    iput-object p2, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .line 73
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->newInstance(Ljava/io/FileDescriptor;Z)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mRegionDecoderInstance:Ljava/lang/Object;

    .line 75
    iget-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mRegionDecoderInstance:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v1, :cond_0

    .line 78
    .end local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    :goto_0
    return-object v0

    .restart local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance(Ljava/io/InputStream;ZLcom/sec/samsung/gallery/decoder/XIVInterface;)Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    .locals 2
    .param p0, "is"    # Ljava/io/InputStream;
    .param p1, "isShareable"    # Z
    .param p2, "xivInterface"    # Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .prologue
    .line 59
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;-><init>()V

    .line 60
    .local v0, "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    iput-object p2, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .line 61
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->newInstance(Ljava/io/InputStream;Z)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mRegionDecoderInstance:Ljava/lang/Object;

    .line 63
    iget-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mRegionDecoderInstance:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v1, :cond_0

    .line 66
    .end local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    :goto_0
    return-object v0

    .restart local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/Object;Lcom/sec/samsung/gallery/decoder/XIVInterface;)Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    .locals 1
    .param p0, "qrd"    # Ljava/lang/Object;
    .param p1, "xivInterface"    # Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .prologue
    .line 95
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;-><init>()V

    .line 96
    .local v0, "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    iput-object p1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .line 97
    iput-object p0, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mRegionDecoderInstance:Ljava/lang/Object;

    .line 98
    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;ZLcom/sec/samsung/gallery/decoder/XIVInterface;)Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    .locals 2
    .param p0, "pathName"    # Ljava/lang/String;
    .param p1, "isShareable"    # Z
    .param p2, "xivInterface"    # Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .prologue
    .line 47
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;-><init>()V

    .line 48
    .local v0, "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    iput-object p2, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .line 49
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->newInstance(Ljava/lang/String;Z)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mRegionDecoderInstance:Ljava/lang/Object;

    .line 51
    iget-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mRegionDecoderInstance:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v1, :cond_0

    .line 54
    .end local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    :goto_0
    return-object v0

    .restart local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance([BIIZLcom/sec/samsung/gallery/decoder/XIVInterface;)Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    .locals 2
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "isShareable"    # Z
    .param p4, "xivInterface"    # Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .prologue
    .line 83
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;-><init>()V

    .line 84
    .local v0, "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    iput-object p4, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .line 85
    invoke-static {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->newInstance([BIIZ)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mRegionDecoderInstance:Ljava/lang/Object;

    .line 87
    iget-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mRegionDecoderInstance:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v1, :cond_0

    .line 90
    .end local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    :goto_0
    return-object v0

    .restart local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "rect"    # Landroid/graphics/Rect;
    .param p2, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mRegionDecoderInstance:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1, p2}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->decodeRegion(Ljava/lang/Object;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public decodeRegionEx(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "region"    # Landroid/graphics/Rect;
    .param p2, "intersectRect"    # Landroid/graphics/Rect;
    .param p3, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mRegionDecoderInstance:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->decodeRegionEx(Ljava/lang/Object;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getHeight()I
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mRegionDecoderInstance:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->getHeight(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getWidth()I
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mRegionDecoderInstance:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->getWidth(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final isRecycled()Z
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mRegionDecoderInstance:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->isRecycled(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public recycle()V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->mRegionDecoderInstance:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->recycle(Ljava/lang/Object;)V

    .line 124
    return-void
.end method
