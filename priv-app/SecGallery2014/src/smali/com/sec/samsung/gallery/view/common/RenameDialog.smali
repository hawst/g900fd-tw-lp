.class public Lcom/sec/samsung/gallery/view/common/RenameDialog;
.super Lcom/sec/samsung/gallery/view/ViewObservable;
.source "RenameDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;
    }
.end annotation


# static fields
.field private static final HIDE_SOFT_INPUT:I = 0x0

.field public static final INVALID_CHAR:[Ljava/lang/String;

.field private static final IS_VISIBLE_WINDOW:Ljava/lang/String; = "AxT9IME.isVisibleWindow"

.field private static final RESPONSE_AXT9INFO:Ljava/lang/String; = "ResponseAxT9Info"


# instance fields
.field private alertEditText:Landroid/widget/EditText;

.field private isSipVisible:Z

.field private final mCtx:Landroid/content/Context;

.field private mDialogFragment:Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

.field private mHandler:Landroid/os/Handler;

.field private mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

.field private mReceiveTime:J

.field private mSipIntentFilter:Landroid/content/IntentFilter;

.field private mSipReceiver:Landroid/content/BroadcastReceiver;

.field private final mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 69
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "\\"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "/"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ":"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "*"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "?"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "<"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ">"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "|"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\n"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->INVALID_CHAR:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 87
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/ViewObservable;-><init>()V

    .line 74
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mHandler:Landroid/os/Handler;

    .line 79
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mReceiveTime:J

    .line 85
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;

    .line 88
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mCtx:Landroid/content/Context;

    .line 89
    new-instance v0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mCtx:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;-><init>(Lcom/sec/samsung/gallery/view/common/RenameDialog;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    .line 90
    new-instance v0, Lcom/sec/samsung/gallery/view/common/RenameDialog$1;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mCtx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/common/RenameDialog$1;-><init>(Lcom/sec/samsung/gallery/view/common/RenameDialog;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mHandler:Landroid/os/Handler;

    .line 103
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mCtx:Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mCtx:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 105
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/common/RenameDialog;Landroid/os/IBinder;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RenameDialog;
    .param p1, "x1"    # Landroid/os/IBinder;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->hideSoftInput(Landroid/os/IBinder;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RenameDialog;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RenameDialog;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->isSipVisible:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/samsung/gallery/view/common/RenameDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RenameDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->isSipVisible:Z

    return p1
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/view/common/RenameDialog;Landroid/widget/EditText;)Landroid/widget/EditText;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RenameDialog;
    .param p1, "x1"    # Landroid/widget/EditText;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/common/RenameDialog;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RenameDialog;

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mReceiveTime:J

    return-wide v0
.end method

.method static synthetic access$1202(Lcom/sec/samsung/gallery/view/common/RenameDialog;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RenameDialog;
    .param p1, "x1"    # J

    .prologue
    .line 66
    iput-wide p1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mReceiveTime:J

    return-wide p1
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RenameDialog;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/samsung/gallery/view/common/RenameDialog;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RenameDialog;
    .param p1, "x1"    # Landroid/content/BroadcastReceiver;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RenameDialog;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RenameDialog;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/common/RenameDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RenameDialog;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->setSipBroadcastReceiver()V

    return-void
.end method

.method private hideSoftInput(Landroid/os/IBinder;)V
    .locals 4
    .param p1, "windowToken"    # Landroid/os/IBinder;

    .prologue
    .line 580
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mCtx:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 582
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mCtx:Landroid/content/Context;

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 584
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 588
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :goto_0
    return-void

    .line 585
    :catch_0
    move-exception v0

    .line 586
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method private setSipBroadcastReceiver()V
    .locals 3

    .prologue
    .line 527
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mSipIntentFilter:Landroid/content/IntentFilter;

    if-nez v0, :cond_0

    .line 528
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mSipIntentFilter:Landroid/content/IntentFilter;

    .line 529
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mSipIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "ResponseAxT9Info"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 531
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_1

    .line 532
    new-instance v0, Lcom/sec/samsung/gallery/view/common/RenameDialog$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/common/RenameDialog$2;-><init>(Lcom/sec/samsung/gallery/view/common/RenameDialog;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;

    .line 540
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mCtx:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mSipIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 541
    return-void
.end method


# virtual methods
.method public dismissMediaRenameDialog()V
    .locals 4

    .prologue
    .line 563
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 564
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 565
    .local v0, "dlg":Landroid/app/Dialog;
    if-eqz v0, :cond_1

    .line 566
    const v3, 0x7f0f0069

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 567
    .local v1, "edit":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 568
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 569
    .local v2, "msg":Landroid/os/Message;
    const/4 v3, 0x0

    iput v3, v2, Landroid/os/Message;->what:I

    .line 570
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    iput-object v3, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 571
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 573
    .end local v2    # "msg":Landroid/os/Message;
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->dismiss()V

    .line 576
    .end local v0    # "dlg":Landroid/app/Dialog;
    .end local v1    # "edit":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public setCurrentFilePath(Ljava/lang/String;)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 551
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->setCurrentFilePath(Ljava/lang/String;)V

    .line 552
    return-void
.end method

.method public setCurrentItem(Lcom/sec/android/gallery3d/data/MediaObject;)V
    .locals 1
    .param p1, "currentItem"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 559
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->setCurrentItem(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 560
    return-void
.end method

.method public setCurrentName(Ljava/lang/String;)V
    .locals 1
    .param p1, "currentName"    # Ljava/lang/String;

    .prologue
    .line 555
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->setCurrentName(Ljava/lang/String;)V

    .line 556
    return-void
.end method

.method public showMediaRenameDialog()V
    .locals 4

    .prologue
    .line 544
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog;->mCtx:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v3, "dialog"

    invoke-virtual {v2, v1, v3}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 548
    :goto_0
    return-void

    .line 545
    :catch_0
    move-exception v0

    .line 546
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method
