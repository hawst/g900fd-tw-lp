.class public Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
.super Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
.source "PositionControllerBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "GroupObject"
.end annotation


# instance fields
.field public mCoverObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

.field public mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field private mIndex:I

.field public mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;)V
    .locals 5
    .param p2, "compView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
    .param p3, "viewConfig"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1234
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .line 1235
    invoke-direct {p0, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    .line 1229
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mIndex:I

    .line 1230
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 1231
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 1232
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mCoverObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 1237
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setUseTouchEvent(Z)V

    .line 1238
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setEnableResizeAnim(Z)V

    .line 1239
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setEnableAlphaAnim(Z)V

    .line 1241
    const/high16 v0, 0x42c80000    # 100.0f

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setBorderWidth(F)V

    .line 1242
    const v0, -0xd0752b

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setBorderColor(I)V

    .line 1243
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setPostDraw(Z)V

    .line 1245
    iget-boolean v0, p3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mUseGroupTitle:Z

    if-eqz v0, :cond_0

    .line 1246
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-direct {v0, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 1247
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setUseTouchEvent(Z)V

    .line 1248
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setObjective(I)V

    .line 1249
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 1250
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {p2, v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setObjectListeners(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;I)V

    .line 1252
    :cond_0
    iget-boolean v0, p3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mUseGroupSelect:Z

    if-eqz v0, :cond_2

    .line 1253
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-direct {v0, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 1254
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_1

    .line 1255
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setObjective(I)V

    .line 1256
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 1257
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {p2, v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setObjectListeners(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;I)V

    .line 1259
    :cond_2
    return-void
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .prologue
    .line 1228
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mIndex:I

    return v0
.end method

.method static synthetic access$702(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    .param p1, "x1"    # I

    .prologue
    .line 1228
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mIndex:I

    return p1
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .prologue
    .line 1228
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->add(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    .param p1, "x1"    # Z

    .prologue
    .line 1228
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setSelected(Z)V

    return-void
.end method

.method private add(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V
    .locals 2
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .prologue
    .line 1262
    iget v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    const v1, 0xffff

    and-int/2addr v0, v1

    if-nez v0, :cond_0

    .line 1263
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mCoverObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 1264
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 1265
    return-void
.end method

.method private setSelected(Z)V
    .locals 2
    .param p1, "isSelected"    # Z

    .prologue
    .line 1272
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_0

    .line 1273
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mCanvChecked:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 1276
    :cond_0
    return-void

    .line 1273
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mCanvUnchecked:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    goto :goto_0
.end method


# virtual methods
.method public changeGroupIndex(I)V
    .locals 6
    .param p1, "newGroupIndex"    # I

    .prologue
    .line 1287
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getAllChildObject()[Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v0

    .line 1289
    .local v0, "allChild":[Lcom/sec/android/gallery3d/glcore/GlObject;
    const/4 v3, 0x0

    .line 1290
    .local v3, "newItemIndex":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v0

    if-ge v2, v4, :cond_0

    .line 1291
    aget-object v1, v0, v2

    check-cast v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 1292
    .local v1, "compObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    iget v4, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    const v5, 0xffff

    and-int/2addr v4, v5

    or-int v3, p1, v4

    .line 1293
    iput v3, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 1290
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1295
    .end local v1    # "compObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_0
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mIndex:I

    .line 1296
    return-void
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 1279
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mIndex:I

    return v0
.end method

.method protected remove(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 0
    .param p1, "obj"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 1268
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->removeChild(Lcom/sec/android/gallery3d/glcore/GlObject;)Z

    .line 1269
    return-void
.end method

.method public setIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 1283
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mIndex:I

    .line 1284
    return-void
.end method
