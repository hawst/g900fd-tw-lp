.class Lcom/sec/samsung/gallery/glview/GlPhotoView$18;
.super Ljava/lang/Object;
.source "GlPhotoView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mPressedObj:Lcom/sec/samsung/gallery/glview/GlSplitObject;

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 2162
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$18;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMove(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    .line 2179
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$18;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0, p2, p3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->onMoved(II)Z

    move-result v0

    return v0
.end method

.method public onPress(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 5
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 2166
    check-cast p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .end local p1    # "object":Lcom/sec/android/gallery3d/glcore/GlObject;
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getMainObject()Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$18;->mPressedObj:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .line 2167
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$18;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mLClickable:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$2602(Lcom/sec/samsung/gallery/glview/GlPhotoView;Z)Z

    .line 2168
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$18;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mNewAlbumMode:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$2300(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$18;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    int-to-float v2, p3

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$18;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$18;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeight:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    sub-float/2addr v0, v2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$18;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSeparatorY:F
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$2700(Lcom/sec/samsung/gallery/glview/GlPhotoView;)F

    move-result v2

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 2169
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$18;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPressedUp:Z
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$2802(Lcom/sec/samsung/gallery/glview/GlPhotoView;Z)Z

    .line 2170
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$18;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$18;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeight:I

    invoke-virtual {v0, v4, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->onPressed(II)Z

    move-result v0

    .line 2174
    :goto_0
    return v0

    .line 2172
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$18;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPressedUp:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$2802(Lcom/sec/samsung/gallery/glview/GlPhotoView;Z)Z

    .line 2173
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$18;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPressedLeft:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$2902(Lcom/sec/samsung/gallery/glview/GlPhotoView;Z)Z

    move v0, v1

    .line 2174
    goto :goto_0
.end method

.method public onRelease(Lcom/sec/android/gallery3d/glcore/GlObject;IIII)Z
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "dx"    # I
    .param p3, "dy"    # I
    .param p4, "speedX"    # I
    .param p5, "speedY"    # I

    .prologue
    .line 2183
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$18;->mPressedObj:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v0, :cond_0

    .line 2184
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$18;->mPressedObj:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->cancelClick()V

    .line 2185
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$18;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->onReleased(IIII)Z

    move-result v0

    return v0
.end method
