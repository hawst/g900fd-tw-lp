.class Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;
.super Landroid/os/Handler;
.source "SlideshowVideoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    .line 67
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 69
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$000(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getInstance()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setVisibility(I)V

    goto :goto_0

    .line 73
    :pswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mCustomSurfaceView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mCustomSurfaceView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mCustomSurfaceView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mCustomSurfaceView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/ViewManager;->removeView(Landroid/view/View;)V

    .line 76
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$000(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getInstance()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$000(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getInstance()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->playSlideshowMusic()V

    .line 78
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$000(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getInstance()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v0

    iput-boolean v2, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsMusicPaused:Z

    .line 80
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mTelephonyManager:Landroid/telephony/TelephonyManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$200(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mTelephonyManager:Landroid/telephony/TelephonyManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$200(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mTelephonyManager:Landroid/telephony/TelephonyManager;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$202(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;Landroid/telephony/TelephonyManager;)Landroid/telephony/TelephonyManager;

    goto :goto_0

    .line 86
    :pswitch_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    goto/16 :goto_0

    .line 90
    :pswitch_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$400(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$400(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->finishCurrentViewState()V

    goto/16 :goto_0

    .line 67
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
