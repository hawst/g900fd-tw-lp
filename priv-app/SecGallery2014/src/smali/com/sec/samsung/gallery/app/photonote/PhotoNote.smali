.class public Lcom/sec/samsung/gallery/app/photonote/PhotoNote;
.super Landroid/app/Activity;
.source "PhotoNote.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;
    }
.end annotation


# static fields
.field public static final BGIMAGE_PATH:Ljava/lang/String;

.field public static final DEFAULT_FRAME_IMAGEDATA_DIRECTORY:Ljava/lang/String; = "Photo frame"

.field public static final DEFAULT_NOTE_IMAGEDATA_DIRECTORY:Ljava/lang/String; = "Photo note"

.field public static final DEFAULT_SECRETBOX_DIRECTORY:Ljava/lang/String;

.field private static final DIALOG_DELETE_CONFIRM:I = 0x0

.field private static final DIALOG_SAVE_CONFIRM:I = 0x1

.field public static final EXTRA_IMAGE_NAME:Ljava/lang/String; = "filename"

.field public static final EXTRA_IMAGE_PATH:Ljava/lang/String; = "path"

.field public static final EXTRA_NOTE_TYPE:Ljava/lang/String; = "type"

.field public static final EXTRA_VIEW_MODE:Ljava/lang/String; = "view_mode"

.field public static final IMAGE_NOTE_REQUEST:I = 0x500

.field public static final PHOTO_FRAME_FOLDER:Ljava/lang/String;

.field public static final PHOTO_NOTE_FOLDER:Ljava/lang/String;

.field public static final SAVED_FILE_EXTENSION:Ljava/lang/String; = "png"

.field public static final TAG:Ljava/lang/String; = "PhotoNote"

.field public static final TYPE_IMAGE_NOTE:I = 0x1

.field public static final TYPE_POLAROID_FRAME:I = 0x2


# instance fields
.field private TYPE_SAVE_ERROR:I

.field private TYPE_SAVE_NEWFILE:I

.field private TYPE_SAVE_NORMAL:I

.field private mContext:Landroid/content/Context;

.field private mFilePath:Ljava/lang/String;

.field private mFolder:Ljava/io/File;

.field private mImageChanged:Z

.field private mIsSecretboxItem:Z

.field private mIsViewMode:Z

.field private mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

.field private mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

.field private mNoteMode:I

.field private mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

.field mShareDialog:Lcom/sec/samsung/gallery/app/photonote/ShareDialog;

.field private mUri:Landroid/net/Uri;

.field private mViewActionListener:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-static {v1}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Photo note"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->PHOTO_NOTE_FOLDER:Ljava/lang/String;

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-static {v1}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Photo frame"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->PHOTO_FRAME_FOLDER:Ljava/lang/String;

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->PHOTO_NOTE_FOLDER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/bgimage.jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->BGIMAGE_PATH:Ljava/lang/String;

    .line 74
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->SECRETBOX_PATH:Ljava/lang/String;

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->DEFAULT_SECRETBOX_DIRECTORY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 76
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->TYPE_SAVE_ERROR:I

    .line 77
    iput v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->TYPE_SAVE_NORMAL:I

    .line 78
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->TYPE_SAVE_NEWFILE:I

    .line 87
    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;

    .line 88
    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFilePath:Ljava/lang/String;

    .line 89
    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFolder:Ljava/io/File;

    .line 90
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mIsSecretboxItem:Z

    .line 91
    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mUri:Landroid/net/Uri;

    .line 92
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteMode:I

    .line 93
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mIsViewMode:Z

    .line 94
    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 96
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$1;-><init>(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    .line 132
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$2;-><init>(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mViewActionListener:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;

    .line 661
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/PhotoNote;
    .param p1, "x1"    # Z

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->finish_note(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->onSave()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->actionDelete()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;Landroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/PhotoNote;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->actionShare(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->actionExport()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;Landroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/PhotoNote;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->actionPrint(Landroid/net/Uri;)V

    return-void
.end method

.method private actionDelete()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 629
    iget v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteMode:I

    if-ne v0, v1, :cond_0

    .line 630
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFilePath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->clearNoteDocData(Ljava/lang/String;)V

    .line 634
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    if-eqz v0, :cond_1

    .line 635
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->clearDataInSpenPageDoc(Z)V

    .line 639
    :cond_1
    return-void
.end method

.method private actionExport()V
    .locals 5

    .prologue
    .line 623
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_SCAN"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 625
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;

    const v1, 0x7f0e020c

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "Photo note"

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 626
    return-void
.end method

.method private actionPrint(Landroid/net/Uri;)V
    .locals 6
    .param p1, "fileUri"    # Landroid/net/Uri;

    .prologue
    .line 642
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MEDIA_SCAN"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "file://"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 645
    :try_start_0
    new-instance v1, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;

    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, p1}, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;->excute()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 649
    :goto_0
    return-void

    .line 646
    :catch_0
    move-exception v0

    .line 647
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private actionShare(Landroid/net/Uri;)V
    .locals 5
    .param p1, "fileUri"    # Landroid/net/Uri;

    .prologue
    .line 614
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_SCAN"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 616
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mShareDialog:Lcom/sec/samsung/gallery/app/photonote/ShareDialog;

    if-nez v0, :cond_0

    .line 617
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mShareDialog:Lcom/sec/samsung/gallery/app/photonote/ShareDialog;

    .line 618
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mShareDialog:Lcom/sec/samsung/gallery/app/photonote/ShareDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->show()V

    .line 620
    return-void
.end method

.method private clearNoteDocData(Ljava/lang/String;)V
    .locals 2
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 544
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    if-eqz v0, :cond_0

    .line 545
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->writeData(Ljava/lang/String;Z)Z

    .line 546
    :cond_0
    return-void
.end method

.method public static createBGimage()Ljava/io/File;
    .locals 4

    .prologue
    .line 283
    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->PHOTO_NOTE_FOLDER:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 284
    .local v1, "folder":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 285
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    .line 286
    const-string v2, "PhotoNote"

    const-string v3, "Default Save Path Creation Error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    const/4 v0, 0x0

    .line 291
    :goto_0
    return-object v0

    .line 290
    :cond_0
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->BGIMAGE_PATH:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 291
    .local v0, "bgFile":Ljava/io/File;
    goto :goto_0
.end method

.method public static deleteBGimage()V
    .locals 2

    .prologue
    .line 295
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->BGIMAGE_PATH:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 296
    .local v0, "tempFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 297
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 299
    :cond_0
    return-void
.end method

.method private finish_note(Z)V
    .locals 1
    .param p1, "isDataChanged"    # Z

    .prologue
    .line 242
    if-eqz p1, :cond_0

    .line 243
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->setResult(I)V

    .line 246
    :goto_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->finish()V

    .line 247
    return-void

    .line 245
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->setResult(I)V

    goto :goto_0
.end method

.method private initData(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x0

    .line 321
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mUri:Landroid/net/Uri;

    .line 322
    const-string/jumbo v1, "type"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteMode:I

    .line 323
    iget v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteMode:I

    if-gtz v1, :cond_0

    .line 324
    iput v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteMode:I

    .line 327
    :cond_0
    const-string/jumbo v1, "view_mode"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mIsViewMode:Z

    .line 329
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mUri:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getFilePathFrom(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFilePath:Ljava/lang/String;

    .line 330
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFilePath:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 339
    :cond_1
    :goto_0
    return v0

    .line 333
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFilePath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->isSecretboxItem(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mIsSecretboxItem:Z

    .line 334
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->initFolder()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 337
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->setImageChanged(Z)V

    .line 339
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private initFolder()Z
    .locals 3

    .prologue
    .line 343
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mIsSecretboxItem:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/privatemode/PrivateModeManager;->isPrivateStorageMounted(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->DEFAULT_SECRETBOX_DIRECTORY:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFolder:Ljava/io/File;

    .line 348
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFolder:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    .line 349
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFolder:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_2

    .line 350
    const-string v0, "PhotoNote"

    const-string v1, "Default Save Path Creation Error"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    const/4 v0, 0x0

    .line 355
    :goto_1
    return v0

    .line 346
    :cond_0
    new-instance v1, Ljava/io/File;

    iget v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteMode:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    sget-object v0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->PHOTO_FRAME_FOLDER:Ljava/lang/String;

    :goto_2
    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFolder:Ljava/io/File;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->PHOTO_NOTE_FOLDER:Ljava/lang/String;

    goto :goto_2

    .line 355
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private initScanvasView()V
    .locals 4

    .prologue
    .line 309
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getIntent()Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mViewActionListener:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;-><init>(Landroid/content/Context;Landroid/content/Intent;Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    .line 310
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->updateAnimation()V

    .line 311
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mIsViewMode:Z

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setMovingMode()V

    .line 314
    :cond_0
    return-void
.end method

.method private isHiddenImage(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 549
    const-string v0, "/.hide/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private isSecretboxItem(Ljava/lang/String;)Z
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 741
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 742
    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 743
    const/4 v0, 0x1

    .line 746
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onSave()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 466
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mIsViewMode:Z

    if-eqz v0, :cond_1

    .line 467
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->finish_note(Z)V

    .line 477
    :cond_0
    :goto_0
    return-void

    .line 470
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteMode:I

    if-ne v0, v2, :cond_3

    .line 471
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->saveCanvasImage()Z

    move-result v0

    if-nez v0, :cond_2

    .line 472
    const-string v0, "PhotoNote"

    const-string v1, "SAVE failed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    :cond_2
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->finish_note(Z)V

    goto :goto_0

    .line 474
    :cond_3
    iget v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 475
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFilePath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->isSecretboxItem(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->DEFAULT_SECRETBOX_DIRECTORY:Ljava/lang/String;

    :goto_1
    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const v0, 0x7f0f0025

    const v2, 0x7f0e020a

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->processImage(Ljava/io/File;ILjava/lang/String;)V

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->PHOTO_FRAME_FOLDER:Ljava/lang/String;

    goto :goto_1
.end method

.method private processImage(Ljava/io/File;ILjava/lang/String;)V
    .locals 2
    .param p1, "folder"    # Ljava/io/File;
    .param p2, "itemId"    # I
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    .line 652
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 653
    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 654
    const-string v0, "PhotoNote"

    const-string v1, "error mkdir!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    :goto_0
    return-void

    .line 658
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;-><init>(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;Ljava/io/File;ILjava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private registerMediaEjectReceiver()V
    .locals 3

    .prologue
    .line 115
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 116
    .local v0, "intentMediaScannerFilter":Landroid/content/IntentFilter;
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 117
    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 118
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 119
    return-void
.end method

.method private setFakeView()V
    .locals 4

    .prologue
    .line 192
    const v3, 0x7f0f014f

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 193
    .local v2, "main":Landroid/widget/FrameLayout;
    const v3, 0x7f0f01f5

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 195
    .local v1, "imageView":Landroid/widget/ImageView;
    sget-object v3, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->BGIMAGE_PATH:Ljava/lang/String;

    invoke-static {v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 196
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 197
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 198
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 200
    :cond_0
    return-void
.end method

.method private setWindow()V
    .locals 3

    .prologue
    const/16 v2, 0x400

    .line 302
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 303
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFullScreen(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 306
    :cond_0
    return-void
.end method

.method private unregisteMediaEjectReceiver()V
    .locals 3

    .prologue
    .line 122
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 124
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    .line 130
    :cond_0
    return-void

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getImageChanged()Z
    .locals 1

    .prologue
    .line 608
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mImageChanged:Z

    return v0
.end method

.method protected getMainLayoutId()I
    .locals 1

    .prologue
    .line 317
    const v0, 0x7f0300bc

    return v0
.end method

.method public getMultiWindowActivity()Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .locals 1

    .prologue
    .line 750
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v0, :cond_1

    .line 751
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-nez v0, :cond_0

    .line 752
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 754
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 756
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 234
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->updateSCanvasViewLayout()V

    .line 236
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->updateColorPickerOffset()V

    .line 238
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 239
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 151
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 152
    iput-object p0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;

    .line 153
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->initializeSPenSDK(Landroid/content/Context;)I

    .line 154
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->initData(Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 157
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->registerMediaEjectReceiver()V

    .line 158
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->setWindow()V

    .line 159
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getMainLayoutId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->setContentView(I)V

    .line 160
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->setFakeView()V

    .line 161
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->initScanvasView()V

    .line 163
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 164
    .local v0, "actionBar":Landroid/app/ActionBar;
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mIsViewMode:Z

    if-eqz v1, :cond_3

    .line 165
    invoke-virtual {v0, v3, v3}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 166
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 167
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 168
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 169
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 170
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200f4

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 171
    const v1, 0x7f0f013d

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 177
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v1, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 183
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v1, :cond_0

    .line 184
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-nez v1, :cond_2

    .line 185
    new-instance v1, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 187
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setMultiWindow(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;)V

    goto :goto_0

    .line 173
    :cond_3
    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    goto :goto_1
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 13
    .param p1, "id"    # I

    .prologue
    const v12, 0x7f0e0210

    const v11, 0x7f0e00db

    const v10, 0x7f0e0046

    const/4 v9, 0x1

    .line 359
    packed-switch p1, :pswitch_data_0

    .line 423
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v7

    :goto_0
    return-object v7

    .line 361
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 362
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e020f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 364
    .local v2, "deleteImageNoteMsg":Ljava/lang/String;
    const v7, 0x7f0e020e

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    new-instance v8, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$3;

    invoke-direct {v8, p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$3;-><init>(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)V

    invoke-virtual {v7, v11, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v10, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 373
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    goto :goto_0

    .line 377
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v2    # "deleteImageNoteMsg":Ljava/lang/String;
    :pswitch_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 378
    .local v1, "builderSave":Landroid/app/AlertDialog$Builder;
    const/4 v5, 0x0

    .line 380
    .local v5, "saveImageNoteMsg":Ljava/lang/String;
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v7, :cond_0

    .line 381
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0e0211

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 401
    :goto_1
    iget v7, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteMode:I

    if-ne v7, v9, :cond_3

    .line 402
    const v7, 0x7f0e01fe

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 407
    .local v6, "title":Ljava/lang/String;
    :goto_2
    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    new-instance v8, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$5;

    invoke-direct {v8, p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$5;-><init>(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)V

    invoke-virtual {v7, v11, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    new-instance v8, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$4;

    invoke-direct {v8, p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$4;-><init>(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)V

    invoke-virtual {v7, v10, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 419
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    goto/16 :goto_0

    .line 384
    .end local v6    # "title":Ljava/lang/String;
    :cond_0
    iget v7, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteMode:I

    if-ne v7, v9, :cond_2

    .line 385
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0e0212

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 390
    .local v4, "saveImageNoteCurrentMsg":Ljava/lang/String;
    :goto_3
    const-string v7, "."

    invoke-virtual {v4, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 392
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    .line 393
    .local v3, "locale":Ljava/lang/String;
    sget-object v7, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 394
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x2e

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 397
    .end local v3    # "locale":Ljava/lang/String;
    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 387
    .end local v4    # "saveImageNoteCurrentMsg":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0e0213

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "saveImageNoteCurrentMsg":Ljava/lang/String;
    goto :goto_3

    .line 404
    .end local v4    # "saveImageNoteCurrentMsg":Ljava/lang/String;
    :cond_3
    const v7, 0x7f0e01ff

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getString(I)Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "title":Ljava/lang/String;
    goto/16 :goto_2

    .line 359
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v3, 0x7f0f0267

    const/4 v2, 0x0

    .line 260
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f120007

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 262
    iget v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 263
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 264
    const v0, 0x7f0f0266

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 267
    :cond_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mIsViewMode:Z

    if-eqz v0, :cond_1

    .line 268
    invoke-interface {p1, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 271
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 272
    const v0, 0x7f0f0268

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 274
    :cond_2
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 212
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 214
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->unregisteMediaEjectReceiver()V

    .line 215
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->destroy()V

    .line 218
    :cond_0
    invoke-static {}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->deleteBGimage()V

    .line 219
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 588
    const/4 v2, 0x4

    if-ne p1, v2, :cond_0

    .line 590
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 600
    :cond_0
    :goto_0
    return v0

    .line 593
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getImageChanged()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 594
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->showDialog(I)V

    :goto_1
    move v0, v1

    .line 598
    goto :goto_0

    .line 596
    :cond_2
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->finish_note(Z)V

    goto :goto_1
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "oldLeft"    # I
    .param p7, "oldTop"    # I
    .param p8, "oldRight"    # I
    .param p9, "oldBottom"    # I

    .prologue
    .line 252
    sub-int v0, p9, p7

    sub-int v1, p5, p3

    if-eq v0, v1, :cond_0

    .line 253
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->updateSCanvasViewLayout()V

    .line 254
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->updateColorPickerOffset()V

    .line 256
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 430
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 431
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->isSpenDocInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    .line 462
    :goto_0
    return v0

    .line 434
    :cond_0
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mIsSecretboxItem:Z

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/privatemode/PrivateModeManager;->isPrivateStorageMounted(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 435
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->initFolder()Z

    .line 437
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 457
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->onSave()V

    move v0, v1

    .line 458
    goto :goto_0

    .line 439
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->onSave()V

    move v0, v1

    .line 440
    goto :goto_0

    .line 442
    :sswitch_2
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->finish_note(Z)V

    move v0, v1

    .line 443
    goto :goto_0

    .line 445
    :sswitch_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFolder:Ljava/io/File;

    const v3, 0x7f0f0265

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v2, v3, v0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->processImage(Ljava/io/File;ILjava/lang/String;)V

    move v0, v1

    .line 446
    goto :goto_0

    .line 448
    :sswitch_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFolder:Ljava/io/File;

    const v3, 0x7f0f0266

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v2, v3, v0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->processImage(Ljava/io/File;ILjava/lang/String;)V

    move v0, v1

    .line 449
    goto :goto_0

    .line 451
    :sswitch_5
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->showDialog(I)V

    move v0, v1

    .line 452
    goto :goto_0

    .line 454
    :sswitch_6
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFolder:Ljava/io/File;

    const v3, 0x7f0f0268

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v2, v3, v0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->processImage(Ljava/io/File;ILjava/lang/String;)V

    move v0, v1

    .line 455
    goto :goto_0

    .line 437
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0f0023 -> :sswitch_2
        0x7f0f0025 -> :sswitch_1
        0x7f0f0265 -> :sswitch_3
        0x7f0f0266 -> :sswitch_4
        0x7f0f0267 -> :sswitch_5
        0x7f0f0268 -> :sswitch_6
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 279
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 223
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 224
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFilePath:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 225
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFilePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 226
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 227
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->finish_note(Z)V

    .line 230
    .end local v0    # "f":Ljava/io/File;
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 204
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 207
    const-string v0, "android:fragments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 208
    return-void
.end method

.method protected photoFileSave(Ljava/lang/String;)I
    .locals 11
    .param p1, "strFileName"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 509
    iget-object v7, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->hasDataInSpenPageDoc()Z

    move-result v7

    if-nez v7, :cond_0

    .line 512
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->clearNoteDocData(Ljava/lang/String;)V

    .line 513
    iget v7, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->TYPE_SAVE_ERROR:I

    .line 539
    :goto_0
    return v7

    .line 516
    :cond_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v7, p1, v8}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->writeData(Ljava/lang/String;Z)Z

    move-result v4

    .line 517
    .local v4, "saveResult":Z
    if-nez v4, :cond_1

    .line 518
    iget v7, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->TYPE_SAVE_ERROR:I

    goto :goto_0

    .line 522
    :cond_1
    :try_start_0
    new-instance v7, Ljava/io/File;

    iget-object v8, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFilePath:Ljava/lang/String;

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 523
    .local v2, "fileSize":J
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 524
    .local v6, "values":Landroid/content/ContentValues;
    const-string v7, "_size"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 525
    iget-object v7, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFilePath:Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->isHiddenImage(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 526
    iget-object v7, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v7}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v1

    .line 528
    .local v1, "manager":Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    const-string v5, "images_hidden_album"

    .line 529
    .local v5, "table":Ljava/lang/String;
    const-string v7, "_data = ?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFilePath:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v1, v5, v6, v7, v8}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 536
    .end local v1    # "manager":Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    .end local v5    # "table":Ljava/lang/String;
    :goto_1
    iget v7, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->TYPE_SAVE_NORMAL:I

    goto :goto_0

    .line 534
    :cond_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mUri:Landroid/net/Uri;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v6, v9, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 537
    .end local v2    # "fileSize":J
    .end local v6    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 538
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 539
    iget v7, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->TYPE_SAVE_ERROR:I

    goto :goto_0
.end method

.method saveBitmapPNG(Ljava/lang/String;Landroid/graphics/Bitmap;)Z
    .locals 8
    .param p1, "strFileName"    # Ljava/lang/String;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v2, 0x0

    .line 554
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 583
    :cond_0
    :goto_0
    return v2

    .line 557
    :cond_1
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 558
    .local v5, "saveFile":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 559
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 563
    :cond_2
    const/4 v2, 0x0

    .line 564
    .local v2, "mSuccessSave":Z
    const/4 v3, 0x0

    .line 567
    .local v3, "out":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {v5}, Ljava/io/File;->createNewFile()Z

    .line 568
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 569
    .end local v3    # "out":Ljava/io/OutputStream;
    .local v4, "out":Ljava/io/OutputStream;
    :try_start_1
    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v7, 0x64

    invoke-virtual {p2, v6, v7, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    .line 574
    if-eqz v4, :cond_4

    .line 576
    :try_start_2
    invoke-virtual {v4}, Ljava/io/OutputStream;->flush()V

    .line 577
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v3, v4

    .line 580
    .end local v4    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    goto :goto_0

    .line 578
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v4    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 579
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v3, v4

    .line 580
    .end local v4    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    goto :goto_0

    .line 570
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 571
    .local v1, "e1":Ljava/io/IOException;
    :goto_1
    :try_start_3
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 572
    const/4 v2, 0x0

    .line 574
    if-eqz v3, :cond_0

    .line 576
    :try_start_4
    invoke-virtual {v3}, Ljava/io/OutputStream;->flush()V

    .line 577
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 578
    :catch_2
    move-exception v0

    .line 579
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 574
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_2
    if-eqz v3, :cond_3

    .line 576
    :try_start_5
    invoke-virtual {v3}, Ljava/io/OutputStream;->flush()V

    .line 577
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 580
    :cond_3
    :goto_3
    throw v6

    .line 578
    :catch_3
    move-exception v0

    .line 579
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 574
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v4    # "out":Ljava/io/OutputStream;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    goto :goto_2

    .line 570
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v4    # "out":Ljava/io/OutputStream;
    :catch_4
    move-exception v1

    move-object v3, v4

    .end local v4    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    goto :goto_1

    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v4    # "out":Ljava/io/OutputStream;
    :cond_4
    move-object v3, v4

    .end local v4    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    goto :goto_0
.end method

.method public saveCanvasImage()Z
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 480
    iget-object v5, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFilePath:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 481
    iget-object v5, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFilePath:Ljava/lang/String;

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->photoFileSave(Ljava/lang/String;)I

    move-result v1

    .line 482
    .local v1, "result":I
    iget v5, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->TYPE_SAVE_NORMAL:I

    if-ne v1, v5, :cond_2

    .line 483
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getImageChanged()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 484
    iget-object v5, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;

    const v6, 0x7f0e020b

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 485
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->setImageChanged(Z)V

    .line 494
    :cond_0
    :goto_0
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFindoSearch:Z

    if-eqz v4, :cond_1

    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-eqz v4, :cond_1

    .line 496
    :try_start_0
    new-instance v2, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFilePath:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 498
    .local v2, "strokeManager":Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;
    const/4 v4, 0x1

    new-array v4, v4, [Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getSpenPageDoc()Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 505
    .end local v1    # "result":I
    .end local v2    # "strokeManager":Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;
    :cond_1
    :goto_1
    return v3

    .line 487
    .restart local v1    # "result":I
    :cond_2
    iget v5, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->TYPE_SAVE_NEWFILE:I

    if-ne v1, v5, :cond_0

    .line 488
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getImageChanged()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 489
    iget-object v5, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;

    const v6, 0x7f0e020c

    new-array v7, v3, [Ljava/lang/Object;

    const-string v8, "Photo note"

    aput-object v8, v7, v4

    invoke-virtual {p0, v6, v7}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 491
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->setImageChanged(Z)V

    goto :goto_0

    .line 499
    :catch_0
    move-exception v0

    .line 500
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "PhotoNote"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "result":I
    :cond_3
    move v3, v4

    .line 505
    goto :goto_1
.end method

.method public setImageChanged(Z)V
    .locals 0
    .param p1, "ischanged"    # Z

    .prologue
    .line 604
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mImageChanged:Z

    .line 605
    return-void
.end method
