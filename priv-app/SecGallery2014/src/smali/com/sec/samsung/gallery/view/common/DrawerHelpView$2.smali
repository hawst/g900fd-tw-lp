.class Lcom/sec/samsung/gallery/view/common/DrawerHelpView$2;
.super Ljava/lang/Object;
.source "DrawerHelpView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->removeView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$2;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$2;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mMainView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$200(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$2;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$100(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/widget/RelativeLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 108
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$2;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$102(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;

    .line 109
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$2;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mIsShowingHelpView:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$002(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;Z)Z

    .line 110
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$2;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$700(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v0

    const/high16 v1, -0x67000000

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setScrimColor(I)V

    .line 111
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$2;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$300(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 112
    return-void
.end method
