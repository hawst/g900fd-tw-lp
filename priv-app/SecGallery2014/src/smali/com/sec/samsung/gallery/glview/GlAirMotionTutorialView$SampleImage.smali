.class public Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;
.super Lcom/sec/samsung/gallery/glview/GlBaseObject;
.source "GlAirMotionTutorialView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SampleImage"
.end annotation


# instance fields
.field private mImageHeight:I

.field private mImageWidth:I

.field private mImgView:Lcom/sec/android/gallery3d/glcore/GlImageView;

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;Lcom/sec/android/gallery3d/glcore/GlLayer;II)V
    .locals 7
    .param p2, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p3, "resId"    # I
    .param p4, "size"    # I

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 232
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->this$0:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;

    move-object v0, p0

    move-object v1, p2

    move v3, v2

    move v4, p4

    move v5, p4

    .line 234
    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V

    .line 236
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->access$000(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImgView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 237
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImgView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {v0, p3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 238
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImgView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 240
    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 241
    .local v6, "opt":Landroid/graphics/BitmapFactory$Options;
    const/4 v0, 0x1

    iput-boolean v0, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 242
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v0

    # getter for: Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->access$000(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1, p3, v6}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 243
    iget v0, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImageWidth:I

    .line 244
    iget v0, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImageHeight:I

    .line 245
    return-void
.end method

.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;Lcom/sec/android/gallery3d/glcore/GlLayer;Landroid/graphics/drawable/Drawable;I)V
    .locals 6
    .param p2, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p3, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p4, "size"    # I

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 247
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->this$0:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;

    move-object v0, p0

    move-object v1, p2

    move v3, v2

    move v4, p4

    move v5, p4

    .line 248
    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V

    .line 250
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->access$000(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImgView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 251
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImgView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {v0, p3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 252
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImgView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 253
    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImageWidth:I

    .line 254
    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImageHeight:I

    .line 255
    return-void
.end method


# virtual methods
.method public getImageHeight()I
    .locals 1

    .prologue
    .line 262
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImageHeight:I

    return v0
.end method

.method public getImageWidth()I
    .locals 1

    .prologue
    .line 258
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImageWidth:I

    return v0
.end method

.method public setDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImgView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 277
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImgView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 278
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImageWidth:I

    .line 279
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImageHeight:I

    .line 280
    return-void
.end method

.method public setImage(I)V
    .locals 3
    .param p1, "resId"    # I

    .prologue
    .line 266
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImgView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 267
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImgView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 268
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 269
    .local v0, "opt":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 270
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->this$0:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->access$000(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1, v2, p1, v0}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 271
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImageWidth:I

    .line 272
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->mImageHeight:I

    .line 273
    return-void
.end method
