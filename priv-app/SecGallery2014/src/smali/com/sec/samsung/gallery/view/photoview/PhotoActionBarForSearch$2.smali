.class Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch$2;
.super Ljava/lang/Object;
.source "PhotoActionBarForSearch.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;->initializeSearchView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 3
    .param p1, "newText"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;

    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_SEARCH_FIELD_EMPTY:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;->notifyObservers(Ljava/lang/Object;)V

    .line 46
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 3
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;

    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_PRESS_SEARCH:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/core/Event;->setData(Ljava/lang/Object;)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;->notifyObservers(Ljava/lang/Object;)V

    .line 53
    const/4 v0, 0x0

    return v0
.end method
