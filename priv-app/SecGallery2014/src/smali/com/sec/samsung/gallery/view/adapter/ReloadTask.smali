.class public Lcom/sec/samsung/gallery/view/adapter/ReloadTask;
.super Ljava/lang/Thread;
.source "ReloadTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/adapter/ReloadTask$OnCheckLoadingFailedListener;,
        Lcom/sec/samsung/gallery/view/adapter/ReloadTask$OnLoadDataListener;
    }
.end annotation


# static fields
.field private static final MSG_LOAD_FINISH:I = 0x2

.field private static final MSG_LOAD_START:I = 0x1


# instance fields
.field private volatile mActive:Z

.field private final mContext:Landroid/content/Context;

.field private volatile mDirty:Z

.field private mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

.field private volatile mIsLoading:Z

.field private mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

.field private mOnCheckLoadingFailedListener:Lcom/sec/samsung/gallery/view/adapter/ReloadTask$OnCheckLoadingFailedListener;

.field private mOnLoadDataListener:Lcom/sec/samsung/gallery/view/adapter/ReloadTask$OnLoadDataListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "threadName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReloadTask"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 23
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mActive:Z

    .line 24
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mDirty:Z

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mIsLoading:Z

    .line 44
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mContext:Landroid/content/Context;

    .line 45
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->initHandler()V

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/adapter/ReloadTask;)Lcom/sec/android/gallery3d/app/LoadingListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/adapter/ReloadTask;)Lcom/sec/samsung/gallery/view/adapter/ReloadTask$OnCheckLoadingFailedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mOnCheckLoadingFailedListener:Lcom/sec/samsung/gallery/view/adapter/ReloadTask$OnCheckLoadingFailedListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/adapter/ReloadTask;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private initHandler()V
    .locals 3

    .prologue
    .line 115
    new-instance v1, Lcom/sec/samsung/gallery/view/adapter/ReloadTask$1;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, p0, v2, v0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask$1;-><init>(Lcom/sec/samsung/gallery/view/adapter/ReloadTask;Lcom/sec/android/gallery3d/ui/GLRoot;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    .line 139
    return-void
.end method

.method private loadFinished()V
    .locals 2

    .prologue
    .line 102
    const-string v0, "Gallery_Performance"

    const-string v1, "ReloadTask ::loadFinished()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    sget-object v0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->RUN:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->postGallery(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V

    .line 104
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->updateLoading(Z)V

    .line 105
    return-void
.end method

.method private loadStarted()V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->updateLoading(Z)V

    .line 99
    return-void
.end method

.method private performanceLogging(Z)V
    .locals 6
    .param p1, "updateComplete"    # Z

    .prologue
    .line 154
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_LAUNCH_FINNISH:Z

    if-nez v0, :cond_0

    .line 155
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_TIME_ENABLE:Z

    if-eqz v0, :cond_2

    .line 156
    const-string v1, "Gallery_Performance"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ReloadTask]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p1, :cond_1

    const-string v0, "finished"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-wide v4, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_TIME:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    :cond_0
    :goto_1
    return-void

    .line 156
    :cond_1
    const-string v0, "progress"

    goto :goto_0

    .line 159
    :cond_2
    const-string v1, "Gallery_Performance"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ReloadTask]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p1, :cond_3

    const-string v0, "finished"

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    const-string v0, "progress"

    goto :goto_2
.end method

.method private postGallery(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V
    .locals 2
    .param p1, "cmd"    # Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/adapter/ReloadTask$2;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask$2;-><init>(Lcom/sec/samsung/gallery/view/adapter/ReloadTask;Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 151
    return-void
.end method

.method private updateLoading(Z)V
    .locals 2
    .param p1, "loading"    # Z

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mIsLoading:Z

    if-ne v0, p1, :cond_0

    .line 112
    :goto_0
    return-void

    .line 110
    :cond_0
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mIsLoading:Z

    .line 111
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    goto :goto_1
.end method


# virtual methods
.method public isActive()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mActive:Z

    return v0
.end method

.method public declared-synchronized notifyDirty()V
    .locals 1

    .prologue
    .line 72
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mDirty:Z

    .line 73
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    monitor-exit p0

    return-void

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 50
    const/4 v0, 0x0

    .line 52
    .local v0, "updateComplete":Z
    :goto_0
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mActive:Z

    if-eqz v1, :cond_2

    .line 53
    monitor-enter p0

    .line 54
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mActive:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mDirty:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 55
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->loadFinished()V

    .line 56
    invoke-static {p0}, Lcom/sec/android/gallery3d/common/Utils;->waitWithoutInterrupt(Ljava/lang/Object;)V

    .line 57
    monitor-exit p0

    goto :goto_0

    .line 59
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 60
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mDirty:Z

    .line 61
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->loadStarted()V

    .line 63
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mOnLoadDataListener:Lcom/sec/samsung/gallery/view/adapter/ReloadTask$OnLoadDataListener;

    if-eqz v1, :cond_1

    .line 64
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mOnLoadDataListener:Lcom/sec/samsung/gallery/view/adapter/ReloadTask$OnLoadDataListener;

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask$OnLoadDataListener;->onLoadData()Z

    move-result v0

    .line 66
    :cond_1
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->performanceLogging(Z)V

    goto :goto_0

    .line 68
    :cond_2
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->loadFinished()V

    .line 69
    return-void
.end method

.method public setLoadingListener(Lcom/sec/android/gallery3d/app/LoadingListener;)V
    .locals 0
    .param p1, "loadingListener"    # Lcom/sec/android/gallery3d/app/LoadingListener;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    .line 91
    return-void
.end method

.method public setOnCheckLoadingFailedListener(Lcom/sec/samsung/gallery/view/adapter/ReloadTask$OnCheckLoadingFailedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/adapter/ReloadTask$OnCheckLoadingFailedListener;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mOnCheckLoadingFailedListener:Lcom/sec/samsung/gallery/view/adapter/ReloadTask$OnCheckLoadingFailedListener;

    .line 95
    return-void
.end method

.method public setOnLoadDataListener(Lcom/sec/samsung/gallery/view/adapter/ReloadTask$OnLoadDataListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/adapter/ReloadTask$OnLoadDataListener;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mOnLoadDataListener:Lcom/sec/samsung/gallery/view/adapter/ReloadTask$OnLoadDataListener;

    .line 87
    return-void
.end method

.method public declared-synchronized terminate()V
    .locals 1

    .prologue
    .line 77
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->mActive:Z

    .line 78
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    monitor-exit p0

    return-void

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
