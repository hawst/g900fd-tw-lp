.class public final enum Lcom/sec/samsung/gallery/util/Consts$ButtonType;
.super Ljava/lang/Enum;
.source "Consts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/util/Consts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ButtonType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/samsung/gallery/util/Consts$ButtonType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/samsung/gallery/util/Consts$ButtonType;

.field public static final enum BUTTON_DESELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

.field public static final enum BUTTON_DONE:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

.field public static final enum BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    const-string v1, "BUTTON_DONE"

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/util/Consts$ButtonType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_DONE:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    .line 9
    new-instance v0, Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    const-string v1, "BUTTON_SELECTALL"

    invoke-direct {v0, v1, v3}, Lcom/sec/samsung/gallery/util/Consts$ButtonType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    .line 10
    new-instance v0, Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    const-string v1, "BUTTON_DESELECTALL"

    invoke-direct {v0, v1, v4}, Lcom/sec/samsung/gallery/util/Consts$ButtonType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_DESELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    .line 7
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    sget-object v1, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_DONE:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_DESELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->$VALUES:[Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/samsung/gallery/util/Consts$ButtonType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    return-object v0
.end method

.method public static values()[Lcom/sec/samsung/gallery/util/Consts$ButtonType;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->$VALUES:[Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v0}, [Lcom/sec/samsung/gallery/util/Consts$ButtonType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    return-object v0
.end method
