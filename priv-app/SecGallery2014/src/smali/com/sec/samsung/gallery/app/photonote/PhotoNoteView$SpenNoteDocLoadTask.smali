.class public Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView$SpenNoteDocLoadTask;
.super Landroid/os/AsyncTask;
.source "PhotoNoteView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SpenNoteDocLoadTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;",
        ">;"
    }
.end annotation


# instance fields
.field protected mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView$SpenNoteDocLoadTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 144
    iput-object p2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView$SpenNoteDocLoadTask;->mContext:Landroid/content/Context;

    .line 145
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    .locals 7
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 150
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView$SpenNoteDocLoadTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;

    new-instance v2, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView$SpenNoteDocLoadTask;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView$SpenNoteDocLoadTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mFilePath:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView$SpenNoteDocLoadTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mImageRect:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    const/4 v6, 0x1

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;-><init>(Landroid/content/Context;Ljava/lang/String;II)V

    iput-object v2, v1, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    :try_end_0
    .catch Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    .line 162
    :goto_0
    const/4 v1, 0x0

    return-object v1

    .line 151
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;->printStackTrace()V

    goto :goto_0

    .line 153
    .end local v0    # "e":Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;
    :catch_1
    move-exception v0

    .line 154
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 155
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 156
    .local v0, "e":Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;->printStackTrace()V

    goto :goto_0

    .line 157
    .end local v0    # "e":Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
    :catch_3
    move-exception v0

    .line 158
    .local v0, "e":Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;->printStackTrace()V

    goto :goto_0

    .line 159
    .end local v0    # "e":Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;
    :catch_4
    move-exception v0

    .line 160
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 139
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView$SpenNoteDocLoadTask;->doInBackground([Ljava/lang/Void;)Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;)V
    .locals 2
    .param p1, "result"    # Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView$SpenNoteDocLoadTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->access$000(Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 168
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 169
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 139
    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView$SpenNoteDocLoadTask;->onPostExecute(Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;)V

    return-void
.end method
