.class Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$12;
.super Ljava/lang/Object;
.source "GlHoverController.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$GlHoverLabelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V
    .locals 0

    .prologue
    .line 1200
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$12;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnter(II)Z
    .locals 2
    .param p1, "margine"    # I
    .param p2, "rId"    # I

    .prologue
    .line 1212
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$12;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$12;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$2500(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    move-result-object v1

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->drawLabel(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;II)V
    invoke-static {v0, v1, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$2600(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;II)V

    .line 1213
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$12;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabel:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$2300(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 1215
    const/4 v0, 0x0

    return v0
.end method

.method public onExit(I)Z
    .locals 3
    .param p1, "rId"    # I

    .prologue
    const/4 v2, 0x0

    .line 1204
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$12;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabel:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$2300(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHoverLabelIndex:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$12;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabel:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$2300(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHoverLabelIndex:I

    if-ne v0, p1, :cond_0

    .line 1205
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$12;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabel:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$2300(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 1206
    :cond_0
    return v2
.end method
