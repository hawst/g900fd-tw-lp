.class Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;
.super Landroid/widget/ArrayAdapter;
.source "ContentToDisplayDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->createViewtypeDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;Landroid/content/Context;II[Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I
    .param p4, "x2"    # I
    .param p5, "x3"    # [Ljava/lang/String;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 164
    if-eqz p2, :cond_1

    move-object v0, p2

    .line 167
    .local v0, "cView":Landroid/view/View;
    :goto_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v4

    .line 168
    .local v4, "viewType":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->isViewByOptionEnabled(I)Z
    invoke-static {v5, v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$400(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)Z

    move-result v2

    .line 169
    .local v2, "isEnabled":Z
    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    move-object v1, v0

    .line 171
    check-cast v1, Landroid/widget/CheckedTextView;

    .line 172
    .local v1, "ctv":Landroid/widget/CheckedTextView;
    if-eqz v2, :cond_2

    .line 173
    sget-object v5, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    iget-object v6, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v6}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 174
    iget-object v5, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # |= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v5, v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$076(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)I

    .line 178
    :cond_0
    :goto_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)I

    move-result v5

    and-int/2addr v5, v4

    if-eqz v5, :cond_3

    const/4 v5, 0x1

    :goto_2
    invoke-virtual {v1, v5}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 180
    new-instance v5, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;

    invoke-direct {v5, p0, v4, p1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;-><init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;II)V

    invoke-virtual {v1, v5}, Landroid/widget/CheckedTextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 250
    const v5, 0x1020014

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 251
    .local v3, "tv":Landroid/widget/TextView;
    if-nez v3, :cond_4

    .line 263
    :goto_3
    return-object v0

    .line 164
    .end local v0    # "cView":Landroid/view/View;
    .end local v1    # "ctv":Landroid/widget/CheckedTextView;
    .end local v2    # "isEnabled":Z
    .end local v3    # "tv":Landroid/widget/TextView;
    .end local v4    # "viewType":I
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 176
    .restart local v0    # "cView":Landroid/view/View;
    .restart local v1    # "ctv":Landroid/widget/CheckedTextView;
    .restart local v2    # "isEnabled":Z
    .restart local v4    # "viewType":I
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    xor-int/lit8 v6, v4, -0x1

    # &= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v5, v6}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$072(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)I

    goto :goto_1

    .line 178
    :cond_3
    const/4 v5, 0x0

    goto :goto_2

    .line 255
    .restart local v3    # "tv":Landroid/widget/TextView;
    :cond_4
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    if-nez v2, :cond_5

    .line 258
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$200(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0e02ac

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 261
    :cond_5
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 158
    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v0

    .line 159
    .local v0, "viewType":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->isViewByOptionEnabled(I)Z
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$400(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)Z

    move-result v1

    return v1
.end method
