.class public Lcom/sec/samsung/gallery/controller/ShareViaCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ShareViaCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/ShareViaCmd$ShareHandlerCallback;
    }
.end annotation


# static fields
.field private static final DELAY_TIME_TO_START:I = 0x64

.field private static final OPEN_SHARE_VIA:I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field protected mCount:I

.field private mHandler:Landroid/os/Handler;

.field protected mSelectedItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mShareAppUtil:Lcom/sec/samsung/gallery/controller/ShareAppUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 54
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/ShareViaCmd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShareViaCmd;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->starSharViatActivity()V

    return-void
.end method

.method private exitSelectionMode()V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "EXIT_SELECTION_MODE"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;)V

    .line 118
    return-void
.end method

.method public static isOrangeCloudExist(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 121
    const/4 v2, 0x0

    .line 122
    .local v2, "result":Z
    const/4 v1, 0x0

    .line 124
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.orange.fr.cloudorange"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 128
    :goto_0
    if-nez v1, :cond_0

    .line 130
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.orange.cloud.android"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 135
    :cond_0
    :goto_1
    if-eqz v1, :cond_1

    .line 136
    const/4 v2, 0x1

    .line 137
    :cond_1
    return v2

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 131
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 132
    .restart local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method private moveOrangeCloudToTop(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/LabeledIntent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 141
    .local p1, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 142
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/LabeledIntent;

    invoke-virtual {v1}, Landroid/content/pm/LabeledIntent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.orange.fr.cloudorange.common.activities.SendToMyCoActivity"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/LabeledIntent;

    invoke-virtual {v1}, Landroid/content/pm/LabeledIntent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.funambol.android.activities.AndroidReceiveShare"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 146
    :cond_0
    const/4 v1, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 147
    add-int/lit8 v1, v0, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 141
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 150
    :cond_2
    return-void
.end method

.method private openShareVia(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    if-nez p1, :cond_0

    .line 71
    sget-object v0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->TAG:Ljava/lang/String;

    const-string v1, "onOptionsItemSelected() - uri is null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    :goto_0
    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SHRE"

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 76
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mShareAppUtil:Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->getUriList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mSelectedItemList:Ljava/util/ArrayList;

    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mSelectedItemList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mSelectedItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 78
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mShareAppUtil:Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->existImageNotCached()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mContext:Landroid/content/Context;

    const v1, 0x7f0e04bb

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 81
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mContext:Landroid/content/Context;

    const v1, 0x7f0e0113

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;II)V

    goto :goto_0

    .line 85
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mShareAppUtil:Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->existImageNotCached()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 86
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mContext:Landroid/content/Context;

    const v1, 0x7f0e04ba

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 90
    :cond_4
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->exitSelectionMode()V

    .line 92
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private starSharViatActivity()V
    .locals 9

    .prologue
    const v8, 0x7f0e0047

    .line 96
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mContext:Landroid/content/Context;

    const-string v7, "enterprise_policy"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 97
    .local v0, "edm":Landroid/app/enterprise/EnterpriseDeviceManager;
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/app/enterprise/RestrictionPolicy;->isShareListAllowed(Z)Z

    move-result v6

    if-nez v6, :cond_0

    .line 113
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mShareAppUtil:Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mSelectedItemList:Ljava/util/ArrayList;

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->createIntentForShare(Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v4

    .line 100
    .local v4, "shareIntent":Landroid/content/Intent;
    const v6, 0x10008000

    invoke-virtual {v4, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 101
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuShareViaMsg:Z

    if-eqz v6, :cond_1

    .line 102
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 103
    .local v2, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v3

    .line 104
    .local v3, "shareChooser":Landroid/content/Intent;
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mShareAppUtil:Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->getUriList()Ljava/util/ArrayList;

    move-result-object v5

    .line 105
    .local v5, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mShareAppUtil:Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    invoke-virtual {v6, v5, v4}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->shareFilterForAUS(Ljava/util/ArrayList;Landroid/content/Intent;)Ljava/util/List;

    move-result-object v2

    .line 106
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Landroid/content/pm/LabeledIntent;

    invoke-interface {v2, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/content/pm/LabeledIntent;

    .line 107
    .local v1, "extraIntents":[Landroid/content/pm/LabeledIntent;
    const-string v6, "android.intent.extra.INITIAL_INTENTS"

    invoke-virtual {v3, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 108
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 111
    .end local v1    # "extraIntents":[Landroid/content/pm/LabeledIntent;
    .end local v2    # "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    .end local v3    # "shareChooser":Landroid/content/Intent;
    .end local v5    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 3
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 46
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    .line 47
    .local v0, "params":[Ljava/lang/Object;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mContext:Landroid/content/Context;

    .line 48
    new-instance v1, Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mShareAppUtil:Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    .line 49
    new-instance v1, Landroid/os/Handler;

    new-instance v2, Lcom/sec/samsung/gallery/controller/ShareViaCmd$ShareHandlerCallback;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/controller/ShareViaCmd$ShareHandlerCallback;-><init>(Lcom/sec/samsung/gallery/controller/ShareViaCmd;)V

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mHandler:Landroid/os/Handler;

    .line 51
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/ShareViaCmd;->openShareVia(Landroid/content/Context;)V

    .line 52
    return-void
.end method
