.class public Lcom/sec/samsung/gallery/glview/GlColorFrameView;
.super Lcom/sec/android/gallery3d/glcore/GlView;
.source "GlColorFrameView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/GlColorFrameView$Frame;
    }
.end annotation


# instance fields
.field private mFrames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/glview/GlColorFrameView$Frame;",
            ">;"
        }
    .end annotation
.end field

.field private mPaint:Landroid/graphics/Paint;

.field private mStartOffset:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlView;-><init>()V

    .line 12
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlColorFrameView;->mPaint:Landroid/graphics/Paint;

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlColorFrameView;->mFrames:Ljava/util/ArrayList;

    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlColorFrameView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 20
    return-void
.end method


# virtual methods
.method public addFrame(Lcom/sec/samsung/gallery/glview/GlColorFrameView$Frame;)V
    .locals 1
    .param p1, "frame"    # Lcom/sec/samsung/gallery/glview/GlColorFrameView$Frame;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlColorFrameView;->mFrames:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlColorFrameView;->getWidth()I

    move-result v11

    .line 25
    .local v11, "width":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlColorFrameView;->getHeight()I

    move-result v7

    .line 27
    .local v7, "height":I
    const/4 v8, 0x0

    .local v8, "i":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlColorFrameView;->mFrames:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v10

    .local v10, "n":I
    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlColorFrameView;->mStartOffset:I

    .local v9, "len":I
    :goto_0
    if-ge v8, v10, :cond_0

    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlColorFrameView;->mFrames:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/samsung/gallery/glview/GlColorFrameView$Frame;

    .line 29
    .local v6, "frame":Lcom/sec/samsung/gallery/glview/GlColorFrameView$Frame;
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlColorFrameView;->mPaint:Landroid/graphics/Paint;

    iget v1, v6, Lcom/sec/samsung/gallery/glview/GlColorFrameView$Frame;->mColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 30
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlColorFrameView;->mPaint:Landroid/graphics/Paint;

    iget v1, v6, Lcom/sec/samsung/gallery/glview/GlColorFrameView$Frame;->mLength:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 31
    int-to-float v1, v9

    int-to-float v2, v9

    sub-int v0, v11, v9

    add-int/lit8 v0, v0, -0x1

    int-to-float v3, v0

    sub-int v0, v7, v9

    add-int/lit8 v0, v0, -0x1

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlColorFrameView;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 33
    iget v0, v6, Lcom/sec/samsung/gallery/glview/GlColorFrameView$Frame;->mLength:I

    add-int/2addr v9, v0

    .line 27
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 35
    .end local v6    # "frame":Lcom/sec/samsung/gallery/glview/GlColorFrameView$Frame;
    :cond_0
    return-void
.end method

.method public setStartOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlColorFrameView;->mStartOffset:I

    .line 43
    return-void
.end method
