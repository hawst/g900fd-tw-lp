.class public Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
.super Ljava/lang/Object;
.source "SecMMCodecRegionDecoder.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SisoRegionDecoder"

.field private static mUseSecMMMultiCore:Z


# instance fields
.field private mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

.field private final mNativeLock:Ljava/lang/Object;

.field private final mNativeLock_decode:Ljava/lang/Object;

.field private mNativeSisoRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

.field private mRecycled:Z

.field private secmmrd:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mUseSecMMMultiCore:Z

    .line 45
    const-string v0, "SecMMCodec"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method private constructor <init>(Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;)V
    .locals 2
    .param p1, "decoder"    # Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->secmmrd:J

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mRecycled:Z

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeLock:Ljava/lang/Object;

    .line 42
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeLock_decode:Ljava/lang/Object;

    .line 49
    iput-object p1, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeSisoRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mRecycled:Z

    .line 51
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;[BIIZZ)V
    .locals 4
    .param p1, "pathName"    # Ljava/lang/String;
    .param p2, "data"    # [B
    .param p3, "off"    # I
    .param p4, "len"    # I
    .param p5, "isShareabl"    # Z
    .param p6, "isPreview"    # Z

    .prologue
    const-wide/16 v2, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-wide v2, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->secmmrd:J

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mRecycled:Z

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeLock:Ljava/lang/Object;

    .line 42
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeLock_decode:Ljava/lang/Object;

    .line 54
    iput-wide v2, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->secmmrd:J

    .line 55
    invoke-static/range {p1 .. p6}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->nativerdinstance(Ljava/lang/String;[BIIZZ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->secmmrd:J

    .line 56
    sget-boolean v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mUseSecMMMultiCore:Z

    if-eqz v0, :cond_0

    .line 57
    iget-wide v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->secmmrd:J

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->configMultiCore(J)I

    .line 59
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mRecycled:Z

    .line 60
    return-void
.end method

.method private static native RequestCancelDecode(J)I
.end method

.method private checkRecycled(Ljava/lang/String;)V
    .locals 1
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 282
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mRecycled:Z

    if-eqz v0, :cond_0

    .line 283
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 285
    :cond_0
    return-void
.end method

.method private static native configLTN(J)I
.end method

.method private static native configMultiCore(J)I
.end method

.method private static native nativeDecodeRegion(JIIIIILandroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
.end method

.method private static native nativeDecodeRegionBB(JIIIIILjava/nio/ByteBuffer;II)Ljava/nio/ByteBuffer;
.end method

.method private static native nativeGetHeight(J)I
.end method

.method private static native nativeGetWidth(J)I
.end method

.method private static native nativerdinstance(Ljava/lang/String;[BIIZZ)J
.end method

.method private static native nativerecycle(J)I
.end method

.method public static newInstance(Ljava/io/FileDescriptor;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    .locals 4
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "isShareable"    # Z

    .prologue
    const/4 v2, 0x0

    .line 199
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    invoke-direct {v0, v2}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;-><init>(Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;)V

    .line 201
    .local v0, "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :try_start_0
    invoke-static {p0, p1}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/io/FileDescriptor;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 207
    :goto_0
    iget-object v3, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v3, :cond_0

    .line 210
    .end local v0    # "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :goto_1
    return-object v0

    .line 202
    .restart local v0    # "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :catch_0
    move-exception v1

    .line 204
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    move-object v0, v2

    .line 210
    goto :goto_1
.end method

.method public static newInstance(Ljava/io/InputStream;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    .locals 4
    .param p0, "is"    # Ljava/io/InputStream;
    .param p1, "isShareable"    # Z

    .prologue
    const/4 v2, 0x0

    .line 179
    if-nez p0, :cond_1

    move-object v0, v2

    .line 193
    :cond_0
    :goto_0
    return-object v0

    .line 183
    :cond_1
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    invoke-direct {v0, v2}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;-><init>(Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;)V

    .line 185
    .local v0, "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :try_start_0
    invoke-static {p0, p1}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    :goto_1
    iget-object v3, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    if-nez v3, :cond_0

    move-object v0, v2

    .line 193
    goto :goto_0

    .line 186
    :catch_0
    move-exception v1

    .line 187
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public static newInstance(Ljava/lang/String;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    .locals 9
    .param p0, "pathName"    # Ljava/lang/String;
    .param p1, "isShareable"    # Z

    .prologue
    const/4 v8, 0x0

    .line 155
    :try_start_0
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;-><init>(Ljava/lang/String;[BIIZZ)V

    .line 156
    .local v0, "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    iget-wide v2, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->secmmrd:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 157
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeSisoRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 159
    :try_start_1
    invoke-static {p0, p1}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/lang/String;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 164
    :goto_0
    :try_start_2
    iget-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v1, :cond_1

    .line 175
    .end local v0    # "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :cond_0
    :goto_1
    return-object v0

    .line 160
    .restart local v0    # "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :catch_0
    move-exception v7

    .line 161
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 171
    .end local v0    # "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    .end local v7    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v7

    .line 173
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v8

    .line 175
    goto :goto_1

    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v0    # "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :cond_1
    move-object v0, v8

    .line 167
    goto :goto_1
.end method

.method private static newInstance(Ljava/lang/String;ZZ)Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    .locals 9
    .param p0, "pathName"    # Ljava/lang/String;
    .param p1, "isShareable"    # Z
    .param p2, "isPreview"    # Z

    .prologue
    const/4 v8, 0x0

    .line 245
    :try_start_0
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v1, p0

    move v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;-><init>(Ljava/lang/String;[BIIZZ)V

    .line 246
    .local v0, "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    iget-wide v2, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->secmmrd:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 247
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeSisoRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 249
    :try_start_1
    invoke-static {p0, p2}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/lang/String;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 254
    :goto_0
    :try_start_2
    iget-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v1, :cond_1

    .line 264
    .end local v0    # "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :goto_1
    return-object v0

    .line 250
    .restart local v0    # "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :catch_0
    move-exception v7

    .line 251
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 260
    .end local v0    # "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    .end local v7    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v7

    .line 262
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .end local v7    # "e":Ljava/lang/Exception;
    :cond_0
    move-object v0, v8

    .line 264
    goto :goto_1

    .restart local v0    # "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :cond_1
    move-object v0, v8

    .line 257
    goto :goto_1
.end method

.method public static newInstance([BIIZ)Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    .locals 9
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "isShareable"    # Z

    .prologue
    const/4 v8, 0x0

    .line 215
    or-int v1, p1, p2

    if-ltz v1, :cond_0

    array-length v1, p0

    add-int v2, p1, p2

    if-ge v1, v2, :cond_1

    .line 216
    :cond_0
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v1

    .line 220
    :cond_1
    :try_start_0
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    const/4 v1, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;-><init>(Ljava/lang/String;[BIIZZ)V

    .line 221
    .local v0, "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    iget-wide v2, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->secmmrd:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    .line 222
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeSisoRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 224
    :try_start_1
    invoke-static {p0, p1, p2, p3}, Landroid/graphics/BitmapRegionDecoder;->newInstance([BIIZ)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 229
    :goto_0
    :try_start_2
    iget-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v1, :cond_3

    .line 240
    .end local v0    # "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :cond_2
    :goto_1
    return-object v0

    .line 225
    .restart local v0    # "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :catch_0
    move-exception v7

    .line 226
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 236
    .end local v0    # "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    .end local v7    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v7

    .line 238
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v8

    .line 240
    goto :goto_1

    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v0    # "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :cond_3
    move-object v0, v8

    .line 232
    goto :goto_1
.end method


# virtual methods
.method public decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "rect"    # Landroid/graphics/Rect;
    .param p2, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 73
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeSisoRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/BitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 96
    :goto_0
    return-object v0

    .line 76
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v8

    .line 77
    .local v8, "tile_width":I
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v9

    .line 78
    .local v9, "tile_height":I
    iget v6, p2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 79
    .local v6, "samplesize":I
    if-nez v6, :cond_1

    .line 80
    const/4 v6, 0x1

    .line 82
    :cond_1
    add-int v0, v8, v6

    add-int/lit8 v0, v0, -0x1

    div-int v8, v0, v6

    .line 83
    add-int v0, v9, v6

    add-int/lit8 v0, v0, -0x1

    div-int v9, v0, v6

    .line 84
    iget-object v0, p2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 85
    iget-object v0, p2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-ne v0, v8, :cond_2

    iget-object v0, p2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-eq v0, v9, :cond_3

    .line 86
    :cond_2
    const-string v0, "SecMM"

    const-string v1, "SecMMCodec RegionDecode Input Bitmap Erraneous\n"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v0, p2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 90
    :cond_3
    iget-object v11, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeLock_decode:Ljava/lang/Object;

    monitor-enter v11
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    :try_start_1
    iget-wide v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->secmmrd:J

    iget v2, p1, Landroid/graphics/Rect;->left:I

    iget v3, p1, Landroid/graphics/Rect;->right:I

    iget v4, p1, Landroid/graphics/Rect;->top:I

    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    iget-object v7, p2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    invoke-static/range {v0 .. v9}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->nativeDecodeRegion(JIIIIILandroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    monitor-exit v11

    goto :goto_0

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 94
    .end local v6    # "samplesize":I
    .end local v8    # "tile_width":I
    .end local v9    # "tile_height":I
    :catch_0
    move-exception v10

    .line 95
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    .line 96
    iget-object v0, p2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public decodeRegionBB(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    .locals 12
    .param p1, "rect"    # Landroid/graphics/Rect;
    .param p2, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p3, "BB"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 102
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeSisoRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v0, :cond_0

    .line 125
    .end local p3    # "BB":Ljava/nio/ByteBuffer;
    :goto_0
    return-object p3

    .line 105
    .restart local p3    # "BB":Ljava/nio/ByteBuffer;
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v8

    .line 106
    .local v8, "tile_width":I
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v9

    .line 107
    .local v9, "tile_height":I
    iget v6, p2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 108
    .local v6, "samplesize":I
    if-nez v6, :cond_1

    .line 109
    const/4 v6, 0x1

    .line 111
    :cond_1
    add-int v0, v8, v6

    add-int/lit8 v0, v0, -0x1

    div-int v8, v0, v6

    .line 112
    add-int v0, v9, v6

    add-int/lit8 v0, v0, -0x1

    div-int v9, v0, v6

    .line 113
    if-eqz p3, :cond_2

    .line 114
    invoke-virtual {p3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    mul-int v1, v8, v9

    mul-int/lit8 v1, v1, 0x4

    if-ge v0, v1, :cond_2

    .line 115
    const-string v0, "SecMM"

    const-string v1, "SecMMCodec RegionDecode Input Bitmap Erraneous\n"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 123
    .end local v6    # "samplesize":I
    .end local v8    # "tile_width":I
    .end local v9    # "tile_height":I
    :catch_0
    move-exception v10

    .line 124
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 119
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v6    # "samplesize":I
    .restart local v8    # "tile_width":I
    .restart local v9    # "tile_height":I
    :cond_2
    :try_start_1
    iget-object v11, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeLock_decode:Ljava/lang/Object;

    monitor-enter v11
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 120
    :try_start_2
    iget-wide v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->secmmrd:J

    iget v2, p1, Landroid/graphics/Rect;->left:I

    iget v3, p1, Landroid/graphics/Rect;->right:I

    iget v4, p1, Landroid/graphics/Rect;->top:I

    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    move-object v7, p3

    invoke-static/range {v0 .. v9}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->nativeDecodeRegionBB(JIIIIILjava/nio/ByteBuffer;II)Ljava/nio/ByteBuffer;

    move-result-object v0

    monitor-exit v11

    move-object p3, v0

    goto :goto_0

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 320
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->recycle()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 322
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 324
    return-void

    .line 322
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getHeight()I
    .locals 4

    .prologue
    .line 143
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 144
    :try_start_0
    const-string v0, "getHeight called on recycled region decoder"

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->checkRecycled(Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeSisoRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v0

    monitor-exit v1

    .line 148
    :goto_0
    return v0

    :cond_0
    iget-wide v2, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->secmmrd:J

    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->nativeGetHeight(J)I

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 150
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getWidth()I
    .locals 4

    .prologue
    .line 131
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 132
    :try_start_0
    const-string v0, "getWidth called on recycled region decoder"

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->checkRecycled(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeSisoRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v0

    monitor-exit v1

    .line 136
    :goto_0
    return v0

    :cond_0
    iget-wide v2, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->secmmrd:J

    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->nativeGetWidth(J)I

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 138
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final isRecycled()Z
    .locals 1

    .prologue
    .line 274
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mRecycled:Z

    return v0
.end method

.method public recycle()V
    .locals 6

    .prologue
    .line 288
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 289
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeLock_decode:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 290
    :try_start_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mRecycled:Z

    if-nez v0, :cond_0

    .line 291
    iget-wide v4, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->secmmrd:J

    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->nativerecycle(J)I

    .line 292
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->secmmrd:J

    .line 293
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mRecycled:Z

    .line 295
    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 296
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 297
    return-void

    .line 295
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 296
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public requestCancelDecode()V
    .locals 4

    .prologue
    .line 300
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 301
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mRecycled:Z

    if-nez v0, :cond_0

    .line 302
    iget-wide v2, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->secmmrd:J

    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->RequestCancelDecode(J)I

    .line 304
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->recycle()V

    .line 305
    monitor-exit v1

    .line 306
    return-void

    .line 305
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public useLTN()I
    .locals 6

    .prologue
    .line 309
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mNativeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 310
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->mRecycled:Z

    if-nez v0, :cond_0

    iget-wide v2, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->secmmrd:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 311
    iget-wide v2, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->secmmrd:J

    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->configLTN(J)I

    move-result v0

    monitor-exit v1

    .line 314
    :goto_0
    return v0

    .line 313
    :cond_0
    monitor-exit v1

    .line 314
    const/4 v0, 0x0

    goto :goto_0

    .line 313
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
