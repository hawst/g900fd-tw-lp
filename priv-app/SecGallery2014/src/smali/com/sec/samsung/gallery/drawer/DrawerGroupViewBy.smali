.class public Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;
.super Lcom/sec/samsung/gallery/drawer/AbstractDrawerGroup;
.source "DrawerGroupViewBy.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mViewByGroup:Lcom/sec/samsung/gallery/drawer/Group;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/drawer/IDrawerCallback;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "callback"    # Lcom/sec/samsung/gallery/drawer/IDrawerCallback;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/drawer/AbstractDrawerGroup;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/drawer/IDrawerCallback;)V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;->mViewByGroup:Lcom/sec/samsung/gallery/drawer/Group;

    .line 15
    return-void
.end method


# virtual methods
.method public getGroup()Lcom/sec/samsung/gallery/drawer/Group;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;->mViewByGroup:Lcom/sec/samsung/gallery/drawer/Group;

    return-object v0
.end method

.method public initialize()V
    .locals 5

    .prologue
    .line 19
    new-instance v0, Lcom/sec/samsung/gallery/drawer/Group;

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0e04cc

    const v3, 0x7f0e02af

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/drawer/Group;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;III)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;->mViewByGroup:Lcom/sec/samsung/gallery/drawer/Group;

    .line 20
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;->mCallback:Lcom/sec/samsung/gallery/drawer/IDrawerCallback;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/drawer/IDrawerCallback;->onUpdate()V

    .line 21
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 33
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 29
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 25
    return-void
.end method
