.class public Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;
.super Ljava/lang/Object;
.source "PositionControllerBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "GroupComInfo"
.end annotation


# instance fields
.field protected mCount:I

.field protected mLineCount:I

.field protected mRatio:[B

.field protected mRatioAct:[B


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 1634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1635
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;->mCount:I

    return-void
.end method


# virtual methods
.method public setItemCount(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 1641
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;->mCount:I

    if-ne v0, p1, :cond_0

    .line 1646
    :goto_0
    return-void

    .line 1644
    :cond_0
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;->mCount:I

    .line 1645
    new-array v0, p1, [B

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;->mRatioAct:[B

    goto :goto_0
.end method
