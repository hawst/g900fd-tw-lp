.class Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$4;
.super Ljava/lang/Object;
.source "ProgressDialogHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$4;->this$0:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 137
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isMEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 138
    :cond_0
    const/16 v1, 0x50

    if-eq p2, v1, :cond_1

    const/16 v1, 0x1b

    if-ne p2, v1, :cond_2

    .line 150
    :cond_1
    :goto_0
    return v0

    .line 142
    :cond_2
    const/4 v1, 0x4

    if-ne p2, v1, :cond_3

    .line 143
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$4;->this$0:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    # getter for: Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->access$000(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 144
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$4;->this$0:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    # getter for: Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->access$000(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->cancel()V

    .line 145
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$4;->this$0:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    # getter for: Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mOnCancelListener:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$onCancelListener;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->access$500(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;)Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$onCancelListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 146
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$4;->this$0:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    # getter for: Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mOnCancelListener:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$onCancelListener;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->access$500(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;)Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$onCancelListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$onCancelListener;->onCancel()V

    goto :goto_0

    .line 150
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
