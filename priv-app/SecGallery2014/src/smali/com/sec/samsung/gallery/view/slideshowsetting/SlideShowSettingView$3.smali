.class Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;
.super Ljava/lang/Object;
.source "SlideShowSettingView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f020565

    const v5, 0x7f0200d4

    const/4 v4, 0x1

    .line 148
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->setNormalBackground()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$200(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)V

    .line 149
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 150
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # setter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mfilter:I
    invoke-static {v2, v7}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$302(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;I)I

    .line 151
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 152
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 153
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 154
    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$000()Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mfilter:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$300(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->setFiltter(I)V

    .line 155
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$100(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)Landroid/content/Context;

    move-result-object v2

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$000()Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->save(Landroid/content/Context;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;)V

    .line 184
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$100(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "SLIDE_SHOW_FILTER_PREFS"

    invoke-virtual {v2, v3, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 186
    .local v1, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 187
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "FILTER"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mfilter:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$300(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)I

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 188
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 190
    return-void

    .line 156
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "preferences":Landroid/content/SharedPreferences;
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 157
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # setter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mfilter:I
    invoke-static {v2, v4}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$302(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;I)I

    .line 158
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 159
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 160
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 161
    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$000()Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mfilter:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$300(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->setFiltter(I)V

    .line 162
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$100(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)Landroid/content/Context;

    move-result-object v2

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$000()Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->save(Landroid/content/Context;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;)V

    goto :goto_0

    .line 164
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 165
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    const/4 v3, 0x2

    # setter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mfilter:I
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$302(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;I)I

    .line 166
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 168
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 169
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 170
    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$000()Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mfilter:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$300(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->setFiltter(I)V

    .line 171
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$100(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)Landroid/content/Context;

    move-result-object v2

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$000()Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->save(Landroid/content/Context;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;)V

    goto/16 :goto_0

    .line 173
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 174
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    const/4 v3, 0x3

    # setter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mfilter:I
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$302(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;I)I

    .line 175
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 177
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 178
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 179
    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$000()Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mfilter:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$300(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->setFiltter(I)V

    .line 180
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$100(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)Landroid/content/Context;

    move-result-object v2

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$000()Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->save(Landroid/content/Context;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;)V

    goto/16 :goto_0
.end method
