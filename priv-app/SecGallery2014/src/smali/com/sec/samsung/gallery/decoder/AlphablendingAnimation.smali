.class public Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;
.super Ljava/lang/Object;
.source "AlphablendingAnimation.java"


# instance fields
.field private mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

.field private mAnimationStarted:Z

.field private mDuration:F

.field private mEndAlpha:F

.field private mInstance:Ljava/lang/Object;

.field private mNeedToAnimate:Z

.field private mStartAlpha:F

.field private mStartTime:J

.field private mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    return-void
.end method

.method public constructor <init>(Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;FFF)V
    .locals 3
    .param p1, "abaInterface"    # Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;
    .param p2, "startAlpha"    # F
    .param p3, "endAlpha"    # F
    .param p4, "duration"    # F

    .prologue
    const/4 v2, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mStartTime:J

    .line 45
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mNeedToAnimate:Z

    .line 46
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mAnimationStarted:Z

    .line 47
    iput p2, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mStartAlpha:F

    .line 48
    iput p3, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mEndAlpha:F

    .line 49
    iput p4, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mDuration:F

    .line 50
    iput-object p1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    .line 51
    return-void
.end method

.method private constructor <init>(Lcom/sec/samsung/gallery/decoder/XIVInterface;)V
    .locals 0
    .param p1, "xivInterface"    # Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .line 41
    return-void
.end method

.method public static create(FFFLcom/sec/samsung/gallery/decoder/XIVInterface;)Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;
    .locals 2
    .param p0, "start"    # F
    .param p1, "end"    # F
    .param p2, "duration"    # F
    .param p3, "xivInterface"    # Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .prologue
    .line 54
    new-instance v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;

    invoke-direct {v0, p3}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;-><init>(Lcom/sec/samsung/gallery/decoder/XIVInterface;)V

    .line 55
    .local v0, "mAlphablendingAnimation":Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;
    invoke-static {p0, p1, p2}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->AlphablendingAnimationCreate(FFF)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mInstance:Ljava/lang/Object;

    .line 56
    return-object v0
.end method

.method public static getAlphablendingAnimationTimeFromLTNToRD()F
    .locals 1

    .prologue
    .line 113
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->getAlphablendingAnimationTimeFromLTNToRD()F

    move-result v0

    return v0
.end method

.method public static isUseAlphablendingAnimationFromLTNToRD()Z
    .locals 1

    .prologue
    .line 109
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->isUseAlphablendingAnimationFromLTNToRD()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public drawBlended(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;F)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "texture"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p3, "source"    # Landroid/graphics/RectF;
    .param p4, "target"    # Landroid/graphics/RectF;
    .param p5, "alpha"    # F

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_1

    .line 80
    invoke-static {p1, p2, p3, p4, p5}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->AlphablendingAnimationDrawBlended(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;F)V

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->drawBlended(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;F)V

    goto :goto_0
.end method

.method public getProgress()F
    .locals 6

    .prologue
    .line 87
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v1, :cond_0

    .line 88
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    iget-object v2, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mInstance:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->AlphablendingAnimationGetProgress(Ljava/lang/Object;)F

    move-result v1

    .line 104
    :goto_0
    return v1

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v1, :cond_3

    .line 90
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mAnimationStarted:Z

    if-eqz v1, :cond_2

    .line 91
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mStartTime:J

    sub-long/2addr v2, v4

    long-to-float v1, v2

    iget v2, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mDuration:F

    div-float v0, v1, v2

    .line 92
    .local v0, "progress":F
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    .line 94
    const/high16 v0, 0x3f800000    # 1.0f

    .line 95
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mNeedToAnimate:Z

    .line 97
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mStartAlpha:F

    iget v2, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mEndAlpha:F

    iget v3, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mStartAlpha:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    goto :goto_0

    .line 99
    .end local v0    # "progress":F
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mAnimationStarted:Z

    .line 100
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mStartTime:J

    .line 101
    iget v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mStartAlpha:F

    goto :goto_0

    .line 104
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public needToAnimate()Z
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mInstance:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->AlphablendingAnimationNeedToAnimate(Ljava/lang/Object;)Z

    move-result v0

    .line 74
    :goto_0
    return v0

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_1

    .line 72
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mNeedToAnimate:Z

    goto :goto_0

    .line 74
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_1

    .line 61
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mInstance:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->AlphablendingAnimationReset(Ljava/lang/Object;)V

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mNeedToAnimate:Z

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->mAnimationStarted:Z

    goto :goto_0
.end method
