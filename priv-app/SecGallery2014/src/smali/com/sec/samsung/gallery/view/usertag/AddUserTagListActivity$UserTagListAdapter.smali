.class public final Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;
.super Landroid/widget/BaseAdapter;
.source "AddUserTagListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "UserTagListAdapter"
.end annotation


# instance fields
.field protected final SPERATOR:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private mInterimAdapters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mUserTagCountMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 383
    .local p3, "userTagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 381
    const-string v0, ","

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->SPERATOR:Ljava/lang/String;

    .line 384
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->mContext:Landroid/content/Context;

    .line 385
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->mInterimAdapters:Ljava/util/ArrayList;

    .line 386
    invoke-virtual {p3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 387
    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mCheckedData:Ljava/util/ArrayList;
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$600(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 388
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->mInterimAdapters:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 390
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->mInterimAdapters:Ljava/util/ArrayList;

    invoke-direct {p0, p3}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->getAllUserTags(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 391
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAddTagAsMoreinfo:Z

    if-eqz v0, :cond_1

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mIsDetailView:Z
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$400(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 392
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->initData(Landroid/content/Context;)V

    .line 393
    :cond_1
    return-void
.end method

.method private getAllUserTags(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 396
    .local p1, "userTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getAllUserTag(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v3

    .line 397
    .local v3, "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 398
    .local v4, "newInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 400
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->mUserTagCountMap:Ljava/util/HashMap;

    if-nez v5, :cond_0

    .line 401
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getUserTagCountMap(Landroid/content/Context;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->mUserTagCountMap:Ljava/util/HashMap;

    .line 404
    :cond_0
    const/4 v2, 0x0

    .line 405
    .local v2, "info":Ljava/lang/String;
    const/4 v0, 0x0

    .line 406
    .local v0, "bContain":Z
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "i":I
    :goto_0
    if-lez v1, :cond_3

    .line 407
    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "info":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 408
    .restart local v2    # "info":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->mUserTagCountMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 409
    if-eqz v0, :cond_1

    if-eqz v0, :cond_2

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->mUserTagCountMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-nez v5, :cond_2

    .line 410
    :cond_1
    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 406
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 414
    :cond_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_4

    .line 415
    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 417
    :cond_4
    return-object v4
.end method


# virtual methods
.method public addTag(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 486
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 487
    .local v0, "tag":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mCheckedData:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$600(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 488
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mCheckedData:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$600(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 489
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$700(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 491
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 4

    .prologue
    .line 422
    const/4 v2, 0x0

    .line 423
    .local v2, "size":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->mInterimAdapters:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 424
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/2addr v2, v3

    .line 425
    goto :goto_0

    .line 426
    .end local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    return v2
.end method

.method public getData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 494
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mCheckedData:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$600(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 431
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->mInterimAdapters:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 432
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 433
    .local v2, "size":I
    if-ge p1, v2, :cond_0

    .line 434
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 438
    .end local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "size":I
    :goto_1
    return-object v3

    .line 436
    .restart local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v2    # "size":I
    :cond_0
    sub-int/2addr p1, v2

    .line 437
    goto :goto_0

    .line 438
    .end local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "size":I
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 443
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 448
    if-nez p2, :cond_0

    .line 449
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f03009d

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 452
    :cond_0
    const v3, 0x7f0f01aa

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 454
    .local v2, "textView":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 455
    .local v1, "tag":Ljava/lang/String;
    if-nez v1, :cond_1

    const-string v3, ""

    :goto_0
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 457
    const v3, 0x7f0f01ab

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 459
    .local v0, "checkBox":Landroid/widget/CheckBox;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mCheckedData:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$600(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 461
    return-object p2

    .end local v0    # "checkBox":Landroid/widget/CheckBox;
    :cond_1
    move-object v3, v1

    .line 455
    goto :goto_0
.end method

.method protected initData(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 498
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$500(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 499
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUriList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$800(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Ljava/util/ArrayList;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getUserTag(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 500
    .local v3, "mIniTag":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 501
    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 502
    .local v5, "tags":[Ljava/lang/String;
    move-object v0, v5

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v4, v0, v1

    .line 503
    .local v4, "tag":Ljava/lang/String;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_0

    .line 504
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$500(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 505
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mCheckedData:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$600(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 502
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 509
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v4    # "tag":Ljava/lang/String;
    .end local v5    # "tags":[Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public removeTag(I)V
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 465
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->mInterimAdapters:Ljava/util/ArrayList;

    monitor-enter v5

    .line 466
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->mInterimAdapters:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 467
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 468
    .local v2, "size":I
    if-ge p1, v2, :cond_3

    .line 469
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 470
    .local v3, "tag":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->mUserTagCountMap:Ljava/util/HashMap;

    if-nez v4, :cond_0

    .line 471
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    invoke-static {v4, v6}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getUserTagCountMap(Landroid/content/Context;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->mUserTagCountMap:Ljava/util/HashMap;

    .line 474
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mCheckedData:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$600(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 475
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mCheckedData:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$600(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 477
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$700(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Landroid/os/Handler;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 482
    .end local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "size":I
    .end local v3    # "tag":Ljava/lang/String;
    :cond_2
    monitor-exit v5

    .line 483
    return-void

    .line 480
    .restart local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v2    # "size":I
    :cond_3
    sub-int/2addr p1, v2

    .line 481
    goto :goto_0

    .line 482
    .end local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "size":I
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method
