.class public Lcom/sec/samsung/gallery/glview/GlThumbObject;
.super Lcom/sec/samsung/gallery/glview/GlBaseObject;
.source "GlThumbObject.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;,
        Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;
    }
.end annotation


# static fields
.field private static final DEF_DIM_DURATION:I = 0x1f4


# instance fields
.field private mAnimationShow:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;

.field private mAnimationTouch:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;

.field public mCAlpha:F

.field public mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

.field public mClicking:Z

.field private mDefTransAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

.field private mDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

.field private mDimDuration:I

.field public mExtObj:Ljava/lang/Object;

.field protected mFocused:Z

.field public mHndDispmnt:I

.field protected mSelected:Z

.field private mTouchAnim:Z

.field private mUnDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

.field mlistenerShow:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V
    .locals 3
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "width"    # F
    .param p3, "height"    # F

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 15
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHndDispmnt:I

    .line 20
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mClicking:Z

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTouchAnim:Z

    .line 22
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDimDuration:I

    .line 23
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    .line 24
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mSelected:Z

    .line 25
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mFocused:Z

    .line 112
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$1;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mlistenerShow:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 251
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDefTransAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 280
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$3;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 291
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mUnDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 39
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTouchAnim:Z

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setTouchAnimEnable(Z)V

    .line 40
    return-void
.end method

.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFI)V
    .locals 3
    .param p1, "baseLayer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "width"    # F
    .param p3, "height"    # F
    .param p4, "textureSize"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 28
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFI)V

    .line 15
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHndDispmnt:I

    .line 20
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mClicking:Z

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTouchAnim:Z

    .line 22
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDimDuration:I

    .line 23
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    .line 24
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mSelected:Z

    .line 25
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mFocused:Z

    .line 112
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$1;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mlistenerShow:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 251
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDefTransAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 280
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$3;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 291
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mUnDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 29
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTouchAnim:Z

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setTouchAnimEnable(Z)V

    .line 30
    return-void
.end method

.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V
    .locals 3
    .param p1, "baseLayer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "width"    # F
    .param p3, "height"    # F
    .param p4, "textureWidth"    # I
    .param p5, "textureHeight"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 33
    invoke-direct/range {p0 .. p5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V

    .line 15
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHndDispmnt:I

    .line 20
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mClicking:Z

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTouchAnim:Z

    .line 22
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDimDuration:I

    .line 23
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    .line 24
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mSelected:Z

    .line 25
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mFocused:Z

    .line 112
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$1;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mlistenerShow:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 251
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDefTransAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 280
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$3;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 291
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mUnDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 34
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTouchAnim:Z

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setTouchAnimEnable(Z)V

    .line 35
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;II)V
    .locals 3
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v2, 0x0

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 43
    invoke-direct {p0, p1, v0, v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 15
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHndDispmnt:I

    .line 20
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mClicking:Z

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTouchAnim:Z

    .line 22
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDimDuration:I

    .line 23
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    .line 24
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mSelected:Z

    .line 25
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mFocused:Z

    .line 112
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$1;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mlistenerShow:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 251
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDefTransAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 280
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$3;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 291
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mUnDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 44
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTouchAnim:Z

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setTouchAnimEnable(Z)V

    .line 45
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v1, p1, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v0, v1, p2, p3}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;IIII)V
    .locals 4
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "childWidth"    # I
    .param p5, "childHeight"    # I

    .prologue
    const/4 v3, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 49
    invoke-direct {p0, p1, v1, v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 15
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;

    .line 17
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHndDispmnt:I

    .line 20
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mClicking:Z

    .line 21
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTouchAnim:Z

    .line 22
    const/16 v1, 0x1f4

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDimDuration:I

    .line 23
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    .line 24
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mSelected:Z

    .line 25
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mFocused:Z

    .line 112
    new-instance v1, Lcom/sec/samsung/gallery/glview/GlThumbObject$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$1;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mlistenerShow:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 251
    new-instance v1, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDefTransAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 280
    new-instance v1, Lcom/sec/samsung/gallery/glview/GlThumbObject$3;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$3;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 291
    new-instance v1, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mUnDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 50
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTouchAnim:Z

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setTouchAnimEnable(Z)V

    .line 51
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v2, p1, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v1, v2, p2, p3}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 53
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlTextObject;

    invoke-direct {v0, p1, p0, p4, p5}, Lcom/sec/samsung/gallery/glview/GlTextObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/samsung/gallery/glview/GlBaseObject;II)V

    .line 54
    .local v0, "childTextObj":Lcom/sec/samsung/gallery/glview/GlTextObject;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 55
    return-void
.end method


# virtual methods
.method public dim()V
    .locals 4

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 234
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDimDuration:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->setDuration(J)V

    .line 235
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->start()V

    .line 236
    return-void
.end method

.method public getDefTransAnim()Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDefTransAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    return-object v0
.end method

.method public getFocused()Z
    .locals 1

    .prologue
    .line 317
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mFocused:Z

    return v0
.end method

.method public getSelected()Z
    .locals 1

    .prologue
    .line 321
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mSelected:Z

    return v0
.end method

.method protected onMoved(II)Z
    .locals 2
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    const/16 v1, 0x2d

    .line 66
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-gt v0, v1, :cond_0

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-le v0, v1, :cond_2

    .line 68
    :cond_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTouchAnim:Z

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->cancelPressAnim()V

    .line 71
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mClicking:Z

    .line 73
    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method protected onPressed(II)Z
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v1, 0x1

    .line 58
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTouchAnim:Z

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->startPressAnim()V

    .line 61
    :cond_0
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mClicking:Z

    .line 62
    return v1
.end method

.method protected onReleased(IIII)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTouchAnim:Z

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->cancelPressAnim()V

    .line 80
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mClicking:Z

    .line 81
    const/4 v0, 0x1

    return v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 329
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->remove()V

    .line 330
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlTextObject;->remove()V

    .line 333
    :cond_0
    return-void
.end method

.method public resetPos()V
    .locals 1

    .prologue
    .line 378
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->resetPos()V

    .line 379
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHndDispmnt:I

    .line 380
    return-void
.end method

.method public setDimDuration(I)V
    .locals 0
    .param p1, "duration"    # I

    .prologue
    .line 248
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDimDuration:I

    .line 249
    return-void
.end method

.method public setFocused(Z)V
    .locals 1
    .param p1, "focus"    # Z

    .prologue
    .line 369
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTouchAnim:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 370
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->startPressAnim()V

    .line 374
    :goto_0
    return-void

    .line 372
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->cancelPressAnim()V

    goto :goto_0
.end method

.method public setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    .prologue
    .line 362
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 363
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 366
    :cond_0
    return-void
.end method

.method public setTouchAnimEnable(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 98
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTouchAnim:Z

    .line 99
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTouchAnim:Z

    if-nez v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;

    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;

    const-wide/16 v2, 0x50

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->setDuration(J)V

    .line 107
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;

    new-instance v1, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;-><init>()V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->setInterpolator(Lcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    .line 108
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    goto :goto_0
.end method

.method public setVisibility(Z)V
    .locals 1
    .param p1, "visibility"    # Z

    .prologue
    .line 340
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(Z)V

    .line 341
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setVisibility(Z)V

    .line 344
    :cond_0
    return-void
.end method

.method public setVisibility(ZI)V
    .locals 1
    .param p1, "visibility"    # Z
    .param p2, "level"    # I

    .prologue
    .line 351
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 352
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mChildTextObj:Lcom/sec/samsung/gallery/glview/GlTextObject;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setVisibility(ZI)V

    .line 355
    :cond_0
    return-void
.end method

.method public showUpAnimDone()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationShow:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationShow:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;

    .line 95
    return-void
.end method

.method public startDefTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V
    .locals 6
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .prologue
    .line 210
    const-wide/16 v2, 0x1f4

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->startDefTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;JJ)V

    .line 211
    return-void
.end method

.method public startDefTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;J)V
    .locals 6
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;
    .param p2, "duration"    # J

    .prologue
    .line 214
    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->startDefTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;JJ)V

    .line 215
    return-void
.end method

.method public startDefTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;JJ)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;
    .param p2, "duration"    # J
    .param p4, "after"    # J

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDefTransAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 219
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDefTransAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 220
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDefTransAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {v0, p2, p3}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->setDuration(J)V

    .line 221
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDefTransAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    new-instance v1, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;-><init>()V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->setInterpolator(Lcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    .line 222
    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-nez v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDefTransAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->start()V

    .line 226
    :goto_0
    return-void

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDefTransAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {v0, p4, p5}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->startAfter(J)V

    goto :goto_0
.end method

.method public startShowUpAnim(I)V
    .locals 4
    .param p1, "animType"    # I

    .prologue
    .line 85
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;-><init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationShow:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;

    .line 86
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationShow:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->setDuration(J)V

    .line 87
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationShow:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mlistenerShow:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationShow:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 89
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAnimationShow:Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->start()V

    .line 90
    return-void
.end method

.method public stopDim()V
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->stop()V

    .line 313
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mUnDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->stop()V

    .line 314
    return-void
.end method

.method public undim()V
    .locals 4

    .prologue
    .line 239
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHndDim:I

    if-nez v0, :cond_1

    .line 245
    :cond_0
    :goto_0
    return-void

    .line 240
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHndDim:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getDim(I)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mUnDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 242
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mUnDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mDimDuration:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->setDuration(J)V

    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mUnDimAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->start()V

    goto :goto_0
.end method
