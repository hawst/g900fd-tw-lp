.class Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;
.super Ljava/lang/Object;
.source "NoItemActionBarForNormal.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, -0x1

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 59
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->access$000(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030002

    invoke-virtual {v4, v5, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 61
    .local v1, "customView":Landroid/view/View;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 62
    new-instance v3, Landroid/app/ActionBar$LayoutParams;

    invoke-direct {v3, v8, v8}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    .line 63
    .local v3, "lp":Landroid/app/ActionBar$LayoutParams;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->access$100(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0042

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    iput v4, v3, Landroid/app/ActionBar$LayoutParams;->leftMargin:I

    .line 64
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->access$200(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v1, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 72
    .end local v3    # "lp":Landroid/app/ActionBar$LayoutParams;
    :goto_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->access$800(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 73
    const/16 v2, 0xc

    .line 75
    .local v2, "flags":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->access$900(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ActionBar;->getDisplayOptions()I

    move-result v4

    or-int v0, v4, v2

    .line 76
    .local v0, "change":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->access$1000(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 78
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    # invokes: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->isReadyToEnableHomeButton()Z
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->access$1100(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 79
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->access$1200(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 81
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->access$1300(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->isFestivalMode()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 82
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->access$1400(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 83
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->access$1500(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 85
    :cond_1
    return-void

    .line 66
    .end local v0    # "change":I
    .end local v2    # "flags":I
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->access$300(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 67
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->access$400(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 68
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->access$500(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 69
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->access$600(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 70
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->access$700(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method
