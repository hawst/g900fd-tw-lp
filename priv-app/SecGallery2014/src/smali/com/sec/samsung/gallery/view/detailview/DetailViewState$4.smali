.class Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;
.super Ljava/lang/Object;
.source "DetailViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0

    .prologue
    .line 1269
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFilmIndexChanged(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 1431
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1432
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setFocusIndex(I)V

    .line 1434
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4202(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;I)I

    .line 1435
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIndexEnabled:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$7900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1436
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$10000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v0

    const-string v1, ""

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getMediaItemCount()I

    move-result v2

    invoke-virtual {v0, v1, p1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;II)V

    .line 1439
    :cond_1
    return-void
.end method

.method public onLoadingFinished(Z)V
    .locals 4
    .param p1, "loadingFailed"    # Z

    .prologue
    const/4 v3, 0x0

    .line 1309
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$6200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    .line 1311
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getMediaItemCount()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$6200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    move-result-object v1

    invoke-interface {v1, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/gallery3d/data/ActionImage;

    if-eqz v1, :cond_1

    .line 1312
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->finishDetailView()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 1380
    :cond_0
    :goto_0
    return-void

    .line 1315
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1316
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getMediaItemCount()I

    move-result v1

    if-nez v1, :cond_2

    .line 1317
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->finishDetailView()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    goto :goto_0

    .line 1322
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$6200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    move-result-object v1

    invoke-interface {v1, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 1323
    .local v0, "photo":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_0

    .line 1325
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateCurrentPhoto(Lcom/sec/android/gallery3d/data/MediaItem;)V
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$6300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 1328
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1329
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->initAgif(Lcom/sec/android/gallery3d/data/MediaItem;)V
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 1332
    :cond_3
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNearby:Z

    if-eqz v1, :cond_4

    .line 1333
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateChangePlayerIcon()V

    .line 1334
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateViewModeIcons(Landroid/content/res/Configuration;)V
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$7500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Landroid/content/res/Configuration;)V

    .line 1337
    :cond_4
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v1, :cond_5

    instance-of v1, v0, Lcom/sec/android/gallery3d/data/SnailItem;

    if-nez v1, :cond_5

    .line 1339
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->onLoadImageFinished()V

    .line 1341
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsResumeManualFaceTag:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1342
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setManualFaceTag()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 1346
    :cond_5
    sget-boolean v1, Lcom/sec/android/gallery3d/ui/FilmStripView;->mUse3DFilmStrip:Z

    if-eqz v1, :cond_6

    .line 1347
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 1348
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setFocusIndex(I)V

    .line 1352
    :cond_6
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/ImageNoteView;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 1353
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/ImageNoteView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->requestLayout()V

    .line 1354
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/ImageNoteView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->updateImageNoteIcon()V

    .line 1356
    :cond_7
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 1357
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$9000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryId()I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->playPendedPlayer(ILcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 1360
    :cond_8
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->isActiveMultiFormatShare()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1361
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$9200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "SHOW_IMAGE_VIDEO_SHARE_DIALOG"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$9100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1364
    :cond_9
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateCountOnActionBar()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$9300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    goto/16 :goto_0

    .line 1368
    .end local v0    # "photo":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_a
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsActive:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$9400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1377
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->finishDetailView()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    goto/16 :goto_0
.end method

.method public onLoadingStarted()V
    .locals 3

    .prologue
    .line 1386
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v0, :cond_0

    .line 1387
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->onUpdatemageStarted()V

    .line 1389
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetAgifMode(ZLcom/sec/android/gallery3d/data/MediaItem;)V

    .line 1391
    return-void
.end method

.method public onPhotoAvailable(Lcom/sec/android/gallery3d/data/Path;Z)V
    .locals 5
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "fullImage"    # Z

    .prologue
    const/4 v4, 0x1

    .line 1397
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBars:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$9500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->initialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1398
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->initialize(I)V

    .line 1399
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$9600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->enableFilmShow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1400
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->show()V

    .line 1404
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSideMirrorView:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/SideMirrorView;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1405
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->initSideMirrorView()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$9700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 1406
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1407
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorViewOnShowListener:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorViewOnShowListener;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$9800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorViewOnShowListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->setSideMirrorViewOnShowListener(Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorViewOnShowListener;)V

    .line 1411
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/UriImage;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    if-eqz v0, :cond_3

    .line 1415
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1416
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->setScalePrepared(Z)V

    .line 1418
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v0

    const-wide/16 v2, 0x40

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 1419
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1420
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->setScalePrepared(Z)V

    .line 1423
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCameraQuickViewTimer:Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$9900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1424
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCameraQuickViewTimer:Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$9900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;->startTimer()V

    .line 1426
    :cond_5
    return-void
.end method

.method public onPhotoChanged(ILcom/sec/android/gallery3d/data/Path;)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 1273
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I
    invoke-static {v1, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4202(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;I)I

    .line 1275
    if-eqz p2, :cond_4

    .line 1276
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mResultIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$7600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1277
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$6200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 1278
    .local v0, "photo":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_0

    .line 1279
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateCurrentPhoto(Lcom/sec/android/gallery3d/data/MediaItem;)V
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$6300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 1284
    .end local v0    # "photo":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateBars()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$5400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 1287
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->refreshHidingMessage()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$7700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 1291
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    const-wide/16 v2, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAgifVersion:J
    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$7802(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;J)J

    .line 1293
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1294
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setFocusIndex(I)V

    .line 1296
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIndexEnabled:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$7900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1297
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v1

    const-string v2, ""

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getMediaItemCount()I

    move-result v3

    invoke-virtual {v1, v2, p1, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;II)V

    .line 1299
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/ImageNoteView;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverMode()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1300
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/ImageNoteView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->requestLayout()V

    .line 1301
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/ImageNoteView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->updateImageNoteIcon()V

    .line 1303
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mResultIntent:Landroid/content/Intent;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$7600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/content/Intent;

    move-result-object v3

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setStateResult(ILandroid/content/Intent;)V
    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;ILandroid/content/Intent;)V

    .line 1305
    return-void

    .line 1282
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mResultIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$7600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onPhotoFailedToLoad()V
    .locals 4

    .prologue
    .line 1443
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCameraQuickViewTimer:Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$9900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1444
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCameraQuickViewTimer:Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$9900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;->startTimer()V

    .line 1446
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    const-wide/32 v2, 0x80000

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/ImageNoteView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1448
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/ImageNoteView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->setVisibility(I)V

    .line 1450
    :cond_1
    return-void
.end method

.method public onPhotoRquestDone()V
    .locals 2

    .prologue
    .line 1455
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    sget-object v1, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->RUN:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->postGallery(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$10100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V

    .line 1456
    return-void
.end method
