.class public Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
.super Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;
.source "GlComposeView.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;
    }
.end annotation


# static fields
.field protected static final CMD_INIT_PENSELECT:I = 0xf

.field protected static final CMD_INIT_UPDATEMAX:I = 0x10

.field protected static final CMD_QUICK_SCROLL:I = 0x8

.field protected static final CMD_QUICK_SCROLL_PROC:I = 0x9

.field protected static final CMD_RESET_LAYOUT:I = 0xc

.field protected static final CMD_SCROLL_END:I = 0xa

.field protected static final CMD_STOP_LOGGING:I = 0xe

.field protected static final CMD_UPDATE_BORDER:I = 0x7

.field protected static final CMD_UPDATE_CHECK:I = 0x4

.field protected static final CMD_UPDATE_CHECKMODE:I = 0x5

.field protected static final CMD_UPDATE_GRAVITY:I = 0x6

.field protected static final CMD_UPDATE_ITEM:I = 0x3

.field protected static final CMD_UPDATE_QUEUE:I = 0xb

.field protected static final CMD_UPDATE_RANGE:I = 0x2

.field protected static final CMD_UPDATE_SIZE:I = 0x1

.field protected static final CMD_UPDATE_TITLE_BORDER:I = 0xd

.field protected static final CRIT_DISTANCE_CLICK:I = 0x2d

.field protected static final MAX_UPDATE_QUEUE_SIZE:I = 0x1f4

.field protected static final MAX_UPDATE_UNIT:I = 0x8

.field protected static SCALE_LOWER:F = 0.0f

.field protected static SCALE_LOWER_CRIT:F = 0.0f

.field protected static SCALE_LOWER_DELTA:F = 0.0f

.field protected static SCALE_UPPER:F = 0.0f

.field protected static SCALE_UPPER_CRIT:F = 0.0f

.field protected static SCALE_UPPER_DELTA:F = 0.0f

.field protected static final SUB_CMD_QSCROLL_END:I = 0x3

.field protected static final SUB_CMD_QSCROLL_PROCESS:I = 0x2

.field protected static final SUB_CMD_QSCROLL_START:I = 0x1

.field private static final TAG:Ljava/lang/String; = "GlComposeView"

.field private static USE_GRAVITY_SENSOR:Z


# instance fields
.field private mAccelerormeterSensor:Landroid/hardware/Sensor;

.field public mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

.field protected mAlbumConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;

.field protected mClickEnabled:Z

.field protected mClickable:Z

.field protected mClicked:Z

.field protected mComposeViewItemAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

.field protected mCurrentConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;

.field protected mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

.field public mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

.field protected mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

.field private mFocusGroupObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

.field private mFocusObject:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

.field mFromDetailAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

.field protected mGlComposeViewAccessibility:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;

.field private mGlComposeViewGroupAndItemAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

.field private mGravityData:F

.field protected mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

.field public mHeightViewRatio:F

.field public mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

.field mHoverCtrlListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;

.field protected mInitialCode:I

.field protected mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mIsDisplayedPenSelectionBox:Z

.field protected mIsEasyMode:Z

.field private mIsPickMode:Z

.field protected mLayoutConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;

.field mListenerExpansionClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

.field public mListenerGenericMotion:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

.field mListenerGrpCheckBoxClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

.field private mListenerGrpCheckBoxLongClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

.field mListenerHover:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

.field mListenerPenSelect:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

.field mListenerScrollBarClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

.field mListenerThumbClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

.field mListenerThumbLongClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

.field mListenerThumbMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

.field protected mMaxLevel:I

.field protected mMinLevel:I

.field protected mOnScaling:Z

.field public mOnscroll:Z

.field public mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

.field public mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

.field protected mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

.field protected mPosCtrlArray:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

.field protected mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

.field protected mPosCtrlCount:I

.field protected mPosCtrlCurrent:I

.field protected mPosCtrlNext:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

.field private mPressObj:Lcom/sec/android/gallery3d/glcore/GlObject;

.field private mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

.field public mResource:Landroid/content/res/Resources;

.field public mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field protected mScaleAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;

.field protected mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

.field private mScreenLocked:Z

.field public mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

.field mScrollBarMoveListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

.field protected mScrollEnabled:Z

.field private mScrollFocusedGroupIndex:I

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field public mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

.field public mSplitViewExpanded:Z

.field private mTiltAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;

.field mToDetailAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

.field mToSelectionModeAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

.field private mUpdateDefaultMax:I

.field private mUpdateQueue:[I

.field private mUpdateQueueRD:I

.field private mUpdateQueueType:[I

.field private mUpdateQueueWR:I

.field protected mUseEnLargeAnimation:Z

.field private mUseHovering:I

.field protected mUseQuickSCroll:Z

.field protected mUseScaleCtrl:Z

.field public mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

.field mViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;

.field public mWidthViewRatio:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const v2, 0x3dcccccd    # 0.1f

    const/high16 v1, 0x3f800000    # 1.0f

    .line 60
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->USE_GRAVITY_SENSOR:Z

    .line 87
    const v0, 0x3f4ccccd    # 0.8f

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->SCALE_LOWER_DELTA:F

    .line 88
    const v0, 0x3fcccccd    # 1.6f

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->SCALE_UPPER_DELTA:F

    .line 89
    sget v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->SCALE_LOWER_DELTA:F

    sub-float v0, v1, v0

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->SCALE_LOWER:F

    .line 90
    sget v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->SCALE_LOWER_DELTA:F

    mul-float/2addr v0, v2

    sub-float v0, v1, v0

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->SCALE_LOWER_CRIT:F

    .line 91
    sget v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->SCALE_UPPER_DELTA:F

    add-float/2addr v0, v1

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->SCALE_UPPER:F

    .line 92
    sget v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->SCALE_UPPER_DELTA:F

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->SCALE_UPPER_CRIT:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/sec/android/gallery3d/data/MediaItem;ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "initCode"    # I
    .param p3, "initItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p4, "option"    # I
    .param p5, "config"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    .prologue
    const/16 v5, 0x1f4

    const/4 v4, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 166
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;-><init>(Landroid/content/Context;)V

    .line 103
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .line 104
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlNext:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .line 105
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlArray:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .line 107
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    .line 113
    new-array v0, v5, [I

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateQueue:[I

    .line 114
    new-array v0, v5, [I

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateQueueType:[I

    .line 115
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateQueueWR:I

    .line 116
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateQueueRD:I

    .line 117
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateDefaultMax:I

    .line 119
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    .line 120
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mLayoutConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;

    .line 121
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAlbumConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;

    .line 122
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mClicked:Z

    .line 123
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mClickable:Z

    .line 124
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mClickEnabled:Z

    .line 125
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollEnabled:Z

    .line 126
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnScaling:Z

    .line 127
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mIsEasyMode:Z

    .line 129
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    .line 130
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnscroll:Z

    .line 131
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mCurrentConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;

    .line 132
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    .line 133
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;

    .line 134
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mTiltAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;

    .line 136
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    .line 137
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .line 138
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    .line 139
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    .line 144
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGravityData:F

    .line 145
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseHovering:I

    .line 146
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseScaleCtrl:Z

    .line 147
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseEnLargeAnimation:Z

    .line 148
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseQuickSCroll:Z

    .line 149
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mSplitViewExpanded:Z

    .line 153
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScreenLocked:Z

    .line 154
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mIsDisplayedPenSelectionBox:Z

    .line 159
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollFocusedGroupIndex:I

    .line 980
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$2;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;

    .line 1009
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$3;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerThumbMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    .line 1026
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerThumbClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 1074
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$5;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerExpansionClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 1108
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$6;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$6;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerGrpCheckBoxClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 1133
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerHover:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

    .line 1187
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$8;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$8;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerGenericMotion:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    .line 1209
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$9;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$9;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mToDetailAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 1229
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$10;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$10;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFromDetailAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 1252
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$11;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$11;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerThumbLongClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

    .line 1269
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$12;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$12;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerGrpCheckBoxLongClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

    .line 1294
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$13;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$13;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrlListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;

    .line 1309
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerPenSelect:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    .line 1376
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$15;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$15;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerScrollBarClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 1382
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBarMoveListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    .line 1616
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$17;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$17;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mToSelectionModeAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 2077
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$18;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$18;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlComposeViewGroupAndItemAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

    .line 2129
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$19;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$19;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mComposeViewItemAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

    .line 167
    new-instance v0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    .line 168
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 169
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->initLayoutConfig()V

    .line 170
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAlbumConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mLayoutConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;

    .line 171
    iput p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mInitialCode:I

    .line 172
    iput-object p3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 173
    and-int/lit8 v0, p4, 0xf

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMode:I

    .line 174
    iput-object p5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    .line 175
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mUseLayoutChange:Z

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseScaleCtrl:Z

    .line 176
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mUseEnlargeAnim:Z

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseEnLargeAnimation:Z

    .line 177
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mUseQuickScroll:Z

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseQuickSCroll:Z

    .line 178
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mPosCtrl:[Ljava/lang/Object;

    array-length v0, v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCount:I

    .line 179
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mMinLevel:I

    if-ne v0, v4, :cond_2

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMinLevel:I

    .line 180
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mMaxLevel:I

    if-ne v0, v4, :cond_3

    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCount:I

    add-int/lit8 v0, v0, -0x1

    :goto_1
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMaxLevel:I

    .line 181
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mInitialLevel:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCurrent:I

    .line 182
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCurrent:I

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMinLevel:I

    if-ge v0, v3, :cond_4

    .line 183
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMinLevel:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCurrent:I

    .line 186
    :cond_0
    :goto_2
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCount:I

    new-array v0, v0, [Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlArray:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .line 187
    and-int/lit8 v0, p4, 0x30

    if-lez v0, :cond_1

    .line 188
    new-instance v3, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    and-int/lit8 v0, p4, 0x20

    if-lez v0, :cond_5

    move v0, v2

    :goto_3
    invoke-direct {v3, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;-><init>(Z)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    .line 189
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFromDetailAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 191
    :cond_1
    and-int/lit16 v0, p4, 0x100

    if-lez v0, :cond_6

    move v0, v2

    :goto_4
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mIsEasyMode:Z

    .line 192
    and-int/lit8 v0, p4, 0x40

    if-lez v0, :cond_7

    :goto_5
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseHovering:I

    .line 195
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPickMode(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mIsPickMode:Z

    .line 196
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mResource:Landroid/content/res/Resources;

    .line 197
    return-void

    .line 179
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mMinLevel:I

    goto :goto_0

    .line 180
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mMaxLevel:I

    goto :goto_1

    .line 184
    :cond_4
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCurrent:I

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMaxLevel:I

    if-le v0, v3, :cond_0

    .line 185
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMaxLevel:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCurrent:I

    goto :goto_2

    :cond_5
    move v0, v1

    .line 188
    goto :goto_3

    :cond_6
    move v0, v1

    .line 191
    goto :goto_4

    .line 192
    :cond_7
    and-int/lit16 v0, p4, 0x80

    if-lez v0, :cond_8

    const/4 v2, 0x2

    goto :goto_5

    :cond_8
    move v2, v1

    goto :goto_5
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->updateContentRange(II)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setCheckBoxVisibility()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->isSlideShowMode:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseHovering:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .param p2, "x2"    # Z

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->scaleAnimationForSelectionMode(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;Lcom/sec/android/gallery3d/glcore/GlObject;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x2"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->convertToViewPointX(Lcom/sec/android/gallery3d/glcore/GlObject;I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;Lcom/sec/android/gallery3d/glcore/GlObject;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x2"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->convertToViewPointY(Lcom/sec/android/gallery3d/glcore/GlObject;I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mIsDisplayedPenSelectionBox:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
    .param p1, "x1"    # Z

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mIsDisplayedPenSelectionBox:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
    .param p1, "x1"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getPosCtrl(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGravityData:F

    return v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->handleQuickScroll(II)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->updateFromQueue()V

    return-void
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
    .param p1, "x1"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateDefaultMax:I

    return p1
.end method

.method static synthetic access$602(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;Lcom/sec/android/gallery3d/glcore/GlObject;)Lcom/sec/android/gallery3d/glcore/GlObject;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPressObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->clearFocus()V

    return-void
.end method

.method private animateTilt(FF)V
    .locals 1
    .param p1, "start"    # F
    .param p2, "end"    # F

    .prologue
    .line 1880
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mTiltAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;->startScale(FF)V

    .line 1881
    return-void
.end method

.method private clearFocus()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2162
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-eqz v0, :cond_0

    .line 2163
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFocusObject:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 2164
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    shr-int/lit8 v1, v1, 0x10

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFocusGroupObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 2165
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFocusObject:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .line 2166
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFocusGroupObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    if-eqz v0, :cond_0

    .line 2167
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFocusGroupObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setTitleFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)V

    .line 2169
    :cond_0
    return-void
.end method

.method private convertToViewPointX(Lcom/sec/android/gallery3d/glcore/GlObject;I)I
    .locals 3
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I

    .prologue
    .line 2058
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAbsX()F

    move-result v0

    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->convX(I)F

    move-result v1

    add-float/2addr v0, v1

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getWidth(Z)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private convertToViewPointY(Lcom/sec/android/gallery3d/glcore/GlObject;I)I
    .locals 3
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "y"    # I

    .prologue
    .line 2062
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAbsY()F

    move-result v0

    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->convY(I)F

    move-result v1

    sub-float/2addr v0, v1

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getHeight(Z)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private getPosCtrl(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 465
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlArray:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    aget-object v3, v3, p1

    if-eqz v3, :cond_0

    .line 466
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlArray:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    aget-object v0, v3, p1

    .line 479
    :goto_0
    return-object v0

    .line 469
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mPosCtrl:[Ljava/lang/Object;

    aget-object v2, v3, p1

    check-cast v2, Ljava/lang/Class;

    .line 471
    .local v2, "klass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;>;"
    const/4 v0, 0x0

    .line 473
    .local v0, "ctrl":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "ctrl":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 477
    .restart local v0    # "ctrl":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->init(I)V

    .line 478
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlArray:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    aput-object v0, v3, p1

    goto :goto_0

    .line 474
    .end local v0    # "ctrl":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
    :catch_0
    move-exception v1

    .line 475
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3
.end method

.method private handleQuickScroll(II)V
    .locals 6
    .param p1, "command"    # I
    .param p2, "param"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 1969
    const/4 v2, 0x1

    if-ne p1, v2, :cond_2

    .line 1970
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->enableQuickScroll()V

    .line 1971
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    if-eqz v2, :cond_0

    .line 1972
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->exitFromHoverView(Z)V

    .line 1973
    :cond_0
    sget v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->QUICK_ITEM_PITCH:F

    invoke-direct {p0, v5, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->animateTilt(FF)V

    .line 1997
    :cond_1
    :goto_0
    return-void

    .line 1975
    :cond_2
    const/4 v2, 0x3

    if-ne p1, v2, :cond_4

    .line 1976
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->updateQuickPopupText()V

    .line 1977
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->hide()V

    .line 1978
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    if-eqz v2, :cond_3

    .line 1979
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->exitFromHoverView(Z)V

    .line 1980
    :cond_3
    sget v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->QUICK_ITEM_PITCH:F

    invoke-direct {p0, v2, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->animateTilt(FF)V

    goto :goto_0

    .line 1982
    :cond_4
    const/4 v2, 0x2

    if-ne p1, v2, :cond_1

    .line 1985
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    sget v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->QUICK_ITEM_PITCH:F

    neg-float v3, v3

    sget v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->QUICK_ROOT_PITCH_RATIO:F

    div-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPitch(F)V

    .line 1986
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    sget v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->QUICK_ROOT_Y:F

    sget v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->QUICK_ROOT_Z:F

    invoke-virtual {v2, v5, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCenter(FFF)V

    .line 1988
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    sget v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->QUICK_ITEM_PITCH:F

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->setDefaultPitch(F)V

    .line 1989
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    sget v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->QUICK_ITEM_PITCH:F

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->setDefaultPitch(F)V

    .line 1991
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->getQuickScrollRatio()F

    move-result v1

    .line 1992
    .local v1, "quickScrollRatio":F
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    mul-float v0, v2, v1

    .line 1994
    .local v0, "delta":F
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    .line 1995
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    invoke-virtual {v2, v0, v5, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->update(FFF)V

    .line 1996
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->updateQuickPopupText()V

    goto :goto_0
.end method

.method private initScrollBar()V
    .locals 3

    .prologue
    .line 483
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseQuickSCroll:Z

    if-eqz v0, :cond_0

    .line 484
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    .line 485
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBarMoveListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 486
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setGenericMotionListener()V

    .line 491
    :goto_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->resetScrollBar()V

    .line 492
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    sget v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->DEF_DISTANCE:F

    neg-float v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setDefZ(F)V

    .line 493
    return-void

    .line 488
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    goto :goto_0
.end method

.method private performanceLogging()V
    .locals 8

    .prologue
    const/16 v2, 0xe

    const/4 v3, 0x0

    .line 2044
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_TIME_ENABLE:Z

    if-eqz v0, :cond_0

    .line 2045
    const-string v0, "Gallery_Performance"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[updateFromQueue END] "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-wide v6, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_TIME:J

    sub-long/2addr v4, v6

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2049
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeMessage(I)V

    .line 2050
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const-wide/16 v6, 0x3e8

    move v4, v3

    move v5, v3

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessageDelayed(IIIIJ)V

    .line 2051
    return-void

    .line 2047
    :cond_0
    const-string v0, "Gallery_Performance"

    const-string v1, "[updateFromQueue END] "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private resetFlingEffect()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 733
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseScaleDownEndEffect:Z

    if-eqz v0, :cond_1

    .line 734
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->setScaleRate(F)V

    .line 735
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPressObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v0, :cond_0

    .line 736
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPressObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setStartedEndEffect(Z)V

    .line 741
    :cond_0
    :goto_0
    return-void

    .line 739
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->setPitchRate(FZ)V

    goto :goto_0
.end method

.method private scaleAnimationForSelectionMode(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)Z
    .locals 4
    .param p1, "object"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1585
    iget v0, p1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 1587
    .local v0, "index":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mActive:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mAutoAnimation:Z

    if-nez v2, :cond_1

    .line 1588
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->requestComplete()V

    .line 1589
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnScaling:Z

    .line 1607
    :cond_0
    :goto_0
    return v1

    .line 1592
    :cond_1
    const-string v2, "GlComposeView"

    const-string v3, "scaleAnimationForSelectionMode"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1593
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseScaleCtrl:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mLevel:I

    if-nez v2, :cond_0

    .line 1596
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionAndSplitPinch:Z

    if-nez v2, :cond_0

    .line 1599
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->prepareScale(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    .line 1600
    const/high16 v1, 0x3f800000    # 1.0f

    sget v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->SCALE_UPPER:F

    invoke-virtual {p0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->animateScale(FF)V

    .line 1601
    if-eqz p2, :cond_2

    .line 1602
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;

    # setter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->mClickedIndex:I
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->access$1602(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;I)I

    .line 1603
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mToSelectionModeAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 1607
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 1605
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    goto :goto_1
.end method

.method private setCheckBoxVisibility()V
    .locals 2

    .prologue
    .line 527
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    if-nez v0, :cond_0

    .line 531
    :goto_0
    return-void

    .line 529
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->IsCheckMode()Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mCheckBoxVisible:Z

    .line 530
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->IsCheckMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mUseGroupSelect:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGroupCheckBoxVisible:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private updateContentRange(II)V
    .locals 3
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 975
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->resetPosition(II)V

    .line 976
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setRange(FF)V

    .line 977
    return-void
.end method

.method private updateFromQueue()V
    .locals 10

    .prologue
    const/16 v9, 0xb

    const/4 v5, 0x0

    .line 890
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateQueueWR:I

    .line 891
    .local v4, "wr":I
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateQueueRD:I

    .line 892
    .local v0, "rd":I
    const/4 v2, 0x0

    .line 893
    .local v2, "updateCnt":I
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnScaling:Z

    if-eqz v6, :cond_4

    const/4 v3, 0x4

    .line 896
    .local v3, "updateLimit":I
    :cond_0
    :goto_0
    if-eq v0, v4, :cond_2

    .line 897
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateQueue:[I

    aget v7, v7, v0

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateQueueType:[I

    aget v8, v8, v0

    invoke-virtual {v6, v7, v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->update(II)Z

    move-result v1

    .line 898
    .local v1, "res":Z
    const/16 v6, 0x1f3

    if-lt v0, v6, :cond_6

    move v0, v5

    .line 899
    :goto_1
    if-eqz v1, :cond_1

    add-int/lit8 v2, v2, 0x1

    .line 900
    :cond_1
    if-lt v2, v3, :cond_0

    .line 903
    .end local v1    # "res":Z
    :cond_2
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateQueueRD:I

    .line 904
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateQueueWR:I

    if-eq v0, v6, :cond_7

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v6, v9}, Lcom/sec/android/gallery3d/glcore/GlHandler;->hasMessage(I)Z

    move-result v6

    if-nez v6, :cond_7

    .line 905
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v6, v9, v5, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 909
    :cond_3
    :goto_2
    return-void

    .line 893
    .end local v3    # "updateLimit":I
    :cond_4
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPhotoLineCount:I

    const/4 v7, 0x6

    if-lt v6, v7, :cond_5

    const/16 v3, 0x20

    goto :goto_0

    :cond_5
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateDefaultMax:I

    goto :goto_0

    .line 898
    .restart local v1    # "res":Z
    .restart local v3    # "updateLimit":I
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 906
    .end local v1    # "res":Z
    :cond_7
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_LAUNCH_FINNISH:Z

    if-nez v5, :cond_3

    .line 907
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->performanceLogging()V

    goto :goto_2
.end method

.method private updateQuickPopupText()V
    .locals 4

    .prologue
    .line 1960
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->getScrollRatio()F

    move-result v1

    .line 1961
    .local v1, "scrollRatio":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getFocusedGroupIndex(F)I

    move-result v0

    .line 1962
    .local v0, "focusedindex":I
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollFocusedGroupIndex:I

    if-eq v3, v0, :cond_0

    .line 1963
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v3, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getMediaSetName(I)Ljava/lang/String;

    move-result-object v2

    .line 1964
    .local v2, "title":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->updateScrollTitle(Ljava/lang/String;)V

    .line 1966
    .end local v2    # "title":Ljava/lang/String;
    :cond_0
    return-void
.end method


# virtual methods
.method public addToUpdateQueue(II)V
    .locals 5
    .param p1, "code"    # I
    .param p2, "type"    # I

    .prologue
    const/16 v4, 0xb

    const/4 v2, 0x0

    .line 912
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateQueue:[I

    monitor-enter v3

    .line 913
    :try_start_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateQueueWR:I

    .line 914
    .local v0, "wr":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateQueue:[I

    aput p1, v1, v0

    .line 915
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateQueueType:[I

    aput p2, v1, v0

    .line 916
    const/16 v1, 0x1f3

    if-lt v0, v1, :cond_1

    move v1, v2

    :goto_0
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateQueueWR:I

    .line 917
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 918
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/glcore/GlHandler;->hasMessage(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 919
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v1, v4, v2, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 921
    :cond_0
    return-void

    .line 916
    :cond_1
    add-int/lit8 v1, v0, 0x1

    goto :goto_0

    .line 917
    .end local v0    # "wr":I
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public animateScale(FF)V
    .locals 4
    .param p1, "start"    # F
    .param p2, "end"    # F

    .prologue
    .line 1611
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 1612
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->setDuration(J)V

    .line 1613
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->startScale(FF)V

    .line 1614
    return-void
.end method

.method protected applyPreviousScroll(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;F)V
    .locals 3
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .param p2, "scroll"    # F

    .prologue
    .line 420
    if-nez p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    sub-float v0, v1, p2

    .line 421
    .local v0, "newScrl":F
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getValidScroll(F)F

    move-result v0

    .line 423
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setScroll(FZ)V

    .line 424
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 425
    return-void

    .line 420
    .end local v0    # "newScrl":F
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v2, p1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getScrollForIndex(I)F

    move-result v1

    goto :goto_0
.end method

.method protected checkDisablePinchZoom(Landroid/view/ScaleGestureDetector;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;II)Z
    .locals 8
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;
    .param p2, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .param p3, "focusX"    # I
    .param p4, "focusY"    # I

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 749
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisablePinchZoomWhenPointersOutOfThumbnail:Z

    if-nez v4, :cond_0

    .line 750
    const/4 v4, 0x1

    .line 760
    :goto_0
    return v4

    .line 752
    :cond_0
    invoke-virtual {p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getObjectRect()Landroid/graphics/Rect;

    move-result-object v3

    .line 754
    .local v3, "thumbRect":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpanX()F

    move-result v4

    div-float v0, v4, v5

    .line 755
    .local v0, "halfSpanX":F
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpanY()F

    move-result v4

    div-float v1, v4, v5

    .line 757
    .local v1, "halfSpanY":F
    new-instance v2, Landroid/graphics/Rect;

    int-to-float v4, p3

    sub-float/2addr v4, v0

    float-to-int v4, v4

    int-to-float v5, p4

    sub-float/2addr v5, v1

    float-to-int v5, v5

    int-to-float v6, p3

    add-float/2addr v6, v0

    float-to-int v6, v6

    int-to-float v7, p4

    add-float/2addr v7, v1

    float-to-int v7, v7

    invoke-direct {v2, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 760
    .local v2, "pinchRect":Landroid/graphics/Rect;
    invoke-virtual {v3, v2}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v4

    goto :goto_0
.end method

.method public convX(I)F
    .locals 2
    .param p1, "x"    # I

    .prologue
    .line 1439
    int-to-float v0, p1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidthViewRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public convY(I)F
    .locals 2
    .param p1, "y"    # I

    .prologue
    .line 1443
    int-to-float v0, p1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeightViewRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;
    .locals 1
    .param p1, "virtualDescendantId"    # I

    .prologue
    .line 2149
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlComposeViewAccessibility:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;

    if-eqz v0, :cond_0

    .line 2150
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlComposeViewAccessibility:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 2151
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public doScroll(F)V
    .locals 2
    .param p1, "scroll"    # F

    .prologue
    .line 744
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 745
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setScroll(FZ)V

    .line 746
    return-void
.end method

.method public getActionBarHeight()I
    .locals 1

    .prologue
    .line 2054
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeightPixel()I

    move-result v0

    return v0
.end method

.method public getCurrentCenterIndex()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    .line 1530
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-nez v2, :cond_1

    .line 1535
    :cond_0
    :goto_0
    return v1

    .line 1532
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidth:I

    int-to-float v3, v3

    div-float/2addr v3, v5

    float-to-int v3, v3

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeight:I

    int-to-float v4, v4

    div-float/2addr v4, v5

    float-to-int v4, v4

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getFocusedObject(IIZ)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    move-result-object v0

    .line 1533
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v0, :cond_0

    .line 1535
    iget v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    goto :goto_0
.end method

.method public getCurrentCenterMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    .line 1521
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-nez v2, :cond_1

    .line 1526
    :cond_0
    :goto_0
    return-object v1

    .line 1523
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidth:I

    int-to-float v3, v3

    div-float/2addr v3, v5

    float-to-int v3, v3

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeight:I

    int-to-float v4, v4

    div-float/2addr v4, v5

    float-to-int v4, v4

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getFocusedObject(IIZ)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    move-result-object v0

    .line 1524
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v2, :cond_0

    .line 1526
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    iget v2, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    shr-int/lit8 v2, v2, 0x10

    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    const v4, 0xffff

    and-int/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    goto :goto_0
.end method

.method public getFirstContentPosition()I
    .locals 1

    .prologue
    .line 1460
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-eqz v0, :cond_0

    .line 1461
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpContentStart:I

    .line 1463
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 1447
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-eqz v0, :cond_0

    .line 1448
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveStart:I

    .line 1450
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGroupObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 1950
    const-string v1, "GlComposeView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "startReorder getGroupObjectIndex : index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1952
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-nez v1, :cond_0

    .line 1953
    const/4 v0, 0x0

    .line 1956
    :goto_0
    return-object v0

    .line 1955
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getGroupObject(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    move-result-object v0

    .line 1956
    .local v0, "glObject":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    goto :goto_0
.end method

.method public getLastContentPosition()I
    .locals 1

    .prologue
    .line 1466
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-eqz v0, :cond_0

    .line 1467
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpContentEnd:I

    .line 1469
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLastVisiblePosition()I
    .locals 1

    .prologue
    .line 1453
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-eqz v0, :cond_0

    .line 1454
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveEnd:I

    .line 1456
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getNextLevel(II)I
    .locals 2
    .param p1, "now"    # I
    .param p2, "toward"    # I

    .prologue
    .line 1633
    if-lez p2, :cond_1

    .line 1634
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMaxLevel:I

    if-ge p1, v1, :cond_0

    .line 1635
    add-int/lit8 v0, p1, 0x1

    .line 1644
    .local v0, "retLevel":I
    :goto_0
    return v0

    .line 1637
    .end local v0    # "retLevel":I
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMaxLevel:I

    .restart local v0    # "retLevel":I
    goto :goto_0

    .line 1639
    .end local v0    # "retLevel":I
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMinLevel:I

    if-le p1, v1, :cond_2

    .line 1640
    add-int/lit8 v0, p1, -0x1

    .restart local v0    # "retLevel":I
    goto :goto_0

    .line 1642
    .end local v0    # "retLevel":I
    :cond_2
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMinLevel:I

    .restart local v0    # "retLevel":I
    goto :goto_0
.end method

.method public getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1943
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-nez v0, :cond_0

    .line 1944
    const/4 v0, 0x0

    .line 1946
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getObject(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    move-result-object v0

    goto :goto_0
.end method

.method public getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1

    .prologue
    .line 2073
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method protected handleLongClick(I)Z
    .locals 5
    .param p1, "index"    # I

    .prologue
    const/4 v2, 0x0

    .line 1425
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnItemLongClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;

    if-eqz v3, :cond_0

    .line 1426
    shr-int/lit8 v0, p1, 0x10

    .line 1427
    .local v0, "albumIndex":I
    const v3, 0xffff

    and-int v1, p1, v3

    .line 1428
    .local v1, "photoIndex":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v4, 0x4

    invoke-virtual {v3, v4, p1, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 1429
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnItemLongClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;->onItemLongClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)Z

    move-result v2

    .line 1431
    .end local v0    # "albumIndex":I
    .end local v1    # "photoIndex":I
    :cond_0
    return v2
.end method

.method protected handleLongClick(III)Z
    .locals 1
    .param p1, "index"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 1435
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->handleLongClick(I)Z

    move-result v0

    return v0
.end method

.method public initLayoutConfig()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 1885
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    .line 1886
    .local v0, "layoutCfg":Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;
    iput v2, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->mLevelMax:I

    .line 1887
    const/16 v1, 0x8

    iput v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->mLevelLand:I

    .line 1888
    iput v2, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->mLevelPort:I

    .line 1889
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAlbumConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;

    .line 1890
    return-void
.end method

.method protected initPenSelect()V
    .locals 1

    .prologue
    .line 2066
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    if-eqz v0, :cond_0

    .line 2067
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mIsDisplayedPenSelectionBox:Z

    .line 2068
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->exitFromPenSelectionView()V

    .line 2070
    :cond_0
    return-void
.end method

.method public isShowingHelpView()Z
    .locals 1

    .prologue
    .line 2000
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    if-nez v0, :cond_0

    .line 2001
    const/4 v0, 0x0

    .line 2003
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->isShowingHelpView()Z

    move-result v0

    goto :goto_0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 887
    return-void
.end method

.method protected onClickThumbnail(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 686
    const/4 v0, 0x1

    return v0
.end method

.method protected onCreate()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 201
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->resetLayout()V

    .line 203
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidthSpace:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidth:I

    int-to-float v6, v6

    div-float/2addr v3, v6

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidthViewRatio:F

    .line 204
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeightSpace:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeight:I

    int-to-float v6, v6

    div-float/2addr v3, v6

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeightViewRatio:F

    .line 205
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 206
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidthSpace:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeightSpace:F

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getHeight()I

    move-result v9

    invoke-virtual {v3, v6, v7, v8, v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->setDimensionUtil(FFII)V

    .line 208
    new-instance v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 209
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    sget v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->DEF_DISTANCE:F

    neg-float v6, v6

    invoke-virtual {v3, v10, v10, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 210
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerThumbMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 212
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCurrent:I

    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getPosCtrl(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .line 213
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mSplitViewExpanded:Z

    iput-boolean v6, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsSplitViewExpanded:Z

    .line 214
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iput-boolean v5, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsSplitView:Z

    .line 215
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v6, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->initControlCommon(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    .line 217
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->initEnv(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;)V

    .line 218
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->setReferCtrl(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;)V

    .line 219
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWideMode:Z

    if-nez v3, :cond_7

    move v3, v4

    :goto_0
    invoke-virtual {v6, v5, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->resetAttributes(IZ)V

    .line 220
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v3, :cond_0

    .line 221
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->setAdapter(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)V

    .line 223
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setCheckBoxVisibility()V

    .line 224
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->initScrollBar()V

    .line 226
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->hideNow()V

    .line 229
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseScaleCtrl:Z

    if-eqz v3, :cond_1

    .line 230
    new-instance v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    .line 232
    :cond_1
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseEnLargeAnimation:Z

    if-eqz v3, :cond_2

    .line 233
    new-instance v3, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    .line 234
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mToDetailAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 236
    :cond_2
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseHovering:I

    if-lez v3, :cond_3

    .line 237
    new-instance v6, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrlListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseHovering:I

    if-ne v3, v4, :cond_8

    move v3, v4

    :goto_1
    invoke-direct {v6, v7, p0, v8, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;Z)V

    iput-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .line 239
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    if-eqz v3, :cond_4

    .line 240
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScreenLocked:Z

    .line 241
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->lockRefresh()V

    .line 243
    :cond_4
    new-instance v3, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-direct {v3, v6, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    .line 244
    new-instance v3, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-direct {v3, v6, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    .line 245
    new-instance v3, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-direct {v3, v6}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    .line 246
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mResource:Landroid/content/res/Resources;

    const v6, 0x7f0e0013

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 247
    .local v2, "speedRatio":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mResource:Landroid/content/res/Resources;

    const v6, 0x7f0e0014

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 248
    .local v0, "decFactor":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mResource:Landroid/content/res/Resources;

    const v6, 0x7f0c0020

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-float v1, v3

    .line 249
    .local v1, "maxSpeed":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v3, v2, v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setFactors(FF)V

    .line 250
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMaxSpeed(F)V

    .line 252
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const-wide/16 v6, 0xbb8

    invoke-virtual {v3, v6, v7}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setDuration(J)V

    .line 253
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v3, p0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setBoundaryAnimationListener(Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;)V

    .line 254
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 256
    new-instance v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;

    .line 257
    new-instance v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mTiltAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$TiltAnimation;

    .line 259
    new-instance v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v3, p0, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$1;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    .line 303
    sget-boolean v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->USE_GRAVITY_SENSOR:Z

    if-eqz v3, :cond_5

    .line 304
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    const-string v6, "sensor"

    invoke-virtual {v3, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/SensorManager;

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mSensorManager:Landroid/hardware/SensorManager;

    .line 305
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v3, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAccelerormeterSensor:Landroid/hardware/Sensor;

    .line 307
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAccelerormeterSensor:Landroid/hardware/Sensor;

    invoke-virtual {v3, p0, v4, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 310
    :cond_5
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlingInTalkBack:Z

    if-eqz v3, :cond_6

    .line 311
    new-instance v3, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v3, v4, v5}, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlComposeViewAccessibility:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;

    .line 312
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlComposeViewAccessibility:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlComposeViewGroupAndItemAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->setAccessibilityNodeInfoListener(Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;)V

    .line 314
    :cond_6
    return-void

    .end local v0    # "decFactor":F
    .end local v1    # "maxSpeed":F
    .end local v2    # "speedRatio":F
    :cond_7
    move v3, v5

    .line 219
    goto/16 :goto_0

    :cond_8
    move v3, v5

    .line 237
    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 318
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setAdapter(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)V

    .line 320
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->clean()V

    .line 321
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->destroy()V

    .line 323
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .line 325
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    if-eqz v0, :cond_1

    .line 326
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->destroy()V

    .line 327
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    .line 329
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    if-eqz v0, :cond_2

    .line 330
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->destroy()V

    .line 331
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    .line 333
    :cond_2
    sget-boolean v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->USE_GRAVITY_SENSOR:Z

    if-eqz v0, :cond_3

    .line 334
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 336
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeAllMessage()V

    .line 337
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setTransitionOnDestroy()V

    .line 338
    return-void
.end method

.method public onFlingEnd(F)V
    .locals 2
    .param p1, "delta"    # F

    .prologue
    const/4 v1, 0x0

    .line 724
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnscroll:Z

    .line 725
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v0, p1, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setScroll(FZ)V

    .line 726
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->isEnabledQuickScroll()Z

    move-result v0

    if-nez v0, :cond_0

    .line 727
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->hide()V

    .line 729
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->resetFlingEffect()V

    .line 730
    return-void
.end method

.method public onFlingProcess(FF)V
    .locals 6
    .param p1, "delta"    # F
    .param p2, "elastic"    # F

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 690
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 691
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->show()V

    .line 692
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    invoke-virtual {v0, p1, v2, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->update(FFF)V

    .line 693
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mUseQuickScroll:Z

    if-eqz v0, :cond_0

    .line 694
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->updateQuickPopupText()V

    .line 698
    :cond_0
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnscroll:Z

    .line 699
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseQuickSCroll:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOverlayHelp:Z

    if-eqz v0, :cond_1

    .line 700
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v0, :cond_1

    .line 701
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mActive:Z

    if-eqz v0, :cond_1

    .line 702
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->addView()V

    .line 707
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v0, p1, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setScroll(FZ)V

    .line 708
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseScaleDownEndEffect:Z

    if-eqz v0, :cond_4

    .line 709
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3fd3333333333333L    # 0.3

    cmpl-double v0, v0, v2

    if-lez v0, :cond_3

    .line 710
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x3da3d70a    # 0.08f

    mul-float/2addr v1, v2

    sub-float v1, v4, v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->setScaleRate(F)V

    .line 711
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPressObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_2

    .line 712
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPressObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setStartedEndEffect(Z)V

    .line 713
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPressObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->onCancelAnimation()V

    .line 721
    :cond_2
    :goto_0
    return-void

    .line 715
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->getScaleRate()F

    move-result v0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_2

    .line 716
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->setScaleRate(F)V

    goto :goto_0

    .line 720
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    neg-float v1, p2

    invoke-virtual {v0, v1, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->setPitchRate(FZ)V

    goto :goto_0
.end method

.method protected onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 583
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->isSlideShowMode:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mAlbumList:Z

    if-eqz v3, :cond_1

    .line 629
    :cond_0
    :goto_0
    return v1

    .line 585
    :cond_1
    const/16 v3, 0xa8

    if-eq p1, v3, :cond_2

    const/16 v3, 0xa9

    if-eq p1, v3, :cond_2

    const/16 v3, 0x118

    if-eq p1, v3, :cond_2

    const/16 v3, 0x117

    if-ne p1, v3, :cond_4

    :cond_2
    move v0, v2

    .line 587
    .local v0, "isZoomKey":Z
    :goto_1
    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->isScreenLocked()Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_3
    move v1, v2

    .line 588
    goto :goto_0

    .end local v0    # "isZoomKey":Z
    :cond_4
    move v0, v1

    .line 585
    goto :goto_1

    .line 590
    .restart local v0    # "isZoomKey":Z
    :cond_5
    const/16 v3, 0x42

    if-eq p1, v3, :cond_6

    const/16 v3, 0x17

    if-ne p1, v3, :cond_8

    .line 591
    :cond_6
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->isGroupCheckBoxFocused:Z

    if-nez v3, :cond_7

    .line 592
    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    :cond_7
    move v1, v2

    .line 593
    goto :goto_0

    .line 596
    :cond_8
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 610
    :sswitch_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 611
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 612
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->moveTo(I)Z

    move-result v1

    goto :goto_0

    .line 598
    :sswitch_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 599
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 600
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->moveTo(I)Z

    move-result v1

    goto :goto_0

    .line 602
    :sswitch_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 603
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 604
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->moveTo(I)Z

    move-result v1

    goto :goto_0

    .line 618
    :sswitch_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 619
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->moveTo(I)Z

    move-result v1

    .line 620
    .local v1, "shouldConsumeEvent":Z
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->isFocusChanged()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 621
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 624
    .end local v1    # "shouldConsumeEvent":Z
    :sswitch_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 625
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 626
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setFocusable(Z)V

    .line 627
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->moveTo(I)Z

    move-result v1

    goto/16 :goto_0

    .line 596
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_3
        0x15 -> :sswitch_1
        0x16 -> :sswitch_2
        0x3d -> :sswitch_4
    .end sparse-switch
.end method

.method protected onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 673
    const/16 v0, 0xa8

    if-eq p1, v0, :cond_0

    const/16 v0, 0xa9

    if-ne p1, v0, :cond_1

    .line 674
    :cond_0
    const/4 v0, 0x1

    .line 682
    :goto_0
    return v0

    .line 677
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getCount()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->isScreenLocked()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 679
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnKeyListenner:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnKeyListener;

    if-eqz v0, :cond_4

    .line 680
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnKeyListenner:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnKeyListener;

    const/16 v1, 0x80

    invoke-interface {v0, p1, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnKeyListener;->onKeyEvent(II)Z

    move-result v0

    goto :goto_0

    .line 682
    :cond_4
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 634
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->isSlideShowMode:Z

    if-eqz v5, :cond_1

    .line 668
    :cond_0
    :goto_0
    return v3

    .line 636
    :cond_1
    const/16 v5, 0xa8

    if-eq p1, v5, :cond_2

    const/16 v5, 0xa9

    if-eq p1, v5, :cond_2

    const/16 v5, 0x118

    if-eq p1, v5, :cond_2

    const/16 v5, 0x117

    if-ne p1, v5, :cond_3

    :cond_2
    move v3, v4

    .line 638
    goto :goto_0

    .line 641
    :cond_3
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getCount()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->isScreenLocked()Z

    move-result v5

    if-nez v5, :cond_0

    .line 644
    const/16 v5, 0x42

    if-eq p1, v5, :cond_4

    const/16 v5, 0x17

    if-ne p1, v5, :cond_9

    .line 645
    :cond_4
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-ltz v5, :cond_7

    .line 646
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    if-eqz v5, :cond_7

    .line 647
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    shr-int/lit8 v0, v5, 0x10

    .line 648
    .local v0, "groupIndex":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    const v6, 0xffff

    and-int v1, v5, v6

    .line 650
    .local v1, "itemIndex":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v2

    .line 651
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 652
    if-eqz v2, :cond_0

    .line 653
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGroupCheckBoxVisible:Z

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->isGroupCheckBoxFocused:Z

    if-nez v5, :cond_0

    .line 654
    :cond_5
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMode:I

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v5, v6, v2, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->prepareAnimation(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)Z

    move-result v5

    if-eqz v5, :cond_6

    move v3, v4

    .line 656
    goto :goto_0

    .line 658
    :cond_6
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setExpansionMode(Z)V

    .line 659
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->isGroupCheckBoxFocused:Z

    if-eqz v5, :cond_8

    .line 660
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v7

    const/4 v8, -0x1

    invoke-interface {v5, v6, v7, v0, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)Z

    .line 663
    :goto_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v6, 0x4

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {v5, v6, v7, v3, v3}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .end local v0    # "groupIndex":I
    .end local v1    # "itemIndex":I
    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_7
    move v3, v4

    .line 666
    goto/16 :goto_0

    .line 662
    .restart local v0    # "groupIndex":I
    .restart local v1    # "itemIndex":I
    .restart local v2    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_8
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v7

    invoke-interface {v5, v6, v7, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)Z

    goto :goto_1

    .line 668
    .end local v0    # "groupIndex":I
    .end local v1    # "itemIndex":I
    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_9
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v3

    goto/16 :goto_0
.end method

.method protected onMessageExtra(ILjava/lang/Object;III)V
    .locals 0
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    .line 445
    return-void
.end method

.method protected onMoved(II)Z
    .locals 4
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    const/4 v3, 0x1

    .line 557
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->isScreenLocked()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnScaling:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollEnabled:Z

    if-nez v0, :cond_1

    .line 563
    :cond_0
    :goto_0
    return v3

    .line 560
    :cond_1
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0x2d

    if-le v0, v1, :cond_2

    .line 561
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mClickable:Z

    .line 562
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    neg-int v1, p2

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeightViewRatio:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMovement(F)V

    goto :goto_0
.end method

.method protected onPressed(II)Z
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 538
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    if-eqz v0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->exitFromHoverView(Z)V

    .line 541
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->isScreenLocked()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnScaling:Z

    if-eqz v0, :cond_2

    .line 542
    :cond_1
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollEnabled:Z

    .line 552
    :goto_0
    return v1

    .line 545
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 546
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->stop()V

    .line 548
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 549
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mClicked:Z

    .line 550
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mClickable:Z

    .line 551
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollEnabled:Z

    goto :goto_0
.end method

.method protected onReleased(IIII)Z
    .locals 3
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    const/4 v2, 0x1

    .line 573
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->isScreenLocked()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnScaling:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mClicked:Z

    if-eqz v0, :cond_1

    .line 578
    :cond_0
    :goto_0
    return v2

    .line 576
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    neg-int v1, p4

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->startFling(F)V

    .line 577
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->resetFlingEffect()V

    goto :goto_0
.end method

.method protected onScale(Landroid/view/ScaleGestureDetector;)V
    .locals 8
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v7, 0x0

    .line 765
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseScaleCtrl:Z

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->checkIfIncorrectMode()Z

    move-result v5

    if-nez v5, :cond_0

    iget-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnScaling:Z

    if-nez v5, :cond_1

    .line 799
    :cond_0
    :goto_0
    return-void

    .line 767
    :cond_1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v4

    .line 768
    .local v4, "scale":F
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    iget-boolean v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mActive:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->isRunning()Z

    move-result v5

    if-nez v5, :cond_2

    .line 769
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    invoke-virtual {v5, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->setScale(F)V

    goto :goto_0

    .line 772
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    if-eqz v5, :cond_0

    .line 776
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->isEnabledQuickScroll()Z

    move-result v5

    if-nez v5, :cond_0

    .line 780
    :cond_3
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->isMaxLevel()Z

    move-result v5

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->isTimeViewStateConfig()Z

    move-result v5

    if-eqz v5, :cond_4

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMode:I

    const/4 v6, 0x1

    if-eq v5, v6, :cond_6

    .line 781
    :cond_4
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->isIdle()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 782
    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v5, v4, v5

    if-lez v5, :cond_6

    .line 783
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v5

    float-to-int v0, v5

    .line 784
    .local v0, "focusX":I
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v5

    float-to-int v1, v5

    .line 785
    .local v1, "focusY":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v5, v0, v1, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getFocusedObject(IIZ)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    move-result-object v2

    .line 786
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v2, :cond_0

    invoke-virtual {p0, p1, v2, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->checkDisablePinchZoom(Landroid/view/ScaleGestureDetector;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;II)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 787
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v5, v6, v2, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->prepareAnimation(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)Z

    goto :goto_0

    .line 792
    .end local v0    # "focusX":I
    .end local v1    # "focusY":I
    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_5
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-virtual {v5, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->setScale(F)I

    move-result v3

    .line 793
    .local v3, "result":I
    if-eqz v3, :cond_0

    .line 798
    .end local v3    # "result":I
    :cond_6
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mScale:F

    mul-float/2addr v6, v4

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->doScale(FF)Z

    goto/16 :goto_0
.end method

.method protected onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 7
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 803
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mClicked:Z

    .line 804
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mClickable:Z

    .line 805
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollEnabled:Z

    .line 806
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->resetFlingEffect()V

    .line 807
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    if-eqz v4, :cond_0

    .line 808
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-virtual {v4, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->setEnabled(Z)V

    .line 810
    :cond_0
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseScaleCtrl:Z

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->checkIfIncorrectMode()Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_1
    move v2, v3

    .line 826
    :cond_2
    :goto_0
    return v2

    .line 813
    :cond_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->isIdle()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 816
    :cond_4
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v0

    .line 817
    .local v0, "focusX":F
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v1

    .line 819
    .local v1, "focusY":F
    const-string v4, "GlComposeView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ScaleBegin"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->isRunning()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 820
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->isRunning()Z

    move-result v4

    if-nez v4, :cond_2

    .line 822
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    float-to-int v5, v0

    float-to-int v6, v1

    invoke-virtual {v4, v5, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->prepareScale(II)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 823
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnScaling:Z

    move v2, v3

    .line 824
    goto :goto_0
.end method

.method protected onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 6
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v5, 0x0

    .line 831
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    if-eqz v2, :cond_0

    .line 832
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->hideNow()V

    .line 833
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->disableQuickScroll()V

    .line 835
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    if-eqz v2, :cond_1

    .line 836
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->setEnabled(Z)V

    .line 838
    :cond_1
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseScaleCtrl:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->checkIfIncorrectMode()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 839
    :cond_2
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnScaling:Z

    .line 872
    :goto_0
    return-void

    .line 842
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mActive:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->isRunning()Z

    move-result v2

    if-nez v2, :cond_4

    .line 843
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->requestComplete()V

    .line 844
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnScaling:Z

    goto :goto_0

    .line 847
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->isReady()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->isActive()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 848
    :cond_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->requestComplete()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 849
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnScaling:Z

    goto :goto_0

    .line 853
    :cond_6
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    iget v0, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mScale:F

    .line 856
    .local v0, "fromScale":F
    sget v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->SCALE_UPPER_CRIT:F

    cmpl-float v2, v0, v2

    if-lez v2, :cond_7

    .line 857
    sget v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->SCALE_UPPER:F

    .line 863
    .local v1, "toScale":F
    :goto_1
    const-string v2, "GlComposeView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ScaleEnd fromScale = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", toScale = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", cur = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCurrent:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 864
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    invoke-virtual {v2, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->isAnimationAvailable(FF)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 865
    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->animateScale(FF)V

    goto :goto_0

    .line 858
    .end local v1    # "toScale":F
    :cond_7
    sget v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->SCALE_LOWER_CRIT:F

    cmpg-float v2, v0, v2

    if-gez v2, :cond_8

    .line 859
    sget v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->SCALE_LOWER:F

    .restart local v1    # "toScale":F
    goto :goto_1

    .line 861
    .end local v1    # "toScale":F
    :cond_8
    const/high16 v1, 0x3f800000    # 1.0f

    .restart local v1    # "toScale":F
    goto :goto_1

    .line 866
    :cond_9
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->isRunning()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 867
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnScaling:Z

    goto/16 :goto_0

    .line 869
    :cond_a
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->stopScaleAnimation()V

    .line 870
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnScaling:Z

    goto/16 :goto_0
.end method

.method protected onScrolled(IIII)Z
    .locals 3
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "x"    # I
    .param p4, "y"    # I

    .prologue
    const/4 v2, 0x0

    .line 568
    invoke-virtual {p0, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onPressed(II)Z

    move-result v0

    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onMoved(II)Z

    move-result v1

    or-int/2addr v0, v1

    invoke-virtual {p0, v2, v2, v2, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onReleased(IIII)Z

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v4, 0x0

    .line 877
    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v1}, Landroid/hardware/Sensor;->getType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 878
    const v0, 0x3f733333    # 0.95f

    .line 880
    .local v0, "alpha":F
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGravityData:F

    mul-float/2addr v1, v0

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, v0

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGravityData:F

    .line 881
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2, v4, v4, v4}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 883
    .end local v0    # "alpha":F
    :cond_0
    return-void
.end method

.method protected onSetMode(IILjava/lang/Object;)V
    .locals 0
    .param p1, "mode"    # I
    .param p2, "parm"    # I
    .param p3, "obj"    # Ljava/lang/Object;

    .prologue
    .line 534
    return-void
.end method

.method public performAction(IILandroid/os/Bundle;)Z
    .locals 1
    .param p1, "virtualViewId"    # I
    .param p2, "action"    # I
    .param p3, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 2156
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlComposeViewAccessibility:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;

    if-eqz v0, :cond_0

    .line 2157
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlComposeViewAccessibility:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->performAction(IILandroid/os/Bundle;)Z

    move-result v0

    .line 2158
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected playSoundOnClickThumb()V
    .locals 3

    .prologue
    .line 2023
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v0

    .line 2024
    .local v0, "soundUtils":Lcom/sec/samsung/gallery/view/utils/SoundUtils;
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMode:I

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mIsPickMode:Z

    if-nez v1, :cond_1

    .line 2025
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 2030
    :cond_0
    :goto_0
    return-void

    .line 2027
    :cond_1
    if-eqz v0, :cond_0

    .line 2028
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    goto :goto_0
.end method

.method public refreshCheckState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 512
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    if-eqz v0, :cond_0

    .line 513
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 515
    :cond_0
    return-void
.end method

.method public refreshItem(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/4 v2, 0x0

    .line 438
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    if-nez v0, :cond_0

    .line 442
    :goto_0
    return-void

    .line 441
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    goto :goto_0
.end method

.method public refreshSelectionBarState(Z)V
    .locals 8
    .param p1, "useDelay"    # Z

    .prologue
    const/16 v2, 0xc

    const/4 v3, 0x0

    .line 2033
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    if-eqz v0, :cond_0

    .line 2036
    if-eqz p1, :cond_1

    .line 2037
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const-wide/16 v6, 0x226

    move v4, v3

    move v5, v3

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessageDelayed(IIIIJ)V

    .line 2041
    :cond_0
    :goto_0
    return-void

    .line 2039
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v0, v2, v3, v3, v3}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    goto :goto_0
.end method

.method public refreshView(Z)V
    .locals 0
    .param p1, "reload"    # Z

    .prologue
    .line 434
    return-void
.end method

.method protected remainObjectForLayerAnimation()V
    .locals 5

    .prologue
    .line 405
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getTopObjectList()Ljava/util/ArrayList;

    move-result-object v0

    .line 407
    .local v0, "curObjList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/glcore/GlObject;>;"
    if-nez v0, :cond_0

    .line 417
    :goto_0
    return-void

    .line 410
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 411
    .local v2, "objCount":I
    new-array v3, v2, [Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 413
    .local v3, "objList":[Lcom/sec/android/gallery3d/glcore/GlObject;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_1

    .line 414
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/glcore/GlObject;

    aput-object v4, v3, v1

    .line 413
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 416
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v4, v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->addObjectNotToDetach([Lcom/sec/android/gallery3d/glcore/GlObject;)V

    goto :goto_0
.end method

.method public removeQuickHelpView()V
    .locals 1

    .prologue
    .line 2007
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    if-nez v0, :cond_0

    .line 2012
    :goto_0
    return-void

    .line 2011
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->removeView()V

    goto :goto_0
.end method

.method protected resetLayout()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 342
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->resetLayout()V

    .line 344
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->isActive()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 345
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->cancel()V

    .line 346
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnScaling:Z

    .line 349
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mActive:Z

    if-eqz v2, :cond_1

    .line 350
    const-string v2, "GlComposeView"

    const-string v5, "ShrinkAnimation is stopped by layoutChange"

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->requestCompleteForward(Z)V

    .line 353
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getTopObject()Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    move-result-object v0

    .line 354
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v0, :cond_9

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v5, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    invoke-virtual {v2, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getScrollForIndex(I)F

    move-result v2

    :goto_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollable:F

    sub-float v1, v2, v5

    .line 356
    .local v1, "scroll":F
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidthSpace:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeightSpace:F

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getHeight()I

    move-result v8

    invoke-virtual {v2, v5, v6, v7, v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->setDimensionUtil(FFII)V

    .line 357
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidthSpace:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidth:I

    int-to-float v5, v5

    div-float/2addr v2, v5

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidthViewRatio:F

    .line 358
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeightSpace:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeight:I

    int-to-float v5, v5

    div-float/2addr v2, v5

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeightViewRatio:F

    .line 360
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWideMode:Z

    if-nez v2, :cond_a

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v5, v4, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->resetAttributes(IZ)V

    .line 361
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->resetPosition()V

    .line 363
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mIsDisplayedPenSelectionBox:Z

    if-nez v2, :cond_2

    .line 364
    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->applyPreviousScroll(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;F)V

    .line 366
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    invoke-virtual {v2, v3, v5}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setRange(FF)V

    .line 367
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    if-eqz v2, :cond_3

    .line 368
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->exitFromHoverView(Z)V

    .line 369
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->initAttribute()V

    .line 371
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    if-eqz v2, :cond_4

    .line 372
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->exitFromPenSelectionView()V

    .line 373
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->initAttribute()V

    .line 375
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    if-eqz v2, :cond_5

    .line 376
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->exitFromPenSelectionView()V

    .line 377
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->initAttribute()V

    .line 380
    :cond_5
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseQuickSCroll:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    if-eqz v2, :cond_6

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOverlayHelp:Z

    if-eqz v2, :cond_6

    .line 381
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->resetLayout()V

    .line 384
    :cond_6
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->resetScrollBar()V

    .line 386
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    if-eqz v2, :cond_7

    .line 387
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->hideNow()V

    .line 389
    :cond_7
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->IsCheckMode()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 390
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->refreshCheckState()V

    .line 392
    :cond_8
    return-void

    .end local v1    # "scroll":F
    :cond_9
    move v2, v3

    .line 354
    goto/16 :goto_0

    .restart local v1    # "scroll":F
    :cond_a
    move v2, v4

    .line 360
    goto :goto_1
.end method

.method public resetQuickHelpView()V
    .locals 1

    .prologue
    .line 2015
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    if-nez v0, :cond_0

    .line 2020
    :goto_0
    return-void

    .line 2019
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->resetLayout()V

    goto :goto_0
.end method

.method protected resetScrollBar()V
    .locals 7

    .prologue
    .line 518
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    if-nez v0, :cond_0

    .line 524
    :goto_0
    return-void

    .line 521
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setRightPadding(F)V

    .line 522
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidthViewRatio:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeightViewRatio:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidthSpace:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeightSpace:F

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getMarginTop()F

    move-result v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getMarginBtm()F

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->reset(FFFFFF)V

    .line 523
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewScrollbarWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setThinkness(F)V

    goto :goto_0
.end method

.method public resetUpdateQueue()V
    .locals 1

    .prologue
    .line 924
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateQueueWR:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUpdateQueueRD:I

    .line 925
    return-void
.end method

.method public saveCurrentScrollInfo()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v5, 0x0

    .line 1926
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-nez v1, :cond_0

    .line 1927
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iput v5, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mPrevScroll:F

    .line 1928
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iput v6, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mPrevCenterObject:I

    .line 1940
    :goto_0
    return-void

    .line 1932
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidth:I

    int-to-float v2, v2

    div-float/2addr v2, v4

    float-to-int v2, v2

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeight:I

    int-to-float v3, v3

    div-float/2addr v3, v4

    float-to-int v3, v3

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getFocusedObject(IIZ)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    move-result-object v0

    .line 1933
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v0, :cond_1

    .line 1934
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollable:F

    iput v2, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mPrevScroll:F

    .line 1935
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget v2, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    iput v2, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mPrevCenterObject:I

    goto :goto_0

    .line 1937
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iput v5, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mPrevScroll:F

    .line 1938
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iput v6, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mPrevCenterObject:I

    goto :goto_0
.end method

.method public scaleAnimationForSelectionMode()V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 1575
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-nez v1, :cond_1

    .line 1582
    :cond_0
    :goto_0
    return-void

    .line 1578
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidth:I

    int-to-float v2, v2

    div-float/2addr v2, v4

    float-to-int v2, v2

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeight:I

    int-to-float v3, v3

    div-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getFocusedObject(II)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    move-result-object v0

    .line 1579
    .local v0, "object":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v0, :cond_0

    .line 1580
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->scaleAnimationForSelectionMode(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)Z

    goto :goto_0
.end method

.method public setAdapter(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)V
    .locals 3
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    .prologue
    .line 448
    const-string v0, "GlComposeView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAdapter = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", new = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v0, :cond_0

    .line 451
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->setViewListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;)V

    .line 454
    :cond_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    .line 455
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    if-eqz v0, :cond_1

    .line 456
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->setAdapter(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)V

    .line 458
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-nez v0, :cond_2

    .line 462
    :goto_0
    return-void

    .line 461
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->setViewListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;)V

    goto :goto_0
.end method

.method public setClickEnabled(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 508
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mClickEnabled:Z

    .line 509
    return-void
.end method

.method public setInitialCode(I)V
    .locals 0
    .param p1, "code"    # I

    .prologue
    .line 1473
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mInitialCode:I

    .line 1474
    return-void
.end method

.method public setMode(IILjava/lang/Object;)V
    .locals 4
    .param p1, "mode"    # I
    .param p2, "parm"    # I
    .param p3, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 496
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMode:I

    .line 497
    .local v0, "previousMode":I
    if-ne p1, v0, :cond_1

    .line 505
    :cond_0
    :goto_0
    return-void

    .line 500
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onSetMode(IILjava/lang/Object;)V

    .line 501
    invoke-super {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->setMode(IILjava/lang/Object;)V

    .line 502
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 503
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v3, v3, v3}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    goto :goto_0
.end method

.method public setObjectListeners(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;I)V
    .locals 3
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .param p2, "mode"    # I

    .prologue
    .line 1491
    const/4 v2, 0x2

    if-ne p2, v2, :cond_1

    .line 1492
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerGrpCheckBoxClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-virtual {p1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)V

    .line 1493
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerThumbMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    invoke-virtual {p1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 1494
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerGenericMotion:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    invoke-virtual {p1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setGenericMotionListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;)V

    .line 1495
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerPenSelect:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    invoke-virtual {p1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPenSelectListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;)V

    .line 1518
    .end local p1    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_0
    :goto_0
    return-void

    .line 1498
    .restart local p1    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerThumbMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    invoke-virtual {p1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 1499
    if-nez p2, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerThumbClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    :goto_1
    invoke-virtual {p1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)V

    .line 1500
    if-nez p2, :cond_3

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerThumbLongClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

    :goto_2
    invoke-virtual {p1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setLongClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;)V

    .line 1501
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerHover:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

    invoke-virtual {p1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setHoverListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;)V

    .line 1502
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerGenericMotion:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    invoke-virtual {p1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setGenericMotionListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;)V

    .line 1503
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerPenSelect:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    invoke-virtual {p1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPenSelectListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;)V

    .line 1505
    if-nez p2, :cond_0

    move-object v2, p1

    .line 1507
    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget-object v1, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 1508
    .local v1, "expObj":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerExpansionClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)V

    .line 1509
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerThumbMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 1510
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerPenSelect:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPenSelectListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;)V

    .line 1513
    check-cast p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .end local p1    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    iget-object v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 1514
    .local v0, "checkObj":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerThumbClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)V

    .line 1515
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerThumbMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 1516
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerPenSelect:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPenSelectListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;)V

    goto :goto_0

    .line 1499
    .end local v0    # "checkObj":Lcom/sec/android/gallery3d/glcore/GlObject;
    .end local v1    # "expObj":Lcom/sec/android/gallery3d/glcore/GlObject;
    .restart local p1    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerGrpCheckBoxClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    goto :goto_1

    .line 1500
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mListenerGrpCheckBoxLongClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

    goto :goto_2
.end method

.method public setSwitchViewState(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 1477
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnSwitchViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnSwitchViewListener;

    if-nez v0, :cond_1

    .line 1485
    :cond_0
    :goto_0
    return-void

    .line 1479
    :cond_1
    if-nez p1, :cond_2

    .line 1480
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnSwitchViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnSwitchViewListener;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnSwitchViewListener;->onViewSwitchStart()V

    goto :goto_0

    .line 1481
    :cond_2
    const/4 v0, 0x1

    if-ne p1, v0, :cond_3

    .line 1482
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnSwitchViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnSwitchViewListener;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnSwitchViewListener;->onViewSwitchDone()V

    goto :goto_0

    .line 1483
    :cond_3
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 1484
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnSwitchViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnSwitchViewListener;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnSwitchViewListener;->onViewSwitchCancel()V

    goto :goto_0
.end method

.method protected setTransitionOnDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 395
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mTransitionAnimationType:I

    if-ltz v0, :cond_1

    .line 396
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->remainObjectForLayerAnimation()V

    .line 397
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mChildCount:I

    if-lez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getChild(I)Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 398
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getChild(I)Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->remainObjectForLayerAnimation()V

    .line 400
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mTransitionAnimationType:I

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setPendingLayerAnimation(Lcom/sec/android/gallery3d/glcore/GlPendingLayerAnimation;)V

    .line 402
    :cond_1
    return-void
.end method

.method protected startEnlargeAnimation()V
    .locals 2

    .prologue
    .line 1551
    const-string v0, "GlComposeView"

    const-string/jumbo v1, "startEnlargeAnimation"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1552
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-nez v0, :cond_1

    .line 1567
    :cond_0
    :goto_0
    return-void

    .line 1554
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->isReady()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1555
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mToDetailAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;->onAnimationEnd(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    goto :goto_0

    .line 1558
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    if-eqz v0, :cond_3

    .line 1559
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->removeView()V

    .line 1561
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->startAnimation()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1562
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->addFadeOutObjs(Landroid/util/SparseArray;)V

    .line 1563
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->isAutoAnimation()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    if-eqz v0, :cond_0

    .line 1564
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->hideNow()V

    goto :goto_0
.end method

.method protected startShrinkAnimation()V
    .locals 2

    .prologue
    .line 1570
    const-string v0, "GlComposeView"

    const-string/jumbo v1, "startShrinkAnimation"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1571
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getActiveGroup()Landroid/util/SparseArray;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->addFadeInObjs(Landroid/util/SparseArray;)V

    .line 1572
    return-void
.end method

.method public updateBorder(II)V
    .locals 3
    .param p1, "oldIndex"    # I
    .param p2, "newIndex"    # I

    .prologue
    .line 1539
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    if-eqz v0, :cond_0

    .line 1540
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v1, 0x7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 1542
    :cond_0
    return-void
.end method

.method protected updateSize(I)V
    .locals 8
    .param p1, "size"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 931
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->resetCount()V

    .line 932
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->resetCount()V

    .line 933
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->resetPosition()V

    .line 935
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    invoke-virtual {v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setRange(FF)V

    .line 936
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v3, :cond_3

    .line 937
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getCodeForMediaItem(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v0

    .line 940
    .local v0, "initItemIndex":I
    :goto_0
    if-eq v0, v6, :cond_4

    .line 941
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getScrollForIndex(I)F

    move-result v2

    .line 942
    .local v2, "scroll":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getCenteredScroll(F)F

    move-result v2

    .line 943
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 951
    :goto_1
    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mInitialCode:I

    .line 952
    iput-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 953
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setScroll(FZ)V

    .line 954
    if-eq v0, v6, :cond_7

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    if-eqz v3, :cond_7

    .line 955
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getObject(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    move-result-object v1

    .line 956
    .local v1, "object":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v1, :cond_6

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v3, v4, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->prepareAnimation(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 957
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->startShrinkAnimation()V

    .line 966
    .end local v1    # "object":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_0
    :goto_2
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScreenLocked:Z

    if-eqz v3, :cond_1

    .line 967
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScreenLocked:Z

    .line 968
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->unlockRefresh()V

    .line 970
    :cond_1
    if-nez p1, :cond_2

    .line 971
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->freeGlObjects()V

    .line 972
    :cond_2
    return-void

    .line 939
    .end local v0    # "initItemIndex":I
    .end local v2    # "scroll":F
    :cond_3
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mInitialCode:I

    .restart local v0    # "initItemIndex":I
    goto :goto_0

    .line 944
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mPrevCenterObject:I

    if-eq v3, v6, :cond_5

    .line 945
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iput v6, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mPrevCenterObject:I

    .line 946
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mPrevScroll:F

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getValidScroll(F)F

    move-result v2

    .line 947
    .restart local v2    # "scroll":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    goto :goto_1

    .line 949
    .end local v2    # "scroll":F
    :cond_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->getScroll()F

    move-result v2

    .restart local v2    # "scroll":F
    goto :goto_1

    .line 959
    .restart local v1    # "object":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_6
    iput-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    goto :goto_2

    .line 962
    .end local v1    # "object":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_7
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mActive:Z

    if-nez v3, :cond_0

    .line 963
    iput-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    goto :goto_2
.end method

.method public updateTitleBorder(II)V
    .locals 3
    .param p1, "oldIndex"    # I
    .param p2, "newIndex"    # I

    .prologue
    .line 1545
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    if-eqz v0, :cond_0

    .line 1546
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 1548
    :cond_0
    return-void
.end method
