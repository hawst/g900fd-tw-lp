.class public Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;
.super Ljava/lang/Object;
.source "SlidingAnimationInterface.java"


# static fields
.field static factor:I

.field static maxDuration:I

.field static minDuration:I

.field static final newInstance:Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;


# instance fields
.field slideDuration:I

.field velocityX:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    const/16 v0, 0xfa

    sput v0, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->maxDuration:I

    .line 7
    const/16 v0, 0x64

    sput v0, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->minDuration:I

    .line 8
    const v0, 0xf4240

    sput v0, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->factor:I

    .line 9
    new-instance v0, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->newInstance:Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->velocityX:F

    .line 5
    const/16 v0, 0xfa

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->slideDuration:I

    return-void
.end method

.method static final create()Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->newInstance:Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;

    return-object v0
.end method


# virtual methods
.method public checkSpeedOfFling()V
    .locals 2

    .prologue
    .line 35
    sget v0, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->factor:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->velocityX:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->slideDuration:I

    .line 37
    iget v0, p0, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->slideDuration:I

    sget v1, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->maxDuration:I

    if-le v0, v1, :cond_1

    .line 38
    sget v0, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->maxDuration:I

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->slideDuration:I

    .line 42
    :cond_0
    :goto_0
    return-void

    .line 39
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->slideDuration:I

    sget v1, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->minDuration:I

    if-ge v0, v1, :cond_0

    .line 40
    sget v0, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->minDuration:I

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->slideDuration:I

    goto :goto_0
.end method

.method public getProgress(FFF)F
    .locals 4
    .param p1, "fromX"    # F
    .param p2, "toX"    # F
    .param p3, "progress"    # F

    .prologue
    .line 23
    const/4 v0, 0x0

    .line 25
    .local v0, "nextProgress":F
    sub-float v1, p2, p1

    const v2, 0x40490fdc

    mul-float/2addr v2, p3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v2, v2

    mul-float/2addr v1, v2

    add-float v0, p1, v1

    .line 27
    return v0
.end method

.method public getSlideAnimationTime(I)I
    .locals 1
    .param p1, "originalTime"    # I

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->slideDuration:I

    return v0
.end method

.method public setFlingVelocity(F)V
    .locals 0
    .param p1, "vX"    # F

    .prologue
    .line 19
    iput p1, p0, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->velocityX:F

    .line 20
    return-void
.end method
