.class public Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;
.super Lcom/sec/samsung/gallery/view/common/DetailsDialog;
.source "DetailsDialogForCamera.java"


# static fields
.field public static final BASE_DETAIL_INDEX:[I

.field public static final BASE_DETAIL_RESOURCE_ID:[I

.field private static final GRAY_B_WEIGHT:F = 0.072169f

.field private static final GRAY_G_WEIGHT:F = 0.71516f

.field private static final GRAY_R_WEIGHT:F = 0.212671f

.field private static final HISTOGRAM_HEIGHT:I = 0x64

.field private static final HISTOGRAM_LEVEL_COUNT:I = 0x100

.field private static final HISTOGRAM_WIDTH:I = 0x100


# instance fields
.field private mDetailsView:Landroid/view/ViewGroup;

.field private mHistogram:Landroid/graphics/Bitmap;

.field private mLoadBitmapTask:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mThumbnail:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x5

    .line 40
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->BASE_DETAIL_INDEX:[I

    .line 48
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->BASE_DETAIL_RESOURCE_ID:[I

    return-void

    .line 40
    :array_0
    .array-data 4
        0x6c
        0x6d
        0x6a
        0x6b
        0x66
    .end array-data

    .line 48
    :array_1
    .array-data 4
        0x7f0f00d3
        0x7f0f00d4
        0x7f0f00d5
        0x7f0f00d6
        0x7f0f00d7
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "media"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0, p1, v1}, Lcom/sec/samsung/gallery/view/common/DetailsDialog;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 65
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mLoadBitmapTask:Lcom/sec/android/gallery3d/util/Future;

    .line 66
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mThumbnail:Landroid/graphics/Bitmap;

    .line 67
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mHistogram:Landroid/graphics/Bitmap;

    .line 68
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mDetailsView:Landroid/view/ViewGroup;

    .line 72
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 74
    new-instance v0, Lcom/sec/samsung/gallery/controller/DetailModel;

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/sec/samsung/gallery/controller/DetailModel;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaDetails;)V

    .line 75
    .local v0, "model":Lcom/sec/samsung/gallery/controller/DetailModel;
    sget-object v1, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->BASE_DETAIL_INDEX:[I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/controller/DetailModel;->addIgnores([I)V

    .line 76
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/controller/DetailModel;->toStringList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->setContensList(Ljava/util/ArrayList;)V

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->createHistogramImage(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;
    .param p1, "x1"    # Landroid/graphics/Bitmap;
    .param p2, "x2"    # Landroid/graphics/Bitmap;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->onThumbnailRequested(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private createHistogramImage(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 117
    const/16 v0, 0x100

    const/16 v1, 0x64

    const/4 v2, -0x1

    invoke-static {p1, v0, v1, v2}, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->createHistogramImage(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static createHistogramImage(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;
    .locals 28
    .param p0, "bmp"    # Landroid/graphics/Bitmap;
    .param p1, "targetW"    # I
    .param p2, "targetH"    # I
    .param p3, "drawColor"    # I

    .prologue
    .line 121
    if-nez p0, :cond_0

    const/16 v17, 0x0

    .line 157
    :goto_0
    return-object v17

    .line 123
    :cond_0
    const/16 v2, 0x100

    new-array v13, v2, [I

    .line 124
    .local v13, "data":[I
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 125
    .local v5, "width":I
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    .line 126
    .local v9, "height":I
    mul-int v2, v5, v9

    new-array v3, v2, [I

    .line 127
    .local v3, "pixels":[I
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move v8, v5

    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 128
    const/16 v22, 0x0

    .line 129
    .local v22, "maxCount":I
    move-object v10, v3

    .local v10, "arr$":[I
    array-length v0, v10

    move/from16 v19, v0

    .local v19, "len$":I
    const/16 v18, 0x0

    .local v18, "i$":I
    :goto_1
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_1

    aget v24, v10, v18

    .line 130
    .local v24, "p":I
    move/from16 v0, v24

    and-int/lit16 v0, v0, 0xff

    move/from16 v26, v0

    .line 131
    .local v26, "r":I
    move/from16 v0, v24

    and-int/lit16 v14, v0, 0xff

    .line 132
    .local v14, "g":I
    move/from16 v0, v24

    and-int/lit16 v11, v0, 0xff

    .line 133
    .local v11, "b":I
    const v2, 0x3e59c66d

    move/from16 v0, v26

    int-to-float v4, v0

    mul-float/2addr v2, v4

    const v4, 0x3f3714ba

    int-to-float v6, v14

    mul-float/2addr v4, v6

    add-float/2addr v2, v4

    const v4, 0x3d93cd57

    int-to-float v6, v11

    mul-float/2addr v4, v6

    add-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v15

    .line 134
    .local v15, "gray":I
    rem-int/lit16 v0, v15, 0x100

    move/from16 v20, v0

    .line 135
    .local v20, "level":I
    aget v2, v13, v20

    add-int/lit8 v2, v2, 0x1

    aput v2, v13, v20

    .line 136
    aget v2, v13, v20

    move/from16 v0, v22

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v22

    .line 129
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 139
    .end local v11    # "b":I
    .end local v14    # "g":I
    .end local v15    # "gray":I
    .end local v20    # "level":I
    .end local v24    # "p":I
    .end local v26    # "r":I
    :cond_1
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 140
    .local v17, "hisBmp":Landroid/graphics/Bitmap;
    new-instance v12, Landroid/graphics/Canvas;

    move-object/from16 v0, v17

    invoke-direct {v12, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 141
    .local v12, "canvas":Landroid/graphics/Canvas;
    const/16 v2, 0x400

    new-array v0, v2, [F

    move-object/from16 v21, v0

    .line 142
    .local v21, "levelPoints":[F
    move/from16 v0, p1

    int-to-float v2, v0

    const/high16 v4, 0x43800000    # 256.0f

    div-float v27, v2, v4

    .line 143
    .local v27, "strokeWeight":F
    const/16 v20, 0x0

    .restart local v20    # "level":I
    :goto_2
    const/16 v2, 0x100

    move/from16 v0, v20

    if-ge v0, v2, :cond_2

    .line 144
    aget v2, v13, v20

    mul-int v2, v2, p2

    div-int v16, v2, v22

    .line 145
    .local v16, "h":I
    mul-int/lit8 v23, v20, 0x4

    .line 146
    .local v23, "offset":I
    move/from16 v0, v20

    int-to-float v2, v0

    mul-float v2, v2, v27

    aput v2, v21, v23

    .line 147
    add-int/lit8 v2, v23, 0x1

    move/from16 v0, p2

    int-to-float v4, v0

    aput v4, v21, v2

    .line 148
    add-int/lit8 v2, v23, 0x2

    move/from16 v0, v20

    int-to-float v4, v0

    mul-float v4, v4, v27

    aput v4, v21, v2

    .line 149
    add-int/lit8 v2, v23, 0x3

    sub-int v4, p2, v16

    int-to-float v4, v4

    aput v4, v21, v2

    .line 143
    add-int/lit8 v20, v20, 0x1

    goto :goto_2

    .line 152
    .end local v16    # "h":I
    .end local v23    # "offset":I
    :cond_2
    new-instance v25, Landroid/graphics/Paint;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/Paint;-><init>()V

    .line 153
    .local v25, "paint":Landroid/graphics/Paint;
    move-object/from16 v0, v25

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 154
    move-object/from16 v0, v25

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 155
    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v12, v0, v1}, Landroid/graphics/Canvas;->drawLines([FLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method private onDialogForAdvance()Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/16 v7, 0x400

    .line 209
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mCtx:Landroid/content/Context;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 211
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030050

    invoke-virtual {v3, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    .line 212
    .local v2, "detailListView":Landroid/widget/ListView;
    const v4, 0x7f03004b

    invoke-virtual {v3, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mDetailsView:Landroid/view/ViewGroup;

    .line 213
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mDetailsView:Landroid/view/ViewGroup;

    invoke-direct {p0, v2, v4}, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->setupView(Landroid/widget/ListView;Landroid/view/ViewGroup;)V

    .line 215
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mCtx:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 216
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0e004b

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x104000a

    new-instance v6, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera$2;

    invoke-direct {v6, p0}, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera$2;-><init>(Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 224
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 225
    .local v1, "detailDialog":Landroid/app/Dialog;
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    const v5, 0x10100

    invoke-virtual {v4, v5}, Landroid/view/Window;->addFlags(I)V

    .line 228
    invoke-virtual {v1}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFullScreen(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 229
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4, v7, v7}, Landroid/view/Window;->setFlags(II)V

    .line 232
    :cond_0
    return-object v1
.end method

.method private onDialogForCamera(Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 6
    .param p1, "detailDialog"    # Landroid/app/Dialog;

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x400

    .line 192
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 194
    .local v1, "dlgWindow":Landroid/view/Window;
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/view/Window;->requestFeature(I)Z

    .line 195
    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 197
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mCtx:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 199
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f03004c

    invoke-virtual {v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mDetailsView:Landroid/view/ViewGroup;

    .line 200
    const v3, 0x7f03004d

    invoke-virtual {v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 201
    .local v0, "baseInfoView":Landroid/view/ViewGroup;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mDetailsView:Landroid/view/ViewGroup;

    invoke-direct {p0, v3, v0}, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->setupView(Landroid/view/ViewGroup;Landroid/view/ViewGroup;)V

    .line 202
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mDetailsView:Landroid/view/ViewGroup;

    invoke-virtual {p1, v3}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 203
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v3}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 205
    return-object p1
.end method

.method private onThumbnailRequested(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "thumb"    # Landroid/graphics/Bitmap;
    .param p2, "histogram"    # Landroid/graphics/Bitmap;

    .prologue
    const v3, 0x7f0f00d1

    const v2, 0x7f0f00cf

    const/16 v1, 0x8

    .line 161
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mLoadBitmapTask:Lcom/sec/android/gallery3d/util/Future;

    .line 162
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mThumbnail:Landroid/graphics/Bitmap;

    .line 163
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mHistogram:Landroid/graphics/Bitmap;

    .line 164
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mDetailsView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 166
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mDetailsView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 171
    :goto_0
    return-void

    .line 168
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mDetailsView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 169
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mDetailsView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public static setupBaseInfoView(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/sec/android/gallery3d/data/MediaDetails;II)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/ViewGroup;
    .param p2, "details"    # Lcom/sec/android/gallery3d/data/MediaDetails;
    .param p3, "detailKey"    # I
    .param p4, "viewId"    # I

    .prologue
    const/4 v3, 0x0

    .line 290
    invoke-virtual {p2, p3}, Lcom/sec/android/gallery3d/data/MediaDetails;->getDetail(I)Ljava/lang/Object;

    move-result-object v0

    .line 291
    .local v0, "detailValue":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 292
    invoke-static {p0, p3, v0}, Lcom/sec/samsung/gallery/controller/DetailModel;->getDetailValue(Landroid/content/Context;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 293
    .local v1, "value":Ljava/lang/String;
    const/16 v2, 0x6a

    if-ne p3, v2, :cond_2

    .line 294
    check-cast v0, Ljava/lang/String;

    .end local v0    # "detailValue":Ljava/lang/Object;
    const-string v2, "--"

    if-ne v0, v2, :cond_1

    .line 304
    .end local v1    # "value":Ljava/lang/String;
    :cond_0
    :goto_0
    return v3

    .line 298
    .restart local v1    # "value":Ljava/lang/String;
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "F "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 301
    :cond_2
    invoke-virtual {p1, p4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 302
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    move v3, v2

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1
.end method

.method private setupView(Landroid/view/ViewGroup;Landroid/view/ViewGroup;)V
    .locals 9
    .param p1, "view"    # Landroid/view/ViewGroup;
    .param p2, "baseInfoView"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v8, 0x65

    .line 236
    const v5, 0x7f0f00db

    invoke-virtual {p1, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 237
    .local v1, "detailsList":Landroid/widget/ListView;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 238
    .local v0, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    const/4 v2, 0x0

    .line 239
    .local v2, "hasBaseInfo":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    sget-object v5, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->BASE_DETAIL_INDEX:[I

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 240
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mCtx:Landroid/content/Context;

    sget-object v6, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->BASE_DETAIL_INDEX:[I

    aget v6, v6, v3

    sget-object v7, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->BASE_DETAIL_RESOURCE_ID:[I

    aget v7, v7, v3

    invoke-static {v5, p2, v0, v6, v7}, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->setupBaseInfoView(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/sec/android/gallery3d/data/MediaDetails;II)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 241
    const/4 v2, 0x1

    .line 239
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 245
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mCtx:Landroid/content/Context;

    invoke-virtual {v0, v8}, Lcom/sec/android/gallery3d/data/MediaDetails;->getDetail(I)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v5, v8, v6}, Lcom/sec/samsung/gallery/controller/DetailModel;->getDetailValue(Landroid/content/Context;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 246
    .local v4, "modelValue":Ljava/lang/String;
    const v5, 0x7f0f00d0

    invoke-virtual {p1, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    if-eqz v2, :cond_2

    .line 248
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, p2, v5, v6}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 250
    :cond_2
    const v5, 0x7f03004f

    invoke-virtual {p0, v1, v5}, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->initDetailList(Landroid/widget/ListView;I)V

    .line 252
    const v5, 0x7f0f00d9

    invoke-virtual {p1, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    new-instance v6, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera$3;

    invoke-direct {v6, p0}, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera$3;-><init>(Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 258
    return-void
.end method

.method private setupView(Landroid/widget/ListView;Landroid/view/ViewGroup;)V
    .locals 12
    .param p1, "listView"    # Landroid/widget/ListView;
    .param p2, "baseInfoView"    # Landroid/view/ViewGroup;

    .prologue
    const v11, 0x7f0f00d0

    const v10, 0x7f0e00ff

    const/16 v9, 0x65

    const/16 v8, 0x8

    .line 261
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v1

    .line 262
    .local v1, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    move-object v0, p1

    .line 264
    .local v0, "detailListView":Landroid/widget/ListView;
    const/4 v2, 0x0

    .line 265
    .local v2, "hasBaseInfo":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    sget-object v5, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->BASE_DETAIL_INDEX:[I

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 266
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mCtx:Landroid/content/Context;

    sget-object v6, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->BASE_DETAIL_INDEX:[I

    aget v6, v6, v3

    sget-object v7, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->BASE_DETAIL_RESOURCE_ID:[I

    aget v7, v7, v3

    invoke-static {v5, p2, v1, v6, v7}, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->setupBaseInfoView(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/sec/android/gallery3d/data/MediaDetails;II)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 267
    const/4 v2, 0x1

    .line 265
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 271
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mCtx:Landroid/content/Context;

    invoke-virtual {v1, v9}, Lcom/sec/android/gallery3d/data/MediaDetails;->getDetail(I)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v5, v9, v6}, Lcom/sec/samsung/gallery/controller/DetailModel;->getDetailValue(Landroid/content/Context;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 272
    .local v4, "modelValue":Ljava/lang/String;
    if-nez v4, :cond_2

    .line 273
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mCtx:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 275
    :cond_2
    invoke-virtual {p2, v11}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mCtx:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 278
    invoke-virtual {p2, v11}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 281
    :cond_3
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v0, p2, v5, v6}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 283
    if-nez v2, :cond_4

    .line 284
    const v5, 0x7f0f00d2

    invoke-virtual {p2, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 286
    :cond_4
    const v5, 0x7f03004f

    invoke-virtual {p0, v0, v5}, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->initDetailList(Landroid/widget/ListView;I)V

    .line 287
    return-void
.end method


# virtual methods
.method protected onCreateDialog()Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 182
    new-instance v0, Landroid/app/Dialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mCtx:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 183
    .local v0, "detailDialog":Landroid/app/Dialog;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 184
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->onDialogForCamera(Landroid/app/Dialog;)Landroid/app/Dialog;

    move-result-object v0

    .line 188
    :goto_0
    return-object v0

    .line 186
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->onDialogForAdvance()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method protected onDialogDismiss()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mLoadBitmapTask:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mLoadBitmapTask:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 178
    :cond_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->onDialogDismiss()V

    .line 179
    return-void
.end method

.method public showDialog()V
    .locals 3

    .prologue
    .line 80
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->showDialog()V

    .line 81
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mCtx:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v1

    new-instance v2, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera$1;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera$1;-><init>(Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;->mLoadBitmapTask:Lcom/sec/android/gallery3d/util/Future;

    .line 114
    return-void
.end method
