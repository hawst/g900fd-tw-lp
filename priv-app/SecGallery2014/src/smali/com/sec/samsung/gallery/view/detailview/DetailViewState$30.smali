.class Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;
.super Landroid/content/BroadcastReceiver;
.source "DetailViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0

    .prologue
    .line 5585
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 5588
    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$5000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "EAM Receive"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5589
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 5591
    .local v0, "intentAction":Ljava/lang/String;
    const-string v1, "android.intent.action.delete"

    if-ne v0, v1, :cond_5

    .line 5592
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseUndoDeletion:Z

    if-eqz v1, :cond_1

    .line 5593
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->startDelete()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$17900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 5624
    :cond_0
    :goto_0
    return-void

    .line 5595
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$18000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;

    if-eqz v1, :cond_2

    .line 5596
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$18100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->showDeleteDialog()V

    goto :goto_0

    .line 5597
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$18200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;

    if-eqz v1, :cond_4

    .line 5598
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$12800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    if-nez v1, :cond_3

    .line 5599
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$18300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0113

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 5601
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$18400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->showDeleteDialog()V

    goto :goto_0

    .line 5603
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$18500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5604
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$18600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;->showDeleteDialog()V

    goto :goto_0

    .line 5608
    :cond_5
    const-string v1, "android.intent.action.slideshow"

    if-ne v0, v1, :cond_0

    .line 5609
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideSettingPopup:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$18700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isExternalDisplayAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 5610
    :cond_6
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$18800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 5611
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$19000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v1

    new-instance v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$18900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 5612
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$9600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_SLIDESHOW_SETTING:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->setShowBarMode(I)V

    .line 5613
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$9200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "SLIDE_SHOW_SETTING_VIEW"

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 5614
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    new-instance v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    const-string v3, "SLIDE_SHOW_SETTING_VIEW"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$19100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;-><init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$14002(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    .line 5615
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$14000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 5616
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$14200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v1

    invoke-interface {v1, v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->setSlideShowMode(Z)V

    .line 5621
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$12800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    if-lez v1, :cond_0

    .line 5622
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$14400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$14300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/content/BroadcastReceiver;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->unregisterEAM(Landroid/content/BroadcastReceiver;)V

    goto/16 :goto_0

    .line 5618
    :cond_7
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$9200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "PREPARE_SLIDE_SHOW_DATA"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$19200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1
.end method
