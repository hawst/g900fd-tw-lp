.class public Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;
.super Ljava/lang/Object;
.source "GlSlideshowPerspective.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PerspectiveObjPositionInfo"
.end annotation


# instance fields
.field public mDim:F

.field public mRoll:F

.field public mX:F

.field public mY:F

.field public mZ:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->this$0:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mX:F

    .line 36
    iput v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mY:F

    .line 37
    iput v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mZ:F

    .line 38
    iput v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mRoll:F

    .line 39
    iput v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mDim:F

    .line 40
    return-void
.end method

.method public constructor <init>(Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;FFFFF)V
    .locals 0
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "z"    # F
    .param p5, "roll"    # F
    .param p6, "dim"    # F

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->this$0:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput p2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mX:F

    .line 44
    iput p3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mY:F

    .line 45
    iput p4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mZ:F

    .line 46
    iput p5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mRoll:F

    .line 47
    iput p6, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mDim:F

    .line 48
    return-void
.end method
