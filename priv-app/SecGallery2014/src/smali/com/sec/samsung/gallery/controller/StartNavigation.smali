.class public Lcom/sec/samsung/gallery/controller/StartNavigation;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "StartNavigation.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/sec/samsung/gallery/controller/StartNavigation;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/StartNavigation;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method private getLastLocation(Landroid/content/Context;)Landroid/location/Location;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 73
    const/4 v1, 0x0

    .line 74
    .local v1, "location":Landroid/location/Location;
    const-string v6, "location"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/LocationManager;

    .line 76
    .local v2, "locationManager":Landroid/location/LocationManager;
    invoke-virtual {v2}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v4

    .line 77
    .local v4, "providers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v4, :cond_0

    .line 85
    :goto_0
    return-object v5

    .line 79
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-ge v0, v6, :cond_1

    .line 80
    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 81
    .local v3, "provider":Ljava/lang/String;
    if-eqz v3, :cond_2

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    .line 82
    :goto_2
    if-eqz v1, :cond_3

    .end local v3    # "provider":Ljava/lang/String;
    :cond_1
    move-object v5, v1

    .line 85
    goto :goto_0

    .restart local v3    # "provider":Ljava/lang/String;
    :cond_2
    move-object v1, v5

    .line 81
    goto :goto_2

    .line 79
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private showOnMap(Landroid/content/Context;DD)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "daddr_latitude"    # D
    .param p4, "daddr_longitude"    # D

    .prologue
    .line 96
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p4, p5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    .line 99
    .local v0, "params":[Ljava/lang/Object;
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "SHOW_ON_MAP"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 100
    return-void
.end method

.method private static startAutoNavi(Landroid/content/Context;DD)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 103
    const-string v3, "NAVI:%s,%s,"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 104
    .local v2, "url":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.autonavi.xmgd.action.NAVIGATOR"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 105
    .local v1, "mapsIntent":Landroid/content/Intent;
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 107
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    :goto_0
    return-void

    .line 108
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v3, Lcom/sec/samsung/gallery/controller/StartNavigation;->TAG:Ljava/lang/String;

    const-string v4, "Activity Not Found"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private static startAutoNaviChn(Landroid/content/Context;DDDD)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "saddr_latitude"    # D
    .param p3, "saddr_longitude"    # D
    .param p5, "daddr_latitude"    # D
    .param p7, "daddr_longitude"    # D

    .prologue
    .line 115
    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v3, "http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {p5, p6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-static {p7, p8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 117
    .local v1, "url":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 118
    .local v0, "mapsIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 119
    return-void
.end method

.method private static startGoogleMaps(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 89
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 92
    .local v0, "params":[Ljava/lang/Object;
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-static {p0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "START_GOOGLE_MAPS"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 93
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 23
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 30
    invoke-interface/range {p1 .. p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Object;

    move-object v12, v2

    check-cast v12, [Ljava/lang/Object;

    .line 31
    .local v12, "params":[Ljava/lang/Object;
    const/4 v2, 0x0

    aget-object v3, v12, v2

    check-cast v3, Landroid/content/Context;

    .line 32
    .local v3, "context":Landroid/content/Context;
    const/4 v2, 0x1

    aget-object v2, v12, v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    .line 33
    .local v4, "daddr_latitude":D
    const/4 v2, 0x2

    aget-object v2, v12, v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    .line 35
    .local v6, "daddr_longitude":D
    const/4 v8, 0x0

    .line 36
    .local v8, "daddr_name":Ljava/lang/String;
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaiduPositioning:Z

    if-eqz v2, :cond_0

    array-length v2, v12

    const/16 v19, 0x3

    move/from16 v0, v19

    if-le v2, v0, :cond_0

    .line 37
    const/4 v2, 0x3

    aget-object v8, v12, v2

    .end local v8    # "daddr_name":Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    .line 42
    .restart local v8    # "daddr_name":Ljava/lang/String;
    :cond_0
    :try_start_0
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/samsung/gallery/controller/StartNavigation;->getLastLocation(Landroid/content/Context;)Landroid/location/Location;

    move-result-object v10

    .line 43
    .local v10, "location":Landroid/location/Location;
    const-wide/16 v14, 0x0

    .line 44
    .local v14, "saddr_latitude":D
    const-wide/16 v16, 0x0

    .line 45
    .local v16, "saddr_longitude":D
    if-eqz v10, :cond_1

    .line 46
    invoke-virtual {v10}, Landroid/location/Location;->getLatitude()D

    move-result-wide v14

    .line 47
    invoke-virtual {v10}, Landroid/location/Location;->getLongitude()D

    move-result-wide v16

    .line 50
    :cond_1
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v2, :cond_2

    .line 51
    invoke-static {v3, v4, v5, v6, v7}, Lcom/sec/samsung/gallery/controller/StartNavigation;->startAutoNavi(Landroid/content/Context;DD)V

    .line 70
    .end local v10    # "location":Landroid/location/Location;
    .end local v14    # "saddr_latitude":D
    .end local v16    # "saddr_longitude":D
    :goto_0
    return-void

    .line 53
    .restart local v10    # "location":Landroid/location/Location;
    .restart local v14    # "saddr_latitude":D
    .restart local v16    # "saddr_longitude":D
    :cond_2
    move-wide/from16 v0, v16

    invoke-static {v4, v5, v0, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 54
    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v19, "http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f"

    const/16 v20, 0x4

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    invoke-static/range {v16 .. v17}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x2

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x3

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v22

    aput-object v22, v20, v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v2, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 56
    .local v13, "uri":Ljava/lang/String;
    invoke-static {v3, v13}, Lcom/sec/samsung/gallery/controller/StartNavigation;->startGoogleMaps(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 61
    .end local v10    # "location":Landroid/location/Location;
    .end local v13    # "uri":Ljava/lang/String;
    .end local v14    # "saddr_latitude":D
    .end local v16    # "saddr_longitude":D
    :catch_0
    move-exception v9

    .line 62
    .local v9, "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/sec/samsung/gallery/controller/StartNavigation;->TAG:Ljava/lang/String;

    const-string v19, "GMM activity not found!"

    move-object/from16 v0, v19

    invoke-static {v2, v0, v9}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 63
    const-string v2, "geo:%f,%f"

    invoke-static {v2, v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->formatLatitudeLongitude(Ljava/lang/String;DD)Ljava/lang/String;

    move-result-object v18

    .line 65
    .local v18, "url":Ljava/lang/String;
    if-eqz v8, :cond_3

    .line 66
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v19, "("

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v19, ")"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 67
    :cond_3
    new-instance v11, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static/range {v18 .. v18}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v11, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 68
    .local v11, "mapsIntent":Landroid/content/Intent;
    invoke-virtual {v3, v11}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .end local v9    # "e":Landroid/content/ActivityNotFoundException;
    .end local v11    # "mapsIntent":Landroid/content/Intent;
    .end local v18    # "url":Ljava/lang/String;
    .restart local v10    # "location":Landroid/location/Location;
    .restart local v14    # "saddr_latitude":D
    .restart local v16    # "saddr_longitude":D
    :cond_4
    move-object/from16 v2, p0

    .line 58
    :try_start_1
    invoke-direct/range {v2 .. v7}, Lcom/sec/samsung/gallery/controller/StartNavigation;->showOnMap(Landroid/content/Context;DD)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
