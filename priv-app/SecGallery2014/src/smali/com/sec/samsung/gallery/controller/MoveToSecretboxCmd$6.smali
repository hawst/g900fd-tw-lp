.class Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$6;
.super Ljava/lang/Object;
.source "MoveToSecretboxCmd.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->showRenameDialog(Ljava/io/File;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

.field final synthetic val$extention:Ljava/lang/String;

.field final synthetic val$operationId:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 474
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$6;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    iput-object p2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$6;->val$extention:Ljava/lang/String;

    iput p3, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$6;->val$operationId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 7
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    .line 477
    move-object v6, p2

    check-cast v6, Lcom/sec/samsung/gallery/core/Event;

    .line 478
    .local v6, "ev":Lcom/sec/samsung/gallery/core/Event;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$6;->val$extention:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 479
    .local v5, "newName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$6;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    iget v1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$6;->val$operationId:I

    const/4 v4, 0x0

    move v3, v2

    # invokes: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->handleMultipleFileOperation(IIILjava/util/ArrayList;Ljava/lang/String;)V
    invoke-static/range {v0 .. v5}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$500(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;IIILjava/util/ArrayList;Ljava/lang/String;)V

    .line 480
    return-void
.end method
