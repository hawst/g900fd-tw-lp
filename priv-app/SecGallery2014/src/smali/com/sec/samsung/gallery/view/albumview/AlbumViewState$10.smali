.class Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$10;
.super Ljava/lang/Object;
.source "AlbumViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 0

    .prologue
    .line 624
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$10;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKeyEvent(II)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "keyAction"    # I

    .prologue
    const/16 v6, 0x80

    const/16 v5, 0x42

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 627
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$10;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v3

    sget-object v4, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_NORMAL:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v3, v4, :cond_1

    if-ne p1, v5, :cond_1

    if-ne p2, v6, :cond_1

    .line 643
    :cond_0
    :goto_0
    return v2

    .line 629
    :cond_1
    const/16 v3, 0x70

    if-ne p1, v3, :cond_2

    if-nez p2, :cond_2

    .line 630
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$10;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$10;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v3

    if-lez v3, :cond_0

    .line 632
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$10;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->showDeleteDialog()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$5100(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    move v2, v1

    .line 633
    goto :goto_0

    .line 635
    :cond_2
    if-eq p1, v5, :cond_3

    const/16 v3, 0x17

    if-ne p1, v3, :cond_0

    :cond_3
    if-ne p2, v6, :cond_0

    .line 636
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$10;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v3

    sget-object v4, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v3, v4, :cond_4

    move v0, v1

    .line 637
    .local v0, "isSinglePickerMode":Z
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$10;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v3

    if-nez v3, :cond_0

    if-nez v0, :cond_0

    .line 638
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$10;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->enterSelectionMode(Z)V
    invoke-static {v3, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$4300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Z)V

    .line 639
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$10;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->updateCountOnActionBar()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$1000(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    move v2, v1

    .line 640
    goto :goto_0

    .end local v0    # "isSinglePickerMode":Z
    :cond_4
    move v0, v2

    .line 636
    goto :goto_1
.end method
