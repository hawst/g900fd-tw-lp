.class Lcom/sec/samsung/gallery/glview/GlDragAnimation$3;
.super Ljava/lang/Object;
.source "GlDragAnimation.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/glview/GlDragAnimation;->startMoveToAnimation(Lcom/sec/android/gallery3d/glcore/GlObject;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlDragAnimation;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlDragAnimation;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation$3;->this$0:Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 1
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation$3;->this$0:Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->releaseDrag()V

    .line 231
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation$3;->this$0:Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mOnDragListener:Lcom/sec/samsung/gallery/glview/GlDragAnimation$OnDragAnimListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->access$300(Lcom/sec/samsung/gallery/glview/GlDragAnimation;)Lcom/sec/samsung/gallery/glview/GlDragAnimation$OnDragAnimListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation$3;->this$0:Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mOnDragListener:Lcom/sec/samsung/gallery/glview/GlDragAnimation$OnDragAnimListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->access$300(Lcom/sec/samsung/gallery/glview/GlDragAnimation;)Lcom/sec/samsung/gallery/glview/GlDragAnimation$OnDragAnimListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/samsung/gallery/glview/GlDragAnimation$OnDragAnimListener;->onDragAnimDone(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 234
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 236
    return-void
.end method

.method public onAnimationStart(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 226
    return-void
.end method
