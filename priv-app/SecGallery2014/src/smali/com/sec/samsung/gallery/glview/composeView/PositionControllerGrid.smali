.class public Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;
.super Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
.source "PositionControllerGrid.java"


# static fields
.field private static GRID_COLCNT:[I

.field public static final GRID_FIRSTLOAD_ROWCNT_LAND:[I

.field public static final GRID_FIRSTLOAD_ROWCNT_PORT:[I

.field private static GRID_ITEM_GAP:[I

.field private static GRID_THM_TYPES:[I


# instance fields
.field private final NO_FOCUS:I

.field private final TITLE_BOX_NEED_ADD:I

.field private final TITLE_BOX_NEED_REMOVE:I

.field private final TITLE_BOX_POST_REMOVED:I

.field private mTitleFocusStatus:I

.field private mTitleInFocus:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 15
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->GRID_FIRSTLOAD_ROWCNT_PORT:[I

    .line 16
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->GRID_FIRSTLOAD_ROWCNT_LAND:[I

    .line 17
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->GRID_COLCNT:[I

    .line 18
    new-array v0, v1, [I

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->GRID_ITEM_GAP:[I

    .line 19
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->GRID_THM_TYPES:[I

    return-void

    .line 15
    :array_0
    .array-data 4
        0xf
        0x7
        0x5
        0x4
    .end array-data

    .line 16
    :array_1
    .array-data 4
        0x8
        0x4
        0x3
        0x2
    .end array-data

    .line 19
    :array_2
    .array-data 4
        0x1
        0x2
        0x2
        0x2
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 14
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;-><init>()V

    .line 25
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->NO_FOCUS:I

    .line 26
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->TITLE_BOX_POST_REMOVED:I

    .line 27
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->TITLE_BOX_NEED_ADD:I

    .line 28
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->TITLE_BOX_NEED_REMOVE:I

    .line 29
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    .line 30
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleInFocus:Z

    return-void
.end method

.method private findNewObj(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .locals 3
    .param p1, "groupInfo"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    .prologue
    .line 379
    iget v1, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v2, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroupVGap:F

    sub-float v0, v1, v2

    .line 380
    .local v0, "scrollAmount":F
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mValidH:F

    sub-float/2addr v0, v1

    .line 382
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    .line 383
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    return-object v1
.end method

.method private fitToLine()V
    .locals 11

    .prologue
    .line 387
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-gez v9, :cond_1

    .line 428
    :cond_0
    :goto_0
    return-void

    .line 395
    :cond_1
    const/4 v7, 0x0

    .line 397
    .local v7, "titleHeight":F
    const/4 v8, 0x0

    .line 399
    .local v8, "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    shr-int/lit8 v0, v9, 0x10

    .line 400
    .local v0, "groupIndex":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    const v10, 0xffff

    and-int v4, v9, v10

    .line 401
    .local v4, "itemIndex":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v9, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 402
    .local v2, "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v9, v0

    .line 404
    .local v1, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v9, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-ge v4, v9, :cond_0

    .line 407
    iget-object v9, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemH:[F

    const/4 v10, 0x0

    aget v3, v9, v10

    .line 408
    .local v3, "itemH":F
    iget v9, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget-object v10, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCy:[F

    aget v10, v10, v4

    add-float v5, v9, v10

    .line 410
    .local v5, "scrollAccu":F
    if-eqz v2, :cond_2

    .line 411
    iget-object v8, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 414
    :cond_2
    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mScrollable:F

    add-float/2addr v9, v3

    cmpg-float v9, v5, v9

    if-gez v9, :cond_5

    .line 415
    iget v9, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    if-ge v4, v9, :cond_4

    .line 416
    if-eqz v8, :cond_3

    .line 417
    iget v9, v8, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHeight:F

    const/high16 v10, 0x41200000    # 10.0f

    add-float v7, v9, v10

    .line 419
    :cond_3
    sub-float v9, v5, v3

    sub-float v6, v9, v7

    .line 423
    .local v6, "scrollAmount":F
    :goto_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v9, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    goto :goto_0

    .line 421
    .end local v6    # "scrollAmount":F
    :cond_4
    sub-float v6, v5, v3

    .restart local v6    # "scrollAmount":F
    goto :goto_1

    .line 424
    .end local v6    # "scrollAmount":F
    :cond_5
    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mValidH:F

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mScrollable:F

    add-float/2addr v9, v10

    cmpl-float v9, v5, v9

    if-lez v9, :cond_0

    .line 425
    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mValidH:F

    sub-float v6, v5, v9

    .line 426
    .restart local v6    # "scrollAmount":F
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v9, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    goto :goto_0
.end method

.method public static getGridColumnsCount(Landroid/content/res/Resources;Z)[I
    .locals 1
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "portraitMode"    # Z

    .prologue
    .line 69
    if-eqz p1, :cond_0

    const v0, 0x7f09008e

    :goto_0
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    return-object v0

    :cond_0
    const v0, 0x7f09008f

    goto :goto_0
.end method

.method private getKey(II)I
    .locals 1
    .param p1, "groupIndex"    # I
    .param p2, "itemIndex"    # I

    .prologue
    .line 542
    shl-int/lit8 v0, p1, 0x10

    add-int/2addr v0, p2

    return v0
.end method

.method public static getThumbSizeType(I)I
    .locals 1
    .param p0, "level"    # I

    .prologue
    .line 33
    sget-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->GRID_THM_TYPES:[I

    aget v0, v0, p0

    return v0
.end method

.method private updateFocused(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    .locals 11
    .param p1, "direction"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v10, -0x1

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 431
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    if-nez v5, :cond_1

    .line 538
    :cond_0
    :goto_0
    return-object v4

    .line 434
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    shr-int/lit8 v0, v5, 0x10

    .line 435
    .local v0, "groupIndex":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    const v6, 0xffff

    and-int v3, v5, v6

    .line 436
    .local v3, "itemIndex":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v5, v5

    if-ge v0, v5, :cond_0

    .line 438
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v4, v0

    .line 441
    .local v1, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    packed-switch p1, :pswitch_data_0

    .line 534
    :cond_2
    :goto_1
    if-nez p1, :cond_3

    .line 535
    iput v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    .line 537
    :cond_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    invoke-direct {p0, v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->getKey(II)I

    move-result v5

    iput v5, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    .line 538
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v4, v4, v0

    goto :goto_0

    .line 443
    :pswitch_0
    iget v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    add-int/2addr v4, v3

    iget v5, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-lt v4, v5, :cond_4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    if-ne v4, v9, :cond_7

    .line 444
    :cond_4
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    if-ne v4, v9, :cond_6

    .line 445
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 446
    .local v2, "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    if-eqz v2, :cond_5

    .line 447
    iget-object v4, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {p0, v4, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->setTitleFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)V

    .line 449
    :cond_5
    iput v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    .line 450
    iput-boolean v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleInFocus:Z

    goto :goto_1

    .line 452
    .end local v2    # "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_6
    iget v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    add-int/2addr v3, v4

    .line 453
    iput-boolean v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleInFocus:Z

    goto :goto_1

    .line 456
    :cond_7
    add-int/lit8 v4, v0, 0x1

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v5, v5

    if-ge v4, v5, :cond_2

    .line 457
    add-int/lit8 v0, v0, 0x1

    .line 458
    iput v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    .line 459
    const/4 v3, 0x0

    goto :goto_1

    .line 464
    :pswitch_1
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    if-ne v4, v8, :cond_9

    .line 465
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 466
    .restart local v2    # "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    if-eqz v2, :cond_8

    .line 467
    iget-object v4, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {p0, v4, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->setTitleFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)V

    .line 469
    :cond_8
    iput v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    .line 470
    iget v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    add-int/2addr v4, v3

    iget v5, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-ge v4, v5, :cond_2

    .line 471
    iget v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    add-int/2addr v3, v4

    goto :goto_1

    .line 473
    .end local v2    # "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_9
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    if-ne v4, v9, :cond_b

    .line 474
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 475
    .restart local v2    # "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    if-eqz v2, :cond_a

    .line 476
    iget-object v4, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {p0, v4, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->setTitleFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)V

    .line 478
    :cond_a
    iput v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    goto/16 :goto_1

    .line 480
    .end local v2    # "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_b
    add-int/lit8 v4, v3, 0x1

    iget v5, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-ge v4, v5, :cond_c

    .line 481
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 483
    :cond_c
    add-int/lit8 v4, v0, 0x1

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v5, v5

    if-ge v4, v5, :cond_2

    .line 484
    add-int/lit8 v0, v0, 0x1

    .line 485
    iput v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    .line 486
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 492
    :pswitch_2
    iget v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    sub-int v4, v3, v4

    if-ltz v4, :cond_d

    .line 493
    iget v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    sub-int/2addr v3, v4

    goto/16 :goto_1

    .line 495
    :cond_d
    iget v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    sub-int v4, v3, v4

    if-gez v4, :cond_e

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    if-ne v4, v10, :cond_e

    .line 496
    iput v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    goto/16 :goto_1

    .line 498
    :cond_e
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    if-ne v4, v9, :cond_10

    .line 499
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 500
    .restart local v2    # "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    if-eqz v2, :cond_f

    .line 501
    iget-object v4, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {p0, v4, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->setTitleFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)V

    .line 503
    :cond_f
    iput v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    .line 505
    .end local v2    # "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_10
    add-int/lit8 v4, v0, -0x1

    if-ltz v4, :cond_2

    .line 506
    add-int/lit8 v0, v0, -0x1

    .line 507
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v4, v4, v0

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    add-int/lit8 v3, v4, -0x1

    goto/16 :goto_1

    .line 513
    :pswitch_3
    add-int/lit8 v4, v3, -0x1

    if-ltz v4, :cond_11

    .line 514
    add-int/lit8 v3, v3, -0x1

    goto/16 :goto_1

    .line 516
    :cond_11
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    if-ne v4, v10, :cond_12

    .line 517
    iput v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    goto/16 :goto_1

    .line 519
    :cond_12
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    if-ne v4, v9, :cond_14

    .line 520
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 521
    .restart local v2    # "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    if-eqz v2, :cond_13

    .line 522
    iget-object v4, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {p0, v4, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->setTitleFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)V

    .line 524
    :cond_13
    iput v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    .line 526
    .end local v2    # "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_14
    add-int/lit8 v4, v0, -0x1

    if-ltz v4, :cond_2

    .line 527
    add-int/lit8 v0, v0, -0x1

    .line 528
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v4, v4, v0

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    add-int/lit8 v3, v4, -0x1

    goto/16 :goto_1

    .line 441
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public calcItemPosition(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;)F
    .locals 14
    .param p1, "groupInfo"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    .prologue
    .line 131
    iget-object v8, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCx:[F

    .line 132
    .local v8, "cx":[F
    iget-object v9, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCy:[F

    .line 133
    .local v9, "cy":[F
    iget-object v7, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemW:[F

    .line 134
    .local v7, "cw":[F
    iget-object v5, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemH:[F

    .line 135
    .local v5, "ch":[F
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPhotoLineCount:I

    .line 137
    .local v6, "colCount":I
    iput v6, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    .line 138
    iget v11, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPhotoLineCount:I

    add-int/2addr v11, v12

    add-int/lit8 v11, v11, -0x1

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPhotoLineCount:I

    div-int/2addr v11, v12

    iput v11, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mRow:I

    .line 140
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mValidW:F

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mItemGapW:F

    add-int/lit8 v13, v6, -0x1

    int-to-float v13, v13

    mul-float/2addr v12, v13

    sub-float/2addr v11, v12

    int-to-float v12, v6

    div-float v4, v11, v12

    .line 141
    .local v4, "SW":F
    move v2, v4

    .line 142
    .local v2, "SH":F
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mItemGapW:F

    add-float v1, v4, v11

    .line 143
    .local v1, "NW":F
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mItemGapH:F

    add-float v0, v2, v11

    .line 144
    .local v0, "NH":F
    iput v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    .line 145
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-object v11, v11, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v11, v11, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mTopGroupTitle:Z

    if-eqz v11, :cond_0

    .line 146
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleH:F

    add-float/2addr v11, v2

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleTextMarginTop:F

    add-float/2addr v11, v12

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleTextMarginBottom:F

    add-float v3, v11, v12

    .line 147
    .local v3, "SIH":F
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleW:F

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    iput v11, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mTitleCx:F

    .line 148
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleTextMarginTop:F

    neg-float v11, v11

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleH:F

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    iput v11, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mTitleCy:F

    .line 155
    :goto_0
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    iget v11, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-ge v10, v11, :cond_1

    .line 156
    rem-int v11, v10, v6

    int-to-float v11, v11

    mul-float/2addr v11, v1

    const/high16 v12, 0x40000000    # 2.0f

    div-float v12, v4, v12

    add-float/2addr v11, v12

    aput v11, v8, v10

    .line 157
    div-int v11, v10, v6

    int-to-float v11, v11

    mul-float/2addr v11, v0

    add-float/2addr v11, v3

    aput v11, v9, v10

    .line 158
    aput v4, v7, v10

    .line 159
    aput v2, v5, v10

    .line 155
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 150
    .end local v3    # "SIH":F
    .end local v10    # "i":I
    :cond_0
    move v3, v2

    .line 151
    .restart local v3    # "SIH":F
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleW:F

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    iput v11, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mTitleCx:F

    .line 152
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleTextMarginTop:F

    neg-float v11, v11

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleH:F

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    iget v12, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mRow:I

    int-to-float v12, v12

    mul-float/2addr v12, v0

    add-float/2addr v11, v12

    iput v11, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mTitleCy:F

    goto :goto_0

    .line 161
    .restart local v10    # "i":I
    :cond_1
    iget v11, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mRow:I

    int-to-float v11, v11

    iget v12, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    mul-float/2addr v11, v12

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroupVGap:F

    add-float/2addr v11, v12

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mItemGapH:F

    sub-float/2addr v11, v12

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleH:F

    add-float/2addr v11, v12

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleTextMarginBottom:F

    add-float/2addr v11, v12

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleTextMarginTop:F

    add-float/2addr v11, v12

    return v11
.end method

.method public doQuickScroll(I)V
    .locals 4
    .param p1, "groupIndex"    # I

    .prologue
    .line 564
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v0, v2, p1

    .line 567
    .local v0, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroupVGap:F

    sub-float v1, v2, v3

    .line 568
    .local v1, "scrolAmount":F
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPortraitMode:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mValidH:F

    :goto_0
    sub-float/2addr v1, v2

    .line 569
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mScrollableMax:F

    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    .line 570
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    .line 574
    :goto_1
    return-void

    .line 568
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mValidW:F

    goto :goto_0

    .line 572
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    goto :goto_1
.end method

.method public getCenteredScroll(F)F
    .locals 4
    .param p1, "scroll"    # F

    .prologue
    .line 183
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mValidH:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mItemH:F

    sub-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float v1, p1, v1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleH:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleTextMarginTop:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleTextMarginBottom:F

    add-float/2addr v2, v3

    add-float v0, v1, v2

    .line 185
    .local v0, "resScroll":F
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    .line 186
    const/4 v0, 0x0

    .line 190
    :cond_0
    :goto_0
    return v0

    .line 187
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mScrollableMax:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 188
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mScrollableMax:F

    goto :goto_0
.end method

.method public getScrollForIndex(I)F
    .locals 7
    .param p1, "index"    # I

    .prologue
    const/4 v4, 0x0

    .line 169
    shr-int/lit8 v0, p1, 0x10

    .line 170
    .local v0, "albumIdx":I
    const v5, 0xffff

    and-int v2, p1, v5

    .line 171
    .local v2, "photoIdx":I
    if-ltz p1, :cond_0

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroupCount:I

    if-lt v0, v5, :cond_1

    .line 178
    :cond_0
    :goto_0
    return v4

    .line 174
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v5, v0

    .line 175
    .local v1, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v5, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    if-lez v5, :cond_0

    .line 177
    iget v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    div-int v3, v2, v4

    .line 178
    .local v3, "row":I
    iget v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    int-to-float v5, v3

    iget v6, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    goto :goto_0
.end method

.method public moveTo(I)Z
    .locals 13
    .param p1, "direction"    # I

    .prologue
    const/4 v10, 0x3

    const/4 v12, -0x1

    const/4 v11, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 292
    const/4 v1, 0x0

    .line 294
    .local v1, "focusDisappeared":Z
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v4, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    .line 296
    .local v4, "lastFocus":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsFocusVisible:Z

    if-nez v9, :cond_6

    .line 297
    const/4 v9, 0x4

    if-eq p1, v9, :cond_1

    .line 374
    :cond_0
    :goto_0
    return v8

    .line 300
    :cond_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iput-boolean v7, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsFocusVisible:Z

    .line 301
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGrpContentStart:I

    iput v10, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    .line 302
    const/4 p1, 0x0

    .line 336
    :cond_2
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-ltz v9, :cond_0

    .line 339
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->updateFocused(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    move-result-object v2

    .line 340
    .local v2, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-ne v4, v9, :cond_3

    iget-boolean v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleInFocus:Z

    if-eqz v9, :cond_10

    :cond_3
    move v9, v7

    :goto_1
    iput-boolean v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mFocusChangedFlag:Z

    .line 341
    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    if-ne v9, v7, :cond_11

    .line 342
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v9, :cond_0

    if-eqz v2, :cond_0

    .line 345
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v9, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 346
    .local v6, "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    invoke-virtual {p0, v6, v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .line 348
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->fitToLine()V

    .line 349
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    shr-int/lit8 v9, v9, 0x10

    invoke-virtual {v8, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 350
    .local v3, "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    if-eqz v3, :cond_4

    .line 351
    iget-object v8, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {p0, v8, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->setTitleFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)V

    .line 353
    :cond_4
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iput-boolean v7, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->isGroupCheckBoxFocused:Z

    .line 354
    iput v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    .end local v3    # "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    .end local v6    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_5
    :goto_2
    move v8, v7

    .line 374
    goto :goto_0

    .line 303
    .end local v2    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    :cond_6
    if-eq p1, v10, :cond_7

    const/4 v9, 0x5

    if-eq p1, v9, :cond_7

    if-ne p1, v7, :cond_2

    .line 305
    :cond_7
    if-ne p1, v10, :cond_e

    .line 306
    const/4 v0, 0x0

    .line 307
    .local v0, "column":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    if-eqz v9, :cond_8

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v9, v9

    if-lez v9, :cond_8

    .line 308
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v9, v9, v8

    iget v0, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    .line 310
    :cond_8
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-ge v9, v0, :cond_d

    move v1, v7

    .line 318
    .end local v0    # "column":I
    :cond_9
    :goto_3
    if-eqz v1, :cond_2

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    if-ne v9, v11, :cond_2

    .line 319
    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    if-ne v7, v11, :cond_b

    .line 320
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    shr-int/lit8 v9, v9, 0x10

    invoke-virtual {v7, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 321
    .restart local v3    # "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    if-eqz v3, :cond_a

    .line 322
    iget-object v7, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {p0, v7, v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->setTitleFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)V

    .line 324
    :cond_a
    iput v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    .line 326
    .end local v3    # "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_b
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {v7, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 327
    .restart local v6    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-eqz v6, :cond_c

    .line 328
    invoke-virtual {p0, v6, v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .line 330
    :cond_c
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iput-boolean v8, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsFocusVisible:Z

    .line 331
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iput v12, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    goto/16 :goto_0

    .end local v6    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .restart local v0    # "column":I
    :cond_d
    move v1, v8

    .line 310
    goto :goto_3

    .line 311
    .end local v0    # "column":I
    :cond_e
    if-ne p1, v7, :cond_f

    .line 312
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-nez v9, :cond_9

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    if-ne v9, v11, :cond_9

    .line 313
    const/4 v1, 0x1

    goto :goto_3

    .line 315
    :cond_f
    const/4 v1, 0x1

    goto :goto_3

    .restart local v2    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    :cond_10
    move v9, v8

    .line 340
    goto/16 :goto_1

    .line 356
    :cond_11
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-ne v4, v9, :cond_12

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    if-nez v9, :cond_5

    .line 357
    :cond_12
    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    if-nez v9, :cond_13

    iput v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleFocusStatus:I

    .line 358
    :cond_13
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v9, :cond_0

    if-eqz v2, :cond_0

    .line 361
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v9, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 362
    .restart local v6    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    invoke-virtual {p0, v6, v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .line 364
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->fitToLine()V

    .line 365
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v10, v10, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {v9, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 366
    .local v5, "newThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iput-boolean v8, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->isGroupCheckBoxFocused:Z

    .line 368
    if-nez v5, :cond_14

    .line 369
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->findNewObj(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    move-result-object v5

    .line 371
    :cond_14
    invoke-virtual {p0, v5, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    goto/16 :goto_2
.end method

.method public onCreateThumbObj(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V
    .locals 0
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .prologue
    .line 580
    return-void
.end method

.method public resetAttributes(IZ)V
    .locals 7
    .param p1, "option"    # I
    .param p2, "portraitMode"    # Z

    .prologue
    .line 73
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getLcdRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mLcdRect:Landroid/graphics/Rect;

    .line 74
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mResource:Landroid/content/res/Resources;

    .line 75
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getActionBarHeight()I

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mMarginTopPixel:I

    .line 76
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v0

    .line 77
    .local v0, "mDimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPickMode(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->IsCheckMode()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 79
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mMarginTopPixel:I

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getSelectionModeLayoutHeight()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mMarginTopPixel:I

    .line 82
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mLcdRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mTitleWidthPixel:I

    .line 84
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumTimeViewVHGap()[I

    move-result-object v4

    sput-object v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->GRID_ITEM_GAP:[I

    .line 85
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->resetValues(Z)V

    .line 86
    sget-object v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->GRID_COLCNT:[I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mLevel:I

    aget v4, v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPhotoLineCount:I

    .line 87
    sget-object v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->GRID_ITEM_GAP:[I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mLevel:I

    aget v4, v4, v5

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->convX(I)F

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mItemGapW:F

    .line 88
    sget-object v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->GRID_ITEM_GAP:[I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mLevel:I

    aget v4, v4, v5

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->convY(I)F

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mItemGapH:F

    .line 90
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPortraitMode:Z

    if-nez v4, :cond_2

    .line 91
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTabletPickerMode(Landroid/content/Intent;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 92
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mLcdRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->convX(I)F

    move-result v4

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPhotoLineCount:I

    int-to-float v5, v5

    div-float v1, v4, v5

    .line 93
    .local v1, "minW":F
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mValidW:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPhotoLineCount:I

    int-to-float v5, v5

    mul-float/2addr v5, v1

    cmpg-float v4, v4, v5

    if-gez v4, :cond_2

    .line 94
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPhotoLineCount:I

    .line 95
    .local v3, "originalPhotoLineCount":I
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mValidW:F

    div-float/2addr v4, v1

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 96
    .local v2, "newPhotoLineCount":I
    const/4 v4, 0x1

    invoke-static {v2, v4, v3}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPhotoLineCount:I

    .line 100
    .end local v1    # "minW":F
    .end local v2    # "newPhotoLineCount":I
    .end local v3    # "originalPhotoLineCount":I
    :cond_2
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mValidW:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mItemGapW:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPhotoLineCount:I

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPhotoLineCount:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mItemW:F

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mItemH:F

    .line 101
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->resetItemMaxSize()V

    .line 102
    return-void
.end method

.method protected resetItemMaxSize()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 52
    const/4 v2, 0x0

    .line 53
    .local v2, "tempMaxSize":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mIsEasyMode:Z

    if-eqz v3, :cond_0

    .line 54
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mItemW:F

    .line 64
    :goto_0
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->rConvX(F)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mItemMaxW:F

    .line 65
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->rConvY(F)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mItemMaxH:F

    .line 66
    return-void

    .line 56
    :cond_0
    sget-object v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->GRID_ITEM_GAP:[I

    aget v3, v3, v4

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->convX(I)F

    move-result v0

    .line 57
    .local v0, "maxLvlItemGapW":F
    sget-object v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->GRID_COLCNT:[I

    aget v1, v3, v4

    .line 58
    .local v1, "minPhotoCount":I
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mPhotoLineCount:I

    if-lt v1, v3, :cond_1

    .line 59
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mItemW:F

    goto :goto_0

    .line 61
    :cond_1
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mValidW:F

    add-int/lit8 v4, v1, -0x1

    int-to-float v4, v4

    mul-float/2addr v4, v0

    sub-float/2addr v3, v4

    int-to-float v4, v1

    div-float v2, v3, v4

    goto :goto_0
.end method

.method public resetPosition()V
    .locals 5

    .prologue
    .line 107
    const/4 v3, 0x0

    .line 109
    .local v3, "scrollable":F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroupCount:I

    if-ge v1, v4, :cond_1

    .line 110
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v0, v4, v1

    .line 111
    .local v0, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    if-nez v0, :cond_0

    .line 109
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 114
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->calcItemPosition(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;)F

    move-result v2

    .line 115
    .local v2, "scrollUnit":F
    iput v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    .line 116
    iput v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    .line 117
    add-float/2addr v3, v2

    goto :goto_1

    .line 119
    .end local v0    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    .end local v2    # "scrollUnit":F
    :cond_1
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mValidH:F

    cmpg-float v4, v3, v4

    if-gtz v4, :cond_2

    .line 120
    const/4 v4, 0x0

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mScrollableMax:F

    .line 123
    :goto_2
    return-void

    .line 122
    :cond_2
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mValidH:F

    sub-float v4, v3, v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mScrollableMax:F

    goto :goto_2
.end method

.method public resetPosition(II)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 126
    return-void
.end method

.method protected resetValues(Z)V
    .locals 1
    .param p1, "portraitMode"    # Z

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->resetValues(Z)V

    .line 48
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mResource:Landroid/content/res/Resources;

    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->getGridColumnsCount(Landroid/content/res/Resources;Z)[I

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->GRID_COLCNT:[I

    .line 49
    return-void
.end method

.method public setTitleFocusBorderVisible(II)V
    .locals 3
    .param p1, "oldIndex"    # I
    .param p2, "newIndex"    # I

    .prologue
    .line 549
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 550
    .local v0, "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    if-eqz v0, :cond_0

    .line 551
    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 552
    .local v1, "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->setTitleFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)V

    .line 555
    .end local v1    # "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v2, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 556
    .restart local v0    # "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    if-eqz v0, :cond_1

    .line 557
    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 558
    .restart local v1    # "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->setTitleFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)V

    .line 560
    .end local v1    # "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_1
    return-void
.end method

.method public setToCurrentCtrl()V
    .locals 3

    .prologue
    .line 39
    sget-object v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->GRID_THM_TYPES:[I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mLevel:I

    aget v0, v1, v2

    .line 40
    .local v0, "thmLevel":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v1, :cond_0

    .line 41
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->setThumbReslevel(I)V

    .line 42
    :cond_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setToCurrentCtrl()V

    .line 43
    return-void
.end method

.method public setVisibleRange()V
    .locals 15

    .prologue
    .line 195
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mScrollable:F

    neg-float v10, v12

    .line 198
    .local v10, "scrollAmount":F
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroupCount:I

    add-int/lit8 v8, v12, -0x1

    .line 199
    .local v8, "lastGrp":I
    move v7, v8

    .line 200
    .local v7, "inGrpStart":I
    move v5, v8

    .line 201
    .local v5, "inGrpEnd":I
    move v4, v8

    .line 202
    .local v4, "inCntGrpStart":I
    move v3, v8

    .line 204
    .local v3, "inCntGrpEnd":I
    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mMargTop:F

    .line 205
    .local v9, "modMargTop":F
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 206
    const/high16 v12, 0x40a00000    # 5.0f

    add-float/2addr v9, v12

    .line 208
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroupCount:I

    if-ge v2, v12, :cond_4

    .line 209
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v12, v2

    .line 211
    .local v1, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v12, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v10, v12

    .line 212
    if-ge v2, v4, :cond_1

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mExtraTop:F

    cmpg-float v12, v12, v10

    if-gtz v12, :cond_1

    .line 213
    move v4, v2

    .line 215
    :cond_1
    if-ge v2, v7, :cond_2

    neg-float v12, v9

    cmpg-float v12, v12, v10

    if-gtz v12, :cond_2

    .line 216
    move v7, v2

    .line 218
    :cond_2
    if-ne v5, v8, :cond_3

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mValidH:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mMargBtm:F

    add-float/2addr v12, v13

    cmpl-float v12, v10, v12

    if-ltz v12, :cond_3

    .line 219
    move v5, v2

    .line 221
    :cond_3
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mExtraBtm:F

    cmpl-float v12, v10, v12

    if-ltz v12, :cond_5

    .line 222
    move v3, v2

    .line 227
    .end local v1    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    :cond_4
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v12, v7

    .line 228
    .restart local v1    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v12, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mScrollable:F

    sub-float v0, v12, v13

    .line 229
    .local v0, "exceedAmount":F
    neg-float v12, v9

    cmpl-float v12, v0, v12

    if-lez v12, :cond_6

    .line 230
    const/4 v6, 0x0

    .line 240
    .local v6, "inGrpIndex":I
    :goto_1
    shl-int/lit8 v12, v7, 0x10

    or-int/2addr v12, v6

    iput v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGrpActiveStart:I

    .line 242
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v12, v5

    .line 243
    iget v12, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v13, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v12, v13

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mScrollable:F

    sub-float v0, v12, v13

    .line 244
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mValidH:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mMargBtm:F

    add-float/2addr v12, v13

    cmpg-float v12, v0, v12

    if-gtz v12, :cond_8

    .line 245
    iget v6, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    .line 255
    :goto_2
    shl-int/lit8 v12, v5, 0x10

    or-int/2addr v12, v6

    iput v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGrpActiveEnd:I

    .line 257
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v12, v4

    .line 258
    iget v12, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mScrollable:F

    sub-float v0, v12, v13

    .line 259
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mExtraTop:F

    cmpl-float v12, v0, v12

    if-lez v12, :cond_a

    .line 260
    const/4 v6, 0x0

    .line 270
    :goto_3
    shl-int/lit8 v12, v4, 0x10

    or-int/2addr v12, v6

    iput v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGrpContentStart:I

    .line 272
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v12, v3

    .line 273
    iget v12, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v13, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v12, v13

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mScrollable:F

    sub-float v0, v12, v13

    .line 274
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mExtraBtm:F

    cmpg-float v12, v0, v12

    if-gtz v12, :cond_c

    .line 275
    iget v6, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    .line 285
    :goto_4
    shl-int/lit8 v12, v3, 0x10

    or-int/2addr v12, v6

    iput v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mGrpContentEnd:I

    .line 286
    return-void

    .line 208
    .end local v0    # "exceedAmount":F
    .end local v6    # "inGrpIndex":I
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 232
    .restart local v0    # "exceedAmount":F
    :cond_6
    add-float v12, v9, v0

    neg-float v11, v12

    .line 233
    .local v11, "scrollExc":F
    const/4 v12, 0x0

    cmpg-float v12, v11, v12

    if-gtz v12, :cond_7

    .line 234
    const/4 v6, 0x0

    .restart local v6    # "inGrpIndex":I
    goto :goto_1

    .line 236
    .end local v6    # "inGrpIndex":I
    :cond_7
    iget v12, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    div-float v12, v11, v12

    float-to-int v6, v12

    .line 237
    .restart local v6    # "inGrpIndex":I
    iget v12, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    mul-int/2addr v6, v12

    goto :goto_1

    .line 247
    .end local v11    # "scrollExc":F
    :cond_8
    iget v12, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mValidH:F

    sub-float v13, v0, v13

    iget v14, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mMargBtm:F

    sub-float/2addr v13, v14

    sub-float v11, v12, v13

    .line 248
    .restart local v11    # "scrollExc":F
    const/4 v12, 0x0

    cmpg-float v12, v11, v12

    if-gtz v12, :cond_9

    .line 249
    const/4 v6, 0x0

    goto :goto_2

    .line 251
    :cond_9
    iget v12, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    div-float v12, v11, v12

    float-to-double v12, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v12

    double-to-int v6, v12

    .line 252
    iget v12, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    mul-int/2addr v12, v6

    iget v13, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v6

    goto :goto_2

    .line 262
    .end local v11    # "scrollExc":F
    :cond_a
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mExtraTop:F

    sub-float v11, v12, v0

    .line 263
    .restart local v11    # "scrollExc":F
    const/4 v12, 0x0

    cmpg-float v12, v11, v12

    if-gtz v12, :cond_b

    .line 264
    const/4 v6, 0x0

    goto :goto_3

    .line 266
    :cond_b
    iget v12, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    div-float v12, v11, v12

    float-to-int v6, v12

    .line 267
    iget v12, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    mul-int/2addr v6, v12

    goto :goto_3

    .line 277
    .end local v11    # "scrollExc":F
    :cond_c
    iget v12, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->mExtraBtm:F

    sub-float v13, v0, v13

    sub-float v11, v12, v13

    .line 278
    .restart local v11    # "scrollExc":F
    const/4 v12, 0x0

    cmpg-float v12, v11, v12

    if-gtz v12, :cond_d

    .line 279
    const/4 v6, 0x0

    goto :goto_4

    .line 281
    :cond_d
    iget v12, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    div-float v12, v11, v12

    float-to-double v12, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v12

    double-to-int v6, v12

    .line 282
    iget v12, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    mul-int/2addr v12, v6

    iget v13, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v6

    goto :goto_4
.end method
