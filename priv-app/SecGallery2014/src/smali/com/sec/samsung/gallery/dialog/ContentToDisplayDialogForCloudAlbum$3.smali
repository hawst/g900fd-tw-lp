.class Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;
.super Landroid/widget/ArrayAdapter;
.source "ContentToDisplayDialogForCloudAlbum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->createViewtypeDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;Landroid/content/Context;II[Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I
    .param p4, "x2"    # I
    .param p5, "x3"    # [Ljava/lang/String;

    .prologue
    .line 302
    iput-object p1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 312
    if-eqz p2, :cond_1

    move-object v0, p2

    .line 315
    .local v0, "cView":Landroid/view/View;
    :goto_0
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v6, :cond_2

    sget v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->mWhiteThemeListClickedColor:I

    .line 317
    .local v1, "colorOnClick":I
    :goto_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidFormatOptions:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$800(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v5

    .line 318
    .local v5, "viewType":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->isViewByOptionEnabled(I)Z
    invoke-static {v6, v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$400(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)Z

    move-result v3

    .line 320
    .local v3, "isEnabled":Z
    sget-object v6, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    iget-object v7, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v7}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v6

    if-nez v6, :cond_0

    sget-object v6, Lcom/sec/samsung/gallery/core/ViewByFilterType;->FACEBOOK:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    iget-object v7, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v7}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v6

    if-nez v6, :cond_0

    sget-object v6, Lcom/sec/samsung/gallery/core/ViewByFilterType;->PICASA:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    iget-object v7, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v7}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v6

    if-nez v6, :cond_0

    sget-object v6, Lcom/sec/samsung/gallery/core/ViewByFilterType;->TCLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    iget-object v7, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v7}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 322
    const/4 v3, 0x0

    .line 323
    :cond_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    move-object v2, v0

    .line 325
    check-cast v2, Landroid/widget/CheckedTextView;

    .line 326
    .local v2, "ctv":Landroid/widget/CheckedTextView;
    iget-object v6, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v6}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)I

    move-result v6

    and-int/2addr v6, v5

    if-eqz v6, :cond_3

    const/4 v6, 0x1

    :goto_2
    invoke-virtual {v2, v6}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 328
    new-instance v6, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3$1;

    invoke-direct {v6, p0, v0, v1, v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3$1;-><init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;Landroid/view/View;II)V

    invoke-virtual {v2, v6}, Landroid/widget/CheckedTextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 356
    const v6, 0x1020014

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 357
    .local v4, "tv":Landroid/widget/TextView;
    if-nez v4, :cond_4

    .line 369
    :goto_3
    return-object v0

    .line 312
    .end local v0    # "cView":Landroid/view/View;
    .end local v1    # "colorOnClick":I
    .end local v2    # "ctv":Landroid/widget/CheckedTextView;
    .end local v3    # "isEnabled":Z
    .end local v4    # "tv":Landroid/widget/TextView;
    .end local v5    # "viewType":I
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 315
    .restart local v0    # "cView":Landroid/view/View;
    :cond_2
    sget v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->mBlackThemeListClickedColor:I

    goto :goto_1

    .line 326
    .restart local v1    # "colorOnClick":I
    .restart local v2    # "ctv":Landroid/widget/CheckedTextView;
    .restart local v3    # "isEnabled":Z
    .restart local v5    # "viewType":I
    :cond_3
    const/4 v6, 0x0

    goto :goto_2

    .line 361
    .restart local v4    # "tv":Landroid/widget/TextView;
    :cond_4
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 363
    if-nez v3, :cond_5

    .line 364
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$200(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0e02ac

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 367
    :cond_5
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 306
    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidFormatOptions:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$800(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v0

    .line 307
    .local v0, "viewType":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->isViewByOptionEnabled(I)Z
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$400(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)Z

    move-result v1

    return v1
.end method
