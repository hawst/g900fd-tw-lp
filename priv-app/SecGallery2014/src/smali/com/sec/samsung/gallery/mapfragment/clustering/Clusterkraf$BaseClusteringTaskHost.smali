.class abstract Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;
.super Ljava/lang/Object;
.source "Clusterkraf.java"

# interfaces
.implements Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Host;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "BaseClusteringTaskHost"
.end annotation


# instance fields
.field private task:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask;

.field final synthetic this$0:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)V
    .locals 1

    .prologue
    .line 510
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;->this$0:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 511
    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask;-><init>(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Host;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;->task:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask;

    .line 512
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 3

    .prologue
    .line 522
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$600()Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getProcessingListener()Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ProcessingListener;

    move-result-object v0

    .line 523
    .local v0, "processingListener":Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ProcessingListener;
    if-eqz v0, :cond_0

    .line 524
    invoke-interface {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ProcessingListener;->onClusteringFinished()V

    .line 526
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;->task:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask;->cancel(Z)Z

    .line 527
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;->task:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask;

    .line 528
    return-void
.end method

.method public executeTask()V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 532
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;->this$0:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mapRef:Ljava/lang/ref/WeakReference;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$1700(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Ljava/lang/ref/WeakReference;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/maps/GoogleMap;

    .line 533
    .local v2, "map":Lcom/google/android/gms/maps/GoogleMap;
    if-eqz v2, :cond_2

    .line 534
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$600()Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getProcessingListener()Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ProcessingListener;

    move-result-object v3

    .line 535
    .local v3, "processingListener":Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ProcessingListener;
    if-eqz v3, :cond_0

    .line 536
    invoke-interface {v3}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ProcessingListener;->onClusteringStarted()V

    .line 539
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Argument;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Argument;-><init>()V

    .line 540
    .local v0, "arg":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Argument;
    invoke-virtual {v2}, Lcom/google/android/gms/maps/GoogleMap;->getProjection()Lcom/google/android/gms/maps/Projection;

    move-result-object v4

    iput-object v4, v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Argument;->projection:Lcom/google/android/gms/maps/Projection;

    .line 541
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$600()Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    move-result-object v4

    iput-object v4, v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Argument;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    .line 542
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;->this$0:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->points:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$1800(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Argument;->points:Ljava/util/ArrayList;

    .line 543
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;->this$0:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->previousClusters:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$1900(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Argument;->previousClusters:Ljava/util/ArrayList;

    .line 544
    invoke-virtual {v2}, Lcom/google/android/gms/maps/GoogleMap;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v1

    .line 545
    .local v1, "cameraPos":Lcom/google/android/gms/maps/model/CameraPosition;
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Argument;->skipCheck:Ljava/lang/Boolean;

    .line 546
    if-eqz v1, :cond_1

    .line 547
    iget v4, v1, Lcom/google/android/gms/maps/model/CameraPosition;->zoom:F

    const/high16 v7, 0x40600000    # 3.5f

    cmpg-float v4, v4, v7

    if-gez v4, :cond_3

    move v4, v5

    :goto_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Argument;->skipCheck:Ljava/lang/Boolean;

    .line 548
    :cond_1
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0xb

    if-lt v4, v7, :cond_4

    .line 549
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;->task:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask;

    sget-object v7, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v5, v5, [Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Argument;

    aput-object v0, v5, v6

    invoke-virtual {v4, v7, v5}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 555
    .end local v0    # "arg":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Argument;
    .end local v1    # "cameraPos":Lcom/google/android/gms/maps/model/CameraPosition;
    .end local v3    # "processingListener":Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ProcessingListener;
    :cond_2
    :goto_1
    return-void

    .restart local v0    # "arg":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Argument;
    .restart local v1    # "cameraPos":Lcom/google/android/gms/maps/model/CameraPosition;
    .restart local v3    # "processingListener":Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ProcessingListener;
    :cond_3
    move v4, v6

    .line 547
    goto :goto_0

    .line 551
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;->task:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask;

    new-array v5, v5, [Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Argument;

    aput-object v0, v5, v6

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method

.method public onClusteringTaskPostExecute(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Result;)V
    .locals 2
    .param p1, "result"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Result;

    .prologue
    .line 516
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;->this$0:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    iget-object v1, p1, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Result;->currentClusters:Ljava/util/ArrayList;

    # setter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusters:Ljava/util/ArrayList;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$1602(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 517
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;->onCurrentClustersSet(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Result;)V

    .line 518
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;->task:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask;

    .line 519
    return-void
.end method

.method protected abstract onCurrentClustersSet(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Result;)V
.end method
