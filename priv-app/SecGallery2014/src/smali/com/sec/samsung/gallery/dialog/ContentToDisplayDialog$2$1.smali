.class Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;
.super Ljava/lang/Object;
.source "ContentToDisplayDialog.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

.field final synthetic val$position:I

.field final synthetic val$viewType:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;II)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iput p2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->val$viewType:I

    iput p3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private isAllItemsSelected()Z
    .locals 5

    .prologue
    .line 239
    const/4 v0, 0x1

    .local v0, "i":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iget-object v3, v3, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 240
    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iget-object v3, v3, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    .line 241
    .local v2, "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iget-object v3, v3, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iget-object v3, v3, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v4

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->isViewByOptionEnabled(I)Z
    invoke-static {v3, v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$400(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 243
    const/4 v3, 0x0

    .line 246
    .end local v2    # "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    :goto_1
    return v3

    .line 239
    .restart local v2    # "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 246
    .end local v2    # "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    :cond_1
    const/4 v3, 0x1

    goto :goto_1
.end method

.method private updateListItem(Z)V
    .locals 8
    .param p1, "isAllSelected"    # Z

    .prologue
    .line 219
    const/4 v2, 0x1

    .local v2, "i":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iget-object v6, v6, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v3

    .local v3, "n":I
    :goto_0
    if-ge v2, v3, :cond_3

    .line 220
    iget-object v6, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iget-object v6, v6, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v6

    check-cast v6, Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 222
    .local v0, "checkbox":Landroid/widget/CheckedTextView;
    iget-object v6, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iget-object v6, v6, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    .line 223
    .local v5, "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    invoke-virtual {v5}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v4

    .line 225
    .local v4, "typeIndex":I
    if-eqz p1, :cond_1

    iget-object v6, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iget-object v6, v6, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->isViewByOptionEnabled(I)Z
    invoke-static {v6, v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$400(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v1, 0x1

    .line 226
    .local v1, "checked":Z
    :goto_1
    if-eqz v0, :cond_0

    .line 227
    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 230
    :cond_0
    if-eqz v1, :cond_2

    .line 231
    iget-object v6, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iget-object v6, v6, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # |= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v6, v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$076(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)I

    .line 219
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 225
    .end local v1    # "checked":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 233
    .restart local v1    # "checked":Z
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iget-object v6, v6, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    xor-int/lit8 v7, v4, -0x1

    # &= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v6, v7}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$072(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)I

    goto :goto_2

    .line 236
    .end local v0    # "checkbox":Landroid/widget/CheckedTextView;
    .end local v1    # "checked":Z
    .end local v4    # "typeIndex":I
    .end local v5    # "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    :cond_3
    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    .line 184
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .local v0, "action":I
    move-object v1, p1

    .line 185
    check-cast v1, Landroid/widget/CheckedTextView;

    .line 186
    .local v1, "ctv":Landroid/widget/CheckedTextView;
    if-nez v0, :cond_1

    .line 187
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$200(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 215
    :cond_0
    :goto_0
    return v4

    .line 188
    :cond_1
    if-ne v0, v4, :cond_0

    .line 189
    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->toggle()V

    .line 190
    const v2, 0x8000

    invoke-virtual {v1, v2}, Landroid/widget/CheckedTextView;->sendAccessibilityEvent(I)V

    .line 191
    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 192
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    iget v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->val$viewType:I

    # |= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$076(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)I

    .line 197
    :goto_1
    iget v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->val$position:I

    if-nez v2, :cond_3

    .line 199
    sget-object v2, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iget-object v3, v3, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->updateListItem(Z)V

    .line 213
    :goto_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->updateSaveButton()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$500(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)V

    goto :goto_0

    .line 194
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    iget v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->val$viewType:I

    xor-int/lit8 v3, v3, -0x1

    # &= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$072(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)I

    goto :goto_1

    .line 203
    :cond_3
    invoke-direct {p0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->isAllItemsSelected()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 205
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    sget-object v3, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v3

    # |= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$076(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)I

    .line 211
    :goto_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    check-cast v2, Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->invalidateViews()V

    goto :goto_2

    .line 207
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    sget-object v3, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v3

    xor-int/lit8 v3, v3, -0x1

    # &= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$072(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)I

    goto :goto_3
.end method
