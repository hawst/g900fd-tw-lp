.class public Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;
.super Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
.source "PositionControllerAlbum.java"


# static fields
.field private static GRID_COLCNT:[I

.field private static GRID_ITEM_GAP:[I

.field private static GRID_ITEM_GAP_EASYMODE:[I

.field private static GRID_THM_TYPES:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 19
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->GRID_ITEM_GAP:[I

    .line 20
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->GRID_ITEM_GAP_EASYMODE:[I

    .line 21
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->GRID_THM_TYPES:[I

    return-void

    .line 19
    nop

    :array_0
    .array-data 4
        0xc
        0xc
    .end array-data

    .line 20
    :array_1
    .array-data 4
        0x14
        0x14
    .end array-data

    .line 21
    :array_2
    .array-data 4
        0x2
        0x2
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;-><init>()V

    return-void
.end method

.method private findNewObj(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .locals 3
    .param p1, "groupInfo"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    .prologue
    .line 348
    iget v1, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v2, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupVGap:F

    sub-float v0, v1, v2

    .line 349
    .local v0, "scrollAmount":F
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mValidH:F

    sub-float/2addr v0, v1

    .line 351
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    .line 352
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    return-object v1
.end method

.method private fitToLine()V
    .locals 6

    .prologue
    .line 356
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-gez v4, :cond_1

    .line 381
    :cond_0
    :goto_0
    return-void

    .line 364
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    shr-int/lit8 v0, v4, 0x10

    .line 366
    .local v0, "groupIndex":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v4, v0

    .line 368
    .local v1, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v5, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float v2, v4, v5

    .line 370
    .local v2, "scrollAccu":F
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mScrollable:F

    iget v5, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v4, v5

    cmpg-float v4, v2, v4

    if-gez v4, :cond_3

    .line 371
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    if-ge v0, v4, :cond_2

    .line 372
    const/4 v3, 0x0

    .line 376
    .local v3, "scrollAmount":F
    :goto_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v4, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    goto :goto_0

    .line 374
    .end local v3    # "scrollAmount":F
    :cond_2
    iget v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    sub-float v3, v2, v4

    .restart local v3    # "scrollAmount":F
    goto :goto_1

    .line 377
    .end local v3    # "scrollAmount":F
    :cond_3
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mValidH:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mScrollable:F

    add-float/2addr v4, v5

    cmpl-float v4, v2, v4

    if-lez v4, :cond_0

    .line 378
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mValidH:F

    sub-float v3, v2, v4

    .line 379
    .restart local v3    # "scrollAmount":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v4, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    goto :goto_0
.end method

.method private getKey(II)I
    .locals 1
    .param p1, "groupIndex"    # I
    .param p2, "itemIndex"    # I

    .prologue
    .line 417
    shl-int/lit8 v0, p1, 0x10

    add-int/2addr v0, p2

    return v0
.end method

.method public static getThumbSizeType(I)I
    .locals 1
    .param p0, "level"    # I

    .prologue
    .line 30
    sget-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->GRID_THM_TYPES:[I

    aget v0, v0, p0

    return v0
.end method

.method private updateFocused(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    .locals 3
    .param p1, "direction"    # I

    .prologue
    const/4 v1, 0x0

    .line 384
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    if-nez v2, :cond_1

    .line 413
    :cond_0
    :goto_0
    return-object v1

    .line 387
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    shr-int/lit8 v0, v2, 0x10

    .line 388
    .local v0, "groupIndex":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 390
    packed-switch p1, :pswitch_data_0

    .line 412
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->getKey(II)I

    move-result v2

    iput v2, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    .line 413
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v1, v0

    goto :goto_0

    .line 392
    :pswitch_0
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    add-int/2addr v1, v0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 393
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    add-int/2addr v0, v1

    goto :goto_1

    .line 397
    :pswitch_1
    add-int/lit8 v1, v0, 0x1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 398
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 402
    :pswitch_2
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    sub-int v1, v0, v1

    if-ltz v1, :cond_2

    .line 403
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    sub-int/2addr v0, v1

    goto :goto_1

    .line 407
    :pswitch_3
    add-int/lit8 v1, v0, -0x1

    if-ltz v1, :cond_2

    .line 408
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 390
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected applyThumbnailBitmap(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V
    .locals 6
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .prologue
    const/4 v5, 0x0

    .line 507
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mUseItemSelect:Z

    if-eqz v1, :cond_0

    .line 508
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mSelected:Z

    invoke-virtual {p1, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setSelected(Z)V

    .line 509
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDisabled:Z

    invoke-virtual {p1, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setDimState(Z)V

    .line 512
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    invoke-virtual {p0, p1, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->setThumbObjectCanvas(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 530
    :goto_0
    return-void

    .line 515
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mCropRect:Landroid/graphics/Rect;

    .line 516
    .local v0, "cropRect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-virtual {p1, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setDecorState(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 517
    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    invoke-virtual {p1, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setTexCoords(FFFF)V

    .line 518
    invoke-virtual {p1, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setEmptyFill(Z)V

    .line 519
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mRotation:I

    iput v1, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mRotation:I

    .line 520
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mReorderEnable:Z

    iput-boolean v1, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mReorderEnable:Z

    .line 522
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->IsCheckMode()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 523
    iget-object v1, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispExpansionIcon:Z

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 524
    iget-object v1, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispCheckBox:Z

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    goto :goto_0

    .line 526
    :cond_2
    iget-object v1, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v1, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 527
    iget-object v1, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v1, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    goto :goto_0
.end method

.method public calcItemPosition(ILcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;)F
    .locals 13
    .param p1, "index"    # I
    .param p2, "groupInfo"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 116
    iget-object v6, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCx:[F

    .line 117
    .local v6, "cx":[F
    iget-object v7, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCy:[F

    .line 118
    .local v7, "cy":[F
    iget-object v5, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemW:[F

    .line 119
    .local v5, "cw":[F
    iget-object v3, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemH:[F

    .line 120
    .local v3, "ch":[F
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    .line 122
    .local v4, "colCount":I
    iget v8, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    iput v8, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    .line 123
    iget v8, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    iput v8, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mRow:I

    .line 125
    rem-int v8, p1, v4

    iput v8, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mGroupCol:I

    .line 126
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    div-int v8, p1, v8

    iput v8, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mGroupRow:I

    .line 128
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mValidW:F

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemGapW:F

    add-int/lit8 v10, v4, -0x1

    int-to-float v10, v10

    mul-float/2addr v9, v10

    sub-float/2addr v8, v9

    int-to-float v9, v4

    div-float v2, v8, v9

    .line 129
    .local v2, "SW":F
    move v1, v2

    .line 130
    .local v1, "SH":F
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemGapH:F

    add-float v0, v1, v8

    .line 131
    .local v0, "NH":F
    iput v0, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    .line 133
    iput v12, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mTitleCx:F

    .line 134
    neg-float v8, v0

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleTextMarginTop:F

    sub-float/2addr v8, v9

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleH:F

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    iput v8, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mTitleCy:F

    .line 136
    iget v8, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-lez v8, :cond_0

    .line 137
    aput v12, v6, v11

    .line 138
    aput v12, v7, v11

    .line 139
    aput v2, v5, v11

    .line 140
    aput v1, v3, v11

    .line 142
    :cond_0
    iget v8, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupVGap:F

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleH:F

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleTextMarginBottom:F

    add-float/2addr v8, v9

    return v8
.end method

.method public changeGroupIndex(Ljava/util/HashSet;III)V
    .locals 10
    .param p2, "offset"    # I
    .param p3, "focusIndex"    # I
    .param p4, "dragIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;",
            ">;III)V"
        }
    .end annotation

    .prologue
    .line 539
    .local p1, "objList":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;>;"
    const/4 v7, 0x0

    .line 540
    .local v7, "originalIndex":I
    const/4 v4, 0x0

    .line 541
    .local v4, "newGroupIndex":I
    const/4 v5, 0x0

    .line 543
    .local v5, "newItemIndex":I
    const/4 v3, 0x0

    .line 544
    .local v3, "isInactive":Z
    const/4 v2, 0x0

    .line 546
    .local v2, "inActivateGrpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    invoke-virtual {p0, p4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->checkAlwaysActiveGroupObjectIndex(I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 547
    const/4 v3, 0x1

    .line 550
    :cond_0
    monitor-enter p1

    .line 551
    :try_start_0
    invoke-virtual {p1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 552
    .local v0, "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getIndex()I

    move-result v7

    .line 554
    if-eqz v3, :cond_4

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGrpActiveStart:I

    shr-int/lit8 v8, v8, 0x10

    if-ne v7, v8, :cond_2

    if-ltz p2, :cond_3

    :cond_2
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGrpActiveEnd:I

    shr-int/lit8 v8, v8, 0x10

    if-ne v7, v8, :cond_4

    if-lez p2, :cond_4

    .line 555
    :cond_3
    move-object v2, v0

    .line 558
    :cond_4
    if-ne v7, p4, :cond_5

    if-eqz v3, :cond_5

    .line 559
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    shl-int/lit8 v9, p4, 0x10

    invoke-virtual {v8, v9}, Landroid/util/SparseArray;->remove(I)V

    .line 560
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v8, p4}, Landroid/util/SparseArray;->remove(I)V

    .line 563
    :cond_5
    if-ne v7, p4, :cond_6

    .line 564
    move v4, p3

    .line 569
    :goto_1
    if-ltz v4, :cond_1

    .line 572
    shl-int/lit8 v5, v4, 0x10

    .line 573
    iget-object v6, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mCoverObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 574
    .local v6, "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    iput v5, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    .line 575
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v8, v5, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 576
    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setIndex(I)V

    .line 577
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v8, v4, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 579
    .end local v0    # "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v6    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :catchall_0
    move-exception v8

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v8

    .line 566
    .restart local v0    # "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_6
    add-int v4, v7, p2

    goto :goto_1

    .line 579
    .end local v0    # "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_7
    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 581
    invoke-virtual {p0, p3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->setAlwaysActiveGroupObjectIndex(I)V

    .line 583
    if-eqz v2, :cond_9

    .line 584
    iget-object v6, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mCoverObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 585
    .restart local v6    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    invoke-virtual {v8, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->inActivateObject(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V

    .line 586
    if-eqz v2, :cond_8

    .line 587
    invoke-virtual {v2, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->remove(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 589
    :cond_8
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    invoke-virtual {v8, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->inActivateGroup(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;)V

    .line 591
    .end local v6    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_9
    return-void
.end method

.method public doQuickScroll(I)V
    .locals 4
    .param p1, "groupIndex"    # I

    .prologue
    .line 439
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v0, v2, p1

    .line 442
    .local v0, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupVGap:F

    sub-float v1, v2, v3

    .line 443
    .local v1, "scrolAmount":F
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPortraitMode:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mValidH:F

    :goto_0
    sub-float/2addr v1, v2

    .line 444
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mScrollableMax:F

    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    .line 445
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    .line 449
    :goto_1
    return-void

    .line 443
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mValidW:F

    goto :goto_0

    .line 447
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    goto :goto_1
.end method

.method public getCenteredScroll(F)F
    .locals 3
    .param p1, "scroll"    # F

    .prologue
    .line 182
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mValidH:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemH:F

    sub-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float v1, p1, v1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleH:F

    add-float v0, v1, v2

    .line 184
    .local v0, "resScroll":F
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    .line 185
    const/4 v0, 0x0

    .line 189
    :cond_0
    :goto_0
    return v0

    .line 186
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mScrollableMax:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 187
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mScrollableMax:F

    goto :goto_0
.end method

.method public getGroupPosition(ILcom/sec/android/gallery3d/glcore/GlPos;)V
    .locals 5
    .param p1, "albumIdx"    # I
    .param p2, "retPos"    # Lcom/sec/android/gallery3d/glcore/GlPos;

    .prologue
    .line 147
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v1, v1

    if-gt v1, p1, :cond_1

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v0, v1, p1

    .line 151
    .local v0, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    if-eqz v0, :cond_0

    .line 152
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemSx:F

    iget v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mMargineLeft:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemSy:F

    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mScrollable:F

    add-float/2addr v2, v3

    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    const/4 v3, 0x0

    invoke-virtual {p2, v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlPos;->set(FFF)V

    goto :goto_0
.end method

.method public getPosition(ILcom/sec/android/gallery3d/glcore/GlPos;)V
    .locals 5
    .param p1, "albumIdx"    # I
    .param p2, "retPos"    # Lcom/sec/android/gallery3d/glcore/GlPos;

    .prologue
    const/4 v3, 0x0

    .line 157
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v0, v1, p1

    .line 159
    .local v0, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    if-eqz v0, :cond_0

    .line 160
    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCx:[F

    aget v1, v1, v3

    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCy:[F

    aget v2, v2, v3

    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupVGap:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemGapH:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    const/4 v3, 0x0

    invoke-virtual {p2, v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlPos;->set(FFF)V

    .line 161
    :cond_0
    return-void
.end method

.method public getScrollForIndex(I)F
    .locals 7
    .param p1, "index"    # I

    .prologue
    const/4 v4, 0x0

    .line 168
    shr-int/lit8 v0, p1, 0x10

    .line 169
    .local v0, "albumIdx":I
    const v5, 0xffff

    and-int v2, p1, v5

    .line 170
    .local v2, "photoIdx":I
    if-ltz p1, :cond_0

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupCount:I

    if-lt v0, v5, :cond_1

    .line 177
    :cond_0
    :goto_0
    return v4

    .line 173
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v5, v0

    .line 174
    .local v1, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v5, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    if-lez v5, :cond_0

    .line 176
    iget v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    div-int v3, v2, v4

    .line 177
    .local v3, "row":I
    iget v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    int-to-float v5, v3

    iget v6, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    goto :goto_0
.end method

.method public moveTo(I)Z
    .locals 10
    .param p1, "direction"    # I

    .prologue
    const/4 v9, 0x3

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 292
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v3, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    .line 294
    .local v3, "lastFocus":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v8, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsFocusVisible:Z

    if-nez v8, :cond_5

    .line 295
    const/4 v8, 0x4

    if-eq p1, v8, :cond_1

    .line 343
    :cond_0
    :goto_0
    return v7

    .line 298
    :cond_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iput-boolean v6, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsFocusVisible:Z

    .line 299
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGrpContentStart:I

    iput v9, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    .line 300
    const/4 p1, 0x0

    .line 323
    :cond_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-ltz v8, :cond_0

    .line 326
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->updateFocused(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    move-result-object v2

    .line 328
    .local v2, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-eq v3, v8, :cond_4

    .line 329
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v8, :cond_0

    if-eqz v2, :cond_0

    .line 332
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v8, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 333
    .local v5, "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    invoke-virtual {p0, v5, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .line 335
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->fitToLine()V

    .line 336
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {v7, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 338
    .local v4, "newThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-nez v4, :cond_3

    .line 339
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->findNewObj(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    move-result-object v4

    .line 341
    :cond_3
    invoke-virtual {p0, v4, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .end local v4    # "newThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .end local v5    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_4
    move v7, v6

    .line 343
    goto :goto_0

    .line 301
    .end local v2    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    :cond_5
    if-eq p1, v9, :cond_6

    const/4 v8, 0x5

    if-ne p1, v8, :cond_2

    .line 302
    :cond_6
    if-ne p1, v9, :cond_a

    .line 303
    const/4 v0, 0x0

    .line 304
    .local v0, "column":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v8, v8

    if-lez v8, :cond_7

    .line 305
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    .line 307
    :cond_7
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    shr-int/lit8 v8, v8, 0x10

    if-ge v8, v0, :cond_9

    move v1, v6

    .line 312
    .end local v0    # "column":I
    .local v1, "focusDisappeared":Z
    :goto_1
    if-eqz v1, :cond_2

    .line 313
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {v6, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 314
    .restart local v5    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-eqz v5, :cond_8

    .line 315
    invoke-virtual {p0, v5, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .line 317
    :cond_8
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iput-boolean v7, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsFocusVisible:Z

    .line 318
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    const/4 v8, -0x1

    iput v8, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    goto/16 :goto_0

    .end local v1    # "focusDisappeared":Z
    .end local v5    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .restart local v0    # "column":I
    :cond_9
    move v1, v7

    .line 307
    goto :goto_1

    .line 309
    .end local v0    # "column":I
    :cond_a
    const/4 v1, 0x1

    .restart local v1    # "focusDisappeared":Z
    goto :goto_1
.end method

.method public onCreateThumbObj(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V
    .locals 0
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .prologue
    .line 535
    return-void
.end method

.method public resetAttributes(IZ)V
    .locals 7
    .param p1, "option"    # I
    .param p2, "portraitMode"    # Z

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 43
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getLcdRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mLcdRect:Landroid/graphics/Rect;

    .line 44
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getActionBarHeight()I

    move-result v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mResource:Landroid/content/res/Resources;

    const v5, 0x7f0d030d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mMarginTopPixel:I

    .line 45
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->IsCheckMode()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 46
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mMarginTopPixel:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d02e0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mMarginTopPixel:I

    .line 49
    :cond_0
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupVGapPixel:I

    .line 51
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mIsEasyMode:Z

    if-nez v3, :cond_4

    .line 52
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f0d0311

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleWidthPixel:I

    .line 53
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f0d030f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleHeightPixel:I

    .line 54
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f0d030e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleTextMarginTopPixel:I

    .line 55
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f0d0310

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleTextMarginBottomPixel:I

    .line 63
    :goto_0
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->resetValues(Z)V

    .line 65
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mIsEasyMode:Z

    if-eqz v3, :cond_1

    .line 66
    sget-object v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->GRID_ITEM_GAP_EASYMODE:[I

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mLevel:I

    aget v3, v3, v4

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->convX(I)F

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemGapW:F

    .line 69
    :cond_1
    sget-object v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->GRID_COLCNT:[I

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mLevel:I

    aget v3, v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    .line 70
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPortraitMode:Z

    if-nez v3, :cond_3

    .line 71
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTabletPickerMode(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 72
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mLcdRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->convX(I)F

    move-result v3

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    int-to-float v4, v4

    div-float v0, v3, v4

    .line 73
    .local v0, "minW":F
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mValidW:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    int-to-float v4, v4

    mul-float/2addr v4, v0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_3

    .line 74
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    .line 75
    .local v2, "originalPhotoLineCount":I
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mValidW:F

    div-float/2addr v3, v0

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 76
    .local v1, "newPhotoLineCount":I
    const/4 v3, 0x1

    invoke-static {v1, v3, v2}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    .line 80
    .end local v0    # "minW":F
    .end local v1    # "newPhotoLineCount":I
    .end local v2    # "originalPhotoLineCount":I
    :cond_3
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mValidW:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemGapW:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemW:F

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemH:F

    .line 81
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mSpaceWidth:F

    neg-float v3, v3

    div-float/2addr v3, v6

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mMargLeft:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemW:F

    div-float/2addr v4, v6

    add-float/2addr v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemSx:F

    .line 82
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mSpaceHeight:F

    div-float/2addr v3, v6

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mMargTop:F

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemH:F

    sub-float/2addr v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemSy:F

    .line 83
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->resetItemMaxSize()V

    .line 84
    return-void

    .line 57
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f0d0319

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleWidthPixel:I

    .line 58
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f0d0317

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleHeightPixel:I

    .line 59
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f0d0316

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleTextMarginTopPixel:I

    .line 60
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f0d0318

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleTextMarginBottomPixel:I

    goto/16 :goto_0
.end method

.method protected resetItemMaxSize()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 595
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemW:F

    .line 596
    .local v2, "tempMaxSize":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mIsEasyMode:Z

    if-nez v3, :cond_0

    .line 597
    sget-object v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->GRID_ITEM_GAP:[I

    aget v3, v3, v4

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->convX(I)F

    move-result v0

    .line 598
    .local v0, "maxLvlItemGapW":F
    sget-object v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->GRID_COLCNT:[I

    aget v1, v3, v4

    .line 599
    .local v1, "minPhotoCount":I
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPhotoLineCount:I

    if-ge v1, v3, :cond_0

    .line 600
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mValidW:F

    add-int/lit8 v4, v1, -0x1

    int-to-float v4, v4

    mul-float/2addr v4, v0

    sub-float/2addr v3, v4

    int-to-float v4, v1

    div-float v2, v3, v4

    .line 603
    .end local v0    # "maxLvlItemGapW":F
    .end local v1    # "minPhotoCount":I
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->rConvX(F)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemMaxW:F

    .line 604
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->rConvY(F)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemMaxH:F

    .line 605
    return-void
.end method

.method public resetPosition()V
    .locals 6

    .prologue
    .line 89
    const/4 v3, 0x0

    .line 91
    .local v3, "scrollable":F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupCount:I

    if-ge v1, v4, :cond_3

    .line 92
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v0, v4, v1

    .line 93
    .local v0, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    if-nez v0, :cond_0

    .line 91
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 96
    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->calcItemPosition(ILcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;)F

    move-result v2

    .line 97
    .local v2, "scrollUnit":F
    iput v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    .line 98
    iput v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    .line 99
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    rem-int v4, v1, v4

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    add-int/lit8 v5, v5, -0x1

    if-eq v4, v5, :cond_1

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupCount:I

    add-int/lit8 v4, v4, -0x1

    if-ne v1, v4, :cond_2

    .line 100
    :cond_1
    add-float/2addr v3, v2

    .line 102
    :cond_2
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemW:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemGapW:F

    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    rem-int v5, v1, v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iput v4, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mMargineLeft:F

    goto :goto_1

    .line 104
    .end local v0    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    .end local v2    # "scrollUnit":F
    :cond_3
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mValidH:F

    cmpg-float v4, v3, v4

    if-gtz v4, :cond_4

    .line 105
    const/4 v4, 0x0

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mScrollableMax:F

    .line 108
    :goto_2
    return-void

    .line 107
    :cond_4
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mValidH:F

    sub-float v4, v3, v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mScrollableMax:F

    goto :goto_2
.end method

.method public resetPosition(II)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 111
    return-void
.end method

.method protected resetValues(Z)V
    .locals 2
    .param p1, "portraitMode"    # Z

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->resetValues(Z)V

    .line 26
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mResource:Landroid/content/res/Resources;

    if-eqz p1, :cond_0

    const v0, 0x7f09008c

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->GRID_COLCNT:[I

    .line 27
    return-void

    .line 26
    :cond_0
    const v0, 0x7f09008d

    goto :goto_0
.end method

.method public setTitleFocusBorderVisible(II)V
    .locals 3
    .param p1, "oldIndex"    # I
    .param p2, "newIndex"    # I

    .prologue
    .line 424
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 425
    .local v0, "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    if-eqz v0, :cond_0

    .line 426
    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 427
    .local v1, "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->setTitleFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)V

    .line 430
    .end local v1    # "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v2, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 431
    .restart local v0    # "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    if-eqz v0, :cond_1

    .line 432
    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 433
    .restart local v1    # "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->setTitleFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)V

    .line 435
    .end local v1    # "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_1
    return-void
.end method

.method public setToCurrentCtrl()V
    .locals 3

    .prologue
    .line 36
    sget-object v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->GRID_THM_TYPES:[I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mLevel:I

    aget v0, v1, v2

    .line 37
    .local v0, "thmLevel":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v1, :cond_0

    .line 38
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->setThumbReslevel(I)V

    .line 39
    :cond_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setToCurrentCtrl()V

    .line 40
    return-void
.end method

.method public setVisibleRange()V
    .locals 15

    .prologue
    .line 196
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mValidH:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mMargBtm:F

    add-float/2addr v12, v13

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemH:F

    add-float v0, v12, v13

    .line 198
    .local v0, "bottomScroll":F
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupCount:I

    add-int/lit8 v9, v12, -0x1

    .line 199
    .local v9, "lastGrp":I
    move v8, v9

    .line 200
    .local v8, "inGrpStart":I
    move v6, v9

    .line 201
    .local v6, "inGrpEnd":I
    move v5, v9

    .line 202
    .local v5, "inCntGrpStart":I
    move v4, v9

    .line 205
    .local v4, "inCntGrpEnd":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupCount:I

    if-ge v3, v12, :cond_3

    .line 206
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v2, v12, v3

    .line 208
    .local v2, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v13, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v12, v13

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mScrollable:F

    sub-float v10, v12, v13

    .line 209
    .local v10, "scrollAmount":F
    if-ge v3, v5, :cond_0

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mExtraTop:F

    cmpg-float v12, v12, v10

    if-gtz v12, :cond_0

    .line 210
    move v5, v3

    .line 212
    :cond_0
    if-ge v3, v8, :cond_1

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mMargTop:F

    neg-float v12, v12

    cmpg-float v12, v12, v10

    if-gtz v12, :cond_1

    .line 213
    move v8, v3

    .line 215
    :cond_1
    if-ne v6, v9, :cond_2

    cmpl-float v12, v10, v0

    if-ltz v12, :cond_2

    .line 216
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    add-int/2addr v12, v3

    add-int/lit8 v6, v12, -0x1

    .line 217
    if-le v6, v9, :cond_2

    move v6, v9

    .line 219
    :cond_2
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mExtraBtm:F

    cmpl-float v12, v10, v12

    if-ltz v12, :cond_4

    .line 220
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupLineCount:I

    add-int/2addr v12, v3

    add-int/lit8 v12, v12, -0x1

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroupCount:I

    add-int/lit8 v13, v13, -0x1

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 225
    .end local v2    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    .end local v10    # "scrollAmount":F
    :cond_3
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v2, v12, v8

    .line 226
    .restart local v2    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mScrollable:F

    sub-float v1, v12, v13

    .line 227
    .local v1, "exceedAmount":F
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mMargTop:F

    neg-float v12, v12

    cmpl-float v12, v1, v12

    if-lez v12, :cond_5

    .line 228
    const/4 v7, 0x0

    .line 238
    .local v7, "inGrpIndex":I
    :goto_1
    shl-int/lit8 v12, v8, 0x10

    or-int/2addr v12, v7

    iput v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGrpActiveStart:I

    .line 240
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v2, v12, v6

    .line 241
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v13, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v12, v13

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mScrollable:F

    sub-float v1, v12, v13

    .line 242
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mValidH:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mMargBtm:F

    add-float/2addr v12, v13

    cmpg-float v12, v1, v12

    if-gtz v12, :cond_7

    .line 243
    iget v7, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    .line 253
    :goto_2
    shl-int/lit8 v12, v6, 0x10

    or-int/2addr v12, v7

    iput v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGrpActiveEnd:I

    .line 255
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v2, v12, v5

    .line 256
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mScrollable:F

    sub-float v1, v12, v13

    .line 257
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mExtraTop:F

    cmpl-float v12, v1, v12

    if-lez v12, :cond_9

    .line 258
    const/4 v7, 0x0

    .line 268
    :goto_3
    shl-int/lit8 v12, v5, 0x10

    or-int/2addr v12, v7

    iput v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGrpContentStart:I

    .line 270
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v2, v12, v4

    .line 271
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v13, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v12, v13

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mScrollable:F

    sub-float v1, v12, v13

    .line 272
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mExtraBtm:F

    cmpg-float v12, v1, v12

    if-gtz v12, :cond_b

    .line 273
    iget v7, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    .line 283
    :goto_4
    shl-int/lit8 v12, v4, 0x10

    or-int/2addr v12, v7

    iput v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGrpContentEnd:I

    .line 284
    return-void

    .line 205
    .end local v1    # "exceedAmount":F
    .end local v7    # "inGrpIndex":I
    .restart local v10    # "scrollAmount":F
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 230
    .end local v10    # "scrollAmount":F
    .restart local v1    # "exceedAmount":F
    :cond_5
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mMargTop:F

    add-float/2addr v12, v1

    neg-float v11, v12

    .line 231
    .local v11, "scrollExc":F
    const/4 v12, 0x0

    cmpg-float v12, v11, v12

    if-gtz v12, :cond_6

    .line 232
    const/4 v7, 0x0

    .restart local v7    # "inGrpIndex":I
    goto :goto_1

    .line 234
    .end local v7    # "inGrpIndex":I
    :cond_6
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    div-float v12, v11, v12

    float-to-int v7, v12

    .line 235
    .restart local v7    # "inGrpIndex":I
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    mul-int/2addr v7, v12

    goto :goto_1

    .line 245
    .end local v11    # "scrollExc":F
    :cond_7
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mValidH:F

    sub-float v13, v1, v13

    iget v14, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mMargBtm:F

    sub-float/2addr v13, v14

    sub-float v11, v12, v13

    .line 246
    .restart local v11    # "scrollExc":F
    const/4 v12, 0x0

    cmpg-float v12, v11, v12

    if-gtz v12, :cond_8

    .line 247
    const/4 v7, 0x1

    goto :goto_2

    .line 249
    :cond_8
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    div-float v12, v11, v12

    const/high16 v13, 0x3f800000    # 1.0f

    add-float/2addr v12, v13

    float-to-int v7, v12

    .line 250
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    mul-int/2addr v12, v7

    iget v13, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v7

    goto :goto_2

    .line 260
    .end local v11    # "scrollExc":F
    :cond_9
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mExtraTop:F

    sub-float v11, v12, v1

    .line 261
    .restart local v11    # "scrollExc":F
    const/4 v12, 0x0

    cmpg-float v12, v11, v12

    if-gtz v12, :cond_a

    .line 262
    const/4 v7, 0x0

    goto :goto_3

    .line 264
    :cond_a
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    div-float v12, v11, v12

    float-to-int v7, v12

    .line 265
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    mul-int/2addr v7, v12

    goto :goto_3

    .line 275
    .end local v11    # "scrollExc":F
    :cond_b
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mExtraBtm:F

    sub-float v13, v1, v13

    sub-float v11, v12, v13

    .line 276
    .restart local v11    # "scrollExc":F
    const/4 v12, 0x0

    cmpg-float v12, v11, v12

    if-gtz v12, :cond_c

    .line 277
    const/4 v7, 0x1

    goto :goto_4

    .line 279
    :cond_c
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    div-float v12, v11, v12

    const/high16 v13, 0x3f800000    # 1.0f

    add-float/2addr v12, v13

    float-to-int v7, v12

    .line 280
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    mul-int/2addr v12, v7

    iget v13, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v7

    goto :goto_4
.end method

.method protected updateThumbnail(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)Z
    .locals 14
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .prologue
    .line 456
    iget v12, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    .line 457
    .local v12, "index":I
    shr-int/lit8 v1, v12, 0x10

    .line 458
    .local v1, "albumIdx":I
    const v0, 0xffff

    and-int v2, v12, v0

    .line 459
    .local v2, "photoIdx":I
    if-ltz v1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v0, v0

    if-lt v1, v0, :cond_1

    .line 460
    :cond_0
    const/4 v0, 0x0

    .line 502
    :goto_0
    return v0

    .line 461
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v11, v0, v1

    .line 462
    .local v11, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v0, v11, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-lt v2, v0, :cond_2

    .line 463
    const/4 v0, 0x0

    goto :goto_0

    .line 464
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v3, v11, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemW:[F

    aget v3, v3, v2

    iput v3, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjWidth:F

    .line 465
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v3, v11, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemH:[F

    aget v3, v3, v2

    iput v3, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjHeight:F

    .line 466
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mUseItemSelect:Z

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispCheckBox:Z

    .line 467
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mDisplaySelectedCount:Z

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispSelectCnt:Z

    .line 468
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v3, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    iput-object v3, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 469
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispExpansionIcon:Z

    .line 470
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getFocusBorderVisible()Z

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getViewInfo(IIILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;Z)Z

    .line 471
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->applyThumbnailBitmap(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V

    .line 473
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemW:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemH:F

    invoke-virtual {p1, v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setSize(FF)V

    .line 474
    iget v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mRotation:I

    neg-int v0, v0

    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setVertexRotation(I)V

    .line 476
    iget-boolean v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mTitleVisible:Z

    if-eqz v0, :cond_5

    .line 477
    iget-object v9, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mAlbumTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 479
    .local v9, "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-nez v9, :cond_6

    .line 480
    new-instance v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .end local v9    # "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    iget-object v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-direct {v9, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    .line 481
    .restart local v9    # "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleCanvsW:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleCanvsH:I

    invoke-direct {v0, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {v9, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 482
    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setUseTouchEvent(Z)V

    .line 483
    invoke-virtual {p1, v9}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 484
    iget-object v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v3, 0x2

    invoke-virtual {v0, v9, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setObjectListeners(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;I)V

    .line 485
    iput-object v9, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mAlbumTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 490
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mResource:Landroid/content/res/Resources;

    const v3, 0x7f0c001f

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v13

    .line 491
    .local v13, "titleTextMarginLeftForMultiWindow":I
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPortraitMode:Z

    if-nez v0, :cond_4

    .line 492
    const/4 v13, 0x5

    .line 494
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    const/4 v5, -0x1

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v8, 0x0

    move v4, v1

    invoke-virtual/range {v3 .. v9}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getView(IILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v10

    .line 495
    .local v10, "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 496
    int-to-float v0, v13

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleTextMarginTop:F

    neg-float v3, v3

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemH:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleH:F

    add-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    invoke-virtual {v9, v0, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 497
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleW:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemW:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_7

    .line 498
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mItemW:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleH:F

    invoke-virtual {v9, v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    .line 502
    .end local v9    # "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .end local v10    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    .end local v13    # "titleTextMarginLeftForMultiWindow":I
    :cond_5
    :goto_2
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 486
    .restart local v9    # "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_6
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleCanvsW:I

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getWidth()I

    move-result v3

    if-eq v0, v3, :cond_3

    .line 488
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleCanvsW:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleCanvsH:I

    invoke-direct {v0, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {v9, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    goto :goto_1

    .line 500
    .restart local v10    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    .restart local v13    # "titleTextMarginLeftForMultiWindow":I
    :cond_7
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleW:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->mTitleH:F

    invoke-virtual {v9, v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    goto :goto_2
.end method
