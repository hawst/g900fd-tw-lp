.class Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;
.super Ljava/lang/Object;
.source "ContentToDisplayDialogForCloudAlbum.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->setupListViewForFormatOption(Landroid/widget/ListView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

.field final synthetic val$listView:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 469
    iput-object p1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    iput-object p2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->val$listView:Landroid/widget/ListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private isAllItemsSelected()Z
    .locals 5

    .prologue
    .line 533
    const/4 v0, 0x1

    .local v0, "i":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidFormatOptions:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$800(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 534
    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidFormatOptions:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$800(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    .line 535
    .local v2, "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v4

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->isViewByOptionEnabled(I)Z
    invoke-static {v3, v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$400(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 537
    const/4 v3, 0x0

    .line 540
    .end local v2    # "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    :goto_1
    return v3

    .line 533
    .restart local v2    # "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 540
    .end local v2    # "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    :cond_1
    const/4 v3, 0x1

    goto :goto_1
.end method

.method private updateListItem(Z)V
    .locals 6
    .param p1, "isAllContentSelected"    # Z

    .prologue
    .line 515
    const/4 v1, 0x1

    .local v1, "i":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidFormatOptions:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$800(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_3

    .line 518
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mFormatListView:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$700(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 519
    .local v0, "checkbox":Landroid/widget/CheckedTextView;
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidFormatOptions:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$800(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    .line 520
    .local v3, "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    if-eqz p1, :cond_1

    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v5

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->isViewByOptionEnabled(I)Z
    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$400(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 521
    if-eqz v0, :cond_0

    .line 522
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 523
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v5

    # |= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$076(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)I

    .line 515
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 525
    :cond_1
    if-eqz v0, :cond_2

    .line 526
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 527
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v5

    xor-int/lit8 v5, v5, -0x1

    # &= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$072(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)I

    goto :goto_1

    .line 530
    .end local v0    # "checkbox":Landroid/widget/CheckedTextView;
    .end local v3    # "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    :cond_3
    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v8, 0x42

    const/16 v7, 0x17

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 472
    if-eq p2, v8, :cond_0

    if-ne p2, v7, :cond_8

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-ne v4, v5, :cond_8

    .line 474
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->val$listView:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    .line 475
    .local v2, "position":I
    if-gez v2, :cond_2

    .line 511
    .end local v2    # "position":I
    :cond_1
    :goto_0
    return v6

    .line 477
    .restart local v2    # "position":I
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidFormatOptions:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$800(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v3

    .line 478
    .local v3, "viewType":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->val$listView:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getSelectedView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckedTextView;

    .line 479
    .local v1, "ctv":Landroid/widget/CheckedTextView;
    if-eqz v1, :cond_3

    iget-object v7, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidFormatOptions:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$800(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v4

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->isViewByOptionEnabled(I)Z
    invoke-static {v7, v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$400(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 480
    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v4

    if-nez v4, :cond_4

    move v4, v5

    :goto_1
    invoke-virtual {v1, v4}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 481
    const v4, 0x8000

    invoke-virtual {v1, v4}, Landroid/widget/CheckedTextView;->sendAccessibilityEvent(I)V

    .line 482
    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 483
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # |= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v4, v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$076(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)I

    .line 489
    :cond_3
    :goto_2
    if-nez v2, :cond_6

    .line 491
    sget-object v4, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    iget-object v5, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v4

    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->updateListItem(Z)V

    .line 506
    :goto_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->updateSaveButton()V
    invoke-static {v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$600(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)V

    goto :goto_0

    :cond_4
    move v4, v6

    .line 480
    goto :goto_1

    .line 485
    :cond_5
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    xor-int/lit8 v5, v3, -0x1

    # &= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$072(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)I

    goto :goto_2

    .line 494
    :cond_6
    invoke-direct {p0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->isAllItemsSelected()Z

    move-result v0

    .line 495
    .local v0, "allItemsChecked":Z
    if-eqz v0, :cond_7

    .line 497
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    sget-object v5, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v5

    # |= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$076(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)I

    .line 504
    :goto_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mFormatListView:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$700(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->invalidateViews()V

    goto :goto_3

    .line 499
    :cond_7
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    sget-object v5, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v5

    xor-int/lit8 v5, v5, -0x1

    # &= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$072(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)I

    goto :goto_4

    .line 507
    .end local v0    # "allItemsChecked":Z
    .end local v1    # "ctv":Landroid/widget/CheckedTextView;
    .end local v2    # "position":I
    .end local v3    # "viewType":I
    :cond_8
    if-eq p2, v8, :cond_9

    if-ne p2, v7, :cond_1

    :cond_9
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_1

    .line 509
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$200(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    goto/16 :goto_0
.end method
