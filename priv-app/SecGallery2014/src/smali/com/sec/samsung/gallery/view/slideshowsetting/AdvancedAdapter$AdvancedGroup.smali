.class public Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;
.super Ljava/lang/Object;
.source "AdvancedAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AdvancedGroup"
.end annotation


# instance fields
.field private mArrayAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private mGroupName:Ljava/lang/String;

.field private mSettingIndex:I

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;III)V
    .locals 2
    .param p2, "settingIndex"    # I
    .param p3, "groupNameId"    # I
    .param p4, "textArrayResId"    # I

    .prologue
    .line 169
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->access$000(Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->mGroupName:Ljava/lang/String;

    .line 171
    iput p2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->mSettingIndex:I

    .line 173
    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->access$000(Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0300d8

    invoke-static {v0, p4, v1}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    .line 175
    return-void
.end method


# virtual methods
.method public get(I)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public getIntSetting()I
    .locals 3

    .prologue
    .line 194
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->access$000(Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v1

    .line 195
    .local v1, "setting":Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    iget v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->mSettingIndex:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 197
    .local v0, "integer":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    return v2
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->mGroupName:Ljava/lang/String;

    return-object v0
.end method

.method public getSettingIndex()I
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->mSettingIndex:I

    return v0
.end method
