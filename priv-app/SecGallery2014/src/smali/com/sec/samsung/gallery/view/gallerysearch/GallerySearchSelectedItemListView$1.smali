.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;
.super Landroid/database/DataSetObserver;
.source "GallerySearchSelectedItemListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 17

    .prologue
    .line 62
    invoke-super/range {p0 .. p0}, Landroid/database/DataSetObserver;->onChanged()V

    .line 64
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    invoke-virtual {v14}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->getChildCount()I

    move-result v13

    .line 65
    .local v13, "viewCount":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;
    invoke-static {v14}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->getCount()I

    move-result v9

    .line 67
    .local v9, "itemCount":I
    if-ne v13, v9, :cond_1

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    if-nez v9, :cond_2

    .line 72
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    invoke-virtual {v14}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->removeAllViews()V

    .line 73
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->setVisibility(I)V

    goto :goto_0

    .line 77
    :cond_2
    if-le v13, v9, :cond_4

    move v4, v13

    .line 78
    .local v4, "count":I
    :goto_1
    if-le v13, v9, :cond_5

    const/4 v12, 0x1

    .line 80
    .local v12, "unselect":Z
    :goto_2
    if-eqz v12, :cond_8

    .line 81
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_3
    if-ge v7, v4, :cond_3

    .line 83
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    invoke-virtual {v14, v7}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 85
    .local v3, "child":Landroid/view/View;
    if-ge v7, v9, :cond_6

    .line 86
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 87
    .local v11, "src":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;
    invoke-static {v14}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;

    move-result-object v14

    invoke-virtual {v14, v7}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 89
    .local v5, "dst":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    if-eqz v11, :cond_7

    invoke-virtual {v11, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_7

    .line 90
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    invoke-virtual {v14, v7}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->removeViewAt(I)V

    .line 145
    .end local v3    # "child":Landroid/view/View;
    .end local v5    # "dst":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    .end local v11    # "src":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    :cond_3
    :goto_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    invoke-virtual {v14}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->getChildCount()I

    move-result v14

    if-lez v14, :cond_0

    .line 146
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->setVisibility(I)V

    goto :goto_0

    .end local v4    # "count":I
    .end local v7    # "i":I
    .end local v12    # "unselect":Z
    :cond_4
    move v4, v9

    .line 77
    goto :goto_1

    .line 78
    .restart local v4    # "count":I
    :cond_5
    const/4 v12, 0x0

    goto :goto_2

    .line 94
    .restart local v3    # "child":Landroid/view/View;
    .restart local v7    # "i":I
    .restart local v12    # "unselect":Z
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    invoke-virtual {v14, v7}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->removeViewAt(I)V

    .line 81
    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 100
    .end local v3    # "child":Landroid/view/View;
    .end local v7    # "i":I
    :cond_8
    sub-int v8, v9, v13

    .line 102
    .local v8, "increasedCount":I
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_5
    if-ge v7, v8, :cond_b

    .line 103
    add-int v6, v13, v7

    .line 105
    .local v6, "dstPosition":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;
    invoke-static {v14}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v14, v6, v15, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 106
    .restart local v3    # "child":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;
    invoke-static {v14}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;

    move-result-object v14

    invoke-virtual {v14, v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v3, v14}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 108
    if-lez v6, :cond_a

    .line 109
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout$LayoutParams;

    .line 111
    .local v10, "params":Landroid/widget/LinearLayout$LayoutParams;
    if-nez v10, :cond_9

    .line 112
    new-instance v10, Landroid/widget/LinearLayout$LayoutParams;

    .end local v10    # "params":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v14, -0x2

    const/4 v15, -0x1

    invoke-direct {v10, v14, v15}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 116
    .restart local v10    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mHorizontalSpacing:I
    invoke-static {v14}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->access$100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;)I

    move-result v14

    iput v14, v10, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 118
    invoke-virtual {v3, v10}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 121
    .end local v10    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_a
    const v14, 0x7f0f010c

    invoke-virtual {v3, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 123
    .local v2, "button":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    invoke-virtual {v14}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0e0041

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v14

    invoke-virtual {v2, v14}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 124
    const/4 v14, 0x1

    invoke-virtual {v2, v14}, Landroid/view/View;->setHoverPopupType(I)V

    .line 125
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    invoke-virtual {v2, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    invoke-virtual {v14, v3, v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->addView(Landroid/view/View;I)V

    .line 128
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    invoke-virtual {v14}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->getContext()Landroid/content/Context;

    move-result-object v14

    const v15, 0x7f040025

    invoke-static {v14, v15}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 129
    .local v1, "ani":Landroid/view/animation/Animation;
    invoke-virtual {v3, v1}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 130
    invoke-virtual {v3, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 102
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_5

    .line 135
    .end local v1    # "ani":Landroid/view/animation/Animation;
    .end local v2    # "button":Landroid/view/View;
    .end local v3    # "child":Landroid/view/View;
    .end local v6    # "dstPosition":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    new-instance v15, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1$1;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1$1;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;)V

    invoke-virtual {v14, v15}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_4
.end method

.method public onInvalidated()V
    .locals 0

    .prologue
    .line 152
    invoke-super {p0}, Landroid/database/DataSetObserver;->onInvalidated()V

    .line 153
    return-void
.end method
