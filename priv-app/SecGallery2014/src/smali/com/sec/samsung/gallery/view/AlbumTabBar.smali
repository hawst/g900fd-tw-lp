.class public abstract Lcom/sec/samsung/gallery/view/AlbumTabBar;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarView;
.source "AlbumTabBar.java"


# static fields
.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private mArrayAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCameraButton:Landroid/widget/ImageButton;

.field private mIsForceInitDropdownlist:Z

.field private mIsHelpMode:Z

.field private final mIsTablet:Z

.field private final mListener:Landroid/app/ActionBar$TabListener;

.field private final mOnNavigationListener:Landroid/app/ActionBar$OnNavigationListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/sec/samsung/gallery/view/AlbumTabBar;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->TAG:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 110
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;I)V

    .line 111
    return-void
.end method

.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;I)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "style"    # I

    .prologue
    const/4 v0, 0x0

    .line 102
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;I)V

    .line 52
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mCameraButton:Landroid/widget/ImageButton;

    .line 53
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mIsHelpMode:Z

    .line 54
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mIsForceInitDropdownlist:Z

    .line 56
    new-instance v1, Lcom/sec/samsung/gallery/view/AlbumTabBar$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar$1;-><init>(Lcom/sec/samsung/gallery/view/AlbumTabBar;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mOnNavigationListener:Landroid/app/ActionBar$OnNavigationListener;

    .line 78
    new-instance v1, Lcom/sec/samsung/gallery/view/AlbumTabBar$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar$2;-><init>(Lcom/sec/samsung/gallery/view/AlbumTabBar;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mListener:Landroid/app/ActionBar$TabListener;

    .line 104
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTapMenu:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mIsTablet:Z

    .line 105
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->isHelpMode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mIsHelpMode:Z

    .line 106
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->initActionBarTabs()V

    .line 107
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AlbumTabBar;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mIsHelpMode:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/AlbumTabBar;Lcom/sec/samsung/gallery/core/TabTagType;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AlbumTabBar;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->isReselectedTab(Lcom/sec/samsung/gallery/core/TabTagType;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/AlbumTabBar;I)Landroid/app/ActionBar$Tab;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AlbumTabBar;
    .param p1, "x1"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->createTab(I)Landroid/app/ActionBar$Tab;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/AlbumTabBar;Landroid/app/ActionBar$Tab;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AlbumTabBar;
    .param p1, "x1"    # Landroid/app/ActionBar$Tab;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->checkSelected(Landroid/app/ActionBar$Tab;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/AlbumTabBar;Lcom/sec/samsung/gallery/core/TabTagType;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AlbumTabBar;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->handleFilterAlbums(Lcom/sec/samsung/gallery/core/TabTagType;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AlbumTabBar;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mIsTablet:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AlbumTabBar;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/samsung/gallery/view/AlbumTabBar;Landroid/widget/ArrayAdapter;)Landroid/widget/ArrayAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AlbumTabBar;
    .param p1, "x1"    # Landroid/widget/ArrayAdapter;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AlbumTabBar;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mIsForceInitDropdownlist:Z

    return v0
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/view/AlbumTabBar;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AlbumTabBar;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mIsForceInitDropdownlist:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AlbumTabBar;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mCameraButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/samsung/gallery/view/AlbumTabBar;Landroid/widget/ImageButton;)Landroid/widget/ImageButton;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AlbumTabBar;
    .param p1, "x1"    # Landroid/widget/ImageButton;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mCameraButton:Landroid/widget/ImageButton;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/AlbumTabBar;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AlbumTabBar;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->updateCurrentSelectedTabType()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AlbumTabBar;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->tabAlreadyAdded()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Landroid/app/ActionBar$OnNavigationListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AlbumTabBar;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mOnNavigationListener:Landroid/app/ActionBar$OnNavigationListener;

    return-object v0
.end method

.method public static changeFilterAlbums(Landroid/content/Context;Lcom/sec/android/gallery3d/app/StateManager;Lcom/sec/samsung/gallery/core/TabTagType;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "statusProxy"    # Lcom/sec/android/gallery3d/app/StateManager;
    .param p2, "selectedTabTag"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    const/4 v5, 0x1

    .line 421
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewMode()Ljava/lang/Class;

    move-result-object v1

    .line 422
    .local v1, "currentViewState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    invoke-virtual {p1, p2}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 424
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHoveringUI:Z

    if-eqz v3, :cond_0

    .line 426
    const/4 v3, 0x1

    const/4 v4, -0x1

    :try_start_0
    invoke-static {v3, v4}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 432
    :cond_0
    :goto_0
    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne p2, v3, :cond_2

    .line 433
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 434
    .local v0, "bundle":Landroid/os/Bundle;
    const-class v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-ne v1, v3, :cond_1

    .line 435
    const-string v3, "KEY_VIEW_REDRAW"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 437
    :cond_1
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    const-class v4, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 478
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_1
    return-void

    .line 427
    .restart local p0    # "context":Landroid/content/Context;
    :catch_0
    move-exception v2

    .line 428
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 438
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_2
    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALL:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne p2, v3, :cond_3

    .line 439
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 440
    .restart local v0    # "bundle":Landroid/os/Bundle;
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    const-class v4, Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_1

    .line 441
    .end local v0    # "bundle":Landroid/os/Bundle;
    .restart local p0    # "context":Landroid/content/Context;
    :cond_3
    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne p2, v3, :cond_4

    .line 442
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 443
    .restart local v0    # "bundle":Landroid/os/Bundle;
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    const-class v4, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_1

    .line 444
    .end local v0    # "bundle":Landroid/os/Bundle;
    .restart local p0    # "context":Landroid/content/Context;
    :cond_4
    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_LOCATION:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne p2, v3, :cond_7

    .line 445
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTestLocationView:Z

    if-eqz v3, :cond_6

    .line 446
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 447
    .restart local v0    # "bundle":Landroid/os/Bundle;
    const-class v3, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-ne v1, v3, :cond_5

    .line 448
    const-string v3, "KEY_VIEW_REDRAW"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 450
    :cond_5
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    const-class v4, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_1

    .end local v0    # "bundle":Landroid/os/Bundle;
    .restart local p0    # "context":Landroid/content/Context;
    :cond_6
    move-object v3, p0

    .line 452
    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v3

    const-string v4, "MAP_VIEW_START"

    invoke-virtual {v3, v4, p0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 454
    :cond_7
    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne p2, v3, :cond_9

    .line 455
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 456
    .restart local v0    # "bundle":Landroid/os/Bundle;
    const-class v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-ne v1, v3, :cond_8

    .line 457
    const-string v3, "KEY_VIEW_REDRAW"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 459
    :cond_8
    const-string v3, "KEY_VIEW_FACE"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 460
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    const-class v4, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 461
    .end local v0    # "bundle":Landroid/os/Bundle;
    .restart local p0    # "context":Landroid/content/Context;
    :cond_9
    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SECRET_BOX:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne p2, v3, :cond_b

    .line 462
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 463
    .restart local v0    # "bundle":Landroid/os/Bundle;
    const-class v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-ne v1, v3, :cond_a

    .line 464
    const-string v3, "KEY_VIEW_REDRAW"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 466
    :cond_a
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    const-class v4, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 467
    .end local v0    # "bundle":Landroid/os/Bundle;
    .restart local p0    # "context":Landroid/content/Context;
    :cond_b
    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_EVENT:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne p2, v3, :cond_d

    .line 468
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 469
    .restart local v0    # "bundle":Landroid/os/Bundle;
    const-class v3, Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    if-ne v1, v3, :cond_c

    .line 470
    const-string v3, "KEY_VIEW_REDRAW"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 472
    :cond_c
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    const-class v4, Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 474
    .end local v0    # "bundle":Landroid/os/Bundle;
    .restart local p0    # "context":Landroid/content/Context;
    :cond_d
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 475
    .restart local v0    # "bundle":Landroid/os/Bundle;
    const-string v3, "KEY_VIEW_REDRAW"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 476
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3, v1, v0}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_1
.end method

.method private checkSelected(Landroid/app/ActionBar$Tab;)Z
    .locals 3
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;

    .prologue
    .line 403
    const/4 v1, 0x0

    .line 404
    .local v1, "ret":Z
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v0

    .line 405
    .local v0, "currentTabTag":Lcom/sec/samsung/gallery/core/TabTagType;
    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v2, v0, :cond_0

    .line 406
    const/4 v1, 0x1

    .line 408
    :cond_0
    return v1
.end method

.method private createTab(I)Landroid/app/ActionBar$Tab;
    .locals 5
    .param p1, "id"    # I

    .prologue
    .line 381
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 382
    .local v1, "v":Landroid/widget/LinearLayout;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03000b

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 384
    .local v0, "t":Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 385
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 386
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/ActionBar$Tab;->setCustomView(Landroid/view/View;)Landroid/app/ActionBar$Tab;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mListener:Landroid/app/ActionBar$TabListener;

    invoke-virtual {v2, v3}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    move-result-object v2

    return-object v2
.end method

.method private handleFilterAlbums(Lcom/sec/samsung/gallery/core/TabTagType;)V
    .locals 2
    .param p1, "selectedTabTag"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    .line 417
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->changeFilterAlbums(Landroid/content/Context;Lcom/sec/android/gallery3d/app/StateManager;Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 418
    return-void
.end method

.method private initActionBarTabs()V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;-><init>(Lcom/sec/samsung/gallery/view/AlbumTabBar;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 260
    return-void
.end method

.method private isReselectedTab(Lcom/sec/samsung/gallery/core/TabTagType;)Z
    .locals 2
    .param p1, "selectedTabTag"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    .line 412
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v0

    .line 413
    .local v0, "currentTabTag":Lcom/sec/samsung/gallery/core/TabTagType;
    if-ne v0, p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private tabAlreadyAdded()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 373
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mIsTablet:Z

    if-eqz v2, :cond_2

    .line 374
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->getTabCount()I

    move-result v2

    if-eqz v2, :cond_1

    .line 376
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 374
    goto :goto_0

    .line 376
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->getNavigationMode()I

    move-result v2

    if-ne v2, v0, :cond_3

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->getNavigationItemCount()I

    move-result v2

    if-gtz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private updateCurrentSelectedTabType()V
    .locals 3

    .prologue
    .line 525
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    if-eqz v2, :cond_0

    .line 526
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->getSelectedNavigationIndex()I

    move-result v0

    .line 528
    .local v0, "navigationIndex":I
    if-ltz v0, :cond_0

    .line 529
    invoke-static {v0}, Lcom/sec/samsung/gallery/core/TabTagType;->from(I)Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    .line 530
    .local v1, "selectedTabTag":Lcom/sec/samsung/gallery/core/TabTagType;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 531
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 534
    .end local v0    # "navigationIndex":I
    .end local v1    # "selectedTabTag":Lcom/sec/samsung/gallery/core/TabTagType;
    :cond_0
    return-void
.end method


# virtual methods
.method public changeViewByType(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 504
    sget-object v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;->LOCAL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 506
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->tabAlreadyAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 507
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mOnNavigationListener:Landroid/app/ActionBar$OnNavigationListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    .line 509
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 510
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/TabTagType;->from(Lcom/sec/samsung/gallery/core/TabTagType;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 514
    :goto_0
    return-void

    .line 512
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    goto :goto_0
.end method

.method protected hideMenues(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 481
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/AlbumTabBar$7;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/AlbumTabBar$7;-><init>(Lcom/sec/samsung/gallery/view/AlbumTabBar;Landroid/view/Menu;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 501
    return-void
.end method

.method protected hideTabs()V
    .locals 2

    .prologue
    .line 392
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/AlbumTabBar$6;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar$6;-><init>(Lcom/sec/samsung/gallery/view/AlbumTabBar;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 400
    return-void
.end method

.method public onConfChanged()V
    .locals 3

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getTabCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/core/TabTagType;->getIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->getTabAt(I)Landroid/app/ActionBar$Tab;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    .line 269
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->initActionBarTabs()V

    .line 270
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->showTabs()V

    .line 271
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->hideTabs()V

    .line 99
    return-void
.end method

.method public onTabChanged()V
    .locals 3

    .prologue
    .line 274
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mIsForceInitDropdownlist:Z

    .line 275
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->initActionBarTabs()V

    .line 276
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mOnNavigationListener:Landroid/app/ActionBar$OnNavigationListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    .line 277
    return-void
.end method

.method protected setupButtons()V
    .locals 2

    .prologue
    .line 281
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 320
    :goto_0
    return-void

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/AlbumTabBar$4;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar$4;-><init>(Lcom/sec/samsung/gallery/view/AlbumTabBar;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public showCameraButton(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 518
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 519
    .local v0, "visibility":I
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mCameraButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_0

    .line 520
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mCameraButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 522
    :cond_0
    return-void

    .line 518
    .end local v0    # "visibility":I
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected showTabs()V
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;-><init>(Lcom/sec/samsung/gallery/view/AlbumTabBar;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 370
    return-void
.end method
