.class Lcom/sec/samsung/gallery/glview/GlSearchView$7;
.super Ljava/lang/Object;
.source "GlSearchView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlSearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V
    .locals 0

    .prologue
    .line 479
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 6
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 481
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->isScreenLocked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 498
    :cond_0
    :goto_0
    return v5

    :cond_1
    move-object v1, p1

    .line 484
    check-cast v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget v0, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 486
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$800(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 487
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->mMode:I

    sget v2, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_NORMAL:I

    if-ne v1, v2, :cond_3

    .line 488
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 493
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    if-eqz v1, :cond_0

    .line 494
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iput v0, v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFocused:I

    .line 495
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlSearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 496
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v0, v4, v4}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setCommand(IIII)V

    goto :goto_0

    .line 490
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$800(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    goto :goto_1
.end method
