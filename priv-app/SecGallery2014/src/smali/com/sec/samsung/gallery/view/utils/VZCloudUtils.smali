.class public final Lcom/sec/samsung/gallery/view/utils/VZCloudUtils;
.super Ljava/lang/Object;
.source "VZCloudUtils.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static final VZW_CLOUD_GALLERY:Ljava/lang/String; = "com.vcast.mediamanager.ACTION_PICTURES"

.field private static final VZW_ClOUD_PKGNAME:Ljava/lang/String; = "com.vcast.mediamanager"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/samsung/gallery/view/utils/VZCloudUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/utils/VZCloudUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getVZCloudAppName(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 25
    .local v4, "pm":Landroid/content/pm/PackageManager;
    const/4 v2, 0x0

    .line 27
    .local v2, "appName":Ljava/lang/String;
    :try_start_0
    const-string v5, "com.vcast.mediamanager"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 28
    .local v1, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget v5, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_0

    iget-boolean v5, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v5, :cond_0

    .line 29
    invoke-virtual {v4, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    .end local v1    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    return-object v2

    .line 31
    :catch_0
    move-exception v3

    .line 32
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v5, Lcom/sec/samsung/gallery/view/utils/VZCloudUtils;->TAG:Ljava/lang/String;

    const-string v6, "getVZCloudAppName|Package not found"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static launchVZCloud(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 53
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 55
    .local v1, "vzIntent":Landroid/content/Intent;
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :goto_0
    return-void

    .line 56
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/sec/samsung/gallery/view/utils/VZCloudUtils;->TAG:Ljava/lang/String;

    const-string v3, "launchVZCloud| intent not found"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static prepareOptionsMenu(Landroid/content/Context;Landroid/view/Menu;I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "menuitem"    # I

    .prologue
    .line 38
    invoke-interface {p1, p2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 39
    .local v1, "vzCloud":Landroid/view/MenuItem;
    if-nez v1, :cond_1

    .line 50
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 43
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsVZW:Z

    if-eqz v2, :cond_0

    .line 44
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/utils/VZCloudUtils;->getVZCloudAppName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "appName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 46
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 47
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_0
.end method
