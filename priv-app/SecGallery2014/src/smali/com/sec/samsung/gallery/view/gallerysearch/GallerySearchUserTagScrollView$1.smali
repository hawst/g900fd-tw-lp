.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$1;
.super Landroid/os/Handler;
.source "GallerySearchUserTagScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;)V
    .locals 0

    .prologue
    .line 984
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 986
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->makeTagList()V

    .line 987
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v3, "USER_TAG_LOADING_FINISHED"

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;)V

    .line 988
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    if-eqz v2, :cond_0

    .line 989
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    .line 991
    .local v1, "state":Lcom/sec/android/gallery3d/app/ActivityState;
    if-eqz v1, :cond_0

    :try_start_0
    instance-of v2, v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    if-eqz v2, :cond_0

    .line 992
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .end local v1    # "state":Lcom/sec/android/gallery3d/app/ActivityState;
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->getTagFilterHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->setFilterTagViewHeight(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 998
    :cond_0
    :goto_0
    return-void

    .line 994
    :catch_0
    move-exception v0

    .line 995
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
