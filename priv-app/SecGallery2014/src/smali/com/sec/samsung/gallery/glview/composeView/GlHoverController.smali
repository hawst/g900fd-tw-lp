.class public Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;
.super Ljava/lang/Object;
.source "GlHoverController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$GlHoverLabelListener;,
        Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;
    }
.end annotation


# static fields
.field private static COUNT_COL:[I = null

.field private static COUNT_ROW:[I = null

.field private static final HOVER_END_EVT:I = 0x3

.field private static final HOVER_EXIT_INTERVAL:J = 0x190L

.field private static final HOVER_EXIT_INTERVAL_LONG:J = 0x7d0L

.field private static final HOVER_NEXT_CRIT_DELTA:J = 0x50L

.field private static final HOVER_NEXT_SET:I = 0x5

.field private static final HOVER_PREV_SET:I = 0x4

.field private static final HOVER_START_ANIM:I = 0x2

.field private static final HOVER_START_INTERVAL:J = 0x190L

.field private static final HOVER_START_REQ:I = 0x1

.field private static final HOVER_STATE_ACTIVE:I = 0x2

.field private static final HOVER_STATE_DESTROYING:I = 0x3

.field private static final HOVER_STATE_IDLE:I = 0x0

.field private static final HOVER_STATE_STARTED:I = 0x1

.field private static final MAX_THUMBNAIL_COUNT:I = 0x9

.field private static final RES_ID_POPUP_BACKGROUND:I = 0x0

.field private static final RES_ID_SUBID_TITLE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "GlHoverController"


# instance fields
.field private AIRV:Ljava/lang/String;

.field protected final BOUND_HGAP_WITH_ACTIONBAR:I

.field protected final BOUND_VGAP_WITH_ACTIONBAR:I

.field private mActionBarHeight:F

.field private mActionBarMinHeight:F

.field private mActionBarMinWidth:F

.field private mActionBarWidth:F

.field private mActiveCount:I

.field private mAppearAnimListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

.field private mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field private mBackgroundShadowSize:F

.field private mBgCommonPaddingInside:F

.field private mBgHeight:F

.field private mBgPaddingBottom:F

.field private mBgPaddingLeft:F

.field private mBgPaddingRight:F

.field private mBgPaddingTop:F

.field private mBgWidth:F

.field private mContext:Landroid/content/Context;

.field private mDeleteActionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

.field private mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

.field private mDisappearAnimListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

.field private mDisplayItemList:[Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;

.field private mEditActionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

.field private mEnabled:Z

.field private mFirstIndex:I

.field private mFlashAnnotateActionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

.field private mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

.field mGlHoverLabelListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$GlHoverLabelListener;

.field private mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

.field private mHoverState:I

.field private mHoveringOffset:F

.field private mImageAreaHeight:F

.field private mImageAreaWidth:F

.field private mIndex:I

.field private mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

.field private mIsActionBarEnabled:Z

.field private mIsSet:Z

.field private mItemGapH:F

.field private mItemGapW:F

.field private mItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private mLabel:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field private mLabelHeight:F

.field private mLabelView:Lcom/sec/android/gallery3d/glcore/GlView;

.field private mLabelWidth:F

.field private mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

.field mListenerBitmap:Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem$OnBitmapAvailableListener;

.field mListenerClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

.field mListenerHover:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

.field mListenerMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

.field private mLoadedCount:I

.field private mMainObj:Lcom/sec/android/gallery3d/glcore/GlObject;

.field private mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mNeedDelay:Z

.field private mNextRequested:Z

.field private mObjSet:[Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field private mOnHoverListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;

.field private mPopupHeight:I

.field private mRatioHeight:F

.field private mRatioWidth:F

.field private mReqIndex:I

.field private mReqMedia:Lcom/sec/android/gallery3d/data/MediaObject;

.field private mReqObj:Lcom/sec/android/gallery3d/glcore/GlObject;

.field private mScreenBottom:F

.field private mScreenLeft:F

.field private mScreenRight:F

.field private mScreenTop:F

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mShareActionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

.field private mStartTime:J

.field private mThmH:F

.field private mThmW:F

.field private mThumbnailType:I

.field private mTotalCount:I

.field private mTransAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

.field private mVibrator:Landroid/os/Vibrator;

.field mVideoPlayIconCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xa

    .line 79
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->COUNT_COL:[I

    .line 80
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->COUNT_ROW:[I

    return-void

    .line 79
    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x2
        0x2
        0x3
        0x3
        0x3
        0x3
        0x3
    .end array-data

    .line 80
    :array_1
    .array-data 4
        0x0
        0x1
        0x1
        0x2
        0x2
        0x2
        0x2
        0x3
        0x3
        0x3
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;Z)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p3, "listener"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;
    .param p4, "viewBySet"    # Z

    .prologue
    const/16 v4, 0x24

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->BOUND_HGAP_WITH_ACTIONBAR:I

    .line 77
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->BOUND_VGAP_WITH_ACTIONBAR:I

    .line 90
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 91
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 103
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoverState:I

    .line 110
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mFirstIndex:I

    .line 124
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mVideoPlayIconCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 130
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsActionBarEnabled:Z

    .line 137
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/glcore/GlView;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabelView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 141
    const-string v1, "AIRV"

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->AIRV:Ljava/lang/String;

    .line 143
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mNeedDelay:Z

    .line 1069
    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$6;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$6;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mListenerBitmap:Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem$OnBitmapAvailableListener;

    .line 1087
    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$7;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$7;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mListenerMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    .line 1114
    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$8;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$8;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mListenerClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 1138
    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$9;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$9;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mListenerHover:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

    .line 1160
    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$10;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$10;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mAppearAnimListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 1173
    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$11;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$11;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDisappearAnimListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 1200
    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$12;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$12;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlHoverLabelListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$GlHoverLabelListener;

    move-object v0, p1

    .line 146
    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 147
    .local v0, "galleryActivity":Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    .line 148
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 149
    iput-object p3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mOnHoverListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;

    .line 150
    iput-boolean p4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsSet:Z

    .line 151
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->setActionBarEnabled()V

    .line 152
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mEnabled:Z

    .line 153
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 154
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 155
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "vibrator"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mVibrator:Landroid/os/Vibrator;

    .line 156
    new-instance v1, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr2;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr2;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    .line 157
    const/16 v1, 0x9

    new-array v1, v1, [Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDisplayItemList:[Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;

    .line 158
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->initAttribute()V

    .line 159
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsActionBarEnabled:Z

    if-eqz v1, :cond_0

    .line 160
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->initActionBarListeners()V

    .line 161
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->createObjects()V

    .line 162
    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->getGlRoot()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    .line 198
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsSet:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;
    .param p1, "x1"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->prepareNextSet(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->exitFromHoverViewWithDelay()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mItemList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mStartTime:J

    return-wide v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLoadedCount:I

    return v0
.end method

.method static synthetic access$1408(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLoadedCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLoadedCount:I

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActiveCount:I

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/android/gallery3d/glcore/GlHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mNeedDelay:Z

    return v0
.end method

.method static synthetic access$1702(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mNeedDelay:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mTotalCount:I

    return v0
.end method

.method static synthetic access$1900(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mOnHoverListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->prepareSetAnimation()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)[Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDisplayItemList:[Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIndex:I

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mFirstIndex:I

    return v0
.end method

.method static synthetic access$2300(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabel:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoverState:I

    return p1
.end method

.method static synthetic access$2500(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->drawLabel(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;II)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->prepareItemAnimation()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/android/gallery3d/glcore/GlLayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsActionBarEnabled:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsActionBarEnabled:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->setActionBarEnabled()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->AIRV:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->checkRemoveHoverView()V

    return-void
.end method

.method private checkRemoveHoverView()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 894
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->getActiveObject()Ljava/lang/Object;

    move-result-object v0

    .line 896
    .local v0, "obj":Ljava/lang/Object;
    const-string v3, "GlHoverController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkRemoveHoverView = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMainObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eq v0, v1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", state = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoverState:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 897
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMainObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eq v0, v1, :cond_0

    .line 898
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->removeHoverViewInter(Z)V

    .line 899
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIndex:I

    .line 901
    :cond_0
    return-void

    .line 896
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private createActionBar(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V
    .locals 10
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .prologue
    .line 659
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    if-nez v6, :cond_0

    .line 660
    new-instance v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    invoke-direct {v6, v7, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;Landroid/content/Context;)V

    iput-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    .line 661
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->removeBGChildview()V

    .line 662
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    invoke-virtual {p1, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 664
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    invoke-direct {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 665
    .local v0, "actionBarBackgroundView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    new-instance v5, Landroid/graphics/drawable/ColorDrawable;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 666
    .local v5, "transparentDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 668
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    invoke-virtual {v6, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->setBackGroundView(Lcom/sec/android/gallery3d/glcore/GlImageView;)V

    .line 669
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringActionBarButtonMarginLeft()I

    move-result v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringActionBarButtonMarginRight()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->setButtonsMargin(II)V

    .line 680
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v6, :cond_1

    .line 681
    const v6, 0x7f020046

    const v7, 0x7f020047

    const v8, 0x7f020048

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDeleteActionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-direct {p0, v6, v7, v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->createGlActionBarButton(IIILcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)Lcom/sec/samsung/gallery/glview/GlButtonView;

    move-result-object v1

    .line 684
    .local v1, "deleteActionBtn":Lcom/sec/samsung/gallery/glview/GlButtonView;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    const/4 v7, 0x4

    invoke-virtual {v6, v1, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->addActionBarItem(Lcom/sec/samsung/gallery/glview/GlButtonView;I)V

    .line 686
    const v6, 0x7f020058

    const v7, 0x7f020059

    const v8, 0x7f02005a

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mShareActionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-direct {p0, v6, v7, v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->createGlActionBarButton(IIILcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)Lcom/sec/samsung/gallery/glview/GlButtonView;

    move-result-object v4

    .line 689
    .local v4, "shareActionButton":Lcom/sec/samsung/gallery/glview/GlButtonView;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    const/4 v7, 0x3

    invoke-virtual {v6, v4, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->addActionBarItem(Lcom/sec/samsung/gallery/glview/GlButtonView;I)V

    .line 691
    const v6, 0x7f02004c

    const v7, 0x7f02004d

    const v8, 0x7f02004e

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mEditActionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-direct {p0, v6, v7, v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->createGlActionBarButton(IIILcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)Lcom/sec/samsung/gallery/glview/GlButtonView;

    move-result-object v2

    .line 694
    .local v2, "editActionBtn":Lcom/sec/samsung/gallery/glview/GlButtonView;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    const/4 v7, 0x2

    invoke-virtual {v6, v2, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->addActionBarItem(Lcom/sec/samsung/gallery/glview/GlButtonView;I)V

    .line 696
    const v6, 0x7f020052

    const v7, 0x7f020053

    const v8, 0x7f020054

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mFlashAnnotateActionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-direct {p0, v6, v7, v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->createGlActionBarButton(IIILcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)Lcom/sec/samsung/gallery/glview/GlButtonView;

    move-result-object v3

    .line 699
    .local v3, "flashAnnotateActionBtn":Lcom/sec/samsung/gallery/glview/GlButtonView;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    const/4 v7, 0x1

    invoke-virtual {v6, v3, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->addActionBarItem(Lcom/sec/samsung/gallery/glview/GlButtonView;I)V

    .line 722
    .end local v0    # "actionBarBackgroundView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v1    # "deleteActionBtn":Lcom/sec/samsung/gallery/glview/GlButtonView;
    .end local v2    # "editActionBtn":Lcom/sec/samsung/gallery/glview/GlButtonView;
    .end local v3    # "flashAnnotateActionBtn":Lcom/sec/samsung/gallery/glview/GlButtonView;
    .end local v4    # "shareActionButton":Lcom/sec/samsung/gallery/glview/GlButtonView;
    .end local v5    # "transparentDrawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    :goto_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->setVisibility(Z)V

    .line 723
    return-void

    .line 701
    .restart local v0    # "actionBarBackgroundView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .restart local v5    # "transparentDrawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    const v6, 0x7f020044

    const v7, 0x7f020045

    const v8, 0x7f020049

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDeleteActionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-direct {p0, v6, v7, v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->createGlActionBarButton(IIILcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)Lcom/sec/samsung/gallery/glview/GlButtonView;

    move-result-object v1

    .line 704
    .restart local v1    # "deleteActionBtn":Lcom/sec/samsung/gallery/glview/GlButtonView;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    const/4 v7, 0x4

    invoke-virtual {v6, v1, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->addActionBarItem(Lcom/sec/samsung/gallery/glview/GlButtonView;I)V

    .line 706
    const v6, 0x7f020056

    const v7, 0x7f020057

    const v8, 0x7f02005b

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mShareActionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-direct {p0, v6, v7, v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->createGlActionBarButton(IIILcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)Lcom/sec/samsung/gallery/glview/GlButtonView;

    move-result-object v4

    .line 709
    .restart local v4    # "shareActionButton":Lcom/sec/samsung/gallery/glview/GlButtonView;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    const/4 v7, 0x3

    invoke-virtual {v6, v4, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->addActionBarItem(Lcom/sec/samsung/gallery/glview/GlButtonView;I)V

    .line 711
    const v6, 0x7f02004a

    const v7, 0x7f02004b

    const v8, 0x7f02004f

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mEditActionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-direct {p0, v6, v7, v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->createGlActionBarButton(IIILcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)Lcom/sec/samsung/gallery/glview/GlButtonView;

    move-result-object v2

    .line 714
    .restart local v2    # "editActionBtn":Lcom/sec/samsung/gallery/glview/GlButtonView;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    const/4 v7, 0x2

    invoke-virtual {v6, v2, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->addActionBarItem(Lcom/sec/samsung/gallery/glview/GlButtonView;I)V

    .line 716
    const v6, 0x7f020050

    const v7, 0x7f020051

    const v8, 0x7f020055

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mFlashAnnotateActionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-direct {p0, v6, v7, v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->createGlActionBarButton(IIILcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)Lcom/sec/samsung/gallery/glview/GlButtonView;

    move-result-object v3

    .line 719
    .restart local v3    # "flashAnnotateActionBtn":Lcom/sec/samsung/gallery/glview/GlButtonView;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    const/4 v7, 0x1

    invoke-virtual {v6, v3, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->addActionBarItem(Lcom/sec/samsung/gallery/glview/GlButtonView;I)V

    goto :goto_0
.end method

.method private createGlActionBarButton(IIILcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)Lcom/sec/samsung/gallery/glview/GlButtonView;
    .locals 3
    .param p1, "drawableId"    # I
    .param p2, "focusedDrawableId"    # I
    .param p3, "pressedDrawableId"    # I
    .param p4, "actionClickListener"    # Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .prologue
    .line 617
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlButtonView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-object v2, v2, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlButtonView;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    .line 618
    .local v0, "actionBarButton":Lcom/sec/samsung/gallery/glview/GlButtonView;
    const/4 v1, 0x3

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlButtonView;->setAlign(II)V

    .line 619
    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/GlButtonView;->setImageResource(I)V

    .line 620
    invoke-virtual {v0, p2}, Lcom/sec/samsung/gallery/glview/GlButtonView;->setFocusedDrawableId(I)V

    .line 621
    invoke-virtual {v0, p3}, Lcom/sec/samsung/gallery/glview/GlButtonView;->setPressedDrawableId(I)V

    .line 622
    invoke-virtual {v0, p4}, Lcom/sec/samsung/gallery/glview/GlButtonView;->setClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)V

    .line 623
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlHoverLabelListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$GlHoverLabelListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlButtonView;->setHoverLabelListener(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$GlHoverLabelListener;)V

    .line 624
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringActionBarButtonWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringActionBarButtonHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlButtonView;->setSize(II)V

    .line 626
    return-object v0
.end method

.method private createObjects()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/16 v6, 0x9

    const/4 v5, 0x0

    .line 907
    new-instance v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-direct {v2, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 908
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAlpha(F)V

    .line 909
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsSet:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mListenerHover:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

    :goto_0
    invoke-virtual {v4, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setHoverListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;)V

    .line 910
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsSet:Z

    if-eqz v4, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mListenerMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    :cond_0
    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 911
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setUseTouchEvent(Z)V

    .line 912
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getTransAnim()Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mTransAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 915
    new-array v2, v6, [Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mObjSet:[Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 916
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v6, :cond_2

    .line 917
    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    .line 918
    .local v1, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mObjSet:[Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    aput-object v1, v2, v0

    .line 919
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 920
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mListenerMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 921
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mListenerClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)V

    .line 922
    invoke-virtual {v1, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 916
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    .end local v1    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_1
    move-object v2, v3

    .line 909
    goto :goto_0

    .line 925
    .restart local v0    # "i":I
    :cond_2
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsActionBarEnabled:Z

    if-eqz v2, :cond_3

    .line 926
    new-instance v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabel:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 927
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabel:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 930
    :cond_3
    return-void
.end method

.method private createVideoPlayIconObject()Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .locals 2

    .prologue
    .line 205
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    .line 206
    .local v0, "videoPlayIconObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->getVideoPlayIconCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 207
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setUseTouchEvent(Z)V

    .line 208
    return-object v0
.end method

.method private drawButtonLabel(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Lcom/sec/android/gallery3d/glcore/GlView;II)I
    .locals 15
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .param p2, "glView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "margine"    # I
    .param p4, "id"    # I

    .prologue
    .line 553
    const/4 v6, 0x0

    .line 554
    .local v6, "nTextWidth":I
    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 557
    .local v8, "popupView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0d03b3

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    .line 558
    .local v4, "fontSize":F
    invoke-static {v4}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getDefaultPaintForLabel(F)Landroid/text/TextPaint;

    move-result-object v7

    .line 559
    .local v7, "paint":Landroid/text/TextPaint;
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    move/from16 v0, p4

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->getLabelNameId(I)I

    move-result v12

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 561
    .local v5, "labelName":Ljava/lang/String;
    const/4 v11, 0x0

    const/high16 v12, 0x43f00000    # 480.0f

    invoke-static {v5, v11, v12, v7}, Lcom/sec/android/gallery3d/glcore/GlTextView;->ellipsizeString(Ljava/lang/String;Ljava/lang/String;FLandroid/text/TextPaint;)Ljava/lang/String;

    move-result-object v10

    .line 562
    .local v10, "title":Ljava/lang/String;
    const/4 v9, 0x0

    .line 563
    .local v9, "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    const/16 v3, 0xb4

    .line 564
    .local v3, "extraW":I
    const/16 v2, 0x46

    .line 566
    .local v2, "extraH":I
    if-nez v8, :cond_4

    .line 567
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f02055f

    invoke-virtual {v11, v12, v13}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 568
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v11

    iput v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mPopupHeight:I

    .line 569
    sget-boolean v11, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v11, :cond_3

    .line 570
    const/high16 v11, -0x1000000

    invoke-static {v10, v4, v11}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v9

    .line 574
    :goto_0
    if-eqz v9, :cond_0

    .line 575
    invoke-virtual {v9, v4}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setTextSize(F)V

    .line 576
    const/4 v11, 0x2

    const/4 v12, 0x2

    invoke-virtual {v9, v11, v12}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 578
    new-instance v8, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v8    # "popupView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    invoke-direct {v8, v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 579
    .restart local v8    # "popupView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v8, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 580
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getWidth()I

    move-result v11

    add-int/2addr v11, v3

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mPopupHeight:I

    add-int/2addr v12, v2

    invoke-virtual {v8, v11, v12}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 581
    const/4 v11, 0x3

    const/4 v12, 0x2

    invoke-virtual {v8, v11, v12}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 582
    const/4 v11, 0x1

    invoke-virtual {v8, v9, v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 583
    const/4 v11, 0x0

    const/16 v12, -0xa

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v8, v11, v12, v13, v14}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    .line 584
    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v8, v11}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 596
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    :goto_1
    if-eqz v9, :cond_2

    .line 597
    const/4 v11, 0x1

    move/from16 v0, p4

    if-ne v0, v11, :cond_1

    .line 598
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getWidth()I

    move-result v11

    sub-int p3, p3, v11

    .line 600
    :cond_1
    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move/from16 v0, p3

    invoke-virtual {v8, v11, v12, v0, v13}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 602
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getWidth()I

    move-result v11

    add-int v11, v11, p3

    add-int/lit8 v6, v11, 0x78

    .line 605
    :cond_2
    return v6

    .line 572
    .restart local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_3
    const/4 v11, -0x1

    invoke-static {v10, v4, v11}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v9

    goto :goto_0

    .line 587
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_4
    const/4 v11, 0x1

    invoke-virtual {v8, v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v9

    .end local v9    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    check-cast v9, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 588
    .restart local v9    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    if-eqz v9, :cond_0

    .line 589
    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setText(Ljava/lang/String;)V

    .line 590
    invoke-virtual {v9, v4}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setTextSize(F)V

    .line 591
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getWidth()I

    move-result v11

    add-int/2addr v11, v3

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mPopupHeight:I

    add-int/2addr v12, v2

    invoke-virtual {v8, v11, v12}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 592
    const/4 v11, 0x3

    const/4 v12, 0x2

    invoke-virtual {v8, v11, v12}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    goto :goto_1
.end method

.method private drawLabel(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;II)V
    .locals 5
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .param p2, "margine"    # I
    .param p3, "id"    # I

    .prologue
    .line 539
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabelView:Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-direct {p0, p1, v1, p2, p3}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->drawButtonLabel(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Lcom/sec/android/gallery3d/glcore/GlView;II)I

    move-result v0

    .line 540
    .local v0, "nWidth":I
    const/16 v1, 0x2d0

    if-gt v0, v1, :cond_0

    .line 541
    const/16 v0, 0x2d0

    .line 543
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabel:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    new-instance v2, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-object v3, v3, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v4, 0x104

    invoke-direct {v2, v3, v0, v4}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 544
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabel:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabelWidth:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabelHeight:F

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    .line 545
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabel:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabelView:Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 546
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabel:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iput p3, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHoverLabelIndex:I

    .line 547
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabel:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v2, p1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    iput v2, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 548
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabel:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 549
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabel:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {p1, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 550
    return-void
.end method

.method private drawVideoIcon(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 6
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x1

    .line 726
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getChildCount()I

    move-result v2

    if-eqz v2, :cond_0

    .line 727
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->removeAllChild()V

    .line 728
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    instance-of v2, p2, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v2, :cond_1

    .line 729
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->createVideoPlayIconObject()Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v1

    .line 731
    .local v1, "videoPlayIconObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    invoke-virtual {p1, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getWidth(Z)F

    move-result v2

    div-float/2addr v2, v5

    invoke-virtual {p1, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getHeight(Z)F

    move-result v3

    div-float/2addr v3, v5

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 732
    .local v0, "size":F
    invoke-virtual {v1, v0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    .line 733
    iget v2, p1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    iput v2, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 734
    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 735
    invoke-virtual {p1, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 737
    .end local v0    # "size":F
    .end local v1    # "videoPlayIconObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_1
    return-void
.end method

.method private exitFromHoverViewWithDelay()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 890
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v2, 0x3

    const-wide/16 v6, 0x7d0

    move v4, v3

    move v5, v3

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessageDelayed(IIIIJ)V

    .line 891
    return-void
.end method

.method private getCroppedBitmap(Lcom/sec/android/gallery3d/data/MediaItem;ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "rotate"    # I
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v3, 0x0

    .line 1032
    const/4 v0, 0x0

    .line 1033
    .local v0, "faceRect":Landroid/graphics/RectF;
    invoke-direct {p0, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->getFrameRatio(I)F

    move-result v1

    .line 1036
    .local v1, "frameRatio":F
    if-eqz p1, :cond_0

    instance-of v2, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v2, :cond_0

    .line 1037
    check-cast p1, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/data/LocalImage;->getRectOfAllFaces(Z)Landroid/graphics/RectF;

    move-result-object v0

    .line 1039
    :cond_0
    if-eqz v0, :cond_1

    .line 1040
    invoke-static {p3, v0, v1, p2, v3}, Lcom/sec/android/gallery3d/common/BitmapUtils;->cropFaceByRatio(Landroid/graphics/Bitmap;Landroid/graphics/RectF;FIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1042
    :goto_0
    return-object v2

    :cond_1
    invoke-static {p3, v1, p2, v3}, Lcom/sec/android/gallery3d/common/BitmapUtils;->cropCenterByRatio(Landroid/graphics/Bitmap;FIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0
.end method

.method private getFrameRatio(I)F
    .locals 8
    .param p1, "rotate"    # I

    .prologue
    const/4 v1, 0x0

    .line 1048
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mTotalCount:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    .line 1049
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1050
    .local v4, "v":F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mTotalCount:I

    int-to-float v5, v5

    div-float/2addr v5, v4

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    int-to-float v3, v5

    .line 1059
    .local v3, "h":F
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgWidth:F

    .line 1060
    .local v2, "frameWidth":F
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgHeight:F

    .line 1061
    .local v0, "frameHeight":F
    cmpl-float v5, v0, v1

    if-nez v5, :cond_4

    .line 1066
    :cond_0
    :goto_1
    return v1

    .line 1051
    .end local v0    # "frameHeight":F
    .end local v2    # "frameWidth":F
    .end local v3    # "h":F
    .end local v4    # "v":F
    :cond_1
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mTotalCount:I

    const/4 v6, 0x5

    if-eq v5, v6, :cond_2

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mTotalCount:I

    const/4 v6, 0x6

    if-ne v5, v6, :cond_3

    .line 1052
    :cond_2
    const/high16 v4, 0x40000000    # 2.0f

    .line 1053
    .restart local v4    # "v":F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mTotalCount:I

    int-to-float v5, v5

    div-float/2addr v5, v4

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    int-to-float v3, v5

    .restart local v3    # "h":F
    goto :goto_0

    .line 1055
    .end local v3    # "h":F
    .end local v4    # "v":F
    :cond_3
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1056
    .restart local v4    # "v":F
    const/high16 v3, 0x3f800000    # 1.0f

    .restart local v3    # "h":F
    goto :goto_0

    .line 1062
    .restart local v0    # "frameHeight":F
    .restart local v2    # "frameWidth":F
    :cond_4
    div-float v5, v2, v3

    div-float v6, v0, v4

    div-float v1, v5, v6

    .line 1063
    .local v1, "frameRatio":F
    rem-int/lit16 v5, p1, 0xb4

    if-eqz v5, :cond_0

    .line 1064
    const/high16 v5, 0x3f800000    # 1.0f

    div-float v1, v5, v1

    goto :goto_1
.end method

.method private getLabelNameId(I)I
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 1220
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 1221
    const p1, 0x7f0e0047

    .line 1229
    .end local p1    # "id":I
    :cond_0
    :goto_0
    return p1

    .line 1222
    .restart local p1    # "id":I
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 1223
    const p1, 0x7f0e0058

    goto :goto_0

    .line 1224
    :cond_2
    const/4 v0, 0x1

    if-ne p1, v0, :cond_3

    .line 1225
    const p1, 0x7f0e04c0

    goto :goto_0

    .line 1226
    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 1227
    const p1, 0x7f0e0041

    goto :goto_0
.end method

.method private getVideoPlayIconCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;
    .locals 3

    .prologue
    .line 212
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mVideoPlayIconCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-nez v1, :cond_0

    .line 213
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200f7

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 215
    .local v0, "playIconBitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v1, :cond_0

    .line 216
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-object v2, v2, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v1, v2, v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mVideoPlayIconCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 218
    .end local v0    # "playIconBitmap":Landroid/graphics/Bitmap;
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mVideoPlayIconCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    return-object v1
.end method

.method private initActionBarListeners()V
    .locals 1

    .prologue
    .line 222
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$2;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mShareActionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 248
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$3;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mEditActionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 262
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$4;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDeleteActionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 275
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$5;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mFlashAnnotateActionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 284
    return-void
.end method

.method private layoutActiveObject()V
    .locals 15

    .prologue
    .line 453
    sget-object v11, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->COUNT_COL:[I

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActiveCount:I

    aget v1, v11, v12

    .line 454
    .local v1, "col":I
    sget-object v11, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->COUNT_ROW:[I

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActiveCount:I

    aget v8, v11, v12

    .line 459
    .local v8, "row":I
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgWidth:F

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingLeft:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingRight:F

    add-float/2addr v12, v13

    sub-float/2addr v11, v12

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mItemGapW:F

    add-int/lit8 v13, v1, -0x1

    int-to-float v13, v13

    mul-float/2addr v12, v13

    sub-float/2addr v11, v12

    int-to-float v12, v1

    div-float/2addr v11, v12

    iput v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mThmW:F

    .line 460
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgHeight:F

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingTop:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingBottom:F

    add-float/2addr v12, v13

    sub-float/2addr v11, v12

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mItemGapH:F

    add-int/lit8 v13, v8, -0x1

    int-to-float v13, v13

    mul-float/2addr v12, v13

    sub-float/2addr v11, v12

    int-to-float v12, v8

    div-float/2addr v11, v12

    iput v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mThmH:F

    .line 463
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mThmW:F

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mItemGapW:F

    add-float v6, v11, v12

    .line 464
    .local v6, "nw":F
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mThmH:F

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mItemGapH:F

    add-float v5, v11, v12

    .line 465
    .local v5, "nh":F
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingLeft:F

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgWidth:F

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mThmW:F

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    add-float v9, v11, v12

    .line 466
    .local v9, "sx":F
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgHeight:F

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingTop:F

    sub-float/2addr v11, v12

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mThmH:F

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    sub-float v10, v11, v12

    .line 468
    .local v10, "sy":F
    iget-boolean v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsActionBarEnabled:Z

    if-eqz v11, :cond_0

    .line 470
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgWidth:F

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackgroundShadowSize:F

    const/high16 v13, 0x40000000    # 2.0f

    mul-float/2addr v12, v13

    sub-float/2addr v11, v12

    iput v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActionBarWidth:F

    .line 471
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgHeight:F

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackgroundShadowSize:F

    sub-float/2addr v11, v12

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActionBarHeight:F

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    sub-float v0, v11, v12

    .line 472
    .local v0, "actionBarPositionY":F
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    const/4 v12, 0x0

    invoke-virtual {v11, v9, v0, v12}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->setPos(FFF)V

    .line 473
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabel:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActionBarWidth:F

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabelWidth:F

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    sub-float/2addr v12, v13

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActionBarHeight:F

    const/high16 v14, 0x40800000    # 4.0f

    add-float/2addr v13, v14

    neg-float v13, v13

    const/4 v14, 0x0

    invoke-virtual {v11, v12, v13, v14}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 475
    .end local v0    # "actionBarPositionY":F
    :cond_0
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActiveCount:I

    if-ge v4, v11, :cond_1

    .line 476
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mObjSet:[Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    aget-object v7, v11, v4

    .line 477
    .local v7, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    rem-int v11, v4, v1

    int-to-float v11, v11

    mul-float/2addr v11, v6

    add-float v2, v11, v9

    .line 478
    .local v2, "cx":F
    div-int v11, v4, v1

    neg-int v11, v11

    int-to-float v11, v11

    mul-float/2addr v11, v5

    add-float v3, v11, v10

    .line 479
    .local v3, "cy":F
    const/4 v11, 0x0

    invoke-virtual {v7, v2, v3, v11}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 475
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 481
    .end local v2    # "cx":F
    .end local v3    # "cy":F
    .end local v7    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_1
    return-void
.end method

.method private playSoundAndHapticFeedback()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1015
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getModeAirViewSoundAndHaptic(Landroid/content/Context;)I

    move-result v0

    .line 1016
    .local v0, "mode":I
    if-eq v0, v3, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1017
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    .line 1019
    :cond_1
    if-eq v0, v3, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 1020
    :cond_2
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVibetones:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v1, :cond_4

    .line 1021
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 1029
    :cond_3
    :goto_0
    return-void

    .line 1023
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mVibrator:Landroid/os/Vibrator;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v1}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1024
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v1}, Landroid/os/Vibrator;->cancel()V

    .line 1025
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v2, 0xc

    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    goto :goto_0
.end method

.method private prepareActiveObject()Z
    .locals 14

    .prologue
    .line 491
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsActionBarEnabled:Z

    if-eqz v8, :cond_0

    .line 492
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    new-instance v9, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-object v10, v10, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActionBarWidth:F

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mRatioWidth:F

    div-float/2addr v11, v12

    float-to-int v11, v11

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActionBarHeight:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mRatioHeight:F

    div-float/2addr v12, v13

    float-to-int v12, v12

    add-int/lit8 v12, v12, -0xa

    invoke-direct {v9, v10, v11, v12}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 494
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mItemList:Ljava/util/ArrayList;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v4

    .line 495
    .local v4, "mediaItemSupportedOperations":J
    invoke-direct {p0, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->updateAllActionBarButtonsVisibility(J)V

    .line 497
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActionBarWidth:F

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActionBarHeight:F

    invoke-virtual {v8, v9, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->setSize(FF)V

    .line 498
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->redrawActionBar()V

    .line 499
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->setVisibility(Z)V

    .line 502
    .end local v4    # "mediaItemSupportedOperations":J
    :cond_0
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActiveCount:I

    if-nez v8, :cond_1

    .line 503
    const/4 v8, 0x0

    .line 535
    :goto_0
    return v8

    .line 505
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActiveCount:I

    if-ge v1, v8, :cond_6

    .line 506
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mObjSet:[Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    aget-object v6, v8, v1

    .line 507
    .local v6, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    iput v1, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 508
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDisplayItemList:[Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;

    aget-object v2, v8, v1

    .line 509
    .local v2, "imageItem":Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 510
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;->getItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    .line 511
    .local v3, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v7

    .line 512
    .local v7, "rotation":I
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsSet:Z

    if-eqz v8, :cond_2

    if-eqz v0, :cond_2

    .line 513
    invoke-direct {p0, v3, v7, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->getCroppedBitmap(Lcom/sec/android/gallery3d/data/MediaItem;ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 515
    :cond_2
    if-eqz v0, :cond_4

    .line 516
    new-instance v8, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v9, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v8, v9, v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Landroid/graphics/Bitmap;)V

    invoke-virtual {v6, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 520
    :goto_2
    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 521
    if-eqz v7, :cond_3

    const/16 v8, 0xb4

    if-ne v7, v8, :cond_5

    .line 522
    :cond_3
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mThmW:F

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mThmH:F

    invoke-virtual {v6, v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    .line 526
    :goto_3
    neg-int v8, v7

    int-to-float v8, v8

    invoke-virtual {v6, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setRoll(F)V

    .line 527
    invoke-direct {p0, v6, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->drawVideoIcon(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 505
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 518
    :cond_4
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    goto :goto_2

    .line 524
    :cond_5
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mThmH:F

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mThmW:F

    invoke-virtual {v6, v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    goto :goto_3

    .line 529
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "imageItem":Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;
    .end local v3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v6    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .end local v7    # "rotation":I
    :cond_6
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActiveCount:I

    :goto_4
    const/16 v8, 0x9

    if-ge v1, v8, :cond_7

    .line 530
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mObjSet:[Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    aget-object v6, v8, v1

    .line 531
    .restart local v6    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 532
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 529
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 535
    .end local v6    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_7
    const/4 v8, 0x1

    goto :goto_0
.end method

.method private prepareItemAnimation()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 396
    const-string v0, "GlHoverController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "prepareItemAnimation = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mReqMedia:Lcom/sec/android/gallery3d/data/MediaObject;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", State = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoverState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mReqMedia:Lcom/sec/android/gallery3d/data/MediaObject;

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoverState:I

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mEnabled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mReqObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mReqMedia:Lcom/sec/android/gallery3d/data/MediaObject;

    if-nez v0, :cond_2

    .line 399
    :cond_1
    const-string v0, "GlHoverController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "prepareItemAnimation canceled = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mReqObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mReqMedia = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mReqMedia:Lcom/sec/android/gallery3d/data/MediaObject;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", enabled = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeAllMessage()V

    .line 420
    :goto_0
    return-void

    .line 403
    :cond_2
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->removeHoverView(Z)V

    .line 405
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mReqObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMainObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 406
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mReqMedia:Lcom/sec/android/gallery3d/data/MediaObject;

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 407
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mReqIndex:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIndex:I

    .line 409
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mItemList:Ljava/util/ArrayList;

    .line 410
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mItemList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 411
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mFirstIndex:I

    .line 412
    iput v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mTotalCount:I

    .line 413
    iput v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActiveCount:I

    .line 414
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLoadedCount:I

    .line 415
    iput v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mThumbnailType:I

    .line 416
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDisplayItemList:[Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;

    new-instance v0, Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mThumbnailType:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mListenerBitmap:Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem$OnBitmapAvailableListener;

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;IILcom/sec/samsung/gallery/view/image_manager/SingleImageItem$OnBitmapAvailableListener;)V

    aput-object v0, v6, v3

    .line 417
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDisplayItemList:[Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;->requestImage()V

    .line 418
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mStartTime:J

    .line 419
    iput v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoverState:I

    goto :goto_0
.end method

.method private prepareNextSet(I)Z
    .locals 13
    .param p1, "addCount"    # I

    .prologue
    const/4 v1, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 423
    if-eqz p1, :cond_1

    .line 424
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoverState:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mTotalCount:I

    const/16 v2, 0x9

    if-gt v0, v2, :cond_1

    .line 448
    :cond_0
    :goto_0
    return v10

    .line 427
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mFirstIndex:I

    add-int v9, v0, p1

    .line 428
    .local v9, "newIndex":I
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mTotalCount:I

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x9

    mul-int/lit8 v8, v0, 0x9

    .line 429
    .local v8, "maxFirstIndex":I
    if-gez v9, :cond_4

    .line 430
    move v9, v8

    .line 433
    :cond_2
    :goto_1
    iput v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mFirstIndex:I

    .line 434
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mFirstIndex:I

    add-int/lit8 v7, v0, 0x9

    .line 435
    .local v7, "lastIndex":I
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mTotalCount:I

    if-le v7, v0, :cond_3

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mTotalCount:I

    .line 436
    :cond_3
    const-string v0, "GlHoverController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prepareNextSet from = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mFirstIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", to = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mFirstIndex:I

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mFirstIndex:I

    sub-int v3, v7, v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mItemList:Ljava/util/ArrayList;

    .line 438
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mItemList:Ljava/util/ArrayList;

    if-nez v0, :cond_5

    move v0, v10

    :goto_2
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActiveCount:I

    .line 439
    iput v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLoadedCount:I

    .line 440
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mTotalCount:I

    const/4 v2, 0x4

    if-gt v0, v2, :cond_6

    move v0, v11

    :goto_3
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mThumbnailType:I

    .line 441
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_4
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActiveCount:I

    if-ge v6, v0, :cond_7

    .line 442
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDisplayItemList:[Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;

    new-instance v0, Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mFirstIndex:I

    add-int/2addr v3, v6

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mThumbnailType:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mListenerBitmap:Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem$OnBitmapAvailableListener;

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;IILcom/sec/samsung/gallery/view/image_manager/SingleImageItem$OnBitmapAvailableListener;)V

    aput-object v0, v12, v6

    .line 443
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDisplayItemList:[Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;

    aget-object v0, v0, v6

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;->requestImage()V

    .line 441
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 431
    .end local v6    # "i":I
    .end local v7    # "lastIndex":I
    :cond_4
    if-le v9, v8, :cond_2

    .line 432
    const/4 v9, 0x0

    goto :goto_1

    .line 438
    .restart local v7    # "lastIndex":I
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_2

    :cond_6
    move v0, v1

    .line 440
    goto :goto_3

    .line 445
    .restart local v6    # "i":I
    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mStartTime:J

    .line 446
    if-eqz p1, :cond_8

    move v10, v11

    :cond_8
    iput-boolean v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mNextRequested:Z

    .line 447
    iput v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoverState:I

    move v10, v11

    .line 448
    goto/16 :goto_0
.end method

.method private prepareSetAnimation()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 378
    const-string v0, "GlHoverController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "prepareSetAnimation = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mReqMedia:Lcom/sec/android/gallery3d/data/MediaObject;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", State = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoverState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mReqMedia:Lcom/sec/android/gallery3d/data/MediaObject;

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoverState:I

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mEnabled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mReqObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mReqMedia:Lcom/sec/android/gallery3d/data/MediaObject;

    if-nez v0, :cond_2

    .line 381
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeAllMessage()V

    .line 393
    :goto_0
    return-void

    .line 384
    :cond_2
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->removeHoverView(Z)V

    .line 386
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mReqObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMainObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 387
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mReqMedia:Lcom/sec/android/gallery3d/data/MediaObject;

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 388
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mReqIndex:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIndex:I

    .line 390
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mFirstIndex:I

    .line 391
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mTotalCount:I

    .line 392
    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->prepareNextSet(I)Z

    goto :goto_0
.end method

.method private declared-synchronized removeHoverViewInter(Z)V
    .locals 9
    .param p1, "doAnimation"    # Z

    .prologue
    .line 836
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/sec/samsung/gallery/util/HoverIconUtil;->setHoveringIconToDefault()V

    .line 838
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mTransAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->stop()V

    .line 839
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeAllMessage()V

    .line 840
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 841
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 842
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActiveCount:I

    if-ge v7, v0, :cond_1

    .line 843
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDisplayItemList:[Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;

    aget-object v0, v0, v7

    if-nez v0, :cond_0

    .line 842
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 845
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDisplayItemList:[Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;

    aget-object v0, v0, v7

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;->cancelImageRequest()V

    .line 846
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDisplayItemList:[Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;

    aget-object v0, v0, v7

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;->recycle()V

    .line 847
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDisplayItemList:[Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;

    const/4 v1, 0x0

    aput-object v1, v0, v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 836
    .end local v7    # "i":I
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 849
    .restart local v7    # "i":I
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActiveCount:I

    .line 852
    const/4 v7, 0x0

    :goto_2
    const/16 v0, 0x9

    if-ge v7, v0, :cond_2

    .line 853
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mObjSet:[Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    aget-object v8, v0, v7

    .line 854
    .local v8, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->removeAllChild()V

    .line 852
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 857
    .end local v8    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_2
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsActionBarEnabled:Z

    if-eqz v0, :cond_3

    .line 858
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    if-eqz v0, :cond_3

    .line 859
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->removeBGChildview()V

    .line 860
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->removeAllChild()V

    .line 864
    :cond_3
    if-nez p1, :cond_4

    .line 865
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAlpha(F)V

    .line 866
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoverState:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 876
    :goto_3
    monitor-exit p0

    return-void

    .line 869
    :cond_4
    :try_start_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEnableAnim(ZZZZZ)V

    .line 870
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourceScale(F)V

    .line 871
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetScale(F)V

    .line 872
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAlpha()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourceAlpha(F)V

    .line 873
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetAlpha(F)V

    .line 874
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDisappearAnimListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    const-wide/16 v2, 0xc8

    const-wide/16 v4, 0x0

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->startTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;JJLcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    .line 875
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoverState:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method private setActionBarButtonVisibility(IZ)V
    .locals 2
    .param p1, "buttonId"    # I
    .param p2, "isVisible"    # Z

    .prologue
    .line 630
    if-eqz p2, :cond_0

    const/4 v0, 0x0

    .line 631
    .local v0, "itemVisibility":I
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    invoke-virtual {v1, p1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;->setItemVisibility(II)V

    .line 632
    return-void

    .line 630
    .end local v0    # "itemVisibility":I
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setActionBarEnabled()V
    .locals 1

    .prologue
    .line 201
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAirViewActionBar:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsSet:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsActionBarEnabled:Z

    .line 202
    return-void

    .line 201
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setBackgroundObject()V
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 990
    const/4 v0, 0x0

    .line 1000
    .local v0, "backgroundResId":I
    const v0, 0x7f0202f6

    .line 1002
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgWidth:F

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWindowCoordinateValue(F)F

    move-result v3

    .line 1003
    .local v3, "ninePatchWidth":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgHeight:F

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWindowCoordinateValue(F)F

    move-result v2

    .line 1005
    .local v2, "ninePatchHeight":F
    cmpg-float v4, v3, v7

    if-gez v4, :cond_0

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    :goto_0
    double-to-int v6, v4

    cmpg-float v4, v2, v7

    if-gez v4, :cond_1

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    :goto_1
    double-to-int v4, v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    invoke-static {v0, v6, v4, v5}, Lcom/sec/android/gallery3d/common/BitmapUtils;->getNinePatch(IIILandroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1009
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    new-instance v5, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v5, v6, v1}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Landroid/graphics/Bitmap;)V

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 1010
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgWidth:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgHeight:F

    invoke-virtual {v4, v5, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    .line 1011
    return-void

    .line 1005
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    float-to-double v4, v3

    goto :goto_0

    :cond_1
    float-to-double v4, v2

    goto :goto_1
.end method

.method private setBackgroundSize()Z
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 934
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsSet:Z

    if-eqz v8, :cond_1

    .line 935
    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mImageAreaWidth:F

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingLeft:F

    add-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingRight:F

    add-float/2addr v7, v8

    iput v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgWidth:F

    .line 936
    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mImageAreaHeight:F

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingTop:F

    add-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingBottom:F

    add-float/2addr v7, v8

    iput v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgHeight:F

    .line 986
    :cond_0
    :goto_0
    return v6

    .line 945
    :cond_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDisplayItemList:[Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;

    aget-object v2, v8, v7

    .line 946
    .local v2, "imageItem":Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;
    if-nez v2, :cond_2

    move v6, v7

    .line 947
    goto :goto_0

    .line 948
    :cond_2
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 949
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;->getItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    .line 950
    .local v3, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v4

    .line 951
    .local v4, "rotation":I
    if-nez v0, :cond_3

    move v6, v7

    .line 952
    goto :goto_0

    .line 953
    :cond_3
    if-eqz v4, :cond_4

    const/16 v7, 0xb4

    if-ne v4, v7, :cond_5

    .line 954
    :cond_4
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 955
    .local v5, "width":I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 974
    .local v1, "height":I
    :goto_1
    if-le v5, v1, :cond_6

    .line 975
    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mImageAreaWidth:F

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingLeft:F

    add-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingRight:F

    add-float/2addr v7, v8

    iput v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgWidth:F

    .line 976
    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mImageAreaWidth:F

    int-to-float v8, v1

    mul-float/2addr v7, v8

    int-to-float v8, v5

    div-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingTop:F

    add-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingBottom:F

    add-float/2addr v7, v8

    iput v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgHeight:F

    .line 981
    :goto_2
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsActionBarEnabled:Z

    if-eqz v7, :cond_0

    .line 982
    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgHeight:F

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActionBarMinHeight:F

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v7

    iput v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgHeight:F

    .line 983
    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgWidth:F

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActionBarMinWidth:F

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v7

    iput v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgWidth:F

    goto :goto_0

    .line 957
    .end local v1    # "height":I
    .end local v5    # "width":I
    :cond_5
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 958
    .restart local v5    # "width":I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .restart local v1    # "height":I
    goto :goto_1

    .line 978
    :cond_6
    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mImageAreaWidth:F

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingTop:F

    add-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingBottom:F

    add-float/2addr v7, v8

    iput v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgHeight:F

    .line 979
    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mImageAreaWidth:F

    int-to-float v8, v5

    mul-float/2addr v7, v8

    int-to-float v8, v1

    div-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingLeft:F

    add-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingRight:F

    add-float/2addr v7, v8

    iput v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgWidth:F

    goto :goto_2
.end method

.method private updateAllActionBarButtonsVisibility(J)V
    .locals 13
    .param p1, "mediaItemSupportedOperations"    # J

    .prologue
    const-wide/16 v10, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 635
    const-wide/16 v8, 0x1

    and-long/2addr v8, p1

    cmp-long v4, v8, v10

    if-eqz v4, :cond_4

    move v0, v5

    .line 636
    .local v0, "isButtonVisibleForDelete":Z
    :goto_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEventViewMode(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v4

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 638
    :cond_0
    const/4 v0, 0x0

    .line 640
    :cond_1
    const/4 v4, 0x4

    invoke-direct {p0, v4, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->setActionBarButtonVisibility(IZ)V

    .line 642
    const-wide/16 v8, 0x4

    and-long/2addr v8, p1

    cmp-long v4, v8, v10

    if-eqz v4, :cond_5

    move v3, v5

    .line 643
    .local v3, "isButtonVisibleForShare":Z
    :goto_1
    const/4 v4, 0x3

    invoke-direct {p0, v4, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->setActionBarButtonVisibility(IZ)V

    .line 645
    const-wide/16 v8, 0x200

    and-long/2addr v8, p1

    cmp-long v4, v8, v10

    if-eqz v4, :cond_6

    move v1, v5

    .line 646
    .local v1, "isButtonVisibleForEdit":Z
    :goto_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v4

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 647
    const/4 v1, 0x1

    .line 649
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 650
    const/4 v1, 0x0

    .line 652
    :cond_3
    const/4 v4, 0x2

    invoke-direct {p0, v4, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->setActionBarButtonVisibility(IZ)V

    .line 654
    const-wide/32 v8, 0x80000

    and-long/2addr v8, p1

    cmp-long v4, v8, v10

    if-eqz v4, :cond_7

    move v2, v5

    .line 655
    .local v2, "isButtonVisibleForFA":Z
    :goto_3
    invoke-direct {p0, v5, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->setActionBarButtonVisibility(IZ)V

    .line 656
    return-void

    .end local v0    # "isButtonVisibleForDelete":Z
    .end local v1    # "isButtonVisibleForEdit":Z
    .end local v2    # "isButtonVisibleForFA":Z
    .end local v3    # "isButtonVisibleForShare":Z
    :cond_4
    move v0, v6

    .line 635
    goto :goto_0

    .restart local v0    # "isButtonVisibleForDelete":Z
    :cond_5
    move v3, v6

    .line 642
    goto :goto_1

    .restart local v3    # "isButtonVisibleForShare":Z
    :cond_6
    move v1, v6

    .line 645
    goto :goto_2

    .restart local v1    # "isButtonVisibleForEdit":Z
    :cond_7
    move v2, v6

    .line 654
    goto :goto_3
.end method


# virtual methods
.method public convX(I)F
    .locals 2
    .param p1, "x"    # I

    .prologue
    .line 348
    int-to-float v0, p1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mRatioWidth:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public convY(I)F
    .locals 2
    .param p1, "y"    # I

    .prologue
    .line 352
    int-to-float v0, p1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mRatioHeight:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public destroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 289
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->removeHoverViewInter(Z)V

    .line 290
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDisplayItemList:[Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;

    .line 291
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->remove()V

    .line 292
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0x9

    if-ge v0, v2, :cond_0

    .line 293
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mObjSet:[Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    aget-object v1, v2, v0

    .line 294
    .local v1, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->remove()V

    .line 292
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 296
    .end local v1    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_0
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 297
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mObjSet:[Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 298
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    .line 299
    return-void
.end method

.method public exitFromHoverView(Z)V
    .locals 8
    .param p1, "delayedExit"    # Z

    .prologue
    const/4 v2, 0x3

    const/4 v4, 0x0

    .line 880
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_0

    .line 881
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->clearHoveringState()V

    .line 882
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeAllMessage()V

    .line 883
    if-eqz p1, :cond_1

    .line 884
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v3, 0x1

    const-wide/16 v6, 0x190

    move v5, v4

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessageDelayed(IIIIJ)V

    .line 887
    :goto_0
    return-void

    .line 886
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v0, v2, v4, v4, v4}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    goto :goto_0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 609
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsActionBarEnabled:Z

    if-eqz v0, :cond_0

    .line 610
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIndex:I

    .line 612
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public initAttribute()V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 303
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidthSpace:F

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v1, v1, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidth:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mRatioWidth:F

    .line 304
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeightSpace:F

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v1, v1, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeight:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mRatioHeight:F

    .line 306
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsSet:Z

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringImageSetWidth()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mImageAreaWidth:F

    .line 308
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringImageSetHeight()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mImageAreaHeight:F

    .line 309
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringImageSetOffsetFromOriginalThumbnail()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoveringOffset:F

    .line 316
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringImageGap()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mItemGapW:F

    .line 317
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringImageGap()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mItemGapH:F

    .line 326
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringBgPaddingInside()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgCommonPaddingInside:F

    .line 327
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringBgPaddingLeft()F

    move-result v0

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgCommonPaddingInside:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingLeft:F

    .line 328
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringBgPaddingRight()F

    move-result v0

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgCommonPaddingInside:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingRight:F

    .line 329
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringBgPaddingTop()F

    move-result v0

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgCommonPaddingInside:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingTop:F

    .line 330
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringBgPaddingBottom()F

    move-result v0

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgCommonPaddingInside:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingBottom:F

    .line 333
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringActionBarHeight()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->convY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActionBarHeight:F

    .line 334
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringActionBarShadowSize()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->convY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackgroundShadowSize:F

    .line 335
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringActionBarMinWidth()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->convX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActionBarMinWidth:F

    .line 336
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringActionBarMinHeight()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->convY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActionBarMinHeight:F

    .line 337
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringActionBarButtonLabelHeight()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->convY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabelHeight:F

    .line 338
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringActionBarButtonLabelWidth()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->convX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLabelWidth:F

    .line 341
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeightSpace:F

    div-float/2addr v0, v2

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActionBarHeight:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mScreenTop:F

    .line 342
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeightSpace:F

    neg-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mScreenBottom:F

    .line 343
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidthSpace:F

    neg-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mScreenLeft:F

    .line 344
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidthSpace:F

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mScreenRight:F

    .line 345
    return-void

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringImageWidth()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mImageAreaWidth:F

    .line 312
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringImageHeight()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mImageAreaHeight:F

    .line 313
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringImageOffsetFromOriginalThumbnail()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoveringOffset:F

    goto/16 :goto_0
.end method

.method public isIdleState()Z
    .locals 1

    .prologue
    .line 365
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoverState:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeHoverView(Z)V
    .locals 3
    .param p1, "doAnimation"    # Z

    .prologue
    .line 824
    const-string v0, "GlHoverController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removeHoverView removed message = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoverState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 825
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoverState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 826
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mTransAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->stop()V

    .line 827
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeAllMessage()V

    .line 828
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAlpha(F)V

    .line 829
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoverState:I

    .line 833
    :goto_0
    return-void

    .line 832
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->removeHoverViewInter(Z)V

    goto :goto_0
.end method

.method public requestStart(Lcom/sec/android/gallery3d/glcore/GlObject;Lcom/sec/android/gallery3d/data/MediaObject;I)V
    .locals 3
    .param p1, "obj"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;
    .param p3, "index"    # I

    .prologue
    const/4 v2, 0x0

    .line 369
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mEnabled:Z

    if-nez v0, :cond_1

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 371
    :cond_1
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mReqObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 372
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mReqMedia:Lcom/sec/android/gallery3d/data/MediaObject;

    .line 373
    iput p3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mReqIndex:I

    .line 374
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 356
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mEnabled:Z

    if-ne p1, v0, :cond_1

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 358
    :cond_1
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mEnabled:Z

    .line 359
    if-nez p1, :cond_0

    .line 360
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->removeHoverViewInter(Z)V

    goto :goto_0
.end method

.method public setHoverControlListener(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;

    .prologue
    .line 1187
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mOnHoverListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;

    .line 1188
    return-void
.end method

.method public startShowAnimation()V
    .locals 14

    .prologue
    const/high16 v13, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/high16 v5, 0x40000000    # 2.0f

    .line 740
    const-string v0, "GlHoverController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "startShowAnimation state = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoverState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 741
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsActionBarEnabled:Z

    if-eqz v0, :cond_0

    .line 742
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mGlActionBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeActionBar;

    .line 743
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->createActionBar(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    .line 745
    :cond_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mNextRequested:Z

    if-eqz v0, :cond_2

    .line 746
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->prepareActiveObject()Z

    .line 747
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mNextRequested:Z

    .line 748
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoverState:I

    .line 821
    :cond_1
    :goto_0
    return-void

    .line 751
    :cond_2
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->playSoundAndHapticFeedback()V

    .line 752
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->setBackgroundSize()Z

    move-result v0

    if-nez v0, :cond_3

    .line 753
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->removeHoverViewInter(Z)V

    .line 755
    :cond_3
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->initAttribute()V

    .line 756
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->setBackgroundObject()V

    .line 757
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->layoutActiveObject()V

    .line 758
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->prepareActiveObject()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 762
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMainObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAbsX()F

    move-result v11

    .line 771
    .local v11, "toX":F
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringBgPaddingInside()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgCommonPaddingInside:F

    .line 772
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringBgPaddingLeft()F

    move-result v0

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgCommonPaddingInside:F

    add-float/2addr v0, v3

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingLeft:F

    .line 773
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringBgPaddingRight()F

    move-result v0

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgCommonPaddingInside:F

    add-float/2addr v0, v3

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingRight:F

    .line 774
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringBgPaddingTop()F

    move-result v0

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgCommonPaddingInside:F

    add-float/2addr v0, v3

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingTop:F

    .line 775
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getHoveringBgPaddingBottom()F

    move-result v0

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgCommonPaddingInside:F

    add-float/2addr v0, v3

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingBottom:F

    .line 778
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMainObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAbsY()F

    move-result v0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMainObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getHeight(Z)F

    move-result v3

    div-float/2addr v3, v5

    sub-float/2addr v0, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoveringOffset:F

    add-float/2addr v0, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgHeight:F

    mul-float/2addr v3, v6

    add-float/2addr v0, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingBottom:F

    sub-float v12, v0, v3

    .line 780
    .local v12, "toY":F
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgHeight:F

    div-float/2addr v0, v5

    add-float/2addr v0, v12

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingTop:F

    sub-float v10, v0, v3

    .line 781
    .local v10, "hoverImgTop":F
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgHeight:F

    div-float/2addr v0, v5

    sub-float v0, v12, v0

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingBottom:F

    add-float v7, v0, v3

    .line 782
    .local v7, "hoverImgBottom":F
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgWidth:F

    div-float/2addr v0, v5

    sub-float v0, v11, v0

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingLeft:F

    add-float v8, v0, v3

    .line 783
    .local v8, "hoverImgLeft":F
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgWidth:F

    div-float/2addr v0, v5

    add-float/2addr v0, v11

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingRight:F

    sub-float v9, v0, v3

    .line 785
    .local v9, "hoverImgRight":F
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoveringOffset:F

    add-float/2addr v0, v10

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mScreenTop:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_8

    .line 788
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMainObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAbsY()F

    move-result v0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMainObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getHeight(Z)F

    move-result v3

    div-float/2addr v3, v5

    add-float/2addr v0, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoveringOffset:F

    sub-float/2addr v0, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgHeight:F

    mul-float/2addr v3, v6

    sub-float/2addr v0, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingTop:F

    add-float/2addr v0, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActionBarHeight:F

    sub-float v12, v0, v3

    .line 789
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgHeight:F

    div-float/2addr v0, v5

    add-float/2addr v0, v12

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingTop:F

    sub-float v10, v0, v3

    .line 790
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgHeight:F

    div-float/2addr v0, v5

    sub-float v0, v12, v0

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingBottom:F

    add-float v7, v0, v3

    .line 791
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoveringOffset:F

    add-float/2addr v0, v10

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mScreenTop:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_7

    .line 793
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mScreenTop:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgHeight:F

    div-float/2addr v3, v5

    sub-float/2addr v0, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingTop:F

    add-float/2addr v0, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoveringOffset:F

    sub-float/2addr v0, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActionBarHeight:F

    sub-float v12, v0, v3

    .line 805
    :cond_4
    :goto_1
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mScreenRight:F

    cmpl-float v0, v9, v0

    if-lez v0, :cond_9

    .line 806
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mScreenRight:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgWidth:F

    div-float/2addr v3, v5

    sub-float/2addr v0, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingRight:F

    add-float/2addr v0, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoveringOffset:F

    sub-float v11, v0, v3

    .line 810
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move v3, v1

    move v4, v2

    move v5, v1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEnableAnim(ZZZZZ)V

    .line 811
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMainObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAbsX()F

    move-result v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMainObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAbsY()F

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMainObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAbsZ()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourcePos(FFF)V

    .line 812
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mMainObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAbsZ()F

    move-result v1

    invoke-virtual {v0, v11, v12, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetPos(FFF)V

    .line 813
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const v1, 0x3f333333    # 0.7f

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourceScale(F)V

    .line 814
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v13}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetScale(F)V

    .line 815
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourceAlpha(F)V

    .line 816
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v13}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetAlpha(F)V

    .line 817
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->moveToLast()V

    .line 818
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsActionBarEnabled:Z

    if-eqz v0, :cond_6

    .line 819
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mListenerHover:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setHoverListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;)V

    .line 820
    :cond_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mAppearAnimListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    const-wide/16 v2, 0xc8

    const-wide/16 v4, 0x0

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->startTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;JJLcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    goto/16 :goto_0

    .line 794
    :cond_7
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mScreenBottom:F

    cmpg-float v0, v7, v0

    if-gez v0, :cond_4

    .line 795
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mScreenBottom:F

    sub-float/2addr v0, v7

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingBottom:F

    add-float/2addr v0, v3

    add-float/2addr v12, v0

    goto/16 :goto_1

    .line 797
    :cond_8
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoveringOffset:F

    sub-float v0, v7, v0

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mScreenBottom:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_4

    .line 798
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mScreenBottom:F

    sub-float/2addr v0, v7

    add-float/2addr v12, v0

    .line 799
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoveringOffset:F

    sub-float v0, v7, v0

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mScreenBottom:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_4

    .line 801
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mScreenBottom:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgHeight:F

    div-float/2addr v3, v5

    add-float/2addr v0, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingBottom:F

    sub-float/2addr v0, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoveringOffset:F

    add-float v12, v0, v3

    goto/16 :goto_1

    .line 807
    :cond_9
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mScreenLeft:F

    cmpg-float v0, v8, v0

    if-gez v0, :cond_5

    .line 808
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mScreenLeft:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgWidth:F

    div-float/2addr v3, v5

    add-float/2addr v0, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mBgPaddingLeft:F

    sub-float/2addr v0, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHoveringOffset:F

    add-float v11, v0, v3

    goto/16 :goto_2
.end method
