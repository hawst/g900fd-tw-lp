.class Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$2;
.super Landroid/os/Handler;
.source "AddUserTagListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$2;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 93
    iget v6, p1, Landroid/os/Message;->what:I

    packed-switch v6, :pswitch_data_0

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 95
    :pswitch_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$2;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mAddTagEditActionbar:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$100(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 96
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$2;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mAddTagEditActionbar:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$100(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$2;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$300(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0e004e

    new-array v9, v4, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$2;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mTagListAdater:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;
    invoke-static {v10}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$200(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->getData()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v5

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->setTitle(Ljava/lang/String;)V

    .line 98
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$2;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mTagListAdater:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$200(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->getData()Ljava/util/ArrayList;

    move-result-object v3

    .line 99
    .local v3, "selectedTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$2;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$300(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getAllUserTag(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$2;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mTagListAdater:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$200(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->getData()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ne v6, v7, :cond_2

    move v2, v4

    .line 100
    .local v2, "isSelectAll":Z
    :goto_1
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAddTagAsMoreinfo:Z

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$2;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mIsDetailView:Z
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$400(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 101
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$2;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$500(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->containsAll(Ljava/util/Collection;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$2;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$500(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eq v6, v7, :cond_3

    :cond_1
    move v0, v4

    .line 103
    .local v0, "chaged":Z
    :goto_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$2;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mAddTagEditActionbar:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$100(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    move-result-object v6

    if-eqz v0, :cond_4

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$2;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mTagListAdater:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$200(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->getCount()I

    move-result v7

    if-lez v7, :cond_4

    :goto_3
    invoke-virtual {v6, v4}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->updateDoneButton(Z)V

    .line 111
    .end local v0    # "chaged":Z
    :goto_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$2;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mAddTagEditActionbar:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$100(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$2;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mTagListAdater:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$200(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->getData()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v2, v5}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->updatePopupMenuItemVisibility(ZI)V

    goto/16 :goto_0

    .end local v2    # "isSelectAll":Z
    :cond_2
    move v2, v5

    .line 99
    goto :goto_1

    .restart local v2    # "isSelectAll":Z
    :cond_3
    move v0, v5

    .line 101
    goto :goto_2

    .restart local v0    # "chaged":Z
    :cond_4
    move v4, v5

    .line 103
    goto :goto_3

    .line 105
    .end local v0    # "chaged":Z
    :cond_5
    const/4 v1, 0x0

    .line 106
    .local v1, "enable":Z
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_6

    .line 107
    const/4 v1, 0x1

    .line 109
    :cond_6
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$2;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    # getter for: Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mAddTagEditActionbar:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->access$100(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->updateDoneButton(Z)V

    goto :goto_4

    .line 93
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
