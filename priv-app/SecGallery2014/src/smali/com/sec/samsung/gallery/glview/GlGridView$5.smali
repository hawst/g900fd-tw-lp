.class Lcom/sec/samsung/gallery/glview/GlGridView$5;
.super Ljava/lang/Object;
.source "GlGridView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGridView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V
    .locals 0

    .prologue
    .line 804
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationDone()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 809
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->getGlRoot()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->clearNoDetach()V

    .line 810
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iput-object v2, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    .line 811
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mOnConvAnim:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$102(Lcom/sec/samsung/gallery/glview/GlGridView;Z)Z

    .line 812
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iput-boolean v1, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mLockPos:Z

    .line 813
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;
    invoke-static {v0, v2}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$202(Lcom/sec/samsung/gallery/glview/GlGridView;Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;)Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    .line 816
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mSizeChangedDurationAnimation:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$300(Lcom/sec/samsung/gallery/glview/GlGridView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 817
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # invokes: Lcom/sec/samsung/gallery/glview/GlGridView;->loadFolders(Z)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$400(Lcom/sec/samsung/gallery/glview/GlGridView;Z)V

    .line 820
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mSizeChangedDurationAnimation:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$302(Lcom/sec/samsung/gallery/glview/GlGridView;Z)Z

    .line 821
    return-void
.end method
