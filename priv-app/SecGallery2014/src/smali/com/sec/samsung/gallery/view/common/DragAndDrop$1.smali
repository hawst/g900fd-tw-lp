.class Lcom/sec/samsung/gallery/view/common/DragAndDrop$1;
.super Ljava/lang/Object;
.source "DragAndDrop.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/common/DragAndDrop;->getImageView()Landroid/widget/ImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/DragAndDrop;

.field final synthetic val$dragView:Landroid/view/View;

.field final synthetic val$mainView:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/common/DragAndDrop;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop$1;->this$0:Lcom/sec/samsung/gallery/view/common/DragAndDrop;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop$1;->val$mainView:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop$1;->val$dragView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 322
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop$1;->val$mainView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop$1;->val$dragView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 323
    monitor-enter p0

    .line 324
    :try_start_0
    const-string v0, "DragAndDrop"

    const-string v1, "addViewOnUiThread notify"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 326
    monitor-exit p0

    .line 327
    return-void

    .line 326
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
