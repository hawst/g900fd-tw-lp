.class public Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlThumbObject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlThumbObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ThumbTouchAnimation"
.end annotation


# instance fields
.field private final PRESS_SCALE:F

.field private mFromScale:F

.field private mLScale:F

.field private mToScale:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V
    .locals 1

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 133
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 127
    const v0, 0x3f733333    # 0.95f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->PRESS_SCALE:F

    .line 130
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->mLScale:F

    .line 134
    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    .line 157
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->mFromScale:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->mToScale:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->mFromScale:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p1

    add-float v0, v1, v2

    .line 158
    .local v0, "dScale":F
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->mLScale:F

    invoke-static {v1, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eqz v1, :cond_0

    .line 159
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v1, v0, v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->setScale(FF)V

    .line 160
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->mLScale:F

    .line 162
    :cond_0
    return-void
.end method

.method public cancelPressAnim()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 144
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->isIdle()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->mToScale:F

    const v1, 0x3f733333    # 0.95f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->stop()V

    .line 147
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->mLScale:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->mToScale:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_1

    .line 148
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->mLScale:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->mFromScale:F

    .line 149
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->mToScale:F

    .line 150
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->start()V

    .line 152
    :cond_1
    return-void
.end method

.method public startPressAnim()V
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->stop()V

    .line 138
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->mLScale:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->mFromScale:F

    .line 139
    const v0, 0x3f733333    # 0.95f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->mToScale:F

    .line 140
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbTouchAnimation;->start()V

    .line 141
    return-void
.end method
