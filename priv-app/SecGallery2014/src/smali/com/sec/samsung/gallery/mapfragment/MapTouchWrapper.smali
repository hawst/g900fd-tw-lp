.class public Lcom/sec/samsung/gallery/mapfragment/MapTouchWrapper;
.super Landroid/widget/FrameLayout;
.source "MapTouchWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/mapfragment/MapTouchWrapper$MapTouchListener;
    }
.end annotation


# instance fields
.field private mListener:Lcom/sec/samsung/gallery/mapfragment/MapTouchWrapper$MapTouchListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 12
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapTouchWrapper;->mListener:Lcom/sec/samsung/gallery/mapfragment/MapTouchWrapper$MapTouchListener;

    .line 17
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    .line 18
    .local v0, "viewStateProxy":Lcom/sec/android/gallery3d/app/StateManager;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    iput-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapTouchWrapper;->mListener:Lcom/sec/samsung/gallery/mapfragment/MapTouchWrapper$MapTouchListener;

    .line 19
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapTouchWrapper;->mListener:Lcom/sec/samsung/gallery/mapfragment/MapTouchWrapper$MapTouchListener;

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapTouchWrapper;->mListener:Lcom/sec/samsung/gallery/mapfragment/MapTouchWrapper$MapTouchListener;

    invoke-interface {v0, p1}, Lcom/sec/samsung/gallery/mapfragment/MapTouchWrapper$MapTouchListener;->onMapTouch(Landroid/view/MotionEvent;)V

    .line 26
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
