.class public interface abstract Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;
.super Ljava/lang/Object;
.source "IFaceTagFeature.java"


# virtual methods
.method public abstract assignName(Landroid/net/Uri;Z)V
.end method

.method public abstract cancelFaceScan()V
.end method

.method public abstract cropManualFD(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
.end method

.method public abstract disableBuddyShare()V
.end method

.method public abstract dismissAlertDialog()V
.end method

.method public abstract enableBuddyShare()V
.end method

.method public abstract getManualFaceTagUri()Landroid/net/Uri;
.end method

.method public abstract setAsCallerId(Landroid/graphics/Bitmap;)V
.end method

.method public abstract setManualFaceTagUri(Landroid/net/Uri;)V
.end method

.method public abstract setScanPriority(Lcom/sec/android/gallery3d/data/MediaItem;)V
.end method

.method public abstract showAlertDialog(Landroid/content/Context;ILjava/lang/String;)V
.end method

.method public abstract showAlertDialog(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
.end method
