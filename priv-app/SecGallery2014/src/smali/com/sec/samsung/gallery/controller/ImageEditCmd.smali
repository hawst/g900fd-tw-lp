.class public Lcom/sec/samsung/gallery/controller/ImageEditCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ImageEditCmd.java"

# interfaces
.implements Ljava/util/Observer;
.implements Lorg/puremvc/java/interfaces/ICommand;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;
    }
.end annotation


# static fields
.field private static final MSG_DIALOG_CANCEL_SELECTED:I = 0x1

.field private static final MSG_DIALOG_SHOW_SELECTED:I = 0x0

.field public static final PAPER_ARTIST_CLASS_NAME:Ljava/lang/String; = "com.dama.paperartistsamsung.PaperArtistSamsungActivity"

.field public static final PAPER_ARTIST_PACKAGE_NAME:Ljava/lang/String; = "com.dama.paperartist"

.field private static final PHOTO_EDITOR_CLASS_NAME:Ljava/lang/String; = "com.sec.android.mimage.photoretouching.PhotoRetouching"

.field public static final PHOTO_EDITOR_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.mimage.photoretouching"

.field private static final PS_TOUCH_PACKAGE_NAME:Ljava/lang/String; = "air.com.adobe.pstouch.oem1"


# instance fields
.field private mContext:Landroid/content/Context;

.field mEditAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

.field private mEditDialog:Lcom/sec/samsung/gallery/view/common/EditDialog;

.field private mHandler:Landroid/os/Handler;

.field protected mMimeType:Ljava/lang/String;

.field protected mSelectedItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field protected mTitleOfDialog:I

.field protected mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 55
    const v0, 0x7f0e0058

    iput v0, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mTitleOfDialog:I

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mMimeType:Ljava/lang/String;

    .line 178
    new-instance v0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$2;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/controller/ImageEditCmd$2;-><init>(Lcom/sec/samsung/gallery/controller/ImageEditCmd;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mHandler:Landroid/os/Handler;

    .line 249
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/ImageEditCmd;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ImageEditCmd;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->showAppChoiceDialog(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/ImageEditCmd;Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ImageEditCmd;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->startShareApp(Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/controller/ImageEditCmd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ImageEditCmd;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->dismissDialog()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/controller/ImageEditCmd;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ImageEditCmd;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private dismissDialog()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mEditAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mEditAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->dismissDialog()V

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mEditDialog:Lcom/sec/samsung/gallery/view/common/EditDialog;

    if-eqz v0, :cond_1

    .line 127
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mEditDialog:Lcom/sec/samsung/gallery/view/common/EditDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/EditDialog;->dismissDialog()V

    .line 128
    :cond_1
    return-void
.end method

.method private queryResolveInfo(Ljava/lang/String;)Landroid/content/pm/ResolveInfo;
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 237
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.EDIT"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 238
    .local v2, "mainIntent":Landroid/content/Intent;
    const-string v4, "image/*"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 239
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 241
    .local v3, "resolveInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 242
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    iget-object v1, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 243
    .local v1, "info":Landroid/content/pm/ActivityInfo;
    iget-object v4, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 244
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 246
    .end local v1    # "info":Landroid/content/pm/ActivityInfo;
    :goto_1
    return-object v4

    .line 241
    .restart local v1    # "info":Landroid/content/pm/ActivityInfo;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 246
    .end local v1    # "info":Landroid/content/pm/ActivityInfo;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private showAppChoiceDialog(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 82
    .local p1, "shareAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;>;"
    iget v0, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mTitleOfDialog:I

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 85
    :cond_0
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    sget v1, Lcom/sec/samsung/gallery/core/Event;->EVENT_START_SELECTED_APP:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v3

    .line 86
    .local v3, "event":Lcom/sec/samsung/gallery/core/Event;
    new-instance v5, Lcom/sec/samsung/gallery/controller/ImageEditCmd$1;

    invoke-direct {v5, p0, p1}, Lcom/sec/samsung/gallery/controller/ImageEditCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/ImageEditCmd;Ljava/util/ArrayList;)V

    .line 112
    .local v5, "itemHander":Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;
    new-instance v0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    const/4 v4, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/sec/samsung/gallery/core/Event;ZLcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mEditAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    .line 113
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mEditAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    iget v1, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mTitleOfDialog:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->setTitle(I)V

    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mEditAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->addObserver(Ljava/util/Observer;)V

    .line 115
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mEditAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->showDialog()V

    .line 116
    return-void
.end method

.method private showPhotoEditorDownloadDialog()V
    .locals 4

    .prologue
    .line 119
    new-instance v0, Lcom/sec/samsung/gallery/view/common/EditDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/common/EditDialog;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mEditDialog:Lcom/sec/samsung/gallery/view/common/EditDialog;

    .line 120
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mEditDialog:Lcom/sec/samsung/gallery/view/common/EditDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/EditDialog;->showDialog()V

    .line 121
    return-void
.end method

.method private startShareApp(Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;)V
    .locals 7
    .param p1, "info"    # Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;

    .prologue
    const/4 v6, 0x0

    .line 144
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 146
    .local v1, "editviewIntent":Landroid/content/Intent;
    new-instance v3, Landroid/content/ComponentName;

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 147
    const-string v3, "air.com.adobe.pstouch.oem1"

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 148
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mUri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mMimeType:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 149
    const-string v3, "android.intent.action.EDIT"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 165
    :cond_0
    :goto_0
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 166
    const/high16 v3, 0x10000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 167
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3, v6, v6}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 170
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    const/16 v4, 0x303

    invoke-virtual {v3, v1, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v3

    const-string v4, "EXIT_SELECTION_MODE"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 176
    return-void

    .line 150
    :cond_2
    const-string v3, "com.dama.paperartist"

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 151
    const-string v3, "android.intent.extra.STREAM"

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 152
    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mMimeType:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 155
    :cond_3
    const-string v3, "android.intent.action.EDIT"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 156
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mUri:Landroid/net/Uri;

    const-string v4, "image/*"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 158
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mSelectedItemList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 159
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 160
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "selectedItems"

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mSelectedItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 161
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 162
    const-string v3, "selectedCount"

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mSelectedItemList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 171
    .end local v0    # "bundle":Landroid/os/Bundle;
    :catch_0
    move-exception v2

    .line 172
    .local v2, "excp":Landroid/content/ActivityNotFoundException;
    const-string v3, "com.sec.android.mimage.photoretouching"

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 173
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->showPhotoEditorDownloadDialog()V

    goto :goto_1
.end method


# virtual methods
.method protected createAppList()Ljava/util/ArrayList;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 203
    .local v14, "shareAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;>;"
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePSTouch:Z

    if-eqz v1, :cond_2

    .line 204
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mMimeType:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ImageEditorUtil;->getPSTouchInfo(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v5

    .line 205
    .local v5, "info":Landroid/content/pm/ResolveInfo;
    if-eqz v5, :cond_0

    .line 206
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    .line 207
    .local v13, "packageManager":Landroid/content/pm/PackageManager;
    new-instance v0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;

    iget-object v1, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v1, v13}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v1, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v1, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;-><init>(Lcom/sec/samsung/gallery/controller/ImageEditCmd;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ResolveInfo;)V

    .line 209
    .local v0, "psTouchInfo":Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;
    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 223
    .end local v0    # "psTouchInfo":Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;
    .end local v5    # "info":Landroid/content/pm/ResolveInfo;
    .end local v13    # "packageManager":Landroid/content/pm/PackageManager;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mMimeType:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/ImageEditorUtil;->isPaperArtistAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 224
    const-string v1, "com.dama.paperartist"

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->queryResolveInfo(Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v5

    .line 225
    .restart local v5    # "info":Landroid/content/pm/ResolveInfo;
    if-eqz v5, :cond_1

    .line 226
    new-instance v7, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mContext:Landroid/content/Context;

    const v2, 0x7f0e011e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "com.dama.paperartist"

    iget-object v1, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object v8, p0

    move-object v12, v5

    invoke-direct/range {v7 .. v12}, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;-><init>(Lcom/sec/samsung/gallery/controller/ImageEditCmd;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ResolveInfo;)V

    .line 229
    .local v7, "paperArtistInfo":Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;
    invoke-virtual {v14, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 232
    .end local v5    # "info":Landroid/content/pm/ResolveInfo;
    .end local v7    # "paperArtistInfo":Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;
    :cond_1
    return-object v14

    .line 212
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/ImageEditorUtil;->isSamsungAppsAvailable(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mMimeType:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/ImageEditorUtil;->isPhotoRetouchingAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213
    :cond_3
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoEditorAtShareList:Z

    if-eqz v1, :cond_0

    .line 214
    new-instance v6, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    const v1, 0x7f0e011c

    :goto_1
    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "com.sec.android.mimage.photoretouching"

    const-string v10, "com.sec.android.mimage.photoretouching.PhotoRetouching"

    const-string v1, "com.sec.android.mimage.photoretouching"

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->queryResolveInfo(Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v11

    move-object v7, p0

    invoke-direct/range {v6 .. v11}, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;-><init>(Lcom/sec/samsung/gallery/controller/ImageEditCmd;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ResolveInfo;)V

    .line 218
    .local v6, "photoEditorInfo":Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 214
    .end local v6    # "photoEditorInfo":Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;
    :cond_4
    const v1, 0x7f0e011b

    goto :goto_1
.end method

.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 5
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 67
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Object;

    move-object v0, v2

    check-cast v0, [Ljava/lang/Object;

    .line 68
    .local v0, "params":[Ljava/lang/Object;
    aget-object v2, v0, v3

    check-cast v2, Landroid/content/Context;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mContext:Landroid/content/Context;

    .line 69
    aget-object v2, v0, v4

    check-cast v2, Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mSelectedItemList:Ljava/util/ArrayList;

    .line 70
    const/4 v2, 0x2

    aget-object v2, v0, v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mMimeType:Ljava/lang/String;

    .line 71
    const/4 v2, 0x3

    aget-object v2, v0, v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 73
    .local v1, "show":Z
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 74
    if-nez v1, :cond_0

    .line 79
    :goto_0
    return-void

    .line 77
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mSelectedItemList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mUri:Landroid/net/Uri;

    .line 78
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 4
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 132
    move-object v0, p2

    check-cast v0, Lcom/sec/samsung/gallery/core/Event;

    .line 134
    .local v0, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_START_SELECTED_APP:I

    if-ne v2, v3, :cond_0

    .line 135
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "EXIT_SELECTION_MODE"

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;)V

    .line 136
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;

    .line 137
    .local v1, "info":Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->startShareApp(Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;)V

    .line 139
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->closeCameraLens(Landroid/content/Context;)V

    .line 141
    .end local v1    # "info":Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;
    :cond_0
    return-void
.end method
