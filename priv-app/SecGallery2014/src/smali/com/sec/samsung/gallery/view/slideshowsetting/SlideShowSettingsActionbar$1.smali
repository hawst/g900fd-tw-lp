.class Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar$1;
.super Ljava/lang/Object;
.source "SlideShowSettingsActionbar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)V
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private disablePopupButtons()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 39
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->access$700(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    const v2, 0x7f0f0018

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    # setter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mSelectionButton:Landroid/widget/Button;
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->access$602(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;Landroid/widget/Button;)Landroid/widget/Button;

    .line 41
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->access$800(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    const v2, 0x7f0f0019

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    iput-object v0, v1, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mSelectionButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->access$600(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mSelectionButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->access$600(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    if-eqz v0, :cond_1

    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setVisibility(I)V

    .line 47
    :cond_1
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->access$000(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->access$100(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->access$200(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->access$300(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->access$400(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 34
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar$1;->disablePopupButtons()V

    .line 35
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->access$500(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)Landroid/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0e0186

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 36
    return-void
.end method
