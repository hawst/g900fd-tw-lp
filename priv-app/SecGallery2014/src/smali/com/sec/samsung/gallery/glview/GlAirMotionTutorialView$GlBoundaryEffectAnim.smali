.class public Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$GlBoundaryEffectAnim;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlAirMotionTutorialView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GlBoundaryEffectAnim"
.end annotation


# instance fields
.field mIsLeft:Z

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$GlBoundaryEffectAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 7
    .param p1, "ratio"    # F

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 300
    float-to-double v2, p1

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    cmpg-double v1, v2, v4

    if-gez v1, :cond_0

    .line 301
    mul-float v0, p1, v6

    .line 304
    .local v0, "v":F
    :goto_0
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$GlBoundaryEffectAnim;->mIsLeft:Z

    if-eqz v1, :cond_1

    .line 305
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$GlBoundaryEffectAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->access$100(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    .line 308
    :goto_1
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->applyTransform(F)V

    .line 309
    return-void

    .line 303
    .end local v0    # "v":F
    :cond_0
    mul-float v1, p1, v6

    sub-float v0, v6, v1

    .restart local v0    # "v":F
    goto :goto_0

    .line 307
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$GlBoundaryEffectAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->access$100(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    goto :goto_1
.end method

.method public startAnimation(Lcom/sec/android/gallery3d/glcore/GlLayer;Z)V
    .locals 3
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "isLeft"    # Z

    .prologue
    const/4 v2, 0x0

    .line 287
    iput-boolean p2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$GlBoundaryEffectAnim;->mIsLeft:Z

    .line 288
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$GlBoundaryEffectAnim;->mIsLeft:Z

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$GlBoundaryEffectAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->access$100(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    .line 292
    :goto_0
    invoke-virtual {p1, p0}, Lcom/sec/android/gallery3d/glcore/GlLayer;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 293
    const-wide/16 v0, 0x258

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$GlBoundaryEffectAnim;->setDuration(J)V

    .line 294
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$GlBoundaryEffectAnim;->start()V

    .line 295
    return-void

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$GlBoundaryEffectAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->access$100(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    goto :goto_0
.end method
