.class Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$3;
.super Ljava/lang/Object;
.source "CameraSearchActionBar.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->initializeSearchView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$3;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1
    .param p1, "newText"    # Ljava/lang/String;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$3;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mOldFilterText:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->access$800(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$3;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    # invokes: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->setSuggestionAdapter(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->access$900(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$3;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    # setter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mOldFilterText:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->access$802(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;Ljava/lang/String;)Ljava/lang/String;

    .line 105
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 3
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$3;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_PRESS_SEARCH:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/core/Event;->setData(Ljava/lang/Object;)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->notifyObservers(Ljava/lang/Object;)V

    .line 112
    const/4 v0, 0x0

    return v0
.end method
