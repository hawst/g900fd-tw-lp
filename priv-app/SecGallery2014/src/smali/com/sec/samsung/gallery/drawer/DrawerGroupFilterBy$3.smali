.class Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$3;
.super Lorg/puremvc/java/patterns/mediator/Mediator;
.source "DrawerGroupFilterBy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/Object;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$3;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    invoke-direct {p0, p2, p3}, Lorg/puremvc/java/patterns/mediator/Mediator;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public handleNotification(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 3
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 120
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getName()Ljava/lang/String;

    move-result-object v0

    .line 122
    .local v0, "notiName":Ljava/lang/String;
    const-string v1, "UPDATE_CATEGORY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "drawer UPDATE_CATEGORY true"

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$3;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->updateFilterByGroup(Z)V

    .line 126
    :cond_0
    return-void
.end method

.method public listNotificationInterests()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 114
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "UPDATE_CATEGORY"

    aput-object v2, v0, v1

    return-object v0
.end method
