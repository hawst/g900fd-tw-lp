.class Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$2;
.super Ljava/lang/Object;
.source "CameraSearchView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$2;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 205
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$2;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mCloseButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->access$100(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)Landroid/widget/ImageView;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$2;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    # invokes: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->onCloseClicked()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->access$200(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)V

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$2;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mVoiceButton:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->access$300(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_2

    .line 208
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$2;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    # invokes: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->startVoiceRecognitionActivity()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->access$400(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)V

    goto :goto_0

    .line 209
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$2;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->access$000(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$2;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->access$000(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->forceQuery()V

    goto :goto_0
.end method
