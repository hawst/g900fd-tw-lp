.class Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;
.super Ljava/lang/Object;
.source "HelpInfoPreviewState.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x1

    .line 250
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 288
    :cond_0
    :goto_0
    return v7

    .line 252
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    # setter for: Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mX:F
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->access$502(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;F)F

    .line 255
    :pswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mX:F
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->access$500(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->INVALID_POSITION:F
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->access$600(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 257
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    .line 258
    .local v6, "dX":F
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mX:F
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->access$500(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)F

    move-result v0

    sub-float v0, v6, v0

    const/high16 v1, -0x3db80000    # -50.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mViewMode:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 259
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->soundAndVibrator()V

    .line 260
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    const/16 v1, 0x9

    iput v1, v0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mViewMode:I

    .line 261
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->setHelpView()V

    .line 263
    new-instance v0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3$1;

    const-wide/16 v2, 0xbb8

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3$1;-><init>(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;JJ)V

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3$1;->start()Landroid/os/CountDownTimer;

    goto :goto_0

    .line 283
    .end local v6    # "dX":F
    :pswitch_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->INVALID_POSITION:F
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->access$600(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)F

    move-result v1

    # setter for: Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mX:F
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->access$502(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;F)F

    goto :goto_0

    .line 250
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
