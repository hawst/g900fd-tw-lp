.class Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$1;
.super Ljava/lang/Object;
.source "EventActionBarForEdit.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->setTitle(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

.field final synthetic val$numberOfItemsSelected:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;I)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    iput p2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$1;->val$numberOfItemsSelected:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 157
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->access$000(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v3

    .line 158
    .local v3, "topState":Lcom/sec/android/gallery3d/app/ActivityState;
    if-nez v3, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/ActivityState;->getActionBarManager()Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v0

    .line 161
    .local v0, "actionView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    if-eqz v0, :cond_0

    instance-of v4, v0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    if-eqz v4, :cond_0

    .line 165
    const/4 v1, 0x0

    .line 166
    .local v1, "numberOfSelectedAlbums":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->access$100(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->invalidateOptionsMenu()V

    .line 167
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->access$200(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 169
    .local v2, "res":Landroid/content/res/Resources;
    iget v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$1;->val$numberOfItemsSelected:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    .line 170
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    iget v5, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$1;->val$numberOfItemsSelected:I

    iput v5, v4, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mQuantitySelectedAlbum:I

    .line 171
    iget v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$1;->val$numberOfItemsSelected:I

    if-ne v4, v6, :cond_3

    .line 172
    const v4, 0x7f0e004f

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$1;->val$numberOfItemsSelected:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 181
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    iget v5, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$1;->val$numberOfItemsSelected:I

    # invokes: Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->setSelectAllButtonTitle(ILjava/lang/String;)V
    invoke-static {v4, v5, v1}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->access$300(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;ILjava/lang/String;)V

    goto :goto_0

    .line 176
    :cond_3
    const v4, 0x7f0e0053

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$1;->val$numberOfItemsSelected:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
