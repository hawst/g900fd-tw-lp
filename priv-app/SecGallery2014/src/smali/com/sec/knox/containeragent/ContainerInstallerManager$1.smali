.class Lcom/sec/knox/containeragent/ContainerInstallerManager$1;
.super Ljava/lang/Object;
.source "ContainerInstallerManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/containeragent/ContainerInstallerManager;-><init>(Landroid/content/Context;Landroid/content/ServiceConnection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/containeragent/ContainerInstallerManager;

.field private final synthetic val$connection:Landroid/content/ServiceConnection;


# direct methods
.method constructor <init>(Lcom/sec/knox/containeragent/ContainerInstallerManager;Landroid/content/ServiceConnection;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager$1;->this$0:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    iput-object p2, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager$1;->val$connection:Landroid/content/ServiceConnection;

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 46
    # getter for: Lcom/sec/knox/containeragent/ContainerInstallerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onServiceConnected() name="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", conn="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager$1;->this$0:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    # getter for: Lcom/sec/knox/containeragent/ContainerInstallerManager;->conn:Landroid/content/ServiceConnection;
    invoke-static {v2}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->access$1(Lcom/sec/knox/containeragent/ContainerInstallerManager;)Landroid/content/ServiceConnection;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    iget-object v0, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager$1;->this$0:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->access$2(Lcom/sec/knox/containeragent/ContainerInstallerManager;Z)V

    .line 49
    iget-object v0, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager$1;->this$0:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    invoke-static {p2}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->access$3(Lcom/sec/knox/containeragent/ContainerInstallerManager;Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;)V

    .line 51
    iget-object v0, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager$1;->val$connection:Landroid/content/ServiceConnection;

    invoke-interface {v0, p1, p2}, Landroid/content/ServiceConnection;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    .line 52
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 55
    # getter for: Lcom/sec/knox/containeragent/ContainerInstallerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onServiceDisconnected() name="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    iget-object v0, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager$1;->this$0:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->access$2(Lcom/sec/knox/containeragent/ContainerInstallerManager;Z)V

    .line 58
    iget-object v0, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager$1;->val$connection:Landroid/content/ServiceConnection;

    invoke-interface {v0, p1}, Landroid/content/ServiceConnection;->onServiceDisconnected(Landroid/content/ComponentName;)V

    .line 59
    return-void
.end method
