.class public Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;
.super Ljava/lang/Object;
.source "PrimitiveCircleARC.java"


# instance fields
.field public Center:Landroid/graphics/Point;

.field public Direction:I

.field public Radius:I

.field public end:Landroid/graphics/Point;

.field public start:Landroid/graphics/Point;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCenter()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->Center:Landroid/graphics/Point;

    return-object v0
.end method

.method public getDirection()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->Direction:I

    return v0
.end method

.method public getEnd()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->end:Landroid/graphics/Point;

    return-object v0
.end method

.method public getRadius()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->Radius:I

    return v0
.end method

.method public getStart()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->start:Landroid/graphics/Point;

    return-object v0
.end method

.method public setCenter(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "center"    # Landroid/graphics/Point;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->Center:Landroid/graphics/Point;

    .line 28
    return-void
.end method

.method public setDirection(I)V
    .locals 0
    .param p1, "direction"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->Direction:I

    .line 22
    return-void
.end method

.method public setEnd(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "end"    # Landroid/graphics/Point;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->end:Landroid/graphics/Point;

    .line 40
    return-void
.end method

.method public setRadius(I)V
    .locals 0
    .param p1, "radius"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->Radius:I

    .line 46
    return-void
.end method

.method public setStart(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "start"    # Landroid/graphics/Point;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->start:Landroid/graphics/Point;

    .line 34
    return-void
.end method
