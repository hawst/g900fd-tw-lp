.class public Lcom/samsung/vip/engine/shape/PrimitiveBezier;
.super Ljava/lang/Object;
.source "PrimitiveBezier.java"


# instance fields
.field public control01:Landroid/graphics/Point;

.field public control02:Landroid/graphics/Point;

.field public control03:Landroid/graphics/Point;

.field public control04:Landroid/graphics/Point;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getControl01()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveBezier;->control01:Landroid/graphics/Point;

    return-object v0
.end method

.method public getControl02()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveBezier;->control02:Landroid/graphics/Point;

    return-object v0
.end method

.method public getControl03()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveBezier;->control03:Landroid/graphics/Point;

    return-object v0
.end method

.method public getControl04()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveBezier;->control04:Landroid/graphics/Point;

    return-object v0
.end method

.method public setControl01(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "control01"    # Landroid/graphics/Point;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveBezier;->control01:Landroid/graphics/Point;

    .line 22
    return-void
.end method

.method public setControl02(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "control02"    # Landroid/graphics/Point;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveBezier;->control02:Landroid/graphics/Point;

    .line 30
    return-void
.end method

.method public setControl03(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "control03"    # Landroid/graphics/Point;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveBezier;->control03:Landroid/graphics/Point;

    .line 38
    return-void
.end method

.method public setControl04(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "control04"    # Landroid/graphics/Point;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveBezier;->control04:Landroid/graphics/Point;

    .line 46
    return-void
.end method
