.class public abstract Lcom/samsung/vip/engine/VITextRecognitionLib;
.super Ljava/lang/Object;
.source "VITextRecognitionLib.java"


# static fields
.field private static final ENABLE_LANG_SET:[Ljava/lang/String;

.field private static final ENABLE_LANG_SET_ID:[I

.field public static final LIBNAME:Ljava/lang/String; = "VIText"

.field private static final TAG:Ljava/lang/String; = "VITextRecognitionLib"

.field public static final VERSION:Ljava/lang/String; = "0.8.3"

.field public static final VIHW_ENGINE_RAM_SIZE:I = 0xa800

.field public static final VIHW_LANG_CHINESE:I = 0x0

.field public static final VIHW_LANG_CHINESE_CURSIVE_ENG:I = 0x1a

.field public static final VIHW_LANG_CHINESE_ENGLISH:I = 0x18

.field public static final VIHW_LANG_CHINESE_HK:I = 0x1c

.field public static final VIHW_LANG_CHINESE_HK_OVERLAY:I = 0x20

.field public static final VIHW_LANG_CHINESE_S_OVERLAY:I = 0x1e

.field public static final VIHW_LANG_CHINESE_T:I = 0x1b

.field public static final VIHW_LANG_CHINESE_TW:I = 0x1d

.field public static final VIHW_LANG_CHINESE_TW_OVERLAY:I = 0x21

.field public static final VIHW_LANG_CHINESE_T_OVERLAY:I = 0x1f

.field public static final VIHW_LANG_CURSIVE_ENGLISH:I = 0xa

.field public static final VIHW_LANG_CURSIVE_ENG_FRE:I = 0x11

.field public static final VIHW_LANG_CURSIVE_ENG_GER:I = 0x12

.field public static final VIHW_LANG_CURSIVE_ENG_ITA:I = 0x13

.field public static final VIHW_LANG_CURSIVE_ENG_POR:I = 0x14

.field public static final VIHW_LANG_CURSIVE_ENG_RUS:I = 0x16

.field public static final VIHW_LANG_CURSIVE_ENG_SPA:I = 0x15

.field public static final VIHW_LANG_CURSIVE_FRENCH:I = 0xb

.field public static final VIHW_LANG_CURSIVE_GERMAN:I = 0xc

.field public static final VIHW_LANG_CURSIVE_ITALIAN:I = 0xd

.field public static final VIHW_LANG_CURSIVE_PORTUGUESE:I = 0xe

.field public static final VIHW_LANG_CURSIVE_RUSSIAN:I = 0x10

.field public static final VIHW_LANG_CURSIVE_SPANISH:I = 0xf

.field public static final VIHW_LANG_ENGLISH:I = 0x3

.field public static final VIHW_LANG_FRENCH:I = 0x4

.field public static final VIHW_LANG_GERMAN:I = 0x5

.field public static final VIHW_LANG_ITALIAN:I = 0x6

.field public static final VIHW_LANG_JAPANESE:I = 0x2

.field public static final VIHW_LANG_KOREAN:I = 0x1

.field public static final VIHW_LANG_KOREAN_CURSIVE_ENG:I = 0x19

.field public static final VIHW_LANG_KOREAN_ENGLISH:I = 0x17

.field public static final VIHW_LANG_NUMERIC:I = 0x22

.field public static final VIHW_LANG_PORTUGUESE:I = 0x7

.field public static final VIHW_LANG_RUSSIAN:I = 0x9

.field public static final VIHW_LANG_SPANISH:I = 0x8

.field public static final VIHW_LANG_WHAT:I = 0x64

.field public static final VIHW_MAX_CANDIDATE_NUM:I = 0x12

.field public static final VIHW_MAX_POINT_DATA_NUM:I = 0x1f40

.field public static final VIHW_MAX_POINT_NUM:I = 0xfa0

.field public static final VIHW_MAX_RECT_HEIGHT:I = 0x320

.field public static final VIHW_MAX_RECT_WIDTH:I = 0x320

.field public static final VIHW_MAX_STRING_LEN:I = 0x16d

.field public static final VIHW_RECOG_TYPE_CHAR:I = 0x0

.field public static final VIHW_RECOG_TYPE_MULTI_LINE:I = 0x3

.field public static final VIHW_RECOG_TYPE_SINGLE_LINE:I = 0x2

.field public static final VIHW_RECOG_TYPE_STRING:I = 0x1


# instance fields
.field protected mLangMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mXstrokeList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<[F>;"
        }
    .end annotation
.end field

.field private mYstrokeList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<[F>;"
        }
    .end annotation
.end field

.field private mbAddStrokeDirectly:Z

.field private mbInitialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 97
    new-array v0, v7, [Ljava/lang/String;

    .line 98
    const-string v1, "en_US"

    aput-object v1, v0, v5

    const-string v1, "ko_KR"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string/jumbo v2, "zh_CN"

    aput-object v2, v0, v1

    const-string v1, "eng"

    aput-object v1, v0, v4

    const-string v1, "kor"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "chn"

    aput-object v2, v0, v1

    .line 97
    sput-object v0, Lcom/samsung/vip/engine/VITextRecognitionLib;->ENABLE_LANG_SET:[Ljava/lang/String;

    .line 100
    new-array v0, v7, [I

    .line 101
    aput v4, v0, v5

    aput v3, v0, v3

    aput v4, v0, v4

    aput v3, v0, v6

    .line 100
    sput-object v0, Lcom/samsung/vip/engine/VITextRecognitionLib;->ENABLE_LANG_SET_ID:[I

    .line 102
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object v2, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mLangMap:Ljava/util/HashMap;

    .line 106
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mbAddStrokeDirectly:Z

    .line 107
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mbInitialized:Z

    .line 127
    iput-object v2, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    .line 128
    iput-object v2, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    .line 131
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    .line 132
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    .line 133
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mLangMap:Ljava/util/HashMap;

    .line 134
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/samsung/vip/engine/VITextRecognitionLib;->ENABLE_LANG_SET:[Ljava/lang/String;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 137
    return-void

    .line 135
    :cond_0
    iget-object v1, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mLangMap:Ljava/util/HashMap;

    sget-object v2, Lcom/samsung/vip/engine/VITextRecognitionLib;->ENABLE_LANG_SET:[Ljava/lang/String;

    aget-object v2, v2, v0

    sget-object v3, Lcom/samsung/vip/engine/VITextRecognitionLib;->ENABLE_LANG_SET_ID:[I

    aget v3, v3, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private declared-synchronized addStroke([F[FZ)V
    .locals 1
    .param p1, "x"    # [F
    .param p2, "y"    # [F
    .param p3, "bAddStrokeDirectly"    # Z

    .prologue
    .line 278
    monitor-enter p0

    if-eqz p3, :cond_0

    .line 279
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/samsung/vip/engine/VITextRecognitionLib;->VIText_AddStroke([F[F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 284
    :goto_0
    monitor-exit p0

    return-void

    .line 281
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 282
    iget-object v0, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 278
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private getStandardLang(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "lang"    # Ljava/lang/String;

    .prologue
    .line 323
    const-string v0, "eng"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 324
    const-string p1, "en_US"

    .line 330
    .end local p1    # "lang":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 325
    .restart local p1    # "lang":Ljava/lang/String;
    :cond_1
    const-string v0, "kor"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 326
    const-string p1, "ko_KR"

    goto :goto_0

    .line 327
    :cond_2
    const-string v0, "chn"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    const-string/jumbo p1, "zh_CN"

    goto :goto_0
.end method

.method private declared-synchronized recog(Z)[Ljava/lang/String;
    .locals 13
    .param p1, "bAddStrokeDirectly"    # Z

    .prologue
    const/4 v8, 0x0

    .line 191
    monitor-enter p0

    if-eqz p1, :cond_1

    .line 192
    const/4 v10, 0x3

    :try_start_0
    invoke-virtual {p0, v10}, Lcom/samsung/vip/engine/VITextRecognitionLib;->VIText_Recog(I)I

    move-result v9

    .line 220
    .local v9, "ret":I
    :goto_0
    if-eqz v9, :cond_5

    .line 221
    const-string v10, "VITextRecognitionLib"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Error Code: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    :cond_0
    :goto_1
    monitor-exit p0

    return-object v8

    .line 194
    .end local v9    # "ret":I
    :cond_1
    :try_start_1
    iget-object v10, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->size()I

    move-result v5

    .line 195
    .local v5, "nStrokeSize":I
    const/4 v6, 0x0

    .line 196
    .local v6, "nTotalPointSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    if-lt v0, v5, :cond_2

    .line 199
    add-int/lit8 v6, v6, 0x1

    .line 201
    const/4 v2, 0x0

    .line 202
    .local v2, "nPointIndex":I
    mul-int/lit8 v10, v6, 0x2

    new-array v7, v10, [I

    .line 203
    .local v7, "pPointData":[I
    const/4 v0, 0x0

    move v3, v2

    .end local v2    # "nPointIndex":I
    .local v3, "nPointIndex":I
    :goto_3
    if-lt v0, v5, :cond_3

    .line 212
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "nPointIndex":I
    .restart local v2    # "nPointIndex":I
    const v10, 0xffff

    aput v10, v7, v3

    .line 213
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "nPointIndex":I
    .restart local v3    # "nPointIndex":I
    const v10, 0xffff

    aput v10, v7, v2

    .line 215
    iget-object v10, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->clear()V

    .line 216
    iget-object v10, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->clear()V

    .line 218
    const/4 v10, 0x3

    invoke-virtual {p0, v10, v7, v6}, Lcom/samsung/vip/engine/VITextRecognitionLib;->VIText_Recog(I[II)I

    move-result v9

    .restart local v9    # "ret":I
    goto :goto_0

    .line 197
    .end local v3    # "nPointIndex":I
    .end local v7    # "pPointData":[I
    .end local v9    # "ret":I
    :cond_2
    iget-object v10, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [F

    array-length v10, v10

    add-int/lit8 v10, v10, 0x1

    add-int/2addr v6, v10

    .line 196
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 204
    .restart local v3    # "nPointIndex":I
    .restart local v7    # "pPointData":[I
    :cond_3
    iget-object v10, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [F

    array-length v4, v10

    .line 205
    .local v4, "nPointSize":I
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_4
    if-lt v1, v4, :cond_4

    .line 209
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "nPointIndex":I
    .restart local v2    # "nPointIndex":I
    const v10, 0xffff

    aput v10, v7, v3

    .line 210
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "nPointIndex":I
    .restart local v3    # "nPointIndex":I
    const/4 v10, 0x0

    aput v10, v7, v2

    .line 203
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 206
    :cond_4
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "nPointIndex":I
    .restart local v2    # "nPointIndex":I
    iget-object v10, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [F

    aget v10, v10, v1

    float-to-int v10, v10

    aput v10, v7, v3

    .line 207
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "nPointIndex":I
    .restart local v3    # "nPointIndex":I
    iget-object v10, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [F

    aget v10, v10, v1

    float-to-int v10, v10

    aput v10, v7, v2

    .line 205
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 231
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v3    # "nPointIndex":I
    .end local v4    # "nPointSize":I
    .end local v5    # "nStrokeSize":I
    .end local v6    # "nTotalPointSize":I
    .end local v7    # "pPointData":[I
    .restart local v9    # "ret":I
    :cond_5
    iget-boolean v10, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mbInitialized:Z

    if-eqz v10, :cond_0

    .line 235
    invoke-virtual {p0}, Lcom/samsung/vip/engine/VITextRecognitionLib;->VIText_GetResultList()[Ljava/lang/String;

    move-result-object v8

    .line 236
    .local v8, "resultList":[Ljava/lang/String;
    if-nez v8, :cond_0

    .line 237
    const-string v10, "VITextRecognitionLib"

    const-string v11, "GetResultList() return null!"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 191
    .end local v8    # "resultList":[Ljava/lang/String;
    .end local v9    # "ret":I
    :catchall_0
    move-exception v10

    monitor-exit p0

    throw v10
.end method


# virtual methods
.method protected abstract VIText_AddStroke([F[F)V
.end method

.method protected abstract VIText_ClearStrokes()V
.end method

.method protected abstract VIText_Close()V
.end method

.method protected abstract VIText_GenerateAndSave(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method protected abstract VIText_GetCharResultInfo()Lcom/samsung/vip/engine/VICharResultInfo;
.end method

.method protected abstract VIText_GetResult()Ljava/lang/String;
.end method

.method protected abstract VIText_GetResultList()[Ljava/lang/String;
.end method

.method protected abstract VIText_GetScoreList()[F
.end method

.method protected abstract VIText_GetWordInfo(I)Lcom/samsung/vip/engine/VIWordInfo;
.end method

.method protected abstract VIText_Init(Ljava/lang/String;IIIS)I
.end method

.method protected abstract VIText_Init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method protected abstract VIText_Init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method protected abstract VIText_Recog(I)I
.end method

.method protected abstract VIText_Recog(I[II)I
.end method

.method protected abstract VIText_SetUserDictMode(Ljava/lang/String;)I
.end method

.method public declared-synchronized addStroke([F[F)V
    .locals 1
    .param p1, "x"    # [F
    .param p2, "y"    # [F

    .prologue
    .line 265
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mbAddStrokeDirectly:Z

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/vip/engine/VITextRecognitionLib;->addStroke([F[FZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 266
    monitor-exit p0

    return-void

    .line 265
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized clearStrokes()V
    .locals 1

    .prologue
    .line 269
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mbAddStrokeDirectly:Z

    if-nez v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 273
    iget-object v0, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275
    :cond_0
    monitor-exit p0

    return-void

    .line 269
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 181
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mbInitialized:Z

    .line 182
    invoke-virtual {p0}, Lcom/samsung/vip/engine/VITextRecognitionLib;->VIText_Close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    monitor-exit p0

    return-void

    .line 181
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public generateUserDictDataFile(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1, "inFile"    # Ljava/lang/String;
    .param p2, "outFile"    # Ljava/lang/String;

    .prologue
    .line 384
    invoke-virtual {p0, p1, p2}, Lcom/samsung/vip/engine/VITextRecognitionLib;->VIText_GenerateAndSave(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getCharInfo()Lcom/samsung/vip/engine/VICharResultInfo;
    .locals 1

    .prologue
    .line 379
    invoke-virtual {p0}, Lcom/samsung/vip/engine/VITextRecognitionLib;->VIText_GetCharResultInfo()Lcom/samsung/vip/engine/VICharResultInfo;

    move-result-object v0

    return-object v0
.end method

.method public getLangList()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mLangMap:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mLangMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mLangMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mLangMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 296
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLanguageMode(Ljava/lang/String;)I
    .locals 4
    .param p1, "lang"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, -0x1

    .line 301
    invoke-direct {p0, p1}, Lcom/samsung/vip/engine/VITextRecognitionLib;->getStandardLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 302
    .local v1, "stdLang":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mLangMap:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 309
    :cond_0
    :goto_0
    return v2

    .line 305
    :cond_1
    iget-object v3, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mLangMap:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 306
    .local v0, "langMode":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 309
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_0
.end method

.method protected getRecogResultCandidate(Ljava/lang/String;)[Ljava/lang/String;
    .locals 8
    .param p1, "rawRet"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const v7, 0xffff

    .line 343
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    .line 344
    .local v4, "nLen":I
    const/4 v3, 0x0

    .line 345
    .local v3, "nCandNum":I
    const/4 v1, 0x0

    .local v1, "ii":I
    :goto_0
    if-lt v1, v4, :cond_1

    .line 352
    if-nez v3, :cond_3

    .line 353
    const/4 v0, 0x0

    .line 367
    :cond_0
    return-object v0

    .line 346
    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v6, v7, :cond_2

    .line 347
    add-int/lit8 v3, v3, 0x1

    .line 345
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 356
    :cond_3
    new-array v0, v3, [Ljava/lang/String;

    .line 357
    .local v0, "arr":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 358
    .local v5, "startId":I
    const/4 v2, 0x0

    .line 359
    .local v2, "jj":I
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_0

    .line 360
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v6, v7, :cond_4

    .line 361
    invoke-virtual {p1, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v2

    .line 362
    add-int/lit8 v2, v2, 0x1

    .line 363
    add-int/lit8 v5, v1, 0x1

    .line 359
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public getResultScoreList()[F
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/samsung/vip/engine/VITextRecognitionLib;->VIText_GetScoreList()[F

    move-result-object v0

    return-object v0
.end method

.method public getWordInfo(I)Lcom/samsung/vip/engine/VIWordInfo;
    .locals 1
    .param p1, "nWordID"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 373
    invoke-virtual {p0, p1}, Lcom/samsung/vip/engine/VITextRecognitionLib;->VIText_GetWordInfo(I)Lcom/samsung/vip/engine/VIWordInfo;

    move-result-object v0

    return-object v0
.end method

.method public init(Ljava/lang/String;IS)I
    .locals 7
    .param p1, "szDataPath"    # Ljava/lang/String;
    .param p2, "nLangMode"    # I
    .param p3, "sRangeMode"    # S
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 149
    const/16 v3, 0x640

    const/16 v4, 0x4b0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/vip/engine/VITextRecognitionLib;->VIText_Init(Ljava/lang/String;IIIS)I

    move-result v6

    .line 150
    .local v6, "ret":I
    if-nez v6, :cond_0

    .line 151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mbInitialized:Z

    .line 155
    :goto_0
    return v6

    .line 153
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mbInitialized:Z

    goto :goto_0
.end method

.method public init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "dataPath"    # Ljava/lang/String;
    .param p2, "lang"    # Ljava/lang/String;
    .param p3, "mode"    # Ljava/lang/String;

    .prologue
    .line 159
    invoke-direct {p0, p2}, Lcom/samsung/vip/engine/VITextRecognitionLib;->getStandardLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 160
    .local v1, "stdLang":Ljava/lang/String;
    invoke-virtual {p0, p1, v1, p3}, Lcom/samsung/vip/engine/VITextRecognitionLib;->VIText_Init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 161
    .local v0, "ret":I
    if-nez v0, :cond_0

    .line 162
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mbInitialized:Z

    .line 166
    :goto_0
    return v0

    .line 164
    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mbInitialized:Z

    goto :goto_0
.end method

.method public init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "mainDataPath"    # Ljava/lang/String;
    .param p2, "subDataPath"    # Ljava/lang/String;
    .param p3, "lang"    # Ljava/lang/String;
    .param p4, "mode"    # Ljava/lang/String;

    .prologue
    .line 170
    invoke-direct {p0, p3}, Lcom/samsung/vip/engine/VITextRecognitionLib;->getStandardLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 171
    .local v1, "stdLang":Ljava/lang/String;
    invoke-virtual {p0, p1, p2, v1, p4}, Lcom/samsung/vip/engine/VITextRecognitionLib;->VIText_Init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 172
    .local v0, "ret":I
    if-nez v0, :cond_0

    .line 173
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mbInitialized:Z

    .line 177
    :goto_0
    return v0

    .line 175
    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mbInitialized:Z

    goto :goto_0
.end method

.method public isSupportedLanguage(Ljava/lang/String;)Z
    .locals 2
    .param p1, "lang"    # Ljava/lang/String;

    .prologue
    .line 314
    iget-object v1, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mLangMap:Ljava/util/HashMap;

    if-nez v1, :cond_0

    .line 315
    const/4 v1, 0x0

    .line 318
    :goto_0
    return v1

    .line 317
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/vip/engine/VITextRecognitionLib;->getStandardLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 318
    .local v0, "stdLang":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mLangMap:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public loadLibrary(Ljava/lang/String;)V
    .locals 4
    .param p1, "pathName"    # Ljava/lang/String;

    .prologue
    .line 141
    :try_start_0
    invoke-static {p1}, Ljava/lang/System;->load(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    :goto_0
    return-void

    .line 142
    :catch_0
    move-exception v0

    .line 143
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    const-string v1, "VITextRecognitionLib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "UnsatisfiedLinkError! so path = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public declared-synchronized recog()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mbAddStrokeDirectly:Z

    invoke-direct {p0, v0}, Lcom/samsung/vip/engine/VITextRecognitionLib;->recog(Z)[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized recog([II)[Ljava/lang/String;
    .locals 5
    .param p1, "pPointData"    # [I
    .param p2, "nPointNumber"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 244
    monitor-enter p0

    const/4 v2, 0x3

    :try_start_0
    invoke-virtual {p0, v2, p1, p2}, Lcom/samsung/vip/engine/VITextRecognitionLib;->VIText_Recog(I[II)I

    move-result v1

    .line 245
    .local v1, "ret":I
    if-eqz v1, :cond_1

    .line 246
    const-string v2, "VITextRecognitionLib"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error Code: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 247
    const/4 v0, 0x0

    .line 261
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 257
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/vip/engine/VITextRecognitionLib;->VIText_GetResultList()[Ljava/lang/String;

    move-result-object v0

    .line 258
    .local v0, "resultList":[Ljava/lang/String;
    if-nez v0, :cond_0

    .line 259
    const-string v2, "VITextRecognitionLib"

    const-string v3, "GetResultList() return null!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 244
    .end local v0    # "resultList":[Ljava/lang/String;
    .end local v1    # "ret":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public setAddStrokeDirectly(Z)V
    .locals 0
    .param p1, "bAddStrokeDirectly"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 288
    iput-boolean p1, p0, Lcom/samsung/vip/engine/VITextRecognitionLib;->mbAddStrokeDirectly:Z

    .line 289
    return-void
.end method

.method public setUserDictMode(Ljava/lang/String;)I
    .locals 1
    .param p1, "dataFile"    # Ljava/lang/String;

    .prologue
    .line 388
    invoke-virtual {p0, p1}, Lcom/samsung/vip/engine/VITextRecognitionLib;->VIText_SetUserDictMode(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
