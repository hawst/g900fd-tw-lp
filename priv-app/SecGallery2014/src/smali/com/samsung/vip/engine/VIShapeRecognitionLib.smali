.class public Lcom/samsung/vip/engine/VIShapeRecognitionLib;
.super Lcom/samsung/vip/engine/VIRecognitionLib;
.source "VIShapeRecognitionLib.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/samsung/vip/engine/VIRecognitionLib;-><init>()V

    .line 15
    return-void
.end method


# virtual methods
.method public declared-synchronized addStroke([F[F)V
    .locals 1
    .param p1, "x"    # [F
    .param p2, "y"    # [F

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 80
    iget-object v0, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    monitor-exit p0

    return-void

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized clearScene()V
    .locals 1

    .prologue
    .line 35
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->VISH_ClearScene()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    monitor-exit p0

    return-void

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 28
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mbInitialized:Z

    .line 30
    invoke-virtual {p0}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->VISH_ReleaseSmartShapeEngine()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31
    monitor-exit p0

    return-void

    .line 28
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPrimitiveName(I)Ljava/lang/String;
    .locals 1
    .param p1, "nType"    # I

    .prologue
    .line 84
    packed-switch p1, :pswitch_data_0

    .line 108
    :pswitch_0
    const-string v0, ""

    :goto_0
    return-object v0

    .line 86
    :pswitch_1
    const-string v0, "line"

    goto :goto_0

    .line 88
    :pswitch_2
    const-string v0, "arrow"

    goto :goto_0

    .line 90
    :pswitch_3
    const-string v0, "bezier"

    goto :goto_0

    .line 92
    :pswitch_4
    const-string v0, "circle"

    goto :goto_0

    .line 94
    :pswitch_5
    const-string v0, "circlearc"

    goto :goto_0

    .line 96
    :pswitch_6
    const-string v0, "ellipse"

    goto :goto_0

    .line 98
    :pswitch_7
    const-string v0, "polygon"

    goto :goto_0

    .line 100
    :pswitch_8
    const-string v0, "ellipsearc"

    goto :goto_0

    .line 102
    :pswitch_9
    const-string/jumbo v0, "table"

    goto :goto_0

    .line 104
    :pswitch_a
    const-string v0, "group"

    goto :goto_0

    .line 106
    :pswitch_b
    const-string v0, "polyline"

    goto :goto_0

    .line 84
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_8
        :pswitch_3
        :pswitch_a
        :pswitch_2
        :pswitch_9
        :pswitch_b
    .end packed-switch
.end method

.method public init()V
    .locals 1

    .prologue
    .line 18
    const/high16 v0, 0xa00000

    invoke-virtual {p0, v0}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->VISH_InitSmartShapeEngine(I)V

    .line 19
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mbInitialized:Z

    .line 20
    return-void
.end method

.method public init(Ljava/lang/String;)V
    .locals 1
    .param p1, "dataPath"    # Ljava/lang/String;

    .prologue
    .line 23
    const/high16 v0, 0xa00000

    invoke-virtual {p0, v0, p1}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->VISH_InitSmartShapeEngineWithData(ILjava/lang/String;)V

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mbInitialized:Z

    .line 25
    return-void
.end method

.method public declared-synchronized recog()[Lcom/samsung/vip/engine/shape/GraphPrimitive;
    .locals 12

    .prologue
    .line 39
    monitor-enter p0

    :try_start_0
    iget-object v9, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->size()I

    move-result v6

    .line 40
    .local v6, "nStrokeSize":I
    const/4 v7, 0x0

    .line 41
    .local v7, "nTotalPointSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v6, :cond_1

    .line 46
    const/4 v3, 0x0

    .line 47
    .local v3, "nPointIndex":I
    mul-int/lit8 v9, v7, 0x2

    new-array v8, v9, [I

    .line 48
    .local v8, "pPointData":[I
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v6, :cond_2

    .line 60
    iget-object v9, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->clear()V

    .line 61
    iget-object v9, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->clear()V

    .line 63
    invoke-virtual {p0, v8}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->VISH_UpdateScene([I)V

    .line 65
    invoke-virtual {p0}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->VISH_GetGraphPrimitiveArray()[Lcom/samsung/vip/engine/shape/GraphPrimitive;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 66
    .local v0, "graphPrimitives":[Lcom/samsung/vip/engine/shape/GraphPrimitive;
    if-nez v0, :cond_4

    .line 67
    const/4 v0, 0x0

    .line 75
    .end local v0    # "graphPrimitives":[Lcom/samsung/vip/engine/shape/GraphPrimitive;
    :cond_0
    :goto_2
    monitor-exit p0

    return-object v0

    .line 42
    .end local v3    # "nPointIndex":I
    .end local v8    # "pPointData":[I
    :cond_1
    :try_start_1
    iget-object v9, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v9, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [F

    array-length v9, v9

    add-int/lit8 v9, v9, 0x1

    add-int/2addr v7, v9

    .line 41
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 49
    .restart local v3    # "nPointIndex":I
    .restart local v8    # "pPointData":[I
    :cond_2
    iget-object v9, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v9, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [F

    array-length v5, v9

    .line 50
    .local v5, "nPointSize":I
    const/4 v2, 0x0

    .local v2, "j":I
    move v4, v3

    .end local v3    # "nPointIndex":I
    .local v4, "nPointIndex":I
    :goto_3
    if-lt v2, v5, :cond_3

    .line 54
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "nPointIndex":I
    .restart local v3    # "nPointIndex":I
    const v9, 0xffff

    aput v9, v8, v4

    .line 55
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "nPointIndex":I
    .restart local v4    # "nPointIndex":I
    const/4 v9, 0x0

    aput v9, v8, v3

    .line 48
    add-int/lit8 v1, v1, 0x1

    move v3, v4

    .end local v4    # "nPointIndex":I
    .restart local v3    # "nPointIndex":I
    goto :goto_1

    .line 51
    .end local v3    # "nPointIndex":I
    .restart local v4    # "nPointIndex":I
    :cond_3
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "nPointIndex":I
    .restart local v3    # "nPointIndex":I
    iget-object v9, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v9, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [F

    aget v9, v9, v2

    float-to-int v9, v9

    aput v9, v8, v4

    .line 52
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "nPointIndex":I
    .restart local v4    # "nPointIndex":I
    iget-object v9, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v9, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [F

    aget v9, v9, v2

    float-to-int v9, v9

    aput v9, v8, v3

    .line 50
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 69
    .end local v2    # "j":I
    .end local v4    # "nPointIndex":I
    .end local v5    # "nPointSize":I
    .restart local v0    # "graphPrimitives":[Lcom/samsung/vip/engine/shape/GraphPrimitive;
    .restart local v3    # "nPointIndex":I
    :cond_4
    sget-object v9, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Result group # : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v11, v0

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iget-boolean v9, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mbInitialized:Z

    if-nez v9, :cond_0

    .line 72
    sget-object v9, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Initialized : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v11, p0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->mbInitialized:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 39
    .end local v0    # "graphPrimitives":[Lcom/samsung/vip/engine/shape/GraphPrimitive;
    .end local v1    # "i":I
    .end local v3    # "nPointIndex":I
    .end local v6    # "nStrokeSize":I
    .end local v7    # "nTotalPointSize":I
    .end local v8    # "pPointData":[I
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9
.end method

.method public setDeviceDPI(I)V
    .locals 0
    .param p1, "deviceDPI"    # I

    .prologue
    .line 113
    invoke-virtual {p0, p1}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->VISH_SetDeviceDPI(I)V

    .line 114
    return-void
.end method
