.class public Lcom/samsung/vip/engine/shape/ssConstant$PRIMITIVE_TYPE;
.super Ljava/lang/Object;
.source "ssConstant.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/vip/engine/shape/ssConstant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PRIMITIVE_TYPE"
.end annotation


# static fields
.field public static final PRI_ARROW:I = 0xb

.field public static final PRI_BEZIER:I = 0x9

.field public static final PRI_CIRCLE:I = 0x2

.field public static final PRI_CIRCLEARC:I = 0x7

.field public static final PRI_ELLIPSE:I = 0x6

.field public static final PRI_ELLIPSEARC:I = 0x8

.field public static final PRI_GROUP:I = 0xa

.field public static final PRI_LINE:I = 0x1

.field public static final PRI_NONE:I = 0x0

.field public static final PRI_POLYGON:I = 0x5

.field public static final PRI_POLYLINE:I = 0xd

.field public static final PRI_QUAD:I = 0x4

.field public static final PRI_TABLE:I = 0xc

.field public static final PRI_TRIANGLE:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
