.class public Lcom/samsung/vip/engine/shape/PrimitiveArrow;
.super Ljava/lang/Object;
.source "PrimitiveArrow.java"


# instance fields
.field public end:Landroid/graphics/Point;

.field public left:Landroid/graphics/Point;

.field public right:Landroid/graphics/Point;

.field public start:Landroid/graphics/Point;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getEnd()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->end:Landroid/graphics/Point;

    return-object v0
.end method

.method public getLeft()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->left:Landroid/graphics/Point;

    return-object v0
.end method

.method public getRight()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->right:Landroid/graphics/Point;

    return-object v0
.end method

.method public getStart()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->start:Landroid/graphics/Point;

    return-object v0
.end method

.method public setEnd(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "end"    # Landroid/graphics/Point;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->end:Landroid/graphics/Point;

    .line 31
    return-void
.end method

.method public setLeft(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "left"    # Landroid/graphics/Point;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->left:Landroid/graphics/Point;

    .line 47
    return-void
.end method

.method public setRight(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "right"    # Landroid/graphics/Point;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->right:Landroid/graphics/Point;

    .line 39
    return-void
.end method

.method public setStart(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "start"    # Landroid/graphics/Point;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->start:Landroid/graphics/Point;

    .line 23
    return-void
.end method
