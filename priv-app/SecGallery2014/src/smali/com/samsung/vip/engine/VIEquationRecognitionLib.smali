.class public Lcom/samsung/vip/engine/VIEquationRecognitionLib;
.super Lcom/samsung/vip/engine/VIRecognitionLib;
.source "VIEquationRecognitionLib.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const-class v0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/samsung/vip/engine/VIRecognitionLib;-><init>()V

    .line 11
    return-void
.end method

.method private getRecogResultCandidate(Ljava/lang/String;)[Ljava/lang/String;
    .locals 8
    .param p1, "rawRet"    # Ljava/lang/String;

    .prologue
    const v7, 0xffff

    .line 101
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    .line 102
    .local v4, "nLen":I
    const/4 v3, 0x0

    .line 103
    .local v3, "nCandNum":I
    const/4 v1, 0x0

    .local v1, "ii":I
    :goto_0
    if-lt v1, v4, :cond_1

    .line 110
    if-nez v3, :cond_3

    .line 111
    const/4 v0, 0x0

    .line 125
    :cond_0
    return-object v0

    .line 104
    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v6, v7, :cond_2

    .line 105
    add-int/lit8 v3, v3, 0x1

    .line 103
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 114
    :cond_3
    new-array v0, v3, [Ljava/lang/String;

    .line 115
    .local v0, "arr":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 116
    .local v5, "startId":I
    const/4 v2, 0x0

    .line 117
    .local v2, "jj":I
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_0

    .line 118
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v6, v7, :cond_4

    .line 119
    invoke-virtual {p1, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v2

    .line 120
    add-int/lit8 v2, v2, 0x1

    .line 121
    add-int/lit8 v5, v1, 0x1

    .line 117
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized addStroke([F[F)V
    .locals 1
    .param p1, "x"    # [F
    .param p2, "y"    # [F

    .prologue
    .line 89
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 90
    iget-object v0, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    monitor-exit p0

    return-void

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized clearStrokes()V
    .locals 1

    .prologue
    .line 94
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 95
    iget-object v0, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    monitor-exit p0

    return-void

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 34
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mbInitialized:Z

    .line 35
    invoke-virtual {p0}, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->VIEQ_Close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    monitor-exit p0

    return-void

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public init(Ljava/lang/String;)I
    .locals 4
    .param p1, "dataPath"    # Ljava/lang/String;

    .prologue
    .line 14
    const v1, 0x4b000

    const/16 v2, 0x640

    const/16 v3, 0x4b0

    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->VIEQ_Init(Ljava/lang/String;III)I

    move-result v0

    .line 15
    .local v0, "ret":I
    if-nez v0, :cond_0

    .line 16
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mbInitialized:Z

    .line 20
    :goto_0
    return v0

    .line 18
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mbInitialized:Z

    goto :goto_0
.end method

.method public init(Ljava/lang/String;III)I
    .locals 7
    .param p1, "dataPath"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "dpi"    # I

    .prologue
    .line 24
    const v2, 0x4b000

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->VIEQ_InitWithScreenInfo(Ljava/lang/String;IIII)I

    move-result v6

    .line 25
    .local v6, "ret":I
    if-nez v6, :cond_0

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mbInitialized:Z

    .line 30
    :goto_0
    return v6

    .line 28
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mbInitialized:Z

    goto :goto_0
.end method

.method public declared-synchronized recog()Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v11, 0x0

    .line 39
    monitor-enter p0

    :try_start_0
    iget-object v10, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->size()I

    move-result v6

    .line 40
    .local v6, "nStrokeSize":I
    const/4 v7, 0x0

    .line 41
    .local v7, "nTotalPointSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v6, :cond_0

    .line 51
    add-int/lit8 v7, v7, 0x1

    .line 53
    const/4 v3, 0x0

    .line 54
    .local v3, "nPointIndex":I
    mul-int/lit8 v10, v7, 0x2

    new-array v8, v10, [I

    .line 55
    .local v8, "pPointData":[I
    const/4 v1, 0x0

    move v4, v3

    .end local v3    # "nPointIndex":I
    .local v4, "nPointIndex":I
    :goto_1
    if-lt v1, v6, :cond_1

    .line 64
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "nPointIndex":I
    .restart local v3    # "nPointIndex":I
    const v10, 0xffff

    aput v10, v8, v4

    .line 65
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "nPointIndex":I
    .restart local v4    # "nPointIndex":I
    const v10, 0xffff

    aput v10, v8, v3

    .line 67
    iget-object v10, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->clear()V

    .line 68
    iget-object v10, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->clear()V

    .line 70
    invoke-virtual {p0, v8, v7}, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->VIEQ_Recog([II)Ljava/lang/String;

    move-result-object v9

    .line 71
    .local v9, "result":Ljava/lang/String;
    if-nez v9, :cond_3

    .line 72
    sget-object v10, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->TAG:Ljava/lang/String;

    const-string v12, "Output result is null!"

    invoke-static {v10, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v10, v11

    .line 85
    :goto_2
    monitor-exit p0

    return-object v10

    .line 42
    .end local v4    # "nPointIndex":I
    .end local v8    # "pPointData":[I
    .end local v9    # "result":Ljava/lang/String;
    :cond_0
    :try_start_1
    iget-object v10, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [F

    array-length v5, v10

    .line 49
    .local v5, "nPointSize":I
    add-int/lit8 v10, v5, 0x1

    add-int/2addr v7, v10

    .line 41
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 56
    .end local v5    # "nPointSize":I
    .restart local v4    # "nPointIndex":I
    .restart local v8    # "pPointData":[I
    :cond_1
    iget-object v10, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [F

    array-length v5, v10

    .line 57
    .restart local v5    # "nPointSize":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_3
    if-lt v2, v5, :cond_2

    .line 61
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "nPointIndex":I
    .restart local v3    # "nPointIndex":I
    const v10, 0xffff

    aput v10, v8, v4

    .line 62
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "nPointIndex":I
    .restart local v4    # "nPointIndex":I
    const/4 v10, 0x0

    aput v10, v8, v3

    .line 55
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 58
    :cond_2
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "nPointIndex":I
    .restart local v3    # "nPointIndex":I
    iget-object v10, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mXstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [F

    aget v10, v10, v2

    float-to-int v10, v10

    aput v10, v8, v4

    .line 59
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "nPointIndex":I
    .restart local v4    # "nPointIndex":I
    iget-object v10, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mYstrokeList:Ljava/util/LinkedList;

    invoke-virtual {v10, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [F

    aget v10, v10, v2

    float-to-int v10, v10

    aput v10, v8, v3

    .line 57
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 76
    .end local v2    # "j":I
    .end local v5    # "nPointSize":I
    .restart local v9    # "result":Ljava/lang/String;
    :cond_3
    iget-boolean v10, p0, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->mbInitialized:Z

    if-nez v10, :cond_4

    move-object v10, v11

    .line 77
    goto :goto_2

    .line 80
    :cond_4
    invoke-direct {p0, v9}, Lcom/samsung/vip/engine/VIEquationRecognitionLib;->getRecogResultCandidate(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "candList":[Ljava/lang/String;
    if-eqz v0, :cond_5

    array-length v10, v0

    if-lez v10, :cond_5

    .line 82
    const/4 v10, 0x0

    aget-object v10, v0, v10

    goto :goto_2

    .line 85
    :cond_5
    const-string v10, ""
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 39
    .end local v0    # "candList":[Ljava/lang/String;
    .end local v1    # "i":I
    .end local v4    # "nPointIndex":I
    .end local v6    # "nStrokeSize":I
    .end local v7    # "nTotalPointSize":I
    .end local v8    # "pPointData":[I
    .end local v9    # "result":Ljava/lang/String;
    :catchall_0
    move-exception v10

    monitor-exit p0

    throw v10
.end method
