.class public Lcom/samsung/vip/engine/VIShapeGenerator;
.super Ljava/lang/Object;
.source "VIShapeGenerator.java"


# static fields
.field private static final PI:F = 3.1415927f

.field private static final STONE_COUNT_LARGE:I = 0x80

.field private static final STONE_COUNT_SMALL:I = 0x40

.field private static final STONE_COUNT_THRES:I = 0xbb8

.field private static final STONE_COUNT_XSMALL:I = 0x20

.field private static mShapeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/vip/engine/shape/GraphPrimitive;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/vip/engine/VIShapeGenerator;->mShapeList:Ljava/util/ArrayList;

    .line 37
    return-void
.end method

.method private CopyDataToArrow(Lcom/samsung/vip/engine/shape/PrimitiveArrow;[SI)V
    .locals 9
    .param p1, "arrow"    # Lcom/samsung/vip/engine/shape/PrimitiveArrow;
    .param p2, "primitiveData"    # [S
    .param p3, "idx"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 382
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    .line 384
    .local v5, "start":Landroid/graphics/Point;
    const/4 v6, 0x2

    new-array v4, v6, [S

    .line 385
    .local v4, "short2":[S
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .local v1, "idx":I
    aget-short v6, p2, p3

    aput-short v6, v4, v7

    .line 386
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "idx":I
    .restart local p3    # "idx":I
    aget-short v6, p2, v1

    aput-short v6, v4, v8

    .line 387
    invoke-direct {p0, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v6

    iput v6, v5, Landroid/graphics/Point;->x:I

    .line 388
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .restart local v1    # "idx":I
    aget-short v6, p2, p3

    aput-short v6, v4, v7

    .line 389
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "idx":I
    .restart local p3    # "idx":I
    aget-short v6, p2, v1

    aput-short v6, v4, v8

    .line 390
    invoke-direct {p0, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v6

    iput v6, v5, Landroid/graphics/Point;->y:I

    .line 392
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 394
    .local v0, "end":Landroid/graphics/Point;
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .restart local v1    # "idx":I
    aget-short v6, p2, p3

    aput-short v6, v4, v7

    .line 395
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "idx":I
    .restart local p3    # "idx":I
    aget-short v6, p2, v1

    aput-short v6, v4, v8

    .line 396
    invoke-direct {p0, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v6

    iput v6, v0, Landroid/graphics/Point;->x:I

    .line 397
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .restart local v1    # "idx":I
    aget-short v6, p2, p3

    aput-short v6, v4, v7

    .line 398
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "idx":I
    .restart local p3    # "idx":I
    aget-short v6, p2, v1

    aput-short v6, v4, v8

    .line 399
    invoke-direct {p0, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v6

    iput v6, v0, Landroid/graphics/Point;->y:I

    .line 401
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 403
    .local v2, "left":Landroid/graphics/Point;
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .restart local v1    # "idx":I
    aget-short v6, p2, p3

    aput-short v6, v4, v7

    .line 404
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "idx":I
    .restart local p3    # "idx":I
    aget-short v6, p2, v1

    aput-short v6, v4, v8

    .line 405
    invoke-direct {p0, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v6

    iput v6, v2, Landroid/graphics/Point;->x:I

    .line 406
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .restart local v1    # "idx":I
    aget-short v6, p2, p3

    aput-short v6, v4, v7

    .line 407
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "idx":I
    .restart local p3    # "idx":I
    aget-short v6, p2, v1

    aput-short v6, v4, v8

    .line 408
    invoke-direct {p0, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v6

    iput v6, v2, Landroid/graphics/Point;->y:I

    .line 410
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 412
    .local v3, "right":Landroid/graphics/Point;
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .restart local v1    # "idx":I
    aget-short v6, p2, p3

    aput-short v6, v4, v7

    .line 413
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "idx":I
    .restart local p3    # "idx":I
    aget-short v6, p2, v1

    aput-short v6, v4, v8

    .line 414
    invoke-direct {p0, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v6

    iput v6, v3, Landroid/graphics/Point;->x:I

    .line 415
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .restart local v1    # "idx":I
    aget-short v6, p2, p3

    aput-short v6, v4, v7

    .line 416
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "idx":I
    .restart local p3    # "idx":I
    aget-short v6, p2, v1

    aput-short v6, v4, v8

    .line 417
    invoke-direct {p0, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v6

    iput v6, v3, Landroid/graphics/Point;->y:I

    .line 419
    invoke-virtual {p1, v5}, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->setStart(Landroid/graphics/Point;)V

    .line 420
    invoke-virtual {p1, v0}, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->setEnd(Landroid/graphics/Point;)V

    .line 421
    invoke-virtual {p1, v2}, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->setLeft(Landroid/graphics/Point;)V

    .line 422
    invoke-virtual {p1, v3}, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->setRight(Landroid/graphics/Point;)V

    .line 423
    return-void
.end method

.method private CopyDataToBezier(Lcom/samsung/vip/engine/shape/PrimitiveBezierList;[SI)V
    .locals 10
    .param p1, "bezierList"    # Lcom/samsung/vip/engine/shape/PrimitiveBezierList;
    .param p2, "primitiveData"    # [S
    .param p3, "idx"    # I

    .prologue
    .line 429
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 430
    .local v1, "bezierLists":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/vip/engine/shape/PrimitiveBezier;>;"
    aget-short v2, p2, p3

    .line 431
    .local v2, "bezierNum":I
    add-int/lit8 p3, p3, 0x2

    .line 432
    const/4 v7, 0x0

    .local v7, "i":I
    move v8, p3

    .end local p3    # "idx":I
    .local v8, "idx":I
    :goto_0
    if-lt v7, v2, :cond_0

    .line 453
    invoke-virtual {p1, v1}, Lcom/samsung/vip/engine/shape/PrimitiveBezierList;->setBezierList(Ljava/util/List;)V

    .line 454
    return-void

    .line 434
    :cond_0
    new-instance v0, Lcom/samsung/vip/engine/shape/PrimitiveBezier;

    invoke-direct {v0}, Lcom/samsung/vip/engine/shape/PrimitiveBezier;-><init>()V

    .line 435
    .local v0, "bezier":Lcom/samsung/vip/engine/shape/PrimitiveBezier;
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 436
    .local v3, "control01":Landroid/graphics/Point;
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 437
    .local v4, "control02":Landroid/graphics/Point;
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    .line 438
    .local v5, "control03":Landroid/graphics/Point;
    new-instance v6, Landroid/graphics/Point;

    invoke-direct {v6}, Landroid/graphics/Point;-><init>()V

    .line 439
    .local v6, "control04":Landroid/graphics/Point;
    add-int/lit8 p3, v8, 0x1

    .end local v8    # "idx":I
    .restart local p3    # "idx":I
    aget-short v9, p2, v8

    iput v9, v3, Landroid/graphics/Point;->x:I

    .line 440
    add-int/lit8 v8, p3, 0x1

    .end local p3    # "idx":I
    .restart local v8    # "idx":I
    aget-short v9, p2, p3

    iput v9, v3, Landroid/graphics/Point;->y:I

    .line 441
    add-int/lit8 p3, v8, 0x1

    .end local v8    # "idx":I
    .restart local p3    # "idx":I
    aget-short v9, p2, v8

    iput v9, v4, Landroid/graphics/Point;->x:I

    .line 442
    add-int/lit8 v8, p3, 0x1

    .end local p3    # "idx":I
    .restart local v8    # "idx":I
    aget-short v9, p2, p3

    iput v9, v4, Landroid/graphics/Point;->y:I

    .line 443
    add-int/lit8 p3, v8, 0x1

    .end local v8    # "idx":I
    .restart local p3    # "idx":I
    aget-short v9, p2, v8

    iput v9, v5, Landroid/graphics/Point;->x:I

    .line 444
    add-int/lit8 v8, p3, 0x1

    .end local p3    # "idx":I
    .restart local v8    # "idx":I
    aget-short v9, p2, p3

    iput v9, v5, Landroid/graphics/Point;->y:I

    .line 445
    add-int/lit8 p3, v8, 0x1

    .end local v8    # "idx":I
    .restart local p3    # "idx":I
    aget-short v9, p2, v8

    iput v9, v6, Landroid/graphics/Point;->x:I

    .line 446
    add-int/lit8 v8, p3, 0x1

    .end local p3    # "idx":I
    .restart local v8    # "idx":I
    aget-short v9, p2, p3

    iput v9, v6, Landroid/graphics/Point;->y:I

    .line 447
    invoke-virtual {v0, v3}, Lcom/samsung/vip/engine/shape/PrimitiveBezier;->setControl01(Landroid/graphics/Point;)V

    .line 448
    invoke-virtual {v0, v4}, Lcom/samsung/vip/engine/shape/PrimitiveBezier;->setControl02(Landroid/graphics/Point;)V

    .line 449
    invoke-virtual {v0, v5}, Lcom/samsung/vip/engine/shape/PrimitiveBezier;->setControl03(Landroid/graphics/Point;)V

    .line 450
    invoke-virtual {v0, v6}, Lcom/samsung/vip/engine/shape/PrimitiveBezier;->setControl04(Landroid/graphics/Point;)V

    .line 451
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 432
    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method private CopyDataToCircle(Lcom/samsung/vip/engine/shape/PrimitiveCircle;[SI)V
    .locals 6
    .param p1, "circle"    # Lcom/samsung/vip/engine/shape/PrimitiveCircle;
    .param p2, "PrimitiveData"    # [S
    .param p3, "idx"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 203
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 205
    .local v0, "center":Landroid/graphics/Point;
    const/4 v3, 0x2

    new-array v2, v3, [S

    .line 206
    .local v2, "short2":[S
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .local v1, "idx":I
    aget-short v3, p2, p3

    aput-short v3, v2, v4

    .line 207
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "idx":I
    .restart local p3    # "idx":I
    aget-short v3, p2, v1

    aput-short v3, v2, v5

    .line 208
    invoke-direct {p0, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v3

    iput v3, v0, Landroid/graphics/Point;->x:I

    .line 210
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .restart local v1    # "idx":I
    aget-short v3, p2, p3

    aput-short v3, v2, v4

    .line 211
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "idx":I
    .restart local p3    # "idx":I
    aget-short v3, p2, v1

    aput-short v3, v2, v5

    .line 212
    invoke-direct {p0, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v3

    iput v3, v0, Landroid/graphics/Point;->y:I

    .line 214
    invoke-virtual {p1, v0}, Lcom/samsung/vip/engine/shape/PrimitiveCircle;->setCenter(Landroid/graphics/Point;)V

    .line 215
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .restart local v1    # "idx":I
    aget-short v3, p2, p3

    invoke-virtual {p1, v3}, Lcom/samsung/vip/engine/shape/PrimitiveCircle;->setRadius(I)V

    .line 216
    return-void
.end method

.method private CopyDataToCircleArc(Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;[SI)V
    .locals 9
    .param p1, "circleArc"    # Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;
    .param p2, "PrimitiveData"    # [S
    .param p3, "idx"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 258
    aget-short v0, p2, v7

    .line 259
    .local v0, "GeomSize":I
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 260
    .local v1, "center":Landroid/graphics/Point;
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    .line 261
    .local v5, "start":Landroid/graphics/Point;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 263
    .local v2, "end":Landroid/graphics/Point;
    const/4 v6, 0x2

    new-array v4, v6, [S

    .line 264
    .local v4, "short2":[S
    add-int/lit8 v3, p3, 0x1

    .end local p3    # "idx":I
    .local v3, "idx":I
    aget-short v6, p2, p3

    aput-short v6, v4, v8

    .line 265
    add-int/lit8 p3, v3, 0x1

    .end local v3    # "idx":I
    .restart local p3    # "idx":I
    aget-short v6, p2, v3

    aput-short v6, v4, v7

    .line 266
    invoke-direct {p0, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v6

    iput v6, v1, Landroid/graphics/Point;->x:I

    .line 268
    add-int/lit8 v3, p3, 0x1

    .end local p3    # "idx":I
    .restart local v3    # "idx":I
    aget-short v6, p2, p3

    aput-short v6, v4, v8

    .line 269
    add-int/lit8 p3, v3, 0x1

    .end local v3    # "idx":I
    .restart local p3    # "idx":I
    aget-short v6, p2, v3

    aput-short v6, v4, v7

    .line 270
    invoke-direct {p0, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v6

    iput v6, v1, Landroid/graphics/Point;->y:I

    .line 272
    invoke-virtual {p1, v1}, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->setCenter(Landroid/graphics/Point;)V

    .line 274
    add-int/lit8 v3, p3, 0x1

    .end local p3    # "idx":I
    .restart local v3    # "idx":I
    aget-short v6, p2, p3

    aput-short v6, v4, v8

    .line 275
    add-int/lit8 p3, v3, 0x1

    .end local v3    # "idx":I
    .restart local p3    # "idx":I
    aget-short v6, p2, v3

    aput-short v6, v4, v7

    .line 276
    invoke-direct {p0, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v6

    iput v6, v5, Landroid/graphics/Point;->x:I

    .line 278
    add-int/lit8 v3, p3, 0x1

    .end local p3    # "idx":I
    .restart local v3    # "idx":I
    aget-short v6, p2, p3

    aput-short v6, v4, v8

    .line 279
    add-int/lit8 p3, v3, 0x1

    .end local v3    # "idx":I
    .restart local p3    # "idx":I
    aget-short v6, p2, v3

    aput-short v6, v4, v7

    .line 280
    invoke-direct {p0, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v6

    iput v6, v5, Landroid/graphics/Point;->y:I

    .line 282
    invoke-virtual {p1, v5}, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->setStart(Landroid/graphics/Point;)V

    .line 284
    add-int/lit8 v3, p3, 0x1

    .end local p3    # "idx":I
    .restart local v3    # "idx":I
    aget-short v6, p2, p3

    aput-short v6, v4, v8

    .line 285
    add-int/lit8 p3, v3, 0x1

    .end local v3    # "idx":I
    .restart local p3    # "idx":I
    aget-short v6, p2, v3

    aput-short v6, v4, v7

    .line 286
    invoke-direct {p0, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v6

    iput v6, v2, Landroid/graphics/Point;->x:I

    .line 288
    add-int/lit8 v3, p3, 0x1

    .end local p3    # "idx":I
    .restart local v3    # "idx":I
    aget-short v6, p2, p3

    aput-short v6, v4, v8

    .line 289
    add-int/lit8 p3, v3, 0x1

    .end local v3    # "idx":I
    .restart local p3    # "idx":I
    aget-short v6, p2, v3

    aput-short v6, v4, v7

    .line 290
    invoke-direct {p0, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v6

    iput v6, v2, Landroid/graphics/Point;->y:I

    .line 292
    invoke-virtual {p1, v2}, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->setEnd(Landroid/graphics/Point;)V

    .line 294
    add-int/lit8 v3, p3, 0x1

    .end local p3    # "idx":I
    .restart local v3    # "idx":I
    aget-short v6, p2, p3

    invoke-virtual {p1, v6}, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->setRadius(I)V

    .line 295
    add-int/lit8 p3, v3, 0x1

    .end local v3    # "idx":I
    .restart local p3    # "idx":I
    aget-short v6, p2, v3

    invoke-virtual {p1, v6}, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->setDirection(I)V

    .line 296
    return-void
.end method

.method private CopyDataToEllipse(Lcom/samsung/vip/engine/shape/PrimitiveEllipse;[SI)V
    .locals 6
    .param p1, "ellipse"    # Lcom/samsung/vip/engine/shape/PrimitiveEllipse;
    .param p2, "PrimitiveData"    # [S
    .param p3, "idx"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 225
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 227
    .local v0, "center":Landroid/graphics/Point;
    const/4 v3, 0x2

    new-array v2, v3, [S

    .line 228
    .local v2, "short2":[S
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .local v1, "idx":I
    aget-short v3, p2, p3

    aput-short v3, v2, v4

    .line 229
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "idx":I
    .restart local p3    # "idx":I
    aget-short v3, p2, v1

    aput-short v3, v2, v5

    .line 230
    invoke-direct {p0, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v3

    iput v3, v0, Landroid/graphics/Point;->x:I

    .line 232
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .restart local v1    # "idx":I
    aget-short v3, p2, p3

    aput-short v3, v2, v4

    .line 233
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "idx":I
    .restart local p3    # "idx":I
    aget-short v3, p2, v1

    aput-short v3, v2, v5

    .line 234
    invoke-direct {p0, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v3

    iput v3, v0, Landroid/graphics/Point;->y:I

    .line 236
    invoke-virtual {p1, v0}, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->setCenter(Landroid/graphics/Point;)V

    .line 238
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .restart local v1    # "idx":I
    aget-short v3, p2, p3

    invoke-virtual {p1, v3}, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->setLongAxis(I)V

    .line 239
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "idx":I
    .restart local p3    # "idx":I
    aget-short v3, p2, v1

    invoke-virtual {p1, v3}, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->setShortAxis(I)V

    .line 241
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .restart local v1    # "idx":I
    aget-short v3, p2, p3

    aput-short v3, v2, v4

    .line 242
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "idx":I
    .restart local p3    # "idx":I
    aget-short v3, p2, v1

    aput-short v3, v2, v5

    .line 243
    invoke-direct {p0, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->getFloat([S)F

    move-result v3

    invoke-virtual {p1, v3}, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->setCosTheta(F)V

    .line 245
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .restart local v1    # "idx":I
    aget-short v3, p2, p3

    aput-short v3, v2, v4

    .line 246
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "idx":I
    .restart local p3    # "idx":I
    aget-short v3, p2, v1

    aput-short v3, v2, v5

    .line 247
    invoke-direct {p0, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->getFloat([S)F

    move-result v3

    invoke-virtual {p1, v3}, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->setSinTheta(F)V

    .line 248
    return-void
.end method

.method private CopyDataToEllipseArc(Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;[SI)V
    .locals 8
    .param p1, "ellipseArc"    # Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;
    .param p2, "primitiveData"    # [S
    .param p3, "idx"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 302
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 303
    .local v0, "center":Landroid/graphics/Point;
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 304
    .local v4, "start":Landroid/graphics/Point;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 306
    .local v1, "end":Landroid/graphics/Point;
    const/4 v5, 0x2

    new-array v3, v5, [S

    .line 307
    .local v3, "short2":[S
    add-int/lit8 v2, p3, 0x1

    .end local p3    # "idx":I
    .local v2, "idx":I
    aget-short v5, p2, p3

    aput-short v5, v3, v6

    .line 308
    add-int/lit8 p3, v2, 0x1

    .end local v2    # "idx":I
    .restart local p3    # "idx":I
    aget-short v5, p2, v2

    aput-short v5, v3, v7

    .line 309
    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v5

    iput v5, v0, Landroid/graphics/Point;->x:I

    .line 311
    add-int/lit8 v2, p3, 0x1

    .end local p3    # "idx":I
    .restart local v2    # "idx":I
    aget-short v5, p2, p3

    aput-short v5, v3, v6

    .line 312
    add-int/lit8 p3, v2, 0x1

    .end local v2    # "idx":I
    .restart local p3    # "idx":I
    aget-short v5, p2, v2

    aput-short v5, v3, v7

    .line 313
    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v5

    iput v5, v0, Landroid/graphics/Point;->y:I

    .line 315
    invoke-virtual {p1, v0}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->setCenter(Landroid/graphics/Point;)V

    .line 317
    add-int/lit8 v2, p3, 0x1

    .end local p3    # "idx":I
    .restart local v2    # "idx":I
    aget-short v5, p2, p3

    aput-short v5, v3, v6

    .line 318
    add-int/lit8 p3, v2, 0x1

    .end local v2    # "idx":I
    .restart local p3    # "idx":I
    aget-short v5, p2, v2

    aput-short v5, v3, v7

    .line 319
    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v5

    iput v5, v4, Landroid/graphics/Point;->x:I

    .line 320
    add-int/lit8 v2, p3, 0x1

    .end local p3    # "idx":I
    .restart local v2    # "idx":I
    aget-short v5, p2, p3

    aput-short v5, v3, v6

    .line 321
    add-int/lit8 p3, v2, 0x1

    .end local v2    # "idx":I
    .restart local p3    # "idx":I
    aget-short v5, p2, v2

    aput-short v5, v3, v7

    .line 322
    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v5

    iput v5, v4, Landroid/graphics/Point;->y:I

    .line 324
    invoke-virtual {p1, v4}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->setStart(Landroid/graphics/Point;)V

    .line 326
    add-int/lit8 v2, p3, 0x1

    .end local p3    # "idx":I
    .restart local v2    # "idx":I
    aget-short v5, p2, p3

    aput-short v5, v3, v6

    .line 327
    add-int/lit8 p3, v2, 0x1

    .end local v2    # "idx":I
    .restart local p3    # "idx":I
    aget-short v5, p2, v2

    aput-short v5, v3, v7

    .line 328
    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v5

    iput v5, v1, Landroid/graphics/Point;->x:I

    .line 329
    add-int/lit8 v2, p3, 0x1

    .end local p3    # "idx":I
    .restart local v2    # "idx":I
    aget-short v5, p2, p3

    aput-short v5, v3, v6

    .line 330
    add-int/lit8 p3, v2, 0x1

    .end local v2    # "idx":I
    .restart local p3    # "idx":I
    aget-short v5, p2, v2

    aput-short v5, v3, v7

    .line 331
    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v5

    iput v5, v1, Landroid/graphics/Point;->y:I

    .line 332
    invoke-virtual {p1, v1}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->setEnd(Landroid/graphics/Point;)V

    .line 334
    add-int/lit8 v2, p3, 0x1

    .end local p3    # "idx":I
    .restart local v2    # "idx":I
    aget-short v5, p2, p3

    invoke-virtual {p1, v5}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->setLongAxis(I)V

    .line 335
    add-int/lit8 p3, v2, 0x1

    .end local v2    # "idx":I
    .restart local p3    # "idx":I
    aget-short v5, p2, v2

    invoke-virtual {p1, v5}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->setShortAxis(I)V

    .line 337
    add-int/lit8 v2, p3, 0x1

    .end local p3    # "idx":I
    .restart local v2    # "idx":I
    aget-short v5, p2, p3

    aput-short v5, v3, v6

    .line 338
    add-int/lit8 p3, v2, 0x1

    .end local v2    # "idx":I
    .restart local p3    # "idx":I
    aget-short v5, p2, v2

    aput-short v5, v3, v7

    .line 339
    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/VIShapeGenerator;->getFloat([S)F

    move-result v5

    invoke-virtual {p1, v5}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->setCosTheta(F)V

    .line 341
    add-int/lit8 v2, p3, 0x1

    .end local p3    # "idx":I
    .restart local v2    # "idx":I
    aget-short v5, p2, p3

    aput-short v5, v3, v6

    .line 342
    add-int/lit8 p3, v2, 0x1

    .end local v2    # "idx":I
    .restart local p3    # "idx":I
    aget-short v5, p2, v2

    aput-short v5, v3, v7

    .line 343
    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/VIShapeGenerator;->getFloat([S)F

    move-result v5

    invoke-virtual {p1, v5}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->setSinTheta(F)V

    .line 345
    add-int/lit8 v2, p3, 0x1

    .end local p3    # "idx":I
    .restart local v2    # "idx":I
    aget-short v5, p2, p3

    invoke-virtual {p1, v5}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->setDirection(I)V

    .line 346
    return-void
.end method

.method private CopyDataToLine(Lcom/samsung/vip/engine/shape/PrimitiveLine;[SI)V
    .locals 7
    .param p1, "line"    # Lcom/samsung/vip/engine/shape/PrimitiveLine;
    .param p2, "PrimitiveData"    # [S
    .param p3, "idx"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 172
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 173
    .local v3, "start":Landroid/graphics/Point;
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 175
    .local v0, "end":Landroid/graphics/Point;
    const/4 v4, 0x2

    new-array v2, v4, [S

    .line 176
    .local v2, "short2":[S
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .local v1, "idx":I
    aget-short v4, p2, p3

    aput-short v4, v2, v5

    .line 177
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "idx":I
    .restart local p3    # "idx":I
    aget-short v4, p2, v1

    aput-short v4, v2, v6

    .line 178
    invoke-direct {p0, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v4

    iput v4, v3, Landroid/graphics/Point;->x:I

    .line 180
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .restart local v1    # "idx":I
    aget-short v4, p2, p3

    aput-short v4, v2, v5

    .line 181
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "idx":I
    .restart local p3    # "idx":I
    aget-short v4, p2, v1

    aput-short v4, v2, v6

    .line 182
    invoke-direct {p0, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v4

    iput v4, v3, Landroid/graphics/Point;->y:I

    .line 184
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .restart local v1    # "idx":I
    aget-short v4, p2, p3

    aput-short v4, v2, v5

    .line 185
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "idx":I
    .restart local p3    # "idx":I
    aget-short v4, p2, v1

    aput-short v4, v2, v6

    .line 186
    invoke-direct {p0, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v4

    iput v4, v0, Landroid/graphics/Point;->x:I

    .line 188
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "idx":I
    .restart local v1    # "idx":I
    aget-short v4, p2, p3

    aput-short v4, v2, v5

    .line 189
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "idx":I
    .restart local p3    # "idx":I
    aget-short v4, p2, v1

    aput-short v4, v2, v6

    .line 190
    invoke-direct {p0, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->getInt([S)I

    move-result v4

    iput v4, v0, Landroid/graphics/Point;->y:I

    .line 192
    invoke-virtual {p1, v3}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->setStart(Landroid/graphics/Point;)V

    .line 193
    invoke-virtual {p1, v0}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->setEnd(Landroid/graphics/Point;)V

    .line 194
    return-void
.end method

.method private CopyDataToPolygon(Lcom/samsung/vip/engine/shape/PrimitivePolygon;[SI)V
    .locals 6
    .param p1, "polygon"    # Lcom/samsung/vip/engine/shape/PrimitivePolygon;
    .param p2, "primitiveData"    # [S
    .param p3, "idx"    # I

    .prologue
    .line 352
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 353
    .local v3, "points":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Point;>;"
    aget-short v1, p2, p3

    .line 354
    .local v1, "number":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 360
    invoke-virtual {p1, v3}, Lcom/samsung/vip/engine/shape/PrimitivePolygon;->setPoints(Ljava/util/ArrayList;)V

    .line 361
    return-void

    .line 355
    :cond_0
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 356
    .local v2, "point":Landroid/graphics/Point;
    add-int/lit8 v4, p3, 0x2

    mul-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x0

    aget-short v4, p2, v4

    iput v4, v2, Landroid/graphics/Point;->x:I

    .line 357
    add-int/lit8 v4, p3, 0x2

    mul-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x1

    aget-short v4, p2, v4

    iput v4, v2, Landroid/graphics/Point;->y:I

    .line 358
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 354
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private CopyDataToPolyline(Lcom/samsung/vip/engine/shape/PrimitivePolyline;[SI)V
    .locals 6
    .param p1, "polyline"    # Lcom/samsung/vip/engine/shape/PrimitivePolyline;
    .param p2, "primitiveData"    # [S
    .param p3, "idx"    # I

    .prologue
    .line 367
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 368
    .local v3, "points":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Point;>;"
    aget-short v1, p2, p3

    .line 369
    .local v1, "number":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 375
    invoke-virtual {p1, v3}, Lcom/samsung/vip/engine/shape/PrimitivePolyline;->setPoints(Ljava/util/ArrayList;)V

    .line 376
    return-void

    .line 370
    :cond_0
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 371
    .local v2, "point":Landroid/graphics/Point;
    add-int/lit8 v4, p3, 0x2

    mul-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x0

    aget-short v4, p2, v4

    iput v4, v2, Landroid/graphics/Point;->x:I

    .line 372
    add-int/lit8 v4, p3, 0x2

    mul-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x1

    aget-short v4, p2, v4

    iput v4, v2, Landroid/graphics/Point;->y:I

    .line 373
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 369
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private POINT_2_POINT_F(Landroid/graphics/Point;)Landroid/graphics/PointF;
    .locals 2
    .param p1, "p"    # Landroid/graphics/Point;

    .prologue
    .line 577
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 578
    .local v0, "pf":Landroid/graphics/PointF;
    iget v1, p1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 579
    iget v1, p1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 580
    return-object v0
.end method

.method private Trans_PointF(Landroid/graphics/PointF;Landroid/graphics/Point;FFI)Landroid/graphics/PointF;
    .locals 4
    .param p1, "mypoint"    # Landroid/graphics/PointF;
    .param p2, "Center"    # Landroid/graphics/Point;
    .param p3, "cos_theta"    # F
    .param p4, "sin_theta"    # F
    .param p5, "trans_direction"    # I

    .prologue
    .line 564
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 565
    .local v0, "trans_point":Landroid/graphics/PointF;
    const/4 v1, 0x1

    if-ne p5, v1, :cond_0

    .line 566
    iget v1, p1, Landroid/graphics/PointF;->x:F

    mul-float/2addr v1, p3

    iget v2, p1, Landroid/graphics/PointF;->y:F

    mul-float/2addr v2, p4

    sub-float/2addr v1, v2

    iget v2, p2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 567
    iget v1, p1, Landroid/graphics/PointF;->x:F

    mul-float/2addr v1, p4

    iget v2, p1, Landroid/graphics/PointF;->y:F

    mul-float/2addr v2, p3

    add-float/2addr v1, v2

    iget v2, p2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 573
    :goto_0
    return-object v0

    .line 570
    :cond_0
    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    mul-float/2addr v1, p3

    iget v2, p1, Landroid/graphics/PointF;->y:F

    iget v3, p2, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    mul-float/2addr v2, p4

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 571
    neg-float v1, p4

    iget v2, p1, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    iget v2, p1, Landroid/graphics/PointF;->y:F

    iget v3, p2, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    mul-float/2addr v2, p3

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0
.end method

.method private getAngle(FF)F
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const v1, 0x40490fdb    # (float)Math.PI

    const/4 v0, 0x0

    .line 139
    cmpl-float v2, p1, v0

    if-nez v2, :cond_3

    .line 140
    cmpl-float v1, p2, v0

    if-nez v1, :cond_1

    .line 159
    :cond_0
    :goto_0
    return v0

    .line 142
    :cond_1
    cmpl-float v0, p2, v0

    if-lez v0, :cond_2

    .line 143
    const v0, 0x3fc90fdb

    goto :goto_0

    .line 145
    :cond_2
    const v0, 0x4096cbe4

    goto :goto_0

    .line 147
    :cond_3
    cmpl-float v2, p1, v0

    if-lez v2, :cond_5

    .line 148
    cmpl-float v1, p2, v0

    if-eqz v1, :cond_0

    .line 150
    cmpl-float v0, p2, v0

    if-lez v0, :cond_4

    .line 151
    div-float v0, p2, p1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->atan(D)D

    move-result-wide v0

    double-to-float v0, v0

    goto :goto_0

    .line 153
    :cond_4
    const v0, 0x40c90fdb

    div-float v1, p2, p1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    double-to-float v1, v2

    add-float/2addr v0, v1

    goto :goto_0

    .line 156
    :cond_5
    cmpl-float v0, p2, v0

    if-nez v0, :cond_6

    move v0, v1

    .line 157
    goto :goto_0

    .line 159
    :cond_6
    div-float v0, p2, p1

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    double-to-float v0, v2

    add-float/2addr v0, v1

    goto :goto_0
.end method

.method private getBezierPoint(FLandroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 8
    .param p1, "t"    # F
    .param p2, "ctrlPt1"    # Landroid/graphics/PointF;
    .param p3, "ctrlPt2"    # Landroid/graphics/PointF;
    .param p4, "ctrlPt3"    # Landroid/graphics/PointF;
    .param p5, "ctrlPt4"    # Landroid/graphics/PointF;

    .prologue
    .line 867
    iget v2, p2, Landroid/graphics/PointF;->x:F

    iget v3, p3, Landroid/graphics/PointF;->x:F

    iget v4, p4, Landroid/graphics/PointF;->x:F

    iget v5, p5, Landroid/graphics/PointF;->x:F

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/vip/engine/VIShapeGenerator;->getBezierValue(FFFFF)F

    move-result v6

    .line 868
    .local v6, "x":F
    iget v2, p2, Landroid/graphics/PointF;->y:F

    iget v3, p3, Landroid/graphics/PointF;->y:F

    iget v4, p4, Landroid/graphics/PointF;->y:F

    iget v5, p5, Landroid/graphics/PointF;->y:F

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/vip/engine/VIShapeGenerator;->getBezierValue(FFFFF)F

    move-result v7

    .line 869
    .local v7, "y":F
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v6, v7}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method

.method private getBezierValue(FFFFF)F
    .locals 8
    .param p1, "t"    # F
    .param p2, "v1"    # F
    .param p3, "v2"    # F
    .param p4, "v3"    # F
    .param p5, "v4"    # F

    .prologue
    .line 873
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p1

    float-to-double v0, v0

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    float-to-double v2, p2

    mul-double/2addr v0, v2

    const/high16 v2, 0x40400000    # 3.0f

    mul-float/2addr v2, p1

    float-to-double v2, v2

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v4, p1

    float-to-double v4, v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v2, v4

    float-to-double v4, p3

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    float-to-double v4, p1

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v2, v4

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v4, p1

    float-to-double v4, v4

    mul-double/2addr v2, v4

    float-to-double v4, p4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    float-to-double v2, p1

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    float-to-double v4, p5

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private getFloat([S)F
    .locals 3
    .param p1, "s"    # [S

    .prologue
    .line 125
    const/4 v0, 0x0

    .line 126
    .local v0, "i":I
    const/4 v1, 0x0

    aget-short v1, p1, v1

    const v2, 0xffff

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    .line 127
    const/4 v1, 0x1

    aget-short v1, p1, v1

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    .line 128
    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v1

    return v1
.end method

.method private getInt([S)I
    .locals 3
    .param p1, "s"    # [S

    .prologue
    .line 132
    const/4 v0, 0x0

    .line 133
    .local v0, "i":I
    const/4 v1, 0x0

    aget-short v1, p1, v1

    const v2, 0xffff

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    .line 134
    const/4 v1, 0x1

    aget-short v1, p1, v1

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    .line 135
    return v0
.end method

.method private get_rad_ellipse_f(Landroid/graphics/PointF;FF)F
    .locals 8
    .param p1, "mypoint"    # Landroid/graphics/PointF;
    .param p2, "longAxis"    # F
    .param p3, "shortAxis"    # F

    .prologue
    .line 586
    iget v1, p1, Landroid/graphics/PointF;->x:F

    .line 587
    .local v1, "x":F
    iget v2, p1, Landroid/graphics/PointF;->y:F

    .line 588
    .local v2, "y":F
    div-float/2addr v1, p2

    .line 589
    div-float/2addr v2, p3

    .line 590
    float-to-double v4, v2

    float-to-double v6, v1

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    double-to-float v0, v4

    .line 591
    .local v0, "len":F
    const/4 v3, 0x0

    cmpg-float v3, v0, v3

    if-gez v3, :cond_0

    .line 592
    float-to-double v4, v0

    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    add-double/2addr v4, v6

    double-to-float v0, v4

    .line 593
    :cond_0
    return v0
.end method

.method private makeArc(Ljava/util/ArrayList;Landroid/graphics/PointF;FFFZLandroid/graphics/PointF;Landroid/graphics/PointF;I)V
    .locals 7
    .param p2, "center"    # Landroid/graphics/PointF;
    .param p3, "minRadius"    # F
    .param p4, "maxRadius"    # F
    .param p5, "orientation"    # F
    .param p6, "bArc"    # Z
    .param p7, "startPt"    # Landroid/graphics/PointF;
    .param p8, "endPt"    # Landroid/graphics/PointF;
    .param p9, "direction"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/graphics/PointF;",
            ">;",
            "Landroid/graphics/PointF;",
            "FFFZ",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 598
    .local p1, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Landroid/graphics/PointF;>;"
    new-instance v2, Lcom/samsung/vip/engine/shape/PrimitiveArc;

    invoke-direct {v2}, Lcom/samsung/vip/engine/shape/PrimitiveArc;-><init>()V

    .line 600
    .local v2, "arc":Lcom/samsung/vip/engine/shape/PrimitiveArc;
    invoke-virtual {v2, p2}, Lcom/samsung/vip/engine/shape/PrimitiveArc;->setCenter(Landroid/graphics/PointF;)V

    .line 601
    invoke-virtual {v2, p3}, Lcom/samsung/vip/engine/shape/PrimitiveArc;->setMinRadius(F)V

    .line 602
    invoke-virtual {v2, p4}, Lcom/samsung/vip/engine/shape/PrimitiveArc;->setMaxRadius(F)V

    .line 603
    invoke-virtual {v2, p5}, Lcom/samsung/vip/engine/shape/PrimitiveArc;->setOrientation(F)V

    .line 605
    if-eqz p6, :cond_3

    .line 606
    iget v4, p7, Landroid/graphics/PointF;->x:F

    iget v5, p2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v4, v5

    iget v5, p7, Landroid/graphics/PointF;->y:F

    iget v6, p2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v5, v6

    invoke-direct {p0, v4, v5}, Lcom/samsung/vip/engine/VIShapeGenerator;->getAngle(FF)F

    move-result v0

    .line 607
    .local v0, "angle1":F
    iget v4, p8, Landroid/graphics/PointF;->x:F

    iget v5, p2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v4, v5

    iget v5, p8, Landroid/graphics/PointF;->y:F

    iget v6, p2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v5, v6

    invoke-direct {p0, v4, v5}, Lcom/samsung/vip/engine/VIShapeGenerator;->getAngle(FF)F

    move-result v1

    .line 608
    .local v1, "angle2":F
    const/4 v3, 0x0

    .line 609
    .local v3, "sweepAngle":F
    invoke-virtual {v2, v0}, Lcom/samsung/vip/engine/shape/PrimitiveArc;->setStartAngle(F)V

    .line 610
    if-lez p9, :cond_1

    .line 612
    cmpl-float v4, v0, v1

    if-lez v4, :cond_0

    .line 613
    const v4, 0x40c90fdb

    sub-float v5, v0, v1

    sub-float v3, v4, v5

    .line 625
    :goto_0
    invoke-virtual {v2, v3}, Lcom/samsung/vip/engine/shape/PrimitiveArc;->setSweepAngle(F)V

    .line 630
    .end local v0    # "angle1":F
    .end local v1    # "angle2":F
    .end local v3    # "sweepAngle":F
    :goto_1
    invoke-direct {p0, p1, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeArc(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveArc;)V

    .line 631
    return-void

    .line 615
    .restart local v0    # "angle1":F
    .restart local v1    # "angle2":F
    .restart local v3    # "sweepAngle":F
    :cond_0
    sub-float v3, v1, v0

    .line 617
    goto :goto_0

    .line 619
    :cond_1
    cmpl-float v4, v0, v1

    if-lez v4, :cond_2

    .line 620
    sub-float v3, v1, v0

    .line 621
    goto :goto_0

    .line 622
    :cond_2
    sub-float v4, v1, v0

    const v5, 0x40c90fdb

    sub-float v3, v4, v5

    goto :goto_0

    .line 627
    .end local v0    # "angle1":F
    .end local v1    # "angle2":F
    .end local v3    # "sweepAngle":F
    :cond_3
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/samsung/vip/engine/shape/PrimitiveArc;->setStartAngle(F)V

    .line 628
    const v4, 0x40c90fdb

    invoke-virtual {v2, v4}, Lcom/samsung/vip/engine/shape/PrimitiveArc;->setSweepAngle(F)V

    goto :goto_1
.end method

.method private makeArc(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveArc;)V
    .locals 26
    .param p2, "data"    # Lcom/samsung/vip/engine/shape/PrimitiveArc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/graphics/PointF;",
            ">;",
            "Lcom/samsung/vip/engine/shape/PrimitiveArc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 634
    .local p1, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Landroid/graphics/PointF;>;"
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveArc;->getCenter()Landroid/graphics/PointF;

    move-result-object v6

    .line 636
    .local v6, "center":Landroid/graphics/PointF;
    iget v7, v6, Landroid/graphics/PointF;->x:F

    .line 637
    .local v7, "cx":F
    iget v8, v6, Landroid/graphics/PointF;->y:F

    .line 639
    .local v8, "cy":F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveArc;->getMinRadius()F

    move-result v11

    .line 640
    .local v11, "minR":F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveArc;->getMaxRadius()F

    move-result v10

    .line 642
    .local v10, "maxR":F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveArc;->getOrientation()F

    move-result v12

    .line 643
    .local v12, "orientation":F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveArc;->getStartAngle()F

    move-result v14

    .line 644
    .local v14, "startAngle":F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveArc;->getSweepAngle()F

    move-result v17

    .line 646
    .local v17, "sweepAngle":F
    add-float v20, v11, v10

    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(F)F

    move-result v21

    mul-float v5, v20, v21

    .line 649
    .local v5, "approximateLength":F
    const v20, 0x453b8000    # 3000.0f

    cmpl-float v20, v5, v20

    if-lez v20, :cond_0

    .line 650
    const/16 v16, 0x80

    .line 656
    .local v16, "stoneCount":I
    :goto_0
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v15, v17, v20

    .line 657
    .local v15, "stone":F
    add-int/lit8 v20, v16, 0x1

    move/from16 v0, v20

    new-array v13, v0, [Landroid/graphics/PointF;

    .line 659
    .local v13, "points":[Landroid/graphics/PointF;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    add-int/lit8 v20, v16, 0x1

    move/from16 v0, v20

    if-lt v9, v0, :cond_1

    .line 667
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 668
    return-void

    .line 652
    .end local v9    # "i":I
    .end local v13    # "points":[Landroid/graphics/PointF;
    .end local v15    # "stone":F
    .end local v16    # "stoneCount":I
    :cond_0
    const/16 v16, 0x40

    .restart local v16    # "stoneCount":I
    goto :goto_0

    .line 660
    .restart local v9    # "i":I
    .restart local v13    # "points":[Landroid/graphics/PointF;
    .restart local v15    # "stone":F
    :cond_1
    int-to-float v0, v9

    move/from16 v20, v0

    mul-float v20, v20, v15

    add-float v4, v14, v20

    .line 662
    .local v4, "angle":F
    float-to-double v0, v7

    move-wide/from16 v20, v0

    float-to-double v0, v10

    move-wide/from16 v22, v0

    float-to-double v0, v4

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->cos(D)D

    move-result-wide v24

    mul-double v22, v22, v24

    float-to-double v0, v12

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->cos(D)D

    move-result-wide v24

    mul-double v22, v22, v24

    add-double v20, v20, v22

    float-to-double v0, v11

    move-wide/from16 v22, v0

    float-to-double v0, v4

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sin(D)D

    move-result-wide v24

    mul-double v22, v22, v24

    float-to-double v0, v12

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sin(D)D

    move-result-wide v24

    mul-double v22, v22, v24

    sub-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v18, v0

    .line 663
    .local v18, "x":F
    float-to-double v0, v8

    move-wide/from16 v20, v0

    float-to-double v0, v10

    move-wide/from16 v22, v0

    float-to-double v0, v4

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->cos(D)D

    move-result-wide v24

    mul-double v22, v22, v24

    float-to-double v0, v12

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sin(D)D

    move-result-wide v24

    mul-double v22, v22, v24

    add-double v20, v20, v22

    float-to-double v0, v11

    move-wide/from16 v22, v0

    float-to-double v0, v4

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sin(D)D

    move-result-wide v24

    mul-double v22, v22, v24

    float-to-double v0, v12

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->cos(D)D

    move-result-wide v24

    mul-double v22, v22, v24

    add-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v19, v0

    .line 665
    .local v19, "y":F
    new-instance v20, Landroid/graphics/PointF;

    move-object/from16 v0, v20

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v20, v13, v9

    .line 659
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1
.end method

.method private makeArrow(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveArrow;)V
    .locals 2
    .param p2, "data"    # Lcom/samsung/vip/engine/shape/PrimitiveArrow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/graphics/PointF;",
            ">;",
            "Lcom/samsung/vip/engine/shape/PrimitiveArrow;",
            ")V"
        }
    .end annotation

    .prologue
    .line 704
    .local p1, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Landroid/graphics/PointF;>;"
    new-instance v0, Lcom/samsung/vip/engine/shape/PrimitiveLine;

    invoke-direct {v0}, Lcom/samsung/vip/engine/shape/PrimitiveLine;-><init>()V

    .line 706
    .local v0, "line":Lcom/samsung/vip/engine/shape/PrimitiveLine;
    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->getStart()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->setStart(Landroid/graphics/Point;)V

    .line 707
    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->getEnd()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->setEnd(Landroid/graphics/Point;)V

    .line 708
    invoke-direct {p0, p1, v0}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeLine(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveLine;)V

    .line 710
    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->getEnd()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->setStart(Landroid/graphics/Point;)V

    .line 711
    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->getLeft()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->setEnd(Landroid/graphics/Point;)V

    .line 712
    invoke-direct {p0, p1, v0}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeLine(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveLine;)V

    .line 714
    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->getEnd()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->setStart(Landroid/graphics/Point;)V

    .line 715
    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->getRight()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->setEnd(Landroid/graphics/Point;)V

    .line 716
    invoke-direct {p0, p1, v0}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeLine(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveLine;)V

    .line 717
    return-void
.end method

.method private makeBezier(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveBezierList;)V
    .locals 17
    .param p2, "bezierList"    # Lcom/samsung/vip/engine/shape/PrimitiveBezierList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/graphics/PointF;",
            ">;",
            "Lcom/samsung/vip/engine/shape/PrimitiveBezierList;",
            ")V"
        }
    .end annotation

    .prologue
    .line 837
    .local p1, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Landroid/graphics/PointF;>;"
    const/16 v15, 0x20

    .line 838
    .local v15, "stoneCount":I
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveBezierList;->getBezierList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v10

    .line 839
    .local v10, "listSize":I
    const/4 v11, 0x0

    .line 840
    .local v11, "nIndex":I
    mul-int/lit8 v13, v10, 0x21

    .line 841
    .local v13, "nPointSize":I
    new-array v14, v13, [Landroid/graphics/PointF;

    .line 842
    .local v14, "points":[Landroid/graphics/PointF;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-lt v8, v10, :cond_0

    .line 863
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 864
    return-void

    .line 843
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveBezierList;->getBezierList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/vip/engine/shape/PrimitiveBezier;

    .line 844
    .local v7, "bezier":Lcom/samsung/vip/engine/shape/PrimitiveBezier;
    new-instance v3, Landroid/graphics/PointF;

    invoke-virtual {v7}, Lcom/samsung/vip/engine/shape/PrimitiveBezier;->getControl01()Landroid/graphics/Point;

    move-result-object v1

    invoke-direct {v3, v1}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    .line 845
    .local v3, "ctrlPt1":Landroid/graphics/PointF;
    new-instance v4, Landroid/graphics/PointF;

    invoke-virtual {v7}, Lcom/samsung/vip/engine/shape/PrimitiveBezier;->getControl02()Landroid/graphics/Point;

    move-result-object v1

    invoke-direct {v4, v1}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    .line 846
    .local v4, "ctrlPt2":Landroid/graphics/PointF;
    new-instance v5, Landroid/graphics/PointF;

    invoke-virtual {v7}, Lcom/samsung/vip/engine/shape/PrimitiveBezier;->getControl03()Landroid/graphics/Point;

    move-result-object v1

    invoke-direct {v5, v1}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    .line 847
    .local v5, "ctrlPt3":Landroid/graphics/PointF;
    new-instance v6, Landroid/graphics/PointF;

    invoke-virtual {v7}, Lcom/samsung/vip/engine/shape/PrimitiveBezier;->getControl04()Landroid/graphics/Point;

    move-result-object v1

    invoke-direct {v6, v1}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    .line 849
    .local v6, "ctrlPt4":Landroid/graphics/PointF;
    const/4 v9, 0x0

    .local v9, "j":I
    move v12, v11

    .end local v11    # "nIndex":I
    .local v12, "nIndex":I
    :goto_1
    const/16 v1, 0x21

    if-lt v9, v1, :cond_2

    .line 842
    :cond_1
    add-int/lit8 v8, v8, 0x1

    move v11, v12

    .end local v12    # "nIndex":I
    .restart local v11    # "nIndex":I
    goto :goto_0

    .line 850
    .end local v11    # "nIndex":I
    .restart local v12    # "nIndex":I
    :cond_2
    int-to-float v1, v9

    int-to-float v0, v15

    move/from16 v16, v0

    div-float v2, v1, v16

    .line 851
    .local v2, "t":F
    if-ge v12, v13, :cond_1

    .line 852
    if-ne v9, v15, :cond_3

    .line 854
    add-int/lit8 v11, v12, 0x1

    .end local v12    # "nIndex":I
    .restart local v11    # "nIndex":I
    const/high16 v2, 0x3f800000    # 1.0f

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/vip/engine/VIShapeGenerator;->getBezierPoint(FLandroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    .end local v2    # "t":F
    move-result-object v1

    aput-object v1, v14, v12

    .line 849
    :goto_2
    add-int/lit8 v9, v9, 0x1

    move v12, v11

    .end local v11    # "nIndex":I
    .restart local v12    # "nIndex":I
    goto :goto_1

    .line 856
    .restart local v2    # "t":F
    :cond_3
    add-int/lit8 v11, v12, 0x1

    .end local v12    # "nIndex":I
    .restart local v11    # "nIndex":I
    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/vip/engine/VIShapeGenerator;->getBezierPoint(FLandroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v1

    aput-object v1, v14, v12

    goto :goto_2
.end method

.method private makeCircle(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveCircle;)V
    .locals 10
    .param p2, "data"    # Lcom/samsung/vip/engine/shape/PrimitiveCircle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/graphics/PointF;",
            ">;",
            "Lcom/samsung/vip/engine/shape/PrimitiveCircle;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Landroid/graphics/PointF;>;"
    const/4 v7, 0x0

    .line 470
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveCircle;->getCenter()Landroid/graphics/Point;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    .line 471
    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveCircle;->getRadius()I

    move-result v0

    int-to-float v3, v0

    .line 472
    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveCircle;->getRadius()I

    move-result v0

    int-to-float v4, v0

    .line 473
    const/4 v5, 0x0

    .line 474
    const/4 v6, 0x0

    .line 477
    const/4 v9, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v8, v7

    .line 470
    invoke-direct/range {v0 .. v9}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeArc(Ljava/util/ArrayList;Landroid/graphics/PointF;FFFZLandroid/graphics/PointF;Landroid/graphics/PointF;I)V

    .line 478
    return-void
.end method

.method private makeCircleArc(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;)V
    .locals 10
    .param p2, "data"    # Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/graphics/PointF;",
            ">;",
            "Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;",
            ")V"
        }
    .end annotation

    .prologue
    .line 492
    .local p1, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Landroid/graphics/PointF;>;"
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->getCenter()Landroid/graphics/Point;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    .line 493
    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->getRadius()I

    move-result v0

    int-to-float v3, v0

    .line 494
    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->getRadius()I

    move-result v0

    int-to-float v4, v0

    .line 495
    const/4 v5, 0x0

    .line 496
    const/4 v6, 0x1

    .line 497
    new-instance v7, Landroid/graphics/PointF;

    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->getStart()Landroid/graphics/Point;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    .line 498
    new-instance v8, Landroid/graphics/PointF;

    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->getEnd()Landroid/graphics/Point;

    move-result-object v0

    invoke-direct {v8, v0}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    .line 499
    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;->getDirection()I

    move-result v9

    move-object v0, p0

    move-object v1, p1

    .line 492
    invoke-direct/range {v0 .. v9}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeArc(Ljava/util/ArrayList;Landroid/graphics/PointF;FFFZLandroid/graphics/PointF;Landroid/graphics/PointF;I)V

    .line 500
    return-void
.end method

.method private makeEllipse(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveEllipse;)V
    .locals 10
    .param p2, "data"    # Lcom/samsung/vip/engine/shape/PrimitiveEllipse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/graphics/PointF;",
            ">;",
            "Lcom/samsung/vip/engine/shape/PrimitiveEllipse;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Landroid/graphics/PointF;>;"
    const/4 v7, 0x0

    .line 481
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->getCenter()Landroid/graphics/Point;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    .line 482
    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->getShortAxis()I

    move-result v0

    int-to-float v3, v0

    .line 483
    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->getLongAxis()I

    move-result v0

    int-to-float v4, v0

    .line 484
    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->getCosTheta()F

    move-result v0

    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;->getSinTheta()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/vip/engine/VIShapeGenerator;->getAngle(FF)F

    move-result v5

    .line 485
    const/4 v6, 0x0

    .line 488
    const/4 v9, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v8, v7

    .line 481
    invoke-direct/range {v0 .. v9}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeArc(Ljava/util/ArrayList;Landroid/graphics/PointF;FFFZLandroid/graphics/PointF;Landroid/graphics/PointF;I)V

    .line 489
    return-void
.end method

.method private makeEllipseArc(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;)V
    .locals 0
    .param p2, "data"    # Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/graphics/PointF;",
            ">;",
            "Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 513
    .local p1, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Landroid/graphics/PointF;>;"
    invoke-direct {p0, p1, p2}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeEllipseArcDirect(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;)V

    .line 514
    return-void
.end method

.method private makeEllipseArcDirect(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;)V
    .locals 21
    .param p2, "ellipseArc"    # Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/graphics/PointF;",
            ">;",
            "Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 517
    .local p1, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Landroid/graphics/PointF;>;"
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getStart()Landroid/graphics/Point;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->POINT_2_POINT_F(Landroid/graphics/Point;)Landroid/graphics/PointF;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getCenter()Landroid/graphics/Point;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getCosTheta()F

    move-result v5

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getSinTheta()F

    move-result v6

    const/4 v7, -0x1

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/samsung/vip/engine/VIShapeGenerator;->Trans_PointF(Landroid/graphics/PointF;Landroid/graphics/Point;FFI)Landroid/graphics/PointF;

    move-result-object v13

    .line 518
    .local v13, "StandardStart":Landroid/graphics/PointF;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getEnd()Landroid/graphics/Point;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->POINT_2_POINT_F(Landroid/graphics/Point;)Landroid/graphics/PointF;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getCenter()Landroid/graphics/Point;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getCosTheta()F

    move-result v5

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getSinTheta()F

    move-result v6

    const/4 v7, -0x1

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/samsung/vip/engine/VIShapeGenerator;->Trans_PointF(Landroid/graphics/PointF;Landroid/graphics/Point;FFI)Landroid/graphics/PointF;

    move-result-object v12

    .line 519
    .local v12, "StandardEnd":Landroid/graphics/PointF;
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3}, Landroid/graphics/PointF;-><init>()V

    .line 523
    .local v3, "CurStandardPoint":Landroid/graphics/PointF;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getLongAxis()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getShortAxis()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v2, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->get_rad_ellipse_f(Landroid/graphics/PointF;FF)F

    move-result v14

    .line 524
    .local v14, "StartAlpha":F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getLongAxis()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getShortAxis()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v2, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->get_rad_ellipse_f(Landroid/graphics/PointF;FF)F

    move-result v11

    .line 526
    .local v11, "EndAlpha":F
    move v8, v14

    .line 527
    .local v8, "CurAlpha":F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getDirection()I

    move-result v10

    .line 528
    .local v10, "Direction":I
    const/4 v2, 0x1

    if-ne v10, v2, :cond_1

    .line 529
    cmpl-float v2, v14, v11

    if-lez v2, :cond_0

    .line 530
    float-to-double v4, v11

    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    add-double/2addr v4, v6

    double-to-float v11, v4

    .line 537
    :cond_0
    :goto_0
    sub-float v20, v11, v14

    .line 538
    .local v20, "sweepAngle":F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getShortAxis()I

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getLongAxis()I

    move-result v4

    add-int/2addr v2, v4

    int-to-float v2, v2

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v4

    mul-float v15, v2, v4

    .line 541
    .local v15, "approximateLength":F
    const v2, 0x453b8000    # 3000.0f

    cmpl-float v2, v15, v2

    if-lez v2, :cond_2

    .line 542
    const/16 v19, 0x80

    .line 548
    .local v19, "stoneCount":I
    :goto_1
    move/from16 v0, v19

    int-to-float v2, v0

    div-float v18, v20, v2

    .line 549
    .local v18, "stone":F
    add-int/lit8 v2, v19, 0x1

    new-array v0, v2, [Landroid/graphics/PointF;

    move-object/from16 v17, v0

    .line 551
    .local v17, "points":[Landroid/graphics/PointF;
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_2
    add-int/lit8 v2, v19, 0x1

    move/from16 v0, v16

    if-lt v0, v2, :cond_3

    .line 560
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 561
    return-void

    .line 533
    .end local v15    # "approximateLength":F
    .end local v16    # "i":I
    .end local v17    # "points":[Landroid/graphics/PointF;
    .end local v18    # "stone":F
    .end local v19    # "stoneCount":I
    .end local v20    # "sweepAngle":F
    :cond_1
    cmpg-float v2, v14, v11

    if-gez v2, :cond_0

    .line 534
    float-to-double v4, v11

    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    sub-double/2addr v4, v6

    double-to-float v11, v4

    goto :goto_0

    .line 544
    .restart local v15    # "approximateLength":F
    .restart local v20    # "sweepAngle":F
    :cond_2
    const/16 v19, 0x40

    .restart local v19    # "stoneCount":I
    goto :goto_1

    .line 552
    .restart local v16    # "i":I
    .restart local v17    # "points":[Landroid/graphics/PointF;
    .restart local v18    # "stone":F
    :cond_3
    move/from16 v0, v16

    int-to-float v2, v0

    mul-float v2, v2, v18

    add-float v8, v14, v2

    .line 554
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getLongAxis()I

    move-result v2

    int-to-double v4, v2

    float-to-double v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-int v2, v4

    int-to-float v2, v2

    iput v2, v3, Landroid/graphics/PointF;->x:F

    .line 555
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getShortAxis()I

    move-result v2

    int-to-double v4, v2

    float-to-double v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-int v2, v4

    int-to-float v2, v2

    iput v2, v3, Landroid/graphics/PointF;->y:F

    .line 556
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getCenter()Landroid/graphics/Point;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getCosTheta()F

    move-result v5

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;->getSinTheta()F

    move-result v6

    const/4 v7, 0x1

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/samsung/vip/engine/VIShapeGenerator;->Trans_PointF(Landroid/graphics/PointF;Landroid/graphics/Point;FFI)Landroid/graphics/PointF;

    move-result-object v9

    .line 558
    .local v9, "CurPoint":Landroid/graphics/PointF;
    new-instance v2, Landroid/graphics/PointF;

    iget v4, v9, Landroid/graphics/PointF;->x:F

    iget v5, v9, Landroid/graphics/PointF;->y:F

    invoke-direct {v2, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v2, v17, v16

    .line 551
    add-int/lit8 v16, v16, 0x1

    goto :goto_2
.end method

.method private makeExceptionalProcessForDoubleArrow(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/graphics/PointF;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/vip/engine/shape/PrimitiveArrow;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/vip/engine/shape/PrimitiveLine;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Landroid/graphics/PointF;>;"
    .local p2, "arrowList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/vip/engine/shape/PrimitiveArrow;>;"
    .local p3, "lineList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/vip/engine/shape/PrimitiveLine;>;"
    const/4 v5, 0x1

    .line 814
    const/4 v0, 0x0

    .line 815
    .local v0, "bDoubleArrow":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 826
    :goto_1
    if-eqz v0, :cond_3

    .line 827
    invoke-virtual {p3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/vip/engine/shape/PrimitiveLine;

    invoke-direct {p0, p1, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeLine(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveLine;)V

    .line 828
    const/4 v4, 0x2

    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/vip/engine/shape/PrimitiveLine;

    invoke-direct {p0, p1, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeLine(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveLine;)V

    .line 834
    :cond_0
    return-void

    .line 816
    :cond_1
    const/4 v4, 0x0

    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/vip/engine/shape/PrimitiveLine;

    .line 817
    .local v3, "mainLine":Lcom/samsung/vip/engine/shape/PrimitiveLine;
    new-instance v2, Lcom/samsung/vip/engine/shape/PrimitiveLine;

    invoke-direct {v2}, Lcom/samsung/vip/engine/shape/PrimitiveLine;-><init>()V

    .line 818
    .local v2, "line":Lcom/samsung/vip/engine/shape/PrimitiveLine;
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/vip/engine/shape/PrimitiveArrow;

    invoke-virtual {v4}, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->getStart()Landroid/graphics/Point;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->setStart(Landroid/graphics/Point;)V

    .line 819
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/vip/engine/shape/PrimitiveArrow;

    invoke-virtual {v4}, Lcom/samsung/vip/engine/shape/PrimitiveArrow;->getEnd()Landroid/graphics/Point;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->setEnd(Landroid/graphics/Point;)V

    .line 821
    invoke-virtual {v3, v2, v5}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->isEqual(Lcom/samsung/vip/engine/shape/PrimitiveLine;Z)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 822
    const/4 v0, 0x1

    .line 823
    goto :goto_1

    .line 815
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 830
    .end local v2    # "line":Lcom/samsung/vip/engine/shape/PrimitiveLine;
    .end local v3    # "mainLine":Lcom/samsung/vip/engine/shape/PrimitiveLine;
    :cond_3
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 831
    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/vip/engine/shape/PrimitiveLine;

    invoke-direct {p0, p1, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeLine(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveLine;)V

    .line 830
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private makeGroupAndObject(Ljava/util/ArrayList;[SLjava/util/ArrayList;)V
    .locals 21
    .param p2, "PrimitiveData"    # [S
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/graphics/PointF;",
            ">;[S",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/vip/engine/shape/PrimitiveArrow;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 720
    .local p1, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Landroid/graphics/PointF;>;"
    .local p3, "arrowList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/vip/engine/shape/PrimitiveArrow;>;"
    const/16 v16, 0x0

    .line 721
    .local v16, "kp":I
    aget-short v4, p2, v16

    .line 723
    .local v4, "PrimitiveNum":I
    add-int/lit8 v16, v16, 0x2

    .line 725
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 727
    .local v17, "lineList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/vip/engine/shape/PrimitiveLine;>;"
    const/4 v15, 0x0

    .local v15, "k":I
    :goto_0
    if-lt v15, v4, :cond_1

    .line 802
    if-eqz p3, :cond_b

    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v19

    if-lez v19, :cond_b

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v19

    const/16 v20, 0x5

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_b

    .line 804
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, v17

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeExceptionalProcessForDoubleArrow(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 811
    :cond_0
    return-void

    .line 728
    :cond_1
    aget-short v18, p2, v16

    .line 729
    .local v18, "nType":S
    add-int/lit8 v16, v16, 0x2

    .line 730
    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_3

    .line 732
    new-instance v11, Lcom/samsung/vip/engine/shape/PrimitiveLine;

    invoke-direct {v11}, Lcom/samsung/vip/engine/shape/PrimitiveLine;-><init>()V

    .line 733
    .local v11, "groupLine":Lcom/samsung/vip/engine/shape/PrimitiveLine;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v16

    invoke-direct {v0, v11, v1, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->CopyDataToLine(Lcom/samsung/vip/engine/shape/PrimitiveLine;[SI)V

    .line 737
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 738
    add-int/lit8 v16, v16, 0x8

    .line 727
    .end local v11    # "groupLine":Lcom/samsung/vip/engine/shape/PrimitiveLine;
    :cond_2
    :goto_1
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 740
    :cond_3
    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 741
    new-instance v7, Lcom/samsung/vip/engine/shape/PrimitiveCircle;

    invoke-direct {v7}, Lcom/samsung/vip/engine/shape/PrimitiveCircle;-><init>()V

    .line 742
    .local v7, "groupCircle":Lcom/samsung/vip/engine/shape/PrimitiveCircle;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v16

    invoke-direct {v0, v7, v1, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->CopyDataToCircle(Lcom/samsung/vip/engine/shape/PrimitiveCircle;[SI)V

    .line 744
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v7}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeCircle(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveCircle;)V

    .line 745
    add-int/lit8 v16, v16, 0x6

    .line 746
    goto :goto_1

    .line 747
    .end local v7    # "groupCircle":Lcom/samsung/vip/engine/shape/PrimitiveCircle;
    :cond_4
    const/16 v19, 0x6

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_5

    .line 748
    new-instance v9, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;

    invoke-direct {v9}, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;-><init>()V

    .line 749
    .local v9, "groupEllipse":Lcom/samsung/vip/engine/shape/PrimitiveEllipse;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v16

    invoke-direct {v0, v9, v1, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->CopyDataToEllipse(Lcom/samsung/vip/engine/shape/PrimitiveEllipse;[SI)V

    .line 751
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v9}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeEllipse(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveEllipse;)V

    .line 752
    add-int/lit8 v16, v16, 0xa

    .line 753
    goto :goto_1

    .line 754
    .end local v9    # "groupEllipse":Lcom/samsung/vip/engine/shape/PrimitiveEllipse;
    :cond_5
    const/16 v19, 0x7

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_6

    .line 755
    new-instance v8, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;

    invoke-direct {v8}, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;-><init>()V

    .line 756
    .local v8, "groupCircleArc":Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v16

    invoke-direct {v0, v8, v1, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->CopyDataToCircleArc(Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;[SI)V

    .line 758
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v8}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeCircleArc(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;)V

    .line 759
    add-int/lit8 v16, v16, 0xe

    .line 760
    goto :goto_1

    .line 761
    .end local v8    # "groupCircleArc":Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;
    :cond_6
    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_7

    .line 762
    new-instance v10, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;

    invoke-direct {v10}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;-><init>()V

    .line 763
    .local v10, "groupEllipseArc":Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v16

    invoke-direct {v0, v10, v1, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->CopyDataToEllipseArc(Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;[SI)V

    .line 765
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v10}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeEllipseArc(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;)V

    .line 766
    add-int/lit8 v16, v16, 0x14

    .line 767
    goto/16 :goto_1

    .line 768
    .end local v10    # "groupEllipseArc":Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;
    :cond_7
    const/16 v19, 0x5

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 769
    new-instance v12, Lcom/samsung/vip/engine/shape/PrimitivePolygon;

    invoke-direct {v12}, Lcom/samsung/vip/engine/shape/PrimitivePolygon;-><init>()V

    .line 770
    .local v12, "groupPolygon":Lcom/samsung/vip/engine/shape/PrimitivePolygon;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v16

    invoke-direct {v0, v12, v1, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->CopyDataToPolygon(Lcom/samsung/vip/engine/shape/PrimitivePolygon;[SI)V

    .line 772
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v12}, Lcom/samsung/vip/engine/VIShapeGenerator;->makePolygon(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitivePolygon;)V

    .line 773
    invoke-virtual {v12}, Lcom/samsung/vip/engine/shape/PrimitivePolygon;->getPoints()Ljava/util/ArrayList;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    mul-int/lit8 v19, v19, 0x4

    add-int/lit8 v19, v19, 0x2

    add-int v16, v16, v19

    .line 774
    goto/16 :goto_1

    .line 775
    .end local v12    # "groupPolygon":Lcom/samsung/vip/engine/shape/PrimitivePolygon;
    :cond_8
    const/16 v19, 0xd

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_9

    .line 776
    new-instance v13, Lcom/samsung/vip/engine/shape/PrimitivePolyline;

    invoke-direct {v13}, Lcom/samsung/vip/engine/shape/PrimitivePolyline;-><init>()V

    .line 777
    .local v13, "groupPolyline":Lcom/samsung/vip/engine/shape/PrimitivePolyline;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v16

    invoke-direct {v0, v13, v1, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->CopyDataToPolyline(Lcom/samsung/vip/engine/shape/PrimitivePolyline;[SI)V

    .line 779
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13}, Lcom/samsung/vip/engine/VIShapeGenerator;->makePolyline(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitivePolyline;)V

    .line 780
    invoke-virtual {v13}, Lcom/samsung/vip/engine/shape/PrimitivePolyline;->getPoints()Ljava/util/ArrayList;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    mul-int/lit8 v19, v19, 0x4

    add-int/lit8 v19, v19, 0x2

    add-int v16, v16, v19

    .line 781
    goto/16 :goto_1

    .line 782
    .end local v13    # "groupPolyline":Lcom/samsung/vip/engine/shape/PrimitivePolyline;
    :cond_9
    const/16 v19, 0x9

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_a

    .line 783
    new-instance v6, Lcom/samsung/vip/engine/shape/PrimitiveBezierList;

    invoke-direct {v6}, Lcom/samsung/vip/engine/shape/PrimitiveBezierList;-><init>()V

    .line 784
    .local v6, "groupBezier":Lcom/samsung/vip/engine/shape/PrimitiveBezierList;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v16

    invoke-direct {v0, v6, v1, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->CopyDataToBezier(Lcom/samsung/vip/engine/shape/PrimitiveBezierList;[SI)V

    .line 786
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeBezier(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveBezierList;)V

    .line 787
    invoke-virtual {v6}, Lcom/samsung/vip/engine/shape/PrimitiveBezierList;->getBezierList()Ljava/util/List;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v19

    mul-int/lit8 v19, v19, 0x2

    mul-int/lit8 v19, v19, 0x4

    add-int/lit8 v19, v19, 0x2

    add-int v16, v16, v19

    .line 788
    goto/16 :goto_1

    .line 789
    .end local v6    # "groupBezier":Lcom/samsung/vip/engine/shape/PrimitiveBezierList;
    :cond_a
    const/16 v19, 0xb

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2

    .line 790
    new-instance v5, Lcom/samsung/vip/engine/shape/PrimitiveArrow;

    invoke-direct {v5}, Lcom/samsung/vip/engine/shape/PrimitiveArrow;-><init>()V

    .line 791
    .local v5, "groupArrow":Lcom/samsung/vip/engine/shape/PrimitiveArrow;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v16

    invoke-direct {v0, v5, v1, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->CopyDataToArrow(Lcom/samsung/vip/engine/shape/PrimitiveArrow;[SI)V

    .line 793
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeArrow(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveArrow;)V

    .line 794
    add-int/lit8 v16, v16, 0x10

    goto/16 :goto_1

    .line 807
    .end local v5    # "groupArrow":Lcom/samsung/vip/engine/shape/PrimitiveArrow;
    .end local v18    # "nType":S
    :cond_b
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_2
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v14, v0, :cond_0

    .line 808
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/vip/engine/shape/PrimitiveLine;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeLine(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveLine;)V

    .line 807
    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method private makeLine(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveLine;)V
    .locals 8
    .param p2, "data"    # Lcom/samsung/vip/engine/shape/PrimitiveLine;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/graphics/PointF;",
            ">;",
            "Lcom/samsung/vip/engine/shape/PrimitiveLine;",
            ")V"
        }
    .end annotation

    .prologue
    .line 457
    .local p1, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Landroid/graphics/PointF;>;"
    const/4 v3, 0x3

    new-array v2, v3, [Landroid/graphics/PointF;

    .line 459
    .local v2, "points":[Landroid/graphics/PointF;
    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->getStart()Landroid/graphics/Point;

    move-result-object v0

    .line 460
    .local v0, "p1":Landroid/graphics/Point;
    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->getEnd()Landroid/graphics/Point;

    move-result-object v1

    .line 462
    .local v1, "p2":Landroid/graphics/Point;
    const/4 v3, 0x0

    new-instance v4, Landroid/graphics/PointF;

    iget v5, v0, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    iget v6, v0, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v2, v3

    .line 463
    const/4 v3, 0x1

    new-instance v4, Landroid/graphics/PointF;

    iget v5, v0, Landroid/graphics/Point;->x:I

    iget v6, v1, Landroid/graphics/Point;->x:I

    add-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    iget v6, v0, Landroid/graphics/Point;->y:I

    iget v7, v1, Landroid/graphics/Point;->y:I

    add-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v2, v3

    .line 464
    const/4 v3, 0x2

    new-instance v4, Landroid/graphics/PointF;

    iget v5, v1, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    iget v6, v1, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v2, v3

    .line 466
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 467
    return-void
.end method

.method private makePolygon(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitivePolygon;)V
    .locals 5
    .param p2, "data"    # Lcom/samsung/vip/engine/shape/PrimitivePolygon;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/graphics/PointF;",
            ">;",
            "Lcom/samsung/vip/engine/shape/PrimitivePolygon;",
            ")V"
        }
    .end annotation

    .prologue
    .line 671
    .local p1, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Landroid/graphics/PointF;>;"
    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitivePolygon;->getPoints()Ljava/util/ArrayList;

    move-result-object v3

    .line 672
    .local v3, "points":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Point;>;"
    if-nez v3, :cond_1

    .line 687
    :cond_0
    return-void

    .line 675
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 676
    .local v2, "pointSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 677
    new-instance v1, Lcom/samsung/vip/engine/shape/PrimitiveLine;

    invoke-direct {v1}, Lcom/samsung/vip/engine/shape/PrimitiveLine;-><init>()V

    .line 678
    .local v1, "line":Lcom/samsung/vip/engine/shape/PrimitiveLine;
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Point;

    invoke-virtual {v1, v4}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->setStart(Landroid/graphics/Point;)V

    .line 679
    add-int/lit8 v4, v2, -0x1

    if-ge v0, v4, :cond_2

    .line 680
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Point;

    invoke-virtual {v1, v4}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->setEnd(Landroid/graphics/Point;)V

    .line 684
    :goto_1
    invoke-direct {p0, p1, v1}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeLine(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveLine;)V

    .line 676
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 682
    :cond_2
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Point;

    invoke-virtual {v1, v4}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->setEnd(Landroid/graphics/Point;)V

    goto :goto_1
.end method

.method private makePolyline(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitivePolyline;)V
    .locals 5
    .param p2, "data"    # Lcom/samsung/vip/engine/shape/PrimitivePolyline;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/graphics/PointF;",
            ">;",
            "Lcom/samsung/vip/engine/shape/PrimitivePolyline;",
            ")V"
        }
    .end annotation

    .prologue
    .line 690
    .local p1, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Landroid/graphics/PointF;>;"
    invoke-virtual {p2}, Lcom/samsung/vip/engine/shape/PrimitivePolyline;->getPoints()Ljava/util/ArrayList;

    move-result-object v3

    .line 691
    .local v3, "points":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Point;>;"
    if-nez v3, :cond_1

    .line 701
    :cond_0
    return-void

    .line 694
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 695
    .local v2, "pointSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    add-int/lit8 v4, v2, -0x1

    if-ge v0, v4, :cond_0

    .line 696
    new-instance v1, Lcom/samsung/vip/engine/shape/PrimitiveLine;

    invoke-direct {v1}, Lcom/samsung/vip/engine/shape/PrimitiveLine;-><init>()V

    .line 697
    .local v1, "line":Lcom/samsung/vip/engine/shape/PrimitiveLine;
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Point;

    invoke-virtual {v1, v4}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->setStart(Landroid/graphics/Point;)V

    .line 698
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Point;

    invoke-virtual {v1, v4}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->setEnd(Landroid/graphics/Point;)V

    .line 699
    invoke-direct {p0, p1, v1}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeLine(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveLine;)V

    .line 695
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private makeTable(Ljava/util/ArrayList;[S)V
    .locals 9
    .param p2, "PrimitiveData"    # [S
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/graphics/PointF;",
            ">;[S)V"
        }
    .end annotation

    .prologue
    .local p1, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Landroid/graphics/PointF;>;"
    const/4 v6, 0x1

    .line 877
    aget-short v5, p2, v6

    add-int/lit8 v5, v5, 0x2

    add-int/lit8 v5, v5, 0x1

    aget-short v0, p2, v5

    .line 878
    .local v0, "HNum":I
    aget-short v5, p2, v6

    add-int/lit8 v5, v5, 0x2

    add-int/lit8 v5, v5, 0x2

    aget-short v1, p2, v5

    .line 880
    .local v1, "VNum":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v0, :cond_0

    .line 886
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    if-lt v3, v1, :cond_1

    .line 892
    return-void

    .line 881
    .end local v3    # "j":I
    :cond_0
    new-instance v4, Lcom/samsung/vip/engine/shape/PrimitiveLine;

    invoke-direct {v4}, Lcom/samsung/vip/engine/shape/PrimitiveLine;-><init>()V

    .line 882
    .local v4, "line":Lcom/samsung/vip/engine/shape/PrimitiveLine;
    new-instance v5, Landroid/graphics/Point;

    mul-int/lit8 v6, v2, 0x4

    add-int/lit8 v6, v6, 0x2

    add-int/lit8 v6, v6, 0x0

    aget-short v6, p2, v6

    mul-int/lit8 v7, v2, 0x4

    add-int/lit8 v7, v7, 0x2

    add-int/lit8 v7, v7, 0x1

    aget-short v7, p2, v7

    invoke-direct {v5, v6, v7}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v4, v5}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->setStart(Landroid/graphics/Point;)V

    .line 883
    new-instance v5, Landroid/graphics/Point;

    mul-int/lit8 v6, v2, 0x4

    add-int/lit8 v6, v6, 0x2

    add-int/lit8 v6, v6, 0x2

    aget-short v6, p2, v6

    mul-int/lit8 v7, v2, 0x4

    add-int/lit8 v7, v7, 0x2

    add-int/lit8 v7, v7, 0x3

    aget-short v7, p2, v7

    invoke-direct {v5, v6, v7}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v4, v5}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->setEnd(Landroid/graphics/Point;)V

    .line 884
    invoke-direct {p0, p1, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeLine(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveLine;)V

    .line 880
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 887
    .end local v4    # "line":Lcom/samsung/vip/engine/shape/PrimitiveLine;
    .restart local v3    # "j":I
    :cond_1
    new-instance v4, Lcom/samsung/vip/engine/shape/PrimitiveLine;

    invoke-direct {v4}, Lcom/samsung/vip/engine/shape/PrimitiveLine;-><init>()V

    .line 888
    .restart local v4    # "line":Lcom/samsung/vip/engine/shape/PrimitiveLine;
    new-instance v5, Landroid/graphics/Point;

    mul-int/lit8 v6, v0, 0x4

    add-int/lit8 v6, v6, 0x2

    mul-int/lit8 v7, v3, 0x4

    add-int/2addr v6, v7

    add-int/lit8 v6, v6, 0x0

    aget-short v6, p2, v6

    mul-int/lit8 v7, v0, 0x4

    add-int/lit8 v7, v7, 0x2

    mul-int/lit8 v8, v3, 0x4

    add-int/2addr v7, v8

    add-int/lit8 v7, v7, 0x1

    aget-short v7, p2, v7

    invoke-direct {v5, v6, v7}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v4, v5}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->setStart(Landroid/graphics/Point;)V

    .line 889
    new-instance v5, Landroid/graphics/Point;

    mul-int/lit8 v6, v0, 0x4

    add-int/lit8 v6, v6, 0x2

    mul-int/lit8 v7, v3, 0x4

    add-int/2addr v6, v7

    add-int/lit8 v6, v6, 0x2

    aget-short v6, p2, v6

    mul-int/lit8 v7, v0, 0x4

    add-int/lit8 v7, v7, 0x2

    mul-int/lit8 v8, v3, 0x4

    add-int/2addr v7, v8

    add-int/lit8 v7, v7, 0x3

    aget-short v7, p2, v7

    invoke-direct {v5, v6, v7}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v4, v5}, Lcom/samsung/vip/engine/shape/PrimitiveLine;->setEnd(Landroid/graphics/Point;)V

    .line 890
    invoke-direct {p0, p1, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeLine(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveLine;)V

    .line 886
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1
.end method


# virtual methods
.method public addShape(Lcom/samsung/vip/engine/shape/GraphPrimitive;)V
    .locals 1
    .param p1, "shape"    # Lcom/samsung/vip/engine/shape/GraphPrimitive;

    .prologue
    .line 40
    sget-object v0, Lcom/samsung/vip/engine/VIShapeGenerator;->mShapeList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 44
    :goto_0
    return-void

    .line 43
    :cond_0
    sget-object v0, Lcom/samsung/vip/engine/VIShapeGenerator;->mShapeList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public generate(Ljava/util/ArrayList;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/graphics/PointF;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Landroid/graphics/PointF;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .local v4, "arrowList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/vip/engine/shape/PrimitiveArrow;>;"
    sget-object v16, Lcom/samsung/vip/engine/VIShapeGenerator;->mShapeList:Ljava/util/ArrayList;

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-nez v17, :cond_0

    .line 122
    return-void

    .line 48
    :cond_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/vip/engine/shape/GraphPrimitive;

    .line 50
    .local v15, "shape":Lcom/samsung/vip/engine/shape/GraphPrimitive;
    iget-short v11, v15, Lcom/samsung/vip/engine/shape/GraphPrimitive;->nType:S

    .line 54
    .local v11, "nType":S
    iget-object v2, v15, Lcom/samsung/vip/engine/shape/GraphPrimitive;->PrimitiveData:[S

    .line 61
    .local v2, "PrimitiveData":[S
    packed-switch v11, :pswitch_data_0

    .line 119
    :goto_1
    :pswitch_0
    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v12, v0, [Landroid/graphics/PointF;

    .line 120
    .local v12, "points":[Landroid/graphics/PointF;
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 63
    .end local v12    # "points":[Landroid/graphics/PointF;
    :pswitch_1
    new-instance v10, Lcom/samsung/vip/engine/shape/PrimitiveLine;

    invoke-direct {v10}, Lcom/samsung/vip/engine/shape/PrimitiveLine;-><init>()V

    .line 64
    .local v10, "line":Lcom/samsung/vip/engine/shape/PrimitiveLine;
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v10, v2, v1}, Lcom/samsung/vip/engine/VIShapeGenerator;->CopyDataToLine(Lcom/samsung/vip/engine/shape/PrimitiveLine;[SI)V

    .line 65
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v10}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeLine(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveLine;)V

    goto :goto_1

    .line 68
    .end local v10    # "line":Lcom/samsung/vip/engine/shape/PrimitiveLine;
    :pswitch_2
    new-instance v6, Lcom/samsung/vip/engine/shape/PrimitiveCircle;

    invoke-direct {v6}, Lcom/samsung/vip/engine/shape/PrimitiveCircle;-><init>()V

    .line 69
    .local v6, "circle":Lcom/samsung/vip/engine/shape/PrimitiveCircle;
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v6, v2, v1}, Lcom/samsung/vip/engine/VIShapeGenerator;->CopyDataToCircle(Lcom/samsung/vip/engine/shape/PrimitiveCircle;[SI)V

    .line 70
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeCircle(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveCircle;)V

    goto :goto_1

    .line 73
    .end local v6    # "circle":Lcom/samsung/vip/engine/shape/PrimitiveCircle;
    :pswitch_3
    new-instance v8, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;

    invoke-direct {v8}, Lcom/samsung/vip/engine/shape/PrimitiveEllipse;-><init>()V

    .line 74
    .local v8, "ellipse":Lcom/samsung/vip/engine/shape/PrimitiveEllipse;
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v8, v2, v1}, Lcom/samsung/vip/engine/VIShapeGenerator;->CopyDataToEllipse(Lcom/samsung/vip/engine/shape/PrimitiveEllipse;[SI)V

    .line 75
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v8}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeEllipse(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveEllipse;)V

    goto :goto_1

    .line 78
    .end local v8    # "ellipse":Lcom/samsung/vip/engine/shape/PrimitiveEllipse;
    :pswitch_4
    new-instance v7, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;

    invoke-direct {v7}, Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;-><init>()V

    .line 79
    .local v7, "circleArc":Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v7, v2, v1}, Lcom/samsung/vip/engine/VIShapeGenerator;->CopyDataToCircleArc(Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;[SI)V

    .line 80
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v7}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeCircleArc(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;)V

    goto :goto_1

    .line 83
    .end local v7    # "circleArc":Lcom/samsung/vip/engine/shape/PrimitiveCircleARC;
    :pswitch_5
    new-instance v9, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;

    invoke-direct {v9}, Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;-><init>()V

    .line 84
    .local v9, "ellipseArc":Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v9, v2, v1}, Lcom/samsung/vip/engine/VIShapeGenerator;->CopyDataToEllipseArc(Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;[SI)V

    .line 85
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v9}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeEllipseArc(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;)V

    goto :goto_1

    .line 88
    .end local v9    # "ellipseArc":Lcom/samsung/vip/engine/shape/PrimitiveEllipseArc;
    :pswitch_6
    new-instance v13, Lcom/samsung/vip/engine/shape/PrimitivePolygon;

    invoke-direct {v13}, Lcom/samsung/vip/engine/shape/PrimitivePolygon;-><init>()V

    .line 89
    .local v13, "polygon":Lcom/samsung/vip/engine/shape/PrimitivePolygon;
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v13, v2, v1}, Lcom/samsung/vip/engine/VIShapeGenerator;->CopyDataToPolygon(Lcom/samsung/vip/engine/shape/PrimitivePolygon;[SI)V

    .line 90
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13}, Lcom/samsung/vip/engine/VIShapeGenerator;->makePolygon(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitivePolygon;)V

    goto/16 :goto_1

    .line 93
    .end local v13    # "polygon":Lcom/samsung/vip/engine/shape/PrimitivePolygon;
    :pswitch_7
    new-instance v14, Lcom/samsung/vip/engine/shape/PrimitivePolyline;

    invoke-direct {v14}, Lcom/samsung/vip/engine/shape/PrimitivePolyline;-><init>()V

    .line 94
    .local v14, "polyline":Lcom/samsung/vip/engine/shape/PrimitivePolyline;
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v14, v2, v1}, Lcom/samsung/vip/engine/VIShapeGenerator;->CopyDataToPolyline(Lcom/samsung/vip/engine/shape/PrimitivePolyline;[SI)V

    .line 95
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v14}, Lcom/samsung/vip/engine/VIShapeGenerator;->makePolyline(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitivePolyline;)V

    goto/16 :goto_1

    .line 98
    .end local v14    # "polyline":Lcom/samsung/vip/engine/shape/PrimitivePolyline;
    :pswitch_8
    new-instance v3, Lcom/samsung/vip/engine/shape/PrimitiveArrow;

    invoke-direct {v3}, Lcom/samsung/vip/engine/shape/PrimitiveArrow;-><init>()V

    .line 99
    .local v3, "arrow":Lcom/samsung/vip/engine/shape/PrimitiveArrow;
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v3, v2, v1}, Lcom/samsung/vip/engine/VIShapeGenerator;->CopyDataToArrow(Lcom/samsung/vip/engine/shape/PrimitiveArrow;[SI)V

    .line 100
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeArrow(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveArrow;)V

    .line 101
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 104
    .end local v3    # "arrow":Lcom/samsung/vip/engine/shape/PrimitiveArrow;
    :pswitch_9
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeGroupAndObject(Ljava/util/ArrayList;[SLjava/util/ArrayList;)V

    goto/16 :goto_1

    .line 107
    :pswitch_a
    new-instance v5, Lcom/samsung/vip/engine/shape/PrimitiveBezierList;

    invoke-direct {v5}, Lcom/samsung/vip/engine/shape/PrimitiveBezierList;-><init>()V

    .line 108
    .local v5, "bezierList":Lcom/samsung/vip/engine/shape/PrimitiveBezierList;
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v5, v2, v1}, Lcom/samsung/vip/engine/VIShapeGenerator;->CopyDataToBezier(Lcom/samsung/vip/engine/shape/PrimitiveBezierList;[SI)V

    .line 109
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeBezier(Ljava/util/ArrayList;Lcom/samsung/vip/engine/shape/PrimitiveBezierList;)V

    goto/16 :goto_1

    .line 112
    .end local v5    # "bezierList":Lcom/samsung/vip/engine/shape/PrimitiveBezierList;
    :pswitch_b
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->makeTable(Ljava/util/ArrayList;[S)V

    goto/16 :goto_1

    .line 61
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_b
        :pswitch_7
    .end packed-switch
.end method
