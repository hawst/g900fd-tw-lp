.class public Lcom/samsung/app/share/via/external/ShareviaObj;
.super Ljava/lang/Object;
.source "ShareviaObj.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/app/share/via/external/ShareviaObj$transcodeMode;,
        Lcom/samsung/app/share/via/external/ShareviaObj$codecType;,
        Lcom/samsung/app/share/via/external/ShareviaObj$videoResType;
    }
.end annotation


# instance fields
.field OutFileResolution:I

.field assetmngr:Landroid/content/res/AssetManager;

.field audioCodecType:I

.field audioLength:I

.field audioOffset:I

.field endTime:I

.field iconFileName:Ljava/lang/String;

.field inputFileName:Ljava/lang/String;

.field maxOutFileDuration:I

.field maxOutFileSize:I

.field outputFileName:Ljava/lang/String;

.field startTime:I

.field transcodeMode:I

.field videoCodecType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method


# virtual methods
.method public getShareViaAudioLength()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->audioLength:I

    return v0
.end method

.method public getShareViaAudioOffset()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->audioOffset:I

    return v0
.end method

.method public getShareViaTranscodeMode()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->transcodeMode:I

    return v0
.end method

.method public setShareViaAssetmngr(Landroid/content/res/AssetManager;)V
    .locals 0
    .param p1, "assetmngr"    # Landroid/content/res/AssetManager;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->assetmngr:Landroid/content/res/AssetManager;

    .line 50
    return-void
.end method

.method public setShareViaAudioCodec(I)V
    .locals 0
    .param p1, "audiocodec"    # I

    .prologue
    .line 86
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->audioCodecType:I

    .line 87
    return-void
.end method

.method public setShareViaAudioLength(I)V
    .locals 0
    .param p1, "audioLength"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->audioLength:I

    .line 63
    return-void
.end method

.method public setShareViaAudioOffset(I)V
    .locals 0
    .param p1, "audioOffset"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->audioOffset:I

    .line 57
    return-void
.end method

.method public setShareViaEndTime(I)V
    .locals 0
    .param p1, "endtime"    # I

    .prologue
    .line 74
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->endTime:I

    .line 75
    return-void
.end method

.method public setShareViaIconFileName(Ljava/lang/String;)V
    .locals 0
    .param p1, "outfilename"    # Ljava/lang/String;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->iconFileName:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public setShareViaInputFilename(Ljava/lang/String;)V
    .locals 0
    .param p1, "inputfilename"    # Ljava/lang/String;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->inputFileName:Ljava/lang/String;

    .line 90
    return-void
.end method

.method public setShareViaOutputFileResolution(I)V
    .locals 0
    .param p1, "OutfileResolution"    # I

    .prologue
    .line 98
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->OutFileResolution:I

    .line 99
    return-void
.end method

.method public setShareViaOutputFilename(Ljava/lang/String;)V
    .locals 0
    .param p1, "outfilename"    # Ljava/lang/String;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->outputFileName:Ljava/lang/String;

    .line 93
    return-void
.end method

.method public setShareViaStartTime(I)V
    .locals 0
    .param p1, "starttime"    # I

    .prologue
    .line 71
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->startTime:I

    .line 72
    return-void
.end method

.method public setShareViaTranscodeMode(I)V
    .locals 0
    .param p1, "transcodeMode"    # I

    .prologue
    .line 68
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->transcodeMode:I

    .line 69
    return-void
.end method

.method public setShareViaVideoCodec(I)V
    .locals 0
    .param p1, "vtVideoCodec"    # I

    .prologue
    .line 83
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->videoCodecType:I

    .line 84
    return-void
.end method

.method public setShareViamaxDuration(I)V
    .locals 0
    .param p1, "maxduration"    # I

    .prologue
    .line 77
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->maxOutFileDuration:I

    .line 78
    return-void
.end method

.method public setShareViamaxSize(I)V
    .locals 0
    .param p1, "maxsize"    # I

    .prologue
    .line 80
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->maxOutFileSize:I

    .line 81
    return-void
.end method
