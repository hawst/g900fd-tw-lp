.class Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;
.super Ljava/lang/Object;
.source "NRRUserModel.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DefaultLocale"
    }
.end annotation


# static fields
.field public static final EMPTY:I = 0x0

.field private static final FILES_DIR:Ljava/lang/String; = "signatures"

.field public static final LOADED_FROM_FILE:I = 0x1

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final MODEL_FILE:Ljava/lang/String; = "model.dat"

.field public static final TRAINED_FROM_USER_INPUT:I = 0x2


# instance fields
.field private final mDirPath:Ljava/lang/String;

.field private mModel:Lcom/samsung/recognitionengine/UserModel;

.field private final mModelFileName:Ljava/lang/String;

.field private mState:I

.field private mTrainer:Lcom/samsung/recognitionengine/Trainer;

.field private mVerifier:Lcom/samsung/recognitionengine/Verifier;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mState:I

    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "signatures"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mDirPath:Ljava/lang/String;

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mDirPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "model.dat"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModelFileName:Ljava/lang/String;

    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->loadModelFromFile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mState:I

    .line 54
    :cond_0
    return-void
.end method

.method public static convertSpenObjectStrokesToSignature(Ljava/util/List;)Lcom/samsung/recognitionengine/Signature;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;)",
            "Lcom/samsung/recognitionengine/Signature;"
        }
    .end annotation

    .prologue
    .line 226
    .local p0, "spenObjectStrokes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    new-instance v10, Lcom/samsung/recognitionengine/Signature;

    invoke-direct {v10}, Lcom/samsung/recognitionengine/Signature;-><init>()V

    .line 229
    .local v10, "signature":Lcom/samsung/recognitionengine/Signature;
    const/high16 v5, -0x80000000

    .line 230
    .local v5, "lastValidTimeStamp":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-lt v3, v0, :cond_0

    .line 264
    return-object v10

    .line 231
    :cond_0
    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .line 232
    .local v12, "spenObjectStroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v17

    move-object/from16 v0, v17

    array-length v11, v0

    .line 233
    .local v11, "size":I
    new-instance v14, Lcom/samsung/recognitionengine/Stroke;

    invoke-direct {v14}, Lcom/samsung/recognitionengine/Stroke;-><init>()V

    .line 234
    .local v14, "stroke":Lcom/samsung/recognitionengine/Stroke;
    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v8

    .line 235
    .local v8, "pointFs":[Landroid/graphics/PointF;
    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPressures()[F

    move-result-object v9

    .line 236
    .local v9, "pressures":[F
    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getTimeStamps()[I

    move-result-object v16

    .line 237
    .local v16, "timeStamps":[I
    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getOrientations()[F

    move-result-object v6

    .line 238
    .local v6, "orientations":[F
    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getTilts()[F

    move-result-object v15

    .line 240
    .local v15, "tilts":[F
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    if-lt v4, v11, :cond_1

    .line 262
    invoke-virtual {v10, v14}, Lcom/samsung/recognitionengine/Signature;->add(Lcom/samsung/recognitionengine/Stroke;)V

    .line 230
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 241
    :cond_1
    new-instance v13, Lcom/samsung/recognitionengine/TouchPoint;

    invoke-direct {v13}, Lcom/samsung/recognitionengine/TouchPoint;-><init>()V

    .line 242
    .local v13, "sshapePoint":Lcom/samsung/recognitionengine/TouchPoint;
    new-instance v7, Lcom/samsung/recognitionengine/PointF;

    aget-object v17, v8, v4

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v17, v0

    aget-object v18, v8, v4

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v7, v0, v1}, Lcom/samsung/recognitionengine/PointF;-><init>(FF)V

    .line 243
    .local v7, "pointF":Lcom/samsung/recognitionengine/PointF;
    invoke-virtual {v13, v7}, Lcom/samsung/recognitionengine/TouchPoint;->setPoint(Lcom/samsung/recognitionengine/PointF;)V

    .line 244
    aget v17, v9, v4

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/samsung/recognitionengine/TouchPoint;->setPressure(F)V

    .line 245
    aget v17, v6, v4

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/samsung/recognitionengine/TouchPoint;->setOrientation(F)V

    .line 246
    aget v17, v15, v4

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/samsung/recognitionengine/TouchPoint;->setTilt(F)V

    .line 248
    aget v2, v16, v4

    .line 250
    .local v2, "currentTimeStamp":I
    if-eqz v4, :cond_2

    sub-int v17, v2, v5

    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(I)I

    move-result v17

    const/16 v18, 0x2710

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_2

    .line 240
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 253
    :cond_2
    if-ge v2, v5, :cond_3

    .line 254
    int-to-double v0, v5

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v13, v0, v1}, Lcom/samsung/recognitionengine/TouchPoint;->setTimestamp(D)V

    .line 260
    :goto_3
    invoke-virtual {v14, v13}, Lcom/samsung/recognitionengine/Stroke;->add(Lcom/samsung/recognitionengine/TouchPoint;)V

    goto :goto_2

    .line 256
    :cond_3
    int-to-double v0, v2

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v13, v0, v1}, Lcom/samsung/recognitionengine/TouchPoint;->setTimestamp(D)V

    .line 257
    move v5, v2

    goto :goto_3
.end method

.method private createTrainer()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 214
    new-instance v0, Lcom/samsung/recognitionengine/Trainer;

    invoke-direct {v0}, Lcom/samsung/recognitionengine/Trainer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    .line 215
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/Trainer;->setValidateNextSignature(Z)V

    .line 217
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/Trainer;->setSimplicityChecking(Z)V

    .line 218
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    sget-object v1, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->SimplicityLevel_Medium:Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/Trainer;->setSimplicityLevel(Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    :goto_0
    return-void

    .line 219
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private dropFile(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 139
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 140
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 141
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 143
    :cond_0
    return-void
.end method

.method public static getMinimumRequiredCount()I
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x3

    return v0
.end method

.method private getModel()Lcom/samsung/recognitionengine/UserModel;
    .locals 3

    .prologue
    .line 129
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mVerifier:Lcom/samsung/recognitionengine/Verifier;

    if-eqz v1, :cond_0

    .line 130
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mVerifier:Lcom/samsung/recognitionengine/Verifier;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/Verifier;->getModel()Lcom/samsung/recognitionengine/UserModel;
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 135
    :goto_0
    return-object v1

    .line 132
    :catch_0
    move-exception v0

    .line 133
    .local v0, "er":Ljava/lang/UnsatisfiedLinkError;
    sget-object v1, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Can not resolve native method getModel() in com.samsung.recognitionengine.Verifier. May be you are using old native library."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 135
    .end local v0    # "er":Ljava/lang/UnsatisfiedLinkError;
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    goto :goto_0
.end method

.method private loadModelFromFile()Z
    .locals 10

    .prologue
    .line 146
    const/4 v2, 0x0

    .line 149
    .local v2, "fileReader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    new-instance v8, Ljava/io/FileInputStream;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModelFileName:Ljava/lang/String;

    invoke-direct {v8, v9}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v7, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    .end local v2    # "fileReader":Ljava/io/BufferedReader;
    .local v3, "fileReader":Ljava/io/BufferedReader;
    :try_start_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 151
    .local v6, "stringBuilder":Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    .line 152
    .local v4, "line":Ljava/lang/String;
    :goto_0
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 156
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 157
    .local v0, "content":Ljava/lang/String;
    new-instance v5, Lcom/samsung/recognitionengine/UserModelStringReader;

    invoke-direct {v5, v0}, Lcom/samsung/recognitionengine/UserModelStringReader;-><init>(Ljava/lang/String;)V

    .line 158
    .local v5, "modelReader":Lcom/samsung/recognitionengine/UserModelStringReader;
    invoke-static {v5}, Lcom/samsung/recognitionengine/UserModel;->readModel(Lcom/samsung/recognitionengine/UserModelReader;)Lcom/samsung/recognitionengine/UserModel;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    .line 160
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    invoke-virtual {v7}, Lcom/samsung/recognitionengine/UserModel;->isValid()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 161
    new-instance v7, Lcom/samsung/recognitionengine/Verifier;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    invoke-direct {v7, v8}, Lcom/samsung/recognitionengine/Verifier;-><init>(Lcom/samsung/recognitionengine/UserModel;)V

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mVerifier:Lcom/samsung/recognitionengine/Verifier;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 167
    if-eqz v3, :cond_0

    .line 169
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 162
    :cond_0
    :goto_1
    const/4 v7, 0x1

    move-object v2, v3

    .line 176
    .end local v0    # "content":Ljava/lang/String;
    .end local v3    # "fileReader":Ljava/io/BufferedReader;
    .end local v4    # "line":Ljava/lang/String;
    .end local v5    # "modelReader":Lcom/samsung/recognitionengine/UserModelStringReader;
    .end local v6    # "stringBuilder":Ljava/lang/StringBuilder;
    .restart local v2    # "fileReader":Ljava/io/BufferedReader;
    :goto_2
    return v7

    .line 153
    .end local v2    # "fileReader":Ljava/io/BufferedReader;
    .restart local v3    # "fileReader":Ljava/io/BufferedReader;
    .restart local v4    # "line":Ljava/lang/String;
    .restart local v6    # "stringBuilder":Ljava/lang/StringBuilder;
    :cond_1
    :try_start_3
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    const/16 v7, 0xa

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 164
    .end local v4    # "line":Ljava/lang/String;
    .end local v6    # "stringBuilder":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v1

    move-object v2, v3

    .line 165
    .end local v3    # "fileReader":Ljava/io/BufferedReader;
    .local v1, "ex":Ljava/lang/Exception;
    .restart local v2    # "fileReader":Ljava/io/BufferedReader;
    :goto_3
    :try_start_4
    sget-object v7, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Failed to read model from file "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModelFileName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 167
    if-eqz v2, :cond_2

    .line 169
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    .line 176
    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_2
    :goto_4
    const/4 v7, 0x0

    goto :goto_2

    .line 170
    .end local v2    # "fileReader":Ljava/io/BufferedReader;
    .restart local v0    # "content":Ljava/lang/String;
    .restart local v3    # "fileReader":Ljava/io/BufferedReader;
    .restart local v4    # "line":Ljava/lang/String;
    .restart local v5    # "modelReader":Lcom/samsung/recognitionengine/UserModelStringReader;
    .restart local v6    # "stringBuilder":Ljava/lang/StringBuilder;
    :catch_1
    move-exception v1

    .line 171
    .restart local v1    # "ex":Ljava/lang/Exception;
    sget-object v7, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    const-string v8, "Failed to close reader!"

    invoke-static {v7, v8, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 170
    .end local v0    # "content":Ljava/lang/String;
    .end local v3    # "fileReader":Ljava/io/BufferedReader;
    .end local v4    # "line":Ljava/lang/String;
    .end local v5    # "modelReader":Lcom/samsung/recognitionengine/UserModelStringReader;
    .end local v6    # "stringBuilder":Ljava/lang/StringBuilder;
    .restart local v2    # "fileReader":Ljava/io/BufferedReader;
    :catch_2
    move-exception v1

    .line 171
    sget-object v7, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    const-string v8, "Failed to close reader!"

    invoke-static {v7, v8, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 166
    .end local v1    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    .line 167
    :goto_5
    if-eqz v2, :cond_3

    .line 169
    :try_start_6
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 174
    :cond_3
    :goto_6
    throw v7

    .line 170
    :catch_3
    move-exception v1

    .line 171
    .restart local v1    # "ex":Ljava/lang/Exception;
    sget-object v8, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    const-string v9, "Failed to close reader!"

    invoke-static {v8, v9, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6

    .line 167
    .end local v1    # "ex":Ljava/lang/Exception;
    .end local v2    # "fileReader":Ljava/io/BufferedReader;
    .restart local v0    # "content":Ljava/lang/String;
    .restart local v3    # "fileReader":Ljava/io/BufferedReader;
    .restart local v4    # "line":Ljava/lang/String;
    .restart local v5    # "modelReader":Lcom/samsung/recognitionengine/UserModelStringReader;
    .restart local v6    # "stringBuilder":Ljava/lang/StringBuilder;
    :cond_4
    if-eqz v3, :cond_5

    .line 169
    :try_start_7
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    move-object v2, v3

    .line 170
    .end local v3    # "fileReader":Ljava/io/BufferedReader;
    .restart local v2    # "fileReader":Ljava/io/BufferedReader;
    goto :goto_4

    .end local v2    # "fileReader":Ljava/io/BufferedReader;
    .restart local v3    # "fileReader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v1

    .line 171
    .restart local v1    # "ex":Ljava/lang/Exception;
    sget-object v7, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    const-string v8, "Failed to close reader!"

    invoke-static {v7, v8, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_5
    move-object v2, v3

    .end local v3    # "fileReader":Ljava/io/BufferedReader;
    .restart local v2    # "fileReader":Ljava/io/BufferedReader;
    goto :goto_4

    .line 166
    .end local v0    # "content":Ljava/lang/String;
    .end local v2    # "fileReader":Ljava/io/BufferedReader;
    .end local v4    # "line":Ljava/lang/String;
    .end local v5    # "modelReader":Lcom/samsung/recognitionengine/UserModelStringReader;
    .end local v6    # "stringBuilder":Ljava/lang/StringBuilder;
    .restart local v3    # "fileReader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v7

    move-object v2, v3

    .end local v3    # "fileReader":Ljava/io/BufferedReader;
    .restart local v2    # "fileReader":Ljava/io/BufferedReader;
    goto :goto_5

    .line 164
    :catch_5
    move-exception v1

    goto :goto_3
.end method

.method private saveModelToFile()Z
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 180
    const/4 v5, 0x0

    .line 182
    .local v5, "returnValue":Z
    new-instance v3, Ljava/io/File;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mDirPath:Ljava/lang/String;

    invoke-direct {v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 184
    .local v3, "modelDir":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-nez v7, :cond_1

    .line 185
    sget-object v7, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Failed to create directory "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mDirPath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    :cond_0
    :goto_0
    return v6

    .line 189
    :cond_1
    const/4 v1, 0x0

    .line 192
    .local v1, "fileWriter":Ljava/io/BufferedWriter;
    :try_start_0
    new-instance v4, Lcom/samsung/recognitionengine/UserModelStringWriter;

    invoke-direct {v4}, Lcom/samsung/recognitionengine/UserModelStringWriter;-><init>()V

    .line 193
    .local v4, "modelWriter":Lcom/samsung/recognitionengine/UserModelStringWriter;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    invoke-virtual {v7, v4}, Lcom/samsung/recognitionengine/UserModel;->writeModel(Lcom/samsung/recognitionengine/UserModelWriter;)Z

    move-result v5

    .line 194
    if-eqz v5, :cond_2

    .line 195
    new-instance v2, Ljava/io/BufferedWriter;

    new-instance v7, Ljava/io/OutputStreamWriter;

    new-instance v8, Ljava/io/FileOutputStream;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModelFileName:Ljava/lang/String;

    invoke-direct {v8, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v7, v8}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v2, v7}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 196
    .end local v1    # "fileWriter":Ljava/io/BufferedWriter;
    .local v2, "fileWriter":Ljava/io/BufferedWriter;
    :try_start_1
    invoke-virtual {v4}, Lcom/samsung/recognitionengine/UserModelStringWriter;->getString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v1, v2

    .line 202
    .end local v2    # "fileWriter":Ljava/io/BufferedWriter;
    .restart local v1    # "fileWriter":Ljava/io/BufferedWriter;
    :cond_2
    if-eqz v1, :cond_3

    .line 204
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    :cond_3
    :goto_1
    move v6, v5

    .line 210
    goto :goto_0

    .line 198
    .end local v4    # "modelWriter":Lcom/samsung/recognitionengine/UserModelStringWriter;
    :catch_0
    move-exception v0

    .line 199
    .local v0, "ex":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    sget-object v7, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Can not open file "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModelFileName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 202
    if-eqz v1, :cond_0

    .line 204
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 205
    :catch_1
    move-exception v0

    .line 206
    sget-object v7, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    const-string v8, "Failed to close writer!"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 201
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    .line 202
    :goto_3
    if-eqz v1, :cond_4

    .line 204
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    .line 209
    :cond_4
    :goto_4
    throw v6

    .line 205
    :catch_2
    move-exception v0

    .line 206
    .restart local v0    # "ex":Ljava/lang/Exception;
    sget-object v7, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    const-string v8, "Failed to close writer!"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 205
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v4    # "modelWriter":Lcom/samsung/recognitionengine/UserModelStringWriter;
    :catch_3
    move-exception v0

    .line 206
    .restart local v0    # "ex":Ljava/lang/Exception;
    sget-object v6, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->LOG_TAG:Ljava/lang/String;

    const-string v7, "Failed to close writer!"

    invoke-static {v6, v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 201
    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v1    # "fileWriter":Ljava/io/BufferedWriter;
    .restart local v2    # "fileWriter":Ljava/io/BufferedWriter;
    :catchall_1
    move-exception v6

    move-object v1, v2

    .end local v2    # "fileWriter":Ljava/io/BufferedWriter;
    .restart local v1    # "fileWriter":Ljava/io/BufferedWriter;
    goto :goto_3

    .line 198
    .end local v1    # "fileWriter":Ljava/io/BufferedWriter;
    .restart local v2    # "fileWriter":Ljava/io/BufferedWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fileWriter":Ljava/io/BufferedWriter;
    .restart local v1    # "fileWriter":Ljava/io/BufferedWriter;
    goto :goto_2
.end method


# virtual methods
.method public declared-synchronized addSignature(Ljava/util/List;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "spenObjectStrokes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 90
    monitor-enter p0

    if-nez p1, :cond_1

    .line 116
    :cond_0
    :goto_0
    monitor-exit p0

    return v2

    .line 94
    :cond_1
    :try_start_0
    iget v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mState:I

    if-ne v4, v3, :cond_2

    .line 95
    sget-object v2, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->StrictnessLevel_High:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    invoke-virtual {p0, v2, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->verify(Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;Ljava/util/List;)Z

    move-result v2

    goto :goto_0

    .line 98
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    if-nez v4, :cond_3

    .line 99
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->createTrainer()V

    .line 102
    :cond_3
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->convertSpenObjectStrokesToSignature(Ljava/util/List;)Lcom/samsung/recognitionengine/Signature;

    move-result-object v1

    .line 104
    .local v1, "sig":Lcom/samsung/recognitionengine/Signature;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    invoke-virtual {v4, v1}, Lcom/samsung/recognitionengine/Trainer;->addSignature(Lcom/samsung/recognitionengine/Signature;)Lcom/samsung/recognitionengine/Trainer$EResult;

    move-result-object v0

    .line 106
    .local v0, "res":Lcom/samsung/recognitionengine/Trainer$EResult;
    sget-object v4, Lcom/samsung/recognitionengine/Trainer$EResult;->RES_OK:Lcom/samsung/recognitionengine/Trainer$EResult;

    if-ne v0, v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    invoke-virtual {v4}, Lcom/samsung/recognitionengine/Trainer;->getSignaturesNumber()J

    move-result-wide v4

    const-wide/16 v6, 0x3

    cmp-long v4, v4, v6

    if-ltz v4, :cond_0

    .line 107
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    invoke-virtual {v2}, Lcom/samsung/recognitionengine/Trainer;->trainModel()Lcom/samsung/recognitionengine/UserModel;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    .line 108
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    invoke-virtual {v2}, Lcom/samsung/recognitionengine/UserModel;->isValid()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 109
    const/4 v2, 0x2

    iput v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mState:I

    .line 110
    new-instance v2, Lcom/samsung/recognitionengine/Verifier;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    invoke-direct {v2, v4}, Lcom/samsung/recognitionengine/Verifier;-><init>(Lcom/samsung/recognitionengine/UserModel;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mVerifier:Lcom/samsung/recognitionengine/Verifier;

    .line 111
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->saveModelToFile()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    move v2, v3

    .line 113
    goto :goto_0

    .line 90
    .end local v0    # "res":Lcom/samsung/recognitionengine/Trainer$EResult;
    .end local v1    # "sig":Lcom/samsung/recognitionengine/Signature;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized drop()V
    .locals 1

    .prologue
    .line 120
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mVerifier:Lcom/samsung/recognitionengine/Verifier;

    .line 123
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mState:I

    .line 124
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModelFileName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->dropFile(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    monitor-exit p0

    return-void

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSignaturesNumber()I
    .locals 4

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->getModel()Lcom/samsung/recognitionengine/UserModel;

    move-result-object v0

    .line 62
    .local v0, "model":Lcom/samsung/recognitionengine/UserModel;
    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {v0}, Lcom/samsung/recognitionengine/UserModel;->getSignaturesNumber()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    long-to-int v1, v2

    .line 70
    :goto_0
    monitor-exit p0

    return v1

    .line 66
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    if-eqz v1, :cond_1

    .line 67
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mTrainer:Lcom/samsung/recognitionengine/Trainer;

    invoke-virtual {v1}, Lcom/samsung/recognitionengine/Trainer;->getSignaturesNumber()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v2

    long-to-int v1, v2

    goto :goto_0

    .line 70
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 61
    .end local v0    # "model":Lcom/samsung/recognitionengine/UserModel;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getState()I
    .locals 1

    .prologue
    .line 57
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized verify(Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;Ljava/util/List;)Z
    .locals 5
    .param p1, "level"    # Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p2, "spenObjectStrokes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 74
    monitor-enter p0

    :try_start_0
    iget v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mState:I

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    invoke-virtual {v4}, Lcom/samsung/recognitionengine/UserModel;->isValid()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    move v2, v3

    .line 86
    :goto_0
    monitor-exit p0

    return v2

    .line 77
    :cond_1
    :try_start_1
    new-instance v1, Lcom/samsung/recognitionengine/Verifier;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    invoke-direct {v1, v4}, Lcom/samsung/recognitionengine/Verifier;-><init>(Lcom/samsung/recognitionengine/UserModel;)V

    .line 78
    .local v1, "verifier":Lcom/samsung/recognitionengine/Verifier;
    invoke-virtual {v1, p1}, Lcom/samsung/recognitionengine/Verifier;->setStrictnessLevel(Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;)V

    .line 79
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lcom/samsung/recognitionengine/Verifier;->setAddExtraSignatures(Z)V

    .line 80
    invoke-static {p2}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->convertSpenObjectStrokesToSignature(Ljava/util/List;)Lcom/samsung/recognitionengine/Signature;

    move-result-object v0

    .line 81
    .local v0, "signature":Lcom/samsung/recognitionengine/Signature;
    invoke-virtual {v1, v0}, Lcom/samsung/recognitionengine/Verifier;->isAuthentic(Lcom/samsung/recognitionengine/Signature;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 82
    invoke-virtual {v1}, Lcom/samsung/recognitionengine/Verifier;->getModel()Lcom/samsung/recognitionengine/UserModel;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->mModel:Lcom/samsung/recognitionengine/UserModel;

    .line 83
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->saveModelToFile()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 74
    .end local v0    # "signature":Lcom/samsung/recognitionengine/Signature;
    .end local v1    # "verifier":Lcom/samsung/recognitionengine/Verifier;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .restart local v0    # "signature":Lcom/samsung/recognitionengine/Signature;
    .restart local v1    # "verifier":Lcom/samsung/recognitionengine/Verifier;
    :cond_2
    move v2, v3

    .line 86
    goto :goto_0
.end method
