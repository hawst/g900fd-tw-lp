.class public Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;
.super Landroid/view/View;
.source "SpenSimpleView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;,
        Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SpenSimpleView"

.field private static final penNameBrush:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Brush"

.field private static final penNameChineseBrush:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

.field private static final penNameFountainPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.FountainPen"

.field private static final penNameInkPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.InkPen"

.field private static final penNameMagicPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.MagicPen"

.field private static final penNameMarker:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Marker"

.field private static final penNameObliquePen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

.field private static final penNamePencil:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Pencil"


# instance fields
.field private activePen:I

.field private bIsSupport:Z

.field private cancelTouch:Z

.field private canvasAction:[I

.field private currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

.field private defaultPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

.field private dstRect:Landroid/graphics/RectF;

.field private eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCirclePaint:Landroid/graphics/Paint;

.field private mCirclePoint:Landroid/graphics/PointF;

.field private mCircleRadius:F

.field private mContext:Landroid/content/Context;

.field private mFocus:Landroid/graphics/PointF;

.field private mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

.field private final mHeight:I

.field private mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

.field private mIndexBrush:I

.field private mIndexEraser:I

.field private mIndexMarker:I

.field private mIndexPencil:I

.field private mMatrix:Landroid/graphics/Matrix;

.field private mOldX:F

.field private mOldY:F

.field private mPenList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;",
            ">;"
        }
    .end annotation
.end field

.field private mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

.field private mRatio:F

.field private mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

.field private mSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

.field private mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

.field private mSmps:Lcom/samsung/audio/SmpsManager;

.field private mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

.field private final mWidth:I

.field private updateRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v5, 0x0

    const/high16 v4, -0x3d380000    # -100.0f

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 422
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 44
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    .line 46
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mContext:Landroid/content/Context;

    .line 47
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    .line 48
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    .line 54
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    .line 55
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->cancelTouch:Z

    .line 57
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    .line 58
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    .line 60
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .line 61
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 64
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mRatio:F

    .line 68
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 69
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCircleRadius:F

    .line 70
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v4, v4}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCirclePoint:Landroid/graphics/PointF;

    .line 71
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCirclePaint:Landroid/graphics/Paint;

    .line 74
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 76
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mOldX:F

    .line 77
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mOldY:F

    .line 80
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->bIsSupport:Z

    .line 81
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 82
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mIndexPencil:I

    .line 83
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mIndexMarker:I

    .line 84
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mIndexBrush:I

    .line 85
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mIndexEraser:I

    .line 86
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->activePen:I

    .line 423
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mContext:Landroid/content/Context;

    .line 424
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->construct(Landroid/content/Context;)V

    .line 426
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mWidth:I

    .line 427
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHeight:I

    .line 428
    invoke-direct {p0, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->createBitmap(II)V

    .line 429
    return-void
.end method

.method private JoinRect(Landroid/graphics/Rect;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "dst"    # Landroid/graphics/Rect;
    .param p2, "src"    # Landroid/graphics/RectF;

    .prologue
    .line 102
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p2, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v1

    if-gez v0, :cond_0

    .line 110
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->right:I

    if-ge v0, v1, :cond_2

    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    if-lt v0, v1, :cond_3

    .line 111
    :cond_2
    iget v0, p2, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 112
    iget v0, p2, Landroid/graphics/RectF;->right:F

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 113
    iget v0, p2, Landroid/graphics/RectF;->top:F

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 114
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 116
    :cond_3
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    .line 117
    iget v0, p2, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 119
    :cond_4
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    .line 120
    iget v0, p2, Landroid/graphics/RectF;->top:F

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 122
    :cond_5
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_6

    .line 123
    iget v0, p2, Landroid/graphics/RectF;->right:F

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 125
    :cond_6
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 126
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mFocus:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)F
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mRatio:F

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;F)V
    .locals 0

    .prologue
    .line 64
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mRatio:F

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method private construct(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x2

    .line 132
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    .line 133
    new-instance v4, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-direct {v4, p1}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    .line 134
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    if-nez v4, :cond_1

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->getPenInfoList()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    .line 139
    .local v3, "penInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;>;"
    if-eqz v3, :cond_0

    .line 143
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 150
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    const-string v5, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->createPen(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->defaultPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 161
    :goto_2
    const/4 v4, 0x5

    new-array v4, v4, [I

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    .line 162
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    aput v7, v4, v7

    .line 163
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    aput v6, v4, v8

    .line 164
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    aput v6, v4, v6

    .line 165
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    aput v6, v4, v9

    .line 166
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    const/4 v5, 0x4

    aput v9, v4, v5

    .line 168
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/engine/SpenEraser;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    .line 170
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCirclePaint:Landroid/graphics/Paint;

    .line 171
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 172
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCirclePaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 173
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCirclePaint:Landroid/graphics/Paint;

    const/high16 v5, -0x1000000

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 175
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    .line 176
    new-instance v4, Landroid/graphics/PointF;

    invoke-direct {v4}, Landroid/graphics/PointF;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mFocus:Landroid/graphics/PointF;

    .line 177
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mMatrix:Landroid/graphics/Matrix;

    .line 178
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->dstRect:Landroid/graphics/RectF;

    .line 179
    new-instance v4, Landroid/view/ScaleGestureDetector;

    new-instance v5, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;

    invoke-direct {v5, p0, v10}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;)V

    invoke-direct {v4, p1, v5}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 181
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->initHapticFeedback()V

    .line 182
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->registerPensoundSolution()V

    goto/16 :goto_0

    .line 143
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;

    .line 144
    .local v2, "penInfo":Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;

    invoke-direct {v0, v10}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;)V

    .line 145
    .local v0, "data":Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;
    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->penInfo:Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;

    .line 146
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 151
    .end local v0    # "data":Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;
    .end local v2    # "penInfo":Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;
    :catch_0
    move-exception v1

    .line 152
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v1}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto/16 :goto_2

    .line 153
    .end local v1    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v1

    .line 154
    .local v1, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v1}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto/16 :goto_2

    .line 155
    .end local v1    # "e":Ljava/lang/InstantiationException;
    :catch_2
    move-exception v1

    .line 156
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto/16 :goto_2

    .line 157
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v1

    .line 158
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2
.end method

.method private convertPenNameToMaxThicknessValue(Ljava/lang/String;)I
    .locals 3
    .param p1, "penName"    # Ljava/lang/String;

    .prologue
    .line 371
    const/4 v0, 0x0

    .line 372
    .local v0, "maxValue":I
    if-nez p1, :cond_0

    move v1, v0

    .line 387
    .end local v0    # "maxValue":I
    .local v1, "maxValue":I
    :goto_0
    return v1

    .line 375
    .end local v1    # "maxValue":I
    .restart local v0    # "maxValue":I
    :cond_0
    const-string v2, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 376
    :cond_1
    const/16 v0, 0x40

    :cond_2
    :goto_1
    move v1, v0

    .line 387
    .end local v0    # "maxValue":I
    .restart local v1    # "maxValue":I
    goto :goto_0

    .line 377
    .end local v1    # "maxValue":I
    .restart local v0    # "maxValue":I
    :cond_3
    const-string v2, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 378
    :cond_4
    const/16 v0, 0x20

    .line 379
    goto :goto_1

    :cond_5
    const-string v2, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 380
    :cond_6
    const/16 v0, 0x50

    .line 381
    goto :goto_1

    :cond_7
    const-string v2, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 382
    const/16 v0, 0x6c

    .line 383
    goto :goto_1

    :cond_8
    const-string v2, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    const-string v2, "Eraser"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 384
    :cond_9
    const/16 v0, 0x64

    goto :goto_1
.end method

.method private createBitmap(II)V
    .locals 5
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 187
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    .line 188
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 189
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    .line 192
    :cond_0
    :try_start_0
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    .line 193
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->defaultPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 194
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 196
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 203
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-nez v2, :cond_2

    .line 204
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->defaultPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    .line 209
    :cond_2
    :goto_1
    return-void

    .line 196
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;

    .line 197
    .local v0, "data":Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;
    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v3, :cond_1

    .line 198
    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 199
    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 206
    .end local v0    # "data":Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;
    :catch_0
    move-exception v1

    .line 207
    .local v1, "e":Ljava/lang/Exception;
    const/4 v2, 0x2

    const-string v3, "Bitmap failed to create."

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_1
.end method

.method private drawEraserIcon(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    .line 675
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCirclePoint:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCircleRadius:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 676
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCirclePoint:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCirclePoint:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCircleRadius:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 678
    :cond_0
    return-void
.end method

.method private initHapticFeedback()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 306
    const-string v2, "SpenSimpleView"

    const-string v3, "initHapticFeedback() - Start"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-nez v2, :cond_0

    .line 309
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 310
    .local v0, "dm":Landroid/util/DisplayMetrics;
    new-instance v2, Lcom/samsung/hapticfeedback/HapticEffect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mContext:Landroid/content/Context;

    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/hapticfeedback/HapticEffect;-><init>(Landroid/content/Context;II)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    .line 319
    .end local v0    # "dm":Landroid/util/DisplayMetrics;
    :cond_0
    :goto_0
    const-string v2, "SpenSimpleView"

    const-string v3, "initHapticFeedback() - End"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    return-void

    .line 311
    :catch_0
    move-exception v1

    .line 312
    .local v1, "error":Ljava/lang/UnsatisfiedLinkError;
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 313
    const-string v2, "TAG"

    const-string v3, "Haptic Effect UnsatisfiedLinkError"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 314
    .end local v1    # "error":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v1

    .line 315
    .local v1, "error":Ljava/lang/NoClassDefFoundError;
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 316
    const-string v2, "TAG"

    const-string v3, "Haptic Effect NoClassDefFoundError"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private onTouchGesture(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 300
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 303
    :cond_0
    return-void
.end method

.method private onTouchInputEraser(Landroid/view/MotionEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v5, -0x3d380000    # -100.0f

    const/4 v4, 0x0

    .line 249
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v0, v2, 0xff

    .line 251
    .local v0, "action":I
    if-nez v0, :cond_0

    .line 252
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->cancelTouch:Z

    .line 255
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->cancelTouch:Z

    if-eqz v2, :cond_1

    .line 297
    :goto_0
    return-void

    .line 259
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCirclePoint:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, v2, Landroid/graphics/PointF;->x:F

    .line 260
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCirclePoint:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, v2, Landroid/graphics/PointF;->y:F

    .line 262
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    if-eqz v2, :cond_3

    .line 263
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 264
    .local v1, "tmpRect":Landroid/graphics/RectF;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    .line 265
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v4, v4, v4}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 268
    :cond_2
    packed-switch v0, :pswitch_data_0

    .line 296
    .end local v1    # "tmpRect":Landroid/graphics/RectF;
    :cond_3
    :goto_1
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_0

    .line 270
    .restart local v1    # "tmpRect":Landroid/graphics/RectF;
    :pswitch_1
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->cancelTouch:Z

    .line 271
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    invoke-virtual {v2, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->startPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 272
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    goto :goto_1

    .line 275
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    invoke-virtual {v2, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->movePen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 276
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    invoke-direct {p0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->JoinRect(Landroid/graphics/Rect;Landroid/graphics/RectF;)V

    goto :goto_1

    .line 281
    :pswitch_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCirclePoint:Landroid/graphics/PointF;

    iput v5, v2, Landroid/graphics/PointF;->x:F

    .line 282
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCirclePoint:Landroid/graphics/PointF;

    iput v5, v2, Landroid/graphics/PointF;->y:F

    .line 283
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    invoke-virtual {v2, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->endPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 284
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    invoke-direct {p0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->JoinRect(Landroid/graphics/Rect;Landroid/graphics/RectF;)V

    goto :goto_1

    .line 287
    :pswitch_4
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCirclePoint:Landroid/graphics/PointF;

    iput v5, v2, Landroid/graphics/PointF;->x:F

    .line 288
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCirclePoint:Landroid/graphics/PointF;

    iput v5, v2, Landroid/graphics/PointF;->y:F

    .line 289
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    invoke-virtual {v2, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->endPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 290
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    invoke-direct {p0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->JoinRect(Landroid/graphics/Rect;Landroid/graphics/RectF;)V

    .line 291
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->cancelTouch:Z

    goto :goto_1

    .line 268
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private onTouchInputPen(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    .line 212
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v0, v2, 0xff

    .line 214
    .local v0, "action":I
    if-nez v0, :cond_0

    .line 215
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->cancelTouch:Z

    .line 218
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->cancelTouch:Z

    if-eqz v2, :cond_1

    .line 246
    :goto_0
    return-void

    .line 222
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v2, :cond_2

    .line 223
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 225
    .local v1, "tmpRect":Landroid/graphics/RectF;
    packed-switch v0, :pswitch_data_0

    .line 245
    .end local v1    # "tmpRect":Landroid/graphics/RectF;
    :cond_2
    :goto_1
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_0

    .line 227
    .restart local v1    # "tmpRect":Landroid/graphics/RectF;
    :pswitch_1
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->cancelTouch:Z

    .line 228
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v2, p1, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 229
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    goto :goto_1

    .line 232
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v2, p1, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 233
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    invoke-direct {p0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->JoinRect(Landroid/graphics/Rect;Landroid/graphics/RectF;)V

    goto :goto_1

    .line 236
    :pswitch_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v2, p1, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 237
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    invoke-direct {p0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->JoinRect(Landroid/graphics/Rect;Landroid/graphics/RectF;)V

    goto :goto_1

    .line 240
    :pswitch_4
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->cancelTouch:Z

    goto :goto_1

    .line 225
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private registerPensoundSolution()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 332
    const-string v1, "SpenSimpleView"

    const-string v2, "registerPensoundSolution() - Start"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    :try_start_0
    sget-boolean v1, Lcom/samsung/audio/SmpsManager;->isSupport:Z

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->bIsSupport:Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    .line 344
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->bIsSupport:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-nez v1, :cond_0

    .line 345
    new-instance v1, Lcom/samsung/audio/SmpsManager;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/audio/SmpsManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 346
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v1, :cond_0

    .line 347
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mIndexPencil:I

    .line 348
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mIndexMarker:I

    .line 349
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mIndexBrush:I

    .line 350
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mIndexEraser:I

    .line 351
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->activePen:I

    if-eq v1, v3, :cond_1

    .line 352
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->activePen:I

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 358
    :cond_0
    :goto_0
    const-string v1, "SpenSimpleView"

    const-string v2, "registerPensoundSolution() - End"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    :goto_1
    return-void

    .line 335
    :catch_0
    move-exception v0

    .line 336
    .local v0, "error":Ljava/lang/UnsatisfiedLinkError;
    const-string v1, "SpenSimpleView"

    const-string v2, "Smps is disabled in this model"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->bIsSupport:Z

    goto :goto_1

    .line 339
    .end local v0    # "error":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v0

    .line 340
    .local v0, "error":Ljava/lang/NoClassDefFoundError;
    const-string v1, "SpenSimpleView"

    const-string v2, "Smps is disabled in this model"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->bIsSupport:Z

    goto :goto_1

    .line 353
    .end local v0    # "error":Ljava/lang/NoClassDefFoundError;
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mIndexPencil:I

    if-eq v1, v3, :cond_0

    .line 354
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mIndexPencil:I

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    goto :goto_0
.end method

.method private releaseHapticFeedback()V
    .locals 2

    .prologue
    .line 323
    const-string v0, "SpenSimpleView"

    const-string v1, "releaseHapticFeedback() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    invoke-virtual {v0}, Lcom/samsung/hapticfeedback/HapticEffect;->closeDevice()V

    .line 326
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 328
    :cond_0
    const-string v0, "SpenSimpleView"

    const-string v1, "releaseHapticFeedback() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    return-void
.end method

.method private unregisterPensoundSolution()V
    .locals 2

    .prologue
    .line 362
    const-string v0, "SpenSimpleView"

    const-string/jumbo v1, "unregisterPensoundSolution() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->bIsSupport:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    invoke-virtual {v0}, Lcom/samsung/audio/SmpsManager;->onDestroy()V

    .line 365
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 367
    :cond_0
    const-string v0, "SpenSimpleView"

    const-string/jumbo v1, "unregisterPensoundSolution() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    return-void
.end method


# virtual methods
.method public captureCurrentView()Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 1063
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mWidth:I

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHeight:I

    if-nez v4, :cond_1

    :cond_0
    move-object v0, v3

    .line 1076
    :goto_0
    return-object v0

    .line 1066
    :cond_1
    const/4 v0, 0x0

    .line 1068
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mWidth:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHeight:I

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1073
    :goto_1
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1074
    .local v1, "canvas":Landroid/graphics/Canvas;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v4, v7, v7, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1075
    const/4 v1, 0x0

    .line 1076
    goto :goto_0

    .line 1069
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    :catch_0
    move-exception v2

    .line 1070
    .local v2, "e":Ljava/lang/Throwable;
    const-string v4, "SpenSimpleView"

    const-string v5, "Failed to create bitmap"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1071
    const/4 v4, 0x2

    const-string v5, " : fail createBitmap."

    invoke-static {v4, v5}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public clearScreen()V
    .locals 3

    .prologue
    .line 1088
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 1089
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1090
    .local v0, "canvas":Landroid/graphics/Canvas;
    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1091
    const/4 v0, 0x0

    .line 1092
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->invalidate()V

    .line 1094
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    :cond_0
    return-void
.end method

.method public close()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 468
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 469
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_4

    .line 478
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 479
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    .line 482
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->defaultPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v1, :cond_1

    .line 483
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->defaultPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 484
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->defaultPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->destroyPen(Lcom/samsung/android/sdk/pen/pen/SpenPen;)V

    .line 485
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->defaultPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    .line 486
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->close()V

    .line 487
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    .line 490
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    .line 491
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 492
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    .line 495
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    if-eqz v1, :cond_3

    .line 496
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->close()V

    .line 497
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    .line 500
    :cond_3
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCirclePaint:Landroid/graphics/Paint;

    .line 501
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCirclePoint:Landroid/graphics/PointF;

    .line 502
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 503
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 504
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    .line 505
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    .line 507
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .line 508
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 510
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mFocus:Landroid/graphics/PointF;

    .line 511
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mMatrix:Landroid/graphics/Matrix;

    .line 512
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->dstRect:Landroid/graphics/RectF;

    .line 513
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 515
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->releaseHapticFeedback()V

    .line 516
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->unregisterPensoundSolution()V

    .line 517
    return-void

    .line 469
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;

    .line 470
    .local v0, "data":Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    if-eqz v2, :cond_5

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v2, :cond_5

    .line 471
    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 472
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->destroyPen(Lcom/samsung/android/sdk/pen/pen/SpenPen;)V

    .line 473
    iput-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    .line 475
    :cond_5
    iput-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->penInfo:Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;

    goto :goto_0
.end method

.method public closeControl()V
    .locals 0

    .prologue
    .line 1170
    return-void
.end method

.method public getCanvasHeight()I
    .locals 1

    .prologue
    .line 1042
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHeight:I

    return v0
.end method

.method public getCanvasWidth()I
    .locals 1

    .prologue
    .line 1031
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mWidth:I

    return v0
.end method

.method public getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    .locals 1

    .prologue
    .line 855
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-nez v0, :cond_0

    .line 856
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 858
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    return-object v0
.end method

.method public getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .locals 1

    .prologue
    .line 804
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-nez v0, :cond_0

    .line 805
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 807
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    return-object v0
.end method

.method public getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    .locals 1

    .prologue
    .line 893
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSelectionSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;
    .locals 1

    .prologue
    .line 930
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 1

    .prologue
    .line 714
    const/4 v0, 0x0

    return-object v0
.end method

.method public getToolTypeAction(I)I
    .locals 1
    .param p1, "toolType"    # I

    .prologue
    .line 1014
    const/4 v0, 0x4

    if-gt p1, v0, :cond_0

    if-gez p1, :cond_1

    .line 1015
    :cond_0
    const/4 p1, 0x0

    .line 1017
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    if-eqz v0, :cond_2

    .line 1018
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    aget v0, v0, p1

    .line 1020
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 660
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 672
    :cond_0
    :goto_0
    return-void

    .line 663
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->dstRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 665
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 667
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->dstRect:Landroid/graphics/RectF;

    invoke-virtual {p1, v0, v4, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 668
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 670
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->drawEraserIcon(Landroid/graphics/Canvas;)V

    .line 671
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 648
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    invoke-interface {v0, p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;->onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 651
    :cond_0
    return v1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 538
    if-nez p1, :cond_0

    .line 539
    const/4 v9, 0x0

    .line 625
    :goto_0
    return v9

    .line 541
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    and-int/lit16 v0, v9, 0xff

    .line 542
    .local v0, "action":I
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v8

    .line 544
    .local v8, "toolType":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 545
    .local v2, "newX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 547
    .local v3, "newY":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v9

    const/4 v10, 0x2

    if-ge v9, v10, :cond_7

    .line 548
    const/4 v9, 0x2

    if-eq v8, v9, :cond_1

    const/4 v9, 0x3

    if-eq v8, v9, :cond_1

    const/4 v9, 0x4

    if-ne v8, v9, :cond_7

    .line 549
    :cond_1
    iget-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->bIsSupport:Z

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v9, :cond_6

    .line 550
    const/4 v9, 0x3

    if-eq v8, v9, :cond_2

    const/4 v9, 0x4

    if-ne v8, v9, :cond_3

    .line 552
    :cond_2
    if-nez v0, :cond_9

    .line 553
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mIndexEraser:I

    invoke-virtual {v9, v10}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 554
    const-string v9, "Eraser"

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->convertPenNameToMaxThicknessValue(Ljava/lang/String;)I

    move-result v1

    .line 555
    .local v1, "maxValue":I
    if-lez v1, :cond_3

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-eqz v9, :cond_3

    .line 556
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v10, v10, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    int-to-float v11, v1

    div-float/2addr v10, v11

    float-to-double v10, v10

    invoke-virtual {v9, v10, v11}, Lcom/samsung/audio/SmpsManager;->setThickness(D)Z

    .line 569
    .end local v1    # "maxValue":I
    :cond_3
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    .line 570
    .local v6, "tempX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    .line 571
    .local v7, "tempY":F
    move v4, v6

    .line 572
    .local v4, "originX":F
    move v5, v7

    .line 573
    .local v5, "originY":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    const/4 v10, 0x0

    cmpg-float v9, v9, v10

    if-gez v9, :cond_4

    .line 574
    const/4 v7, 0x0

    .line 576
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    const/4 v10, 0x0

    cmpg-float v9, v9, v10

    if-gez v9, :cond_5

    .line 577
    const/4 v6, 0x0

    .line 579
    :cond_5
    invoke-virtual {p1, v6, v7}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 580
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    invoke-virtual {v9, p1}, Lcom/samsung/audio/SmpsManager;->generateSound(Landroid/view/MotionEvent;)V

    .line 581
    invoke-virtual {p1, v4, v5}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 584
    .end local v4    # "originX":F
    .end local v5    # "originY":F
    .end local v6    # "tempX":F
    .end local v7    # "tempY":F
    :cond_6
    if-nez v0, :cond_a

    .line 585
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mOldX:F

    .line 586
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mOldY:F

    .line 603
    :cond_7
    :goto_2
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    if-eqz v9, :cond_8

    .line 604
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    aget v9, v9, v8

    packed-switch v9, :pswitch_data_0

    .line 621
    :cond_8
    :goto_3
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    if-eqz v9, :cond_d

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    invoke-interface {v9, p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 622
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 558
    :cond_9
    const/4 v9, 0x1

    if-ne v0, v9, :cond_3

    .line 559
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->activePen:I

    invoke-virtual {v9, v10}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 560
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-eqz v9, :cond_3

    .line 561
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v9, v9, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->convertPenNameToMaxThicknessValue(Ljava/lang/String;)I

    move-result v1

    .line 562
    .restart local v1    # "maxValue":I
    if-lez v1, :cond_3

    .line 563
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v10, v10, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    int-to-float v11, v1

    div-float/2addr v10, v11

    float-to-double v10, v10

    invoke-virtual {v9, v10, v11}, Lcom/samsung/audio/SmpsManager;->setThickness(D)Z

    goto :goto_1

    .line 587
    .end local v1    # "maxValue":I
    :cond_a
    const/4 v9, 0x2

    if-ne v0, v9, :cond_c

    .line 589
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-eqz v9, :cond_b

    .line 590
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mOldX:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mOldY:F

    invoke-virtual {v9, v10, v11, v2, v3}, Lcom/samsung/hapticfeedback/HapticEffect;->playEffectByDistance(FFFF)V

    .line 592
    :cond_b
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mOldX:F

    .line 593
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mOldY:F

    goto :goto_2

    .line 594
    :cond_c
    const/4 v9, 0x1

    if-ne v0, v9, :cond_7

    .line 597
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-eqz v9, :cond_7

    .line 598
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    invoke-virtual {v9}, Lcom/samsung/hapticfeedback/HapticEffect;->stopAllEffect()V

    goto :goto_2

    .line 606
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->onTouchInputPen(Landroid/view/MotionEvent;)V

    goto :goto_3

    .line 610
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->onTouchInputEraser(Landroid/view/MotionEvent;)V

    goto :goto_3

    .line 614
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->onTouchGesture(Landroid/view/MotionEvent;)V

    goto :goto_3

    .line 625
    :cond_d
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 604
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 447
    const-string v0, "SpenSimpleView"

    const-string v1, "onWindowFocusChanged() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    if-eqz p1, :cond_0

    .line 449
    const-string v0, "SpenSimpleView"

    const-string v1, "onWindowFocusChanged() - hasWindowFocus : true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->registerPensoundSolution()V

    .line 455
    :goto_0
    const-string v0, "SpenSimpleView"

    const-string v1, "onWindowFocusChanged() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    return-void

    .line 452
    :cond_0
    const-string v0, "SpenSimpleView"

    const-string v1, "onWindowFocusChanged() - hasWindowFocus : false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->unregisterPensoundSolution()V

    goto :goto_0
.end method

.method public setBackgroundColorChangeListener(Ljava/lang/Object;Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;)V
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "listener"    # Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .prologue
    .line 1183
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    if-eqz v0, :cond_0

    .line 1184
    const-string v0, "SpenSimpleView"

    const-string v1, "setBackgroundColorListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1185
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    .line 1187
    :cond_0
    return-void
.end method

.method public setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .prologue
    .line 827
    if-eqz p1, :cond_1

    .line 829
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 830
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 832
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->setSize(F)V

    .line 834
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 835
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-eqz v0, :cond_2

    .line 836
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mRatio:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mCircleRadius:F

    .line 838
    :cond_2
    return-void
.end method

.method public setHoverListener(Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .prologue
    .line 1127
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .line 1128
    return-void
.end method

.method public setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 6
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .prologue
    .line 732
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    if-eqz p1, :cond_1

    .line 733
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_5

    .line 772
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 774
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->bIsSupport:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-eqz v2, :cond_4

    .line 775
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v3, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v3, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 776
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v3, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 777
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v3, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 778
    :cond_2
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mIndexPencil:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->activePen:I

    .line 784
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->activePen:I

    invoke-virtual {v2, v3}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 785
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->convertPenNameToMaxThicknessValue(Ljava/lang/String;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    float-to-double v4, v3

    invoke-virtual {v2, v4, v5}, Lcom/samsung/audio/SmpsManager;->setThickness(D)Z

    .line 787
    :cond_4
    return-void

    .line 733
    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;

    .line 734
    .local v0, "data":Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;
    iget-object v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->penInfo:Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    .line 735
    iget v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_6

    .line 736
    const/high16 v3, 0x41200000    # 10.0f

    iput v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 739
    :cond_6
    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-nez v3, :cond_7

    .line 741
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->penInfo:Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->createPen(Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;)Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-result-object v3

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    .line 742
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_7

    .line 743
    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 756
    :cond_7
    :goto_2
    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v3, :cond_0

    .line 757
    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget v4, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setColor(I)V

    .line 758
    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget v4, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setSize(F)V

    .line 760
    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getPenAttribute(I)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 761
    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-boolean v4, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setCurveEnabled(Z)V

    .line 763
    :cond_8
    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getPenAttribute(I)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 764
    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v4, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setAdvancedSetting(Ljava/lang/String;)V

    .line 766
    :cond_9
    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    goto/16 :goto_0

    .line 745
    :catch_0
    move-exception v1

    .line 746
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v1}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 747
    .end local v1    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v1

    .line 748
    .local v1, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v1}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_2

    .line 749
    .end local v1    # "e":Ljava/lang/InstantiationException;
    :catch_2
    move-exception v1

    .line 750
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    .line 751
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v1

    .line 752
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 779
    .end local v0    # "data":Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_a
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v3, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v3, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 780
    :cond_b
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mIndexBrush:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->activePen:I

    goto/16 :goto_1

    .line 781
    :cond_c
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v3, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v3, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 782
    :cond_d
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mIndexMarker:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->activePen:I

    goto/16 :goto_1
.end method

.method public setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 0
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    .line 876
    return-void
.end method

.method public setSelectionSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V
    .locals 0
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    .prologue
    .line 913
    return-void
.end method

.method public setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 0
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    .line 697
    return-void
.end method

.method public setToolTypeAction(II)V
    .locals 1
    .param p1, "toolType"    # I
    .param p2, "action"    # I

    .prologue
    .line 969
    const/4 v0, 0x7

    if-gt p2, v0, :cond_0

    if-gez p2, :cond_1

    .line 970
    :cond_0
    const/4 p2, 0x0

    .line 972
    :cond_1
    const/4 v0, 0x4

    if-gt p1, v0, :cond_2

    if-gez p1, :cond_3

    .line 973
    :cond_2
    const/4 p1, 0x0

    .line 975
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    if-eqz v0, :cond_4

    .line 976
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    aput p2, v0, p1

    .line 978
    :cond_4
    return-void
.end method

.method public setTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .prologue
    .line 1110
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 1111
    return-void
.end method
