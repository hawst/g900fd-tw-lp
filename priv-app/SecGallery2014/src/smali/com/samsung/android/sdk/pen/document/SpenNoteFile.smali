.class public Lcom/samsung/android/sdk/pen/document/SpenNoteFile;
.super Ljava/lang/Object;
.source "SpenNoteFile.java"


# static fields
.field public static final ORIENTATION_LANDSCAPE:I = 0x1

.field public static final ORIENTATION_PORTRAIT:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method

.method private static native NoteFile_copy(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private static native NoteFile_getAppName(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private static native NoteFile_getAppVersion(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/StringBuffer;)Z
.end method

.method private static native NoteFile_getCoverImage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method private static native NoteFile_getFormatVersion(Ljava/lang/String;)I
.end method

.method private static native NoteFile_getOrientation(Ljava/lang/String;)I
.end method

.method private static native NoteFile_getSize(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)Z
.end method

.method private static native NoteFile_hasUnsavedData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Z
.end method

.method private static native NoteFile_isFavorite(Ljava/lang/String;)Z
.end method

.method private static native NoteFile_isLocked(Ljava/lang/String;)Z
.end method

.method private static native NoteFile_isValid(Ljava/lang/String;)Z
.end method

.method private static native NoteFile_lock(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private static native NoteFile_removeCache(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private static native NoteFile_removeNote(Ljava/lang/String;)Z
.end method

.method private static native NoteFile_setCoverImage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private static native NoteFile_setFavorite(Ljava/lang/String;Z)Z
.end method

.method private static native NoteFile_unlock(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public static copy(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "destFilePath"    # Ljava/lang/String;
    .param p1, "srcFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    .line 163
    invoke-static {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_copy(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 164
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 165
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 172
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 176
    :cond_0
    return-void

    .line 167
    :pswitch_1
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 169
    :pswitch_2
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "E_UNSUPPORTED_TYPE : ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 170
    const-string v3, "] does not correspond to the SPD file format"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 169
    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 165
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static getAppName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 413
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_getAppName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getAppVersion(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/StringBuffer;)V
    .locals 2
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "major"    # Ljava/lang/Integer;
    .param p2, "minor"    # Ljava/lang/Integer;
    .param p3, "patchName"    # Ljava/lang/StringBuffer;

    .prologue
    .line 436
    invoke-static {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_getAppVersion(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/StringBuffer;)Z

    move-result v0

    .line 437
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 438
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 440
    :cond_0
    return-void
.end method

.method public static getCoverImagePath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 306
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_getCoverImage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getFormatVersion(Ljava/lang/String;)I
    .locals 1
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 454
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_getFormatVersion(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getOrientation(Ljava/lang/String;)I
    .locals 1
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 323
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_getOrientation(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getSize(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 2
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "width"    # Ljava/lang/Integer;
    .param p2, "height"    # Ljava/lang/Integer;

    .prologue
    .line 392
    invoke-static {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_getSize(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)Z

    move-result v0

    .line 393
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 394
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 396
    :cond_0
    return-void
.end method

.method public static hasUnsavedData(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Long;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "lastEditedTime"    # Ljava/lang/Long;

    .prologue
    .line 342
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_hasUnsavedData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Z

    move-result v0

    return v0
.end method

.method private static isBuildTypeEngMode()Z
    .locals 2

    .prologue
    .line 484
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isFavorite(Ljava/lang/String;)Z
    .locals 1
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 225
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_isFavorite(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isLocked(Ljava/lang/String;)Z
    .locals 1
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 141
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_isLocked(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isValid(Ljava/lang/String;)Z
    .locals 1
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 240
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_isValid(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static lock(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    .line 65
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_lock(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 66
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 67
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 77
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 80
    :cond_0
    return-void

    .line 69
    :pswitch_1
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 74
    :pswitch_2
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "E_UNSUPPORTED_TYPE : ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 75
    const-string v3, "] does not correspond to the SPD file format"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 74
    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 67
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static removeCache(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 474
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_removeCache(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 475
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 481
    :cond_0
    :goto_0
    return-void

    .line 477
    :catch_0
    move-exception v0

    .line 478
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 479
    const-string v1, "SpenNoteFile"

    const-string v2, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static removeNote(Ljava/lang/String;)V
    .locals 4
    .param p0, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    .line 361
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_removeNote(Ljava/lang/String;)Z

    move-result v0

    .line 362
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 363
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 373
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 376
    :cond_0
    return-void

    .line 365
    :pswitch_1
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 370
    :pswitch_2
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "E_UNSUPPORTED_TYPE : ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 371
    const-string v3, "] does not correspond to the SPD file format"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 370
    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 363
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static setCoverImage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "imageFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    .line 272
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_setCoverImage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 273
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 274
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 284
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 287
    :cond_0
    return-void

    .line 276
    :pswitch_1
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 281
    :pswitch_2
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "E_UNSUPPORTED_TYPE : ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 282
    const-string v3, "] does not correspond to the SPD file format"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 281
    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 274
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static setFavorite(Ljava/lang/String;Z)V
    .locals 4
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "flag"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    .line 198
    invoke-static {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_setFavorite(Ljava/lang/String;Z)Z

    move-result v0

    .line 199
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 200
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 207
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 210
    :cond_0
    return-void

    .line 202
    :pswitch_1
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 204
    :pswitch_2
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "E_UNSUPPORTED_TYPE : ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 205
    const-string v3, "] does not correspond to the SPD file format"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 204
    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 200
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static unlock(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    .line 108
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->NoteFile_unlock(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 109
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 110
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 122
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 125
    :cond_0
    return-void

    .line 112
    :sswitch_0
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 114
    :sswitch_1
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;

    const-string v2, "E_INVALID_PASSWORD : the password is wrong"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 119
    :sswitch_2
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "E_UNSUPPORTED_TYPE : ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 120
    const-string v3, "] does not correspond to the SPD file format"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 119
    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 110
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0xd -> :sswitch_2
        0x11 -> :sswitch_1
    .end sparse-switch
.end method
