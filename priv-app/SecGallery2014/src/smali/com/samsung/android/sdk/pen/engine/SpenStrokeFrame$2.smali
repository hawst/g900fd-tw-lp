.class Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;
.super Ljava/lang/Object;
.source "SpenStrokeFrame.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->startCamera()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    .line 468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x0

    .line 473
    const/4 v0, 0x0

    .line 474
    .local v0, "drawStroke":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v5

    if-nez v5, :cond_0

    .line 475
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/4 v6, 0x1

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    .line 476
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v7

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    invoke-static {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V

    .line 477
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v0

    .line 484
    :goto_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->setChangeFrameButtonResources()V
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    .line 486
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v6

    invoke-virtual {v5, v6, v8}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 487
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 488
    .local v2, "relativePath":Landroid/graphics/Path;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v6

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertRelative(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v3

    .line 489
    .local v3, "relativeRect":Landroid/graphics/RectF;
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 490
    .local v1, "m":Landroid/graphics/Matrix;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v5

    sget-object v6, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v1, v5, v3, v6}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 491
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v5

    invoke-virtual {v5, v1, v2}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    .line 492
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createShapeMaskBitmap(Landroid/graphics/Path;)V
    invoke-static {v5, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V

    .line 494
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$17(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v7

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->drawPen(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    invoke-static {v5, v6, v0, v7, v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v4

    .line 496
    .local v4, "resultValue":Z
    if-nez v4, :cond_1

    .line 497
    const-string v5, "SpenStrokeFrame"

    const-string v6, "drawPen is failed"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/16 v6, 0x20

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    .line 508
    :goto_1
    return-void

    .line 479
    .end local v1    # "m":Landroid/graphics/Matrix;
    .end local v2    # "relativePath":Landroid/graphics/Path;
    .end local v3    # "relativeRect":Landroid/graphics/RectF;
    .end local v4    # "resultValue":Z
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v5, v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    .line 480
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v5

    .line 481
    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .line 480
    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    invoke-static {v7, v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v5

    invoke-static {v6, v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V

    .line 482
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v0

    goto/16 :goto_0

    .line 502
    .restart local v1    # "m":Landroid/graphics/Matrix;
    .restart local v2    # "relativePath":Landroid/graphics/Path;
    .restart local v3    # "relativeRect":Landroid/graphics/RectF;
    .restart local v4    # "resultValue":Z
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->setRect(Landroid/graphics/RectF;)V
    invoke-static {v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$20(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)V

    .line 504
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateChangeCameraButtonPosition()V
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    .line 505
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateChangeFrameButtonPosition()V
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$22(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    .line 506
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateChangeCameraButtonPosition()V
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    .line 507
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$23(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/ImageView;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$17(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method
