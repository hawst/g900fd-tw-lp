.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$12;
.super Ljava/lang/Object;
.source "SpenSettingTextLayout2.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$12;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    .line 574
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 578
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$12;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    if-eqz v1, :cond_1

    .line 579
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$12;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getCurrentTableIndex()I

    move-result v0

    .line 580
    .local v0, "index":I
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 581
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$12;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteRightButton:Landroid/view/View;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 582
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$12;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteRightButton:Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 585
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$12;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setBackColorTable(I)V

    .line 587
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$12;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$12;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setSelectBoxPos(I)V

    .line 589
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$12;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getCurrentTableIndex()I

    move-result v0

    .line 590
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 591
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$12;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteLeftButton:Landroid/view/View;

    const v2, 0x3e4ccccd    # 0.2f

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 592
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$12;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPaletteLeftButton:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 595
    .end local v0    # "index":I
    :cond_1
    return-void
.end method
