.class Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;
.super Ljava/lang/Object;
.source "SpenTextByteLengthFilter.java"

# interfaces
.implements Landroid/text/InputFilter;


# instance fields
.field private mByte:I

.field private mContext:Landroid/content/Context;

.field private mToastMessage:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "paramContext"    # Landroid/content/Context;
    .param p2, "length"    # I

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mByte:I

    .line 20
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mContext:Landroid/content/Context;

    .line 21
    return-void
.end method

.method private getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "strName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 68
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 69
    .local v1, "manager":Landroid/content/pm/PackageManager;
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v2

    .line 71
    .local v2, "sdkResources":Landroid/content/res/Resources;
    const-string/jumbo v5, "string"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, p1, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 72
    .local v3, "strID":I
    if-nez v3, :cond_0

    .line 80
    .end local v1    # "manager":Landroid/content/pm/PackageManager;
    .end local v2    # "sdkResources":Landroid/content/res/Resources;
    .end local v3    # "strID":I
    :goto_0
    return-object v4

    .line 75
    .restart local v1    # "manager":Landroid/content/pm/PackageManager;
    .restart local v2    # "sdkResources":Landroid/content/res/Resources;
    .restart local v3    # "strID":I
    :cond_0
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 76
    .local v4, "string":Ljava/lang/String;
    goto :goto_0

    .line 78
    .end local v1    # "manager":Landroid/content/pm/PackageManager;
    .end local v2    # "sdkResources":Landroid/content/res/Resources;
    .end local v3    # "strID":I
    .end local v4    # "string":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private showToast()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 85
    const-string/jumbo v1, "string_reached_maximum_input"

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, "msg":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 87
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mByte:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mToastMessage:Landroid/widget/Toast;

    if-nez v1, :cond_1

    .line 89
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mContext:Landroid/content/Context;

    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mToastMessage:Landroid/widget/Toast;

    .line 94
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mToastMessage:Landroid/widget/Toast;

    const/16 v2, 0x50

    const/16 v3, 0x96

    invoke-virtual {v1, v2, v4, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 95
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mToastMessage:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 97
    :cond_0
    return-void

    .line 91
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mToastMessage:Landroid/widget/Toast;

    invoke-virtual {v1, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mToastMessage:Landroid/widget/Toast;

    invoke-virtual {v1, v4}, Landroid/widget/Toast;->setDuration(I)V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method

.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 5
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    const/4 v1, 0x0

    .line 37
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 62
    :cond_0
    :goto_0
    return-object v1

    .line 40
    :cond_1
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mByte:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 41
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    .line 43
    :cond_2
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mByte:I

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v3

    sub-int v4, p6, p5

    sub-int/2addr v3, v4

    sub-int v0, v2, v3

    .line 45
    .local v0, "i":I
    if-gtz v0, :cond_3

    .line 46
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->showToast()V

    .line 47
    const-string v1, ""

    goto :goto_0

    .line 49
    :cond_3
    sub-int v2, p3, p2

    if-ge v0, v2, :cond_0

    .line 53
    add-int/2addr v0, p2

    .line 54
    add-int/lit8 v1, v0, -0x1

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 55
    add-int/lit8 v0, v0, -0x1

    .line 56
    if-ne v0, p2, :cond_4

    .line 57
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->showToast()V

    .line 58
    const-string v1, ""

    goto :goto_0

    .line 62
    :cond_4
    invoke-interface {p1, p2, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0
.end method

.method public getLimitLength()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mByte:I

    return v0
.end method

.method public setLimitLength(I)V
    .locals 0
    .param p1, "length"    # I

    .prologue
    .line 24
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mByte:I

    .line 25
    return-void
.end method
