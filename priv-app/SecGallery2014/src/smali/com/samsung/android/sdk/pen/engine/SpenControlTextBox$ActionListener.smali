.class public interface abstract Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
.super Ljava/lang/Object;
.source "SpenControlTextBox.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60c
    name = "ActionListener"
.end annotation


# virtual methods
.method public abstract onFocusChanged(Z)V
.end method

.method public abstract onMoreButtonDown(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
.end method

.method public abstract onSelectionChanged(II)Z
.end method

.method public abstract onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
.end method
