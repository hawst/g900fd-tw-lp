.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$6;
.super Ljava/lang/Object;
.source "SpenSettingDockingLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setTopBottomToZeroMarginItem()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 85
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v3

    .line 86
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 85
    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 87
    .local v2, "textParam":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v3, 0x0

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 88
    const/4 v3, 0x0

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 89
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 90
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    move-result-object v3

    .line 91
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 90
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 92
    .local v0, "eraserParam":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v3, 0x0

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 93
    const/4 v3, 0x0

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 94
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 95
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    move-result-object v3

    .line 96
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 95
    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 97
    .local v1, "selectionParam":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v3, 0x0

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 98
    const/4 v3, 0x0

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 99
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    .end local v0    # "eraserParam":Landroid/widget/LinearLayout$LayoutParams;
    .end local v1    # "selectionParam":Landroid/widget/LinearLayout$LayoutParams;
    .end local v2    # "textParam":Landroid/widget/LinearLayout$LayoutParams;
    :goto_0
    return-void

    .line 100
    :catch_0
    move-exception v3

    goto :goto_0
.end method
