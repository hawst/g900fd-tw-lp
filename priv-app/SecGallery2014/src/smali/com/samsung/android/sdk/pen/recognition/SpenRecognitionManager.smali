.class public abstract Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;
.super Ljava/lang/Object;
.source "SpenRecognitionManager.java"


# static fields
.field private static final INPUT_TYPE_INDEX:I = 0x0

.field private static final OUTPUT_TYPE_INDEX:I = 0x1


# instance fields
.field private mPluginList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    .line 20
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    .line 27
    if-nez p1, :cond_0

    .line 28
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "E_INVALID_ARG : parameter \'context\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    .line 31
    return-void
.end method


# virtual methods
.method protected close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 340
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    .line 341
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 342
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 345
    :cond_0
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    .line 347
    :cond_1
    return-void
.end method

.method protected createPluginObject(Landroid/content/Context;Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;Ljava/lang/String;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "info"    # Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;
    .param p3, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 298
    if-nez p2, :cond_0

    .line 299
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "E_INVALID_ARG : parameter \'info\' is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 302
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 303
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "E_INVALID_STATE : candidateList is not made yet"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 306
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 309
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 321
    new-instance v2, Ljava/lang/ClassNotFoundException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Can not find "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p2, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Recognition"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ClassNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 312
    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 314
    .local v1, "pluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    iget-object v2, p2, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->className:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 315
    iget-object v2, p2, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->name:Ljava/lang/String;

    iget-object v3, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->pluginNameUri:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p2, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->version:I

    iget v3, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    if-ne v2, v3, :cond_2

    .line 316
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    invoke-virtual {v2, p1, v1, p3}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->loadPlugin(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    return-object v2
.end method

.method protected destroyPluginObject(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;)V
    .locals 2
    .param p1, "pluginObject"    # Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 329
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    if-nez v0, :cond_0

    .line 330
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE: Recognition Manager is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->unloadPlugin(Ljava/lang/Object;)V

    .line 333
    return-void
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 355
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 357
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    .line 358
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 359
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 362
    :cond_0
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    .line 364
    :cond_1
    return-void
.end method

.method protected getInfoList(Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    if-nez v7, :cond_0

    .line 39
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "E_INVALID_STATE: Recognition Manager is null"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 41
    :cond_0
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    invoke-virtual {v7, p1}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getPluginList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 42
    .local v6, "typeMatchedList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    if-nez v6, :cond_1

    .line 43
    const/4 v2, 0x0

    .line 85
    :goto_0
    return-object v2

    .line 46
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 47
    .local v5, "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 49
    .local v2, "outputList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 52
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_4

    .line 80
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    .line 81
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 83
    :cond_3
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    goto :goto_0

    .line 55
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 57
    .local v3, "pluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    iget-object v7, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    const-string v8, "SpenRecognitionInterface"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 58
    iget-object v7, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->extraInfo:Ljava/lang/String;

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "extraInfoArray":[Ljava/lang/String;
    array-length v7, v0

    const/4 v8, 0x2

    if-lt v7, v8, :cond_2

    .line 63
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    new-instance v4, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;-><init>()V

    .line 67
    .local v4, "recognitionInfo":Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;
    iget-object v7, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->pluginNameUri:Ljava/lang/String;

    iput-object v7, v4, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->name:Ljava/lang/String;

    .line 68
    iget v7, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    iput v7, v4, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->version:I

    .line 69
    new-instance v7, Ljava/lang/StringBuilder;

    iget-object v8, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v4, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->className:Ljava/lang/String;

    .line 70
    iget-boolean v7, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->hasPrivateKey:Z

    iput-boolean v7, v4, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->hasPrivateKey:Z

    .line 71
    iget-object v7, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->iconImageUri:Ljava/lang/String;

    iput-object v7, v4, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->iconImageUri:Ljava/lang/String;

    .line 73
    const/4 v7, 0x0

    aget-object v7, v0, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, v4, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->inputType:I

    .line 74
    const/4 v7, 0x1

    aget-object v7, v0, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, v4, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->outputType:I

    .line 76
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method protected getInfoList(Ljava/lang/String;II)Ljava/util/List;
    .locals 11
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "inputType"    # I
    .param p3, "outputType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    if-nez v9, :cond_0

    .line 94
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string v10, "E_INVALID_STATE: Recognition Manager is null"

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 96
    :cond_0
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    invoke-virtual {v9, p1}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getPluginList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    .line 97
    .local v8, "typeMatchedList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    if-nez v8, :cond_1

    .line 98
    const/4 v2, 0x0

    .line 144
    :goto_0
    return-object v2

    .line 101
    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 102
    .local v7, "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 104
    .local v2, "outputList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;>;"
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 107
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_4

    .line 139
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_3

    .line 140
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 142
    :cond_3
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    goto :goto_0

    .line 110
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 112
    .local v3, "pluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    iget-object v9, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->extraInfo:Ljava/lang/String;

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 114
    .local v0, "extraInfoArray":[Ljava/lang/String;
    array-length v9, v0

    const/4 v10, 0x2

    if-lt v9, v10, :cond_2

    .line 118
    const/4 v9, 0x0

    aget-object v9, v0, v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 119
    .local v4, "pluginInputType":I
    const/4 v9, 0x1

    aget-object v9, v0, v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 121
    .local v5, "pluginOutputType":I
    if-ne p2, v4, :cond_2

    if-ne p3, v5, :cond_2

    .line 122
    iget-object v9, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    const-string v10, "SpenRecognitionInterface"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 123
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    new-instance v6, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;

    invoke-direct {v6}, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;-><init>()V

    .line 127
    .local v6, "recognitionInfo":Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;
    iget-object v9, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->pluginNameUri:Ljava/lang/String;

    iput-object v9, v6, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->name:Ljava/lang/String;

    .line 128
    iget v9, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    iput v9, v6, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->version:I

    .line 129
    new-instance v9, Ljava/lang/StringBuilder;

    iget-object v10, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->className:Ljava/lang/String;

    .line 130
    iput v4, v6, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->inputType:I

    .line 131
    iput v5, v6, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->outputType:I

    .line 132
    iget-boolean v9, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->hasPrivateKey:Z

    iput-boolean v9, v6, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->hasPrivateKey:Z

    .line 133
    iget-object v9, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->iconImageUri:Ljava/lang/String;

    iput-object v9, v6, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->iconImageUri:Ljava/lang/String;

    .line 135
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method

.method protected getInfoList(Ljava/lang/String;IILjava/lang/String;)Ljava/util/List;
    .locals 11
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "inputType"    # I
    .param p3, "outputType"    # I
    .param p4, "interfaceName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    if-nez v9, :cond_0

    .line 207
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string v10, "E_INVALID_STATE: Recognition Manager is null"

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 209
    :cond_0
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    invoke-virtual {v9, p1}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getPluginList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    .line 210
    .local v8, "typeMatchedList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    if-nez v8, :cond_1

    .line 211
    const/4 v2, 0x0

    .line 257
    :goto_0
    return-object v2

    .line 214
    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 215
    .local v7, "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 217
    .local v2, "outputList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;>;"
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 220
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_4

    .line 252
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_3

    .line 253
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 255
    :cond_3
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    goto :goto_0

    .line 223
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 225
    .local v3, "pluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    iget-object v9, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->extraInfo:Ljava/lang/String;

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 227
    .local v0, "extraInfoArray":[Ljava/lang/String;
    array-length v9, v0

    const/4 v10, 0x2

    if-lt v9, v10, :cond_2

    .line 231
    const/4 v9, 0x0

    aget-object v9, v0, v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 232
    .local v4, "pluginInputType":I
    const/4 v9, 0x1

    aget-object v9, v0, v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 234
    .local v5, "pluginOutputType":I
    if-ne p2, v4, :cond_2

    if-ne p3, v5, :cond_2

    .line 235
    iget-object v9, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    invoke-virtual {v9, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 236
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 238
    new-instance v6, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;

    invoke-direct {v6}, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;-><init>()V

    .line 240
    .local v6, "recognitionInfo":Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;
    iget-object v9, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->pluginNameUri:Ljava/lang/String;

    iput-object v9, v6, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->name:Ljava/lang/String;

    .line 241
    iget v9, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    iput v9, v6, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->version:I

    .line 242
    new-instance v9, Ljava/lang/StringBuilder;

    iget-object v10, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->className:Ljava/lang/String;

    .line 243
    iput v4, v6, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->inputType:I

    .line 244
    iput v5, v6, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->outputType:I

    .line 245
    iget-boolean v9, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->hasPrivateKey:Z

    iput-boolean v9, v6, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->hasPrivateKey:Z

    .line 246
    iget-object v9, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->iconImageUri:Ljava/lang/String;

    iput-object v9, v6, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->iconImageUri:Ljava/lang/String;

    .line 248
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method

.method protected getInfoList(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "interfaceName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    if-nez v7, :cond_0

    .line 153
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "E_INVALID_STATE: Recognition Manager is null"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 155
    :cond_0
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    invoke-virtual {v7, p1}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getPluginList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 156
    .local v6, "typeMatchedList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    if-nez v6, :cond_1

    .line 157
    const/4 v2, 0x0

    .line 198
    :goto_0
    return-object v2

    .line 160
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 161
    .local v5, "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 163
    .local v2, "outputList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 166
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_4

    .line 193
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    .line 194
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 196
    :cond_3
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    goto :goto_0

    .line 169
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 171
    .local v3, "pluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    iget-object v7, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    invoke-virtual {v7, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 172
    iget-object v7, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->extraInfo:Ljava/lang/String;

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 173
    .local v0, "extraInfoArray":[Ljava/lang/String;
    array-length v7, v0

    const/4 v8, 0x2

    if-lt v7, v8, :cond_2

    .line 177
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 179
    new-instance v4, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;-><init>()V

    .line 181
    .local v4, "recognitionInfo":Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;
    iget-object v7, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->pluginNameUri:Ljava/lang/String;

    iput-object v7, v4, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->name:Ljava/lang/String;

    .line 182
    iget v7, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    iput v7, v4, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->version:I

    .line 183
    new-instance v7, Ljava/lang/StringBuilder;

    iget-object v8, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v4, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->className:Ljava/lang/String;

    .line 184
    iget-boolean v7, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->hasPrivateKey:Z

    iput-boolean v7, v4, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->hasPrivateKey:Z

    .line 185
    iget-object v7, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->iconImageUri:Ljava/lang/String;

    iput-object v7, v4, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->iconImageUri:Ljava/lang/String;

    .line 186
    const/4 v7, 0x0

    aget-object v7, v0, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, v4, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->inputType:I

    .line 187
    const/4 v7, 0x1

    aget-object v7, v0, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, v4, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->outputType:I

    .line 189
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method protected getPrivateKeyHint(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;)Ljava/lang/String;
    .locals 5
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/InstantiationException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    .prologue
    .line 266
    if-nez p1, :cond_0

    .line 267
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "E_INVALID_ARG : parameter \'info\' is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 270
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 271
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "E_INVALID_STATE : candidateList is not made yet"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 274
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 277
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 289
    new-instance v2, Ljava/lang/ClassNotFoundException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Can not find "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Recognition"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ClassNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 280
    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 282
    .local v1, "pluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    iget-object v2, p1, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->className:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 283
    iget-object v2, p1, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->name:Ljava/lang/String;

    iget-object v3, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->pluginNameUri:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p1, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->version:I

    iget v3, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    if-ne v2, v3, :cond_2

    .line 284
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getPrivateKeyHint(Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
