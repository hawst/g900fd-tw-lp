.class Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
.super Landroid/widget/BaseAdapter;
.source "SpenPenPresetListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;,
        Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;
    }
.end annotation


# instance fields
.field private customDrawable:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;",
            ">;"
        }
    .end annotation
.end field

.field private final mBitmapPaint:Landroid/graphics/Paint;

.field protected mContext:Landroid/content/Context;

.field protected mCurrentSeleted:I

.field protected mCustom_imagepath:Ljava/lang/String;

.field protected mDeletePresetItem:Landroid/view/View$OnClickListener;

.field protected mFocusItemListener:Landroid/view/View$OnFocusChangeListener;

.field private mIsSwitchTab:Z

.field protected mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;",
            ">;"
        }
    .end annotation
.end field

.field protected mOnClickPresetItemListener:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;

.field protected mOnePT:F

.field protected mPenPluginInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

.field private final mRect:Landroid/graphics/RectF;

.field protected mScale:F

.field protected mSdkResources:Landroid/content/res/Resources;

.field mSecondViewEnter:I

.field protected mSelectLongPresetItem:Landroid/view/View$OnLongClickListener;

.field protected mSelectPresetItem:Landroid/view/View$OnClickListener;

.field protected mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;Ljava/lang/String;Ljava/util/ArrayList;F)V
    .locals 4
    .param p1, "paramContext"    # Landroid/content/Context;
    .param p2, "paramInt"    # I
    .param p4, "custom_imagepath"    # Ljava/lang/String;
    .param p6, "ratio"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;",
            ">;F)V"
        }
    .end annotation

    .prologue
    .local p3, "presetInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;>;"
    .local p5, "penPluginInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;>;"
    const/4 v3, -0x1

    .line 119
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 38
    const/4 v2, 0x0

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    .line 39
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mScale:F

    .line 40
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCustom_imagepath:Ljava/lang/String;

    .line 48
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCurrentSeleted:I

    .line 52
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSecondViewEnter:I

    .line 54
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mIsSwitchTab:Z

    .line 56
    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSelectPresetItem:Landroid/view/View$OnClickListener;

    .line 83
    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$2;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSelectLongPresetItem:Landroid/view/View$OnLongClickListener;

    .line 95
    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$3;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mDeletePresetItem:Landroid/view/View$OnClickListener;

    .line 107
    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mFocusItemListener:Landroid/view/View$OnFocusChangeListener;

    .line 442
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->customDrawable:Ljava/util/ArrayList;

    .line 121
    iput p6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mScale:F

    .line 122
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mContext:Landroid/content/Context;

    .line 123
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 125
    .local v1, "localDisplayMetrics":Landroid/util/DisplayMetrics;
    iget v2, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, p6

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    .line 126
    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 127
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    .line 128
    iput-object p4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCustom_imagepath:Ljava/lang/String;

    .line 129
    iput-object p5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    .line 130
    new-instance v2, Landroid/graphics/Paint;

    const/4 v3, 0x4

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mBitmapPaint:Landroid/graphics/Paint;

    .line 131
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mRect:Landroid/graphics/RectF;

    .line 133
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSdkResources:Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :goto_0
    return-void

    .line 134
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private bitmapResize(I)Landroid/graphics/drawable/Drawable;
    .locals 8
    .param p1, "index"    # I

    .prologue
    const/16 v7, 0x41

    .line 259
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenName()Ljava/lang/String;

    move-result-object v1

    .line 260
    .local v1, "presetName":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->customDrawable:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 265
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSdkResources:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPresetImageName()Ljava/lang/String;

    move-result-object v3

    const-string v5, "drawable"

    .line 266
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v6

    .line 265
    invoke-virtual {v4, v3, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 267
    .local v2, "resID":I
    if-nez v2, :cond_2

    .line 268
    const/4 v3, 0x0

    .line 270
    .end local v2    # "resID":I
    :goto_1
    return-object v3

    .line 261
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->customDrawable:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->getPenName()Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 262
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->customDrawable:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->getDrawable()Landroid/graphics/drawable/Drawable;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    goto :goto_1

    .line 260
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 270
    .restart local v2    # "resID":I
    :cond_2
    invoke-virtual {p0, v2, v7, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->resizeImage(III)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    goto :goto_1
.end method


# virtual methods
.method public OnClickPresetItemListener(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;)V
    .locals 0
    .param p1, "paramOnClickPresetItemListener"    # Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnClickPresetItemListener:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;

    .line 168
    return-void
.end method

.method public close()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 141
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mContext:Landroid/content/Context;

    .line 142
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 143
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 144
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    .line 146
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->customDrawable:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 147
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->customDrawable:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_3

    .line 153
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->customDrawable:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 154
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->customDrawable:Ljava/util/ArrayList;

    .line 157
    .end local v0    # "i":I
    :cond_1
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnClickPresetItemListener:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;

    .line 158
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 159
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 160
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    .line 162
    :cond_2
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCustom_imagepath:Ljava/lang/String;

    .line 163
    return-void

    .line 148
    .restart local v0    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->customDrawable:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;

    .line 149
    .local v1, "mImage":Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;
    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->close()V
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;)V

    .line 147
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 421
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 422
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 424
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 430
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 435
    int-to-long v0, p1

    return-wide v0
.end method

.method protected getPresetImage(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "presetName"    # Ljava/lang/String;

    .prologue
    .line 390
    const/4 v0, 0x0

    .line 391
    .local v0, "imgName":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    if-eqz v5, :cond_1

    .line 392
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 394
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;>;"
    :cond_1
    :goto_0
    move-object v2, v0

    .line 416
    :goto_1
    return-object v2

    .line 398
    .restart local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    .line 400
    .local v3, "penPluginInfo":Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;
    new-instance v4, Ljava/lang/String;

    const-string/jumbo v5, "urlInfo"

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 401
    .local v4, "selectImage":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v5

    iget-object v2, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->presetIconImageUri:Ljava/lang/String;

    .line 402
    .local v2, "localSelectImage":Ljava/lang/String;
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 403
    const-string v2, "pen_preset_brush"

    .line 404
    const/4 v4, 0x0

    .line 405
    goto :goto_1

    .line 408
    :cond_3
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 409
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v5

    iget-object v0, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->presetIconImageUri:Ljava/lang/String;

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 173
    if-nez p2, :cond_0

    .line 174
    new-instance v10, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCustom_imagepath:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mScale:F

    invoke-direct {v10, v11, v12, v13}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getPenPresetListRow()Landroid/view/View;

    move-result-object p2

    .line 176
    :cond_0
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSecondViewEnter:I

    move/from16 v0, p1

    if-ne v0, v10, :cond_5

    .line 177
    const/4 v10, 0x4

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 182
    :goto_0
    const v10, 0xb82e65

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    .line 183
    .local v8, "previewButton":Landroid/widget/ImageButton;
    const/4 v10, 0x1

    invoke-virtual {v8, v10}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 184
    const/4 v10, 0x1

    invoke-virtual {v8, v10}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 185
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 186
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSelectPresetItem:Landroid/view/View$OnClickListener;

    invoke-virtual {v8, v10}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSelectLongPresetItem:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v8, v10}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 189
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mFocusItemListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v8, v10}, Landroid/widget/ImageButton;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 191
    const v10, 0xb82e66

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    .line 193
    .local v3, "deleteButton":Landroid/widget/ImageButton;
    const/4 v10, 0x1

    invoke-virtual {v3, v10}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 194
    const/4 v10, 0x1

    invoke-virtual {v3, v10}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 195
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v3, v10}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 196
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mDeletePresetItem:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v10}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mFocusItemListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v3, v10}, Landroid/widget/ImageButton;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 200
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCurrentSeleted:I

    move/from16 v0, p1

    if-ne v0, v10, :cond_6

    .line 201
    const/4 v10, 0x1

    invoke-virtual {v8, v10}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 203
    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mIsSwitchTab:Z

    if-nez v10, :cond_1

    .line 204
    invoke-virtual {v8}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 212
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v11, "string_delete_preset"

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_2

    .line 213
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v11, "string_delete_preset"

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "%d"

    const-string v12, ""

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 217
    :cond_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    if-eqz v10, :cond_4

    .line 218
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    .line 219
    .local v7, "presetInfo_temp":Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;
    const v10, 0xb82ec9

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 220
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->bitmapResize(I)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 221
    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getFlag()Z

    move-result v10

    if-nez v10, :cond_3

    .line 222
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->presetPreview(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;)V

    .line 223
    const/4 v10, 0x1

    invoke-virtual {v7, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->setFlag(Z)V

    .line 226
    :cond_3
    const v10, 0xb82f2d

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 225
    check-cast v5, Landroid/widget/RelativeLayout;

    .line 227
    .local v5, "penAlphaPreview":Landroid/widget/RelativeLayout;
    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 228
    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 229
    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getColor()I

    move-result v10

    shr-int/lit8 v10, v10, 0x18

    and-int/lit16 v4, v10, 0xff

    .line 230
    .local v4, "penAlpha":I
    int-to-double v10, v4

    const-wide v12, 0x406fe00000000000L    # 255.0

    div-double/2addr v10, v12

    double-to-float v10, v10

    invoke-virtual {v5, v10}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 231
    const-wide v10, 0x400599999999999aL    # 2.7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenSize()F

    move-result v12

    const/high16 v13, 0x41900000    # 18.0f

    sub-float/2addr v12, v13

    const v13, 0x40c9999a    # 6.3f

    mul-float/2addr v12, v13

    const/high16 v13, 0x42280000    # 42.0f

    div-float/2addr v12, v13

    float-to-double v12, v12

    add-double/2addr v10, v12

    double-to-float v2, v10

    .line 233
    .local v2, "alphaWidth":F
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 234
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    const/high16 v11, 0x41f00000    # 30.0f

    mul-float/2addr v10, v11

    float-to-int v10, v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    mul-float/2addr v11, v2

    float-to-int v11, v11

    .line 233
    invoke-direct {v6, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 236
    .local v6, "penPreviewLayoutParams02":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    const/high16 v11, 0x41980000    # 19.0f

    mul-float/2addr v10, v11

    float-to-int v10, v10

    iput v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 237
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    float-to-double v10, v10

    const-wide v12, 0x4040400000000000L    # 32.5

    const/high16 v14, 0x40000000    # 2.0f

    div-float v14, v2, v14

    float-to-double v14, v14

    sub-double/2addr v12, v14

    mul-double/2addr v10, v12

    double-to-int v10, v10

    iput v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 239
    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 246
    .end local v2    # "alphaWidth":F
    .end local v4    # "penAlpha":I
    .end local v6    # "penPreviewLayoutParams02":Landroid/widget/RelativeLayout$LayoutParams;
    :goto_2
    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-virtual {v8, v10}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 247
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v11, "string_pen_preset"

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 248
    .local v9, "strPreset":Ljava/lang/String;
    if-eqz v9, :cond_4

    .line 249
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v11, "string_pen_preset"

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    .line 250
    add-int/lit8 v13, p1, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    .line 249
    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 255
    .end local v5    # "penAlphaPreview":Landroid/widget/RelativeLayout;
    .end local v7    # "presetInfo_temp":Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;
    .end local v9    # "strPreset":Ljava/lang/String;
    :cond_4
    return-object p2

    .line 179
    .end local v3    # "deleteButton":Landroid/widget/ImageButton;
    .end local v8    # "previewButton":Landroid/widget/ImageButton;
    :cond_5
    const/4 v10, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 207
    .restart local v3    # "deleteButton":Landroid/widget/ImageButton;
    .restart local v8    # "previewButton":Landroid/widget/ImageButton;
    :cond_6
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto/16 :goto_1

    .line 243
    .restart local v5    # "penAlphaPreview":Landroid/widget/RelativeLayout;
    .restart local v7    # "presetInfo_temp":Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;
    :cond_7
    const/16 v10, 0x8

    invoke-virtual {v5, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_2
.end method

.method protected presetPreview(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;)V
    .locals 25
    .param p1, "presetInfo"    # Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    .prologue
    .line 319
    if-eqz p1, :cond_0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    if-eqz v6, :cond_0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    if-nez v6, :cond_1

    .line 386
    :cond_0
    :goto_0
    return-void

    .line 323
    :cond_1
    const/16 v18, 0x0

    .line 325
    .local v18, "localBitmap":Landroid/graphics/Bitmap;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v18

    .line 326
    if-eqz v18, :cond_0

    .line 329
    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 330
    new-instance v19, Landroid/graphics/Canvas;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 332
    .local v19, "localCanvas":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v23

    .line 333
    .local v23, "penPluginIndex":I
    const/4 v6, -0x1

    move/from16 v0, v23

    if-eq v0, v6, :cond_0

    .line 336
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    if-nez v6, :cond_2

    .line 337
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->loadPenPlugin(Landroid/content/Context;Ljava/lang/String;)V

    .line 338
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 342
    :cond_2
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-interface {v6, v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 344
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    invoke-interface {v6}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getSize()F

    move-result v22

    .line 345
    .local v22, "mTempSize":F
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    invoke-interface {v6}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getColor()I

    move-result v21

    .line 346
    .local v21, "mTempColor":I
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenSize()F

    move-result v7

    invoke-interface {v6, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setSize(F)V

    .line 347
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getColor()I

    move-result v7

    invoke-interface {v6, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setColor(I)V

    .line 348
    const/high16 v6, 0x42040000    # 33.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    mul-float v20, v6, v7

    .line 351
    .local v20, "mPosY":F
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 352
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    .line 353
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getAdvancedSetting()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setAdvancedSetting(Ljava/lang/String;)V

    .line 355
    const/high16 v6, 0x42080000    # 34.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    mul-float v20, v6, v7

    .line 358
    :cond_3
    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 360
    const-wide/16 v2, 0x0

    .line 361
    .local v2, "time":J
    const/high16 v24, 0x41b00000    # 22.0f

    .line 362
    .local v24, "startX":F
    const/high16 v16, 0x422c0000    # 43.0f

    .line 363
    .local v16, "endX":F
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    const/high16 v7, 0x40000000    # 2.0f

    cmpg-float v6, v6, v7

    if-gez v6, :cond_4

    .line 364
    const/high16 v16, 0x42200000    # 40.0f

    .line 367
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 368
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    mul-float v7, v7, v24

    const/high16 v8, 0x42040000    # 33.0f

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    mul-float/2addr v8, v9

    .line 369
    const/high16 v9, 0x3f000000    # 0.5f

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenSize()F

    move-result v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-wide v4, v2

    .line 368
    invoke-static/range {v2 .. v15}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v17

    .line 370
    .local v17, "event":Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mRect:Landroid/graphics/RectF;

    move-object/from16 v0, v17

    invoke-interface {v6, v0, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 371
    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->recycle()V

    .line 372
    const-wide/16 v6, 0x5

    add-long v4, v2, v6

    .line 373
    .local v4, "currentTime":J
    const/4 v6, 0x2

    add-float v7, v24, v16

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    mul-float/2addr v7, v8

    .line 374
    const/high16 v9, 0x3f000000    # 0.5f

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenSize()F

    move-result v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move/from16 v8, v20

    .line 373
    invoke-static/range {v2 .. v15}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v17

    .line 375
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mRect:Landroid/graphics/RectF;

    move-object/from16 v0, v17

    invoke-interface {v6, v0, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 376
    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->recycle()V

    .line 377
    const-wide/16 v6, 0xa

    add-long v4, v2, v6

    .line 378
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    mul-float v7, v7, v16

    const/high16 v8, 0x42040000    # 33.0f

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    mul-float/2addr v8, v9

    const/high16 v9, 0x3f000000    # 0.5f

    .line 379
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenSize()F

    move-result v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    .line 378
    invoke-static/range {v2 .. v15}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v17

    .line 380
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mRect:Landroid/graphics/RectF;

    move-object/from16 v0, v17

    invoke-interface {v6, v0, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 381
    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->recycle()V

    .line 382
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mBitmapPaint:Landroid/graphics/Paint;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v6, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 384
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move/from16 v0, v22

    invoke-interface {v6, v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setSize(F)V

    .line 385
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move/from16 v0, v21

    invoke-interface {v6, v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setColor(I)V

    goto/16 :goto_0
.end method

.method public resizeImage(III)Landroid/graphics/drawable/Drawable;
    .locals 14
    .param p1, "resId"    # I
    .param p2, "iconWidth"    # I
    .param p3, "iconHeight"    # I

    .prologue
    .line 277
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v13

    .line 278
    .local v13, "stream":Ljava/io/InputStream;
    invoke-static {v13}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 281
    .local v1, "BitmapOrg":Landroid/graphics/Bitmap;
    if-nez v1, :cond_0

    .line 282
    const/4 v2, 0x0

    .line 306
    :goto_0
    return-object v2

    .line 284
    :cond_0
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 285
    .local v4, "width":I
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 286
    .local v5, "height":I
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    move/from16 v0, p2

    int-to-float v3, v0

    mul-float/2addr v2, v3

    float-to-int v9, v2

    .line 287
    .local v9, "newWidth":I
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    move/from16 v0, p3

    int-to-float v3, v0

    mul-float/2addr v2, v3

    float-to-int v8, v2

    .line 290
    .local v8, "newHeight":I
    int-to-float v2, v9

    int-to-float v3, v4

    div-float v12, v2, v3

    .line 291
    .local v12, "scaleWidth":F
    int-to-float v2, v8

    int-to-float v3, v5

    div-float v11, v2, v3

    .line 294
    .local v11, "scaleHeight":F
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 296
    .local v6, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v6, v12, v11}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 302
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x1

    invoke-static/range {v1 .. v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 306
    .local v10, "resizedBitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSdkResources:Landroid/content/res/Resources;

    invoke-direct {v2, v3, v10}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public setPenPlugin(Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;)V
    .locals 1
    .param p1, "penPluginManager"    # Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    .prologue
    .line 312
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    .line 313
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginInfoList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    .line 314
    return-void
.end method

.method protected setPresetImageDrawable(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1, "penName"    # Ljava/lang/String;
    .param p2, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 445
    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    .line 446
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->customDrawable:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 453
    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;

    invoke-direct {v2, p2, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    .line 454
    .local v2, "mCustomPresetImage":Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->customDrawable:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 465
    .end local v1    # "i":I
    .end local v2    # "mCustomPresetImage":Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->notifyDataSetChanged()V

    .line 466
    :goto_1
    return-void

    .line 447
    .restart local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->customDrawable:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->getPenName()Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 448
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->customDrawable:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->setDrawable(Landroid/graphics/drawable/Drawable;)V
    invoke-static {v3, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;Landroid/graphics/drawable/Drawable;)V

    .line 449
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->notifyDataSetChanged()V

    goto :goto_1

    .line 446
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 455
    .end local v1    # "i":I
    :cond_3
    if-eqz p1, :cond_0

    .line 456
    const/4 v0, 0x0

    .line 457
    .local v0, "count":I
    :goto_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->customDrawable:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 458
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->customDrawable:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->getPenName()Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 459
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->customDrawable:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_2

    .line 461
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method protected setSwitchTabFlag(Z)V
    .locals 0
    .param p1, "isSwitch"    # Z

    .prologue
    .line 439
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mIsSwitchTab:Z

    .line 440
    return-void
.end method
