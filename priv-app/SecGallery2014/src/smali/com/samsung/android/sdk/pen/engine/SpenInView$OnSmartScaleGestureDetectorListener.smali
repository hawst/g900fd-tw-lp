.class Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;
.super Ljava/lang/Object;
.source "SpenInView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenInView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnSmartScaleGestureDetectorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 0

    .prologue
    .line 5982
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;)V
    .locals 0

    .prologue
    .line 5982
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    return-void
.end method


# virtual methods
.method public onChangePan(FF)V
    .locals 7
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 5995
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 6006
    :goto_0
    return-void

    .line 5998
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 5999
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getStyle()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 6000
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    .line 6003
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v2

    const/4 v6, 0x1

    move v4, p1

    move v5, p2

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPan(JFFZ)V
    invoke-static/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenInView;JFFZ)V

    .line 6005
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateNotepad()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$48(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    goto :goto_0
.end method

.method public onChangeScale(FFF)V
    .locals 2
    .param p1, "centerX"    # F
    .param p2, "centerY"    # F
    .param p3, "ratio"    # F

    .prologue
    .line 5985
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5986
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getStyle()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 5987
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    .line 5990
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoom(FFF)V

    .line 5991
    return-void
.end method

.method public onFlick(I)Z
    .locals 3
    .param p1, "direction"    # I

    .prologue
    .line 6010
    const-string v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onFlick direction = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6011
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$49(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$49(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6012
    const/4 v0, 0x1

    .line 6017
    :goto_0
    return v0

    .line 6014
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$50(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 6015
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$50(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;->onFlick(I)Z

    move-result v0

    goto :goto_0

    .line 6017
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onUpdate(Z)V
    .locals 3
    .param p1, "isScreenFramebuffer"    # Z

    .prologue
    const/4 v2, 0x0

    .line 6022
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEditableTextBox()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6023
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v1, 0x0

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V
    invoke-static {v0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$43(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;Z)V

    .line 6027
    :goto_0
    return-void

    .line 6025
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V
    invoke-static {v0, v2, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$43(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;Z)V

    goto :goto_0
.end method

.method public onUpdateScreenFrameBuffer()V
    .locals 1

    .prologue
    .line 6031
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreenFrameBuffer()V

    .line 6032
    return-void
.end method
