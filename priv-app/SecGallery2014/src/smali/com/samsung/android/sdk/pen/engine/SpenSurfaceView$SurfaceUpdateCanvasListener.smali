.class Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;
.super Ljava/lang/Object;
.source "SpenSurfaceView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SurfaceUpdateCanvasListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)V
    .locals 0

    .prologue
    .line 3112
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdateCanvas(Landroid/graphics/RectF;Z)V
    .locals 24
    .param p1, "rectf"    # Landroid/graphics/RectF;
    .param p2, "isScreenFramebuffer"    # Z

    .prologue
    .line 3115
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v18

    if-nez v18, :cond_1

    .line 3188
    :cond_0
    :goto_0
    return-void

    .line 3119
    :cond_1
    const/4 v10, 0x0

    .line 3120
    .local v10, "dstRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->GetPageEffectWorking()Z

    move-result v18

    if-nez v18, :cond_5

    .line 3121
    if-eqz p1, :cond_3

    .line 3122
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->width()F

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->GetScreenWidth()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    cmpl-float v18, v18, v19

    if-nez v18, :cond_2

    .line 3123
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->height()F

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->GetScreenHeight()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    cmpl-float v18, v18, v19

    if-eqz v18, :cond_3

    .line 3124
    :cond_2
    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->GetRtoBmpItstScrWidth()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    .line 3125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->GetRtoBmpItstScrHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    .line 3124
    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->intersect(FFFF)Z

    move-result v18

    .line 3125
    if-eqz v18, :cond_0

    .line 3128
    if-nez p2, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomRatio()F

    move-result v18

    const/high16 v19, 0x3f800000    # 1.0f

    cmpl-float v18, v18, v19

    if-eqz v18, :cond_3

    .line 3129
    const/16 p1, 0x0

    .line 3134
    :cond_3
    if-eqz p1, :cond_5

    .line 3135
    new-instance v10, Landroid/graphics/Rect;

    .end local v10    # "dstRect":Landroid/graphics/Rect;
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->floor(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v19, v0

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->floor(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v0, v0

    move/from16 v19, v0

    .line 3136
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v21, v0

    .line 3135
    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-direct {v10, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3138
    .restart local v10    # "dstRect":Landroid/graphics/Rect;
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->width()F

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->GetScreenWidth()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    cmpl-float v18, v18, v19

    if-nez v18, :cond_4

    .line 3139
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->height()F

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->GetScreenHeight()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    cmpl-float v18, v18, v19

    if-eqz v18, :cond_5

    .line 3140
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->GetScreenStartX()I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->GetScreenStartY()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 3145
    :cond_5
    const/4 v7, 0x0

    .line 3146
    .local v7, "copyDstRect":Landroid/graphics/Rect;
    if-eqz v10, :cond_6

    .line 3147
    new-instance v7, Landroid/graphics/Rect;

    .end local v7    # "copyDstRect":Landroid/graphics/Rect;
    invoke-direct {v7, v10}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 3150
    .restart local v7    # "copyDstRect":Landroid/graphics/Rect;
    :cond_6
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v14

    .line 3152
    .local v14, "time":J
    move-wide/from16 v16, v14

    .local v16, "unlcokCanvasTime":J
    move-wide v8, v14

    .local v8, "drawTime":J
    move-wide v12, v14

    .line 3154
    .local v12, "lockCanvasTime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v11

    .line 3155
    .local v11, "holder":Landroid/view/SurfaceHolder;
    const/4 v6, 0x0

    .line 3156
    .local v6, "canvas":Landroid/graphics/Canvas;
    monitor-enter v11

    .line 3157
    :try_start_0
    invoke-interface {v11, v10}, Landroid/view/SurfaceHolder;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v6

    if-nez v6, :cond_7

    .line 3158
    const-string v18, "SpenSurfaceView"

    const-string v19, "Performance onUpdateCanvas lockCanvas return null"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3159
    monitor-exit v11

    goto/16 :goto_0

    .line 3156
    :catchall_0
    move-exception v18

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v18

    .line 3161
    :cond_7
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v12

    .line 3163
    if-eqz v10, :cond_9

    .line 3164
    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    iget v0, v7, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    iget v0, v7, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 3165
    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v18, v0

    iget v0, v7, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    iget v0, v7, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_9

    .line 3166
    :cond_8
    const-string v18, "SpenSurfaceView"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "Performance lockCanvas warning: originDirtyRect ("

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v7, Landroid/graphics/Rect;->left:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 3167
    iget v0, v7, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ") ("

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    iget v0, v7, Landroid/graphics/Rect;->right:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    iget v0, v7, Landroid/graphics/Rect;->bottom:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ") w = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 3168
    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " h = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 3166
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3169
    const-string v18, "SpenSurfaceView"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "Performance lockCanvas warning: outDirtyRect ("

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 3170
    const-string v20, ") ("

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ") w = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " h = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 3171
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 3169
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3172
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 3176
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v6, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->UpdateCanvas(Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V

    .line 3177
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 3179
    if-eqz v6, :cond_a

    .line 3180
    invoke-interface {v11, v6}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 3182
    :cond_a
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v16

    .line 3184
    const-string v18, "SpenSurfaceView"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "Performance total = "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v20

    sub-long v20, v20, v14

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " ms lockCanvasTime = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 3185
    sub-long v20, v12, v14

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " ms drawTime = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    sub-long v20, v8, v12

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 3186
    const-string v20, " ms unlcokCanvasTime = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    sub-long v20, v16, v8

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " ms"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 3184
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3156
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method
