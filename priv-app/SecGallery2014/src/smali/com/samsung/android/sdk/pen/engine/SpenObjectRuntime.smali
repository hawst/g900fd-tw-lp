.class public Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;
.super Ljava/lang/Object;
.source "SpenObjectRuntime.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;
    }
.end annotation


# static fields
.field private static mGlobalObjectRuntimeStart:Z


# instance fields
.field private final mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

.field private mThisObjectRuntimeStart:Z

.field private mUpdateListener:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;)V
    .locals 2
    .param p1, "objectRuntimeObject"    # Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    if-nez p1, :cond_0

    .line 83
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "E_INVALID_ARG : parameter \'objectRuntimeObject\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

    .line 86
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;)Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mUpdateListener:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;

    return-object v0
.end method

.method static synthetic access$1(Z)V
    .locals 0

    .prologue
    .line 18
    sput-boolean p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mGlobalObjectRuntimeStart:Z

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;Z)V
    .locals 0

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mThisObjectRuntimeStart:Z

    return-void
.end method


# virtual methods
.method getObjectRuntimeObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;->getType()I

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;->onTouchEvent(Landroid/view/MotionEvent;)V

    .line 212
    return-void
.end method

.method public setListener(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;

    .prologue
    .line 261
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mUpdateListener:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;

    .line 262
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mUpdateListener:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;)V

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;->setListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;)Z

    .line 288
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;->setListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;)Z

    goto :goto_0
.end method

.method public setRect(Landroid/graphics/RectF;)V
    .locals 1
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 222
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;->setRect(Landroid/graphics/RectF;)V

    .line 223
    return-void
.end method

.method public start(Ljava/lang/Object;Landroid/graphics/RectF;Landroid/graphics/PointF;FLandroid/graphics/PointF;Landroid/view/ViewGroup;)V
    .locals 7
    .param p1, "objectBase"    # Ljava/lang/Object;
    .param p2, "relativeRect"    # Landroid/graphics/RectF;
    .param p3, "pan"    # Landroid/graphics/PointF;
    .param p4, "zoomRatio"    # F
    .param p5, "frameStartPosition"    # Landroid/graphics/PointF;
    .param p6, "layout"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v1, 0x1

    .line 175
    if-eqz p1, :cond_0

    if-eqz p6, :cond_0

    if-eqz p2, :cond_0

    if-nez p5, :cond_1

    .line 176
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Argument is null. ObjectBase = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Rect = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 177
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ViewGroup = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " startFramePosition = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 176
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_1
    sget-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mGlobalObjectRuntimeStart:Z

    if-eqz v0, :cond_2

    .line 180
    const-string v0, "SpenObjectRuntime"

    const-string v1, "SpenObjectRuntime was already started"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    :goto_0
    return-void

    .line 183
    :cond_2
    sput-boolean v1, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mGlobalObjectRuntimeStart:Z

    .line 184
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mThisObjectRuntimeStart:Z

    .line 185
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;->start(Ljava/lang/Object;Landroid/graphics/RectF;Landroid/graphics/PointF;FLandroid/graphics/PointF;Landroid/view/ViewGroup;)V

    goto :goto_0
.end method

.method public stop(Z)V
    .locals 2
    .param p1, "cancel"    # Z

    .prologue
    const/4 v1, 0x0

    .line 196
    sget-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mGlobalObjectRuntimeStart:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mThisObjectRuntimeStart:Z

    if-eqz v0, :cond_0

    .line 197
    sput-boolean v1, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mGlobalObjectRuntimeStart:Z

    .line 198
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mThisObjectRuntimeStart:Z

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;->stop(Z)V

    .line 201
    return-void
.end method
