.class public Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
.super Ljava/lang/Object;
.source "SpenContextMenu.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;,
        Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_HEIGHT:I = 0x3d

.field private static final DEFAULT_HEIGHT_N1:I = 0x48

.field private static final DEFAULT_HEIGHT_TAB_A:I = 0x4c

.field private static final DEFAULT_HEIGHT_VIENNA:I = 0x50

.field protected static final DEFAULT_ITEM_WIDTH:I = 0x48

.field protected static final DEFAULT_ITEM_WIDTH_N1:I = 0x62

.field private static final DEFAULT_ITEM_WIDTH_TAB_A:I = 0x56

.field private static final DEFAULT_MINIMUM_DELAY:I = 0x3e8

.field protected static final DEFAULT_SEPERATOR_WIDTH:I = 0x1

.field protected static final DEFAULT_SEPERATOR_WIDTH_N1:I = 0x1

.field private static final DEFAULT_SEPERATOR_WIDTH_TAB_A:I = 0x1

.field private static final DEFAULT_TEXT_SIZE:I = 0xc

.field private static final DEFAULT_TEXT_SIZE_N1:I = 0x11

.field private static final DEFAULT_TEXT_SIZE_TAB_A:I = 0xe

.field private static final DEFAULT_WIDTH:I = 0x136

.field private static final MAX_RATE_PER_WIDTH:F = 0.9f

.field protected static final QUICK_POPUP_BG:Ljava/lang/String; = "quick_popup_bg"

.field protected static final QUICK_POPUP_BG_PRESS:Ljava/lang/String; = "quick_popup_bg_press"

.field protected static final QUICK_POPUP_BG_PRESS_VIENNA:Ljava/lang/String; = "quick_popup_bg_press_vienna"

.field protected static final QUICK_POPUP_DV:Ljava/lang/String; = "quick_popup_dv"

.field protected static final QUICK_POPUP_DV_VIENNA:Ljava/lang/String; = "quick_popup_dv_vienna"

.field private static final SDK_VERSION:I

.field protected static final SHADOW_BORDER:Ljava/lang/String; = "shadow_border"

.field protected static final SHADOW_BORDER_N1:Ljava/lang/String; = "shadow_border_n1"

.field protected static final SHADOW_BORDER_VIENNA:Ljava/lang/String; = "shadow_border_vienna"

.field protected static final TW_QUICK_BUBBLE_DIVIDER_HOLO_LIGHT:Ljava/lang/String; = "tw_quick_bubble_divider_holo_light"

.field public static final TYPE_DEFAULT:I = 0x0

.field public static final TYPE_WIDE:I = 0x1

.field private static mType:I


# instance fields
.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private final mContext:Landroid/content/Context;

.field private mCount:F

.field private mDelay:I

.field private final mHandler:Landroid/os/Handler;

.field private mHorizontalScrollView:Landroid/widget/HorizontalScrollView;

.field private mIsViennaModel:Z

.field private mItemHeight:I

.field private mItemSize:I

.field private mItemWidth:I

.field mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

.field private mNumDisplayedItem:I

.field private final mOnButtonClicked:Landroid/view/View$OnClickListener;

.field private mParent:Landroid/view/View;

.field private mPopupView:Landroid/view/View;

.field private mPopupWindow:Landroid/widget/PopupWindow;

.field private mPrevRect:Landroid/graphics/Rect;

.field private mRect:Landroid/graphics/Rect;

.field private mResourceSDK:Landroid/content/res/Resources;

.field private mScrollFlag:Z

.field private mScrollTimer:Ljava/util/Timer;

.field private mSelectListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

.field private mSeperatorWidth:I

.field private mTextColor:I

.field private mTextSize:I

.field private mTimer:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 107
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I

    .line 121
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Ljava/util/ArrayList;Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;)V
    .locals 30
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parent"    # Landroid/view/View;
    .param p4, "selectListener"    # Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/View;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;",
            ">;",
            "Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 275
    .local p3, "menu":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 115
    const/16 v25, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSelectListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

    .line 116
    const/16 v25, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    .line 117
    new-instance v25, Landroid/os/Handler;

    invoke-direct/range {v25 .. v25}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHandler:Landroid/os/Handler;

    .line 118
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mDelay:I

    .line 119
    const/16 v25, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    .line 120
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollFlag:Z

    .line 627
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mCount:F

    .line 728
    new-instance v25, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$1;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mOnButtonClicked:Landroid/view/View$OnClickListener;

    .line 277
    if-nez p3, :cond_0

    .line 278
    const/16 v25, 0x7

    const-string v26, " The list of menu items must be not null"

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 281
    :cond_0
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    .line 282
    if-eqz p1, :cond_9

    .line 283
    const/16 v21, 0x0

    .line 284
    .local v21, "res":Landroid/content/res/Resources;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    .line 285
    invoke-virtual/range {v21 .. v21}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v10

    .line 287
    .local v10, "dm":Landroid/util/DisplayMetrics;
    const-string v25, "accessibility"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/view/accessibility/AccessibilityManager;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 290
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z

    .line 293
    iget v0, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v25, v0

    iget v0, v10, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_a

    .line 294
    iget v5, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 299
    .local v5, "canvasWidth":I
    :goto_0
    const/16 v25, 0x640

    move/from16 v0, v25

    if-ne v5, v0, :cond_1

    iget v0, v10, Landroid/util/DisplayMetrics;->density:F

    move/from16 v25, v0

    const/high16 v26, 0x40000000    # 2.0f

    cmpl-float v25, v25, v26

    if-nez v25, :cond_1

    .line 300
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z

    .line 303
    :cond_1
    new-instance v25, Landroid/graphics/Rect;

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x136

    const/16 v29, 0x3d

    invoke-direct/range {v25 .. v29}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    .line 304
    new-instance v25, Landroid/graphics/Rect;

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x136

    const/16 v29, 0x3d

    invoke-direct/range {v25 .. v29}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPrevRect:Landroid/graphics/Rect;

    .line 306
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v16

    .line 308
    .local v16, "manager":Landroid/content/pm/PackageManager;
    :try_start_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    :goto_1
    new-instance v25, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    const/16 v26, 0x0

    invoke-direct/range {v25 .. v26}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    .line 314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;Ljava/util/ArrayList;)V

    .line 316
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSelectListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

    .line 318
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    .line 319
    const/16 v25, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupView:Landroid/view/View;

    .line 321
    new-instance v15, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p1

    invoke-direct {v15, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 322
    .local v15, "linearLayout":Landroid/widget/RelativeLayout;
    const/16 v22, 0x0

    .line 323
    .local v22, "shadowImage":Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    move-object/from16 v25, v0

    if-eqz v25, :cond_2

    .line 324
    sget v25, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_c

    .line 326
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z

    move/from16 v25, v0

    if-eqz v25, :cond_b

    .line 327
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    move-object/from16 v25, v0

    const-string v26, "quick_popup_bg"

    const-string v27, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v25 .. v28}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v13

    .line 332
    .local v13, "id":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v22

    .line 338
    .end local v13    # "id":I
    :cond_2
    :goto_3
    sget v25, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I

    const/16 v26, 0x10

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_d

    if-eqz v22, :cond_d

    .line 339
    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/RelativeLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 344
    :cond_3
    :goto_4
    const/16 v23, 0x136

    .line 345
    .local v23, "width":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v25, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v25 .. v25}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemSize:I

    .line 346
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    .line 347
    const/4 v9, 0x5

    .line 349
    .local v9, "delta_padding":I
    sget v25, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_e

    .line 350
    const/16 v25, 0x62

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    .line 351
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSeperatorWidth:I

    .line 352
    const/16 v25, 0x48

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemHeight:I

    .line 353
    const/16 v25, 0x11

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTextSize:I

    .line 354
    const-string v25, "#1e1e1e"

    invoke-static/range {v25 .. v25}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTextColor:I

    .line 356
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z

    move/from16 v25, v0

    if-eqz v25, :cond_4

    .line 357
    const/16 v25, 0x50

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemHeight:I

    .line 376
    :cond_4
    :goto_5
    const/16 v25, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    move/from16 v26, v0

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-static {v0, v1, v10}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    .line 377
    const/16 v25, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemHeight:I

    move/from16 v26, v0

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-static {v0, v1, v10}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemHeight:I

    .line 379
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemSize:I

    move/from16 v25, v0

    const/16 v26, 0x4

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_10

    .line 380
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemSize:I

    move/from16 v25, v0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    .line 381
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    move/from16 v26, v0

    mul-int v23, v25, v26

    .line 401
    :goto_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSeperatorWidth:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    move/from16 v26, v0

    add-int/lit8 v26, v26, -0x1

    mul-int v25, v25, v26

    add-int v23, v23, v25

    .line 403
    new-instance v18, Landroid/graphics/Rect;

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x438

    const/16 v28, 0x438

    move-object/from16 v0, v18

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 404
    .local v18, "outRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    move-object/from16 v25, v0

    if-eqz v25, :cond_5

    .line 405
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 408
    :cond_5
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Rect;->width()I

    move-result v19

    .line 409
    .local v19, "pWidth":I
    if-eqz v19, :cond_7

    .line 410
    move/from16 v24, v23

    .line 411
    .local v24, "widthAdapt":I
    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v25, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v26, v0

    div-float v25, v25, v26

    const v26, 0x3f666666    # 0.9f

    cmpl-float v25, v25, v26

    if-lez v25, :cond_7

    .line 413
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    move/from16 v25, v0

    if-gtz v25, :cond_14

    .line 422
    :goto_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    move/from16 v25, v0

    if-lez v25, :cond_15

    .line 423
    move/from16 v23, v24

    .line 430
    .end local v24    # "widthAdapt":I
    :cond_7
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move/from16 v0, v23

    move-object/from16 v1, v25

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 431
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemHeight:I

    move/from16 v26, v0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 433
    new-instance v14, Landroid/widget/FrameLayout$LayoutParams;

    .line 434
    const/16 v25, -0x1

    const/16 v26, -0x1

    .line 433
    move/from16 v0, v25

    move/from16 v1, v26

    invoke-direct {v14, v0, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 435
    .local v14, "layoutParam":Landroid/widget/FrameLayout$LayoutParams;
    new-instance v25, Landroid/widget/HorizontalScrollView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-direct/range {v25 .. v26}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalScrollView:Landroid/widget/HorizontalScrollView;

    .line 436
    new-instance v17, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 437
    .local v17, "myLinearLayout":Landroid/widget/LinearLayout;
    const/16 v25, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 438
    new-instance v20, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v25, -0x2

    .line 439
    const/16 v26, -0x1

    .line 438
    move-object/from16 v0, v20

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 440
    .local v20, "param":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v25, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v25 .. v25}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v25

    mul-int/lit8 v25, v25, 0x2

    add-int/lit8 v25, v25, -0x1

    move/from16 v0, v25

    if-lt v12, v0, :cond_16

    .line 443
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalScrollView:Landroid/widget/HorizontalScrollView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v14}, Landroid/widget/HorizontalScrollView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 444
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalScrollView:Landroid/widget/HorizontalScrollView;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Landroid/widget/HorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 447
    new-instance v7, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-direct {v7, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 448
    .local v7, "contentLayout":Landroid/widget/RelativeLayout;
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 449
    const/16 v25, -0x1

    const/16 v26, -0x1

    .line 448
    move/from16 v0, v25

    move/from16 v1, v26

    invoke-direct {v8, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 450
    .local v8, "contentLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 451
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalScrollView:Landroid/widget/HorizontalScrollView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 452
    invoke-virtual {v15, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 453
    iget v0, v10, Landroid/util/DisplayMetrics;->density:F

    move/from16 v25, v0

    const/high16 v26, 0x3f800000    # 1.0f

    cmpl-float v25, v25, v26

    if-lez v25, :cond_8

    .line 454
    const/16 v25, 0xfa

    const/16 v26, 0xfa

    const/16 v27, 0xfa

    invoke-static/range {v25 .. v27}, Landroid/graphics/Color;->rgb(III)I

    move-result v25

    move/from16 v0, v25

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 456
    :cond_8
    const/16 v25, 0x1

    int-to-float v0, v9

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-static {v0, v1, v10}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v25

    move/from16 v0, v25

    float-to-int v9, v0

    .line 457
    invoke-virtual {v15, v9, v9, v9, v9}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 459
    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupView:Landroid/view/View;

    .line 461
    new-instance v25, Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupView:Landroid/view/View;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/Rect;->width()I

    move-result v27

    mul-int/lit8 v28, v9, 0x2

    add-int v27, v27, v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/graphics/Rect;->height()I

    move-result v28

    .line 462
    mul-int/lit8 v29, v9, 0x2

    add-int v28, v28, v29

    invoke-direct/range {v25 .. v28}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    .line 461
    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    .line 464
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    move-object/from16 v25, v0

    const/16 v26, -0x1

    invoke-virtual/range {v25 .. v26}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 465
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    move-object/from16 v25, v0

    new-instance v26, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    invoke-direct/range {v26 .. v27}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual/range {v25 .. v26}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 466
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 467
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 469
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    move-object/from16 v25, v0

    new-instance v26, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V

    invoke-virtual/range {v25 .. v26}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    .line 490
    .end local v5    # "canvasWidth":I
    .end local v7    # "contentLayout":Landroid/widget/RelativeLayout;
    .end local v8    # "contentLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v9    # "delta_padding":I
    .end local v10    # "dm":Landroid/util/DisplayMetrics;
    .end local v12    # "i":I
    .end local v14    # "layoutParam":Landroid/widget/FrameLayout$LayoutParams;
    .end local v15    # "linearLayout":Landroid/widget/RelativeLayout;
    .end local v16    # "manager":Landroid/content/pm/PackageManager;
    .end local v17    # "myLinearLayout":Landroid/widget/LinearLayout;
    .end local v18    # "outRect":Landroid/graphics/Rect;
    .end local v19    # "pWidth":I
    .end local v20    # "param":Landroid/widget/LinearLayout$LayoutParams;
    .end local v21    # "res":Landroid/content/res/Resources;
    .end local v22    # "shadowImage":Landroid/graphics/drawable/Drawable;
    .end local v23    # "width":I
    :cond_9
    return-void

    .line 296
    .restart local v10    # "dm":Landroid/util/DisplayMetrics;
    .restart local v21    # "res":Landroid/content/res/Resources;
    :cond_a
    iget v5, v10, Landroid/util/DisplayMetrics;->heightPixels:I

    .restart local v5    # "canvasWidth":I
    goto/16 :goto_0

    .line 309
    .restart local v16    # "manager":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v11

    .line 310
    .local v11, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/16 v25, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    goto/16 :goto_1

    .line 329
    .end local v11    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v15    # "linearLayout":Landroid/widget/RelativeLayout;
    .restart local v22    # "shadowImage":Landroid/graphics/drawable/Drawable;
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    move-object/from16 v25, v0

    const-string/jumbo v26, "shadow_border"

    const-string v27, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v25 .. v28}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v13

    .restart local v13    # "id":I
    goto/16 :goto_2

    .line 334
    .end local v13    # "id":I
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    move-object/from16 v26, v0

    const-string/jumbo v27, "shadow_border"

    const-string v28, "drawable"

    .line 335
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v29

    .line 334
    invoke-virtual/range {v26 .. v29}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v22

    goto/16 :goto_3

    .line 340
    :cond_d
    if-eqz v22, :cond_3

    .line 341
    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    .line 361
    .restart local v9    # "delta_padding":I
    .restart local v23    # "width":I
    :cond_e
    iget v0, v10, Landroid/util/DisplayMetrics;->density:F

    move/from16 v25, v0

    const/high16 v26, 0x3f800000    # 1.0f

    cmpl-float v25, v25, v26

    if-nez v25, :cond_f

    .line 362
    const/16 v25, 0x56

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    .line 363
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSeperatorWidth:I

    .line 364
    const/16 v25, 0x4c

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemHeight:I

    .line 365
    const/16 v25, 0xe

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTextSize:I

    .line 366
    const-string v25, "#252525"

    invoke-static/range {v25 .. v25}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTextColor:I

    goto/16 :goto_5

    .line 368
    :cond_f
    const/16 v25, 0x48

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    .line 369
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSeperatorWidth:I

    .line 370
    const/16 v25, 0x3d

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemHeight:I

    .line 371
    const/16 v25, 0xc

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTextSize:I

    .line 372
    const/high16 v25, -0x1000000

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTextColor:I

    goto/16 :goto_5

    .line 382
    :cond_10
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemSize:I

    move/from16 v25, v0

    const/16 v26, 0x6

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_12

    .line 383
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    .line 384
    .local v6, "config":Landroid/content/res/Configuration;
    iget v0, v6, Landroid/content/res/Configuration;->orientation:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_11

    .line 385
    const/16 v25, 0x4

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    .line 386
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    move/from16 v26, v0

    mul-int v23, v25, v26

    .line 387
    goto/16 :goto_6

    .line 388
    :cond_11
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemSize:I

    move/from16 v25, v0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    .line 389
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    move/from16 v26, v0

    mul-int v23, v25, v26

    .line 391
    goto/16 :goto_6

    .line 392
    .end local v6    # "config":Landroid/content/res/Configuration;
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    .line 393
    .restart local v6    # "config":Landroid/content/res/Configuration;
    iget v0, v6, Landroid/content/res/Configuration;->orientation:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_13

    .line 394
    const/16 v25, 0x4

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    .line 395
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    move/from16 v26, v0

    mul-int v23, v25, v26

    .line 396
    goto/16 :goto_6

    .line 397
    :cond_13
    const/16 v25, 0x6

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    .line 398
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    move/from16 v26, v0

    mul-int v23, v25, v26

    goto/16 :goto_6

    .line 414
    .end local v6    # "config":Landroid/content/res/Configuration;
    .restart local v18    # "outRect":Landroid/graphics/Rect;
    .restart local v19    # "pWidth":I
    .restart local v24    # "widthAdapt":I
    :cond_14
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    move/from16 v25, v0

    add-int/lit8 v25, v25, -0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    .line 415
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    move/from16 v26, v0

    mul-int v24, v25, v26

    .line 416
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSeperatorWidth:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    move/from16 v26, v0

    add-int/lit8 v26, v26, -0x1

    mul-int v25, v25, v26

    add-int v24, v24, v25

    .line 417
    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v25, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v26, v0

    div-float v25, v25, v26

    const v26, 0x3f666666    # 0.9f

    cmpg-float v25, v25, v26

    if-gtz v25, :cond_6

    goto/16 :goto_7

    .line 425
    :cond_15
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    .line 426
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    move/from16 v23, v0

    goto/16 :goto_8

    .line 441
    .end local v24    # "widthAdapt":I
    .restart local v12    # "i":I
    .restart local v14    # "layoutParam":Landroid/widget/FrameLayout$LayoutParams;
    .restart local v17    # "myLinearLayout":Landroid/widget/LinearLayout;
    .restart local v20    # "param":Landroid/widget/LinearLayout$LayoutParams;
    :cond_16
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getItemView(I)Landroid/view/View;

    move-result-object v25

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 440
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_9
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSelectListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollFlag:Z

    return v0
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Ljava/util/Timer;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Z)V
    .locals 0

    .prologue
    .line 120
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollFlag:Z

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mDelay:I

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;I)V
    .locals 0

    .prologue
    .line 673
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->updateTimer(I)V

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)F
    .locals 1

    .prologue
    .line 627
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mCount:F

    return v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;F)V
    .locals 0

    .prologue
    .line 627
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mCount:F

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/widget/HorizontalScrollView;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalScrollView:Landroid/widget/HorizontalScrollView;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    return-object v0
.end method

.method private getItemView(I)Landroid/view/View;
    .locals 41
    .param p1, "position"    # I

    .prologue
    .line 740
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    if-nez v34, :cond_0

    .line 741
    const-string v34, "SpenContextMenu"

    new-instance v35, Ljava/lang/StringBuilder;

    const-string v36, "mContext is NULL at position = "

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v35

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 742
    const/16 v21, 0x0

    .line 966
    :goto_0
    return-object v21

    .line 745
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    .line 746
    .local v26, "res":Landroid/content/res/Resources;
    invoke-virtual/range {v26 .. v26}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v11

    .line 748
    .local v11, "dm":Landroid/util/DisplayMetrics;
    new-instance v21, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 749
    .local v21, "itemLayout":Landroid/widget/RelativeLayout;
    new-instance v24, Landroid/widget/RelativeLayout$LayoutParams;

    .line 750
    const/16 v34, -0x1

    const/16 v35, -0x1

    .line 749
    move-object/from16 v0, v24

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 751
    .local v24, "layoutParam":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 753
    add-int/lit8 v34, p1, 0x1

    rem-int/lit8 v34, v34, 0x2

    if-nez v34, :cond_6

    .line 754
    new-instance v27, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 755
    .local v27, "separator":Landroid/widget/ImageView;
    new-instance v28, Landroid/view/ViewGroup$LayoutParams;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSeperatorWidth:I

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemHeight:I

    move/from16 v35, v0

    move-object/from16 v0, v28

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 756
    .local v28, "separatorParams":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual/range {v27 .. v28}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 757
    sget v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_3

    .line 758
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z

    move/from16 v34, v0

    if-eqz v34, :cond_1

    .line 759
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    move-object/from16 v35, v0

    const-string v36, "quick_popup_dv_vienna"

    .line 760
    const-string v37, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v38

    .line 759
    invoke-virtual/range {v35 .. v38}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v35

    invoke-virtual/range {v34 .. v35}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v34

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 777
    :goto_1
    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 762
    :cond_1
    sget v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I

    const/16 v35, 0x13

    move/from16 v0, v34

    move/from16 v1, v35

    if-gt v0, v1, :cond_2

    .line 763
    const v34, -0x777778

    move-object/from16 v0, v27

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_1

    .line 765
    :cond_2
    const/16 v34, 0x7d

    const/16 v35, 0x16

    const/16 v36, 0x25

    const/16 v37, 0x2e

    invoke-static/range {v34 .. v37}, Landroid/graphics/Color;->argb(IIII)I

    move-result v34

    move-object/from16 v0, v27

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_1

    .line 769
    :cond_3
    sget v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I

    const/16 v35, 0x13

    move/from16 v0, v34

    move/from16 v1, v35

    if-gt v0, v1, :cond_4

    .line 770
    const v34, -0x777778

    move-object/from16 v0, v27

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_1

    .line 772
    :cond_4
    iget v0, v11, Landroid/util/DisplayMetrics;->density:F

    move/from16 v34, v0

    const/high16 v35, 0x40000000    # 2.0f

    cmpg-float v34, v34, v35

    if-gez v34, :cond_5

    const/16 v34, 0xcf

    const/16 v35, 0xcf

    const/16 v36, 0xcf

    invoke-static/range {v34 .. v36}, Landroid/graphics/Color;->rgb(III)I

    move-result v8

    .line 773
    .local v8, "color":I
    :goto_2
    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_1

    .line 772
    .end local v8    # "color":I
    :cond_5
    const/16 v34, 0x16

    const/16 v35, 0x25

    const/16 v36, 0x2e

    invoke-static/range {v34 .. v36}, Landroid/graphics/Color;->rgb(III)I

    move-result v8

    goto :goto_2

    .line 780
    .end local v27    # "separator":Landroid/widget/ImageView;
    .end local v28    # "separatorParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_6
    div-int/lit8 v13, p1, 0x2

    .line 781
    .local v13, "index":I
    new-instance v17, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 782
    .local v17, "itemButton":Landroid/widget/RelativeLayout;
    new-instance v18, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemHeight:I

    move/from16 v35, v0

    move-object/from16 v0, v18

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 783
    .local v18, "itemButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual/range {v17 .. v18}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 784
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v34, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v34 .. v34}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v34

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v34, v0

    move-object/from16 v0, v17

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 785
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v34, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v34 .. v34}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v34

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v34, v0

    move-object/from16 v0, v17

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 786
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v34, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v34 .. v34}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v34

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v34, v0

    move-object/from16 v0, v17

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 787
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v34, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v34 .. v34}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v34

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v34, v0

    move-object/from16 v0, v17

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 789
    new-instance v6, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v6}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 791
    .local v6, "backgroundImageDrawable":Landroid/graphics/drawable/StateListDrawable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v34, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v34 .. v34}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v34

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundPressed:Landroid/graphics/drawable/Drawable;

    .line 792
    .local v5, "backgroundImage":Landroid/graphics/drawable/Drawable;
    if-nez v5, :cond_7

    .line 793
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z

    move/from16 v34, v0

    if-eqz v34, :cond_d

    .line 794
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    move-object/from16 v35, v0

    const-string v36, "quick_popup_bg_press_vienna"

    .line 795
    const-string v37, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v38

    .line 794
    invoke-virtual/range {v35 .. v38}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v35

    invoke-virtual/range {v34 .. v35}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 801
    :cond_7
    :goto_3
    const/16 v34, 0x1

    move/from16 v0, v34

    new-array v0, v0, [I

    move-object/from16 v34, v0

    const/16 v35, 0x0

    const v36, 0x10100a7

    aput v36, v34, v35

    move-object/from16 v0, v34

    invoke-virtual {v6, v0, v5}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 803
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v34, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v34 .. v34}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v34

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundNormal:Landroid/graphics/drawable/Drawable;

    .line 805
    new-instance v16, Landroid/widget/RelativeLayout$LayoutParams;

    .line 806
    const/16 v34, -0x1

    const/16 v35, -0x1

    .line 805
    move-object/from16 v0, v16

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 808
    .local v16, "itemBackgroundParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v10, 0x5

    .line 810
    .local v10, "delta_padding":I
    new-instance v15, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-direct {v15, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 811
    .local v15, "itemBackground":Landroid/widget/ImageButton;
    invoke-virtual/range {v15 .. v16}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 813
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    move-object/from16 v34, v0

    if-eqz v34, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v34

    if-nez v34, :cond_e

    .line 814
    :cond_8
    const/16 v34, 0x0

    move-object/from16 v0, v34

    invoke-virtual {v15, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 819
    :goto_4
    sget v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_11

    .line 820
    if-eqz v5, :cond_10

    .line 821
    sget v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I

    const/16 v35, 0x10

    move/from16 v0, v34

    move/from16 v1, v35

    if-ge v0, v1, :cond_f

    .line 822
    invoke-virtual {v15, v5}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 841
    :goto_5
    sget-object v34, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    move-object/from16 v0, v34

    invoke-virtual {v15, v0}, Landroid/widget/ImageButton;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 843
    const/16 v34, 0x1

    int-to-float v0, v10

    move/from16 v35, v0

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-static {v0, v1, v11}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v34

    move/from16 v0, v34

    float-to-int v10, v0

    .line 845
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v34, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v34 .. v34}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v34

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v34, v0

    move/from16 v0, v34

    invoke-virtual {v15, v0}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 846
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v34, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v34 .. v34}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v34

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v34, v0

    move/from16 v0, v34

    invoke-virtual {v15, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 847
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v34, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v34 .. v34}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v34

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v34, v0

    move/from16 v0, v34

    invoke-virtual {v15, v0}, Landroid/widget/ImageButton;->setLongClickable(Z)V

    .line 848
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v34, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v34 .. v34}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v34

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v34, v0

    move/from16 v0, v34

    invoke-virtual {v15, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 850
    sget v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I

    const/16 v35, 0x13

    move/from16 v0, v34

    move/from16 v1, v35

    if-le v0, v1, :cond_14

    if-nez v5, :cond_14

    .line 851
    new-instance v9, Landroid/content/res/ColorStateList;

    const/16 v34, 0x1

    move/from16 v0, v34

    new-array v0, v0, [[I

    move-object/from16 v34, v0

    const/16 v35, 0x0

    const/16 v36, 0x1

    move/from16 v0, v36

    new-array v0, v0, [I

    move-object/from16 v36, v0

    const/16 v37, 0x0

    const v38, 0x10100a7

    aput v38, v36, v37

    aput-object v36, v34, v35

    .line 852
    const/16 v35, 0x1

    move/from16 v0, v35

    new-array v0, v0, [I

    move-object/from16 v35, v0

    const/16 v36, 0x0

    const/16 v37, 0x1e

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    invoke-static/range {v37 .. v40}, Landroid/graphics/Color;->argb(IIII)I

    move-result v37

    aput v37, v35, v36

    .line 851
    move-object/from16 v0, v34

    move-object/from16 v1, v35

    invoke-direct {v9, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 853
    .local v9, "colorStateList":Landroid/content/res/ColorStateList;
    new-instance v34, Landroid/graphics/drawable/RippleDrawable;

    const/16 v35, 0x0

    new-instance v36, Landroid/graphics/drawable/ColorDrawable;

    const/16 v37, -0x1

    invoke-direct/range {v36 .. v37}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-direct {v0, v9, v1, v2}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, v34

    invoke-virtual {v15, v0}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 858
    .end local v9    # "colorStateList":Landroid/content/res/ColorStateList;
    :goto_6
    neg-int v0, v10

    move/from16 v34, v0

    neg-int v0, v10

    move/from16 v35, v0

    neg-int v0, v10

    move/from16 v36, v0

    neg-int v0, v10

    move/from16 v37, v0

    move/from16 v0, v34

    move/from16 v1, v35

    move/from16 v2, v36

    move/from16 v3, v37

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/widget/ImageButton;->setPadding(IIII)V

    .line 859
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v34, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v34 .. v34}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v34

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    move/from16 v34, v0

    move/from16 v0, v34

    invoke-virtual {v15, v0}, Landroid/widget/ImageButton;->setId(I)V

    .line 860
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v34, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v34 .. v34}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v34

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v34, v0

    if-eqz v34, :cond_9

    .line 861
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mOnButtonClicked:Landroid/view/View$OnClickListener;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-virtual {v15, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 864
    :cond_9
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 866
    new-instance v19, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 868
    .local v19, "itemIcon":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v34, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v34 .. v34}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v34

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v34, v0

    if-nez v34, :cond_15

    .line 869
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v34, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v34 .. v34}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v34

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableDisableItem:Landroid/graphics/drawable/Drawable;

    move-object/from16 v34, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 874
    :goto_7
    const/16 v34, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 875
    new-instance v20, Landroid/widget/RelativeLayout$LayoutParams;

    .line 876
    const/16 v34, -0x2

    const/16 v35, -0x2

    .line 875
    move-object/from16 v0, v20

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 877
    .local v20, "itemIconParams":Landroid/widget/RelativeLayout$LayoutParams;
    sget v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_17

    .line 878
    const/16 v34, 0x1

    const/high16 v35, 0x42040000    # 33.0f

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-static {v0, v1, v11}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v34

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    move/from16 v0, v34

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 879
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsViennaModel:Z

    move/from16 v34, v0

    if-eqz v34, :cond_16

    .line 880
    const/16 v34, 0x1

    const/high16 v35, 0x41500000    # 13.0f

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-static {v0, v1, v11}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v34

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    move/from16 v0, v34

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 881
    const/16 v34, 0x1

    const/high16 v35, 0x420c0000    # 35.0f

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-static {v0, v1, v11}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v34

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    move/from16 v0, v34

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 897
    :goto_8
    const/16 v34, 0x6

    move-object/from16 v0, v20

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 898
    const/16 v34, 0xe

    move-object/from16 v0, v20

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 899
    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 900
    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 902
    new-instance v22, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 903
    .local v22, "itemText":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTextColor:I

    move/from16 v34, v0

    move-object/from16 v0, v22

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 904
    const/16 v34, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTextSize:I

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    move-object/from16 v0, v22

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 906
    new-instance v23, Landroid/widget/RelativeLayout$LayoutParams;

    .line 907
    const/16 v34, -0x1

    const/16 v35, -0x2

    .line 906
    move-object/from16 v0, v23

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 908
    .local v23, "itemTextParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v34, 0xe

    move-object/from16 v0, v23

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 909
    const/16 v34, 0xc

    invoke-virtual/range {v19 .. v19}, Landroid/widget/ImageView;->getId()I

    move-result v35

    move-object/from16 v0, v23

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 911
    const/4 v14, 0x1

    .line 912
    .local v14, "isNormalText":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v34, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v34 .. v34}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v34

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    move-object/from16 v29, v0

    .line 913
    .local v29, "str":Ljava/lang/String;
    if-eqz v29, :cond_b

    .line 914
    invoke-virtual/range {v29 .. v29}, Ljava/lang/String;->length()I

    move-result v34

    move/from16 v0, v34

    new-array v0, v0, [F

    move-object/from16 v33, v0

    .line 915
    .local v33, "widths":[F
    invoke-virtual/range {v22 .. v22}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v25

    .line 916
    .local v25, "paint":Landroid/graphics/Paint;
    move-object/from16 v0, v25

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    .line 917
    const/16 v34, 0x0

    invoke-virtual/range {v29 .. v29}, Ljava/lang/String;->length()I

    move-result v35

    move-object/from16 v0, v25

    move-object/from16 v1, v29

    move/from16 v2, v34

    move/from16 v3, v35

    move-object/from16 v4, v33

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;II[F)I

    move-result v7

    .line 918
    .local v7, "c":I
    const/16 v32, 0x0

    .line 919
    .local v32, "textWidth":F
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_9
    if-lt v12, v7, :cond_19

    .line 923
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    move/from16 v34, v0

    move/from16 v0, v34

    int-to-float v0, v0

    move/from16 v34, v0

    cmpl-float v34, v32, v34

    if-lez v34, :cond_a

    .line 924
    const/4 v14, 0x0

    .line 926
    :cond_a
    const/16 v34, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 927
    move-object/from16 v0, v22

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 928
    if-eqz v14, :cond_1a

    .line 929
    sget-object v34, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v22

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 940
    .end local v7    # "c":I
    .end local v12    # "i":I
    .end local v25    # "paint":Landroid/graphics/Paint;
    .end local v32    # "textWidth":F
    .end local v33    # "widths":[F
    :cond_b
    :goto_a
    sget v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_1b

    .line 941
    const/16 v34, 0x1

    const/high16 v35, 0x41200000    # 10.0f

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-static {v0, v1, v11}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v34

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    move/from16 v0, v34

    move-object/from16 v1, v23

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 942
    const/16 v34, 0x1

    const/high16 v35, 0x40e00000    # 7.0f

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-static {v0, v1, v11}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v34

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    move/from16 v0, v34

    move-object/from16 v1, v23

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 957
    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v34, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v34 .. v34}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v34

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    move/from16 v34, v0

    if-nez v34, :cond_c

    .line 958
    const v34, 0x3e99999a    # 0.3f

    move-object/from16 v0, v22

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 960
    :cond_c
    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 961
    const/16 v34, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 963
    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 965
    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 797
    .end local v10    # "delta_padding":I
    .end local v14    # "isNormalText":Z
    .end local v15    # "itemBackground":Landroid/widget/ImageButton;
    .end local v16    # "itemBackgroundParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v19    # "itemIcon":Landroid/widget/ImageView;
    .end local v20    # "itemIconParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v22    # "itemText":Landroid/widget/TextView;
    .end local v23    # "itemTextParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v29    # "str":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    move-object/from16 v35, v0

    const-string v36, "quick_popup_bg_press"

    const-string v37, "drawable"

    .line 798
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v38

    .line 797
    invoke-virtual/range {v35 .. v38}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v35

    invoke-virtual/range {v34 .. v35}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    goto/16 :goto_3

    .line 816
    .restart local v10    # "delta_padding":I
    .restart local v15    # "itemBackground":Landroid/widget/ImageButton;
    .restart local v16    # "itemBackgroundParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v34, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v34 .. v34}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v34

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-virtual {v15, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 824
    :cond_f
    invoke-virtual {v15, v5}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_5

    .line 827
    :cond_10
    const/16 v34, 0xff

    const/16 v35, 0xff

    const/16 v36, 0xff

    invoke-static/range {v34 .. v36}, Landroid/graphics/Color;->rgb(III)I

    move-result v34

    move/from16 v0, v34

    invoke-virtual {v15, v0}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    goto/16 :goto_5

    .line 830
    :cond_11
    if-eqz v5, :cond_13

    .line 831
    sget v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->SDK_VERSION:I

    const/16 v35, 0x10

    move/from16 v0, v34

    move/from16 v1, v35

    if-ge v0, v1, :cond_12

    .line 832
    invoke-virtual {v15, v5}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_5

    .line 834
    :cond_12
    invoke-virtual {v15, v5}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_5

    .line 837
    :cond_13
    const/16 v34, 0xe9

    const/16 v35, 0xeb

    const/16 v36, 0xec

    invoke-static/range {v34 .. v36}, Landroid/graphics/Color;->rgb(III)I

    move-result v34

    move/from16 v0, v34

    invoke-virtual {v15, v0}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    goto/16 :goto_5

    .line 855
    :cond_14
    invoke-virtual {v15, v6}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_6

    .line 871
    .restart local v19    # "itemIcon":Landroid/widget/ImageView;
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v34, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static/range {v34 .. v34}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    move-object/from16 v0, v34

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableNormalItem:Landroid/graphics/drawable/Drawable;

    move-object/from16 v34, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_7

    .line 883
    .restart local v20    # "itemIconParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_16
    const/16 v34, 0x1

    const/high16 v35, 0x41000000    # 8.0f

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-static {v0, v1, v11}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v34

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    move/from16 v0, v34

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 884
    const/16 v34, 0x1

    const/high16 v35, 0x42000000    # 32.0f

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-static {v0, v1, v11}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v34

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    move/from16 v0, v34

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_8

    .line 887
    :cond_17
    iget v0, v11, Landroid/util/DisplayMetrics;->density:F

    move/from16 v34, v0

    const/high16 v35, 0x3f800000    # 1.0f

    cmpl-float v34, v34, v35

    if-nez v34, :cond_18

    .line 888
    const/16 v34, 0x1

    const/high16 v35, 0x41e80000    # 29.0f

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-static {v0, v1, v11}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v34

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    move/from16 v0, v34

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 889
    const/16 v34, 0x1

    const/high16 v35, 0x41700000    # 15.0f

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-static {v0, v1, v11}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v34

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    move/from16 v0, v34

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 890
    const/16 v34, 0x1

    const/high16 v35, 0x42040000    # 33.0f

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-static {v0, v1, v11}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v34

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    move/from16 v0, v34

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_8

    .line 892
    :cond_18
    const/16 v34, 0x1

    const/high16 v35, 0x41a00000    # 20.0f

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-static {v0, v1, v11}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v34

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    move/from16 v0, v34

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 893
    const/16 v34, 0x1

    const/high16 v35, 0x41000000    # 8.0f

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-static {v0, v1, v11}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v34

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    move/from16 v0, v34

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 894
    const/16 v34, 0x1

    const/high16 v35, 0x41a80000    # 21.0f

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-static {v0, v1, v11}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v34

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    move/from16 v0, v34

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_8

    .line 920
    .restart local v7    # "c":I
    .restart local v12    # "i":I
    .restart local v14    # "isNormalText":Z
    .restart local v22    # "itemText":Landroid/widget/TextView;
    .restart local v23    # "itemTextParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v25    # "paint":Landroid/graphics/Paint;
    .restart local v29    # "str":Ljava/lang/String;
    .restart local v32    # "textWidth":F
    .restart local v33    # "widths":[F
    :cond_19
    aget v34, v33, v12

    add-float v32, v32, v34

    .line 919
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_9

    .line 931
    :cond_1a
    const/16 v34, 0x1

    const/high16 v35, 0x40400000    # 3.0f

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-static {v0, v1, v11}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v34

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    move/from16 v0, v34

    move-object/from16 v1, v23

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 932
    const/16 v34, 0x1

    const/high16 v35, 0x40400000    # 3.0f

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-static {v0, v1, v11}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v34

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    move/from16 v0, v34

    move-object/from16 v1, v23

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 933
    const/16 v34, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 934
    const/16 v34, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 935
    const/16 v34, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 936
    sget-object v34, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v22

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 937
    const/16 v34, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setFocusableInTouchMode(Z)V

    goto/16 :goto_a

    .line 945
    .end local v7    # "c":I
    .end local v12    # "i":I
    .end local v25    # "paint":Landroid/graphics/Paint;
    .end local v32    # "textWidth":F
    .end local v33    # "widths":[F
    :cond_1b
    const/16 v31, 0x2

    .line 946
    .local v31, "textTopMargin":I
    const/16 v30, 0x5

    .line 948
    .local v30, "textBottomMargin":I
    iget v0, v11, Landroid/util/DisplayMetrics;->density:F

    move/from16 v34, v0

    const/high16 v35, 0x3f800000    # 1.0f

    cmpl-float v34, v34, v35

    if-nez v34, :cond_1c

    .line 949
    const/16 v30, 0xa

    .line 950
    const/16 v31, 0x5

    .line 953
    :cond_1c
    const/16 v34, 0x1

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v35, v0

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-static {v0, v1, v11}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v34

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    move/from16 v0, v34

    move-object/from16 v1, v23

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 954
    const/16 v34, 0x1

    .line 955
    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v35, v0

    .line 954
    move/from16 v0, v34

    move/from16 v1, v35

    invoke-static {v0, v1, v11}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v34

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    move/from16 v0, v34

    move-object/from16 v1, v23

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_b
.end method

.method public static getType()I
    .locals 1

    .prologue
    .line 166
    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    return v0
.end method

.method private playScrollAnimation(I)V
    .locals 11
    .param p1, "delay"    # I

    .prologue
    .line 631
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 632
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 634
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 671
    :goto_0
    return-void

    .line 638
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 639
    .local v9, "res":Landroid/content/res/Resources;
    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    .line 642
    .local v6, "dm":Landroid/util/DisplayMetrics;
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemSize:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    mul-int/2addr v0, v1

    int-to-float v7, v0

    .line 643
    .local v7, "lvWidth":F
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSeperatorWidth:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemSize:I

    add-int/lit8 v1, v1, -0x1

    mul-int/2addr v0, v1

    int-to-float v0, v0

    add-float/2addr v7, v0

    .line 644
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v7, v0

    .line 646
    move v8, v7

    .line 647
    .local v8, "lvWidthAct":F
    const/4 v0, 0x1

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v10

    .line 649
    .local v10, "step":F
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    .line 650
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

    invoke-direct {v1, p0, v8, v10}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;FF)V

    .line 670
    const-wide/16 v2, 0x64

    int-to-long v4, p1

    .line 650
    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    goto :goto_0
.end method

.method public static setType(I)V
    .locals 0
    .param p0, "type"    # I

    .prologue
    .line 178
    sput p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    .line 179
    return-void
.end method

.method private updateContextMenuLocation()V
    .locals 14

    .prologue
    const/4 v13, 0x6

    const v12, 0x3f666666    # 0.9f

    const/4 v9, 0x4

    const/4 v10, 0x0

    const/4 v11, 0x1

    .line 182
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v8

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    if-eqz v8, :cond_3

    .line 183
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 184
    .local v5, "res":Landroid/content/res/Resources;
    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 186
    .local v2, "dm":Landroid/util/DisplayMetrics;
    const/16 v6, 0x136

    .line 187
    .local v6, "width":I
    const/4 v1, 0x5

    .line 188
    .local v1, "delta_padding":I
    iput v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    .line 190
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemSize:I

    if-ge v8, v9, :cond_4

    .line 191
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemSize:I

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    .line 192
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    mul-int v6, v8, v9

    .line 214
    :goto_0
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSeperatorWidth:I

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    add-int/lit8 v9, v9, -0x1

    mul-int/2addr v8, v9

    add-int/2addr v6, v8

    .line 216
    new-instance v3, Landroid/graphics/Rect;

    const/16 v8, 0x438

    const/16 v9, 0x438

    invoke-direct {v3, v10, v10, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 217
    .local v3, "outRect":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    if-eqz v8, :cond_0

    .line 218
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    invoke-virtual {v8, v3}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 220
    :cond_0
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v4

    .line 221
    .local v4, "pWidth":I
    if-eqz v4, :cond_2

    .line 222
    move v7, v6

    .line 223
    .local v7, "widthAdapt":I
    int-to-float v8, v7

    int-to-float v9, v4

    div-float/2addr v8, v9

    cmpl-float v8, v8, v12

    if-lez v8, :cond_2

    .line 226
    :cond_1
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    if-gtz v8, :cond_8

    .line 235
    :goto_1
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    if-lez v8, :cond_9

    .line 236
    move v6, v7

    .line 244
    .end local v7    # "widthAdapt":I
    :cond_2
    :goto_2
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemHeight:I

    add-int/2addr v9, v10

    iput v9, v8, Landroid/graphics/Rect;->bottom:I

    .line 245
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    add-int/2addr v9, v6

    iput v9, v8, Landroid/graphics/Rect;->right:I

    .line 246
    int-to-float v8, v1

    invoke-static {v11, v8, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v8

    float-to-int v1, v8

    .line 248
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v8, :cond_3

    .line 249
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    mul-int/lit8 v10, v1, 0x2

    add-int/2addr v9, v10

    invoke-virtual {v8, v9}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 250
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    mul-int/lit8 v10, v1, 0x2

    add-int/2addr v9, v10

    invoke-virtual {v8, v9}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 252
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupView:Landroid/view/View;

    if-eqz v8, :cond_3

    .line 253
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupView:Landroid/view/View;

    invoke-virtual {v8, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 257
    .end local v1    # "delta_padding":I
    .end local v2    # "dm":Landroid/util/DisplayMetrics;
    .end local v3    # "outRect":Landroid/graphics/Rect;
    .end local v4    # "pWidth":I
    .end local v5    # "res":Landroid/content/res/Resources;
    .end local v6    # "width":I
    :cond_3
    return-void

    .line 193
    .restart local v1    # "delta_padding":I
    .restart local v2    # "dm":Landroid/util/DisplayMetrics;
    .restart local v5    # "res":Landroid/content/res/Resources;
    .restart local v6    # "width":I
    :cond_4
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemSize:I

    if-ge v8, v13, :cond_6

    .line 194
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 195
    .local v0, "config":Landroid/content/res/Configuration;
    iget v8, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v8, v11, :cond_5

    .line 196
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    .line 197
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    mul-int v6, v8, v9

    .line 199
    goto/16 :goto_0

    .line 200
    :cond_5
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemSize:I

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    .line 201
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    mul-int v6, v8, v9

    .line 203
    goto/16 :goto_0

    .line 204
    .end local v0    # "config":Landroid/content/res/Configuration;
    :cond_6
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 205
    .restart local v0    # "config":Landroid/content/res/Configuration;
    iget v8, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v8, v11, :cond_7

    .line 207
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    .line 208
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    mul-int v6, v8, v9

    .line 209
    goto/16 :goto_0

    .line 210
    :cond_7
    iput v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    .line 211
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    mul-int v6, v8, v9

    goto/16 :goto_0

    .line 227
    .end local v0    # "config":Landroid/content/res/Configuration;
    .restart local v3    # "outRect":Landroid/graphics/Rect;
    .restart local v4    # "pWidth":I
    .restart local v7    # "widthAdapt":I
    :cond_8
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    .line 228
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    mul-int v7, v8, v9

    .line 229
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSeperatorWidth:I

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    add-int/lit8 v9, v9, -0x1

    mul-int/2addr v8, v9

    add-int/2addr v7, v8

    .line 230
    int-to-float v8, v7

    int-to-float v9, v4

    div-float/2addr v8, v9

    cmpg-float v8, v8, v12

    if-gtz v8, :cond_1

    goto/16 :goto_1

    .line 238
    :cond_9
    iput v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumDisplayedItem:I

    .line 239
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mItemWidth:I

    goto/16 :goto_2
.end method

.method private updateTimer(I)V
    .locals 4
    .param p1, "delay"    # I

    .prologue
    .line 674
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 675
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 678
    :cond_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    .line 679
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$4;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V

    .line 690
    int-to-long v2, p1

    .line 679
    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 691
    return-void
.end method


# virtual methods
.method public close()Z
    .locals 1

    .prologue
    .line 501
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->hide()V

    .line 502
    const/4 v0, 0x1

    return v0
.end method

.method public getItemEnabled(I)Z
    .locals 4
    .param p1, "id"    # I

    .prologue
    const/4 v1, 0x0

    .line 560
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    if-nez v2, :cond_1

    .line 570
    :cond_0
    :goto_0
    return v1

    .line 564
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    .line 565
    .local v0, "contextMenuItemInfo":Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;
    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    if-ne v3, p1, :cond_2

    .line 566
    iget-boolean v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    goto :goto_0
.end method

.method protected getPopupHeight()F
    .locals 1

    .prologue
    .line 974
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method protected getPopupMenu()Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 982
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 983
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    .line 986
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 715
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 717
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 718
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 720
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mCount:F

    .line 721
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 723
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 724
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    .line 726
    :cond_1
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 621
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 622
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    .line 624
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setItemEnabled(IZ)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "enable"    # Z

    .prologue
    .line 539
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_1

    .line 547
    :cond_0
    return-void

    .line 542
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    .line 543
    .local v0, "contextMenuItemInfo":Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;
    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    if-ne v2, p1, :cond_2

    .line 544
    iput-boolean p2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    goto :goto_0
.end method

.method public setRect(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 525
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 526
    return-void
.end method

.method public show()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 579
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPrevRect:Landroid/graphics/Rect;

    if-nez v1, :cond_1

    .line 609
    :cond_0
    :goto_0
    return-void

    .line 583
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->updateContextMenuLocation()V

    .line 585
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 586
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPrevRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPrevRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPrevRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    if-ne v1, v2, :cond_2

    .line 587
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPrevRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-eq v1, v2, :cond_0

    .line 590
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->hide()V

    .line 593
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPrevRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 595
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    if-le v1, v2, :cond_4

    .line 596
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v5}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/PopupWindow;->update(IIII)V

    .line 597
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v2, v6, v3, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 608
    :goto_1
    const/4 v1, 0x6

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->playScrollAnimation(I)V

    goto/16 :goto_0

    .line 599
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v2

    sub-int v0, v1, v2

    .line 600
    .local v0, "left":I
    if-gez v0, :cond_5

    .line 601
    const/4 v0, 0x0

    .line 604
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v5}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/PopupWindow;->update(IIII)V

    .line 605
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v2, v6, v0, v3}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_1
.end method

.method public show(I)V
    .locals 0
    .param p1, "delay"    # I

    .prologue
    .line 702
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->show()V

    .line 704
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mDelay:I

    .line 706
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->updateTimer(I)V

    .line 707
    return-void
.end method
