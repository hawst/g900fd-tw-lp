.class Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;
.super Ljava/lang/Object;
.source "SpenControlTextBox.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V
    .locals 0

    .prologue
    .line 1339
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;)V
    .locals 0

    .prologue
    .line 1339
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1342
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 1347
    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1352
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 1356
    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1361
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 1365
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mReceiveActionDown:Z
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-boolean v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsObjectChange:Z

    if-eqz v2, :cond_1

    .line 1392
    :cond_0
    :goto_0
    return v6

    .line 1368
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .line 1369
    .local v0, "objectTextBox":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    new-instance v1, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1370
    .local v1, "touchPoint":Landroid/graphics/PointF;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getControlPivotX(I)F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getControlPivotY(I)F

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mRotateAngle:F

    neg-float v5, v5

    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v1

    .line 1371
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v2

    iget v3, v1, Landroid/graphics/PointF;->x:F

    iget v4, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1372
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->isReadOnlyEnabled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1373
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->bringToFront()V

    .line 1374
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->enableTextInput(Z)V

    .line 1375
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 1376
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setEditable(Z)V

    .line 1378
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setStyle(I)V

    .line 1379
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v2, :cond_2

    .line 1380
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->hide()V

    .line 1385
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getStyle()I

    move-result v2

    if-eq v2, v7, :cond_0

    .line 1386
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v2, :cond_0

    .line 1387
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->updateContextMenu()V

    .line 1388
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->show()V

    goto/16 :goto_0
.end method
