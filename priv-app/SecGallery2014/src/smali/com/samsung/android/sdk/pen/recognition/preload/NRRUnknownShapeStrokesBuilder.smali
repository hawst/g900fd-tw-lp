.class Lcom/samsung/android/sdk/pen/recognition/preload/NRRUnknownShapeStrokesBuilder;
.super Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;
.source "NRRUnknownShapeStrokesBuilder.java"


# static fields
.field private static final CANVAS_DRAW_POINT_COUNT:I = 0x2

.field private static final LINEARIZE_ANGLE_CONSTANT:I = 0x1e

.field private static final LINEARIZE_MIDDLE_POINT_COUNT:I = 0xe

.field private static final MIN_RECOGNITION_DISTANCE:F = 10.0f

.field private static final SMALL_LINE_COEF:F = 0.25f


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;Lcom/samsung/recognitionengine/ShapeInfo;)V
    .locals 0
    .param p1, "penSettings"    # Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;
    .param p2, "shapeInfo"    # Lcom/samsung/recognitionengine/ShapeInfo;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;Lcom/samsung/recognitionengine/ShapeInfo;)V

    .line 38
    return-void
.end method

.method private getLinearizedObject()Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 31

    .prologue
    .line 53
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUnknownShapeStrokesBuilder;->mShapeInfo:Lcom/samsung/recognitionengine/ShapeInfo;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/recognitionengine/ShapeInfo;->getRecognizedPoints()Lcom/samsung/recognitionengine/PointFVector;

    move-result-object v18

    .line 54
    .local v18, "recognizedPoints":Lcom/samsung/recognitionengine/PointFVector;
    const-wide/high16 v28, 0x403e000000000000L    # 30.0

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-float v0, v0

    move/from16 v28, v0

    .line 55
    const/high16 v29, 0x41200000    # 10.0f

    const/high16 v30, 0x3e800000    # 0.25f

    .line 54
    move-object/from16 v0, v18

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/recognitionengine/PolylineSmoother;->linearize(Lcom/samsung/recognitionengine/PointFVector;FFF)Lcom/samsung/recognitionengine/LinesFVector;

    move-result-object v11

    .line 56
    .local v11, "lineSegments":Lcom/samsung/recognitionengine/LinesFVector;
    invoke-virtual {v11}, Lcom/samsung/recognitionengine/LinesFVector;->size()J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v19, v0

    .line 57
    .local v19, "segmentsCount":I
    if-nez v19, :cond_0

    .line 58
    const/4 v5, 0x0

    .line 116
    :goto_0
    return-object v5

    .line 60
    :cond_0
    new-instance v5, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>()V

    .line 61
    .local v5, "container":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    new-instance v24, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-direct/range {v24 .. v24}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;-><init>()V

    .line 63
    .local v24, "stroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    mul-int/lit8 v28, v19, 0x2

    mul-int/lit8 v29, v19, 0xe

    add-int v28, v28, v29

    move/from16 v0, v28

    new-array v0, v0, [Landroid/graphics/PointF;

    move-object/from16 v16, v0

    .line 64
    .local v16, "pointsToAdd":[Landroid/graphics/PointF;
    mul-int/lit8 v28, v19, 0x2

    mul-int/lit8 v29, v19, 0xe

    add-int v28, v28, v29

    move/from16 v0, v28

    new-array v0, v0, [F

    move-object/from16 v17, v0

    .line 65
    .local v17, "pressures":[F
    mul-int/lit8 v28, v19, 0x2

    mul-int/lit8 v29, v19, 0xe

    add-int v28, v28, v29

    move/from16 v0, v28

    new-array v0, v0, [I

    move-object/from16 v25, v0

    .line 66
    .local v25, "timestamps":[I
    const/high16 v28, 0x3f800000    # 1.0f

    move-object/from16 v0, v17

    move/from16 v1, v28

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 68
    const/4 v15, 0x0

    .line 69
    .local v15, "pointIndex":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    move/from16 v0, v19

    if-lt v8, v0, :cond_1

    .line 109
    move-object/from16 v0, v24

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    move-object/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setPoints([Landroid/graphics/PointF;[F[I)V

    .line 111
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUnknownShapeStrokesBuilder;->setStrokeStyle(Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;)V

    .line 112
    const/16 v28, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setToolType(I)V

    .line 113
    const/16 v28, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setCurveEnabled(Z)V

    .line 114
    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto :goto_0

    .line 70
    :cond_1
    invoke-virtual {v11, v8}, Lcom/samsung/recognitionengine/LinesFVector;->get(I)Lcom/samsung/recognitionengine/LineF;

    move-result-object v10

    .line 71
    .local v10, "lineSegment":Lcom/samsung/recognitionengine/LineF;
    add-int/lit8 v28, v8, 0x1

    move/from16 v0, v28

    move/from16 v1, v19

    if-ge v0, v1, :cond_3

    .line 74
    add-int/lit8 v28, v8, 0x1

    move/from16 v0, v28

    invoke-virtual {v11, v0}, Lcom/samsung/recognitionengine/LinesFVector;->get(I)Lcom/samsung/recognitionengine/LineF;

    move-result-object v14

    .line 75
    .local v14, "nextLineSegment":Lcom/samsung/recognitionengine/LineF;
    invoke-virtual {v10}, Lcom/samsung/recognitionengine/LineF;->getEndPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v28

    invoke-virtual {v14}, Lcom/samsung/recognitionengine/LineF;->getStartPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v29

    cmpl-float v28, v28, v29

    if-nez v28, :cond_2

    .line 76
    invoke-virtual {v10}, Lcom/samsung/recognitionengine/LineF;->getEndPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v28

    invoke-virtual {v14}, Lcom/samsung/recognitionengine/LineF;->getStartPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v29

    cmpl-float v28, v28, v29

    if-eqz v28, :cond_3

    .line 77
    :cond_2
    invoke-virtual {v14}, Lcom/samsung/recognitionengine/LineF;->getStartPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v28

    invoke-virtual {v10}, Lcom/samsung/recognitionengine/LineF;->getEndPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v29

    add-float v28, v28, v29

    const/high16 v29, 0x40000000    # 2.0f

    div-float v26, v28, v29

    .line 78
    .local v26, "x":F
    invoke-virtual {v14}, Lcom/samsung/recognitionengine/LineF;->getStartPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v28

    invoke-virtual {v10}, Lcom/samsung/recognitionengine/LineF;->getEndPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v29

    add-float v28, v28, v29

    const/high16 v29, 0x40000000    # 2.0f

    div-float v27, v28, v29

    .line 79
    .local v27, "y":F
    new-instance v4, Lcom/samsung/recognitionengine/PointF;

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-direct {v4, v0, v1}, Lcom/samsung/recognitionengine/PointF;-><init>(FF)V

    .line 80
    .local v4, "anglePoint":Lcom/samsung/recognitionengine/PointF;
    new-instance v28, Lcom/samsung/recognitionengine/LineF;

    invoke-virtual {v10}, Lcom/samsung/recognitionengine/LineF;->getStartPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v29

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-direct {v0, v1, v4}, Lcom/samsung/recognitionengine/LineF;-><init>(Lcom/samsung/recognitionengine/PointF;Lcom/samsung/recognitionengine/PointF;)V

    move-object/from16 v0, v28

    invoke-virtual {v11, v8, v0}, Lcom/samsung/recognitionengine/LinesFVector;->set(ILcom/samsung/recognitionengine/LineF;)V

    .line 81
    add-int/lit8 v28, v8, 0x1

    new-instance v29, Lcom/samsung/recognitionengine/LineF;

    invoke-virtual {v14}, Lcom/samsung/recognitionengine/LineF;->getEndPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v30

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    invoke-direct {v0, v4, v1}, Lcom/samsung/recognitionengine/LineF;-><init>(Lcom/samsung/recognitionengine/PointF;Lcom/samsung/recognitionengine/PointF;)V

    move/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v11, v0, v1}, Lcom/samsung/recognitionengine/LinesFVector;->set(ILcom/samsung/recognitionengine/LineF;)V

    .line 85
    .end local v4    # "anglePoint":Lcom/samsung/recognitionengine/PointF;
    .end local v14    # "nextLineSegment":Lcom/samsung/recognitionengine/LineF;
    .end local v26    # "x":F
    .end local v27    # "y":F
    :cond_3
    invoke-virtual {v10}, Lcom/samsung/recognitionengine/LineF;->getStartPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v20

    .line 86
    .local v20, "startX":F
    invoke-virtual {v10}, Lcom/samsung/recognitionengine/LineF;->getStartPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v21

    .line 87
    .local v21, "startY":F
    new-instance v28, Landroid/graphics/PointF;

    move-object/from16 v0, v28

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v28, v16, v15

    .line 88
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v28, v0

    aput v28, v25, v15

    .line 89
    add-int/lit8 v15, v15, 0x1

    .line 93
    invoke-virtual {v10}, Lcom/samsung/recognitionengine/LineF;->getEndPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v28

    invoke-virtual {v10}, Lcom/samsung/recognitionengine/LineF;->getStartPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v29

    sub-float v28, v28, v29

    const/high16 v29, 0x41700000    # 15.0f

    div-float v22, v28, v29

    .line 94
    .local v22, "stepX":F
    invoke-virtual {v10}, Lcom/samsung/recognitionengine/LineF;->getEndPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v28

    invoke-virtual {v10}, Lcom/samsung/recognitionengine/LineF;->getStartPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v29

    sub-float v28, v28, v29

    const/high16 v29, 0x41700000    # 15.0f

    div-float v23, v28, v29

    .line 95
    .local v23, "stepY":F
    const/4 v9, 0x1

    .local v9, "j":I
    :goto_2
    const/16 v28, 0xe

    move/from16 v0, v28

    if-le v9, v0, :cond_4

    .line 102
    invoke-virtual {v10}, Lcom/samsung/recognitionengine/LineF;->getEndPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v6

    .line 103
    .local v6, "endX":F
    invoke-virtual {v10}, Lcom/samsung/recognitionengine/LineF;->getEndPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v7

    .line 104
    .local v7, "endY":F
    new-instance v28, Landroid/graphics/PointF;

    move-object/from16 v0, v28

    invoke-direct {v0, v6, v7}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v28, v16, v15

    .line 105
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v28, v0

    aput v28, v25, v15

    .line 106
    add-int/lit8 v15, v15, 0x1

    .line 69
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 96
    .end local v6    # "endX":F
    .end local v7    # "endY":F
    :cond_4
    invoke-virtual {v10}, Lcom/samsung/recognitionengine/LineF;->getStartPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v28

    int-to-float v0, v9

    move/from16 v29, v0

    mul-float v29, v29, v22

    add-float v12, v28, v29

    .line 97
    .local v12, "midleX":F
    invoke-virtual {v10}, Lcom/samsung/recognitionengine/LineF;->getStartPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v28

    int-to-float v0, v9

    move/from16 v29, v0

    mul-float v29, v29, v23

    add-float v13, v28, v29

    .line 98
    .local v13, "midleY":F
    new-instance v28, Landroid/graphics/PointF;

    move-object/from16 v0, v28

    invoke-direct {v0, v12, v13}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v28, v16, v15

    .line 99
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v28, v0

    aput v28, v25, v15

    .line 100
    add-int/lit8 v15, v15, 0x1

    .line 95
    add-int/lit8 v9, v9, 0x1

    goto :goto_2
.end method

.method private getSmothedObject()Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 30

    .prologue
    .line 120
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUnknownShapeStrokesBuilder;->mShapeInfo:Lcom/samsung/recognitionengine/ShapeInfo;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/recognitionengine/ShapeInfo;->getRecognizedPoints()Lcom/samsung/recognitionengine/PointFVector;

    move-result-object v17

    .line 121
    .local v17, "recognizedPoints":Lcom/samsung/recognitionengine/PointFVector;
    new-instance v22, Lcom/samsung/recognitionengine/PolylineSmoother;

    invoke-direct/range {v22 .. v22}, Lcom/samsung/recognitionengine/PolylineSmoother;-><init>()V

    .line 123
    .local v22, "smoother":Lcom/samsung/recognitionengine/PolylineSmoother;
    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/recognitionengine/PolylineSmoother;->smoothPolyline(Lcom/samsung/recognitionengine/PointFVector;)Lcom/samsung/recognitionengine/ShapeInfoVector;

    move-result-object v21

    .line 124
    .local v21, "smoothedSegments":Lcom/samsung/recognitionengine/ShapeInfoVector;
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/recognitionengine/ShapeInfoVector;->size()J

    move-result-wide v26

    move-wide/from16 v0, v26

    long-to-int v0, v0

    move/from16 v19, v0

    .line 125
    .local v19, "segmentsCount":I
    if-nez v19, :cond_0

    .line 126
    const/4 v4, 0x0

    .line 171
    :goto_0
    return-object v4

    .line 128
    :cond_0
    new-instance v4, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>()V

    .line 129
    .local v4, "container":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    new-instance v23, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-direct/range {v23 .. v23}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;-><init>()V

    .line 131
    .local v23, "stroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    const/4 v14, 0x0

    .line 132
    .local v14, "pointsCount":I
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v19}, Ljava/util/ArrayList;-><init>(I)V

    .line 133
    .local v18, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/recognitionengine/VectorPointFVectors;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    move/from16 v0, v19

    if-lt v6, v0, :cond_2

    .line 144
    new-array v15, v14, [Landroid/graphics/PointF;

    .line 145
    .local v15, "pointsToAdd":[Landroid/graphics/PointF;
    new-array v0, v14, [F

    move-object/from16 v16, v0

    .line 146
    .local v16, "pressures":[F
    new-array v0, v14, [I

    move-object/from16 v24, v0

    .line 147
    .local v24, "timestamps":[I
    const/high16 v25, 0x3f800000    # 1.0f

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 149
    const/4 v10, 0x0

    .line 150
    .local v10, "pointIndex":I
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :cond_1
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-nez v26, :cond_4

    .line 164
    move-object/from16 v0, v23

    move-object/from16 v1, v16

    move-object/from16 v2, v24

    invoke-virtual {v0, v15, v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setPoints([Landroid/graphics/PointF;[F[I)V

    .line 166
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUnknownShapeStrokesBuilder;->setStrokeStyle(Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;)V

    .line 167
    const/16 v25, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setToolType(I)V

    .line 168
    const/16 v25, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setCurveEnabled(Z)V

    .line 169
    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto :goto_0

    .line 134
    .end local v10    # "pointIndex":I
    .end local v15    # "pointsToAdd":[Landroid/graphics/PointF;
    .end local v16    # "pressures":[F
    .end local v24    # "timestamps":[I
    :cond_2
    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Lcom/samsung/recognitionengine/ShapeInfoVector;->get(I)Lcom/samsung/recognitionengine/ShapeInfo;

    move-result-object v20

    .line 136
    .local v20, "shapeSegment":Lcom/samsung/recognitionengine/ShapeInfo;
    const/16 v5, 0x19

    .line 137
    .local v5, "generateCount":I
    int-to-long v0, v5

    move-wide/from16 v26, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeInfo;->generatePoints(J)Lcom/samsung/recognitionengine/VectorPointFVectors;

    move-result-object v11

    .line 138
    .local v11, "pointVector":Lcom/samsung/recognitionengine/VectorPointFVectors;
    move-object/from16 v0, v18

    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    invoke-virtual {v11}, Lcom/samsung/recognitionengine/VectorPointFVectors;->size()J

    move-result-wide v26

    move-wide/from16 v0, v26

    long-to-int v12, v0

    .line 140
    .local v12, "pointVectorSize":I
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_2
    if-lt v7, v12, :cond_3

    .line 133
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 141
    :cond_3
    int-to-long v0, v14

    move-wide/from16 v26, v0

    invoke-virtual {v11, v7}, Lcom/samsung/recognitionengine/VectorPointFVectors;->get(I)Lcom/samsung/recognitionengine/PointFVector;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/recognitionengine/PointFVector;->size()J

    move-result-wide v28

    add-long v26, v26, v28

    move-wide/from16 v0, v26

    long-to-int v14, v0

    .line 140
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 150
    .end local v5    # "generateCount":I
    .end local v7    # "j":I
    .end local v11    # "pointVector":Lcom/samsung/recognitionengine/VectorPointFVectors;
    .end local v12    # "pointVectorSize":I
    .end local v20    # "shapeSegment":Lcom/samsung/recognitionengine/ShapeInfo;
    .restart local v10    # "pointIndex":I
    .restart local v15    # "pointsToAdd":[Landroid/graphics/PointF;
    .restart local v16    # "pressures":[F
    .restart local v24    # "timestamps":[I
    :cond_4
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/recognitionengine/VectorPointFVectors;

    .line 151
    .restart local v11    # "pointVector":Lcom/samsung/recognitionengine/VectorPointFVectors;
    invoke-virtual {v11}, Lcom/samsung/recognitionengine/VectorPointFVectors;->size()J

    move-result-wide v26

    move-wide/from16 v0, v26

    long-to-int v12, v0

    .line 153
    .restart local v12    # "pointVectorSize":I
    const/4 v7, 0x0

    .restart local v7    # "j":I
    :goto_3
    if-ge v7, v12, :cond_1

    .line 154
    invoke-virtual {v11, v7}, Lcom/samsung/recognitionengine/VectorPointFVectors;->get(I)Lcom/samsung/recognitionengine/PointFVector;

    move-result-object v13

    .line 155
    .local v13, "points":Lcom/samsung/recognitionengine/PointFVector;
    invoke-virtual {v13}, Lcom/samsung/recognitionengine/PointFVector;->size()J

    move-result-wide v26

    move-wide/from16 v0, v26

    long-to-int v14, v0

    .line 156
    const/4 v8, 0x0

    .local v8, "l":I
    :goto_4
    if-lt v8, v14, :cond_5

    .line 153
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 157
    :cond_5
    invoke-virtual {v13, v8}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v9

    .line 158
    .local v9, "p":Lcom/samsung/recognitionengine/PointF;
    new-instance v26, Landroid/graphics/PointF;

    invoke-virtual {v9}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v27

    invoke-virtual {v9}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v28

    invoke-direct/range {v26 .. v28}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v26, v15, v10

    .line 159
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v26

    move-wide/from16 v0, v26

    long-to-int v0, v0

    move/from16 v26, v0

    aput v26, v24, v10

    .line 160
    add-int/lit8 v10, v10, 0x1

    .line 156
    add-int/lit8 v8, v8, 0x1

    goto :goto_4
.end method


# virtual methods
.method public buildLayoutObject()Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 2

    .prologue
    .line 41
    const/4 v0, 0x0

    .line 44
    .local v0, "container":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUnknownShapeStrokesBuilder;->getLinearizedObject()Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 49
    :goto_0
    return-object v0

    .line 45
    :catch_0
    move-exception v1

    .line 46
    .local v1, "e":Ljava/lang/Exception;
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUnknownShapeStrokesBuilder;->getSmothedObject()Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    goto :goto_0
.end method
