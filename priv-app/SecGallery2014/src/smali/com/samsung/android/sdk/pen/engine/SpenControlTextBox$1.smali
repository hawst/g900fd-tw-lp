.class Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;
.super Ljava/lang/Object;
.source "SpenControlTextBox.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    .line 1258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onExceedLimit()V
    .locals 0

    .prologue
    .line 1290
    return-void
.end method

.method public onFocusChanged(Z)V
    .locals 1
    .param p1, "gainFocus"    # Z

    .prologue
    .line 1333
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1334
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;->onFocusChanged(Z)V

    .line 1336
    :cond_0
    return-void
.end method

.method public onMoreButtonDown(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 1
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .prologue
    .line 1303
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1304
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;->onMoreButtonDown(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    .line 1306
    :cond_0
    return-void
.end method

.method public onObjectChanged(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 1
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .prologue
    .line 1284
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    .line 1285
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->invalidate()V

    .line 1286
    return-void
.end method

.method public onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
    .locals 1
    .param p1, "coordinateInfo"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .prologue
    .line 1310
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    .line 1311
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 1313
    :cond_0
    return-void
.end method

.method public onRequestScroll(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 1261
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onRequestScroll(FF)V

    .line 1262
    return-void
.end method

.method public onSelectionChanged(II)Z
    .locals 1
    .param p1, "selStart"    # I
    .param p2, "selEnd"    # I

    .prologue
    .line 1294
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1295
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;->onSelectionChanged(II)Z

    move-result v0

    .line 1298
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    .line 1266
    if-eqz p1, :cond_0

    .line 1267
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    .line 1268
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 1269
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 1270
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget-object v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 1271
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    .line 1272
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    .line 1273
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    .line 1274
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    .line 1276
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1277
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;->onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 1280
    :cond_0
    return-void
.end method

.method public onUndo()V
    .locals 2

    .prologue
    .line 1317
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;Z)V

    .line 1319
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onObjectChanged()V

    .line 1321
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;Z)V

    .line 1322
    return-void
.end method

.method public onVisibleUpdated(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;Z)V
    .locals 2
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    .param p2, "visible"    # Z

    .prologue
    .line 1326
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    .line 1327
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onVisibleUpdated(Ljava/util/ArrayList;Z)V

    .line 1329
    :cond_0
    return-void
.end method
