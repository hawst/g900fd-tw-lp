.class public Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;
.super Ljava/lang/Object;
.source "SpenHighlightInfo.java"


# instance fields
.field public color:I

.field public rect:Landroid/graphics/RectF;

.field public size:F


# direct methods
.method public constructor <init>(Landroid/graphics/RectF;FI)V
    .locals 0
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "size"    # F
    .param p3, "color"    # I

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->rect:Landroid/graphics/RectF;

    .line 63
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->size:F

    .line 65
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->color:I

    .line 67
    return-void
.end method
