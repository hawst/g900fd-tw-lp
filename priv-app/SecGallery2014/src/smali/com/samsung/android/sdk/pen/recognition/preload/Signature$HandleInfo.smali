.class Lcom/samsung/android/sdk/pen/recognition/preload/Signature$HandleInfo;
.super Ljava/lang/Object;
.source "Signature.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/recognition/preload/Signature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HandleInfo"
.end annotation


# instance fields
.field private mInput:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;"
        }
    .end annotation
.end field

.field mResult:Z


# direct methods
.method constructor <init>(Ljava/util/List;Z)V
    .locals 1
    .param p2, "result"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 248
    .local p1, "input":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$HandleInfo;->mInput:Ljava/util/List;

    .line 246
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$HandleInfo;->mResult:Z

    .line 249
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$HandleInfo;->mInput:Ljava/util/List;

    .line 250
    iput-boolean p2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$HandleInfo;->mResult:Z

    .line 251
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/recognition/preload/Signature$HandleInfo;)Ljava/util/List;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$HandleInfo;->mInput:Ljava/util/List;

    return-object v0
.end method
