.class Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;
.super Landroid/widget/ImageView;
.source "SpenColorGradationView2.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;
    }
.end annotation


# instance fields
.field private mBorderPaint:Landroid/graphics/Paint;

.field private mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;

.field private mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

.field private mCursorRect:Landroid/graphics/Rect;

.field private mCustom_imagepath:Ljava/lang/String;

.field private mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

.field mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private mHeight:I

.field private mSpectrum:Landroid/graphics/Bitmap;

.field private mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;F)V
    .locals 2
    .param p1, "paramContext"    # Landroid/content/Context;
    .param p2, "custom_imagepath"    # Ljava/lang/String;
    .param p3, "ratio"    # F

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 36
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCustom_imagepath:Ljava/lang/String;

    .line 48
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCustom_imagepath:Ljava/lang/String;

    .line 49
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCustom_imagepath:Ljava/lang/String;

    invoke-direct {v0, p1, v1, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 50
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->initView()V

    .line 51
    return-void
.end method

.method private static equalsColor(IID)Z
    .locals 10
    .param p0, "color"    # I
    .param p1, "color2"    # I
    .param p2, "deltaColor"    # D

    .prologue
    const/high16 v9, 0xff0000

    const/high16 v8, 0x10000

    const v7, 0xff00

    .line 227
    and-int v6, p0, v9

    div-int v4, v6, v8

    .line 228
    .local v4, "r1":I
    and-int v6, p0, v7

    div-int/lit16 v2, v6, 0x100

    .line 229
    .local v2, "g1":I
    and-int/lit16 v0, p0, 0xff

    .line 231
    .local v0, "b1":I
    and-int v6, p1, v9

    div-int v5, v6, v8

    .line 232
    .local v5, "r2":I
    and-int v6, p1, v7

    div-int/lit16 v3, v6, 0x100

    .line 233
    .local v3, "g2":I
    and-int/lit16 v1, p1, 0xff

    .line 234
    .local v1, "b2":I
    sub-int v6, v4, v5

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    int-to-double v6, v6

    cmpg-double v6, v6, p2

    if-gez v6, :cond_0

    sub-int v6, v2, v3

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    int-to-double v6, v6

    cmpg-double v6, v6, p2

    if-gez v6, :cond_0

    sub-int v6, v0, v1

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    int-to-double v6, v6

    cmpg-double v6, v6, p2

    if-gez v6, :cond_0

    .line 235
    const/4 v6, 0x1

    .line 237
    :goto_0
    return v6

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private initView()V
    .locals 8

    .prologue
    const/16 v7, 0x105

    const/16 v6, 0x41

    const/4 v5, 0x0

    .line 245
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v1, :cond_0

    .line 246
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v2, "snote_color_select_kit"

    const/16 v3, 0xf

    const/16 v4, 0x11

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 248
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mWidth:I

    .line 249
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mHeight:I

    .line 250
    new-instance v1, Landroid/graphics/Rect;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mWidth:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mHeight:I

    invoke-direct {v1, v5, v5, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorRect:Landroid/graphics/Rect;

    .line 251
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 253
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v2, "snote_color_box_shadow"

    invoke-virtual {v1, v2, v7, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 254
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v2, "snote_color_box_shadow"

    invoke-virtual {v1, v2, v7, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    .line 255
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v1

    .line 256
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v2

    .line 255
    invoke-direct {v0, v5, v5, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 257
    .local v0, "mGradationRect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 259
    .end local v0    # "mGradationRect":Landroid/graphics/Rect;
    :cond_1
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mBorderPaint:Landroid/graphics/Paint;

    .line 260
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mBorderPaint:Landroid/graphics/Paint;

    const v2, -0x737374

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 261
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mBorderPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 262
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mBorderPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 263
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 57
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_2

    .line 60
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 61
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 63
    :cond_1
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    .line 65
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_4

    .line 66
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 67
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 69
    :cond_3
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 73
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_6

    .line 74
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 75
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 77
    :cond_5
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 80
    :cond_6
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;

    .line 82
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 84
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorRect:Landroid/graphics/Rect;

    .line 85
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mBorderPaint:Landroid/graphics/Paint;

    .line 87
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    .line 147
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 148
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 150
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mBorderPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 153
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 156
    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 4
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 184
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 186
    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_2

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 188
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 189
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    .line 193
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_color_box"

    const/16 v2, 0x105

    const/16 v3, 0x41

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 194
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    .line 196
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 198
    :cond_2
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v10, 0x1

    const v9, 0xffffff

    .line 98
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 99
    .local v1, "i":I
    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    invoke-interface {v4, v10}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 103
    :cond_0
    const/4 v0, -0x1

    .line 105
    .local v0, "color":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v2, v4

    .line 106
    .local v2, "mCurX":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v3, v4

    .line 107
    .local v3, "mCurY":I
    if-gez v2, :cond_1

    .line 108
    const/4 v2, 0x0

    .line 110
    :cond_1
    if-gez v3, :cond_2

    .line 111
    const/4 v3, 0x0

    .line 113
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_5

    .line 114
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    if-gt v4, v2, :cond_3

    .line 115
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 117
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-gt v4, v3, :cond_4

    .line 118
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    add-int/lit8 v3, v4, -0x1

    .line 120
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v2, v3}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    .line 123
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorRect:Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mWidth:I

    div-int/lit8 v5, v5, 0x2

    sub-int v5, v2, v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mHeight:I

    div-int/lit8 v6, v6, 0x2

    sub-int v6, v3, v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mWidth:I

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v7, v2

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mHeight:I

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v8, v3

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 125
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorRect:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 127
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;

    if-eqz v4, :cond_7

    .line 128
    and-int v4, v0, v9

    if-ne v4, v9, :cond_6

    .line 129
    const v0, 0xfefefe

    .line 131
    :cond_6
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;

    const/high16 v5, -0x2000000

    and-int v6, v9, v0

    or-int/2addr v5, v6

    invoke-interface {v4, v5, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;->onColorChanged(III)V

    .line 133
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->invalidate()V

    .line 134
    return v10
.end method

.method public selectColorForGradiation(ID)V
    .locals 8
    .param p1, "color"    # I
    .param p2, "deltaColor"    # D

    .prologue
    .line 202
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v3, :cond_0

    .line 203
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v4, "snote_color_box"

    const/16 v5, 0x105

    const/16 v6, 0x41

    invoke-virtual {v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 205
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    if-nez v3, :cond_1

    .line 206
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    .line 208
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-nez v3, :cond_2

    .line 209
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 212
    :cond_2
    const/4 v1, 0x0

    .local v1, "mCurX":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-lt v1, v3, :cond_3

    .line 224
    :goto_1
    return-void

    .line 213
    :cond_3
    const/4 v2, 0x0

    .local v2, "mCurY":I
    :goto_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-lt v2, v3, :cond_4

    .line 212
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 214
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    .line 215
    .local v0, "color2":I
    invoke-static {p1, v0, p2, p3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->equalsColor(IID)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 217
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorRect:Landroid/graphics/Rect;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mWidth:I

    div-int/lit8 v4, v4, 0x2

    sub-int v4, v1, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mHeight:I

    div-int/lit8 v5, v5, 0x2

    sub-int v5, v2, v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mWidth:I

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v6, v1

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mHeight:I

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v7, v2

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 218
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 219
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->invalidate()V

    goto :goto_1

    .line 213
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public setColorPickerColorChangeListener(Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;)V
    .locals 0
    .param p1, "parama"    # Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;

    .line 167
    return-void
.end method
