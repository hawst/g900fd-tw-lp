.class Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$1;
.super Ljava/lang/Object;
.source "SpenColorPickerLayout2.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    .line 572
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "oldLeft"    # I
    .param p7, "oldTop"    # I
    .param p8, "oldRight"    # I
    .param p9, "oldBottom"    # I

    .prologue
    .line 578
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    if-nez v1, :cond_1

    .line 597
    :cond_0
    :goto_0
    return-void

    .line 581
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    .line 584
    if-eq p3, p5, :cond_0

    .line 588
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mIsRotated:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 589
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->rotatePosition()V

    .line 590
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 594
    :catch_0
    move-exception v0

    .line 595
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 592
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->checkPosition()V
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
