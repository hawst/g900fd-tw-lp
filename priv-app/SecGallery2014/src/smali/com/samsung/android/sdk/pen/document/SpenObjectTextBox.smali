.class public Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
.super Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
.source "SpenObjectTextBox.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BulletParagraphInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$IndentLevelParagraphInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;
    }
.end annotation


# static fields
.field public static final ALIGN_BOTH:I = 0x3

.field public static final ALIGN_CENTER:I = 0x2

.field public static final ALIGN_LEFT:I = 0x0

.field public static final ALIGN_RIGHT:I = 0x1

.field public static final AUTO_FIT_BOTH:I = 0x3

.field public static final AUTO_FIT_HORIZONTAL:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final AUTO_FIT_NONE:I = 0x0

.field public static final AUTO_FIT_VERTICAL:I = 0x2

.field public static final BORDER_TYPE_DOT:I = 0x3

.field public static final BORDER_TYPE_NONE:I = 0x0

.field public static final BORDER_TYPE_SHADOW:I = 0x2

.field public static final BORDER_TYPE_SQUARE:I = 0x1

.field public static final BULLET_TYPE_ALPHABET:I = 0x6

.field public static final BULLET_TYPE_ARROW:I = 0x1

.field public static final BULLET_TYPE_CHECKER:I = 0x2

.field public static final BULLET_TYPE_CIRCLED_DIGIT:I = 0x5

.field public static final BULLET_TYPE_DIAMOND:I = 0x3

.field public static final BULLET_TYPE_DIGIT:I = 0x4

.field public static final BULLET_TYPE_NONE:I = 0x0

.field public static final BULLET_TYPE_ROMAN_NUMERAL:I = 0x7

.field public static final CURSOR_POS_END:I = 0x1

.field public static final CURSOR_POS_START:I = 0x0

.field public static final DIRECTION_LEFT_TO_RIGHT:I = 0x0

.field public static final DIRECTION_RIGHT_TO_LEFT:I = 0x1

.field public static final ELLIPSIS_DOTS:I = 0x1

.field public static final ELLIPSIS_NONE:I = 0x0

.field public static final ELLIPSIS_TRIANGLE:I = 0x2

.field public static final GRAVITY_BOTTOM:I = 0x2

.field public static final GRAVITY_CENTER:I = 0x1

.field public static final GRAVITY_TOP:I = 0x0

.field public static final HYPER_TEXT_ADDRESS:I = 0x5

.field public static final HYPER_TEXT_DATE:I = 0x4

.field public static final HYPER_TEXT_EMAIL:I = 0x1

.field public static final HYPER_TEXT_TEL:I = 0x2

.field public static final HYPER_TEXT_UNKNOWN:I = 0x0

.field public static final HYPER_TEXT_URL:I = 0x3

.field public static final IMEACTION_PREVIOUS:I = 0x7

.field public static final IME_ACTION_DONE:I = 0x4

.field public static final IME_ACTION_GO:I = 0x2

.field public static final IME_ACTION_NEXT:I = 0x6

.field public static final IME_ACTION_NONE:I = 0x1

.field public static final IME_ACTION_SEARCH:I = 0x3

.field public static final IME_ACTION_SEND:I = 0x5

.field public static final IME_ACTION_UNSPECIFIED:I = 0x0

.field public static final INPUT_TYPE_DATETIME:I = 0x4

.field public static final INPUT_TYPE_NONE:I = 0x0

.field public static final INPUT_TYPE_NUMBER:I = 0x2

.field public static final INPUT_TYPE_PHONE:I = 0x3

.field public static final INPUT_TYPE_TEXT:I = 0x1

.field public static final LINE_SPACING_PERCENT:I = 0x1

.field public static final LINE_SPACING_PIXEL:I = 0x0

.field public static final STYLE_BOLD:I = 0x1

.field public static final STYLE_ITALIC:I = 0x2

.field public static final STYLE_MASK:I = 0x7

.field public static final STYLE_NONE:I = 0x0

.field public static final STYLE_UNDERLINE:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 846
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 847
    return-void
.end method

.method protected constructor <init>(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 837
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 838
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 873
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 874
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init2(Ljava/lang/String;)Z

    move-result v0

    .line 875
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 876
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 878
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 911
    .local p2, "span":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 913
    if-eqz p2, :cond_1

    .line 914
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 923
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init3(Ljava/lang/String;Ljava/util/ArrayList;)Z

    move-result v0

    .line 924
    .local v0, "rnt":Z
    if-nez v0, :cond_2

    .line 925
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 927
    .end local v0    # "rnt":Z
    :cond_2
    :goto_0
    return-void

    .line 914
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 915
    .local v1, "temp":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ltz v3, :cond_4

    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-gez v3, :cond_0

    .line 916
    :cond_4
    const/4 v2, 0x7

    .line 917
    const-string/jumbo v3, "startPos and endPos of TextSpanInfo should have positive value"

    .line 916
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 979
    .local p2, "span":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    .local p3, "paras":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 981
    if-eqz p2, :cond_1

    .line 982
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 991
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init4(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v0

    .line 992
    .local v0, "rnt":Z
    if-nez v0, :cond_2

    .line 993
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 996
    .end local v0    # "rnt":Z
    :cond_2
    :goto_0
    return-void

    .line 982
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 983
    .local v1, "temp":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ltz v3, :cond_4

    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-gez v3, :cond_0

    .line 984
    :cond_4
    const/4 v2, 0x7

    .line 985
    const-string/jumbo v3, "startPos and endPos of TextSpanInfo should have positive value"

    .line 984
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .param p4, "isTemplateObject"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1019
    .local p2, "span":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    .local p3, "paras":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 1021
    if-eqz p2, :cond_1

    .line 1022
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1031
    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init5(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Z

    move-result v0

    .line 1032
    .local v0, "rnt":Z
    if-nez v0, :cond_2

    .line 1033
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1036
    .end local v0    # "rnt":Z
    :cond_2
    :goto_0
    return-void

    .line 1022
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 1023
    .local v1, "temp":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ltz v3, :cond_4

    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-gez v3, :cond_0

    .line 1024
    :cond_4
    const/4 v2, 0x7

    .line 1025
    const-string/jumbo v3, "startPos and endPos of TextSpanInfo should have positive value"

    .line 1024
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;Z)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .param p3, "isTemplateObject"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 944
    .local p2, "span":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 946
    if-eqz p2, :cond_1

    .line 947
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 956
    :cond_1
    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, v2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init5(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Z

    move-result v0

    .line 957
    .local v0, "rnt":Z
    if-nez v0, :cond_2

    .line 958
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 960
    .end local v0    # "rnt":Z
    :cond_2
    :goto_0
    return-void

    .line 947
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 948
    .local v1, "temp":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ltz v3, :cond_4

    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-gez v3, :cond_0

    .line 949
    :cond_4
    const/4 v2, 0x7

    .line 950
    const-string/jumbo v3, "startPos and endPos of TextSpanInfo should have positive value"

    .line 949
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "isTemplateObject"    # Z

    .prologue
    const/4 v2, 0x0

    .line 891
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 892
    invoke-direct {p0, p1, v2, v2, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init5(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Z

    move-result v0

    .line 893
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 894
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 896
    :cond_0
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 3
    .param p1, "isTemplateObject"    # Z

    .prologue
    const/4 v2, 0x0

    .line 857
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 858
    invoke-direct {p0, v2, v2, v2, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init5(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Z

    move-result v0

    .line 859
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 860
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 862
    :cond_0
    return-void
.end method

.method private native ObjectTextBox_appendParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)Z
.end method

.method private native ObjectTextBox_appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)Z
.end method

.method private native ObjectTextBox_clearChangedFlag()V
.end method

.method private native ObjectTextBox_copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
.end method

.method private native ObjectTextBox_enableReadOnly(Z)Z
.end method

.method private native ObjectTextBox_findParagraphs(II)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;"
        }
    .end annotation
.end method

.method private native ObjectTextBox_findSpans(II)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;"
        }
    .end annotation
.end method

.method private native ObjectTextBox_getAutoFitOption()I
.end method

.method private native ObjectTextBox_getBackgroundColor()I
.end method

.method private native ObjectTextBox_getBorderType()I
.end method

.method private native ObjectTextBox_getBottomMargin()F
.end method

.method private native ObjectTextBox_getBulletType()I
.end method

.method private native ObjectTextBox_getCursorPos()I
.end method

.method private native ObjectTextBox_getDrawnRect()Landroid/graphics/RectF;
.end method

.method private native ObjectTextBox_getEllipsisType()I
.end method

.method private native ObjectTextBox_getFont()Ljava/lang/String;
.end method

.method private native ObjectTextBox_getFontSize()F
.end method

.method private native ObjectTextBox_getGravity()I
.end method

.method private native ObjectTextBox_getHintText()Ljava/lang/String;
.end method

.method private native ObjectTextBox_getHintTextColor()I
.end method

.method private native ObjectTextBox_getHintTextFontSize()F
.end method

.method private native ObjectTextBox_getIMEActionType()I
.end method

.method private native ObjectTextBox_getLeftMargin()F
.end method

.method private native ObjectTextBox_getLineBorderColor()I
.end method

.method private native ObjectTextBox_getLineBorderWidth()F
.end method

.method private native ObjectTextBox_getParagraph()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;"
        }
    .end annotation
.end method

.method private native ObjectTextBox_getRightMargin()F
.end method

.method private native ObjectTextBox_getSpan()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;"
        }
    .end annotation
.end method

.method private native ObjectTextBox_getText()Ljava/lang/String;
.end method

.method private native ObjectTextBox_getTextAlignment()I
.end method

.method private native ObjectTextBox_getTextColor()I
.end method

.method private native ObjectTextBox_getTextDirection()I
.end method

.method private native ObjectTextBox_getTextIndentLevel()I
.end method

.method private native ObjectTextBox_getTextInputType()I
.end method

.method private native ObjectTextBox_getTextLineSpacing()F
.end method

.method private native ObjectTextBox_getTextLineSpacingType()I
.end method

.method private native ObjectTextBox_getTextStyle()I
.end method

.method private native ObjectTextBox_getTopMargin()F
.end method

.method private native ObjectTextBox_getVerticalPan()F
.end method

.method private native ObjectTextBox_init1()Z
.end method

.method private native ObjectTextBox_init2(Ljava/lang/String;)Z
.end method

.method private native ObjectTextBox_init3(Ljava/lang/String;Ljava/util/ArrayList;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;)Z"
        }
    .end annotation
.end method

.method private native ObjectTextBox_init4(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;)Z"
        }
    .end annotation
.end method

.method private native ObjectTextBox_init5(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;Z)Z"
        }
    .end annotation
.end method

.method private native ObjectTextBox_insertChar(CI)Z
.end method

.method private native ObjectTextBox_insertCharAtCursor(C)Z
.end method

.method private native ObjectTextBox_insertText(Ljava/lang/String;I)Z
.end method

.method private native ObjectTextBox_insertTextAtCursor(Ljava/lang/String;)Z
.end method

.method private native ObjectTextBox_isChanged()Z
.end method

.method private native ObjectTextBox_isHintTextVisiable()Z
.end method

.method private native ObjectTextBox_isReadOnly()Z
.end method

.method private native ObjectTextBox_parseHyperText()Z
.end method

.method private native ObjectTextBox_removeAllText()Z
.end method

.method private native ObjectTextBox_removeParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)Z
.end method

.method private native ObjectTextBox_removeSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)Z
.end method

.method private native ObjectTextBox_removeText(II)Z
.end method

.method private native ObjectTextBox_replaceText(Ljava/lang/String;II)Z
.end method

.method private native ObjectTextBox_setAutoFitOption(I)Z
.end method

.method private native ObjectTextBox_setBackgroundColor(I)Z
.end method

.method private native ObjectTextBox_setBorderType(I)Z
.end method

.method private native ObjectTextBox_setBulletType(I)Z
.end method

.method private native ObjectTextBox_setCursorPos(I)Z
.end method

.method private native ObjectTextBox_setEllipsisType(I)Z
.end method

.method private native ObjectTextBox_setFont(Ljava/lang/String;)Z
.end method

.method private native ObjectTextBox_setFontSize(F)Z
.end method

.method private native ObjectTextBox_setGravity(I)Z
.end method

.method private native ObjectTextBox_setHintText(Ljava/lang/String;)Z
.end method

.method private native ObjectTextBox_setHintTextColor(I)Z
.end method

.method private native ObjectTextBox_setHintTextFontSize(F)Z
.end method

.method private native ObjectTextBox_setHintTextVisibility(Z)Z
.end method

.method private native ObjectTextBox_setIMEActionType(I)Z
.end method

.method private static native ObjectTextBox_setInitialCursorPos(I)Z
.end method

.method private native ObjectTextBox_setLineBorderColor(I)Z
.end method

.method private native ObjectTextBox_setLineBorderWidth(F)Z
.end method

.method private native ObjectTextBox_setMargin(FFFF)Z
.end method

.method private native ObjectTextBox_setParagraph(Ljava/util/ArrayList;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;)Z"
        }
    .end annotation
.end method

.method private native ObjectTextBox_setSpan(Ljava/util/ArrayList;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;)Z"
        }
    .end annotation
.end method

.method private native ObjectTextBox_setText(Ljava/lang/String;)Z
.end method

.method private native ObjectTextBox_setTextAlignment(I)Z
.end method

.method private native ObjectTextBox_setTextColor(I)Z
.end method

.method private native ObjectTextBox_setTextDirection(I)Z
.end method

.method private native ObjectTextBox_setTextIndentLevel(I)Z
.end method

.method private native ObjectTextBox_setTextInputType(I)Z
.end method

.method private native ObjectTextBox_setTextLineSpacingInfo(IF)Z
.end method

.method private native ObjectTextBox_setTextStyle(I)Z
.end method

.method private native ObjectTextBox_setVerticalPan(F)Z
.end method

.method private static parseHyperlink(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 38
    .param p0, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<[I>;"
        }
    .end annotation

    .prologue
    .line 2187
    new-instance v30, Ljava/util/ArrayList;

    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V

    .line 2189
    .local v30, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[I>;"
    const-string v7, "\\b(?:(?:[\\w]?[\\d]{1,4}(?:[\\-\\s]\\d[0-9a-zA-Z]{0,3})?)|(?:\\d[0-9a-zA-Z]{0,3}))\\b"

    .line 2190
    .local v7, "REGEX_ADDRESS_STREETNUM":Ljava/lang/String;
    const-string v6, "(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}"

    .line 2191
    .local v6, "REGEX_ADDRESS_SEPARATECITYSTATES":Ljava/lang/String;
    const-string v5, "\\b(?:(?:[\\d]{4})|(?:[a-zA-Z][a-zA-Z0-9]{1,3}[\\-\\s][0-9][a-zA-Z][a-zA-Z])|(?:[a-zA-Z][0-9][a-zA-Z][\\-\\s][0-9][a-zA-Z][0-9])|(?:[\\d]{5}(?:[\\-\\s][\\d]{4})?))"

    .line 2192
    .local v5, "REGEX_ADDRESS_POSTALCODE":Ljava/lang/String;
    const-string v3, "(?:[a-zA-Z\u00a0-\uaf00\\\'\\s]{2,20}(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}[a-zA-Z\u00a0-\uaf00\\\']{2,10})"

    .line 2194
    .local v3, "REGEX_ADDRESS_CITYSTATES":Ljava/lang/String;
    const-string v4, "(?:[\\t \\,][\\t ]?(?:[Uu]nited[\\t ])?[a-zA-Z\u00a0-\uaf00\\\'\\.]{2,20}\\b)?"

    .line 2195
    .local v4, "REGEX_ADDRESS_COUNTRY":Ljava/lang/String;
    const-string v8, "(?:(?:Ankara)|(?:Athens)|(?:Atlanta)|(?:Baghdad)|(?:Bandung)|(?:Bangalore)|(?:Bangkok)|(?:Barcelona)|(?:Beijing)|(?:Berlin)|(?:Bombay)|(?:Boston)|(?:Brasillia)|(?:Buenos\\sAires)|(?:Busan)|(?:Cairo)|(?:Calcutta)|(?:Casablandca)|(?:Chicago)|(?:Chongqing)|(?:Dallas)|(?:Delhi)|(?:Detroit)|(?:Dhaka)|(?:Guangzhou)|(?:Hanoi)|(?:Hong\\sKong)|(?:Houston)|(?:Istanbul)|(?:Karachi)|(?:Jakarta)|(?:Kobe)|(?:Lagos)|(?:Lahore)|(?:Lima)|(?:London)|(?:Los\\sAngeles)|(?:Madrid)|(?:Melbourne)|(?:Metro\\sManila)|(?:Mexico\\sCity)|(?:Miami)|(?:Milan)|(?:Montreal)|(?:Moscow)|(?:Mumbai)|(?:New\\sYork)|(?:Osaka)|(?:Paris)|(?:Philadelphia)|(?:Phoenix)|(?:Pusan)|(?:Rio\\sde\\sJaneiro)|(?:Santiago)|(?:Sao\\sPaulo)|(?:Seoul)|(?:Shanghai)|(?:Shenyang)|(?:Singapore)|(?:Sydney)|(?:Tehran)|(?:Tianjin)|(?:Tokyo)|(?:Toronto)|(?:Washington(?:[\\,\\s]{1,2}[Dd][\\.]?[Cc][\\.]?))|(?:Wuhan)|(?:Xi[\\\']?an))"

    .line 2203
    .local v8, "REGEX_ADDRESS_WORLDCITY":Ljava/lang/String;
    const-string v13, "(?<!((\\s[0-9]{1,2}:)|([0-9]{1,2}/)))\\b(?:(?:[\\w]?[\\d]{1,4}(?:[\\-\\s]\\d[0-9a-zA-Z]{0,3})?)|(?:\\d[0-9a-zA-Z]{0,3}))\\b(?:\\s|\\,|(?:\\&nbsp;)){1,3}[a-zA-Z\u00a0-\uaf00](?:[\u00a0-\uaf00\\\'\\w\\s#@\\-\\,\\.]{4,18})(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}(?i:<[^>]+>(?:[\\s\\x0d\\x0a]|(?:\\&nbsp;)){0,2}){0,5}(?:(?:(?:[a-zA-Z\u00a0-\uaf00\\\'\\s]{2,20}(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}[a-zA-Z\u00a0-\uaf00\\\']{2,10})(?:\\s|\\,|(?:\\&nbsp;)){1,3}\\b(?:(?:[\\d]{4})|(?:[a-zA-Z][a-zA-Z0-9]{1,3}[\\-\\s][0-9][a-zA-Z][a-zA-Z])|(?:[a-zA-Z][0-9][a-zA-Z][\\-\\s][0-9][a-zA-Z][0-9])|(?:[\\d]{5}(?:[\\-\\s][\\d]{4})?))\\b)|(?:\\b(?:(?:[\\d]{4})|(?:[a-zA-Z][a-zA-Z0-9]{1,3}[\\-\\s][0-9][a-zA-Z][a-zA-Z])|(?:[a-zA-Z][0-9][a-zA-Z][\\-\\s][0-9][a-zA-Z][0-9])|(?:[\\d]{5}(?:[\\-\\s][\\d]{4})?))(?:\\s|\\,|(?:\\&nbsp;)){1,3}(?:[a-zA-Z\u00a0-\uaf00\\\'\\s]{2,20}(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}[a-zA-Z\u00a0-\uaf00\\\']{2,10})\\b)|(?:(?:(?:Ankara)|(?:Athens)|(?:Atlanta)|(?:Baghdad)|(?:Bandung)|(?:Bangalore)|(?:Bangkok)|(?:Barcelona)|(?:Beijing)|(?:Berlin)|(?:Bombay)|(?:Boston)|(?:Brasillia)|(?:Buenos\\sAires)|(?:Busan)|(?:Cairo)|(?:Calcutta)|(?:Casablandca)|(?:Chicago)|(?:Chongqing)|(?:Dallas)|(?:Delhi)|(?:Detroit)|(?:Dhaka)|(?:Guangzhou)|(?:Hanoi)|(?:Hong\\sKong)|(?:Houston)|(?:Istanbul)|(?:Karachi)|(?:Jakarta)|(?:Kobe)|(?:Lagos)|(?:Lahore)|(?:Lima)|(?:London)|(?:Los\\sAngeles)|(?:Madrid)|(?:Melbourne)|(?:Metro\\sManila)|(?:Mexico\\sCity)|(?:Miami)|(?:Milan)|(?:Montreal)|(?:Moscow)|(?:Mumbai)|(?:New\\sYork)|(?:Osaka)|(?:Paris)|(?:Philadelphia)|(?:Phoenix)|(?:Pusan)|(?:Rio\\sde\\sJaneiro)|(?:Santiago)|(?:Sao\\sPaulo)|(?:Seoul)|(?:Shanghai)|(?:Shenyang)|(?:Singapore)|(?:Sydney)|(?:Tehran)|(?:Tianjin)|(?:Tokyo)|(?:Toronto)|(?:Washington(?:[\\,\\s]{1,2}[Dd][\\.]?[Cc][\\.]?))|(?:Wuhan)|(?:Xi[\\\']?an))))(?:[\\t \\,][\\t ]?(?:[Uu]nited[\\t ])?[a-zA-Z\u00a0-\uaf00\\\'\\.]{2,20}\\b)?"

    .line 2210
    .local v13, "REGEX_US":Ljava/lang/String;
    const/4 v14, 0x5

    .line 2212
    .local v14, "TYPE_ADDRESS":I
    const-string v10, "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}(?:\\@|(?:\\&\\#[0]*64\\;))[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(?:\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+"

    .line 2214
    .local v10, "REGEX_EMAIL":Ljava/lang/String;
    const/16 v16, 0x1

    .line 2224
    .local v16, "TYPE_EMAIL":I
    const-string v9, "(([0-9]{4}[/.-][0-9]{1,2}[/.-][0-9]{1,2})|([0-9]{1,2}[.-][0-9]{1,2}[.-]([0-9]{4}|[0-9]{2})))((\\s[0-9]{1,2}(\\s(AM|Am|am|PM|Pm|pm)))|(\\s[0-9]{1,2}(:[0-9]{1,2})(\\s(AM|Am|am|PM|Pm|pm))?))?"

    .line 2226
    .local v9, "REGEX_DATA_TIME":Ljava/lang/String;
    const/4 v15, 0x4

    .line 2228
    .local v15, "TYPE_DATA_TIME":I
    const-string v11, "(?<!((\\s[0-9]{1,2}:)|([0-9]{1,2}[/.-])))(?:\\+[0-9]{1,3}[\\- \\.]*)?(?:([0-9]{2,5}[\\- \\.]?[0-9]{3,5}[\\- \\.]?[0-9]{3,5})|[0-9]{5,7})"

    .line 2230
    .local v11, "REGEX_TEL":Ljava/lang/String;
    const/16 v17, 0x2

    .line 2245
    .local v17, "TYPE_TEL":I
    const-string v12, "(?i)((https?|ftp)://)?([a-z0-9+!*(),;?&=$_.-]+(:[a-z0-9+!*(),;?&=$_.-]+)?@)?([A-Za-z0-9-.]*)\\.([a-z]{2,3})(:[0-9]{2,5})?(/([A-Za-z0-9?#+$_-].?[A-Za-z0-9(),\'!;:@&%=+/$_.-?]*)+)*/?(#[A-Za-z_.-][A-Za-z0-9+$_.-]*)?(?=([\\u0080-\\uffff]|\\W)|$)"

    .line 2249
    .local v12, "REGEX_URL":Ljava/lang/String;
    const/16 v18, 0x3

    .line 2251
    .local v18, "TYPE_URL":I
    new-instance v28, Ljava/util/LinkedHashMap;

    invoke-direct/range {v28 .. v28}, Ljava/util/LinkedHashMap;-><init>()V

    .line 2253
    .local v28, "regexList":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    const/16 v34, 0x4

    invoke-static/range {v34 .. v34}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v34

    const-string v35, "(([0-9]{4}[/.-][0-9]{1,2}[/.-][0-9]{1,2})|([0-9]{1,2}[.-][0-9]{1,2}[.-]([0-9]{4}|[0-9]{2})))((\\s[0-9]{1,2}(\\s(AM|Am|am|PM|Pm|pm)))|(\\s[0-9]{1,2}(:[0-9]{1,2})(\\s(AM|Am|am|PM|Pm|pm))?))?"

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2254
    const/16 v34, 0x2

    invoke-static/range {v34 .. v34}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v34

    const-string v35, "(?<!((\\s[0-9]{1,2}:)|([0-9]{1,2}[/.-])))(?:\\+[0-9]{1,3}[\\- \\.]*)?(?:([0-9]{2,5}[\\- \\.]?[0-9]{3,5}[\\- \\.]?[0-9]{3,5})|[0-9]{5,7})"

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2255
    const/16 v34, 0x1

    invoke-static/range {v34 .. v34}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v34

    const-string v35, "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}(?:\\@|(?:\\&\\#[0]*64\\;))[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(?:\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+"

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2256
    const/16 v34, 0x3

    invoke-static/range {v34 .. v34}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v34

    const-string v35, "(?i)((https?|ftp)://)?([a-z0-9+!*(),;?&=$_.-]+(:[a-z0-9+!*(),;?&=$_.-]+)?@)?([A-Za-z0-9-.]*)\\.([a-z]{2,3})(:[0-9]{2,5})?(/([A-Za-z0-9?#+$_-].?[A-Za-z0-9(),\'!;:@&%=+/$_.-?]*)+)*/?(#[A-Za-z_.-][A-Za-z0-9+$_.-]*)?(?=([\\u0080-\\uffff]|\\W)|$)"

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2257
    const/16 v34, 0x5

    invoke-static/range {v34 .. v34}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v34

    const-string v35, "(?<!((\\s[0-9]{1,2}:)|([0-9]{1,2}/)))\\b(?:(?:[\\w]?[\\d]{1,4}(?:[\\-\\s]\\d[0-9a-zA-Z]{0,3})?)|(?:\\d[0-9a-zA-Z]{0,3}))\\b(?:\\s|\\,|(?:\\&nbsp;)){1,3}[a-zA-Z\u00a0-\uaf00](?:[\u00a0-\uaf00\\\'\\w\\s#@\\-\\,\\.]{4,18})(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}(?i:<[^>]+>(?:[\\s\\x0d\\x0a]|(?:\\&nbsp;)){0,2}){0,5}(?:(?:(?:[a-zA-Z\u00a0-\uaf00\\\'\\s]{2,20}(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}[a-zA-Z\u00a0-\uaf00\\\']{2,10})(?:\\s|\\,|(?:\\&nbsp;)){1,3}\\b(?:(?:[\\d]{4})|(?:[a-zA-Z][a-zA-Z0-9]{1,3}[\\-\\s][0-9][a-zA-Z][a-zA-Z])|(?:[a-zA-Z][0-9][a-zA-Z][\\-\\s][0-9][a-zA-Z][0-9])|(?:[\\d]{5}(?:[\\-\\s][\\d]{4})?))\\b)|(?:\\b(?:(?:[\\d]{4})|(?:[a-zA-Z][a-zA-Z0-9]{1,3}[\\-\\s][0-9][a-zA-Z][a-zA-Z])|(?:[a-zA-Z][0-9][a-zA-Z][\\-\\s][0-9][a-zA-Z][0-9])|(?:[\\d]{5}(?:[\\-\\s][\\d]{4})?))(?:\\s|\\,|(?:\\&nbsp;)){1,3}(?:[a-zA-Z\u00a0-\uaf00\\\'\\s]{2,20}(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}[a-zA-Z\u00a0-\uaf00\\\']{2,10})\\b)|(?:(?:(?:Ankara)|(?:Athens)|(?:Atlanta)|(?:Baghdad)|(?:Bandung)|(?:Bangalore)|(?:Bangkok)|(?:Barcelona)|(?:Beijing)|(?:Berlin)|(?:Bombay)|(?:Boston)|(?:Brasillia)|(?:Buenos\\sAires)|(?:Busan)|(?:Cairo)|(?:Calcutta)|(?:Casablandca)|(?:Chicago)|(?:Chongqing)|(?:Dallas)|(?:Delhi)|(?:Detroit)|(?:Dhaka)|(?:Guangzhou)|(?:Hanoi)|(?:Hong\\sKong)|(?:Houston)|(?:Istanbul)|(?:Karachi)|(?:Jakarta)|(?:Kobe)|(?:Lagos)|(?:Lahore)|(?:Lima)|(?:London)|(?:Los\\sAngeles)|(?:Madrid)|(?:Melbourne)|(?:Metro\\sManila)|(?:Mexico\\sCity)|(?:Miami)|(?:Milan)|(?:Montreal)|(?:Moscow)|(?:Mumbai)|(?:New\\sYork)|(?:Osaka)|(?:Paris)|(?:Philadelphia)|(?:Phoenix)|(?:Pusan)|(?:Rio\\sde\\sJaneiro)|(?:Santiago)|(?:Sao\\sPaulo)|(?:Seoul)|(?:Shanghai)|(?:Shenyang)|(?:Singapore)|(?:Sydney)|(?:Tehran)|(?:Tianjin)|(?:Tokyo)|(?:Toronto)|(?:Washington(?:[\\,\\s]{1,2}[Dd][\\.]?[Cc][\\.]?))|(?:Wuhan)|(?:Xi[\\\']?an))))(?:[\\t \\,][\\t ]?(?:[Uu]nited[\\t ])?[a-zA-Z\u00a0-\uaf00\\\'\\.]{2,20}\\b)?"

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2259
    invoke-virtual/range {v28 .. v28}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v34

    invoke-interface/range {v34 .. v34}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v35

    :cond_0
    invoke-interface/range {v35 .. v35}, Ljava/util/Iterator;->hasNext()Z

    move-result v34

    if-nez v34, :cond_1

    .line 2335
    return-object v30

    .line 2259
    :cond_1
    invoke-interface/range {v35 .. v35}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/util/Map$Entry;

    .line 2260
    .local v29, "regexSet":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    invoke-interface/range {v29 .. v29}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Integer;

    .line 2261
    .local v23, "key":Ljava/lang/Integer;
    invoke-interface/range {v29 .. v29}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    invoke-static/range {v34 .. v34}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v26

    .line 2262
    .local v26, "p":Ljava/util/regex/Pattern;
    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v24

    .line 2263
    .local v24, "m":Ljava/util/regex/Matcher;
    :cond_2
    :goto_0
    invoke-virtual/range {v24 .. v24}, Ljava/util/regex/Matcher;->find()Z

    move-result v34

    if-eqz v34, :cond_0

    .line 2264
    const/16 v34, 0x3

    move/from16 v0, v34

    new-array v0, v0, [I

    move-object/from16 v20, v0

    .line 2265
    .local v20, "hyperlink":[I
    const/16 v34, 0x0

    invoke-virtual/range {v24 .. v24}, Ljava/util/regex/Matcher;->start()I

    move-result v36

    aput v36, v20, v34

    .line 2266
    const/16 v34, 0x1

    invoke-virtual/range {v24 .. v24}, Ljava/util/regex/Matcher;->end()I

    move-result v36

    aput v36, v20, v34

    .line 2267
    const/16 v34, 0x2

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v36

    aput v36, v20, v34

    .line 2272
    const/16 v19, 0x0

    .line 2273
    .local v19, "contain":Z
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_1
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v34

    move/from16 v0, v21

    move/from16 v1, v34

    if-lt v0, v1, :cond_5

    .line 2324
    :cond_3
    :goto_2
    if-nez v19, :cond_2

    .line 2325
    const/16 v34, 0x1

    aget v34, v20, v34

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-le v0, v1, :cond_4

    .line 2326
    const/16 v34, 0x1

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v36

    aput v36, v20, v34

    .line 2328
    :cond_4
    const-string v34, "Model_ObjectText"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "Add hyperlink - type["

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v37, 0x2

    aget v37, v20, v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, "] startPos["

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const/16 v37, 0x0

    aget v37, v20, v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    .line 2329
    const-string v37, "] endPos["

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const/16 v37, 0x1

    aget v37, v20, v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, "]"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    .line 2328
    move-object/from16 v0, v34

    move-object/from16 v1, v36

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2330
    move-object/from16 v0, v30

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2274
    :cond_5
    move-object/from16 v0, v30

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, [I

    .line 2276
    .local v32, "v":[I
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v34

    const/16 v36, 0x3

    move/from16 v0, v34

    move/from16 v1, v36

    if-ne v0, v1, :cond_6

    const/16 v34, 0x2

    aget v34, v32, v34

    const/16 v36, 0x1

    move/from16 v0, v34

    move/from16 v1, v36

    if-ne v0, v1, :cond_6

    const/16 v34, 0x0

    aget v34, v20, v34

    const/16 v36, 0x0

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-ne v0, v1, :cond_6

    const/16 v34, 0x1

    aget v34, v20, v34

    const/16 v36, 0x1

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-ne v0, v1, :cond_6

    .line 2277
    const/16 v19, 0x1

    .line 2278
    goto/16 :goto_2

    .line 2279
    :cond_6
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v34

    const/16 v36, 0x5

    move/from16 v0, v34

    move/from16 v1, v36

    if-eq v0, v1, :cond_7

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v34

    const/16 v36, 0x3

    move/from16 v0, v34

    move/from16 v1, v36

    if-eq v0, v1, :cond_7

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v34

    const/16 v36, 0x1

    move/from16 v0, v34

    move/from16 v1, v36

    if-ne v0, v1, :cond_9

    :cond_7
    const/16 v34, 0x0

    aget v34, v20, v34

    const/16 v36, 0x0

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-gt v0, v1, :cond_9

    .line 2280
    const/16 v34, 0x1

    aget v34, v20, v34

    const/16 v36, 0x1

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-lt v0, v1, :cond_9

    .line 2282
    const-string v34, "Model_ObjectText"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "Remove contain hyperlink - type["

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v37, 0x2

    aget v37, v32, v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, "] startPos["

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const/16 v37, 0x0

    aget v37, v32, v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    .line 2283
    const-string v37, "] endPos["

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const/16 v37, 0x1

    aget v37, v32, v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, "]"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    .line 2282
    move-object/from16 v0, v34

    move-object/from16 v1, v36

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2284
    move-object/from16 v0, v30

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2285
    add-int/lit8 v21, v21, -0x1

    .line 2273
    :cond_8
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_1

    .line 2286
    :cond_9
    const/16 v34, 0x0

    aget v34, v20, v34

    const/16 v36, 0x0

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-lt v0, v1, :cond_a

    const/16 v34, 0x0

    aget v34, v20, v34

    const/16 v36, 0x1

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-le v0, v1, :cond_b

    .line 2287
    :cond_a
    const/16 v34, 0x1

    aget v34, v20, v34

    const/16 v36, 0x0

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-lt v0, v1, :cond_8

    const/16 v34, 0x1

    aget v34, v20, v34

    const/16 v36, 0x1

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-gt v0, v1, :cond_8

    .line 2289
    :cond_b
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v34

    const/16 v36, 0x5

    move/from16 v0, v34

    move/from16 v1, v36

    if-eq v0, v1, :cond_c

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v34

    const/16 v36, 0x2

    move/from16 v0, v34

    move/from16 v1, v36

    if-ne v0, v1, :cond_f

    :cond_c
    const/16 v34, 0x0

    aget v34, v20, v34

    const/16 v36, 0x1

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-ge v0, v1, :cond_f

    const/16 v34, 0x0

    aget v34, v20, v34

    const/16 v36, 0x0

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-le v0, v1, :cond_f

    .line 2290
    const/16 v34, 0x1

    aget v34, v32, v34

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v36

    add-int/lit8 v36, v36, -0x1

    move/from16 v0, v34

    move/from16 v1, v36

    if-ge v0, v1, :cond_f

    .line 2291
    const/16 v34, 0x1

    aget v34, v32, v34

    add-int/lit8 v34, v34, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v31

    .line 2292
    .local v31, "subStr":Ljava/lang/String;
    invoke-interface/range {v29 .. v29}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    invoke-static/range {v34 .. v34}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v27

    .line 2293
    .local v27, "p1":Ljava/util/regex/Pattern;
    move-object/from16 v0, v27

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v25

    .line 2294
    .local v25, "m1":Ljava/util/regex/Matcher;
    invoke-virtual/range {v25 .. v25}, Ljava/util/regex/Matcher;->find()Z

    move-result v34

    if-eqz v34, :cond_e

    .line 2295
    const/16 v34, 0x0

    invoke-virtual/range {v25 .. v25}, Ljava/util/regex/Matcher;->start()I

    move-result v36

    const/16 v37, 0x1

    aget v37, v32, v37

    add-int v36, v36, v37

    add-int/lit8 v36, v36, 0x1

    aput v36, v20, v34

    .line 2296
    const/16 v34, 0x1

    invoke-virtual/range {v25 .. v25}, Ljava/util/regex/Matcher;->end()I

    move-result v36

    const/16 v37, 0x1

    aget v37, v32, v37

    add-int v36, v36, v37

    add-int/lit8 v36, v36, 0x1

    aput v36, v20, v34

    .line 2298
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v34

    const/16 v36, 0x5

    move/from16 v0, v34

    move/from16 v1, v36

    if-ne v0, v1, :cond_3

    .line 2299
    const/16 v22, 0x0

    .local v22, "j":I
    :goto_3
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v34

    move/from16 v0, v22

    move/from16 v1, v34

    if-ge v0, v1, :cond_3

    .line 2300
    move-object/from16 v0, v30

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v33

    check-cast v33, [I

    .line 2301
    .local v33, "v1":[I
    const/16 v34, 0x0

    aget v34, v20, v34

    const/16 v36, 0x0

    aget v36, v33, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-gt v0, v1, :cond_d

    const/16 v34, 0x1

    aget v34, v20, v34

    const/16 v36, 0x1

    aget v36, v33, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-lt v0, v1, :cond_d

    .line 2302
    const-string v34, "Model_ObjectText"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "Remove overlap hyperlink - type["

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v37, 0x2

    aget v37, v33, v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    .line 2303
    const-string v37, "] startPos["

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const/16 v37, 0x0

    aget v37, v33, v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, "] endPos["

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const/16 v37, 0x1

    aget v37, v33, v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, "]"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    .line 2302
    move-object/from16 v0, v34

    move-object/from16 v1, v36

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2304
    move-object/from16 v0, v30

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2305
    add-int/lit8 v22, v22, -0x1

    .line 2299
    :cond_d
    add-int/lit8 v22, v22, 0x1

    goto :goto_3

    .line 2311
    .end local v22    # "j":I
    .end local v33    # "v1":[I
    :cond_e
    const/16 v19, 0x1

    .line 2312
    goto/16 :goto_2

    .line 2316
    .end local v25    # "m1":Ljava/util/regex/Matcher;
    .end local v27    # "p1":Ljava/util/regex/Pattern;
    .end local v31    # "subStr":Ljava/lang/String;
    :cond_f
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v34

    const/16 v36, 0x5

    move/from16 v0, v34

    move/from16 v1, v36

    if-ne v0, v1, :cond_10

    const/16 v34, 0x1

    aget v34, v20, v34

    const/16 v36, 0x0

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-le v0, v1, :cond_10

    const/16 v34, 0x1

    aget v34, v20, v34

    const/16 v36, 0x1

    aget v36, v32, v36

    move/from16 v0, v34

    move/from16 v1, v36

    if-ge v0, v1, :cond_10

    .line 2317
    const/16 v34, 0x1

    const/16 v36, 0x0

    aget v36, v32, v36

    add-int/lit8 v36, v36, -0x1

    aput v36, v20, v34

    goto/16 :goto_2

    .line 2320
    :cond_10
    const/16 v19, 0x1

    .line 2321
    goto/16 :goto_2
.end method

.method public static setInitialCursorPos(I)V
    .locals 4
    .param p0, "initialCursorPos"    # I

    .prologue
    .line 2392
    if-eqz p0, :cond_0

    const/4 v2, 0x1

    if-eq p0, v2, :cond_0

    .line 2393
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "E_INVALID_ARG"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2396
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setInitialCursorPos(I)Z

    move-result v1

    .line 2397
    .local v1, "rnt":Z
    if-nez v1, :cond_1

    .line 2398
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "E_INVALID_ARG"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2400
    .end local v1    # "rnt":Z
    :catch_0
    move-exception v0

    .line 2401
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2402
    const-string v2, "SpenObjectTextBox"

    const-string v3, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2404
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :cond_1
    return-void
.end method

.method private throwUncheckedException(I)V
    .locals 3
    .param p1, "errno"    # I

    .prologue
    .line 2407
    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    .line 2408
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenObjectTextBox("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2410
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2412
    return-void
.end method


# virtual methods
.method public appendParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)V
    .locals 2
    .param p1, "paragraph"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;

    .prologue
    .line 1352
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_appendParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)Z

    move-result v0

    .line 1353
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1354
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1356
    :cond_0
    return-void
.end method

.method public appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V
    .locals 2
    .param p1, "span"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .prologue
    .line 1278
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)Z

    move-result v0

    .line 1279
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1280
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1282
    :cond_0
    return-void
.end method

.method public clearChangedFlag()V
    .locals 0

    .prologue
    .line 2343
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_clearChangedFlag()V

    .line 2344
    return-void
.end method

.method public copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 2
    .param p1, "source"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 2368
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    .line 2369
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2370
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 2372
    :cond_0
    return-void
.end method

.method public findParagraphs(II)Ljava/util/ArrayList;
    .locals 1
    .param p1, "startPos"    # I
    .param p2, "endPos"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1324
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_findParagraphs(II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public findSpans(II)Ljava/util/ArrayList;
    .locals 1
    .param p1, "startPos"    # I
    .param p2, "endPos"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1249
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_findSpans(II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getAutoFitOption()I
    .locals 1

    .prologue
    .line 1602
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getAutoFitOption()I

    move-result v0

    return v0
.end method

.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 1702
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getBackgroundColor()I

    move-result v0

    return v0
.end method

.method public getBorderType()I
    .locals 1

    .prologue
    .line 1495
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getBorderType()I

    move-result v0

    return v0
.end method

.method public getBottomMargin()F
    .locals 1

    .prologue
    .line 1415
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getBottomMargin()F

    move-result v0

    return v0
.end method

.method public getBulletType()I
    .locals 1

    .prologue
    .line 2005
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getBulletType()I

    move-result v0

    return v0
.end method

.method public getCursorPos()I
    .locals 1

    .prologue
    .line 1194
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getCursorPos()I

    move-result v0

    return v0
.end method

.method public getDrawnRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 2359
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getDrawnRect()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getEllipsisType()I
    .locals 1

    .prologue
    .line 2183
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getEllipsisType()I

    move-result v0

    return v0
.end method

.method public getFont()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1769
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getFont()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFontSize()F
    .locals 1

    .prologue
    .line 1735
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getFontSize()F

    move-result v0

    return v0
.end method

.method public getGravity()I
    .locals 1

    .prologue
    .line 1641
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getGravity()I

    move-result v0

    return v0
.end method

.method public getHintText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1546
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getHintText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHintTextColor()I
    .locals 1

    .prologue
    .line 2029
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getHintTextColor()I

    move-result v0

    return v0
.end method

.method public getHintTextFontSize()F
    .locals 1

    .prologue
    .line 2056
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getHintTextFontSize()F

    move-result v0

    return v0
.end method

.method public getIMEActionType()I
    .locals 1

    .prologue
    .line 2085
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getIMEActionType()I

    move-result v0

    return v0
.end method

.method public getLeftMargin()F
    .locals 1

    .prologue
    .line 1385
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getLeftMargin()F

    move-result v0

    return v0
.end method

.method public getLineBorderColor()I
    .locals 1

    .prologue
    .line 1439
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getLineBorderColor()I

    move-result v0

    return v0
.end method

.method public getLineBorderWidth()F
    .locals 1

    .prologue
    .line 1466
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getLineBorderWidth()F

    move-result v0

    return v0
.end method

.method public getParagraph()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1310
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getParagraph()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getRightMargin()F
    .locals 1

    .prologue
    .line 1405
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getRightMargin()F

    move-result v0

    return v0
.end method

.method public getSpan()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1234
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getSpan()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1070
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTextAlignment()I
    .locals 1

    .prologue
    .line 1878
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextAlignment()I

    move-result v0

    return v0
.end method

.method public getTextColor()I
    .locals 1

    .prologue
    .line 1674
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextColor()I

    move-result v0

    return v0
.end method

.method public getTextDirection()I
    .locals 1

    .prologue
    .line 1805
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextDirection()I

    move-result v0

    return v0
.end method

.method public getTextIndentLevel()I
    .locals 1

    .prologue
    .line 1967
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextIndentLevel()I

    move-result v0

    return v0
.end method

.method public getTextInputType()I
    .locals 1

    .prologue
    .line 2113
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextInputType()I

    move-result v0

    return v0
.end method

.method public getTextLineSpacing()F
    .locals 1

    .prologue
    .line 1929
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextLineSpacing()F

    move-result v0

    return v0
.end method

.method public getTextLineSpacingType()I
    .locals 1

    .prologue
    .line 1915
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextLineSpacingType()I

    move-result v0

    return v0
.end method

.method public getTextStyle()I
    .locals 1

    .prologue
    .line 1845
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextStyle()I

    move-result v0

    return v0
.end method

.method public getTopMargin()F
    .locals 1

    .prologue
    .line 1395
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTopMargin()F

    move-result v0

    return v0
.end method

.method public getVerticalPan()F
    .locals 1

    .prologue
    .line 2155
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getVerticalPan()F

    move-result v0

    return v0
.end method

.method public insertText(Ljava/lang/String;I)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "position"    # I

    .prologue
    .line 1089
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_insertText(Ljava/lang/String;I)Z

    move-result v0

    .line 1090
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1091
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1093
    :cond_0
    return-void
.end method

.method public insertTextAtCursor(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 1107
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_insertTextAtCursor(Ljava/lang/String;)Z

    move-result v0

    .line 1108
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1109
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1111
    :cond_0
    return-void
.end method

.method public isChanged()Z
    .locals 1

    .prologue
    .line 2351
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_isChanged()Z

    move-result v0

    return v0
.end method

.method public isHintTextEnabled()Z
    .locals 1

    .prologue
    .line 1570
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_isHintTextVisiable()Z

    move-result v0

    return v0
.end method

.method public isReadOnlyEnabled()Z
    .locals 1

    .prologue
    .line 1522
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_isReadOnly()Z

    move-result v0

    return v0
.end method

.method public parseHyperText()V
    .locals 2

    .prologue
    .line 1611
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_parseHyperText()Z

    move-result v0

    .line 1612
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1613
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1616
    :cond_0
    return-void
.end method

.method public removeAllText()V
    .locals 2

    .prologue
    .line 1139
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_removeAllText()Z

    move-result v0

    .line 1140
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1141
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1143
    :cond_0
    return-void
.end method

.method public removeParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)V
    .locals 2
    .param p1, "paragraph"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;

    .prologue
    .line 1337
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_removeParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)Z

    move-result v0

    .line 1338
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1339
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1341
    :cond_0
    return-void
.end method

.method public removeSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V
    .locals 2
    .param p1, "span"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .prologue
    .line 1263
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_removeSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)Z

    move-result v0

    .line 1264
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1265
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1267
    :cond_0
    return-void
.end method

.method public removeText(II)V
    .locals 2
    .param p1, "startPosition"    # I
    .param p2, "length"    # I

    .prologue
    .line 1127
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_removeText(II)Z

    move-result v0

    .line 1128
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1129
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1131
    :cond_0
    return-void
.end method

.method public replaceText(Ljava/lang/String;II)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "startPosition"    # I
    .param p3, "length"    # I

    .prologue
    .line 1160
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_replaceText(Ljava/lang/String;II)Z

    move-result v0

    .line 1161
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1162
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1164
    :cond_0
    return-void
.end method

.method public setAutoFitOption(I)V
    .locals 2
    .param p1, "textAutoFit"    # I

    .prologue
    .line 1587
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setAutoFitOption(I)Z

    move-result v0

    .line 1588
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1589
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1591
    :cond_0
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 1687
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setBackgroundColor(I)Z

    move-result v0

    .line 1688
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1689
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1691
    :cond_0
    return-void
.end method

.method public setBorderType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 1481
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setBorderType(I)Z

    move-result v0

    .line 1482
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1483
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1486
    :cond_0
    return-void
.end method

.method public setBulletType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 1986
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setBulletType(I)Z

    move-result v0

    .line 1987
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1988
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1990
    :cond_0
    return-void
.end method

.method public setCursorPos(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1180
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setCursorPos(I)Z

    move-result v0

    .line 1181
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1182
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1184
    :cond_0
    return-void
.end method

.method public setEllipsisType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 2170
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setEllipsisType(I)Z

    move-result v0

    .line 2171
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2172
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 2174
    :cond_0
    return-void
.end method

.method public setFont(Ljava/lang/String;)V
    .locals 2
    .param p1, "font"    # Ljava/lang/String;

    .prologue
    .line 1751
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setFont(Ljava/lang/String;)Z

    move-result v0

    .line 1752
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1753
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1755
    :cond_0
    return-void
.end method

.method public setFontSize(F)V
    .locals 2
    .param p1, "size"    # F

    .prologue
    .line 1718
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setFontSize(F)Z

    move-result v0

    .line 1719
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1720
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1722
    :cond_0
    return-void
.end method

.method public setGravity(I)V
    .locals 2
    .param p1, "textGravity"    # I

    .prologue
    .line 1628
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setGravity(I)Z

    move-result v0

    .line 1629
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1630
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1632
    :cond_0
    return-void
.end method

.method public setHintText(Ljava/lang/String;)V
    .locals 2
    .param p1, "hintText"    # Ljava/lang/String;

    .prologue
    .line 1533
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setHintText(Ljava/lang/String;)Z

    move-result v0

    .line 1534
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1535
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1537
    :cond_0
    return-void
.end method

.method public setHintTextColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 2016
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setHintTextColor(I)Z

    move-result v0

    .line 2017
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2018
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 2020
    :cond_0
    return-void
.end method

.method public setHintTextEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 1557
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setHintTextVisibility(Z)Z

    move-result v0

    .line 1558
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1559
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1561
    :cond_0
    return-void
.end method

.method public setHintTextFontSize(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 2043
    int-to-float v1, p1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setHintTextFontSize(F)Z

    move-result v0

    .line 2044
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2045
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 2047
    :cond_0
    return-void
.end method

.method public setIMEActionType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 2072
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setIMEActionType(I)Z

    move-result v0

    .line 2073
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2074
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 2076
    :cond_0
    return-void
.end method

.method public setLineBorderColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 1426
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setLineBorderColor(I)Z

    move-result v0

    .line 1427
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1428
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1430
    :cond_0
    return-void
.end method

.method public setLineBorderWidth(F)V
    .locals 2
    .param p1, "width"    # F

    .prologue
    .line 1453
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setLineBorderWidth(F)Z

    move-result v0

    .line 1454
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1455
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1457
    :cond_0
    return-void
.end method

.method public setMargin(FFFF)V
    .locals 2
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "right"    # F
    .param p4, "bottom"    # F

    .prologue
    .line 1372
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setMargin(FFFF)Z

    move-result v0

    .line 1373
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1374
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1376
    :cond_0
    return-void
.end method

.method public setParagraph(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1297
    .local p1, "paragraph":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setParagraph(Ljava/util/ArrayList;)Z

    move-result v0

    .line 1298
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1299
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1301
    :cond_0
    return-void
.end method

.method public setReadOnlyEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 1506
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_enableReadOnly(Z)Z

    move-result v0

    .line 1507
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1508
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1510
    :cond_0
    return-void
.end method

.method public setRect(Landroid/graphics/RectF;Z)V
    .locals 0
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "regionOnly"    # Z

    .prologue
    .line 1046
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    .line 1047
    return-void
.end method

.method public setSpan(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1211
    .local p1, "span":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    if-eqz p1, :cond_1

    .line 1212
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1221
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setSpan(Ljava/util/ArrayList;)Z

    move-result v0

    .line 1222
    .local v0, "rnt":Z
    if-nez v0, :cond_2

    .line 1223
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1225
    .end local v0    # "rnt":Z
    :cond_2
    :goto_0
    return-void

    .line 1212
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 1213
    .local v1, "temp":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ltz v3, :cond_4

    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-gez v3, :cond_0

    .line 1214
    :cond_4
    const/4 v2, 0x7

    .line 1215
    const-string/jumbo v3, "startPos and endPos of TextSpanInfo should have positive value"

    .line 1214
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 1057
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setText(Ljava/lang/String;)Z

    move-result v0

    .line 1058
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1059
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1061
    :cond_0
    return-void
.end method

.method public setTextAlignment(I)V
    .locals 2
    .param p1, "align"    # I

    .prologue
    .line 1861
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextAlignment(I)Z

    move-result v0

    .line 1862
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1863
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1865
    :cond_0
    return-void
.end method

.method public setTextColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 1656
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextColor(I)Z

    move-result v0

    .line 1657
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1658
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1660
    :cond_0
    return-void
.end method

.method public setTextDirection(I)V
    .locals 2
    .param p1, "direction"    # I

    .prologue
    .line 1786
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextDirection(I)Z

    move-result v0

    .line 1787
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1788
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1790
    :cond_0
    return-void
.end method

.method public setTextIndentLevel(I)V
    .locals 2
    .param p1, "indentLevel"    # I

    .prologue
    .line 1948
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextIndentLevel(I)Z

    move-result v0

    .line 1949
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1950
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1952
    :cond_0
    return-void
.end method

.method public setTextInputType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 2100
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextInputType(I)Z

    move-result v0

    .line 2101
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2102
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 2104
    :cond_0
    return-void
.end method

.method public setTextLineSpacingInfo(IF)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "lineSpacing"    # F

    .prologue
    .line 1895
    const/4 v1, 0x0

    cmpg-float v1, p2, v1

    if-gez v1, :cond_0

    .line 1896
    const/4 v1, 0x7

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1898
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextLineSpacingInfo(IF)Z

    move-result v0

    .line 1899
    .local v0, "rnt":Z
    if-nez v0, :cond_1

    .line 1900
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1902
    :cond_1
    return-void
.end method

.method public setTextStyle(I)V
    .locals 3
    .param p1, "style"    # I

    .prologue
    .line 1825
    and-int/lit8 v1, p1, 0x7

    if-nez v1, :cond_0

    if-eqz p1, :cond_0

    .line 1826
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "style is invalid"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1828
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextStyle(I)Z

    move-result v0

    .line 1829
    .local v0, "rnt":Z
    if-nez v0, :cond_1

    .line 1830
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 1832
    :cond_1
    return-void
.end method

.method public setVereticalPan(F)V
    .locals 2
    .param p1, "position"    # F
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2126
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setVerticalPan(F)Z

    move-result v0

    .line 2127
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2128
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 2130
    :cond_0
    return-void
.end method

.method public setVerticalPan(F)V
    .locals 2
    .param p1, "position"    # F

    .prologue
    .line 2142
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setVerticalPan(F)Z

    move-result v0

    .line 2143
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2144
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    .line 2146
    :cond_0
    return-void
.end method
