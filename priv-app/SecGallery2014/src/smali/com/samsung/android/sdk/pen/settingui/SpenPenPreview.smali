.class Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;
.super Landroid/view/View;
.source "SpenPenPreview.java"


# static fields
.field private static CANVAS_WIDTH_TABS2:I

.field private static CANVAS_WIDTH_VEINNA:I

.field private static NUM_POINTS:I


# instance fields
.field private mAdvancedSetting:Ljava/lang/String;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBitmapPaint:Landroid/graphics/Paint;

.field private mCanvasWidth:I

.field private mColor:I

.field private mContext:Landroid/content/Context;

.field private mDensity:I

.field private mMax:F

.field private mMin:F

.field private mPenPluginInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

.field private mPenType:Ljava/lang/String;

.field private mPoints:[F

.field private mPressures:[F

.field private mRect:Landroid/graphics/RectF;

.field private mStrokeWidth:F

.field private mbottom:I

.field private mleft:I

.field private mright:I

.field private mtop:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/16 v0, 0xa

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    .line 20
    const/16 v0, 0x640

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->CANVAS_WIDTH_VEINNA:I

    .line 21
    const/16 v0, 0x600

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->CANVAS_WIDTH_TABS2:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 49
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 26
    const/high16 v1, 0x41a00000    # 20.0f

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    .line 27
    const/high16 v1, -0x1000000

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mColor:I

    .line 36
    const/16 v1, 0x438

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mCanvasWidth:I

    .line 39
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mleft:I

    .line 40
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mright:I

    .line 41
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mtop:I

    .line 42
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mbottom:I

    .line 44
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mMin:F

    .line 45
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mMax:F

    .line 51
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mContext:Landroid/content/Context;

    .line 53
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v1, v1, 0x2

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    .line 54
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    .line 56
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mRect:Landroid/graphics/RectF;

    .line 57
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmapPaint:Landroid/graphics/Paint;

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 59
    .local v0, "localDisplayMetrics":Landroid/util/DisplayMetrics;
    iget v1, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    div-int/lit16 v1, v1, 0xa0

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mDensity:I

    .line 60
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v1, v2, :cond_0

    .line 61
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mCanvasWidth:I

    .line 65
    :goto_0
    return-void

    .line 63
    :cond_0
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mCanvasWidth:I

    goto :goto_0
.end method

.method private checkResolution()V
    .locals 13

    .prologue
    .line 129
    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mright:I

    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mleft:I

    sub-int v7, v9, v10

    .line 130
    .local v7, "width":I
    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mbottom:I

    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mtop:I

    sub-int v3, v9, v10

    .line 131
    .local v3, "height":I
    int-to-float v9, v7

    const/high16 v10, 0x41800000    # 16.0f

    div-float v8, v9, v10

    .line 132
    .local v8, "widthUnit":F
    int-to-float v9, v3

    const/high16 v10, 0x41800000    # 16.0f

    div-float v2, v9, v10

    .line 134
    .local v2, "heighUnit":F
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    const/4 v10, 0x0

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mleft:I

    int-to-float v11, v11

    const/high16 v12, 0x3f800000    # 1.0f

    mul-float/2addr v12, v8

    add-float/2addr v11, v12

    aput v11, v9, v10

    .line 135
    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mCanvasWidth:I

    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->CANVAS_WIDTH_VEINNA:I

    if-ne v9, v10, :cond_2

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    const-string v10, "Marker"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    const-string v10, ".Brush"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 136
    :cond_0
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v6

    .line 137
    .local v6, "penPluginIndex":I
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v9

    invoke-interface {v9}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getMinSettingValue()F

    move-result v5

    .line 139
    .local v5, "min":F
    const/high16 v9, 0x41200000    # 10.0f

    add-float/2addr v9, v5

    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mCanvasWidth:I

    int-to-float v10, v10

    mul-float/2addr v9, v10

    const/high16 v10, 0x43b40000    # 360.0f

    div-float/2addr v9, v10

    iput v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mMax:F

    .line 140
    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mCanvasWidth:I

    int-to-float v9, v9

    mul-float/2addr v9, v5

    const/high16 v10, 0x43b40000    # 360.0f

    div-float/2addr v9, v10

    iput v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mMin:F

    .line 141
    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mMin:F

    sub-float/2addr v9, v10

    const/high16 v10, 0x41200000    # 10.0f

    mul-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mMax:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mMin:F

    sub-float/2addr v10, v11

    div-float v0, v9, v10

    .line 142
    .local v0, "delta":F
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    const/4 v10, 0x1

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mtop:I

    int-to-float v11, v11

    sub-float/2addr v11, v0

    const/high16 v12, 0x41600000    # 14.0f

    mul-float/2addr v12, v2

    add-float/2addr v11, v12

    aput v11, v9, v10

    .line 143
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v10, v10, 0x2

    add-int/lit8 v10, v10, -0x1

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mtop:I

    int-to-float v11, v11

    sub-float/2addr v11, v0

    const/high16 v12, 0x41600000    # 14.0f

    mul-float/2addr v12, v2

    add-float/2addr v11, v12

    aput v11, v9, v10

    .line 158
    .end local v0    # "delta":F
    .end local v5    # "min":F
    .end local v6    # "penPluginIndex":I
    :goto_0
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v10, v10, 0x2

    add-int/lit8 v10, v10, -0x2

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mleft:I

    int-to-float v11, v11

    const/high16 v12, 0x41800000    # 16.0f

    mul-float/2addr v12, v8

    add-float/2addr v11, v12

    aput v11, v9, v10

    .line 160
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v10, v10, 0x2

    add-int/lit8 v10, v10, -0x2

    aget v9, v9, v10

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    const/4 v11, 0x0

    aget v10, v10, v11

    sub-float/2addr v9, v10

    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    add-int/lit8 v10, v10, -0x1

    int-to-float v10, v10

    div-float v1, v9, v10

    .line 162
    .local v1, "dp":F
    const/4 v4, 0x2

    .local v4, "i":I
    :goto_1
    sget v9, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v9, v9, 0x2

    add-int/lit8 v9, v9, -0x2

    if-lt v4, v9, :cond_4

    .line 167
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    const/4 v10, 0x0

    const v11, 0x3f333333    # 0.7f

    aput v11, v9, v10

    .line 169
    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mDensity:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_6

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    const-string v10, ".FountainPen"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 170
    const/4 v4, 0x1

    :goto_2
    sget v9, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    if-lt v4, v9, :cond_5

    .line 178
    :cond_1
    return-void

    .line 144
    .end local v1    # "dp":F
    .end local v4    # "i":I
    :cond_2
    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mCanvasWidth:I

    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->CANVAS_WIDTH_TABS2:I

    if-ne v9, v10, :cond_3

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    const-string v10, "ChineseBrush"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 145
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v6

    .line 146
    .restart local v6    # "penPluginIndex":I
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v9

    invoke-interface {v9}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getMinSettingValue()F

    move-result v5

    .line 148
    .restart local v5    # "min":F
    const/high16 v9, 0x41900000    # 18.0f

    add-float/2addr v9, v5

    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mCanvasWidth:I

    int-to-float v10, v10

    mul-float/2addr v9, v10

    const/high16 v10, 0x43b40000    # 360.0f

    div-float/2addr v9, v10

    iput v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mMax:F

    .line 149
    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mCanvasWidth:I

    int-to-float v9, v9

    mul-float/2addr v9, v5

    const/high16 v10, 0x43b40000    # 360.0f

    div-float/2addr v9, v10

    iput v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mMin:F

    .line 150
    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mMin:F

    sub-float/2addr v9, v10

    const/high16 v10, 0x41200000    # 10.0f

    mul-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mMax:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mMin:F

    sub-float/2addr v10, v11

    div-float v0, v9, v10

    .line 151
    .restart local v0    # "delta":F
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    const/4 v10, 0x1

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mtop:I

    int-to-float v11, v11

    sub-float/2addr v11, v0

    const/high16 v12, 0x41600000    # 14.0f

    mul-float/2addr v12, v2

    add-float/2addr v11, v12

    aput v11, v9, v10

    .line 152
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v10, v10, 0x2

    add-int/lit8 v10, v10, -0x1

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mtop:I

    int-to-float v11, v11

    sub-float/2addr v11, v0

    const/high16 v12, 0x41600000    # 14.0f

    mul-float/2addr v12, v2

    add-float/2addr v11, v12

    aput v11, v9, v10

    goto/16 :goto_0

    .line 154
    .end local v0    # "delta":F
    .end local v5    # "min":F
    .end local v6    # "penPluginIndex":I
    :cond_3
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    const/4 v10, 0x1

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mtop:I

    int-to-float v11, v11

    const/high16 v12, 0x41600000    # 14.0f

    mul-float/2addr v12, v2

    add-float/2addr v11, v12

    aput v11, v9, v10

    .line 155
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v10, v10, 0x2

    add-int/lit8 v10, v10, -0x1

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mtop:I

    int-to-float v11, v11

    const/high16 v12, 0x41600000    # 14.0f

    mul-float/2addr v12, v2

    add-float/2addr v11, v12

    aput v11, v9, v10

    goto/16 :goto_0

    .line 163
    .restart local v1    # "dp":F
    .restart local v4    # "i":I
    :cond_4
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    add-int/lit8 v11, v4, -0x2

    aget v10, v10, v11

    add-float/2addr v10, v1

    aput v10, v9, v4

    .line 164
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    add-int/lit8 v10, v4, 0x1

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    const/4 v12, 0x1

    aget v11, v11, v12

    aput v11, v9, v10

    .line 162
    add-int/lit8 v4, v4, 0x2

    goto/16 :goto_1

    .line 171
    :cond_5
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    add-int/lit8 v11, v4, -0x1

    aget v10, v10, v11

    aput v10, v9, v4

    .line 170
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    .line 174
    :cond_6
    const/4 v4, 0x1

    :goto_3
    sget v9, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    if-ge v4, v9, :cond_1

    .line 175
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    add-int/lit8 v11, v4, -0x1

    aget v10, v10, v11

    const v11, 0x3ccccccd    # 0.025f

    sub-float/2addr v10, v11

    aput v10, v9, v4

    .line 174
    add-int/lit8 v4, v4, 0x1

    goto :goto_3
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mContext:Landroid/content/Context;

    .line 69
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    .line 70
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    .line 72
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    .line 73
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 75
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 79
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    .line 81
    :cond_1
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mRect:Landroid/graphics/RectF;

    .line 82
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmapPaint:Landroid/graphics/Paint;

    .line 84
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 21
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 190
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v19

    .line 191
    .local v19, "penPluginIndex":I
    const/4 v6, -0x1

    move/from16 v0, v19

    if-ne v0, v6, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    if-nez v6, :cond_2

    .line 195
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->loadPenPlugin(Landroid/content/Context;Ljava/lang/String;)V

    .line 196
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 200
    :cond_2
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    invoke-interface {v6, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 202
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    .line 203
    const/4 v7, 0x4

    invoke-interface {v6, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 204
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mAdvancedSetting:Ljava/lang/String;

    invoke-interface {v6, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setAdvancedSetting(Ljava/lang/String;)V

    .line 207
    :cond_3
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    invoke-interface {v6, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setSize(F)V

    .line 208
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mColor:I

    invoke-interface {v6, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setColor(I)V

    .line 210
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 211
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->checkResolution()V

    .line 212
    const-wide/16 v2, 0x0

    .line 214
    .local v2, "time":J
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    const/4 v8, 0x0

    aget v7, v7, v8

    mul-float v20, v6, v7

    .line 215
    .local v20, "startShift":F
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    sget v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    add-int/lit8 v8, v8, -0x1

    aget v7, v7, v8

    mul-float v16, v6, v7

    .line 216
    .local v16, "endShift":F
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    const-string v7, "Marker"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 217
    const/16 v20, 0x0

    .line 218
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/high16 v7, 0x40000000    # 2.0f

    div-float v16, v6, v7

    .line 219
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v7, v7, 0x2

    add-int/lit8 v7, v7, -0x2

    aget v6, v6, v7

    sub-float v6, v6, v16

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v8, v8, 0x2

    add-int/lit8 v8, v8, -0x4

    aget v7, v7, v8

    cmpg-float v6, v6, v7

    if-gez v6, :cond_4

    .line 220
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v7, v7, 0x2

    add-int/lit8 v7, v7, -0x4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v9, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v9, v9, 0x2

    add-int/lit8 v9, v9, -0x2

    aget v8, v8, v9

    sub-float v8, v8, v16

    const/high16 v9, 0x3f800000    # 1.0f

    sub-float/2addr v8, v9

    aput v8, v6, v7

    .line 223
    :cond_4
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    const-string v7, ".Brush"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 224
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    const/4 v8, 0x0

    aget v7, v7, v8

    mul-float v16, v6, v7

    .line 227
    :cond_5
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_1
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    move/from16 v0, v18

    if-lt v0, v6, :cond_6

    .line 248
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 250
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmapPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 228
    :cond_6
    if-nez v18, :cond_7

    .line 229
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 230
    const/4 v6, 0x0

    .line 231
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    mul-int/lit8 v8, v18, 0x2

    aget v7, v7, v8

    add-float v7, v7, v20

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    mul-int/lit8 v9, v18, 0x2

    add-int/lit8 v9, v9, 0x1

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    aget v9, v9, v18

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-wide v4, v2

    .line 230
    invoke-static/range {v2 .. v15}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v17

    .line 232
    .local v17, "event":Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mRect:Landroid/graphics/RectF;

    move-object/from16 v0, v17

    invoke-interface {v6, v0, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 233
    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->recycle()V

    .line 227
    :goto_2
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 234
    .end local v17    # "event":Landroid/view/MotionEvent;
    :cond_7
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    add-int/lit8 v6, v6, -0x1

    move/from16 v0, v18

    if-ne v0, v6, :cond_8

    .line 235
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    sub-int v6, v6, v18

    mul-int/lit8 v6, v6, 0x5

    int-to-long v6, v6

    add-long v4, v2, v6

    .line 236
    .local v4, "currentTime":J
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    mul-int/lit8 v8, v18, 0x2

    aget v7, v7, v8

    .line 237
    sub-float v7, v7, v16

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    mul-int/lit8 v9, v18, 0x2

    add-int/lit8 v9, v9, 0x1

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    aget v9, v9, v18

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    .line 236
    invoke-static/range {v2 .. v15}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v17

    .line 238
    .restart local v17    # "event":Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mRect:Landroid/graphics/RectF;

    move-object/from16 v0, v17

    invoke-interface {v6, v0, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 239
    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->recycle()V

    goto :goto_2

    .line 241
    .end local v4    # "currentTime":J
    .end local v17    # "event":Landroid/view/MotionEvent;
    :cond_8
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    sub-int v6, v6, v18

    mul-int/lit8 v6, v6, 0x5

    int-to-long v6, v6

    add-long v4, v2, v6

    .line 242
    .restart local v4    # "currentTime":J
    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    mul-int/lit8 v8, v18, 0x2

    aget v7, v7, v8

    .line 243
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    mul-int/lit8 v9, v18, 0x2

    add-int/lit8 v9, v9, 0x1

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    aget v9, v9, v18

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    .line 242
    invoke-static/range {v2 .. v15}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v17

    .line 244
    .restart local v17    # "event":Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mRect:Landroid/graphics/RectF;

    move-object/from16 v0, v17

    invoke-interface {v6, v0, v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 245
    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->recycle()V

    goto/16 :goto_2
.end method

.method protected onLayout(ZIIII)V
    .locals 10
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 89
    if-eqz p1, :cond_0

    .line 90
    sub-int v4, p4, p2

    .line 91
    .local v4, "width":I
    sub-int v2, p5, p3

    .line 92
    .local v2, "height":I
    int-to-float v6, v4

    const/high16 v7, 0x41800000    # 16.0f

    div-float v5, v6, v7

    .line 93
    .local v5, "widthUnit":F
    int-to-float v6, v2

    const/high16 v7, 0x41800000    # 16.0f

    div-float v1, v6, v7

    .line 95
    .local v1, "heighUnit":F
    iput p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mleft:I

    .line 96
    iput p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mtop:I

    .line 97
    iput p4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mright:I

    .line 98
    iput p5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mbottom:I

    .line 100
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    const/4 v7, 0x0

    int-to-float v8, p2

    const/high16 v9, 0x3f800000    # 1.0f

    mul-float/2addr v9, v5

    add-float/2addr v8, v9

    aput v8, v6, v7

    .line 101
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    const/4 v7, 0x1

    int-to-float v8, p3

    const/high16 v9, 0x41600000    # 14.0f

    mul-float/2addr v9, v1

    add-float/2addr v8, v9

    aput v8, v6, v7

    .line 103
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v7, v7, 0x2

    add-int/lit8 v7, v7, -0x2

    int-to-float v8, p2

    const/high16 v9, 0x41800000    # 16.0f

    mul-float/2addr v9, v5

    add-float/2addr v8, v9

    aput v8, v6, v7

    .line 104
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v7, v7, 0x2

    add-int/lit8 v7, v7, -0x1

    int-to-float v8, p3

    const/high16 v9, 0x41600000    # 14.0f

    mul-float/2addr v9, v1

    add-float/2addr v8, v9

    aput v8, v6, v7

    .line 106
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v7, v7, 0x2

    add-int/lit8 v7, v7, -0x2

    aget v6, v6, v7

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    const/4 v8, 0x0

    aget v7, v7, v8

    sub-float/2addr v6, v7

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    add-int/lit8 v7, v7, -0x1

    int-to-float v7, v7

    div-float v0, v6, v7

    .line 108
    .local v0, "dp":F
    const/4 v3, 0x2

    .local v3, "i":I
    :goto_0
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v6, v6, 0x2

    add-int/lit8 v6, v6, -0x2

    if-lt v3, v6, :cond_1

    .line 113
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    const/4 v7, 0x0

    const v8, 0x3f333333    # 0.7f

    aput v8, v6, v7

    .line 115
    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mDensity:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_3

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    const-string v7, ".FountainPen"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 116
    const/4 v3, 0x1

    :goto_1
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    if-lt v3, v6, :cond_2

    .line 125
    .end local v0    # "dp":F
    .end local v1    # "heighUnit":F
    .end local v2    # "height":I
    .end local v3    # "i":I
    .end local v4    # "width":I
    .end local v5    # "widthUnit":F
    :cond_0
    return-void

    .line 109
    .restart local v0    # "dp":F
    .restart local v1    # "heighUnit":F
    .restart local v2    # "height":I
    .restart local v3    # "i":I
    .restart local v4    # "width":I
    .restart local v5    # "widthUnit":F
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    add-int/lit8 v8, v3, -0x2

    aget v7, v7, v8

    add-float/2addr v7, v0

    aput v7, v6, v3

    .line 110
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    add-int/lit8 v7, v3, 0x1

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    const/4 v9, 0x1

    aget v8, v8, v9

    aput v8, v6, v7

    .line 108
    add-int/lit8 v3, v3, 0x2

    goto :goto_0

    .line 117
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    add-int/lit8 v8, v3, -0x1

    aget v7, v7, v8

    aput v7, v6, v3

    .line 116
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 120
    :cond_3
    const/4 v3, 0x1

    :goto_2
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    if-ge v3, v6, :cond_0

    .line 121
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    add-int/lit8 v8, v3, -0x1

    aget v7, v7, v8

    const v8, 0x3ccccccd    # 0.025f

    sub-float/2addr v7, v8

    aput v7, v6, v3

    .line 120
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 182
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 185
    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    .line 186
    return-void
.end method

.method public setPenPlugin(Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;)V
    .locals 1
    .param p1, "penPluginManager"    # Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    .prologue
    .line 314
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    .line 315
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginInfoList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    .line 316
    return-void
.end method

.method public setPenType(Ljava/lang/String;)V
    .locals 0
    .param p1, "penType"    # Ljava/lang/String;

    .prologue
    .line 293
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    .line 294
    return-void
.end method

.method public setStrokeAdvancedSetting(Ljava/lang/String;)V
    .locals 0
    .param p1, "adnvance"    # Ljava/lang/String;

    .prologue
    .line 303
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mAdvancedSetting:Ljava/lang/String;

    .line 304
    return-void
.end method

.method public setStrokeAlpha(I)V
    .locals 3
    .param p1, "alpha"    # I

    .prologue
    .line 272
    shl-int/lit8 v0, p1, 0x18

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mColor:I

    const v2, 0xffffff

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mColor:I

    .line 273
    return-void
.end method

.method public setStrokeColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 282
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mColor:I

    .line 283
    return-void
.end method

.method public setStrokeSize(F)V
    .locals 0
    .param p1, "width"    # F

    .prologue
    .line 261
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    .line 262
    return-void
.end method
