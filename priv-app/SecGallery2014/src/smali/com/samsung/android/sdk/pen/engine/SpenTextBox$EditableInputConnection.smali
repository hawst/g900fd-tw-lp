.class public Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;
.super Landroid/view/inputmethod/BaseInputConnection;
.source "SpenTextBox.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EditableInputConnection"
.end annotation


# instance fields
.field private final mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/view/View;)V
    .locals 1
    .param p2, "textview"    # Landroid/view/View;

    .prologue
    .line 5560
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 5561
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Landroid/view/inputmethod/BaseInputConnection;-><init>(Landroid/view/View;Z)V

    .line 5563
    check-cast p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .end local p2    # "textview":Landroid/view/View;
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 5564
    return-void
.end method

.method private checkSelection(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    const/16 v2, 0x8

    .line 5638
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$22(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 5639
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$22(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 5640
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$23(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 5641
    return-void
.end method


# virtual methods
.method public clearMetaKeyStates(I)Z
    .locals 4
    .param p1, "states"    # I

    .prologue
    .line 5435
    const-string v2, "EditableInputConnection"

    const-string v3, "clearMetaKeyStates()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5436
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->getEditable()Landroid/text/Editable;

    move-result-object v0

    .line 5437
    .local v0, "content":Landroid/text/Editable;
    if-nez v0, :cond_0

    .line 5438
    const/4 v2, 0x0

    .line 5447
    :goto_0
    return v2

    .line 5440
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/method/KeyListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 5442
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/method/KeyListener;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-interface {v2, v3, v0, p1}, Landroid/text/method/KeyListener;->clearMetaKeyState(Landroid/view/View;Landroid/text/Editable;I)V
    :try_end_0
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 5447
    :cond_1
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    .line 5443
    :catch_0
    move-exception v1

    .line 5444
    .local v1, "e":Ljava/lang/AbstractMethodError;
    invoke-virtual {v1}, Ljava/lang/AbstractMethodError;->printStackTrace()V

    goto :goto_1
.end method

.method public commitText(Ljava/lang/CharSequence;I)Z
    .locals 3
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "newCursorPosition"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 5524
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->checkSelection(Ljava/lang/CharSequence;)V

    .line 5526
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 5528
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$20(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 5530
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    .line 5532
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$20(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 5534
    return v2
.end method

.method public deleteSurroundingText(II)Z
    .locals 12
    .param p1, "beforeLength"    # I
    .param p2, "afterLength"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v11, -0x1

    .line 5455
    add-int v10, p1, p2

    if-lez v10, :cond_0

    .line 5456
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-static {v10, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 5459
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->getEditable()Landroid/text/Editable;

    move-result-object v4

    .line 5460
    .local v4, "content":Landroid/text/Editable;
    if-nez v4, :cond_1

    .line 5461
    const/4 v9, 0x0

    .line 5517
    :goto_0
    return v9

    .line 5464
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->beginBatchEdit()Z

    .line 5466
    invoke-static {v4}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    .line 5467
    .local v0, "a":I
    invoke-static {v4}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    .line 5469
    .local v1, "b":I
    if-le v0, v1, :cond_2

    .line 5470
    move v8, v0

    .line 5471
    .local v8, "tmp":I
    move v0, v1

    .line 5472
    move v1, v8

    .line 5476
    .end local v8    # "tmp":I
    :cond_2
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    move-result v2

    .line 5477
    .local v2, "ca":I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v3

    .line 5478
    .local v3, "cb":I
    if-ge v3, v2, :cond_3

    .line 5479
    move v8, v2

    .line 5480
    .restart local v8    # "tmp":I
    move v2, v3

    .line 5481
    move v3, v8

    .line 5484
    .end local v8    # "tmp":I
    :cond_3
    if-eq v2, v11, :cond_5

    if-eq v3, v11, :cond_5

    .line 5485
    if-ge v2, v0, :cond_4

    .line 5486
    move v0, v2

    .line 5488
    :cond_4
    if-le v3, v1, :cond_5

    .line 5489
    move v1, v3

    .line 5493
    :cond_5
    const/4 v5, 0x0

    .line 5495
    .local v5, "deleted":I
    if-lez p1, :cond_7

    .line 5496
    sub-int v7, v0, p1

    .line 5497
    .local v7, "start":I
    if-gez v7, :cond_6

    .line 5498
    const/4 v7, 0x0

    .line 5500
    :cond_6
    invoke-interface {v4, v7, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 5501
    sub-int v5, v0, v7

    .line 5504
    .end local v7    # "start":I
    :cond_7
    if-lez p2, :cond_9

    .line 5505
    sub-int/2addr v1, v5

    .line 5507
    add-int v6, v1, p2

    .line 5508
    .local v6, "end":I
    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v10

    if-le v6, v10, :cond_8

    .line 5509
    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v6

    .line 5512
    :cond_8
    invoke-interface {v4, v1, v6}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 5515
    .end local v6    # "end":I
    :cond_9
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->endBatchEdit()Z

    goto :goto_0
.end method

.method public finishComposingText()Z
    .locals 1

    .prologue
    .line 5557
    invoke-super {p0}, Landroid/view/inputmethod/BaseInputConnection;->finishComposingText()Z

    move-result v0

    return v0
.end method

.method public getEditable()Landroid/text/Editable;
    .locals 1

    .prologue
    .line 5568
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public getExtractedText(Landroid/view/inputmethod/ExtractedTextRequest;I)Landroid/view/inputmethod/ExtractedText;
    .locals 6
    .param p1, "request"    # Landroid/view/inputmethod/ExtractedTextRequest;
    .param p2, "flags"    # I

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 5607
    const/4 v1, 0x0

    .line 5609
    .local v1, "et":Landroid/view/inputmethod/ExtractedText;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v3, :cond_0

    .line 5610
    new-instance v1, Landroid/view/inputmethod/ExtractedText;

    .end local v1    # "et":Landroid/view/inputmethod/ExtractedText;
    invoke-direct {v1}, Landroid/view/inputmethod/ExtractedText;-><init>()V

    .line 5611
    .restart local v1    # "et":Landroid/view/inputmethod/ExtractedText;
    iput v4, v1, Landroid/view/inputmethod/ExtractedText;->flags:I

    .line 5612
    iput v5, v1, Landroid/view/inputmethod/ExtractedText;->partialStartOffset:I

    .line 5613
    iput v5, v1, Landroid/view/inputmethod/ExtractedText;->partialEndOffset:I

    .line 5614
    iput v4, v1, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    .line 5615
    const-string v3, ""

    iput-object v3, v1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    .line 5616
    iput v4, v1, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    .line 5617
    iput v4, v1, Landroid/view/inputmethod/ExtractedText;->selectionEnd:I

    .line 5620
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->getEditable()Landroid/text/Editable;

    move-result-object v0

    .line 5621
    .local v0, "content":Landroid/text/Editable;
    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 5623
    .local v2, "str":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 5624
    iput-object v2, v1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    .line 5629
    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v3

    iput v3, v1, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    .line 5630
    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v3

    iput v3, v1, Landroid/view/inputmethod/ExtractedText;->selectionEnd:I

    .line 5634
    .end local v0    # "content":Landroid/text/Editable;
    .end local v2    # "str":Ljava/lang/String;
    :cond_0
    return-object v1
.end method

.method public performContextMenuAction(I)Z
    .locals 7
    .param p1, "id"    # I

    .prologue
    const/4 v4, 0x0

    .line 5645
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-nez v5, :cond_1

    .line 5671
    :cond_0
    :goto_0
    return v4

    .line 5648
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "clipboard"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 5649
    .local v0, "clipboard":Landroid/content/ClipboardManager;
    if-eqz v0, :cond_0

    .line 5653
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    .line 5654
    .local v2, "start":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    .line 5655
    .local v1, "end":I
    if-le v2, v1, :cond_2

    .line 5656
    move v3, v2

    .line 5657
    .local v3, "temp":I
    move v2, v1

    .line 5658
    move v1, v3

    .line 5661
    .end local v3    # "temp":I
    :cond_2
    packed-switch p1, :pswitch_data_0

    .line 5671
    invoke-super {p0, p1}, Landroid/view/inputmethod/BaseInputConnection;->performContextMenuAction(I)Z

    move-result v4

    goto :goto_0

    .line 5663
    :pswitch_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->selectAll()Z
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$24(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v4

    goto :goto_0

    .line 5665
    :pswitch_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->cut(Landroid/content/ClipboardManager;II)Z
    invoke-static {v4, v0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/content/ClipboardManager;II)Z

    move-result v4

    goto :goto_0

    .line 5667
    :pswitch_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->copy(Landroid/content/ClipboardManager;II)Z
    invoke-static {v4, v0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$26(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/content/ClipboardManager;II)Z

    move-result v4

    goto :goto_0

    .line 5669
    :pswitch_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->paste(Landroid/content/ClipboardManager;II)Z
    invoke-static {v4, v0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$27(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/content/ClipboardManager;II)Z

    move-result v4

    goto :goto_0

    .line 5661
    nop

    :pswitch_data_0
    .packed-switch 0x102001f
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public sendKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 5575
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    .line 5600
    invoke-super {p0, p1}, Landroid/view/inputmethod/BaseInputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 5602
    .local v0, "ret":Z
    return v0
.end method

.method public setComposingText(Ljava/lang/CharSequence;I)Z
    .locals 3
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "newCursorPosition"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 5541
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->checkSelection(Ljava/lang/CharSequence;)V

    .line 5543
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 5545
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 5547
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    .line 5549
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 5551
    return v2
.end method
