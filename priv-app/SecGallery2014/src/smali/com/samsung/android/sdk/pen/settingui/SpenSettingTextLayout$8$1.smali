.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;
.super Ljava/lang/Object;
.source "SpenSettingTextLayout.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$NameDropdownSelectListner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    .line 759
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSelectItem(I)V
    .locals 8
    .param p1, "position"    # I

    .prologue
    const/high16 v7, 0x42040000    # 33.0f

    .line 763
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v5

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v5, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$54(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Ljava/lang/String;)V

    .line 765
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$27(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    move-result-object v4

    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(I)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTypeface(Landroid/graphics/Typeface;)V

    .line 767
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$27(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->invalidate()V

    .line 768
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$30(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 769
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$30(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    move-result-object v4

    invoke-interface {v4}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    .line 770
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontList()Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v5

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$55(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontName(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 771
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$30(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    move-result-object v4

    invoke-interface {v4, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 774
    .end local v0    # "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    :cond_0
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$56()I

    move-result v4

    const/16 v5, 0x10

    if-ge v4, v5, :cond_3

    .line 775
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$55(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/lang/String;

    move-result-object v2

    .line 776
    .local v2, "mTempFontName":Ljava/lang/String;
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 777
    .local v1, "mPaint":Landroid/graphics/Paint;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v4

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 778
    .local v3, "mTextWidth":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$55(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 779
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$57(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getWidth()I

    move-result v4

    .line 780
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v5

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    sub-int/2addr v4, v5

    if-lt v3, v4, :cond_2

    .line 781
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$57(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getWidth()I

    move-result v4

    .line 782
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v5

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    sub-int/2addr v4, v5

    .line 781
    if-gt v3, v4, :cond_1

    .line 788
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$57(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v5

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$55(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 796
    .end local v1    # "mPaint":Landroid/graphics/Paint;
    .end local v2    # "mTempFontName":Ljava/lang/String;
    .end local v3    # "mTextWidth":I
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$57(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/TextView;

    move-result-object v4

    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(I)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 797
    return-void

    .line 783
    .restart local v1    # "mPaint":Landroid/graphics/Paint;
    .restart local v2    # "mTempFontName":Ljava/lang/String;
    .restart local v3    # "mTextWidth":I
    :cond_1
    const/4 v4, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 784
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v4

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    .line 785
    const-string v6, "..."

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v6

    add-float/2addr v5, v6

    .line 784
    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 786
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "..."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$54(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 790
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$57(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v5

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$55(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 794
    .end local v1    # "mPaint":Landroid/graphics/Paint;
    .end local v2    # "mTempFontName":Ljava/lang/String;
    .end local v3    # "mTextWidth":I
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$57(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v5

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$55(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method
