.class Lcom/samsung/android/sdk/pen/pen/preload/Beautify$3;
.super Ljava/lang/Object;
.source "Beautify.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$3;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    .line 838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "buttonView"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 841
    const/4 v0, 0x0

    .local v0, "viewIndex":I
    :goto_0
    const/4 v1, 0x6

    if-lt v0, v1, :cond_0

    .line 855
    :goto_1
    return-void

    .line 842
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$3;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mButtonList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$8(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 843
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$3;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 844
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$3;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mButtonList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$8(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$3;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 845
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$3;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCursiveSeekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$4(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 846
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$3;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSustenanceSeekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$5(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 847
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$3;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mDummySeekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$6(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 848
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$3;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mModulationSeekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$7(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 849
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$3;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mButtonList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$8(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$3;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_1

    .line 852
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$3;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # invokes: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setBeautifyType(I)V
    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$9(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;I)V

    .line 841
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
