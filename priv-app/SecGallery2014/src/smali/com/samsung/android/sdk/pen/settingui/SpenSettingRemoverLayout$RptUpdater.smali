.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;
.super Ljava/lang/Object;
.source "SpenSettingRemoverLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RptUpdater"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V
    .locals 0

    .prologue
    .line 569
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 573
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoIncrement:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$35(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 574
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$27(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$27(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getMax()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 575
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$28(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$29(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)I

    move-result v2

    aget-object v1, v1, v2

    iget v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 576
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$30(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$31(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    add-float/2addr v2, v3

    .line 575
    iput v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 577
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$27(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/widget/SeekBar;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    .line 578
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$28(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$29(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)I

    move-result v3

    aget-object v2, v2, v3

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iput v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 580
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->repeatUpdateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$34(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    const-wide/16 v4, 0x14

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 593
    :cond_1
    :goto_0
    return-void

    .line 581
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoDecrement:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$38(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 582
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$27(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    if-eqz v1, :cond_3

    .line 583
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$28(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$29(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)I

    move-result v2

    aget-object v1, v1, v2

    iget v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 584
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$30(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$31(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    sub-float/2addr v2, v3

    .line 583
    iput v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 585
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$27(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/widget/SeekBar;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    .line 586
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$28(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$29(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)I

    move-result v3

    aget-object v2, v2, v3

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iput v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 588
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->repeatUpdateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$34(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    const-wide/16 v4, 0x14

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 590
    :catch_0
    move-exception v0

    .line 591
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
