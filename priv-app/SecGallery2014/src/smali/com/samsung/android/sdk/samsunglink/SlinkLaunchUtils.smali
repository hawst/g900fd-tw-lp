.class public Lcom/samsung/android/sdk/samsunglink/SlinkLaunchUtils;
.super Ljava/lang/Object;
.source "SlinkLaunchUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/samsunglink/SlinkLaunchUtils$LaunchToContentTypes;
    }
.end annotation


# static fields
.field public static final BROADCAST_SAMSUNG_LINK_STARTED:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkLaunchUtils.BROADCAST_SAMSUNG_LINK_STARTED"

.field public static final EXTRA_LAUNCH_TO_CONTENT_TYPE:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkLaunchUtils.EXTRA_LAUNCH_TO_CONTENT_TYPE"

.field public static final EXTRA_LAUNCH_TO_DEVICE_ID:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkLaunchUtils.EXTRA_LAUNCH_TO_DEVICE_ID"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method public static createLaunchToDeviceIntent(JJ)Landroid/content/Intent;
    .locals 2
    .param p0, "deviceId"    # J
    .param p2, "initialContentType"    # J

    .prologue
    .line 56
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mfluent.asp.ui.MobileChargesNotificationActivity"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 58
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkLaunchUtils.EXTRA_LAUNCH_TO_DEVICE_ID"

    invoke-virtual {v0, v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 59
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkLaunchUtils.EXTRA_LAUNCH_TO_CONTENT_TYPE"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 61
    return-object v0
.end method
