.class Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
.super Landroid/view/View;
.source "SpenColorPaletteView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;
    }
.end annotation


# static fields
.field private static final COLOR_COLUMN_NUM:I = 0x7

.field private static final COLOR_NUM_MAX:I = 0xe

.field private static final COLOR_ROW_NUM:I = 0x2

.field private static final CUSTOM_COLOR_IDX:I = 0xc

.field protected static final DEFAULT_COLOR:I = -0x1000000

.field protected static IS_COLOR_GRADATION_SELECT:Z = false

.field private static ITEM_BORDER_WIDTH:I = 0x0

.field private static final SPOID_ICON:I = -0x1100001

.field private static final USER_COLOR:I = -0x1000002

.field private static final WINDOW_BORDER_WIDTH:I

.field private static final mColorTableSetStringName:[[Ljava/lang/String;

.field private static final mColorTableStringName:[Ljava/lang/String;


# instance fields
.field private currentTableIndex:I

.field isRainbow:Z

.field private mBorderPaint:Landroid/graphics/Paint;

.field private mCanvasRect:Landroid/graphics/Rect;

.field private mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

.field public mColorContentDescritionTable:[Ljava/lang/String;

.field public mColorContentDescritionTableSet:[[Ljava/lang/String;

.field private mColorPaint:Landroid/graphics/Paint;

.field private mColorTable:[I

.field private mColorTableSet:[[I

.field private mCustom_imagepath:Ljava/lang/String;

.field private mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

.field private mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

.field private mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

.field private mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

.field mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private mFirstExecuted:Z

.field private mIsColorPickerEnable:Z

.field private mItemH:I

.field private mItemOffsetLeft:I

.field private mItemOffsetTop:I

.field private mItemRect:Landroid/graphics/Rect;

.field private mItemW:I

.field private mSelectRect:Landroid/graphics/Rect;

.field private mSeletedItem:I

.field private final mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

.field private mcurrentFocus:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 35
    sput-boolean v4, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->IS_COLOR_GRADATION_SELECT:Z

    .line 37
    sput v5, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    .line 109
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "string_color_royal_blue"

    aput-object v1, v0, v4

    const-string/jumbo v1, "string_color_indigo"

    aput-object v1, v0, v5

    .line 110
    const-string/jumbo v1, "string_color_permanent_violet"

    aput-object v1, v0, v6

    const-string/jumbo v1, "string_color_pitch_black"

    aput-object v1, v0, v7

    const-string/jumbo v1, "string_color_grey"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string/jumbo v2, "string_color_viridian"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 111
    const-string/jumbo v2, "string_color_sap_green"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "string_color_vandyke_brown"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "string_color_yellow_ochre"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 112
    const-string/jumbo v2, "string_color_burnt_umber"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "string_color_lilac"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "string_color_burgundy"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "string_color_palette"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 113
    const-string/jumbo v2, "string_color_picker_tts"

    aput-object v2, v0, v1

    .line 109
    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableStringName:[Ljava/lang/String;

    .line 115
    new-array v0, v8, [[Ljava/lang/String;

    .line 116
    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "string_color_royal_blue"

    aput-object v2, v1, v4

    const-string/jumbo v2, "string_color_indigo"

    aput-object v2, v1, v5

    const-string/jumbo v2, "string_color_permanent_violet"

    aput-object v2, v1, v6

    .line 117
    const-string/jumbo v2, "string_color_pitch_black"

    aput-object v2, v1, v7

    const-string/jumbo v2, "string_color_grey"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string/jumbo v3, "string_color_viridian"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "string_color_sap_green"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    .line 118
    const-string/jumbo v3, "string_color_vandyke_brown"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "string_color_yellow_ochre"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "string_color_burnt_umber"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    .line 119
    const-string/jumbo v3, "string_color_lilac"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "string_color_burgundy"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "string_color_palette"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string/jumbo v3, "string_color_picker_tts"

    aput-object v3, v1, v2

    aput-object v1, v0, v4

    .line 121
    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "string_color_white"

    aput-object v2, v1, v4

    const-string/jumbo v2, "string_color_yellow"

    aput-object v2, v1, v5

    const-string/jumbo v2, "string_color_coral"

    aput-object v2, v1, v6

    const-string/jumbo v2, "string_color_tomato"

    aput-object v2, v1, v7

    .line 122
    const-string/jumbo v2, "string_color_hot_pink"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string/jumbo v3, "string_color_plum"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "string_color_blue_violet"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    .line 123
    const-string/jumbo v3, "string_color_forest_green"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "string_color_sky_blue"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "string_color_peacock_blue"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    .line 124
    const-string/jumbo v3, "string_color_dark_grey"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "string_color_black"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "string_color_palette"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string/jumbo v3, "string_color_picker_tts"

    aput-object v3, v1, v2

    aput-object v1, v0, v5

    .line 126
    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "string_color_peach_puff"

    aput-object v2, v1, v4

    const-string/jumbo v2, "string_color_light_salmon"

    aput-object v2, v1, v5

    const-string/jumbo v2, "string_color_dark_salmon"

    aput-object v2, v1, v6

    .line 127
    const-string/jumbo v2, "string_color_light_pink"

    aput-object v2, v1, v7

    const-string/jumbo v2, "string_color_magenta"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string/jumbo v3, "string_color_crimson"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "string_color_red"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    .line 128
    const-string/jumbo v3, "string_color_maroon"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "string_color_purple"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "string_color_medium_orchid"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    .line 129
    const-string/jumbo v3, "string_color_dim_grey"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "string_color_saddle_brown"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "string_color_palette"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    .line 130
    const-string/jumbo v3, "string_color_picker_tts"

    aput-object v3, v1, v2

    aput-object v1, v0, v6

    .line 132
    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "string_color_gold"

    aput-object v2, v1, v4

    const-string/jumbo v2, "string_color_orange"

    aput-object v2, v1, v5

    const-string/jumbo v2, "string_color_chartreuse"

    aput-object v2, v1, v6

    const-string/jumbo v2, "string_color_lime_green"

    aput-object v2, v1, v7

    .line 133
    const-string/jumbo v2, "string_color_sea_green"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string/jumbo v3, "string_color_green"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "string_color_olive_drab"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    .line 134
    const-string/jumbo v3, "string_color_dark_cyan"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "string_color_teal"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "string_color_steel_blue"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "string_color_blue"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    .line 135
    const-string/jumbo v3, "string_color_dark_blue"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "string_color_palette"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string/jumbo v3, "string_color_picker_tts"

    aput-object v3, v1, v2

    aput-object v1, v0, v7

    .line 115
    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSetStringName:[[Ljava/lang/String;

    .line 137
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;F)V
    .locals 12
    .param p1, "paramContext"    # Landroid/content/Context;
    .param p2, "custom_imagepath"    # Ljava/lang/String;
    .param p3, "ratio"    # F

    .prologue
    const/16 v11, 0xa

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 172
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 42
    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 45
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->isRainbow:Z

    .line 47
    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mcurrentFocus:I

    .line 50
    iput v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    .line 52
    const/16 v0, 0xe

    new-array v0, v0, [I

    const/16 v1, 0x38

    const/16 v2, 0xd0

    invoke-static {v9, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v7

    const/16 v1, 0x4c

    const/16 v2, 0x71

    invoke-static {v7, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v8

    const/4 v1, 0x2

    const/16 v2, 0x28

    const/16 v3, 0x10

    const/16 v4, 0x67

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    .line 53
    const/16 v1, 0x9

    const/16 v2, 0xf

    invoke-static {v11, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v9

    const/16 v1, 0x54

    const/16 v2, 0x54

    const/16 v3, 0x54

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v10

    const/4 v1, 0x5

    const/16 v2, 0x15

    const/16 v3, 0x5d

    const/16 v4, 0x3a

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x45

    const/16 v3, 0x62

    const/16 v4, 0x16

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    .line 54
    const/16 v2, 0x62

    const/16 v3, 0x36

    const/16 v4, 0x17

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0xe5

    const/16 v3, 0x8a

    const/16 v4, 0x17

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0xa5

    const/16 v3, 0x3b

    const/16 v4, 0x11

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xa9

    const/16 v2, 0x36

    const/16 v3, 0x4d

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v11

    const/16 v1, 0xb

    .line 55
    const/16 v2, 0x7e

    const/16 v3, 0xe

    const/16 v4, 0x41

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    const v2, -0x1000002

    aput v2, v0, v1

    const/16 v1, 0xd

    const v2, -0x1100001

    aput v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    .line 57
    new-array v0, v10, [[I

    .line 58
    const/16 v1, 0xe

    new-array v1, v1, [I

    const/16 v2, 0x38

    const/16 v3, 0xd0

    invoke-static {v9, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v7

    const/16 v2, 0x4c

    const/16 v3, 0x71

    invoke-static {v7, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v8

    const/4 v2, 0x2

    const/16 v3, 0x28

    const/16 v4, 0x10

    const/16 v5, 0x67

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0x9

    const/16 v3, 0xf

    invoke-static {v11, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v9

    .line 59
    const/16 v2, 0x54

    const/16 v3, 0x54

    const/16 v4, 0x54

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v10

    const/4 v2, 0x5

    const/16 v3, 0x15

    const/16 v4, 0x5d

    const/16 v5, 0x3a

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x6

    const/16 v3, 0x45

    const/16 v4, 0x62

    const/16 v5, 0x16

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x7

    const/16 v3, 0x62

    const/16 v4, 0x36

    const/16 v5, 0x17

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0x8

    .line 60
    const/16 v3, 0xe5

    const/16 v4, 0x8a

    const/16 v5, 0x17

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0x9

    const/16 v3, 0xa5

    const/16 v4, 0x3b

    const/16 v5, 0x11

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0xa9

    const/16 v3, 0x36

    const/16 v4, 0x4d

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v11

    const/16 v2, 0xb

    const/16 v3, 0x7e

    const/16 v4, 0xe

    const/16 v5, 0x41

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0xc

    .line 61
    const v3, -0x1000002

    aput v3, v1, v2

    const/16 v2, 0xd

    const v3, -0x1100001

    aput v3, v1, v2

    aput-object v1, v0, v7

    .line 63
    const/16 v1, 0xe

    new-array v1, v1, [I

    const/16 v2, 0xff

    const/16 v3, 0xff

    const/16 v4, 0xff

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v7

    const/16 v2, 0xfd

    const/16 v3, 0xff

    const/16 v4, 0x2d

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v8

    const/4 v2, 0x2

    const/16 v3, 0xff

    const/16 v4, 0x83

    const/16 v5, 0x5d

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0xff

    const/16 v3, 0x3b

    const/16 v4, 0x5b

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v9

    .line 64
    const/16 v2, 0xff

    const/16 v3, 0x49

    const/16 v4, 0xc9

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v10

    const/4 v2, 0x5

    const/16 v3, 0xca

    const/16 v4, 0x85

    const/16 v5, 0xff

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x6

    const/16 v3, 0x7a

    const/16 v4, 0x37

    const/16 v5, 0xde

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x7

    const/16 v3, 0x94

    const/16 v4, 0x2e

    invoke-static {v8, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0x8

    .line 65
    const/16 v3, 0x38

    const/16 v4, 0xa8

    const/16 v5, 0xff

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0x9

    const/16 v3, 0x33

    const/16 v4, 0x67

    const/16 v5, 0xfd

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0xa6

    const/16 v3, 0xa5

    const/16 v4, 0xa5

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v11

    const/16 v2, 0xb

    invoke-static {v7, v7, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0xc

    .line 66
    const v3, -0x1000002

    aput v3, v1, v2

    const/16 v2, 0xd

    const v3, -0x1100001

    aput v3, v1, v2

    aput-object v1, v0, v8

    const/4 v1, 0x2

    .line 68
    const/16 v2, 0xe

    new-array v2, v2, [I

    const/16 v3, 0xff

    const/16 v4, 0xcd

    const/16 v5, 0xc1

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v7

    const/16 v3, 0xf9

    const/16 v4, 0xa4

    const/16 v5, 0x8f

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v8

    const/4 v3, 0x2

    const/16 v4, 0xf5

    const/16 v5, 0x7e

    const/16 v6, 0x60

    invoke-static {v4, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    aput v4, v2, v3

    const/16 v3, 0xff

    const/16 v4, 0x95

    const/16 v5, 0xa5

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v9

    .line 69
    const/16 v3, 0xf5

    const/16 v4, 0x4d

    const/16 v5, 0x67

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v10

    const/4 v3, 0x5

    const/16 v4, 0xed

    const/16 v5, 0x1a

    const/16 v6, 0x3b

    invoke-static {v4, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    aput v4, v2, v3

    const/4 v3, 0x6

    const/16 v4, 0xbc

    const/16 v5, 0x21

    invoke-static {v4, v10, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    aput v4, v2, v3

    const/4 v3, 0x7

    const/16 v4, 0x8b

    const/16 v5, 0x7c

    invoke-static {v4, v7, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    aput v4, v2, v3

    const/16 v3, 0x8

    .line 70
    const/16 v4, 0xbc

    const/16 v5, 0x5b

    invoke-static {v4, v10, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    aput v4, v2, v3

    const/16 v3, 0x9

    const/16 v4, 0x8c

    const/16 v5, 0x53

    const/16 v6, 0x7a

    invoke-static {v4, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    aput v4, v2, v3

    const/16 v3, 0x59

    const/16 v4, 0x42

    const/16 v5, 0x54

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v11

    const/16 v3, 0xb

    const/16 v4, 0x66

    const/16 v5, 0x30

    invoke-static {v4, v5, v11}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    aput v4, v2, v3

    const/16 v3, 0xc

    .line 71
    const v4, -0x1000002

    aput v4, v2, v3

    const/16 v3, 0xd

    const v4, -0x1100001

    aput v4, v2, v3

    aput-object v2, v0, v1

    .line 73
    const/16 v1, 0xe

    new-array v1, v1, [I

    const/16 v2, 0xfc

    const/16 v3, 0xe3

    const/16 v4, 0x3c

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v7

    const/16 v2, 0xfa

    const/16 v3, 0xc7

    const/16 v4, 0x4b

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v8

    const/4 v2, 0x2

    const/16 v3, 0xcb

    const/16 v4, 0xe3

    const/16 v5, 0x6c

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0x5b

    const/16 v3, 0xdd

    const/16 v4, 0x38

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v9

    .line 74
    const/16 v2, 0x41

    const/16 v3, 0xa5

    const/16 v4, 0x26

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v10

    const/4 v2, 0x5

    const/16 v3, 0xe

    const/16 v4, 0x80

    const/16 v5, 0x19

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x6

    const/16 v3, 0x66

    const/16 v4, 0x7f

    const/16 v5, 0x2a

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x7

    const/16 v3, 0xe

    const/16 v4, 0x80

    const/16 v5, 0x64

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0x8

    .line 75
    const/16 v3, 0x13

    const/16 v4, 0x9b

    const/16 v5, 0x69

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0x9

    const/4 v3, 0x5

    const/16 v4, 0x59

    const/16 v5, 0x75

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0x11

    const/16 v3, 0x4b

    const/16 v4, 0x9d

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v1, v11

    const/16 v2, 0xb

    const/16 v3, 0x29

    const/16 v4, 0x6c

    invoke-static {v11, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v1, v2

    const/16 v2, 0xc

    .line 76
    const v3, -0x1000002

    aput v3, v1, v2

    const/16 v2, 0xd

    const v3, -0x1100001

    aput v3, v1, v2

    aput-object v1, v0, v9

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    .line 79
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "White, Tap to apply"

    aput-object v1, v0, v7

    const-string v1, "Yellow, Tap to apply"

    aput-object v1, v0, v8

    const/4 v1, 0x2

    .line 80
    const-string v2, "Coral, Tap to apply"

    aput-object v2, v0, v1

    const-string v1, "Tomato, Tap to apply"

    aput-object v1, v0, v9

    const-string v1, "Hotpink, Tap to apply"

    aput-object v1, v0, v10

    const/4 v1, 0x5

    const-string v2, "Plum, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 81
    const-string v2, "Blueviolet, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Forestgreen, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Dodgeblue, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 82
    const-string v2, "Royalblue, Tap to apply"

    aput-object v2, v0, v1

    const-string v1, "Darkgray, Tap to apply"

    aput-object v1, v0, v11

    const/16 v1, 0xb

    const-string v2, "Black, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 83
    const-string v2, "Custom colour palette, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Spuit, Button"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTable:[Ljava/lang/String;

    .line 85
    new-array v0, v10, [[Ljava/lang/String;

    .line 86
    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "White, Tap to apply"

    aput-object v2, v1, v7

    const-string v2, "Yellow, Tap to apply"

    aput-object v2, v1, v8

    const/4 v2, 0x2

    const-string v3, "Coral, Tap to apply"

    aput-object v3, v1, v2

    const-string v2, "Tomato, Tap to apply"

    aput-object v2, v1, v9

    .line 87
    const-string v2, "Hotpink, Tap to apply"

    aput-object v2, v1, v10

    const/4 v2, 0x5

    const-string v3, "Plum, Tap to apply"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "Blueviolet, Tap to apply"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    .line 88
    const-string v3, "Forestgreen, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "Dodgeblue, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "Royalblue, Tap to apply"

    aput-object v3, v1, v2

    .line 89
    const-string v2, "Darkgray, Tap to apply"

    aput-object v2, v1, v11

    const/16 v2, 0xb

    const-string v3, "Black, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "Custom colour palette, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    .line 90
    const-string v3, "Spuit, Button"

    aput-object v3, v1, v2

    aput-object v1, v0, v7

    .line 92
    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "Peachpuff, Tap to apply"

    aput-object v2, v1, v7

    const-string v2, "Lightsalmon, Tap to apply"

    aput-object v2, v1, v8

    const/4 v2, 0x2

    const-string v3, "Darksalmon, Tap to apply"

    aput-object v3, v1, v2

    .line 93
    const-string v2, "linghtpink, Tap to apply"

    aput-object v2, v1, v9

    const-string v2, "Palevioletred, Tap to apply"

    aput-object v2, v1, v10

    const/4 v2, 0x5

    const-string v3, "Crimson, Tap to apply"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    .line 94
    const-string v3, "Red, Tap to apply"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "Mediumvioletred, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "Purple, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    .line 95
    const-string v3, "MediumOrchid, Tap to apply"

    aput-object v3, v1, v2

    const-string v2, "Dimgray, Tap to apply"

    aput-object v2, v1, v11

    const/16 v2, 0xb

    const-string v3, "Saddlebrown, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    .line 96
    const-string v3, "Custom colour palette, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "Spuit, Button"

    aput-object v3, v1, v2

    aput-object v1, v0, v8

    const/4 v1, 0x2

    .line 98
    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Gold, Tap to apply"

    aput-object v3, v2, v7

    const-string v3, "Orange, Tap to apply"

    aput-object v3, v2, v8

    const/4 v3, 0x2

    const-string v4, "Greenyellow, Tap to apply"

    aput-object v4, v2, v3

    const-string v3, "Limegreen, Tap to apply"

    aput-object v3, v2, v9

    .line 99
    const-string v3, "Seegreen, Tap to apply"

    aput-object v3, v2, v10

    const/4 v3, 0x5

    const-string v4, "Green, Tap to apply"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "OliverDrab, Tap to apply"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "Teal, Tap to apply"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    .line 100
    const-string v4, "DarkCyan, Tap to apply"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "Steelblue, Tap to apply"

    aput-object v4, v2, v3

    const-string v3, "Blue, Tap to apply"

    aput-object v3, v2, v11

    const/16 v3, 0xb

    .line 101
    const-string v4, "Darkblue, Tap to apply"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "Custom colour palette, Tap to apply"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "Spuit, Button"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    .line 103
    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "White, Tap to apply"

    aput-object v2, v1, v7

    const-string v2, "Yellow, Tap to apply"

    aput-object v2, v1, v8

    const/4 v2, 0x2

    const-string v3, "Coral, Tap to apply"

    aput-object v3, v1, v2

    const-string v2, "Tomato, Tap to apply"

    aput-object v2, v1, v9

    .line 104
    const-string v2, "Hotpink, Tap to apply"

    aput-object v2, v1, v10

    const/4 v2, 0x5

    const-string v3, "Plum, Tap to apply"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "Blueviolet, Tap to apply"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    .line 105
    const-string v3, "Forestgreen, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "Dodgeblue, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "Royalblue, Tap to apply"

    aput-object v3, v1, v2

    .line 106
    const-string v2, "Darkgray, Tap to apply"

    aput-object v2, v1, v11

    const/16 v2, 0xb

    const-string v3, "Black, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "Custom colour palette, Tap to apply"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    .line 107
    const-string v3, "Spuit, Button"

    aput-object v3, v1, v2

    aput-object v1, v0, v9

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTableSet:[[Ljava/lang/String;

    .line 144
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    .line 147
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mIsColorPickerEnable:Z

    .line 152
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCustom_imagepath:Ljava/lang/String;

    .line 462
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mFirstExecuted:Z

    .line 173
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 174
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCustom_imagepath:Ljava/lang/String;

    .line 175
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCustom_imagepath:Ljava/lang/String;

    invoke-direct {v0, p1, v1, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 176
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->initColorName()V

    .line 177
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->initView()V

    .line 178
    return-void
.end method

.method private drawSeleteBox(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v5, 0x0

    .line 554
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCanvasRect:Landroid/graphics/Rect;

    if-eqz v2, :cond_2

    .line 555
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    rem-int/lit8 v0, v2, 0x7

    .line 556
    .local v0, "m":I
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    div-int/lit8 v1, v2, 0x7

    .line 557
    .local v1, "n":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    if-nez v2, :cond_0

    .line 558
    new-instance v2, Landroid/graphics/Rect;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemW:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemH:I

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    .line 560
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSelectRect:Landroid/graphics/Rect;

    if-nez v2, :cond_1

    .line 561
    new-instance v2, Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    sub-int/2addr v4, v5

    .line 562
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    add-int/2addr v6, v7

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 561
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSelectRect:Landroid/graphics/Rect;

    .line 564
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSelectRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 565
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSelectRect:Landroid/graphics/Rect;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemOffsetLeft:I

    mul-int/2addr v3, v0

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemOffsetTop:I

    mul-int/2addr v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 567
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSelectRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 568
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 571
    .end local v0    # "m":I
    .end local v1    # "n":I
    :cond_2
    return-void
.end method

.method private getColorIdx(FF)I
    .locals 9
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v8, 0x7

    const/4 v7, 0x2

    const/4 v3, 0x0

    .line 643
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCanvasRect:Landroid/graphics/Rect;

    if-nez v4, :cond_0

    .line 667
    :goto_0
    return v3

    .line 647
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    if-nez v4, :cond_1

    .line 648
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41c80000    # 25.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 649
    .local v0, "itemH":I
    new-instance v4, Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemW:I

    invoke-direct {v4, v3, v3, v5, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    .line 651
    .end local v0    # "itemH":I
    :cond_1
    const/4 v1, 0x1

    .local v1, "m":I
    :goto_1
    if-le v1, v8, :cond_6

    .line 656
    :cond_2
    const/4 v2, 0x1

    .local v2, "n":I
    :goto_2
    if-le v2, v7, :cond_7

    .line 661
    :cond_3
    if-le v1, v8, :cond_4

    .line 662
    const/4 v1, 0x7

    .line 664
    :cond_4
    if-le v2, v7, :cond_5

    .line 665
    const/4 v2, 0x2

    .line 667
    :cond_5
    add-int/lit8 v3, v2, -0x1

    mul-int/lit8 v3, v3, 0x7

    add-int/lit8 v4, v1, -0x1

    add-int/2addr v3, v4

    goto :goto_0

    .line 652
    .end local v2    # "n":I
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    mul-int/2addr v3, v1

    add-int/lit8 v4, v1, -0x1

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x40400000    # 3.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    int-to-float v3, v3

    cmpg-float v3, p1, v3

    if-lez v3, :cond_2

    .line 651
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 657
    .restart local v2    # "n":I
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    mul-int/2addr v3, v2

    int-to-float v3, v3

    cmpg-float v3, p2, v3

    if-ltz v3, :cond_3

    .line 656
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method private initColorName()V
    .locals 6

    .prologue
    .line 181
    const-string v2, "SPenColorPaletteView"

    const-string v3, "initColorName"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTable:[Ljava/lang/String;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 188
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTableSet:[[Ljava/lang/String;

    array-length v2, v2

    if-lt v1, v2, :cond_1

    .line 194
    return-void

    .line 184
    .end local v1    # "j":I
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTable:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableStringName:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 185
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v5, "string_color_tap_to_apply"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 184
    aput-object v3, v2, v0

    .line 183
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 189
    .restart local v1    # "j":I
    :cond_1
    const/4 v0, 0x0

    :goto_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTable:[Ljava/lang/String;

    array-length v2, v2

    if-lt v0, v2, :cond_2

    .line 188
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 190
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTableSet:[[Ljava/lang/String;

    aget-object v2, v2, v1

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSetStringName:[[Ljava/lang/String;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 191
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v5, "string_color_tap_to_apply"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 190
    aput-object v3, v2, v0

    .line 189
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method private initView()V
    .locals 4

    .prologue
    const/high16 v1, 0x41c80000    # 25.0f

    const/16 v3, 0x1a

    const/16 v2, 0x19

    .line 312
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemW:I

    .line 313
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemH:I

    .line 314
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41e00000    # 28.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemOffsetLeft:I

    .line 315
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemOffsetTop:I

    .line 316
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    .line 319
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mBorderPaint:Landroid/graphics/Paint;

    .line 320
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mBorderPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 321
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mBorderPaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 322
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mBorderPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 324
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorPaint:Landroid/graphics/Paint;

    .line 325
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_colorchip_select_box"

    invoke-virtual {v0, v1, v2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    .line 326
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_colorchip_shadow"

    invoke-virtual {v0, v1, v2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    .line 327
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_colorbox_mini"

    invoke-virtual {v0, v1, v2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    .line 329
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mIsColorPickerEnable:Z

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_color_spoid_normal"

    invoke-virtual {v0, v1, v3, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    .line 334
    :goto_0
    return-void

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_color_spoid_dim"

    invoke-virtual {v0, v1, v3, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    goto :goto_0
.end method

.method private onDrawColorSet(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 508
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCanvasRect:Landroid/graphics/Rect;

    if-eqz v4, :cond_1

    .line 509
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    if-nez v4, :cond_0

    .line 510
    new-instance v4, Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemW:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemH:I

    invoke-direct {v4, v7, v7, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    .line 512
    :cond_0
    new-instance v3, Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    invoke-direct {v3, v4}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 513
    .local v3, "localRect":Landroid/graphics/Rect;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v4, 0x2

    if-lt v1, v4, :cond_2

    .line 543
    .end local v1    # "i":I
    .end local v3    # "localRect":Landroid/graphics/Rect;
    :cond_1
    return-void

    .line 514
    .restart local v1    # "i":I
    .restart local v3    # "localRect":Landroid/graphics/Rect;
    :cond_2
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    const/4 v4, 0x7

    if-lt v2, v4, :cond_3

    .line 540
    iget v4, v3, Landroid/graphics/Rect;->left:I

    neg-int v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemOffsetTop:I

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    .line 513
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 515
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    mul-int/lit8 v5, v1, 0x7

    add-int/2addr v5, v2

    aget v0, v4, v5

    .line 516
    .local v0, "color":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 517
    const v4, -0x1000002

    if-ne v0, v4, :cond_6

    .line 518
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->isRainbow:Z

    if-eqz v4, :cond_5

    .line 519
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 520
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 538
    :cond_4
    :goto_2
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemOffsetLeft:I

    invoke-virtual {v3, v4, v7}, Landroid/graphics/Rect;->offset(II)V

    .line 514
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 522
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 523
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 524
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2

    .line 528
    :cond_6
    const v4, -0x1100001

    if-ne v0, v4, :cond_7

    .line 529
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 530
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_4

    .line 531
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2

    .line 534
    :cond_7
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 535
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 536
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2
.end method


# virtual methods
.method protected close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 239
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCanvasRect:Landroid/graphics/Rect;

    .line 240
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    .line 241
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSelectRect:Landroid/graphics/Rect;

    .line 242
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mBorderPaint:Landroid/graphics/Paint;

    .line 243
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorPaint:Landroid/graphics/Paint;

    .line 244
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    .line 245
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCustom_imagepath:Ljava/lang/String;

    .line 246
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 248
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_1

    .line 249
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 250
    .local v0, "bm":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 251
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 253
    :cond_0
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    .line 255
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_3

    .line 256
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 257
    .restart local v0    # "bm":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 258
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 260
    :cond_2
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    .line 262
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_5

    .line 263
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 264
    .restart local v0    # "bm":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_4

    .line 265
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 267
    :cond_4
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    .line 270
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_7

    .line 271
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 272
    .restart local v0    # "bm":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_6

    .line 273
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 275
    :cond_6
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    .line 277
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    :cond_7
    return-void
.end method

.method public getColor()I
    .locals 2

    .prologue
    .line 722
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 723
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    aget v0, v0, v1

    .line 725
    :goto_0
    return v0

    :cond_0
    const v0, -0x1000002

    goto :goto_0
.end method

.method protected getCurrentTableIndex()I
    .locals 1

    .prologue
    .line 307
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 473
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 474
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getPaddingLeft()I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    int-to-float v5, v5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getPaddingTop()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    int-to-float v6, v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 476
    iget-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mFirstExecuted:Z

    if-eqz v5, :cond_1

    .line 477
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 478
    .local v1, "localLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getWidth()I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    div-int/lit8 v2, v5, 0x7

    .line 479
    .local v2, "m":I
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    array-length v5, v5

    int-to-float v5, v5

    const/high16 v6, 0x40e00000    # 7.0f

    div-float v4, v5, v6

    .line 480
    .local v4, "rowNumF":F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    array-length v5, v5

    div-int/lit8 v3, v5, 0x7

    .line 482
    .local v3, "rowNum":I
    int-to-float v5, v3

    cmpl-float v5, v4, v5

    if-lez v5, :cond_0

    .line 483
    add-int/lit8 v3, v3, 0x1

    .line 485
    :cond_0
    mul-int v5, v2, v3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getPaddingBottom()I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 486
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 487
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mFirstExecuted:Z

    .line 491
    .end local v1    # "localLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    .end local v2    # "m":I
    .end local v3    # "rowNum":I
    .end local v4    # "rowNumF":F
    :cond_1
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->onDrawColorSet(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 495
    :goto_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->drawSeleteBox(Landroid/graphics/Canvas;)V

    .line 496
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 497
    return-void

    .line 492
    :catch_0
    move-exception v0

    .line 493
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 703
    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 704
    if-eqz p1, :cond_0

    .line 705
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mcurrentFocus:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 706
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    .line 712
    :goto_0
    return-void

    .line 708
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 709
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v5, 0xe

    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 399
    const-string v1, "SpenColorPaletteView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "keycode = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    sparse-switch p1, :sswitch_data_0

    .line 459
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 403
    :sswitch_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x7

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x7

    if-ge v1, v5, :cond_0

    .line 406
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x7

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 407
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mcurrentFocus:I

    .line 408
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->playSoundEffect(I)V

    .line 410
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    goto :goto_0

    .line 414
    :sswitch_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x7

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x7

    if-ge v1, v5, :cond_0

    .line 417
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x7

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 418
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mcurrentFocus:I

    .line 419
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->playSoundEffect(I)V

    .line 421
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    goto :goto_0

    .line 425
    :sswitch_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x1

    if-ge v1, v5, :cond_0

    .line 428
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 429
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mcurrentFocus:I

    .line 430
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->playSoundEffect(I)V

    .line 432
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    goto :goto_0

    .line 436
    :sswitch_3
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x1

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x1

    if-ge v1, v5, :cond_0

    .line 439
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 440
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mcurrentFocus:I

    .line 441
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->playSoundEffect(I)V

    .line 443
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    goto :goto_0

    .line 448
    :sswitch_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    if-ltz v1, :cond_1

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 449
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    aget v2, v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;->colorChanged(II)V

    .line 452
    :cond_1
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->playSoundEffect(I)V

    goto/16 :goto_0

    .line 401
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x17 -> :sswitch_4
        0x42 -> :sswitch_4
    .end sparse-switch
.end method

.method protected onSizeChanged(IIII)V
    .locals 11
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    const/4 v10, 0x0

    const/high16 v9, 0x41c80000    # 25.0f

    .line 364
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 366
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getPaddingLeft()I

    move-result v1

    .line 367
    .local v1, "mPaddingLeft":I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getPaddingRight()I

    move-result v2

    .line 368
    .local v2, "mPaddingRight":I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getPaddingTop()I

    move-result v3

    .line 369
    .local v3, "mPaddingTop":I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getPaddingBottom()I

    move-result v0

    .line 371
    .local v0, "mPaddingBottom":I
    new-instance v4, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getLeft()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getTop()I

    move-result v6

    add-int/2addr v6, v3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getRight()I

    move-result v7

    sub-int/2addr v7, v2

    .line 372
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getBottom()I

    move-result v8

    sub-int/2addr v8, v0

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 371
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mCanvasRect:Landroid/graphics/Rect;

    .line 375
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemW:I

    .line 376
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemH:I

    .line 377
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41e00000    # 28.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemOffsetLeft:I

    .line 378
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41f00000    # 30.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemOffsetTop:I

    .line 379
    new-instance v4, Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemW:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemH:I

    invoke-direct {v4, v10, v10, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    .line 382
    new-instance v4, Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    .line 383
    sget v8, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    sget v9, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->ITEM_BORDER_WIDTH:I

    add-int/2addr v8, v9

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 382
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSelectRect:Landroid/graphics/Rect;

    .line 384
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mItemRect:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 385
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v4, 0xd

    const/4 v3, 0x0

    .line 583
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 589
    .local v0, "colorIdx":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 605
    :cond_0
    :goto_0
    :pswitch_0
    const/16 v1, 0xc

    if-ne v0, v1, :cond_4

    .line 606
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    .line 607
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 608
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->isRainbow:Z

    if-eqz v1, :cond_1

    .line 609
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->isRainbow:Z

    .line 621
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    if-ltz v1, :cond_2

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 622
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 628
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    .line 630
    const/4 v1, 0x1

    return v1

    .line 594
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getColorIdx(FF)I

    move-result v0

    .line 595
    if-lt v0, v4, :cond_3

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mIsColorPickerEnable:Z

    if-eqz v1, :cond_0

    .line 596
    :cond_3
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->playSoundEffect(I)V

    goto :goto_0

    .line 612
    :cond_4
    if-ne v0, v4, :cond_5

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mIsColorPickerEnable:Z

    if-eqz v1, :cond_1

    .line 613
    :cond_5
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 614
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mcurrentFocus:I

    goto :goto_1

    .line 624
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    aget v2, v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;->colorChanged(II)V

    goto :goto_2

    .line 589
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 622
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method

.method protected setBackColorTable(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 295
    add-int/lit8 p1, p1, -0x1

    .line 296
    if-nez p1, :cond_0

    .line 297
    const/4 p1, 0x4

    .line 299
    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    .line 300
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    .line 301
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    .line 303
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    .line 304
    return-void
.end method

.method public setColor(I)V
    .locals 8
    .param p1, "color"    # I

    .prologue
    const/4 v7, 0x0

    const/high16 v3, -0x1000000

    const/4 v6, 0x1

    const/16 v5, 0xc

    .line 740
    or-int/2addr p1, v3

    .line 742
    and-int/2addr v3, p1

    const/high16 v4, -0x2000000

    if-eq v3, v4, :cond_7

    .line 743
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    array-length v3, v3

    add-int/lit8 v2, v3, -0x1

    .local v2, "m":I
    :goto_0
    if-gez v2, :cond_1

    .line 750
    :goto_1
    if-gez v2, :cond_0

    .line 751
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    array-length v3, v3

    add-int/lit8 v0, v3, -0x2

    .local v0, "i":I
    :goto_2
    if-gez v0, :cond_3

    .line 758
    :goto_3
    if-gez v0, :cond_0

    .line 759
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 760
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    aput p1, v3, v5

    .line 761
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_4
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    array-length v3, v3

    if-lt v1, v3, :cond_6

    .line 764
    sput-boolean v6, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->IS_COLOR_GRADATION_SELECT:Z

    .line 776
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v2    # "m":I
    :cond_0
    :goto_5
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    .line 777
    return-void

    .line 744
    .restart local v2    # "m":I
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    aget v3, v3, v2

    if-ne p1, v3, :cond_2

    .line 745
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 746
    sput-boolean v7, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->IS_COLOR_GRADATION_SELECT:Z

    goto :goto_1

    .line 743
    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 752
    .restart local v0    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    aget-object v3, v3, v7

    aget v3, v3, v0

    if-eq p1, v3, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    aget-object v3, v3, v6

    aget v3, v3, v0

    if-eq p1, v3, :cond_4

    .line 753
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    const/4 v4, 0x2

    aget-object v3, v3, v4

    aget v3, v3, v0

    if-eq p1, v3, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    const/4 v4, 0x3

    aget-object v3, v3, v4

    aget v3, v3, v0

    if-ne p1, v3, :cond_5

    .line 754
    :cond_4
    const/4 v3, -0x1

    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    goto :goto_3

    .line 751
    :cond_5
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 762
    .restart local v1    # "j":I
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    aget-object v3, v3, v1

    aput p1, v3, v5

    .line 761
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 768
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v2    # "m":I
    :cond_7
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 769
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    aput p1, v3, v5

    .line 770
    const/4 v1, 0x0

    .restart local v1    # "j":I
    :goto_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    array-length v3, v3

    if-lt v1, v3, :cond_8

    .line 773
    sput-boolean v6, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->IS_COLOR_GRADATION_SELECT:Z

    goto :goto_5

    .line 771
    :cond_8
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    aget-object v3, v3, v1

    aput p1, v3, v5

    .line 770
    add-int/lit8 v1, v1, 0x1

    goto :goto_6
.end method

.method public setColorPickerColor(I)V
    .locals 4
    .param p1, "color"    # I

    .prologue
    const/high16 v3, -0x1000000

    const/16 v2, 0xc

    .line 787
    and-int v1, p1, v3

    if-nez v1, :cond_0

    .line 788
    or-int/2addr p1, v3

    .line 790
    :cond_0
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 791
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    aput p1, v1, v2

    .line 792
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 795
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    .line 796
    return-void

    .line 793
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    aget-object v1, v1, v0

    aput p1, v1, v2

    .line 792
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected setColorPickerEnable(Z)V
    .locals 3
    .param p1, "isEnable"    # Z

    .prologue
    const/16 v2, 0x1a

    .line 226
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mIsColorPickerEnable:Z

    .line 228
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    if-eqz v0, :cond_0

    .line 229
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mIsColorPickerEnable:Z

    if-eqz v0, :cond_1

    .line 230
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_color_spoid_normal"

    invoke-virtual {v0, v1, v2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    .line 234
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    .line 236
    :cond_0
    return-void

    .line 232
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_color_spoid_dim"

    invoke-virtual {v0, v1, v2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    goto :goto_0
.end method

.method public setColorPickerMode()V
    .locals 1

    .prologue
    .line 799
    const/16 v0, 0xc

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 800
    return-void
.end method

.method public setInitialValue(Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;I)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;
    .param p2, "color"    # I

    .prologue
    .line 345
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    .line 346
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setSelectBoxPos(I)V

    .line 347
    return-void
.end method

.method protected setMontblancColorPalette(Z)V
    .locals 8
    .param p1, "isEnabled"    # Z

    .prologue
    const/16 v7, 0xe

    const/4 v6, 0x3

    const/4 v1, 0x1

    const/16 v5, 0xb

    const/4 v4, 0x0

    .line 197
    if-eqz p1, :cond_1

    .line 198
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    if-ne v0, v1, :cond_0

    .line 199
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    const/16 v1, 0x1d

    const/16 v2, 0x2a

    const/16 v3, 0x44

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v4

    .line 200
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    const/16 v1, 0x63

    const/16 v2, 0x2e

    const/16 v3, 0x47

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v5

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    aget-object v0, v0, v4

    const/16 v1, 0x1d

    const/16 v2, 0x2a

    const/16 v3, 0x44

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v4

    .line 204
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    aget-object v0, v0, v4

    const/16 v1, 0x63

    const/16 v2, 0x2e

    const/16 v3, 0x47

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v5

    .line 222
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    .line 223
    return-void

    .line 206
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    if-ne v0, v1, :cond_2

    .line 207
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    const/16 v1, 0x38

    const/16 v2, 0xd0

    invoke-static {v6, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v4

    .line 208
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    const/16 v1, 0x7e

    const/16 v2, 0x41

    invoke-static {v1, v7, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v5

    .line 209
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTable:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_color_royal_blue"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 210
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_color_tap_to_apply"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 209
    aput-object v1, v0, v4

    .line 211
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTable:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_color_burgundy"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 212
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_color_tap_to_apply"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 211
    aput-object v1, v0, v5

    .line 214
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    aget-object v0, v0, v4

    const/16 v1, 0x38

    const/16 v2, 0xd0

    invoke-static {v6, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v4

    .line 215
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    aget-object v0, v0, v4

    const/16 v1, 0x7e

    const/16 v2, 0x41

    invoke-static {v1, v7, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v5

    .line 217
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTableSet:[[Ljava/lang/String;

    aget-object v0, v0, v4

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_color_royal_blue"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 218
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_color_tap_to_apply"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 217
    aput-object v1, v0, v4

    .line 219
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTableSet:[[Ljava/lang/String;

    aget-object v0, v0, v4

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_color_burgundy"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 220
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_color_tap_to_apply"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 219
    aput-object v1, v0, v5

    goto/16 :goto_0
.end method

.method protected setNextColorTable(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/4 v1, 0x5

    .line 281
    add-int/lit8 p1, p1, 0x1

    .line 282
    if-ne p1, v1, :cond_0

    .line 283
    const/4 p1, 0x1

    .line 285
    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    .line 286
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    if-ge v0, v1, :cond_1

    .line 287
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTableSet:[[I

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->currentTableIndex:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    .line 289
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    .line 291
    const/16 v0, 0xc2

    const/16 v1, 0x37

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->getHeight()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->onSizeChanged(IIII)V

    .line 292
    return-void
.end method

.method public setSelectBoxPos(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    const/high16 v1, -0x1000000

    .line 679
    if-ne p1, v1, :cond_0

    .line 680
    const/high16 p1, -0x1000000

    .line 684
    :cond_0
    or-int/2addr p1, v1

    .line 685
    const/4 v0, 0x0

    .line 686
    .local v0, "m":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-gez v0, :cond_1

    .line 692
    :goto_1
    if-gez v0, :cond_3

    .line 693
    const/4 v1, -0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    .line 698
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    .line 699
    return-void

    .line 687
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorTable:[I

    aget v1, v1, v0

    if-ne p1, v1, :cond_2

    .line 688
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    goto :goto_1

    .line 686
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 695
    :cond_3
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mSeletedItem:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mcurrentFocus:I

    goto :goto_2
.end method
