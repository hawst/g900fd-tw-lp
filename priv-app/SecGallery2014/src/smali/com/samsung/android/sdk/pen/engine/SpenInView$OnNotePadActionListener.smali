.class Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;
.super Ljava/lang/Object;
.source "SpenInView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenInView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnNotePadActionListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 0

    .prologue
    .line 6083
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;)V
    .locals 0

    .prologue
    .line 6083
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    return-void
.end method


# virtual methods
.method public onChangePan(FF)V
    .locals 7
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 6087
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 6100
    :cond_0
    :goto_0
    return-void

    .line 6090
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v2

    const/4 v6, 0x0

    move v4, p1

    move v5, p2

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPan(JFFZ)V
    invoke-static/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenInView;JFFZ)V

    .line 6091
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreenFrameBuffer()V

    .line 6092
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 6093
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreen()V

    .line 6096
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6097
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$53(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$54(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$55(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$56(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setPanAndZoom(FFFFF)V

    .line 6098
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setScreenStart(II)V

    goto :goto_0
.end method

.method public onCropBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;FF)V
    .locals 8
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "rect"    # Landroid/graphics/RectF;
    .param p3, "ratioX"    # F
    .param p4, "ratioY"    # F

    .prologue
    .line 6200
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmp-long v1, v2, v6

    if-nez v1, :cond_0

    .line 6212
    :goto_0
    return-void

    .line 6203
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BitmapInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$BitmapInfo;-><init>()V

    .line 6204
    .local v0, "info":Lcom/samsung/android/sdk/pen/engine/SpenInView$BitmapInfo;
    iput-object p1, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BitmapInfo;->bitmap:Landroid/graphics/Bitmap;

    .line 6205
    iput-object p2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BitmapInfo;->rect:Landroid/graphics/RectF;

    .line 6206
    iput p3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BitmapInfo;->ratioX:F

    .line 6207
    iput p4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$BitmapInfo;->ratioY:F

    .line 6208
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 6209
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6211
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v2

    const/4 v4, 0x2

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;
    invoke-static/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$61(Lcom/samsung/android/sdk/pen/engine/SpenInView;JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public onPostTouch(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 6183
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$60(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$60(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6184
    const-string v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Performance posttouch listener has consumed action = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 6185
    const/4 v0, 0x1

    .line 6187
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPreTouch(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 6174
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$59(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$59(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6175
    const-string v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Performance pretouch listener has consumed action = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 6176
    const/4 v0, 0x1

    .line 6178
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSizeChanged()V
    .locals 6

    .prologue
    .line 6192
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6193
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$53(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$54(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$55(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$56(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setPanAndZoom(FFFFF)V

    .line 6194
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setScreenStart(II)V

    .line 6196
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 4

    .prologue
    .line 6114
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 6125
    :cond_0
    :goto_0
    return-void

    .line 6117
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 6118
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 6121
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreenFrameBuffer()V

    .line 6122
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomPadListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$58(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6123
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomPadListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$58(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;->onStop()V

    goto :goto_0
.end method

.method public onUpdate(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 6104
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 6110
    :cond_0
    :goto_0
    return-void

    .line 6107
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 6108
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v2

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_onTouch(JLandroid/view/MotionEvent;I)Z
    invoke-static {v0, v2, v3, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$57(Lcom/samsung/android/sdk/pen/engine/SpenInView;JLandroid/view/MotionEvent;I)Z

    goto :goto_0
.end method

.method public onViewTouchEvent(Landroid/view/MotionEvent;FFFF)V
    .locals 26
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "startX"    # F
    .param p3, "startY"    # F
    .param p4, "rateW"    # F
    .param p5, "rateH"    # F

    .prologue
    .line 6129
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    move-result-object v9

    if-nez v9, :cond_0

    .line 6161
    :goto_0
    return-void

    .line 6132
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v21

    .line 6133
    .local v21, "x":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v22

    .line 6134
    .local v22, "y":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    and-int/lit16 v8, v9, 0xff

    .line 6135
    .local v8, "action":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    .line 6136
    .local v4, "downTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    .line 6137
    .local v6, "eventTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v12

    .line 6138
    .local v12, "metaState":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v13

    .line 6139
    .local v13, "buttonState":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getXPrecision()F

    move-result v14

    .line 6140
    .local v14, "xPrecision":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getYPrecision()F

    move-result v15

    .line 6141
    .local v15, "yPrecision":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v16

    .line 6142
    .local v16, "deviceId":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v17

    .line 6143
    .local v17, "edgeFlags":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v18

    .line 6144
    .local v18, "source":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getFlags()I

    move-result v19

    .line 6146
    .local v19, "flags":I
    const/4 v9, 0x1

    new-array v10, v9, [Landroid/view/MotionEvent$PointerProperties;

    .line 6147
    .local v10, "pp":[Landroid/view/MotionEvent$PointerProperties;
    const/4 v9, 0x0

    new-instance v23, Landroid/view/MotionEvent$PointerProperties;

    invoke-direct/range {v23 .. v23}, Landroid/view/MotionEvent$PointerProperties;-><init>()V

    aput-object v23, v10, v9

    .line 6148
    const/4 v9, 0x0

    aget-object v9, v10, v9

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v23

    move/from16 v0, v23

    iput v0, v9, Landroid/view/MotionEvent$PointerProperties;->toolType:I

    .line 6149
    const/4 v9, 0x1

    new-array v11, v9, [Landroid/view/MotionEvent$PointerCoords;

    .line 6150
    .local v11, "pc":[Landroid/view/MotionEvent$PointerCoords;
    const/4 v9, 0x0

    new-instance v23, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v23 .. v23}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v23, v11, v9

    .line 6152
    const/4 v9, 0x0

    aget-object v9, v10, v9

    const/16 v23, 0x0

    move/from16 v0, v23

    iput v0, v9, Landroid/view/MotionEvent$PointerProperties;->id:I

    .line 6153
    const/4 v9, 0x0

    aget-object v9, v11, v9

    mul-float v23, v21, p4

    add-float v23, v23, p2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-object/from16 v24, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I
    invoke-static/range {v24 .. v24}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    sub-float v23, v23, v24

    move/from16 v0, v23

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 6154
    const/4 v9, 0x0

    aget-object v9, v11, v9

    mul-float v23, v22, p5

    add-float v23, v23, p3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-object/from16 v24, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I
    invoke-static/range {v24 .. v24}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    sub-float v23, v23, v24

    move/from16 v0, v23

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 6156
    const/4 v9, 0x1

    invoke-static/range {v4 .. v19}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v20

    .line 6159
    .local v20, "ev":Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v24

    const/16 v23, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v23

    move-wide/from16 v0, v24

    move-object/from16 v2, v20

    move/from16 v3, v23

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_onTouch(JLandroid/view/MotionEvent;I)Z
    invoke-static {v9, v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$57(Lcom/samsung/android/sdk/pen/engine/SpenInView;JLandroid/view/MotionEvent;I)Z

    .line 6160
    invoke-virtual/range {v20 .. v20}, Landroid/view/MotionEvent;->recycle()V

    goto/16 :goto_0
.end method
