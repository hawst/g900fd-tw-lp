.class Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;
.super Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;
.source "SPenFontSizeDropdown.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;,
        Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$SizeDropdownSelectListner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# static fields
.field protected static isAutoScroll:Z = false

.field protected static final mDefaultPath:Ljava/lang/String; = ""

.field private static final mDropdownFocusPath:Ljava/lang/String; = "tw_spinner_list_focused_holo_light"

.field private static final mDropdownNormalPath:Ljava/lang/String; = ""

.field private static final mDropdownPressPath:Ljava/lang/String; = "tw_spinner_list_pressed_holo_light"

.field protected static mSdkVersion:I


# instance fields
.field private final context:Landroid/content/Context;

.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private final mHandler:Landroid/os/Handler;

.field private final mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private final mItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mListView:Landroid/widget/ListView;

.field private mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$SizeDropdownSelectListner;

.field private mOffsetX:I

.field private mOffsetY:I

.field private final mRunnable:Ljava/lang/Runnable;

.field private mSelectedIndex:I

.field private final root:Landroid/view/View;

.field private windowHeight:I

.field private windowWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->isAutoScroll:Z

    .line 39
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mSdkVersion:I

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/view/View;Ljava/util/ArrayList;IIF)V
    .locals 8
    .param p1, "anchor"    # Landroid/view/View;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "ratio"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;IIF)V"
        }
    .end annotation

    .prologue
    .line 184
    .local p2, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;-><init>(Landroid/view/View;)V

    .line 32
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$SizeDropdownSelectListner;

    .line 35
    const/4 v4, 0x0

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->windowWidth:I

    .line 36
    const/4 v4, 0x0

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->windowHeight:I

    .line 46
    const/4 v4, 0x0

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mOffsetY:I

    .line 47
    const/4 v4, 0x0

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mOffsetX:I

    .line 48
    const/4 v4, -0x1

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mSelectedIndex:I

    .line 241
    new-instance v4, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mRunnable:Ljava/lang/Runnable;

    .line 185
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mHandler:Landroid/os/Handler;

    .line 186
    iput p3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->windowWidth:I

    .line 187
    iput p4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->windowHeight:I

    .line 188
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->context:Landroid/content/Context;

    .line 189
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mItemList:Ljava/util/ArrayList;

    .line 190
    new-instance v4, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->context:Landroid/content/Context;

    const-string v6, ""

    invoke-direct {v4, v5, v6, p5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 192
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->context:Landroid/content/Context;

    const-string v5, "accessibility"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/accessibility/AccessibilityManager;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 194
    new-instance v2, Landroid/widget/FrameLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->context:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 195
    .local v2, "dropdownLayout":Landroid/widget/FrameLayout;
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x2

    .line 196
    const/4 v5, -0x2

    .line 195
    invoke-direct {v3, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 197
    .local v3, "dropdownLayoutParam":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 199
    new-instance v4, Landroid/widget/ListView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->context:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mListView:Landroid/widget/ListView;

    .line 205
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mListView:Landroid/widget/ListView;

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v6, -0x1

    const/4 v7, -0x1

    invoke-direct {v5, v6, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 207
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 209
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->root:Landroid/view/View;

    .line 210
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->root:Landroid/view/View;

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->setContentView(Landroid/view/View;)V

    .line 214
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->context:Landroid/content/Context;

    const v5, 0x1090003

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mItemList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;Landroid/content/Context;ILjava/util/List;)V

    .line 215
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 216
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 217
    sget v4, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mSdkVersion:I

    const/16 v5, 0x15

    if-ge v4, v5, :cond_3

    .line 218
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mListView:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v6, "tw_list_divider_holo_light"

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 219
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mListView:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 223
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v5, "tw_menu_dropdown_panel_holo_light"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 224
    .local v1, "background":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 225
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getScreenWidth()I

    move-result v4

    const/16 v5, 0x640

    if-ne v4, v5, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getDensity()F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_1

    .line 226
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getScreenWidth()I

    move-result v4

    const/16 v5, 0x300

    if-ne v4, v5, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getDensity()F

    move-result v4

    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v4, v4, v5

    if-nez v4, :cond_2

    .line 227
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getScreenWidth()I

    move-result v4

    const/16 v5, 0x640

    if-ne v4, v5, :cond_4

    .line 228
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v5, 0x4242ae14    # 48.67f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->windowWidth:I

    .line 232
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/lit8 v4, v4, 0x4

    neg-int v4, v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mOffsetY:I

    .line 233
    const/4 v4, -0x2

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mOffsetX:I

    .line 235
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->window:Landroid/widget/PopupWindow;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->windowWidth:I

    invoke-virtual {v4, v5}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 236
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v4, p4}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 237
    return-void

    .line 221
    .end local v1    # "background":Landroid/graphics/drawable/Drawable;
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mListView:Landroid/widget/ListView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 230
    .restart local v1    # "background":Landroid/graphics/drawable/Drawable;
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v5, 0x42746666    # 61.1f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->windowWidth:I

    goto :goto_1
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->windowHeight:I

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mOffsetX:I

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mOffsetY:I

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->windowWidth:I

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mSelectedIndex:I

    return v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Landroid/view/accessibility/AccessibilityManager;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method


# virtual methods
.method public changeOrientation(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->dismiss()V

    .line 320
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 324
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$SizeDropdownSelectListner;

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$SizeDropdownSelectListner;

    invoke-interface {v0, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$SizeDropdownSelectListner;->onSelectItem(I)V

    .line 327
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->dismiss()V

    .line 328
    return-void
.end method

.method public setOnItemSelectListner(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$SizeDropdownSelectListner;)V
    .locals 0
    .param p1, "listner"    # Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$SizeDropdownSelectListner;

    .prologue
    .line 331
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$SizeDropdownSelectListner;

    .line 332
    return-void
.end method

.method public show(IILjava/lang/String;)V
    .locals 8
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "setItem"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x2

    .line 281
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->show()V

    .line 282
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 283
    .local v0, "index":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 284
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mSelectedIndex:I

    .line 285
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setSelection(I)V

    .line 287
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->root:Landroid/view/View;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 288
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->root:Landroid/view/View;

    invoke-virtual {v2, v4, v4}, Landroid/view/View;->measure(II)V

    .line 290
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->windowHeight:I

    .line 291
    .local v1, "tempHeight":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->window:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->anchor:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;)I

    move-result v2

    if-le v1, v2, :cond_5

    .line 292
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->window:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->anchor:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;)I

    move-result v1

    .line 296
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->window:Landroid/widget/PopupWindow;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->windowWidth:I

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 297
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 299
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v2, v7, :cond_6

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 300
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_6

    :cond_1
    sget v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mSdkVersion:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_6

    .line 301
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v5}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 305
    :goto_1
    sput-boolean v5, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->isAutoScroll:Z

    .line 306
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getScreenWidth()I

    move-result v2

    const/16 v3, 0x640

    if-ne v2, v3, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getDensity()F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_3

    .line 307
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getScreenWidth()I

    move-result v2

    const/16 v3, 0x300

    if-ne v2, v3, :cond_7

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getDensity()F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_7

    .line 308
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->window:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->anchor:Landroid/view/View;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mOffsetX:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mOffsetY:I

    invoke-virtual {v2, v3, v4, v5}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    .line 312
    :goto_2
    sput-boolean v6, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->isAutoScroll:Z

    .line 313
    if-ne p2, v7, :cond_4

    .line 314
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->updatePosition()V

    .line 316
    :cond_4
    return-void

    .line 294
    :cond_5
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->windowHeight:I

    goto :goto_0

    .line 303
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v6}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    goto :goto_1

    .line 310
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->window:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->anchor:Landroid/view/View;

    invoke-virtual {v2, v3, p1, p2}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    goto :goto_2
.end method

.method public updatePosition()V
    .locals 4

    .prologue
    .line 272
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->window:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 274
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 276
    :cond_0
    return-void
.end method
