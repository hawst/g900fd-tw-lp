.class Lcom/samsung/android/sdk/pen/engine/SpenToolTip;
.super Ljava/lang/Object;
.source "SpenToolTip.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mDensity:F

.field private mEraserBitmap:Landroid/graphics/Bitmap;

.field private mEraserPaint:Landroid/graphics/Paint;

.field private mPenBitmap:Landroid/graphics/Bitmap;

.field private mPenImageMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private mPenList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

.field private mPenPaint:Landroid/graphics/Paint;

.field private mPoints:[Landroid/graphics/PointF;

.field private mPressures:[F

.field private mSdkResources:Landroid/content/res/Resources;

.field private mSpoidBitmap:Landroid/graphics/Bitmap;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x0

    const/high16 v9, 0x43160000    # 150.0f

    const/high16 v8, 0x42380000    # 46.0f

    const/4 v3, 0x0

    const/4 v7, 0x1

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    .line 33
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    .line 34
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    .line 35
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    .line 36
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    .line 45
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mContext:Landroid/content/Context;

    .line 47
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 48
    .local v2, "manager":Landroid/content/pm/PackageManager;
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    .end local v2    # "manager":Landroid/content/pm/PackageManager;
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 54
    .local v1, "localDisplayMetrics":Landroid/util/DisplayMetrics;
    iget v3, v1, Landroid/util/DisplayMetrics;->density:F

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mDensity:F

    .line 55
    new-instance v3, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-direct {v3, p1}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    .line 57
    const/4 v3, 0x3

    new-array v3, v3, [Landroid/graphics/PointF;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    .line 58
    const/4 v3, 0x3

    new-array v3, v3, [F

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    .line 59
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    .line 60
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 61
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 62
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setDither(Z)V

    .line 63
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    .line 64
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 65
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x40800000    # 4.0f

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 66
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    const/high16 v4, -0x1000000

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 67
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    const/16 v4, 0xff

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 68
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 70
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    new-instance v4, Landroid/graphics/PointF;

    const/4 v5, 0x0

    invoke-direct {v4, v5, v9}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v3, v10

    .line 71
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    new-instance v4, Landroid/graphics/PointF;

    const/high16 v5, 0x42480000    # 50.0f

    invoke-direct {v4, v5, v9}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v3, v7

    .line 72
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v4, 0x2

    new-instance v5, Landroid/graphics/PointF;

    const/high16 v6, 0x42c80000    # 100.0f

    invoke-direct {v5, v6, v9}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v5, v3, v4

    .line 73
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v3, v10

    .line 74
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const v4, 0x3f4ccccd    # 0.8f

    aput v4, v3, v7

    .line 75
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const/4 v4, 0x2

    const v5, 0x3f19999a    # 0.6f

    aput v5, v3, v4

    .line 77
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mDensity:F

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_0

    .line 78
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    new-instance v4, Landroid/graphics/PointF;

    const/4 v5, 0x0

    invoke-direct {v4, v5, v8}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v3, v10

    .line 79
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    new-instance v4, Landroid/graphics/PointF;

    const/high16 v5, 0x41f00000    # 30.0f

    invoke-direct {v4, v5, v8}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v3, v7

    .line 80
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v4, 0x2

    new-instance v5, Landroid/graphics/PointF;

    const/high16 v6, 0x42700000    # 60.0f

    invoke-direct {v5, v6, v8}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v5, v3, v4

    .line 82
    :cond_0
    return-void

    .line 49
    .end local v1    # "localDisplayMetrics":Landroid/util/DisplayMetrics;
    :catch_0
    move-exception v0

    .line 50
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_0
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 85
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 87
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 91
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 92
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 95
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 96
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    .line 98
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 99
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 100
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    .line 102
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    if-eqz v0, :cond_4

    .line 103
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->close()V

    .line 104
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    .line 106
    :cond_4
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mContext:Landroid/content/Context;

    .line 108
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    if-eqz v0, :cond_5

    .line 109
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->flushLayoutCache()V

    .line 110
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    .line 112
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    if-eqz v0, :cond_6

    .line 113
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    .line 114
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v1, 0x1

    aput-object v2, v0, v1

    .line 115
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v1, 0x2

    aput-object v2, v0, v1

    .line 116
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    .line 118
    :cond_6
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    .line 119
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    .line 120
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    .line 121
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    if-eqz v0, :cond_7

    .line 122
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 123
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    .line 125
    :cond_7
    return-void
.end method

.method public getDrawableEraserImage(F)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1, "size"    # F

    .prologue
    const/16 v2, 0xcc

    const/high16 v3, 0x42cc0000    # 102.0f

    .line 316
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    .line 317
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v2, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    .line 320
    :cond_0
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 321
    .local v0, "canvas":Landroid/graphics/Canvas;
    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 322
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v3, p1, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 323
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v1
.end method

.method public getDrawableEraserImage(FFF)Landroid/graphics/drawable/Drawable;
    .locals 8
    .param p1, "size"    # F
    .param p2, "rx"    # F
    .param p3, "ry"    # F

    .prologue
    const/16 v5, 0xcc

    const/high16 v7, 0x42cc0000    # 102.0f

    .line 327
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    if-nez v4, :cond_0

    .line 328
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v5, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    .line 332
    :cond_0
    mul-float v2, p1, p2

    .line 333
    .local v2, "xr":F
    mul-float v3, p1, p3

    .line 334
    .local v3, "yr":F
    new-instance v1, Landroid/graphics/RectF;

    sub-float v4, v7, v2

    sub-float v5, v7, v3

    add-float v6, v7, v2

    add-float/2addr v7, v3

    invoke-direct {v1, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 336
    .local v1, "ovalRect":Landroid/graphics/RectF;
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 337
    .local v0, "canvas":Landroid/graphics/Canvas;
    const/4 v4, 0x0

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 338
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 340
    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v4, v5, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v4
.end method

.method public getDrawableHoverImage()Landroid/graphics/drawable/Drawable;
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/high16 v12, 0x3f800000    # 1.0f

    const/16 v11, 0x64

    const/16 v10, 0x39

    const/4 v5, 0x0

    .line 376
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    const-string/jumbo v7, "snote_toolbar_icon_spoid_hover"

    const-string v8, "drawable"

    .line 377
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v9

    .line 376
    invoke-virtual {v6, v7, v8, v9}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 378
    .local v4, "mDrawableResID":I
    if-nez v4, :cond_1

    .line 410
    :cond_0
    :goto_0
    return-object v5

    .line 381
    :cond_1
    const/4 v1, 0x0

    .line 382
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mDensity:F

    cmpl-float v6, v6, v12

    if-nez v6, :cond_3

    .line 383
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    const/16 v7, 0x1e

    const/16 v8, 0x1e

    invoke-virtual {p0, v6, v4, v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 387
    :goto_1
    if-eqz v1, :cond_0

    .line 391
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    if-nez v6, :cond_2

    .line 392
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v11, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    .line 394
    :cond_2
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 395
    .local v3, "iconBitmap":Landroid/graphics/Bitmap;
    if-eqz v3, :cond_0

    .line 399
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 400
    .local v0, "canvas":Landroid/graphics/Canvas;
    sget-object v6, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v13, v6}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 401
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 402
    .local v2, "dstRect":Landroid/graphics/Rect;
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mDensity:F

    cmpl-float v6, v6, v12

    if-nez v6, :cond_4

    .line 403
    const/16 v6, 0x2b

    const/16 v7, 0x17

    const/16 v8, 0x49

    const/16 v9, 0x35

    invoke-virtual {v2, v6, v7, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 407
    :goto_2
    invoke-virtual {v0, v3, v5, v2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 410
    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v5, v6, v7}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 385
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v2    # "dstRect":Landroid/graphics/Rect;
    .end local v3    # "iconBitmap":Landroid/graphics/Bitmap;
    .restart local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {p0, v6, v4, v10, v10}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_1

    .line 405
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v0    # "canvas":Landroid/graphics/Canvas;
    .restart local v2    # "dstRect":Landroid/graphics/Rect;
    .restart local v3    # "iconBitmap":Landroid/graphics/Bitmap;
    :cond_4
    const/16 v6, 0x2b

    invoke-virtual {v2, v6, v13, v11, v10}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_2
.end method

.method public getDrawableImage(Ljava/lang/String;IF)Landroid/graphics/drawable/Drawable;
    .locals 35
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "color"    # I
    .param p3, "size"    # F

    .prologue
    .line 128
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    if-nez v8, :cond_0

    .line 129
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->getPenInfoList()Ljava/util/List;

    move-result-object v8

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    .line 130
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    if-nez v8, :cond_0

    .line 131
    const/4 v8, 0x0

    .line 312
    :goto_0
    return-object v8

    .line 134
    :cond_0
    const/16 v33, 0x0

    .local v33, "pos":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    move/from16 v0, v33

    if-lt v0, v8, :cond_1

    .line 312
    const/4 v8, 0x0

    goto :goto_0

    .line 135
    :cond_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    move/from16 v0, v33

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;

    .line 136
    .local v28, "info":Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_20

    .line 137
    const/16 v24, 0x0

    .line 138
    .local v24, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    if-eqz v8, :cond_2

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 139
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    .end local v24    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    check-cast v24, Landroid/graphics/drawable/BitmapDrawable;

    .line 141
    .restart local v24    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    :cond_2
    if-nez v24, :cond_3

    .line 142
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->iconImageUri:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v24

    .end local v24    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    check-cast v24, Landroid/graphics/drawable/BitmapDrawable;

    .line 144
    .restart local v24    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    :cond_3
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    if-nez v8, :cond_4

    .line 145
    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mDensity:F

    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v8, v8, v9

    if-nez v8, :cond_9

    .line 146
    const/16 v8, 0x3c

    const/16 v9, 0x3c

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    .line 152
    :cond_4
    :goto_2
    new-instance v25, Landroid/graphics/Rect;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/Rect;-><init>()V

    .line 153
    .local v25, "dst":Landroid/graphics/Rect;
    new-instance v22, Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, v22

    invoke-direct {v0, v8}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 154
    .local v22, "canvas":Landroid/graphics/Canvas;
    const/4 v8, 0x0

    sget-object v9, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 156
    :try_start_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->createPen(Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;)Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-result-object v31

    .line 157
    .local v31, "pen":Lcom/samsung/android/sdk/pen/pen/SpenPen;
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_b

    .line 159
    const-string/jumbo v8, "snote_popup_pensetting_preview_alpha"

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v30

    check-cast v30, Landroid/graphics/drawable/BitmapDrawable;

    .line 160
    .local v30, "magic":Landroid/graphics/drawable/BitmapDrawable;
    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mDensity:F

    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v8, v8, v9

    if-nez v8, :cond_a

    .line 161
    const/4 v8, 0x0

    const/16 v9, 0x2e

    const/16 v10, 0x3c

    const/high16 v11, 0x42380000    # 46.0f

    add-float v11, v11, p3

    float-to-int v11, v11

    move-object/from16 v0, v25

    invoke-virtual {v0, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 165
    :goto_3
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    shr-int/lit8 v9, p2, 0x18

    and-int/lit16 v9, v9, 0xff

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 167
    if-eqz v30, :cond_5

    .line 168
    invoke-virtual/range {v30 .. v30}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v8, v9, v1, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 170
    :cond_5
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    const/16 v9, 0xff

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 171
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, v31

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 266
    .end local v30    # "magic":Landroid/graphics/drawable/BitmapDrawable;
    :goto_4
    const/4 v8, 0x0

    move-object/from16 v0, v31

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 267
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    move-object/from16 v0, v31

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->destroyPen(Lcom/samsung/android/sdk/pen/pen/SpenPen;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 278
    .end local v31    # "pen":Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :goto_5
    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mDensity:F

    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v8, v8, v9

    if-nez v8, :cond_16

    .line 279
    const/4 v8, 0x5

    const/4 v9, 0x5

    const/16 v10, 0x41

    const/16 v11, 0x41

    move-object/from16 v0, v25

    invoke-virtual {v0, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 283
    :goto_6
    if-eqz v24, :cond_8

    .line 284
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_6

    .line 285
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.MontblancFountainPen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_17

    .line 286
    :cond_6
    const/high16 v8, -0x3e500000    # -22.0f

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 306
    :cond_7
    :goto_7
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v8, v9, v1, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 309
    :cond_8
    new-instance v8, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v8, v9, v10}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 148
    .end local v22    # "canvas":Landroid/graphics/Canvas;
    .end local v25    # "dst":Landroid/graphics/Rect;
    :cond_9
    const/16 v8, 0x64

    const/16 v9, 0x12c

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_2

    .line 163
    .restart local v22    # "canvas":Landroid/graphics/Canvas;
    .restart local v25    # "dst":Landroid/graphics/Rect;
    .restart local v30    # "magic":Landroid/graphics/drawable/BitmapDrawable;
    .restart local v31    # "pen":Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :cond_a
    const/4 v8, 0x0

    const/16 v9, 0x96

    const/16 v10, 0x64

    const/high16 v11, 0x43160000    # 150.0f

    add-float v11, v11, p3

    float-to-int v11, v11

    :try_start_1
    move-object/from16 v0, v25

    invoke-virtual {v0, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    goto/16 :goto_3

    .line 268
    .end local v30    # "magic":Landroid/graphics/drawable/BitmapDrawable;
    .end local v31    # "pen":Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :catch_0
    move-exception v26

    .line 269
    .local v26, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual/range {v26 .. v26}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_5

    .line 172
    .end local v26    # "e":Ljava/lang/ClassNotFoundException;
    .restart local v31    # "pen":Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :cond_b
    :try_start_2
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_d

    .line 173
    const/16 v8, 0x12c

    const/16 v9, 0x12c

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v29

    .line 174
    .local v29, "mOblicquePenBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, v31

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setColor(I)V

    .line 175
    const/high16 v8, 0x40400000    # 3.0f

    mul-float v8, v8, p3

    move-object/from16 v0, v31

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setSize(F)V

    .line 176
    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 177
    const/4 v8, 0x3

    new-array v0, v8, [Landroid/graphics/PointF;

    move-object/from16 v32, v0

    .line 178
    .local v32, "points":[Landroid/graphics/PointF;
    const/4 v8, 0x0

    new-instance v9, Landroid/graphics/PointF;

    const/high16 v10, 0x42700000    # 60.0f

    const/high16 v11, 0x43160000    # 150.0f

    invoke-direct {v9, v10, v11}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v9, v32, v8

    .line 179
    const/4 v8, 0x1

    new-instance v9, Landroid/graphics/PointF;

    const/high16 v10, 0x43160000    # 150.0f

    const/high16 v11, 0x43160000    # 150.0f

    invoke-direct {v9, v10, v11}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v9, v32, v8

    .line 180
    const/4 v8, 0x2

    new-instance v9, Landroid/graphics/PointF;

    const/high16 v10, 0x43730000    # 243.0f

    const/high16 v11, 0x43160000    # 150.0f

    invoke-direct {v9, v10, v11}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v9, v32, v8

    .line 182
    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mDensity:F

    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v8, v8, v9

    if-nez v8, :cond_c

    .line 183
    const/4 v8, 0x0

    new-instance v9, Landroid/graphics/PointF;

    const/high16 v10, 0x41f00000    # 30.0f

    const/high16 v11, 0x42480000    # 50.0f

    invoke-direct {v9, v10, v11}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v9, v32, v8

    .line 184
    const/4 v8, 0x1

    new-instance v9, Landroid/graphics/PointF;

    const/high16 v10, 0x42b40000    # 90.0f

    const/high16 v11, 0x42480000    # 50.0f

    invoke-direct {v9, v10, v11}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v9, v32, v8

    .line 185
    const/4 v8, 0x2

    new-instance v9, Landroid/graphics/PointF;

    const/high16 v10, 0x43160000    # 150.0f

    const/high16 v11, 0x42480000    # 50.0f

    invoke-direct {v9, v10, v11}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v9, v32, v8

    .line 186
    move/from16 v0, p3

    float-to-double v8, v0

    const-wide v10, 0x3ff3333333333333L    # 1.2

    mul-double/2addr v8, v10

    double-to-float v0, v8

    move/from16 p3, v0

    .line 187
    move-object/from16 v0, v31

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setSize(F)V

    .line 190
    :cond_c
    new-instance v34, Landroid/graphics/RectF;

    invoke-direct/range {v34 .. v34}, Landroid/graphics/RectF;-><init>()V

    .line 191
    .local v34, "rect":Landroid/graphics/RectF;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 193
    .local v4, "time":J
    const/4 v8, 0x0

    const/4 v9, 0x0

    aget-object v9, v32, v9

    iget v9, v9, Landroid/graphics/PointF;->x:F

    .line 194
    const/4 v10, 0x0

    aget-object v10, v32, v10

    iget v10, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const/4 v12, 0x0

    aget v11, v11, v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-wide v6, v4

    move/from16 v12, p3

    .line 193
    invoke-static/range {v4 .. v17}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v27

    .line 195
    .local v27, "event":Landroid/view/MotionEvent;
    move-object/from16 v0, v31

    move-object/from16 v1, v27

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 196
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->recycle()V

    .line 199
    const-wide/16 v8, 0x5

    add-long v6, v4, v8

    .line 200
    .local v6, "currentTime":J
    const/4 v8, 0x2

    .line 201
    const/4 v9, 0x1

    aget-object v9, v32, v9

    iget v9, v9, Landroid/graphics/PointF;->x:F

    const/4 v10, 0x1

    aget-object v10, v32, v10

    iget v10, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const/4 v12, 0x1

    aget v11, v11, v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v12, p3

    .line 200
    invoke-static/range {v4 .. v17}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v27

    .line 202
    move-object/from16 v0, v31

    move-object/from16 v1, v27

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 203
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->recycle()V

    .line 206
    const-wide/16 v8, 0xa

    add-long v6, v4, v8

    .line 207
    const/4 v8, 0x1

    .line 208
    const/4 v9, 0x2

    aget-object v9, v32, v9

    iget v9, v9, Landroid/graphics/PointF;->x:F

    const/4 v10, 0x2

    aget-object v10, v32, v10

    iget v10, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const/4 v12, 0x2

    aget v11, v11, v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v12, p3

    .line 207
    invoke-static/range {v4 .. v17}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v27

    .line 209
    move-object/from16 v0, v31

    move-object/from16 v1, v27

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 210
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->recycle()V

    .line 213
    new-instance v23, Landroid/graphics/Rect;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x64

    const/16 v11, 0x12c

    move-object/from16 v0, v23

    invoke-direct {v0, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 214
    .local v23, "dest":Landroid/graphics/Rect;
    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    move-object/from16 v0, v22

    move-object/from16 v1, v29

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v8, v2, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 215
    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->recycle()V

    .line 216
    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x2

    const/4 v11, 0x0

    aput-object v11, v32, v10

    aput-object v11, v32, v9

    aput-object v11, v32, v8
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_4

    .line 270
    .end local v4    # "time":J
    .end local v6    # "currentTime":J
    .end local v23    # "dest":Landroid/graphics/Rect;
    .end local v27    # "event":Landroid/view/MotionEvent;
    .end local v29    # "mOblicquePenBitmap":Landroid/graphics/Bitmap;
    .end local v31    # "pen":Lcom/samsung/android/sdk/pen/pen/SpenPen;
    .end local v32    # "points":[Landroid/graphics/PointF;
    .end local v34    # "rect":Landroid/graphics/RectF;
    :catch_1
    move-exception v26

    .line 271
    .local v26, "e":Ljava/lang/InstantiationException;
    invoke-virtual/range {v26 .. v26}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto/16 :goto_5

    .line 219
    .end local v26    # "e":Ljava/lang/InstantiationException;
    .restart local v31    # "pen":Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :cond_d
    :try_start_3
    move-object/from16 v0, v31

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setColor(I)V

    .line 220
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_10

    .line 221
    move/from16 v0, p3

    float-to-double v8, v0

    const-wide/high16 v10, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v8, v10

    double-to-float v0, v8

    move/from16 p3, v0

    .line 225
    :cond_e
    :goto_8
    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mDensity:F

    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v8, v8, v9

    if-nez v8, :cond_15

    .line 226
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_11

    .line 227
    move/from16 v0, p3

    float-to-double v8, v0

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v8, v10

    double-to-float v0, v8

    move/from16 p3, v0

    .line 237
    :cond_f
    :goto_9
    move-object/from16 v0, v31

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setSize(F)V

    .line 241
    :goto_a
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, v31

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 243
    new-instance v34, Landroid/graphics/RectF;

    invoke-direct/range {v34 .. v34}, Landroid/graphics/RectF;-><init>()V

    .line 244
    .restart local v34    # "rect":Landroid/graphics/RectF;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 246
    .restart local v4    # "time":J
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    iget v13, v8, Landroid/graphics/PointF;->x:F

    .line 247
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    iget v14, v8, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const/4 v9, 0x0

    aget v15, v8, v9

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-wide v8, v4

    move-wide v10, v4

    move/from16 v16, p3

    .line 246
    invoke-static/range {v8 .. v21}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v27

    .line 248
    .restart local v27    # "event":Landroid/view/MotionEvent;
    move-object/from16 v0, v31

    move-object/from16 v1, v27

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 249
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->recycle()V

    .line 252
    const-wide/16 v8, 0x5

    add-long v6, v4, v8

    .line 253
    .restart local v6    # "currentTime":J
    const/4 v8, 0x2

    .line 254
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v10, 0x1

    aget-object v9, v9, v10

    iget v9, v9, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v11, 0x1

    aget-object v10, v10, v11

    iget v10, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const/4 v12, 0x1

    aget v11, v11, v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v12, p3

    .line 253
    invoke-static/range {v4 .. v17}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v27

    .line 255
    move-object/from16 v0, v31

    move-object/from16 v1, v27

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 256
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->recycle()V

    .line 259
    const-wide/16 v8, 0xa

    add-long v6, v4, v8

    .line 260
    const/4 v8, 0x1

    .line 261
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v10, 0x2

    aget-object v9, v9, v10

    iget v9, v9, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v11, 0x2

    aget-object v10, v10, v11

    iget v10, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const/4 v12, 0x2

    aget v11, v11, v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v12, p3

    .line 260
    invoke-static/range {v4 .. v17}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v27

    .line 262
    move-object/from16 v0, v31

    move-object/from16 v1, v27

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 263
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->recycle()V
    :try_end_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_4

    .line 272
    .end local v4    # "time":J
    .end local v6    # "currentTime":J
    .end local v27    # "event":Landroid/view/MotionEvent;
    .end local v31    # "pen":Lcom/samsung/android/sdk/pen/pen/SpenPen;
    .end local v34    # "rect":Landroid/graphics/RectF;
    :catch_2
    move-exception v26

    .line 273
    .local v26, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual/range {v26 .. v26}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto/16 :goto_5

    .line 222
    .end local v26    # "e":Ljava/lang/IllegalAccessException;
    .restart local v31    # "pen":Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :cond_10
    :try_start_4
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_e

    .line 223
    move/from16 v0, p3

    float-to-double v8, v0

    const-wide v10, 0x3ff3333333333333L    # 1.2

    mul-double/2addr v8, v10

    double-to-float v0, v8

    move/from16 p3, v0

    goto/16 :goto_8

    .line 228
    :cond_11
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_12

    .line 229
    move/from16 v0, p3

    float-to-double v8, v0

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v8, v10

    double-to-float v0, v8

    move/from16 p3, v0

    .line 230
    goto/16 :goto_9

    :cond_12
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_13

    .line 231
    move/from16 v0, p3

    float-to-double v8, v0

    const-wide v10, 0x3ff199999999999aL    # 1.1

    mul-double/2addr v8, v10

    double-to-float v0, v8

    move/from16 p3, v0

    .line 232
    goto/16 :goto_9

    :cond_13
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_14

    .line 233
    move/from16 v0, p3

    float-to-double v8, v0

    const-wide v10, 0x3fe6666666666666L    # 0.7

    mul-double/2addr v8, v10

    double-to-float v0, v8

    move/from16 p3, v0

    .line 234
    goto/16 :goto_9

    :cond_14
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_f

    .line 235
    move/from16 v0, p3

    float-to-double v8, v0

    const-wide v10, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v8, v10

    double-to-float v0, v8

    move/from16 p3, v0

    goto/16 :goto_9

    .line 239
    :cond_15
    const/high16 v8, 0x40400000    # 3.0f

    mul-float v8, v8, p3

    move-object/from16 v0, v31

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setSize(F)V
    :try_end_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_a

    .line 274
    .end local v31    # "pen":Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :catch_3
    move-exception v26

    .line 275
    .local v26, "e":Ljava/lang/Exception;
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_5

    .line 281
    .end local v26    # "e":Ljava/lang/Exception;
    :cond_16
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x64

    const/16 v11, 0xfa

    move-object/from16 v0, v25

    invoke-virtual {v0, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_6

    .line 287
    :cond_17
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_18

    .line 288
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    .line 289
    const-string v9, "com.samsung.android.sdk.pen.pen.preload.MontblancCalligraphyPen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    .line 288
    if-nez v8, :cond_19

    .line 290
    :cond_18
    const/high16 v8, -0x3eb00000    # -13.0f

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_7

    .line 291
    :cond_19
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_1a

    .line 292
    const/high16 v8, -0x3e600000    # -20.0f

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_7

    .line 293
    :cond_1a
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_1b

    .line 294
    const/high16 v8, -0x3e700000    # -18.0f

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_7

    .line 295
    :cond_1b
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_1c

    .line 296
    const/high16 v8, -0x3e900000    # -15.0f

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_7

    .line 297
    :cond_1c
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_1d

    .line 298
    const/high16 v8, -0x3e500000    # -22.0f

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_7

    .line 299
    :cond_1d
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_1e

    .line 300
    const/high16 v8, -0x3e900000    # -15.0f

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_7

    .line 301
    :cond_1e
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_1f

    .line 302
    const/high16 v8, -0x3e500000    # -22.0f

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_7

    .line 303
    :cond_1f
    move-object/from16 v0, v28

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_7

    .line 304
    const/high16 v8, -0x3e600000    # -20.0f

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_7

    .line 134
    .end local v22    # "canvas":Landroid/graphics/Canvas;
    .end local v24    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    .end local v25    # "dst":Landroid/graphics/Rect;
    :cond_20
    add-int/lit8 v33, v33, 0x1

    goto/16 :goto_1
.end method

.method public getDrawableRemoverImage(FFF)Landroid/graphics/drawable/Drawable;
    .locals 8
    .param p1, "size"    # F
    .param p2, "rx"    # F
    .param p3, "ry"    # F

    .prologue
    const/16 v5, 0xcc

    const/high16 v7, 0x42cc0000    # 102.0f

    .line 359
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    if-nez v4, :cond_0

    .line 360
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v5, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    .line 364
    :cond_0
    mul-float v2, p1, p2

    .line 365
    .local v2, "xr":F
    mul-float v3, p1, p3

    .line 366
    .local v3, "yr":F
    new-instance v1, Landroid/graphics/RectF;

    sub-float v4, v7, v2

    sub-float v5, v7, v3

    add-float v6, v7, v2

    add-float/2addr v7, v3

    invoke-direct {v1, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 368
    .local v1, "ovalRect":Landroid/graphics/RectF;
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 369
    .local v0, "canvas":Landroid/graphics/Canvas;
    const/4 v4, 0x0

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 370
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 372
    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v4, v5, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v4
.end method

.method public getDrawableRemoverImage(I)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1, "size"    # I

    .prologue
    const/16 v2, 0xcc

    const/high16 v3, 0x42cc0000    # 102.0f

    .line 344
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    .line 345
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v2, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    .line 348
    :cond_0
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 349
    .local v0, "canvas":Landroid/graphics/Canvas;
    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 350
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mDensity:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    .line 351
    div-int/lit8 v1, p1, 0x2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 355
    :goto_0
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v1

    .line 353
    :cond_1
    int-to-float v1, p1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;
    .locals 13
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "resId"    # I
    .param p3, "iconWidth"    # I
    .param p4, "iconHeight"    # I

    .prologue
    .line 424
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v12

    .line 425
    .local v12, "stream":Ljava/io/InputStream;
    invoke-static {v12}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 427
    .local v0, "BitmapOrg":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 428
    const/4 v1, 0x0

    .line 440
    :goto_0
    return-object v1

    .line 430
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 431
    .local v3, "width":I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 432
    .local v4, "height":I
    move/from16 v8, p3

    .line 433
    .local v8, "newWidth":I
    move/from16 v7, p4

    .line 435
    .local v7, "newHeight":I
    int-to-float v1, v8

    int-to-float v2, v3

    div-float v11, v1, v2

    .line 436
    .local v11, "scaleWidth":F
    int-to-float v1, v7

    int-to-float v2, v4

    div-float v10, v1, v2

    .line 437
    .local v10, "scaleHeight":F
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 438
    .local v5, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v5, v11, v10}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 439
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 440
    .local v9, "resizedBitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1, p1, v9}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1, "drawableName"    # Ljava/lang/String;

    .prologue
    .line 414
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    const-string v2, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 415
    .local v0, "mDrawableResID":I
    if-nez v0, :cond_0

    .line 416
    const/4 v1, 0x0

    .line 418
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_0
.end method

.method public setPenTooltipImage(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "penName"    # Ljava/lang/String;
    .param p2, "image"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 444
    if-nez p1, :cond_0

    .line 451
    :goto_0
    return-void

    .line 447
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    if-nez v0, :cond_1

    .line 448
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    .line 450
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenImageMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
