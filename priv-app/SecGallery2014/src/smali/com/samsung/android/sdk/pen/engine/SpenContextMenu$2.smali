.class Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;
.super Ljava/lang/Object;
.source "SpenContextMenu.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;-><init>(Landroid/content/Context;Landroid/view/View;Ljava/util/ArrayList;Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    .line 469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    .line 472
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v0, v1, 0xff

    .line 474
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 486
    :cond_0
    :goto_0
    :pswitch_0
    return v3

    .line 476
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-static {v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Z)V

    goto :goto_0

    .line 479
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mDelay:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)I

    move-result v1

    const/16 v2, 0x3e8

    if-le v1, v2, :cond_0

    .line 480
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mDelay:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)I

    move-result v2

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->updateTimer(I)V
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;I)V

    goto :goto_0

    .line 474
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
