.class public Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;
.super Ljava/lang/Object;
.source "SpenControlBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CTouchZone"
.end annotation


# instance fields
.field private final mTempTouchZone:Landroid/graphics/RectF;

.field private mZoneSize:F

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 630
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 631
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    .line 632
    const/high16 v1, 0x40e00000    # 7.0f

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    .line 634
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getType()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 635
    const/high16 v1, 0x41400000    # 12.0f

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    .line 638
    :cond_0
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mControlBaseContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 639
    .local v0, "dm":Landroid/util/DisplayMetrics;
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    invoke-static {v2, v1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    .line 640
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;)F
    .locals 1

    .prologue
    .line 628
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    return v0
.end method


# virtual methods
.method protected checkTouchPosition(Landroid/graphics/RectF;IILcom/samsung/android/sdk/pen/document/SpenObjectBase;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;)V
    .locals 6
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .param p5, "retState"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    .prologue
    const/4 v5, 0x1

    const v2, 0x38d1b717    # 1.0E-4f

    const/4 v4, 0x2

    .line 729
    invoke-virtual {p5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    .line 731
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    .line 754
    :cond_0
    :goto_0
    return-void

    .line 735
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 736
    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->getRect(ILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v1

    int-to-float v2, p2

    int-to-float v3, p3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 737
    if-nez v0, :cond_2

    invoke-virtual {p4}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isRotatable()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 738
    :cond_2
    if-lt v0, v5, :cond_3

    const/16 v1, 0x8

    if-gt v0, v1, :cond_3

    .line 739
    invoke-virtual {p4}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v1

    if-eq v1, v4, :cond_4

    .line 740
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    if-eqz v1, :cond_5

    if-eq v0, v4, :cond_4

    .line 741
    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/4 v1, 0x7

    if-ne v0, v1, :cond_5

    .line 735
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 745
    :cond_5
    iput v0, p5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    .line 746
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)I

    move-result v1

    if-eq v1, v4, :cond_6

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)I

    move-result v1

    if-ne v1, v5, :cond_0

    .line 747
    :cond_6
    iget v1, p5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v2, 0x9

    if-eq v1, v2, :cond_0

    .line 748
    const/4 v1, -0x1

    iput v1, p5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    goto :goto_0
.end method

.method getRect(ILandroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 15
    .param p1, "touchZone"    # I
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 643
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    float-to-int v8, v9

    .line 644
    .local v8, "rect_w":I
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    float-to-int v7, v9

    .line 645
    .local v7, "rect_h":I
    const/4 v4, 0x0

    .line 646
    .local v4, "disableTopBottom":Z
    const/4 v3, 0x0

    .line 648
    .local v3, "disableLeftRight":Z
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v10, 0x3fc00000    # 1.5f

    mul-float v2, v9, v10

    .line 649
    .local v2, "deltaWidth":F
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v10, 0x3fc00000    # 1.5f

    mul-float v1, v9, v10

    .line 650
    .local v1, "deltaHeight":F
    int-to-float v9, v8

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v11, 0x40400000    # 3.0f

    mul-float/2addr v10, v11

    cmpg-float v9, v9, v10

    if-gez v9, :cond_3

    .line 651
    const/4 v4, 0x1

    .line 656
    :cond_0
    :goto_0
    int-to-float v9, v7

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v11, 0x40400000    # 3.0f

    mul-float/2addr v10, v11

    cmpg-float v9, v9, v10

    if-gez v9, :cond_4

    .line 657
    const/4 v3, 0x1

    .line 664
    :cond_1
    :goto_1
    const/high16 v9, 0x40000000    # 2.0f

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    mul-float v5, v9, v10

    .line 666
    .local v5, "outsideGap":F
    packed-switch p1, :pswitch_data_0

    .line 725
    :cond_2
    :goto_2
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    return-object v9

    .line 652
    .end local v5    # "outsideGap":F
    :cond_3
    int-to-float v9, v8

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v11, 0x40c00000    # 6.0f

    mul-float/2addr v10, v11

    cmpg-float v9, v9, v10

    if-gtz v9, :cond_0

    .line 653
    div-int/lit8 v9, v8, 0x4

    int-to-float v2, v9

    goto :goto_0

    .line 658
    :cond_4
    int-to-float v9, v7

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v11, 0x40c00000    # 6.0f

    mul-float/2addr v10, v11

    cmpg-float v9, v9, v10

    if-gtz v9, :cond_1

    .line 659
    div-int/lit8 v9, v7, 0x4

    int-to-float v1, v9

    goto :goto_1

    .line 668
    .restart local v5    # "outsideGap":F
    :pswitch_0
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    move-object/from16 v0, p2

    iget v10, v0, Landroid/graphics/RectF;->left:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    sub-float/2addr v10, v11

    sub-float/2addr v10, v5

    move-object/from16 v0, p2

    iget v11, v0, Landroid/graphics/RectF;->top:F

    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    sub-float/2addr v11, v12

    sub-float/2addr v11, v5

    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/RectF;->left:F

    .line 669
    add-float/2addr v12, v2

    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v13, v1

    .line 668
    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_2

    .line 672
    :pswitch_1
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerX()F

    move-result v10

    sub-float/2addr v10, v2

    move-object/from16 v0, p2

    iget v11, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v11, v5

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerX()F

    move-result v12

    add-float/2addr v12, v2

    .line 673
    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v13, v1

    .line 672
    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 674
    if-eqz v4, :cond_2

    .line 675
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_2

    .line 680
    :pswitch_2
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    move-object/from16 v0, p2

    iget v10, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v10, v2

    move-object/from16 v0, p2

    iget v11, v0, Landroid/graphics/RectF;->top:F

    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    sub-float/2addr v11, v12

    sub-float/2addr v11, v5

    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/RectF;->right:F

    iget v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    add-float/2addr v12, v13

    .line 681
    add-float/2addr v12, v5

    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v13, v1

    .line 680
    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_2

    .line 684
    :pswitch_3
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    move-object/from16 v0, p2

    iget v10, v0, Landroid/graphics/RectF;->left:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    sub-float/2addr v10, v11

    sub-float/2addr v10, v5

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerY()F

    move-result v11

    sub-float/2addr v11, v1

    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/RectF;->left:F

    .line 685
    add-float/2addr v12, v2

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerY()F

    move-result v13

    add-float/2addr v13, v1

    .line 684
    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 686
    if-eqz v3, :cond_2

    .line 687
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_2

    .line 691
    :pswitch_4
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    move-object/from16 v0, p2

    iget v10, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p2

    iget v11, v0, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_2

    .line 694
    :pswitch_5
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    move-object/from16 v0, p2

    iget v10, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v10, v2

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerY()F

    move-result v11

    sub-float/2addr v11, v1

    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/RectF;->right:F

    iget v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    add-float/2addr v12, v13

    .line 695
    add-float/2addr v12, v5

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerY()F

    move-result v13

    add-float/2addr v13, v1

    .line 694
    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 696
    if-eqz v3, :cond_2

    .line 697
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_2

    .line 701
    :pswitch_6
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    move-object/from16 v0, p2

    iget v10, v0, Landroid/graphics/RectF;->left:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    sub-float/2addr v10, v11

    sub-float/2addr v10, v5

    move-object/from16 v0, p2

    iget v11, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v11, v1

    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/RectF;->left:F

    .line 702
    add-float/2addr v12, v2

    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/RectF;->bottom:F

    iget v14, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    add-float/2addr v13, v14

    add-float/2addr v13, v5

    .line 701
    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_2

    .line 705
    :pswitch_7
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerX()F

    move-result v10

    sub-float/2addr v10, v2

    move-object/from16 v0, p2

    iget v11, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v11, v1

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerX()F

    move-result v12

    add-float/2addr v12, v2

    .line 706
    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/RectF;->bottom:F

    iget v14, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    add-float/2addr v13, v14

    add-float/2addr v13, v5

    .line 705
    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 707
    if-eqz v4, :cond_2

    .line 708
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_2

    .line 713
    :pswitch_8
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    move-object/from16 v0, p2

    iget v10, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v10, v2

    move-object/from16 v0, p2

    iget v11, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v11, v1

    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/RectF;->right:F

    iget v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    add-float/2addr v12, v13

    .line 714
    add-float/2addr v12, v5

    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/RectF;->bottom:F

    iget v14, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    add-float/2addr v13, v14

    add-float/2addr v13, v5

    .line 713
    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_2

    .line 717
    :pswitch_9
    const/high16 v6, 0x3fc00000    # 1.5f

    .line 718
    .local v6, "ratio":F
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    move-object/from16 v0, p2

    iget v10, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p2

    iget v11, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    add-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    mul-float/2addr v11, v6

    sub-float/2addr v10, v11

    move-object/from16 v0, p2

    iget v11, v0, Landroid/graphics/RectF;->top:F

    .line 719
    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    const/high16 v13, 0x40800000    # 4.0f

    mul-float/2addr v12, v13

    mul-float/2addr v12, v6

    sub-float/2addr v11, v12

    const/high16 v12, 0x41f00000    # 30.0f

    sub-float/2addr v11, v12

    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    add-float/2addr v11, v12

    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p2

    iget v14, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    add-float/2addr v12, v13

    .line 720
    iget v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    mul-float/2addr v13, v6

    add-float/2addr v12, v13

    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/RectF;->top:F

    iget v14, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    mul-float/2addr v14, v6

    sub-float/2addr v13, v14

    const/high16 v14, 0x41f00000    # 30.0f

    sub-float/2addr v13, v14

    iget v14, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F

    add-float/2addr v13, v14

    .line 718
    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_2

    .line 666
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_4
    .end packed-switch
.end method
