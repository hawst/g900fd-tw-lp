.class final enum Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;
.super Ljava/lang/Enum;
.source "SpenSettingTextLayout2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "scrollBar"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;

.field public static final enum SCROLL_NORMAL:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;

.field public static final enum SCROLL_PRESS:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2623
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;

    const-string v1, "SCROLL_NORMAL"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;->SCROLL_NORMAL:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;

    .line 2624
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;

    const-string v1, "SCROLL_PRESS"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;->SCROLL_PRESS:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;

    .line 2622
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;->SCROLL_NORMAL:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;->SCROLL_PRESS:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;->ENUM$VALUES:[Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2622
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;->ENUM$VALUES:[Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$scrollBar;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
