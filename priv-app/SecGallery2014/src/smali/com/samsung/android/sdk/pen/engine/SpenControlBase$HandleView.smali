.class Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;
.super Landroid/view/View;
.source "SpenControlBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "HandleView"
.end annotation


# instance fields
.field private centerX:F

.field private centerY:F

.field private isDrawRotationText:Z

.field private mAngle:F

.field private mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

.field private final mDensity:F

.field private mPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

.field private final mRectHandle:Landroid/graphics/Rect;

.field private final mRectRotation:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 4812
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 4814
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 4815
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mDensity:F

    .line 4817
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mAngle:F

    .line 4818
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->centerX:F

    .line 4819
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->centerY:F

    .line 4821
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mRectRotation:Landroid/graphics/RectF;

    .line 4822
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mRectHandle:Landroid/graphics/Rect;

    .line 4823
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->isDrawRotationText:Z

    .line 4824
    return-void
.end method

.method private convertToArabicNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 4863
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4864
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 4871
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 4865
    :cond_0
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4866
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->arabicChars:[C
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->access$2()[C

    move-result-object v2

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    add-int/lit8 v3, v3, -0x30

    aget-char v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 4864
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4868
    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v9, 0x42700000    # 60.0f

    const/high16 v12, 0x41f00000    # 30.0f

    const/high16 v11, 0x40800000    # 4.0f

    const/high16 v10, 0x40000000    # 2.0f

    .line 4832
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mRectHandle:Landroid/graphics/Rect;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-nez v4, :cond_1

    .line 4860
    :cond_0
    :goto_0
    return-void

    .line 4836
    :cond_1
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->isDrawRotationText:Z

    if-eqz v4, :cond_4

    .line 4837
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mRectRotation:Landroid/graphics/RectF;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->centerX:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mDensity:F

    mul-float/2addr v6, v9

    div-float/2addr v6, v10

    sub-float/2addr v5, v6

    .line 4838
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->centerY:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mDensity:F

    mul-float/2addr v7, v12

    div-float/2addr v7, v10

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->centerX:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mDensity:F

    mul-float/2addr v8, v9

    div-float/2addr v8, v10

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->centerY:F

    .line 4839
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mDensity:F

    mul-float/2addr v9, v12

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    .line 4837
    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 4841
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v2

    .line 4842
    .local v2, "paintRectRotation":Landroid/graphics/Paint;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mRectRotation:Landroid/graphics/RectF;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mDensity:F

    mul-float/2addr v5, v11

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mDensity:F

    mul-float/2addr v6, v11

    invoke-virtual {p1, v4, v5, v6, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 4843
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mAngle:F

    float-to-int v0, v4

    .line 4844
    .local v0, "displayAngle":I
    if-gez v0, :cond_2

    .line 4845
    add-int/lit16 v0, v0, 0x168

    .line 4847
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4848
    .local v3, "strAngle":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ar"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 4849
    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->convertToArabicNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 4851
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v1

    .line 4852
    .local v1, "mPaintRotationText":Landroid/graphics/Paint;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, "\u00b0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->centerX:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->centerY:F

    .line 4853
    invoke-virtual {v1}, Landroid/graphics/Paint;->descent()F

    move-result v7

    invoke-virtual {v1}, Landroid/graphics/Paint;->ascent()F

    move-result v8

    add-float/2addr v7, v8

    div-float/2addr v7, v10

    sub-float/2addr v6, v7

    .line 4852
    invoke-virtual {p1, v4, v5, v6, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 4856
    .end local v0    # "displayAngle":I
    .end local v1    # "mPaintRotationText":Landroid/graphics/Paint;
    .end local v2    # "paintRectRotation":Landroid/graphics/Paint;
    .end local v3    # "strAngle":Ljava/lang/String;
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 4857
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mAngle:F

    float-to-int v4, v4

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->centerX:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->centerY:F

    invoke-virtual {p1, v4, v5, v6}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 4858
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mRectHandle:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v4

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v5, p1, v6, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onDrawObject(Landroid/graphics/Canvas;Landroid/graphics/Rect;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 4859
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0
.end method

.method public setAngle(F)V
    .locals 0
    .param p1, "angle"    # F

    .prologue
    .line 4888
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mAngle:F

    .line 4889
    return-void
.end method

.method public setCenter(FF)V
    .locals 0
    .param p1, "centerX"    # F
    .param p2, "centerY"    # F

    .prologue
    .line 4879
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->centerX:F

    .line 4880
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->centerY:F

    .line 4881
    return-void
.end method

.method protected setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V
    .locals 0
    .param p1, "control"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    .prologue
    .line 4827
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    .line 4828
    return-void
.end method

.method public setDrawRotation(Z)V
    .locals 0
    .param p1, "isDraw"    # Z

    .prologue
    .line 4875
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->isDrawRotationText:Z

    .line 4876
    return-void
.end method

.method public setPaint(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;)V
    .locals 0
    .param p1, "paint"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    .prologue
    .line 4892
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    .line 4893
    return-void
.end method

.method public setRect(Landroid/graphics/RectF;)V
    .locals 5
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 4884
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->mRectHandle:Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget v2, p1, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    iget v3, p1, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    float-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 4885
    return-void
.end method
