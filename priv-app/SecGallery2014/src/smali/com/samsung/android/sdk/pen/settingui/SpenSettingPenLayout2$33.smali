.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;
.super Ljava/lang/Object;
.source "SpenSettingPenLayout2.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    .line 1650
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1654
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 1655
    const/4 v0, 0x0

    .line 1656
    .local v0, "beautifyStyleIndex":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1657
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/widget/ImageButton;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1672
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCurrentBeautifyStyle:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$32(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)I

    move-result v4

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->resetBeautifyAdvanceDataAndUpdateSeekBarUi(I)V
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$34(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;I)V

    .line 1674
    .end local v0    # "beautifyStyleIndex":I
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/widget/ImageButton;>;"
    :cond_1
    return-void

    .line 1658
    .restart local v0    # "beautifyStyleIndex":I
    .restart local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/widget/ImageButton;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 1659
    .local v2, "styleBtn":Landroid/widget/ImageButton;
    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1660
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCurrentBeautifyStyle:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$32(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)I

    move-result v3

    if-eq v0, v3, :cond_0

    .line 1663
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-static {v3, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$33(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;I)V

    .line 1664
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1669
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1667
    :cond_3
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_1
.end method
