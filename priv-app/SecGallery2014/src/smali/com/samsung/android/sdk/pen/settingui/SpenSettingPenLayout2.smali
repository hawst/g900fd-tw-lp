.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
.super Landroid/widget/LinearLayout;
.source "SpenSettingPenLayout2.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$ActionListener;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PopupListener;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PresetListener;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$RptUpdater;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$scrollBar;
    }
.end annotation


# static fields
.field private static final BEAUTIFY_ADVANCE_CURSIVE_MAX_VALUE:I = 0xc

.field private static final BEAUTIFY_ADVANCE_DEFAULT_MAX_VALUE:I = 0x14

.field private static final BEAUTIFY_ADVANCE_DEFAULT_SETTING_VALUES:[[I

.field private static final BEAUTIFY_ADVANCE_DUMMY_MAX_VALUE:I = 0x14

.field private static final BEAUTIFY_ADVANCE_MODULATION_MAX_VALUE:I = 0x64

.field private static final BEAUTIFY_ADVANCE_SUSTENANCE_MAX_VALUE:I = 0x10

.field private static final BEAUTIFY_MAX_STYLEID_COUNT:I = 0x6

.field private static final BEAUTIFY_PARAMETER_CURSIVE:I = 0x2

.field private static final BEAUTIFY_PARAMETER_DUMMY:I = 0x4

.field private static final BEAUTIFY_PARAMETER_MODULATION:I = 0x6

.field private static final BEAUTIFY_PARAMETER_SLANT:I = 0x9

.field private static final BEAUTIFY_PARAMETER_STYLEID:I = 0x0

.field private static final BEAUTIFY_PARAMETER_SUSTENANCE:I = 0x3

.field private static final BEAUTIFY_PEN_NAME:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Beautify"

.field protected static final BEAUTIFY_RESET_BUTTON_HEIGHT:I = 0x28

.field private static final BEAUTIFY_STYLEID_CURSIVE_LM:I = 0xb

.field private static final BEAUTIFY_STYLEID_HUAI:I = 0xc

.field private static final BEAUTIFY_STYLEID_HUANG:I = 0x5

.field private static final BEAUTIFY_STYLEID_HUI:I = 0x6

.field private static final BEAUTIFY_STYLEID_RUNNING_HAND_S:I = 0x1

.field private static final BEAUTIFY_STYLEID_WANG:I = 0x3

.field protected static final BODY_LAYOUT_HEIGHT:I = 0x16e

.field protected static final BODY_LAYOUT_HEIGHT_MAGIC_PEN:I = 0x10b

.field protected static final BODY_LAYOUT_HEIGHT_WITH_ALPHA:I = 0x1ac

.field protected static final BODY_LAYOUT_WIDTH_BEAUTIFY_PEN:I = 0x1f8

.field protected static final BOTTOM_LAYOUT_HEIGHT:I = 0x1

.field private static final CHINESE_PEN_NAME:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

.field protected static final EXIT_BUTTON_RAW_HEIGHT:I = 0x2a

.field protected static final EXIT_BUTTON_RAW_WIDTH:I = 0x2a

.field protected static final LINE_BUTTON_RAW_HEIGHT:I = 0x11

.field protected static final LINE_BUTTON_RAW_WIDTH:I = 0x1

.field private static final MAGIC_PEN_NAME:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.MagicPen"

.field protected static final MAXIMUM_PRESET_NUMBER:I = 0x24

.field private static final MAX_HEIGHT_FLAG:I = 0x1869f

.field private static final MAX_PARAMETER_INDEX:I = 0xa

.field protected static final PEN_SIZE_MAX:I = 0x63

.field private static final REP_DELAY:I = 0x14

.field protected static final SEEKBAR_LAYOUT_HEIGHT:I = 0x3e

.field protected static final SUITABLE_WIDTH_IN_ANIMATION_SCROLL:F = 55.0f

.field protected static final TITLE_LAYOUT_HEIGHT:I = 0x29

.field protected static final TOTAL_LAYOUT_WIDTH:I = 0x149

.field protected static final TYPE_SELECTOR_LAYOUT_HEIGHT:I = 0x2a

.field public static final VIEW_MODE_COLOR:I = 0x6

.field public static final VIEW_MODE_EXTENSION:I = 0x2

.field public static final VIEW_MODE_EXTENSION_WITHOUT_PRESET:I = 0x3

.field public static final VIEW_MODE_EXTENSION_WITHOUT_PRESET_NO_RESIZE:I = 0x9

.field public static final VIEW_MODE_MINIMUM:I = 0x1

.field public static final VIEW_MODE_NORMAL:I = 0x0

.field public static final VIEW_MODE_PRESET:I = 0x7

.field public static final VIEW_MODE_SIZE:I = 0x5

.field public static final VIEW_MODE_TITLE:I = 0x8

.field public static final VIEW_MODE_TYPE:I = 0x4

.field private static final bottomExpandPath:Ljava/lang/String;

.field private static final bottomHandlePath:Ljava/lang/String;

.field private static final grayBodyLeftPath:Ljava/lang/String;

.field private static final handelFocusPath:Ljava/lang/String;

.field private static final handelPath:Ljava/lang/String;

.field private static final handelPressPath:Ljava/lang/String;

.field private static isHighlightPenRemoved:Z

.field private static isMagicPenRemoved:Z

.field private static final lefBgFocuslPath:Ljava/lang/String;

.field private static final lefBgPresslPath:Ljava/lang/String;

.field private static final leftBgPath:Ljava/lang/String;

.field private static final lightBodyLeftPath:Ljava/lang/String;

.field private static final lineDivider:Ljava/lang/String;

.field private static final linePath:Ljava/lang/String;

.field protected static mColorPickerShow:Z

.field protected static mDefaultPath:Ljava/lang/String;

.field private static final mImagePath_snote_add:Ljava/lang/String;

.field private static final mImagePath_snote_add_dim:Ljava/lang/String;

.field private static final mImagePath_snote_add_press:Ljava/lang/String;

.field protected static mPresetInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;",
            ">;"
        }
    .end annotation
.end field

.field protected static final mPreviewBgPath:Ljava/lang/String;

.field protected static mScale:F

.field private static final mSdkVersion:I

.field private static final minusBgFocusPath:Ljava/lang/String;

.field private static final minusBgPath:Ljava/lang/String;

.field private static final minusBgPressPath:Ljava/lang/String;

.field private static final plusBgFocusPath:Ljava/lang/String;

.field private static final plusBgPath:Ljava/lang/String;

.field private static final plusBgPressPath:Ljava/lang/String;

.field private static final popupBtnBgFocusPath:Ljava/lang/String;

.field private static final popupBtnBgNomalPath:Ljava/lang/String;

.field private static final popupBtnBgPressPath:Ljava/lang/String;

.field private static final popupMaxPath:Ljava/lang/String;

.field private static final popupMinPath:Ljava/lang/String;

.field private static final presetAddFocusPath:Ljava/lang/String;

.field private static final presetAddPath:Ljava/lang/String;

.field private static final presetAddPressPath:Ljava/lang/String;

.field private static final previewAlphaPath:Ljava/lang/String;

.field private static final progressAlphaPath:Ljava/lang/String;

.field private static final progressBgPath:Ljava/lang/String;

.field private static final progressShadowPath:Ljava/lang/String;

.field private static final rightBgFocuslPath:Ljava/lang/String;

.field private static final rightBgPath:Ljava/lang/String;

.field private static final rightBgPresslPath:Ljava/lang/String;

.field private static final switchCheckFalseBgPath:Ljava/lang/String;

.field private static final switchCheckTrueBgPath:Ljava/lang/String;

.field private static final switchThumbPath:Ljava/lang/String;

.field private static final tabLineFocusPath:Ljava/lang/String;

.field private static final tabLinePath:Ljava/lang/String;

.field private static final tabLineSelectPath:Ljava/lang/String;

.field private static final titleLeftPath:Ljava/lang/String;


# instance fields
.field protected EXIT_BUTTON_RIGHT_MARGIN:F

.field protected EXIT_BUTTON_TOP_MARGIN:F

.field protected EXIT_BUTTON_WIDTH:I

.field protected LINE_BUTTON_WIDTH:F

.field private alphaDrawable:Landroid/graphics/drawable/Drawable;

.field private colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

.field private currenMagicPenHeight:I

.field private currentOrtherPenHeight:I

.field private deltaOfMiniMode:I

.field handler:Landroid/os/Handler;

.field private final horizontalScrollViewTouchListener:Landroid/view/View$OnTouchListener;

.field private isMagicPenEnable:Z

.field private isMinimumMode:Z

.field private isPresetClicked:Z

.field private localPenTypeViewGroup:Landroid/widget/RelativeLayout;

.field private mAdvancedSettingButton:Landroid/view/View;

.field private final mAdvancedSettingButtonListner:Landroid/view/View$OnClickListener;

.field private final mAdvancedSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

.field private mAdvancedSettingShow:Z

.field private final mBeautifyAdvanceCursiveChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field protected mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

.field private final mBeautifyAdvanceCursiveMinusButtonListener:Landroid/view/View$OnClickListener;

.field protected mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

.field private final mBeautifyAdvanceCursivePlusButtonListener:Landroid/view/View$OnClickListener;

.field protected mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

.field private final mBeautifyAdvanceDummyChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field protected mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

.field private final mBeautifyAdvanceDummyMinusButtonListener:Landroid/view/View$OnClickListener;

.field protected mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

.field private final mBeautifyAdvanceDummyPlusButtonListener:Landroid/view/View$OnClickListener;

.field protected mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

.field private final mBeautifyAdvanceModulationChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field protected mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

.field private final mBeautifyAdvanceModulationMinusButtonListener:Landroid/view/View$OnClickListener;

.field protected mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

.field private final mBeautifyAdvanceModulationPlusButtonListener:Landroid/view/View$OnClickListener;

.field protected mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

.field protected mBeautifyAdvanceResetButton:Landroid/widget/Button;

.field private final mBeautifyAdvanceResetButtonListener:Landroid/view/View$OnClickListener;

.field protected mBeautifyAdvanceSettingLayout:Landroid/view/View;

.field private final mBeautifyAdvanceSustenanceChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field protected mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

.field private final mBeautifyAdvanceSustenanceMinusButtonListener:Landroid/view/View$OnClickListener;

.field protected mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

.field private final mBeautifyAdvanceSustenancePlusButtonListener:Landroid/view/View$OnClickListener;

.field protected mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

.field protected mBeautifyCursiveTextView:Landroid/widget/TextView;

.field protected mBeautifyDummyTextView:Landroid/widget/TextView;

.field protected mBeautifyEnableLayout:Landroid/view/View;

.field protected mBeautifyEnableSwitchView:Landroid/widget/Switch;

.field protected mBeautifyEnableTextView:Landroid/widget/TextView;

.field private final mBeautifyEnablecheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field protected mBeautifyModulationTextView:Landroid/widget/TextView;

.field protected mBeautifySeekbarColor:Landroid/graphics/drawable/GradientDrawable;

.field protected mBeautifyStr:Ljava/lang/String;

.field protected mBeautifyStyleBtnViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageButton;",
            ">;"
        }
    .end annotation
.end field

.field protected mBeautifyStyleBtnsLayout:Landroid/view/View;

.field private final mBeautifyStyleBtnsListener:Landroid/view/View$OnClickListener;

.field protected mBeautifySustenanceTextView:Landroid/widget/TextView;

.field protected mBodyBg:Landroid/view/View;

.field protected mBodyLayout:Landroid/widget/RelativeLayout;

.field protected mBodyLayout2:Landroid/widget/RelativeLayout;

.field protected mBodyLayoutHeight:I

.field protected mBottomExtendBg:Landroid/widget/ImageView;

.field protected mBottomHandle:Landroid/widget/ImageView;

.field protected mBottomLayout:Landroid/view/View;

.field protected mCanvasLayout:Landroid/widget/RelativeLayout;

.field protected mCanvasSize:I

.field protected mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

.field protected mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

.field protected mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

.field private final mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;

.field protected mColorPickerColorImage:Landroid/view/View;

.field protected mColorPickerCurrentColor:Landroid/view/View;

.field private final mColorPickerCurrentColorListener:Landroid/view/View$OnClickListener;

.field protected mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

.field protected mColorPickerSettingExitButton:Landroid/view/View;

.field protected mColorSelectPickerLayout:Landroid/view/View;

.field private mCount:I

.field private mCountForScrollPen:I

.field private final mCurrentBeautifyAdvanceSettingValues:[[I

.field private mCurrentBeautifyStyle:I

.field protected mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private final mExitButtonListener:Landroid/view/View$OnClickListener;

.field protected mExpandFlag:Z

.field mExpendBarListener:Landroid/view/View$OnTouchListener;

.field protected mFirstLongPress:Z

.field protected mFirstTimeColorPickerShow:Z

.field private final mHandler:Landroid/os/Handler;

.field private final mHandlerForScrollPen:Landroid/os/Handler;

.field private mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

.field protected mLeftMargin:I

.field protected mLine1Button:Landroid/view/View;

.field protected mLine2Button:Landroid/view/View;

.field protected mMovableRect:Landroid/graphics/Rect;

.field mNumberOfPenExist:I

.field private final mOnClickPresetItemListener:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;

.field private final mOnColorChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

.field private final mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

.field private final mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

.field protected mOpacityMinusButton:Landroid/view/View;

.field protected mOpacityPlusButton:Landroid/view/View;

.field private mOrientation:I

.field private final mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

.field protected mPaletteBg:Landroid/view/View;

.field protected mPaletteLeftButton:Landroid/view/View;

.field private final mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

.field protected mPaletteRightButton:Landroid/view/View;

.field protected mPaletteView:Landroid/view/View;

.field protected mPenAlpha:I

.field private mPenAlphaAutoDecrement:Z

.field private mPenAlphaAutoIncrement:Z

.field private final mPenAlphaChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private final mPenAlphaKeyListener:Landroid/view/View$OnKeyListener;

.field private final mPenAlphaMinusButtonListener:Landroid/view/View$OnClickListener;

.field private final mPenAlphaMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mPenAlphaMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

.field private final mPenAlphaPlusButtonListener:Landroid/view/View$OnClickListener;

.field private final mPenAlphaPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mPenAlphaPlusButtonTouchListener:Landroid/view/View$OnTouchListener;

.field protected mPenAlphaPreview:Landroid/widget/RelativeLayout;

.field protected mPenAlphaSeekbar:Landroid/widget/SeekBar;

.field protected mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

.field protected mPenAlphaSeekbarView:Landroid/view/View;

.field protected mPenAlphaTextView:Landroid/widget/TextView;

.field protected mPenContext:Landroid/content/Context;

.field protected mPenDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;",
            ">;"
        }
    .end annotation
.end field

.field protected mPenLayout:Landroid/view/View;

.field protected mPenNameIndex:I

.field protected mPenPluginCount:I

.field protected mPenPluginInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;",
            ">;"
        }
    .end annotation
.end field

.field protected mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

.field protected mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

.field protected mPenSeekbarLayout:Landroid/view/View;

.field private mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

.field private mPenSizeAutoDecrement:Z

.field private mPenSizeAutoIncrement:Z

.field private final mPenSizeChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private final mPenSizeKeyListener:Landroid/view/View$OnKeyListener;

.field protected mPenSizeMinusButton:Landroid/view/View;

.field private final mPenSizeMinusButtonListener:Landroid/view/View$OnClickListener;

.field private final mPenSizeMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mPenSizeMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

.field protected mPenSizePlusButton:Landroid/view/View;

.field private final mPenSizePlusButtonListener:Landroid/view/View$OnClickListener;

.field private final mPenSizePlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mPenSizePlusButtonTouchListener:Landroid/view/View$OnTouchListener;

.field protected mPenSizeSeekbar:Landroid/widget/SeekBar;

.field protected mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

.field protected mPenSizeSeekbarView:Landroid/view/View;

.field protected mPenSizeTextView:Landroid/widget/TextView;

.field protected mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

.field protected mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

.field protected mPenTypeHorizontalScrollView3:Landroid/view/ViewGroup;

.field private final mPenTypeHoverListener:Landroid/view/View$OnHoverListener;

.field protected mPenTypeLayout:Landroid/view/ViewGroup;

.field private final mPenTypeListner:Landroid/view/View$OnClickListener;

.field private final mPenTypeTouchListener:Landroid/view/View$OnTouchListener;

.field protected mPenTypeView:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field protected mPickerView:Landroid/view/View;

.field protected mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

.field private final mPopupButtonListener:Landroid/view/View$OnClickListener;

.field private mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PopupListener;

.field protected mPopupMaxButton:Landroid/view/View;

.field protected mPopupMinButton:Landroid/view/View;

.field protected mPreCanvasFingerAction:I

.field protected mPreCanvasPenAction:I

.field private final mPreSetAddButtonListner:Landroid/view/View$OnClickListener;

.field protected mPresetAddButton:Landroid/view/View;

.field protected mPresetDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;",
            ">;"
        }
    .end annotation
.end field

.field protected mPresetGridView:Landroid/widget/GridView;

.field protected mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

.field protected mPresetListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PresetListener;

.field protected mPresetTextView:Landroid/widget/TextView;

.field protected mPreviewLayout:Landroid/view/View;

.field protected mPreviousSelectedPresetIndex:I

.field protected mScrollAxis:I

.field private mScrollTimer:Ljava/util/Timer;

.field private mScrollTimerForScrollPen:Ljava/util/Timer;

.field protected mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

.field private final mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

.field protected mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

.field protected mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

.field private mSupportBeautifyPen:Z

.field protected mSustenanceStr:Ljava/lang/String;

.field protected mTitleLayout:Landroid/view/View;

.field protected mTopMargin:I

.field protected mTotalLeftMargin:I

.field protected mTotalTopMargin:I

.field protected mTypeSelectorLayout:Landroid/widget/LinearLayout;

.field protected mViewMode:I

.field protected mWindowHeight:I

.field private penTypeLayout:Landroid/widget/RelativeLayout;

.field private previousPenMagicSelected:Z

.field private final repeatUpdateHandler:Landroid/os/Handler;

.field private titleTextView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x5

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 152
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    .line 158
    const-string v0, ""

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    .line 304
    sput-boolean v9, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isHighlightPenRemoved:Z

    .line 305
    sput-boolean v9, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMagicPenRemoved:Z

    .line 327
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSdkVersion:I

    .line 627
    sput-boolean v9, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerShow:Z

    .line 6906
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "vienna_popup_title_bg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->titleLeftPath:Ljava/lang/String;

    .line 6907
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_arrow_max_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->popupMaxPath:Ljava/lang/String;

    .line 6908
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_arrow_min_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->popupMinPath:Ljava/lang/String;

    .line 6910
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_add"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->presetAddPath:Ljava/lang/String;

    .line 6911
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_add_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->presetAddFocusPath:Ljava/lang/String;

    .line 6912
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_add_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->presetAddPressPath:Ljava/lang/String;

    .line 6913
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_divider"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->linePath:Ljava/lang/String;

    .line 6916
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_line"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->lineDivider:Ljava/lang/String;

    .line 6918
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "vienna_popup_bg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->lightBodyLeftPath:Ljava/lang/String;

    .line 6919
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "vienna_popup_bg02"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->grayBodyLeftPath:Ljava/lang/String;

    .line 6920
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_page_num_bg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->tabLinePath:Ljava/lang/String;

    .line 6921
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_page_num_bg_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->tabLineSelectPath:Ljava/lang/String;

    .line 6922
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_page_num_bg_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->tabLineFocusPath:Ljava/lang/String;

    .line 6925
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_preview_bg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviewBgPath:Ljava/lang/String;

    .line 6927
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_arrow_left_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->leftBgPath:Ljava/lang/String;

    .line 6928
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_arrow_left_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->lefBgPresslPath:Ljava/lang/String;

    .line 6929
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_arrow_left_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->lefBgFocuslPath:Ljava/lang/String;

    .line 6930
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_arrow_right_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->rightBgPath:Ljava/lang/String;

    .line 6931
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_arrow_right_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->rightBgPresslPath:Ljava/lang/String;

    .line 6932
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_arrow_right_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->rightBgFocuslPath:Ljava/lang/String;

    .line 6933
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_plus_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgPath:Ljava/lang/String;

    .line 6934
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_plus_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgPressPath:Ljava/lang/String;

    .line 6935
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_plus_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgFocusPath:Ljava/lang/String;

    .line 6936
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_minus_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgPath:Ljava/lang/String;

    .line 6937
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_minus_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgPressPath:Ljava/lang/String;

    .line 6938
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_minus_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgFocusPath:Ljava/lang/String;

    .line 6939
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "progress_handle_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->handelPath:Ljava/lang/String;

    .line 6940
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "progress_handle_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->handelPressPath:Ljava/lang/String;

    .line 6941
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "progress_handle_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->handelFocusPath:Ljava/lang/String;

    .line 6943
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "progress_bg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->progressBgPath:Ljava/lang/String;

    .line 6944
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "progress_shadow"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->progressShadowPath:Ljava/lang/String;

    .line 6945
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "progress_bg_alpha"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->progressAlphaPath:Ljava/lang/String;

    .line 6946
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_pensetting_preview_alpha"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->previewAlphaPath:Ljava/lang/String;

    .line 6947
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "beautify_switch_off"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->switchCheckFalseBgPath:Ljava/lang/String;

    .line 6948
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "beautify_switch_on"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->switchCheckTrueBgPath:Ljava/lang/String;

    .line 6949
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "beautify_switch_thumb"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->switchThumbPath:Ljava/lang/String;

    .line 6950
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_btn_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->popupBtnBgNomalPath:Ljava/lang/String;

    .line 6951
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_btn_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->popupBtnBgFocusPath:Ljava/lang/String;

    .line 6952
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_btn_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->popupBtnBgPressPath:Ljava/lang/String;

    .line 6954
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_bg_expand"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->bottomExpandPath:Ljava/lang/String;

    .line 6956
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_handler"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->bottomHandlePath:Ljava/lang/String;

    .line 6966
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_add"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImagePath_snote_add:Ljava/lang/String;

    .line 6967
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_add_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImagePath_snote_add_press:Ljava/lang/String;

    .line 6968
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_add_dim"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImagePath_snote_add_dim:Ljava/lang/String;

    .line 7004
    const/4 v0, 0x6

    new-array v0, v0, [[I

    .line 7005
    const/16 v1, 0xa

    new-array v1, v1, [I

    const/16 v2, 0xb

    aput v2, v1, v9

    aput v6, v1, v5

    aput v8, v1, v6

    const/16 v2, 0x8

    aput v2, v1, v7

    const/4 v2, 0x4

    aput v5, v1, v2

    const/16 v2, 0xf

    aput v2, v1, v8

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    const/16 v3, 0xb

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v5, v1, v2

    aput-object v1, v0, v9

    .line 7006
    const/16 v1, 0xa

    new-array v1, v1, [I

    const/16 v2, 0xc

    aput v2, v1, v9

    aput v6, v1, v5

    aput v8, v1, v6

    aput v5, v1, v7

    const/4 v2, 0x4

    aput v5, v1, v2

    const/16 v2, 0x12

    aput v2, v1, v8

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    const/16 v3, 0xd

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v5, v1, v2

    aput-object v1, v0, v5

    .line 7007
    const/16 v1, 0xa

    new-array v1, v1, [I

    aput v8, v1, v9

    aput v6, v1, v5

    aput v6, v1, v6

    aput v6, v1, v7

    const/4 v2, 0x4

    aput v5, v1, v2

    const/16 v2, 0x12

    aput v2, v1, v8

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    aput v8, v1, v2

    const/16 v2, 0x9

    aput v5, v1, v2

    aput-object v1, v0, v6

    .line 7008
    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v2, 0x6

    aput v2, v1, v9

    aput v6, v1, v5

    aput v7, v1, v6

    aput v7, v1, v7

    const/4 v2, 0x4

    aput v5, v1, v2

    const/16 v2, 0xc

    aput v2, v1, v8

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    const/4 v3, 0x6

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v5, v1, v2

    aput-object v1, v0, v7

    const/4 v1, 0x4

    .line 7009
    const/16 v2, 0xa

    new-array v2, v2, [I

    aput v5, v2, v9

    aput v6, v2, v5

    aput v6, v2, v6

    const/16 v3, 0x8

    aput v3, v2, v7

    const/4 v3, 0x4

    aput v5, v2, v3

    const/16 v3, 0xf

    aput v3, v2, v8

    const/4 v3, 0x6

    const/16 v4, 0x46

    aput v4, v2, v3

    const/4 v3, 0x7

    aput v7, v2, v3

    const/16 v3, 0x9

    aput v5, v2, v3

    aput-object v2, v0, v1

    .line 7010
    const/16 v1, 0xa

    new-array v1, v1, [I

    aput v7, v1, v9

    aput v5, v1, v5

    aput v7, v1, v6

    aput v8, v1, v7

    const/4 v2, 0x4

    aput v5, v1, v2

    const/16 v2, 0xc

    aput v2, v1, v8

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    aput v5, v1, v2

    const/16 v2, 0x9

    aput v5, v1, v2

    aput-object v1, v0, v8

    .line 7004
    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->BEAUTIFY_ADVANCE_DEFAULT_SETTING_VALUES:[[I

    .line 7010
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "customImagePath"    # Ljava/lang/String;
    .param p3, "relativeLayout"    # Landroid/widget/RelativeLayout;

    .prologue
    const/4 v9, 0x5

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1930
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 142
    iput v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOrientation:I

    .line 143
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .line 148
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PopupListener;

    .line 154
    const/16 v0, 0xff

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlpha:I

    .line 159
    const/16 v0, 0x438

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    .line 236
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mExpandFlag:Z

    .line 249
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    .line 253
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    .line 256
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PresetListener;

    .line 268
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mViewMode:I

    .line 270
    const/4 v0, -0x2

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    .line 271
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mWindowHeight:I

    .line 272
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollAxis:I

    .line 274
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviousSelectedPresetIndex:I

    .line 276
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mFirstLongPress:Z

    .line 292
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mHandler:Landroid/os/Handler;

    .line 293
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCount:I

    .line 295
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    .line 297
    const v0, 0x1869f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->currenMagicPenHeight:I

    .line 298
    const v0, 0x1869f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->currentOrtherPenHeight:I

    .line 299
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->previousPenMagicSelected:Z

    .line 300
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMagicPenEnable:Z

    .line 301
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isPresetClicked:Z

    .line 303
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penTypeLayout:Landroid/widget/RelativeLayout;

    .line 307
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mHandlerForScrollPen:Landroid/os/Handler;

    .line 321
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->repeatUpdateHandler:Landroid/os/Handler;

    .line 322
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeAutoIncrement:Z

    .line 323
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeAutoDecrement:Z

    .line 324
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaAutoIncrement:Z

    .line 325
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaAutoDecrement:Z

    .line 332
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mAdvancedSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    .line 348
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCurrentBeautifyStyle:I

    .line 350
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSupportBeautifyPen:Z

    .line 516
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOnClickPresetItemListener:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;

    .line 628
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mFirstTimeColorPickerShow:Z

    .line 630
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOnColorChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    .line 718
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;

    .line 765
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->handler:Landroid/os/Handler;

    .line 767
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 883
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 1048
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizePlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 1059
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeTouchListener:Landroid/view/View$OnTouchListener;

    .line 1105
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHoverListener:Landroid/view/View$OnHoverListener;

    .line 1156
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizePlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    .line 1169
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 1180
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    .line 1192
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$13;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$13;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 1203
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$14;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$14;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    .line 1215
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$15;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$15;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 1226
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$16;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$16;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    .line 1239
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$17;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$17;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizePlusButtonListener:Landroid/view/View$OnClickListener;

    .line 1269
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$18;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$18;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 1305
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$19;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$19;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPlusButtonListener:Landroid/view/View$OnClickListener;

    .line 1314
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$20;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$20;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 1324
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$21;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$21;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

    .line 1346
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$22;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$22;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

    .line 1371
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$23;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$23;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeListner:Landroid/view/View$OnClickListener;

    .line 1379
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$24;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$24;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerCurrentColorListener:Landroid/view/View$OnClickListener;

    .line 1439
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$25;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$25;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    .line 1454
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$26;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$26;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mExitButtonListener:Landroid/view/View$OnClickListener;

    .line 1498
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$27;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$27;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    .line 1508
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$28;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$28;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    .line 1519
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$29;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$29;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mAdvancedSettingButtonListner:Landroid/view/View$OnClickListener;

    .line 1537
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$30;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$30;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreSetAddButtonListner:Landroid/view/View$OnClickListener;

    .line 1620
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$31;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$31;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mExpendBarListener:Landroid/view/View$OnTouchListener;

    .line 1629
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$32;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$32;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnablecheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 1650
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnsListener:Landroid/view/View$OnClickListener;

    .line 1677
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$34;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$34;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 1699
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$35;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$35;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 1722
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$36;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$36;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 1744
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$37;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$37;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 1766
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$38;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$38;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursivePlusButtonListener:Landroid/view/View$OnClickListener;

    .line 1776
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$39;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$39;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 1785
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$40;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$40;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenancePlusButtonListener:Landroid/view/View$OnClickListener;

    .line 1793
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$41;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$41;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 1802
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$42;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$42;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyPlusButtonListener:Landroid/view/View$OnClickListener;

    .line 1811
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$43;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$43;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 1820
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$44;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$44;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationPlusButtonListener:Landroid/view/View$OnClickListener;

    .line 1829
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$45;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$45;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 1838
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$46;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$46;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceResetButtonListener:Landroid/view/View$OnClickListener;

    .line 1846
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$47;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$47;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeKeyListener:Landroid/view/View$OnKeyListener;

    .line 1877
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$48;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$48;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaKeyListener:Landroid/view/View$OnKeyListener;

    .line 3993
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$49;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$49;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->horizontalScrollViewTouchListener:Landroid/view/View$OnTouchListener;

    .line 5457
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->deltaOfMiniMode:I

    .line 5458
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMinimumMode:Z

    .line 6545
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mNumberOfPenExist:I

    .line 6871
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$50;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$50;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

    .line 6961
    const-string v0, "Beautify"

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStr:Ljava/lang/String;

    .line 6963
    const-string v0, "Sustenance"

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSustenanceStr:Ljava/lang/String;

    .line 7012
    const/4 v0, 0x6

    new-array v0, v0, [[I

    .line 7013
    const/16 v1, 0xa

    new-array v1, v1, [I

    const/16 v2, 0xb

    aput v2, v1, v5

    aput v7, v1, v6

    aput v9, v1, v7

    const/16 v2, 0x8

    aput v2, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0xf

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    const/16 v3, 0xb

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v5

    .line 7014
    const/16 v1, 0xa

    new-array v1, v1, [I

    const/16 v2, 0xc

    aput v2, v1, v5

    aput v7, v1, v6

    aput v9, v1, v7

    aput v6, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0x12

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    const/16 v3, 0xd

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v6

    .line 7015
    const/16 v1, 0xa

    new-array v1, v1, [I

    aput v9, v1, v5

    aput v7, v1, v6

    aput v7, v1, v7

    aput v7, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0x12

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    aput v9, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v7

    .line 7016
    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v2, 0x6

    aput v2, v1, v5

    aput v7, v1, v6

    aput v8, v1, v7

    aput v8, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0xc

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    const/4 v3, 0x6

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v8

    const/4 v1, 0x4

    .line 7017
    const/16 v2, 0xa

    new-array v2, v2, [I

    aput v6, v2, v5

    aput v7, v2, v6

    aput v7, v2, v7

    const/16 v3, 0x8

    aput v3, v2, v8

    const/4 v3, 0x4

    aput v6, v2, v3

    const/16 v3, 0xf

    aput v3, v2, v9

    const/4 v3, 0x6

    const/16 v4, 0x46

    aput v4, v2, v3

    const/4 v3, 0x7

    aput v8, v2, v3

    const/16 v3, 0x9

    aput v6, v2, v3

    aput-object v2, v0, v1

    .line 7018
    const/16 v1, 0xa

    new-array v1, v1, [I

    aput v8, v1, v5

    aput v6, v1, v6

    aput v8, v1, v7

    aput v9, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0xc

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    aput v6, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v9

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCurrentBeautifyAdvanceSettingValues:[[I

    .line 1932
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1933
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 1934
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 1936
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    .line 1937
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 1939
    const/4 v0, -0x2

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    .line 1941
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    .line 1943
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    if-eqz v0, :cond_0

    .line 1944
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->initPenPlugin(Landroid/content/Context;)V

    .line 1948
    :cond_0
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSupportBeautifyPen:Z

    .line 1950
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 1952
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->initButtonValue()V

    .line 1953
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->initView(Ljava/lang/String;)V

    .line 1954
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setListener()V

    .line 1956
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    .line 1957
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "customImagePath"    # Ljava/lang/String;
    .param p3, "relativeLayout"    # Landroid/widget/RelativeLayout;
    .param p4, "ratio"    # F

    .prologue
    const/4 v9, 0x5

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1984
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 142
    iput v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOrientation:I

    .line 143
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .line 148
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PopupListener;

    .line 154
    const/16 v0, 0xff

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlpha:I

    .line 159
    const/16 v0, 0x438

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    .line 236
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mExpandFlag:Z

    .line 249
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    .line 253
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    .line 256
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PresetListener;

    .line 268
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mViewMode:I

    .line 270
    const/4 v0, -0x2

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    .line 271
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mWindowHeight:I

    .line 272
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollAxis:I

    .line 274
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviousSelectedPresetIndex:I

    .line 276
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mFirstLongPress:Z

    .line 292
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mHandler:Landroid/os/Handler;

    .line 293
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCount:I

    .line 295
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    .line 297
    const v0, 0x1869f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->currenMagicPenHeight:I

    .line 298
    const v0, 0x1869f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->currentOrtherPenHeight:I

    .line 299
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->previousPenMagicSelected:Z

    .line 300
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMagicPenEnable:Z

    .line 301
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isPresetClicked:Z

    .line 303
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penTypeLayout:Landroid/widget/RelativeLayout;

    .line 307
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mHandlerForScrollPen:Landroid/os/Handler;

    .line 321
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->repeatUpdateHandler:Landroid/os/Handler;

    .line 322
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeAutoIncrement:Z

    .line 323
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeAutoDecrement:Z

    .line 324
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaAutoIncrement:Z

    .line 325
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaAutoDecrement:Z

    .line 332
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mAdvancedSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    .line 348
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCurrentBeautifyStyle:I

    .line 350
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSupportBeautifyPen:Z

    .line 516
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOnClickPresetItemListener:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;

    .line 628
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mFirstTimeColorPickerShow:Z

    .line 630
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOnColorChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    .line 718
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;

    .line 765
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->handler:Landroid/os/Handler;

    .line 767
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 883
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 1048
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizePlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 1059
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeTouchListener:Landroid/view/View$OnTouchListener;

    .line 1105
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHoverListener:Landroid/view/View$OnHoverListener;

    .line 1156
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizePlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    .line 1169
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 1180
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    .line 1192
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$13;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$13;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 1203
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$14;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$14;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    .line 1215
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$15;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$15;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 1226
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$16;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$16;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    .line 1239
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$17;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$17;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizePlusButtonListener:Landroid/view/View$OnClickListener;

    .line 1269
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$18;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$18;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 1305
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$19;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$19;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPlusButtonListener:Landroid/view/View$OnClickListener;

    .line 1314
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$20;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$20;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 1324
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$21;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$21;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

    .line 1346
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$22;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$22;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

    .line 1371
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$23;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$23;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeListner:Landroid/view/View$OnClickListener;

    .line 1379
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$24;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$24;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerCurrentColorListener:Landroid/view/View$OnClickListener;

    .line 1439
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$25;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$25;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    .line 1454
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$26;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$26;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mExitButtonListener:Landroid/view/View$OnClickListener;

    .line 1498
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$27;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$27;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    .line 1508
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$28;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$28;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    .line 1519
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$29;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$29;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mAdvancedSettingButtonListner:Landroid/view/View$OnClickListener;

    .line 1537
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$30;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$30;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreSetAddButtonListner:Landroid/view/View$OnClickListener;

    .line 1620
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$31;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$31;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mExpendBarListener:Landroid/view/View$OnTouchListener;

    .line 1629
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$32;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$32;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnablecheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 1650
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnsListener:Landroid/view/View$OnClickListener;

    .line 1677
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$34;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$34;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 1699
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$35;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$35;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 1722
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$36;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$36;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 1744
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$37;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$37;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 1766
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$38;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$38;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursivePlusButtonListener:Landroid/view/View$OnClickListener;

    .line 1776
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$39;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$39;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 1785
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$40;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$40;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenancePlusButtonListener:Landroid/view/View$OnClickListener;

    .line 1793
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$41;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$41;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 1802
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$42;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$42;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyPlusButtonListener:Landroid/view/View$OnClickListener;

    .line 1811
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$43;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$43;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 1820
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$44;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$44;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationPlusButtonListener:Landroid/view/View$OnClickListener;

    .line 1829
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$45;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$45;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 1838
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$46;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$46;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceResetButtonListener:Landroid/view/View$OnClickListener;

    .line 1846
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$47;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$47;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeKeyListener:Landroid/view/View$OnKeyListener;

    .line 1877
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$48;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$48;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaKeyListener:Landroid/view/View$OnKeyListener;

    .line 3993
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$49;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$49;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->horizontalScrollViewTouchListener:Landroid/view/View$OnTouchListener;

    .line 5457
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->deltaOfMiniMode:I

    .line 5458
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMinimumMode:Z

    .line 6545
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mNumberOfPenExist:I

    .line 6871
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$50;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$50;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

    .line 6961
    const-string v0, "Beautify"

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStr:Ljava/lang/String;

    .line 6963
    const-string v0, "Sustenance"

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSustenanceStr:Ljava/lang/String;

    .line 7012
    const/4 v0, 0x6

    new-array v0, v0, [[I

    .line 7013
    const/16 v1, 0xa

    new-array v1, v1, [I

    const/16 v2, 0xb

    aput v2, v1, v5

    aput v7, v1, v6

    aput v9, v1, v7

    const/16 v2, 0x8

    aput v2, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0xf

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    const/16 v3, 0xb

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v5

    .line 7014
    const/16 v1, 0xa

    new-array v1, v1, [I

    const/16 v2, 0xc

    aput v2, v1, v5

    aput v7, v1, v6

    aput v9, v1, v7

    aput v6, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0x12

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    const/16 v3, 0xd

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v6

    .line 7015
    const/16 v1, 0xa

    new-array v1, v1, [I

    aput v9, v1, v5

    aput v7, v1, v6

    aput v7, v1, v7

    aput v7, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0x12

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    aput v9, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v7

    .line 7016
    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v2, 0x6

    aput v2, v1, v5

    aput v7, v1, v6

    aput v8, v1, v7

    aput v8, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0xc

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    const/4 v3, 0x6

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v8

    const/4 v1, 0x4

    .line 7017
    const/16 v2, 0xa

    new-array v2, v2, [I

    aput v6, v2, v5

    aput v7, v2, v6

    aput v7, v2, v7

    const/16 v3, 0x8

    aput v3, v2, v8

    const/4 v3, 0x4

    aput v6, v2, v3

    const/16 v3, 0xf

    aput v3, v2, v9

    const/4 v3, 0x6

    const/16 v4, 0x46

    aput v4, v2, v3

    const/4 v3, 0x7

    aput v8, v2, v3

    const/16 v3, 0x9

    aput v6, v2, v3

    aput-object v2, v0, v1

    .line 7018
    const/16 v1, 0xa

    new-array v1, v1, [I

    aput v8, v1, v5

    aput v6, v1, v6

    aput v8, v1, v7

    aput v9, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0xc

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    aput v6, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v9

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCurrentBeautifyAdvanceSettingValues:[[I

    .line 1985
    sput p4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    .line 1986
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, p1, p2, p4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1987
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 1988
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 1990
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    .line 1991
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 1993
    const/4 v0, -0x2

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    .line 1995
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    .line 1997
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    if-eqz v0, :cond_0

    .line 1998
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->initPenPlugin(Landroid/content/Context;)V

    .line 2002
    :cond_0
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSupportBeautifyPen:Z

    .line 2004
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 2006
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->initButtonValue()V

    .line 2007
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->initView(Ljava/lang/String;)V

    .line 2008
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setListener()V

    .line 2009
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    .line 2010
    return-void
.end method

.method private ColorPickerSettingInit()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2644
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;-><init>(Landroid/content/Context;Landroid/widget/RelativeLayout;FII)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    .line 2645
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    if-nez v0, :cond_0

    .line 2653
    :goto_0
    return-void

    .line 2648
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSettingExitButton:Landroid/view/View;

    .line 2649
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSettingExitButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mExitButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2650
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerColorImage:Landroid/view/View;

    .line 2651
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    .line 2652
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerCurrentColorListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private PaletteView()Landroid/view/ViewGroup;
    .locals 15

    .prologue
    const/16 v14, 0xf

    const/4 v13, 0x1

    const/high16 v12, 0x41d80000    # 27.0f

    const/high16 v3, 0x40800000    # 4.0f

    const/high16 v11, 0x42a60000    # 83.0f

    .line 5053
    new-instance v9, Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v9, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 5054
    .local v9, "sizeDisplayLayout":Landroid/widget/RelativeLayout;
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    .line 5055
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 5054
    invoke-direct {v10, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5056
    .local v10, "sizeDisplayLayoutParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 5057
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    const/4 v3, 0x0

    .line 5056
    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 5058
    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5060
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteRightButton:Landroid/view/View;

    .line 5061
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 5062
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 5061
    invoke-direct {v8, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5063
    .local v8, "rightImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v13, v8, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 5064
    const/16 v0, 0xb

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 5065
    invoke-virtual {v8, v14}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 5067
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteRightButton:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5068
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteRightButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_next"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 5070
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteRightButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->rightBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->rightBgPresslPath:Ljava/lang/String;

    .line 5071
    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->rightBgFocuslPath:Ljava/lang/String;

    const/16 v5, 0x1b

    const/16 v6, 0x53

    .line 5070
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 5073
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteLeftButton:Landroid/view/View;

    .line 5074
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 5075
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 5074
    invoke-direct {v7, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5076
    .local v7, "leftImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v13, v7, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 5077
    const/16 v0, 0x9

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 5078
    invoke-virtual {v7, v14}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 5080
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteLeftButton:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5081
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteLeftButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_back"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 5083
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteLeftButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->leftBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->lefBgPresslPath:Ljava/lang/String;

    .line 5084
    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->lefBgFocuslPath:Ljava/lang/String;

    const/16 v5, 0x1b

    const/16 v6, 0x53

    .line 5083
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 5088
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->colorPaletteView()Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    .line 5091
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteLeftButton:Landroid/view/View;

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 5092
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 5093
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteRightButton:Landroid/view/View;

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 5095
    return-object v9
.end method

.method private PickerView()Landroid/view/ViewGroup;
    .locals 6

    .prologue
    const/high16 v5, 0x42080000    # 34.0f

    .line 5008
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 5009
    .local v1, "localLinearLayout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 5010
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v3, 0x43828000    # 261.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42820000    # 65.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 5009
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5011
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 5012
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 5013
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42b00000    # 88.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 5014
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5016
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->colorGradationView()Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    .line 5017
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 5019
    return-object v1
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V
    .locals 0

    .prologue
    .line 5399
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->presetDisplay()V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 4693
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isBeautifyPen(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Z
    .locals 1

    .prologue
    .line 324
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaAutoIncrement:Z

    return v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Z)V
    .locals 0

    .prologue
    .line 324
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaAutoIncrement:Z

    return-void
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Z
    .locals 1

    .prologue
    .line 325
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaAutoDecrement:Z

    return v0
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Z)V
    .locals 0

    .prologue
    .line 325
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaAutoDecrement:Z

    return-void
.end method

.method static synthetic access$14(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Z
    .locals 1

    .prologue
    .line 322
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeAutoIncrement:Z

    return v0
.end method

.method static synthetic access$15(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Z)V
    .locals 0

    .prologue
    .line 322
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeAutoIncrement:Z

    return-void
.end method

.method static synthetic access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Z
    .locals 1

    .prologue
    .line 323
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeAutoDecrement:Z

    return v0
.end method

.method static synthetic access$17(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Z)V
    .locals 0

    .prologue
    .line 323
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeAutoDecrement:Z

    return-void
.end method

.method static synthetic access$18(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->repeatUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$19(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Z
    .locals 1

    .prologue
    .line 301
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isPresetClicked:Z

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Z)V
    .locals 0

    .prologue
    .line 300
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMagicPenEnable:Z

    return-void
.end method

.method static synthetic access$20(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 6544
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;I)V
    .locals 0

    .prologue
    .line 3707
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penSelectIndex(I)V

    return-void
.end method

.method static synthetic access$22(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    return-object v0
.end method

.method static synthetic access$23(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 5777
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penSelection(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$24(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PopupListener;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PopupListener;

    return-object v0
.end method

.method static synthetic access$25(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Z)V
    .locals 0

    .prologue
    .line 331
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mAdvancedSettingShow:Z

    return-void
.end method

.method static synthetic access$26(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Z
    .locals 1

    .prologue
    .line 331
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mAdvancedSettingShow:Z

    return v0
.end method

.method static synthetic access$27(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mAdvancedSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    return-object v0
.end method

.method static synthetic access$28(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Z)V
    .locals 0

    .prologue
    .line 3389
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->showBeautifySettingViews(Z)V

    return-void
.end method

.method static synthetic access$29(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 4723
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isChinesePen(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V
    .locals 0

    .prologue
    .line 6688
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->playScrollAnimationChoosePen()V

    return-void
.end method

.method static synthetic access$30(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V
    .locals 0

    .prologue
    .line 4915
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->updateBeautifySettingData()V

    return-void
.end method

.method static synthetic access$31(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Z)V
    .locals 0

    .prologue
    .line 4969
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->beautifyUpdateSettingUI(Z)V

    return-void
.end method

.method static synthetic access$32(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)I
    .locals 1

    .prologue
    .line 348
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCurrentBeautifyStyle:I

    return v0
.end method

.method static synthetic access$33(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;I)V
    .locals 0

    .prologue
    .line 348
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCurrentBeautifyStyle:I

    return-void
.end method

.method static synthetic access$34(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;I)V
    .locals 0

    .prologue
    .line 4906
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->resetBeautifyAdvanceDataAndUpdateSeekBarUi(I)V

    return-void
.end method

.method static synthetic access$35(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;II)V
    .locals 0

    .prologue
    .line 4954
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setBeautifyAdvancedDataToPlugin(II)V

    return-void
.end method

.method static synthetic access$36(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;IFF)V
    .locals 0

    .prologue
    .line 6705
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->playScrollAnimation(IFF)V

    return-void
.end method

.method static synthetic access$37(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mHandlerForScrollPen:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$38(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)I
    .locals 1

    .prologue
    .line 309
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCountForScrollPen:I

    return v0
.end method

.method static synthetic access$39(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;I)V
    .locals 0

    .prologue
    .line 309
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCountForScrollPen:I

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Z
    .locals 1

    .prologue
    .line 5458
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMinimumMode:Z

    return v0
.end method

.method static synthetic access$40(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimerForScrollPen:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$41(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Ljava/util/Timer;)V
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimerForScrollPen:Ljava/util/Timer;

    return-void
.end method

.method static synthetic access$42(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$43(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)I
    .locals 1

    .prologue
    .line 293
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCount:I

    return v0
.end method

.method static synthetic access$44(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;I)V
    .locals 0

    .prologue
    .line 293
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCount:I

    return-void
.end method

.method static synthetic access$45(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$46(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Ljava/util/Timer;)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimer:Ljava/util/Timer;

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Z)V
    .locals 0

    .prologue
    .line 5458
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMinimumMode:Z

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;I)V
    .locals 0

    .prologue
    .line 5457
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->deltaOfMiniMode:I

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;I)V
    .locals 0

    .prologue
    .line 5949
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Z
    .locals 1

    .prologue
    .line 300
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMagicPenEnable:Z

    return v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->alphaDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private advancedSettingButton()Landroid/view/View;
    .locals 7

    .prologue
    const/4 v3, -0x2

    .line 3696
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 3697
    .local v1, "localImageButton":Landroid/widget/ImageButton;
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 3699
    .local v0, "layoutParam":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3700
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 3701
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImagePath_snote_add:Ljava/lang/String;

    .line 3702
    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImagePath_snote_add_press:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImagePath_snote_add_press:Ljava/lang/String;

    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImagePath_snote_add_dim:Ljava/lang/String;

    .line 3701
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3703
    return-object v1
.end method

.method private beautifyAdvanceCursive()Landroid/view/View;
    .locals 19

    .prologue
    .line 4238
    new-instance v10, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v10, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 4240
    .local v10, "advanceZoneLayout":Landroid/widget/LinearLayout;
    new-instance v11, Landroid/widget/LinearLayout$LayoutParams;

    .line 4241
    const/4 v1, -0x1

    const/4 v2, -0x1

    .line 4240
    invoke-direct {v11, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 4242
    .local v11, "advanceZoneParam":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4243
    const/4 v1, 0x1

    invoke-virtual {v10, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 4244
    const/4 v1, 0x1

    invoke-virtual {v10, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 4248
    new-instance v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    .line 4251
    new-instance v18, Landroid/widget/LinearLayout$LayoutParams;

    .line 4252
    const/4 v1, -0x1

    const/4 v2, -0x2

    .line 4251
    move-object/from16 v0, v18

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 4253
    .local v18, "titleViewParam":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    move-object/from16 v0, v18

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 4254
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    move-object/from16 v0, v18

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4255
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41400000    # 12.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 4256
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 4257
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 4259
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x420c0000    # 35.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    const/4 v3, 0x0

    .line 4260
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x420c0000    # 35.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    const/4 v5, 0x0

    .line 4259
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 4262
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    const/16 v2, 0x56

    const/16 v3, 0x57

    const/16 v4, 0x5b

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 4263
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_cursive"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4267
    new-instance v1, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    .line 4268
    new-instance v16, Landroid/widget/RelativeLayout$LayoutParams;

    .line 4269
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41c80000    # 25.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 4268
    move-object/from16 v0, v16

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4270
    .local v16, "plusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v1, 0x1

    move-object/from16 v0, v16

    iput-boolean v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 4271
    const/16 v1, 0xb

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4272
    const/16 v1, 0xf

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4274
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4275
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_plus"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 4277
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgPath:Ljava/lang/String;

    .line 4278
    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgPressPath:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgFocusPath:Ljava/lang/String;

    const/16 v6, 0x19

    const/16 v7, 0x19

    .line 4277
    invoke-virtual/range {v1 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 4282
    new-instance v1, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    .line 4284
    new-instance v15, Landroid/widget/RelativeLayout$LayoutParams;

    .line 4285
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41c80000    # 25.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 4284
    invoke-direct {v15, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4286
    .local v15, "minusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v1, 0x1

    iput-boolean v1, v15, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 4287
    const/16 v1, 0x9

    invoke-virtual {v15, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4288
    const/16 v1, 0xf

    invoke-virtual {v15, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4290
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    invoke-virtual {v1, v15}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4291
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_minus"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 4292
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgPath:Ljava/lang/String;

    .line 4293
    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgPressPath:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgFocusPath:Ljava/lang/String;

    const/16 v6, 0x19

    const/16 v7, 0x19

    .line 4292
    invoke-virtual/range {v1 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 4297
    new-instance v8, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v8, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 4298
    .local v8, "advanceSeekZoneLayout":Landroid/widget/RelativeLayout;
    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    .line 4299
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41c80000    # 25.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 4298
    invoke-direct {v9, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 4300
    .local v9, "advanceSeekZoneLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v9, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 4301
    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4303
    const/4 v1, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getBeautifyAdvanceMaxValue(I)I

    move-result v14

    .line 4304
    .local v14, "maxProgress":I
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->beautifyAdvanceSeekBar()Landroid/widget/SeekBar;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    .line 4305
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v14}, Landroid/widget/SeekBar;->setMax(I)V

    .line 4307
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    invoke-virtual {v8, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 4308
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v8, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 4309
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    invoke-virtual {v8, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 4312
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    invoke-virtual {v10, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 4313
    invoke-virtual {v10, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 4316
    new-instance v13, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v13, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 4317
    .local v13, "imgDivider":Landroid/widget/ImageView;
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    .line 4318
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 4317
    invoke-direct {v12, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4319
    .local v12, "dividerParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v1, 0x1

    iput-boolean v1, v12, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 4320
    const/4 v1, 0x6

    invoke-virtual {v12, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4321
    invoke-virtual {v13, v12}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4322
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v12, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 4323
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->lineDivider:Ljava/lang/String;

    invoke-virtual {v1, v13, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 4324
    new-instance v17, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    move-object/from16 v0, v17

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 4325
    .local v17, "relativeLayoutContainer":Landroid/widget/RelativeLayout;
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4326
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 4327
    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 4329
    return-object v17
.end method

.method private beautifyAdvanceDummy()Landroid/view/View;
    .locals 15

    .prologue
    .line 4424
    new-instance v9, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v9, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 4426
    .local v9, "advanceZoneLayout":Landroid/widget/LinearLayout;
    new-instance v10, Landroid/widget/LinearLayout$LayoutParams;

    .line 4427
    const/4 v0, -0x1

    const/4 v1, -0x1

    .line 4426
    invoke-direct {v10, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 4428
    .local v10, "advanceZoneParam":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4429
    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 4430
    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 4434
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyDummyTextView:Landroid/widget/TextView;

    .line 4436
    new-instance v14, Landroid/widget/LinearLayout$LayoutParams;

    .line 4437
    const/4 v0, -0x1

    const/4 v1, -0x2

    .line 4436
    invoke-direct {v14, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 4438
    .local v14, "titleViewParam":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v14, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 4439
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyDummyTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4440
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyDummyTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41400000    # 12.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 4441
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyDummyTextView:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 4442
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyDummyTextView:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 4444
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyDummyTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x420c0000    # 35.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    const/4 v2, 0x0

    .line 4445
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x420c0000    # 35.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    const/4 v4, 0x0

    .line 4444
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 4447
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyDummyTextView:Landroid/widget/TextView;

    const/16 v1, 0x56

    const/16 v2, 0x57

    const/16 v3, 0x5b

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 4448
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyDummyTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_dummy"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4452
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    .line 4453
    new-instance v13, Landroid/widget/RelativeLayout$LayoutParams;

    .line 4454
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41c80000    # 25.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 4453
    invoke-direct {v13, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4455
    .local v13, "plusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v13, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 4456
    const/16 v0, 0xb

    invoke-virtual {v13, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4457
    const/16 v0, 0xf

    invoke-virtual {v13, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4459
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    invoke-virtual {v0, v13}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4460
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_plus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 4462
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgPressPath:Ljava/lang/String;

    .line 4463
    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgFocusPath:Ljava/lang/String;

    const/16 v5, 0x19

    const/16 v6, 0x19

    .line 4462
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 4467
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    .line 4469
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    .line 4470
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41c80000    # 25.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 4469
    invoke-direct {v12, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4471
    .local v12, "minusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v12, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 4472
    const/16 v0, 0x9

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4473
    const/16 v0, 0xf

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4475
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    invoke-virtual {v0, v12}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4476
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_minus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 4477
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgPath:Ljava/lang/String;

    .line 4478
    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgFocusPath:Ljava/lang/String;

    const/16 v5, 0x19

    const/16 v6, 0x19

    .line 4477
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 4482
    new-instance v7, Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 4483
    .local v7, "advanceSeekZoneLayout":Landroid/widget/RelativeLayout;
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x1

    .line 4484
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 4483
    invoke-direct {v8, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 4485
    .local v8, "advanceSeekZoneLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v8, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 4486
    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4488
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getBeautifyAdvanceMaxValue(I)I

    move-result v11

    .line 4489
    .local v11, "maxProgress":I
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->beautifyAdvanceSeekBar()Landroid/widget/SeekBar;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    .line 4490
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v11}, Landroid/widget/SeekBar;->setMax(I)V

    .line 4492
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 4493
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 4494
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 4497
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyDummyTextView:Landroid/widget/TextView;

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 4498
    invoke-virtual {v9, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 4499
    return-object v9
.end method

.method private beautifyAdvanceModulation()Landroid/view/View;
    .locals 15

    .prologue
    .line 4508
    new-instance v9, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v9, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 4510
    .local v9, "advanceZoneLayout":Landroid/widget/LinearLayout;
    new-instance v10, Landroid/widget/LinearLayout$LayoutParams;

    .line 4511
    const/4 v0, -0x1

    const/4 v1, -0x1

    .line 4510
    invoke-direct {v10, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 4512
    .local v10, "advanceZoneParam":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4513
    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 4514
    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 4518
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyModulationTextView:Landroid/widget/TextView;

    .line 4520
    new-instance v14, Landroid/widget/LinearLayout$LayoutParams;

    .line 4521
    const/4 v0, -0x1

    const/4 v1, -0x2

    .line 4520
    invoke-direct {v14, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 4522
    .local v14, "titleViewParam":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v14, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 4523
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyModulationTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4524
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyModulationTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    .line 4525
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41400000    # 12.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    .line 4524
    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 4526
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyModulationTextView:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 4527
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyModulationTextView:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 4529
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyModulationTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x420c0000    # 35.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    const/4 v2, 0x0

    .line 4530
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x420c0000    # 35.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    const/4 v4, 0x0

    .line 4529
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 4532
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyModulationTextView:Landroid/widget/TextView;

    const/16 v1, 0x56

    const/16 v2, 0x57

    const/16 v3, 0x5b

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 4533
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyModulationTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_modulation"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4537
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    .line 4538
    new-instance v13, Landroid/widget/RelativeLayout$LayoutParams;

    .line 4539
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41c80000    # 25.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 4538
    invoke-direct {v13, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4540
    .local v13, "plusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v13, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 4541
    const/16 v0, 0xb

    invoke-virtual {v13, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4542
    const/16 v0, 0xf

    invoke-virtual {v13, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4544
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    invoke-virtual {v0, v13}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4545
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_plus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 4547
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgPath:Ljava/lang/String;

    .line 4548
    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgFocusPath:Ljava/lang/String;

    const/16 v5, 0x19

    const/16 v6, 0x19

    .line 4547
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 4552
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    .line 4554
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    .line 4555
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41c80000    # 25.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 4554
    invoke-direct {v12, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4556
    .local v12, "minusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v12, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 4557
    const/16 v0, 0x9

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4558
    const/16 v0, 0xf

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4560
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    invoke-virtual {v0, v12}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4561
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_minus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 4562
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgPath:Ljava/lang/String;

    .line 4563
    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgFocusPath:Ljava/lang/String;

    const/16 v5, 0x19

    const/16 v6, 0x19

    .line 4562
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 4567
    new-instance v7, Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 4568
    .local v7, "advanceSeekZoneLayout":Landroid/widget/RelativeLayout;
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x1

    .line 4569
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 4568
    invoke-direct {v8, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 4570
    .local v8, "advanceSeekZoneLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v8, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 4571
    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4573
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getBeautifyAdvanceMaxValue(I)I

    move-result v11

    .line 4574
    .local v11, "maxProgress":I
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->beautifyAdvanceSeekBar()Landroid/widget/SeekBar;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    .line 4575
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v11}, Landroid/widget/SeekBar;->setMax(I)V

    .line 4577
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 4578
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 4579
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 4582
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyModulationTextView:Landroid/widget/TextView;

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 4583
    invoke-virtual {v9, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 4584
    return-object v9
.end method

.method private beautifyAdvanceResetBtn()Landroid/widget/Button;
    .locals 6

    .prologue
    const/high16 v5, 0x40a00000    # 5.0f

    .line 4632
    new-instance v1, Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    .line 4633
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 4634
    const/4 v1, -0x1

    .line 4635
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42200000    # 40.0f

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 4633
    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 4636
    .local v0, "resetBtnLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 4637
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 4638
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4640
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->popupBtnBgNomalPath:Ljava/lang/String;

    .line 4641
    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->popupBtnBgPressPath:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->popupBtnBgFocusPath:Ljava/lang/String;

    .line 4640
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 4643
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 4644
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setGravity(I)V

    .line 4645
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    const/4 v2, 0x0

    .line 4646
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41600000    # 14.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    .line 4645
    invoke-virtual {v1, v2, v3}, Landroid/widget/Button;->setTextSize(IF)V

    .line 4647
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_reset"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 4648
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    return-object v1
.end method

.method private beautifyAdvanceSeekBar()Landroid/widget/SeekBar;
    .locals 12

    .prologue
    const/16 v4, 0x16

    const/high16 v5, 0x41200000    # 10.0f

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 4593
    new-instance v8, Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v8, v2}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 4594
    .local v8, "seekBar":Landroid/widget/SeekBar;
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    .line 4595
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x433e0000    # 190.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    const/4 v3, -0x2

    .line 4594
    invoke-direct {v9, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4597
    .local v9, "seekBarParams":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v11, v9, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 4598
    const/16 v2, 0xe

    invoke-virtual {v9, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4599
    const/16 v2, 0xf

    invoke-virtual {v9, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4600
    invoke-virtual {v8, v9}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4603
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 4604
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 4603
    invoke-virtual {v8, v2, v10, v3, v10}, Landroid/widget/SeekBar;->setPadding(IIII)V

    .line 4606
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->handelPath:Ljava/lang/String;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->handelPressPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->handelFocusPath:Ljava/lang/String;

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v8, v2}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 4609
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    if-nez v2, :cond_0

    .line 4610
    new-instance v2, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    .line 4611
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v2, v10}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 4612
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40900000    # 4.5f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 4615
    :cond_0
    new-instance v6, Landroid/graphics/drawable/ClipDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    const/4 v3, 0x3

    invoke-direct {v6, v2, v3, v11}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    .line 4616
    .local v6, "localClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->progressBgPath:Ljava/lang/String;

    const/16 v4, 0xbe

    const/16 v5, 0x9

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 4619
    .local v1, "localDrawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    move v2, v10

    move v3, v10

    move v4, v10

    move v5, v10

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 4620
    .local v0, "localInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    new-instance v7, Landroid/graphics/drawable/LayerDrawable;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    aput-object v0, v2, v10

    aput-object v6, v2, v11

    invoke-direct {v7, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 4621
    .local v7, "localLayerDrawable":Landroid/graphics/drawable/LayerDrawable;
    invoke-virtual {v8, v7}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 4623
    return-object v8
.end method

.method private beautifyAdvanceSettingSeekbars()Landroid/view/View;
    .locals 4

    .prologue
    .line 4215
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 4218
    .local v0, "containerLayout":Landroid/widget/LinearLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 4219
    const/4 v2, -0x1

    const/4 v3, -0x2

    .line 4218
    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 4220
    .local v1, "containerParam":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4223
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 4224
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->beautifyAdvanceCursive()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 4225
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->beautifyAdvanceSustenance()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 4226
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->beautifyAdvanceDummy()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 4227
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->beautifyAdvanceModulation()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 4228
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->beautifyAdvanceResetBtn()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 4229
    return-object v0
.end method

.method private beautifyAdvanceSustenance()Landroid/view/View;
    .locals 15

    .prologue
    .line 4339
    new-instance v9, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v9, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 4341
    .local v9, "advanceZoneLayout":Landroid/widget/LinearLayout;
    new-instance v10, Landroid/widget/LinearLayout$LayoutParams;

    .line 4342
    const/4 v0, -0x1

    const/4 v1, -0x1

    .line 4341
    invoke-direct {v10, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 4343
    .local v10, "advanceZoneParam":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4344
    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 4345
    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 4349
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    .line 4351
    new-instance v14, Landroid/widget/LinearLayout$LayoutParams;

    .line 4352
    const/4 v0, -0x1

    const/4 v1, -0x2

    .line 4351
    invoke-direct {v14, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 4353
    .local v14, "titleViewParam":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v14, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 4354
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4355
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    .line 4356
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41400000    # 12.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    .line 4355
    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 4357
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 4358
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 4360
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x420c0000    # 35.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    const/4 v2, 0x0

    .line 4361
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x420c0000    # 35.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    const/4 v4, 0x0

    .line 4360
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 4363
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    const/16 v1, 0x56

    const/16 v2, 0x57

    const/16 v3, 0x5b

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 4364
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_sustenance"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4368
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    .line 4369
    new-instance v13, Landroid/widget/RelativeLayout$LayoutParams;

    .line 4370
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41c80000    # 25.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 4369
    invoke-direct {v13, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4371
    .local v13, "plusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v13, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 4372
    const/16 v0, 0xb

    invoke-virtual {v13, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4373
    const/16 v0, 0xf

    invoke-virtual {v13, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4375
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    invoke-virtual {v0, v13}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4376
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_plus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 4378
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgPath:Ljava/lang/String;

    .line 4379
    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgFocusPath:Ljava/lang/String;

    const/16 v5, 0x19

    const/16 v6, 0x19

    .line 4378
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 4383
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    .line 4385
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    .line 4386
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41c80000    # 25.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 4385
    invoke-direct {v12, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4387
    .local v12, "minusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v12, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 4388
    const/16 v0, 0x9

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4389
    const/16 v0, 0xf

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4391
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    invoke-virtual {v0, v12}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4392
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_minus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 4393
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgPath:Ljava/lang/String;

    .line 4394
    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgFocusPath:Ljava/lang/String;

    const/16 v5, 0x19

    const/16 v6, 0x19

    .line 4393
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 4398
    new-instance v7, Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 4399
    .local v7, "advanceSeekZoneLayout":Landroid/widget/RelativeLayout;
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x1

    .line 4400
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 4399
    invoke-direct {v8, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 4401
    .local v8, "advanceSeekZoneLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v8, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 4402
    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4404
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getBeautifyAdvanceMaxValue(I)I

    move-result v11

    .line 4405
    .local v11, "maxProgress":I
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->beautifyAdvanceSeekBar()Landroid/widget/SeekBar;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    .line 4406
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v11}, Landroid/widget/SeekBar;->setMax(I)V

    .line 4408
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 4409
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 4410
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 4413
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 4414
    invoke-virtual {v9, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 4415
    return-object v9
.end method

.method private beautifyEnableLayout()Landroid/view/View;
    .locals 15
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/16 v11, 0xf

    const/4 v10, -0x2

    const/4 v14, 0x0

    const/4 v9, -0x1

    const/4 v13, 0x1

    .line 3294
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v1, v7}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 3297
    .local v1, "enableLayout":Landroid/widget/RelativeLayout;
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 3298
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x421c0000    # 39.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 3297
    invoke-direct {v2, v9, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3301
    .local v2, "enableParam":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x40d00000    # 6.5f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    iput v7, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 3302
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x40b00000    # 5.5f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    iput v7, v2, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 3304
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3307
    new-instance v3, Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v3, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 3308
    .local v3, "imgDivider":Landroid/widget/ImageView;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3309
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 3308
    invoke-direct {v0, v9, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3310
    .local v0, "dividerParams":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v13, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 3311
    const/4 v7, 0x6

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3312
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3313
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v8, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->lineDivider:Ljava/lang/String;

    invoke-virtual {v7, v3, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 3314
    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 3318
    new-instance v7, Landroid/widget/TextView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableTextView:Landroid/widget/TextView;

    .line 3319
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v6, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3321
    .local v6, "titleParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v13, v6, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 3324
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41580000    # 13.5f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 3326
    const/16 v7, 0x9

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3327
    invoke-virtual {v6, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3328
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableTextView:Landroid/widget/TextView;

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3330
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableTextView:Landroid/widget/TextView;

    const/high16 v8, -0x1000000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 3331
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableTextView:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x41700000    # 15.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v7, v14, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 3332
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableTextView:Landroid/widget/TextView;

    const/16 v8, 0x13

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setGravity(I)V

    .line 3333
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableTextView:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStr:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3334
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableTextView:Landroid/widget/TextView;

    invoke-virtual {v7, v14}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 3335
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableTextView:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStr:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3340
    new-instance v7, Landroid/widget/Switch;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    .line 3341
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 3343
    .local v4, "sdk":I
    const/4 v5, 0x0

    .line 3349
    .local v5, "switchParam":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3350
    .end local v5    # "switchParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41c80000    # 25.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 3349
    invoke-direct {v5, v10, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3354
    .restart local v5    # "switchParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v13, v5, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 3357
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41080000    # 8.5f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    iput v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 3359
    const/16 v7, 0xb

    invoke-virtual {v5, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3360
    invoke-virtual {v5, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3361
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    invoke-virtual {v7, v5}, Landroid/widget/Switch;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3363
    const/16 v7, 0x10

    if-lt v4, v7, :cond_0

    .line 3364
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v9, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->switchThumbPath:Ljava/lang/String;

    const/16 v10, 0x18

    const/16 v11, 0x18

    invoke-virtual {v8, v9, v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/Switch;->setThumbDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3365
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v9, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->switchCheckFalseBgPath:Ljava/lang/String;

    .line 3366
    sget-object v10, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->switchCheckTrueBgPath:Ljava/lang/String;

    const/16 v11, 0x2e

    const/16 v12, 0x19

    .line 3365
    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableCheckedImg(Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/Switch;->setTrackDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3367
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x42380000    # 46.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/Switch;->setSwitchMinWidth(I)V

    .line 3371
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    invoke-virtual {v7, v13}, Landroid/widget/Switch;->setThumbTextPadding(I)V

    .line 3375
    :cond_0
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    invoke-virtual {v7, v14}, Landroid/widget/Switch;->setChecked(Z)V

    .line 3378
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 3379
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 3380
    return-object v1
.end method

.method private beautifyStyleBtnsLayout()Landroid/view/View;
    .locals 19

    .prologue
    .line 3465
    new-instance v11, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v11, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 3468
    .local v11, "containerLayout":Landroid/widget/LinearLayout;
    new-instance v12, Landroid/widget/LinearLayout$LayoutParams;

    .line 3469
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x422c0000    # 43.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 3468
    invoke-direct {v12, v1, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3470
    .local v12, "containerParam":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3473
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41300000    # 11.0f

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 3474
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40c00000    # 6.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41300000    # 11.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 3475
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x40800000    # 4.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 3473
    invoke-virtual {v11, v1, v4, v5, v6}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 3479
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 3480
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    .line 3483
    :cond_0
    const/16 v18, 0x0

    .local v18, "styleIndex":I
    :goto_0
    const/4 v1, 0x6

    move/from16 v0, v18

    if-lt v0, v1, :cond_1

    .line 3519
    new-instance v14, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v14, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 3520
    .local v14, "imgDivider":Landroid/widget/ImageView;
    new-instance v13, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    .line 3521
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 3520
    invoke-direct {v13, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3522
    .local v13, "dividerParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v1, 0x1

    iput-boolean v1, v13, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 3523
    const/4 v1, 0x6

    invoke-virtual {v13, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3524
    invoke-virtual {v14, v13}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3525
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40c00000    # 6.0f

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v13, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 3526
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v13, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 3527
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->lineDivider:Ljava/lang/String;

    invoke-virtual {v1, v14, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 3529
    new-instance v15, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v15, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 3530
    .local v15, "relativeLayoutContainer":Landroid/widget/RelativeLayout;
    invoke-virtual {v15, v12}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3531
    invoke-virtual {v15, v14}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 3532
    invoke-virtual {v15, v11}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 3534
    return-object v15

    .line 3484
    .end local v13    # "dividerParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v14    # "imgDivider":Landroid/widget/ImageView;
    .end local v15    # "relativeLayoutContainer":Landroid/widget/RelativeLayout;
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "chinabrush_mode_0"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v4, v18, 0x1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3485
    .local v2, "defaultStyleImgPath":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "chinabrush_mode_0"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v4, v18, 0x1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "_press"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3487
    .local v3, "pressStyleImgPath":Ljava/lang/String;
    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3488
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3489
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3492
    :cond_2
    new-instance v16, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    move-object/from16 v0, v16

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 3493
    .local v16, "styleBtn":Landroid/widget/ImageButton;
    new-instance v17, Landroid/widget/LinearLayout$LayoutParams;

    .line 3494
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42180000    # 38.0f

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42040000    # 33.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 3493
    move-object/from16 v0, v17

    invoke-direct {v0, v1, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3495
    .local v17, "styleBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3497
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 3498
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40800000    # 4.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x40a00000    # 5.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 3499
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x40a00000    # 5.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 3497
    move-object/from16 v0, v16

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/widget/ImageButton;->setPadding(IIII)V

    .line 3501
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 3502
    const/16 v5, 0x1c

    const/16 v6, 0x18

    move-object v4, v3

    .line 3501
    invoke-virtual/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3504
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->popupBtnBgNomalPath:Ljava/lang/String;

    sget-object v7, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->popupBtnBgPressPath:Ljava/lang/String;

    .line 3505
    sget-object v8, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->popupBtnBgFocusPath:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42180000    # 38.0f

    invoke-virtual {v1, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v9

    .line 3506
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42040000    # 33.0f

    invoke-virtual {v1, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v10

    move-object/from16 v5, v16

    .line 3504
    invoke-virtual/range {v4 .. v10}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 3509
    if-nez v18, :cond_3

    .line 3510
    const/4 v1, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3513
    :cond_3
    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3514
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3483
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_0
.end method

.method private beautifyUpdateSettingUI(Z)V
    .locals 7
    .param p1, "isBeautify"    # Z

    .prologue
    const/16 v6, 0x438

    const/high16 v5, 0x41200000    # 10.0f

    .line 4971
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getMinSettingValue()F

    move-result v1

    .line 4972
    .local v1, "min":F
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getMaxSettingValue()F

    move-result v0

    .line 4974
    .local v0, "max":F
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v3, :cond_3

    .line 4975
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v4}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 4976
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    .line 4980
    :goto_0
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    if-nez v3, :cond_0

    .line 4981
    iput v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    .line 4989
    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    const/high16 v4, 0x43b40000    # 360.0f

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    sub-float/2addr v3, v1

    mul-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 4990
    .local v2, "progress":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    sub-float v4, v0, v1

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setMax(I)V

    .line 4993
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 4995
    if-eqz p1, :cond_1

    .line 4996
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->updateBeautifySeekBarsFromString(Ljava/lang/String;)V

    .line 4999
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setPenType(Ljava/lang/String;)V

    .line 5000
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeSize(F)V

    .line 5001
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeColor(I)V

    .line 5002
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeAdvancedSetting(Ljava/lang/String;)V

    .line 5003
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->invalidate()V

    .line 5004
    return-void

    .line 4978
    .end local v2    # "progress":I
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    goto :goto_0

    .line 4984
    :cond_3
    iput v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    goto :goto_1
.end method

.method private bodyBg()Landroid/view/ViewGroup;
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 2906
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2907
    .local v0, "bodyBgLayout":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2910
    .local v1, "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2912
    new-instance v2, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 2913
    .local v2, "bodyLeft":Landroid/widget/ImageView;
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2915
    .local v3, "bodyLeftParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v4, 0x1

    iput-boolean v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 2916
    const/16 v4, 0x9

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2917
    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2918
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2931
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->lightBodyLeftPath:Ljava/lang/String;

    invoke-virtual {v4, v2, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 2934
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2935
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 2937
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2939
    return-object v0
.end method

.method private bodyLayout()Landroid/widget/RelativeLayout;
    .locals 5

    .prologue
    .line 2088
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2089
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2090
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v3, 0x43a48000    # 329.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    const/4 v3, -0x2

    .line 2089
    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2092
    .local v1, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2094
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penLayout()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenLayout:Landroid/view/View;

    .line 2096
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->bodyBg()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyBg:Landroid/view/View;

    .line 2098
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyBg:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2099
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2101
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->bottomLayout()Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    .line 2102
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v4, "string_resize"

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2103
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2104
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2107
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->presetTypeButton()Landroid/view/View;

    .line 2108
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->presetLayout()Landroid/widget/LinearLayout;

    .line 2111
    return-object v0
.end method

.method private bodyLayout2()Landroid/widget/RelativeLayout;
    .locals 5

    .prologue
    .line 2115
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2116
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 2117
    const v3, 0x43a48000    # 329.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43480000    # 200.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2116
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2119
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->bodyBg()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2120
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->typeSelectorlayout2()Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2121
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->presetLayout()Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2123
    return-object v0
.end method

.method private bottomLayout()Landroid/view/View;
    .locals 11

    .prologue
    const/4 v10, -0x1

    const/high16 v9, 0x41b00000    # 22.0f

    const/high16 v8, 0x41800000    # 16.0f

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    .line 5148
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 5149
    .local v1, "bottomLayout":Landroid/widget/RelativeLayout;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 5150
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v4, v7, v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 5149
    invoke-direct {v2, v10, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5153
    .local v2, "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43b70000    # 366.0f

    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v5, v7, v5

    sub-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 5152
    invoke-virtual {v2, v6, v3, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 5155
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5156
    new-instance v3, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomExtendBg:Landroid/widget/ImageView;

    .line 5157
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomExtendBg:Landroid/widget/ImageView;

    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 5158
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v6, v7, v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-direct {v4, v10, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5157
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5160
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomExtendBg:Landroid/widget/ImageView;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->bottomExpandPath:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 5162
    new-instance v3, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomHandle:Landroid/widget/ImageView;

    .line 5164
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 5165
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v4, v9, v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 5166
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v5, v8, v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 5164
    invoke-direct {v0, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5168
    .local v0, "bottomHandleParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v3, 0x1

    iput-boolean v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 5169
    const/16 v3, 0xe

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 5170
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 5174
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomHandle:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5176
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomHandle:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->bottomHandlePath:Ljava/lang/String;

    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v6, v9, v6

    float-to-int v6, v6

    .line 5177
    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v7, v8, v7

    float-to-int v7, v7

    .line 5176
    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 5179
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomExtendBg:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 5180
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomHandle:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 5181
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 5183
    return-object v1
.end method

.method private checkPosition()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 6198
    const/4 v4, 0x2

    new-array v0, v4, [I

    .line 6199
    .local v0, "location":[I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x437a0000    # 250.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 6200
    .local v2, "minWidth":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43230000    # 163.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 6202
    .local v1, "minHeight":I
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getLocationOnScreen([I)V

    .line 6204
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 6206
    .local v3, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    aget v4, v0, v6

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    if-ge v4, v5, :cond_0

    .line 6207
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 6209
    :cond_0
    aget v4, v0, v7

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    if-ge v4, v5, :cond_1

    .line 6210
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 6213
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    aget v5, v0, v6

    sub-int/2addr v4, v5

    if-ge v4, v2, :cond_2

    .line 6214
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int/2addr v4, v2

    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 6216
    iget v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-gez v4, :cond_2

    .line 6217
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 6220
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    aget v5, v0, v7

    sub-int/2addr v4, v5

    if-ge v4, v1, :cond_3

    .line 6221
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v4, v1

    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 6223
    iget v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-gez v4, :cond_3

    .line 6224
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 6228
    :cond_3
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6229
    return-void
.end method

.method private colorGradationView()Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 5129
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    .line 5130
    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    .line 5129
    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    .line 5131
    .local v1, "localf":Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 5132
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v3, 0x43828000    # 261.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42820000    # 65.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 5131
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5134
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5135
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setClickable(Z)V

    .line 5136
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_gradation"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 5137
    invoke-virtual {v1, v5, v5, v5, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setPadding(IIII)V

    .line 5138
    return-object v1
.end method

.method private colorPaletteGradationLayout()Landroid/view/ViewGroup;
    .locals 5

    .prologue
    .line 5030
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 5031
    .local v1, "localLinearLayout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    .line 5032
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43230000    # 163.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 5031
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5034
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5036
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->PickerView()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPickerView:Landroid/view/View;

    .line 5038
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->PaletteView()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteView:Landroid/view/View;

    .line 5039
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->paletteBg()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteBg:Landroid/view/View;

    .line 5040
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 5041
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 5042
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPickerView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 5043
    return-object v1
.end method

.method private colorPaletteView()Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 5100
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    .line 5103
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 5104
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v2, 0x43828000    # 261.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    const/4 v2, -0x2

    .line 5103
    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5106
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 5107
    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 5108
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 5111
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5112
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v1, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setClickable(Z)V

    .line 5113
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v1, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setFocusable(Z)V

    .line 5118
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    return-object v1
.end method

.method private findMinValue(Landroid/widget/TextView;I)V
    .locals 4
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "maxWidth"    # I

    .prologue
    const/4 v3, 0x0

    .line 2745
    const/high16 v0, 0x41a00000    # 20.0f

    .line 2747
    .local v0, "currentFloat":F
    :goto_0
    invoke-virtual {p1, v3, v3}, Landroid/widget/TextView;->measure(II)V

    .line 2748
    invoke-virtual {p1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    .line 2750
    .local v1, "width":I
    if-le v1, p2, :cond_0

    .line 2751
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v0, v2

    .line 2752
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 2754
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2758
    return-void
.end method

.method private getBeautifyAdvanceArrayDataToString([I)Ljava/lang/String;
    .locals 3
    .param p1, "advanceData"    # [I

    .prologue
    .line 4791
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4792
    .local v0, "advancedData":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "paramIndex":I
    :goto_0
    const/16 v2, 0xa

    if-lt v1, v2, :cond_0

    .line 4796
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 4793
    :cond_0
    aget v2, p1, v1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 4794
    const/16 v2, 0x3b

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 4792
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private getBeautifyAdvanceMaxValue(I)I
    .locals 1
    .param p1, "advanceParamType"    # I

    .prologue
    .line 4663
    const/4 v0, 0x0

    .line 4665
    .local v0, "maxValue":I
    packed-switch p1, :pswitch_data_0

    .line 4680
    :pswitch_0
    const/16 v0, 0x14

    .line 4683
    :goto_0
    return v0

    .line 4667
    :pswitch_1
    const/16 v0, 0xc

    .line 4668
    goto :goto_0

    .line 4670
    :pswitch_2
    const/16 v0, 0x10

    .line 4671
    goto :goto_0

    .line 4673
    :pswitch_3
    const/16 v0, 0x14

    .line 4674
    goto :goto_0

    .line 4676
    :pswitch_4
    const/16 v0, 0x64

    .line 4677
    goto :goto_0

    .line 4665
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private getBeautifyAdvanceParamDataFromArray([II)I
    .locals 4
    .param p1, "advanceData"    # [I
    .param p2, "beautifyParam"    # I

    .prologue
    .line 4773
    const/4 v0, 0x0

    .line 4775
    .local v0, "data":I
    if-eqz p1, :cond_0

    array-length v2, p1

    const/16 v3, 0xa

    if-eq v2, v3, :cond_1

    :cond_0
    move v1, v0

    .line 4781
    .end local v0    # "data":I
    .local v1, "data":I
    :goto_0
    return v1

    .line 4779
    .end local v1    # "data":I
    .restart local v0    # "data":I
    :cond_1
    aget v0, p1, p2

    move v1, v0

    .line 4781
    .end local v0    # "data":I
    .restart local v1    # "data":I
    goto :goto_0
.end method

.method private getBeautifyAdvanceParamDataFromString(Ljava/lang/String;I)I
    .locals 5
    .param p1, "advanceSetting"    # Ljava/lang/String;
    .param p2, "beautifyParam"    # I

    .prologue
    .line 4746
    const/4 v1, 0x0

    .line 4747
    .local v1, "data":I
    const-string v3, ";"

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 4749
    .local v0, "advanceStyleData":[Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    array-length v3, v0

    const/16 v4, 0xa

    if-eq v3, v4, :cond_1

    :cond_0
    move v2, v1

    .line 4755
    .end local v1    # "data":I
    .local v2, "data":I
    :goto_0
    return v2

    .line 4753
    .end local v2    # "data":I
    .restart local v1    # "data":I
    :cond_1
    aget-object v3, v0, p2

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    move v2, v1

    .line 4755
    .end local v1    # "data":I
    .restart local v2    # "data":I
    goto :goto_0
.end method

.method private getBeautifyStyleBtnIndex(Ljava/lang/String;)I
    .locals 3
    .param p1, "advanceSetting"    # Ljava/lang/String;

    .prologue
    .line 3546
    const/4 v2, 0x0

    invoke-direct {p0, p1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getBeautifyAdvanceParamDataFromString(Ljava/lang/String;I)I

    move-result v1

    .line 3547
    .local v1, "styleId":I
    const/4 v0, 0x0

    .line 3549
    .local v0, "styleBtnIdx":I
    packed-switch v1, :pswitch_data_0

    .line 3574
    :pswitch_0
    const/4 v0, 0x0

    .line 3578
    :goto_0
    return v0

    .line 3551
    :pswitch_1
    const/4 v0, 0x0

    .line 3552
    goto :goto_0

    .line 3554
    :pswitch_2
    const/4 v0, 0x1

    .line 3555
    goto :goto_0

    .line 3558
    :pswitch_3
    const/4 v0, 0x2

    .line 3559
    goto :goto_0

    .line 3562
    :pswitch_4
    const/4 v0, 0x3

    .line 3563
    goto :goto_0

    .line 3566
    :pswitch_5
    const/4 v0, 0x4

    .line 3567
    goto :goto_0

    .line 3570
    :pswitch_6
    const/4 v0, 0x5

    .line 3571
    goto :goto_0

    .line 3549
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getMovableRect()Landroid/graphics/Rect;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2133
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 2134
    .local v0, "location":[I
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 2136
    .local v1, "r":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getLocationOnScreen([I)V

    .line 2138
    aget v2, v0, v4

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mLeftMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 2139
    aget v2, v0, v5

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTopMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 2140
    aget v2, v0, v4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 2141
    aget v2, v0, v5

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 2143
    return-object v1
.end method

.method private hasBeautifyPen()Z
    .locals 4

    .prologue
    .line 3452
    const/4 v0, 0x0

    .line 3453
    .local v0, "hasBeautifyPen":Z
    const/4 v1, -0x1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    const-string v3, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 3454
    const/4 v0, 0x1

    .line 3456
    :cond_0
    return v0
.end method

.method private initButtonValue()V
    .locals 1

    .prologue
    .line 2129
    const/16 v0, 0x2a

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->EXIT_BUTTON_WIDTH:I

    .line 2130
    return-void
.end method

.method private initColorSelecteView()V
    .locals 3

    .prologue
    .line 2657
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    if-eqz v0, :cond_0

    .line 2658
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOnColorChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setInitialValue(Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;I)V

    .line 2663
    :cond_0
    return-void
.end method

.method private initPenPlugin(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 6239
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;-><init>(Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    .line 6241
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenCount()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginCount:I

    .line 6243
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginInfoList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    .line 6246
    return-void
.end method

.method private isBeautifyPen(I)Z
    .locals 3
    .param p1, "penNameIndex"    # I

    .prologue
    .line 4708
    const/4 v0, 0x0

    .line 4709
    .local v0, "isBeautifyPen":Z
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v1

    .line 4710
    .local v1, "penName":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v2, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4711
    const/4 v0, 0x1

    .line 4713
    :cond_0
    return v0
.end method

.method private isBeautifyPen(Ljava/lang/String;)Z
    .locals 2
    .param p1, "penName"    # Ljava/lang/String;

    .prologue
    .line 4694
    const/4 v0, 0x0

    .line 4695
    .local v0, "isBeautifyPen":Z
    if-eqz p1, :cond_0

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4696
    const/4 v0, 0x1

    .line 4698
    :cond_0
    return v0
.end method

.method private isChinesePen(Ljava/lang/String;)Z
    .locals 2
    .param p1, "penName"    # Ljava/lang/String;

    .prologue
    .line 4724
    const/4 v0, 0x0

    .line 4725
    .local v0, "isChinesePen":Z
    if-eqz p1, :cond_0

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4726
    const/4 v0, 0x1

    .line 4728
    :cond_0
    return v0
.end method

.method private lineButton1()Landroid/view/View;
    .locals 6

    .prologue
    .line 2866
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->EXIT_BUTTON_RIGHT_MARGIN:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->EXIT_BUTTON_WIDTH:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->LINE_BUTTON_WIDTH:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->EXIT_BUTTON_WIDTH:I

    int-to-float v4, v4

    add-float v2, v3, v4

    .line 2868
    .local v2, "rightMargin":F
    new-instance v0, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 2869
    .local v0, "lineView":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2870
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 2871
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41880000    # 17.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 2869
    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2872
    .local v1, "presetButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v3, 0xb

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2873
    const/16 v3, 0xf

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2876
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2878
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2880
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->linePath:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 2881
    return-object v0
.end method

.method private lineButton2()Landroid/view/View;
    .locals 6

    .prologue
    .line 2886
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->EXIT_BUTTON_RIGHT_MARGIN:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->EXIT_BUTTON_WIDTH:I

    int-to-float v4, v4

    add-float v2, v3, v4

    .line 2888
    .local v2, "rightMargin":F
    new-instance v0, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 2889
    .local v0, "lineView":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2890
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 2891
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41880000    # 17.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 2889
    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2892
    .local v1, "presetButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v3, 0xb

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2895
    const/16 v3, 0xf

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2896
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2898
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2900
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->linePath:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 2901
    return-object v0
.end method

.method private paletteBg()Landroid/view/ViewGroup;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 2944
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2945
    .local v1, "paletteBgLayout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2948
    .local v0, "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2960
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->grayBodyLeftPath:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 2966
    return-object v1
.end method

.method private penAlphaLayout()Landroid/view/ViewGroup;
    .locals 14

    .prologue
    .line 4077
    new-instance v12, Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v12, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 4079
    .local v12, "sizeDisplayLayout":Landroid/widget/RelativeLayout;
    new-instance v13, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    .line 4080
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42780000    # 62.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 4079
    invoke-direct {v13, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4081
    .local v13, "sizeDisplayLayoutParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v12, v13}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4082
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 4086
    new-instance v8, Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v8, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 4087
    .local v8, "imgDivider":Landroid/widget/ImageView;
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    .line 4088
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 4087
    invoke-direct {v7, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4089
    .local v7, "dividerParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 4090
    const/4 v0, 0x6

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4091
    const/16 v0, 0xe

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4092
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 4093
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 4094
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4095
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->lineDivider:Ljava/lang/String;

    invoke-virtual {v0, v8, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 4096
    invoke-virtual {v12, v8}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 4098
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOpacityPlusButton:Landroid/view/View;

    .line 4099
    new-instance v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 4100
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x42100000    # 36.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42100000    # 36.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 4099
    invoke-direct {v11, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4101
    .local v11, "plusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 4102
    const/4 v0, 0x1

    iput-boolean v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 4103
    const/16 v0, 0xb

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4104
    const/16 v0, 0x8

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4106
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    const/4 v1, 0x0

    .line 4107
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 4106
    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 4109
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOpacityPlusButton:Landroid/view/View;

    invoke-virtual {v0, v11}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4110
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOpacityPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_plus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 4112
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOpacityPlusButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgPressPath:Ljava/lang/String;

    .line 4113
    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgFocusPath:Ljava/lang/String;

    const/16 v5, 0x24

    const/16 v6, 0x24

    .line 4112
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 4115
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOpacityMinusButton:Landroid/view/View;

    .line 4117
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    .line 4118
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x42100000    # 36.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42100000    # 36.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 4117
    invoke-direct {v9, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4119
    .local v9, "minusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 4120
    const/4 v0, 0x1

    iput-boolean v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 4121
    const/16 v0, 0x9

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4122
    const/16 v0, 0x8

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4124
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    const/4 v1, 0x0

    .line 4125
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 4124
    invoke-virtual {v9, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 4127
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOpacityMinusButton:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4128
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOpacityMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_minus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 4130
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOpacityMinusButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgPressPath:Ljava/lang/String;

    .line 4131
    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgFocusPath:Ljava/lang/String;

    const/16 v5, 0x24

    const/16 v6, 0x24

    .line 4130
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 4133
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penAlphaSeekbar()Landroid/widget/SeekBar;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    .line 4134
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaTextView:Landroid/widget/TextView;

    .line 4135
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaTextView:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 4136
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaTextView:Landroid/widget/TextView;

    const/16 v1, 0x56

    const/16 v2, 0x57

    const/16 v3, 0x5b

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 4137
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41400000    # 12.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 4138
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x2

    .line 4139
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41500000    # 13.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 4138
    invoke-direct {v10, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4140
    .local v10, "penAlphaTextParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x4

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4141
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4142
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaTextView:Landroid/widget/TextView;

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 4143
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 4144
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOpacityPlusButton:Landroid/view/View;

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 4145
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOpacityMinusButton:Landroid/view/View;

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 4147
    return-object v12
.end method

.method private penAlphaSeekbar()Landroid/widget/SeekBar;
    .locals 15

    .prologue
    const/16 v7, 0xc

    const/4 v14, 0x3

    const/4 v13, 0x1

    const/high16 v6, 0x40000000    # 2.0f

    const/4 v12, 0x0

    .line 4157
    new-instance v10, Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v10, v3}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 4159
    .local v10, "seekBar":Landroid/widget/SeekBar;
    new-instance v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 4160
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43650000    # 229.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42040000    # 33.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 4159
    invoke-direct {v11, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4161
    .local v11, "seekBarParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 4162
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40a00000    # 5.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 4161
    invoke-virtual {v11, v3, v12, v12, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 4163
    iput-boolean v13, v11, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 4164
    const/16 v3, 0xe

    invoke-virtual {v11, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4165
    const/16 v3, 0x8

    invoke-virtual {v11, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4166
    invoke-virtual {v10, v11}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4167
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 4168
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 4167
    invoke-virtual {v10, v3, v12, v4, v12}, Landroid/widget/SeekBar;->setPadding(IIII)V

    .line 4170
    const/16 v3, 0x63

    invoke-virtual {v10, v3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 4171
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->handelPath:Ljava/lang/String;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->handelPressPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->handelFocusPath:Ljava/lang/String;

    const/16 v4, 0x21

    const/16 v5, 0x21

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 4172
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40400000    # 3.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v10, v3}, Landroid/widget/SeekBar;->setThumbOffset(I)V

    .line 4173
    new-instance v3, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v3}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    .line 4174
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v3, v12}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 4176
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40e00000    # 7.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 4177
    new-instance v8, Landroid/graphics/drawable/ClipDrawable;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v8, v3, v14, v13}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    .line 4179
    .local v8, "localClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->progressShadowPath:Ljava/lang/String;

    const/16 v5, 0xe5

    invoke-virtual {v3, v4, v5, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 4180
    .local v1, "localDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->progressAlphaPath:Ljava/lang/String;

    const/16 v5, 0xe5

    invoke-virtual {v3, v4, v5, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->alphaDrawable:Landroid/graphics/drawable/Drawable;

    .line 4181
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    move v2, v12

    move v3, v12

    move v4, v12

    move v5, v12

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 4182
    .local v0, "localInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    new-instance v2, Landroid/graphics/drawable/InsetDrawable;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->alphaDrawable:Landroid/graphics/drawable/Drawable;

    move v4, v12

    move v5, v12

    move v6, v12

    move v7, v12

    invoke-direct/range {v2 .. v7}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 4184
    .local v2, "alphaInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    new-instance v9, Landroid/graphics/drawable/LayerDrawable;

    new-array v3, v14, [Landroid/graphics/drawable/Drawable;

    aput-object v0, v3, v12

    aput-object v2, v3, v13

    const/4 v4, 0x2

    .line 4185
    aput-object v8, v3, v4

    .line 4184
    invoke-direct {v9, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 4186
    .local v9, "localLayerDrawable":Landroid/graphics/drawable/LayerDrawable;
    invoke-virtual {v10, v9}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 4188
    new-instance v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$53;

    invoke-direct {v3, p0, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$53;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Landroid/widget/SeekBar;)V

    invoke-virtual {v10, v3}, Landroid/widget/SeekBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 4206
    return-object v10
.end method

.method private penLayout()Landroid/view/ViewGroup;
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v10, -0x2

    const/4 v9, -0x1

    const/4 v8, 0x0

    .line 3190
    new-instance v5, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    .line 3191
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3193
    .local v2, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x42280000    # 42.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-virtual {v2, v8, v5, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 3194
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v5, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3195
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setVerticalFadingEdgeEnabled(Z)V

    .line 3196
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setFadingEdgeLength(I)V

    .line 3197
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v5, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 3198
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v5, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setOverScrollMode(I)V

    .line 3200
    new-instance v3, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;-><init>(Landroid/content/Context;)V

    .line 3201
    .local v3, "palletViewLayout":Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;
    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    .line 3202
    invoke-direct {v5, v9, v10}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 3201
    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3203
    invoke-virtual {v3, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->setOrientation(I)V

    .line 3204
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penTypeLayout()Landroid/view/ViewGroup;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeLayout:Landroid/view/ViewGroup;

    .line 3206
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->colorPaletteGradationLayout()Landroid/view/ViewGroup;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    .line 3207
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->beautifyEnableLayout()Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableLayout:Landroid/view/View;

    .line 3208
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->beautifyStyleBtnsLayout()Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnsLayout:Landroid/view/View;

    .line 3209
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penSeekbarLayout()Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSeekbarLayout:Landroid/view/View;

    .line 3210
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->typeSelectorlayout()Landroid/widget/LinearLayout;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    .line 3211
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 3212
    .local v0, "bottonPaddingLayout":Landroid/widget/LinearLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 3213
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x3f800000    # 1.0f

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 3212
    invoke-direct {v1, v9, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3214
    .local v1, "bottonPaddingLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3215
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 3217
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    .line 3218
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableLayout:Landroid/view/View;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    .line 3219
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnsLayout:Landroid/view/View;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    .line 3220
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    .line 3221
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    .line 3222
    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    .line 3224
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v5, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->addView(Landroid/view/View;)V

    .line 3225
    new-instance v4, Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 3226
    .local v4, "penLayout":Landroid/widget/RelativeLayout;
    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v9, v10}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3227
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 3228
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 3230
    return-object v4
.end method

.method private penSeekbarLayout()Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x41400000    # 12.0f

    .line 3611
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 3612
    .local v1, "localLinearLayout":Landroid/widget/LinearLayout;
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    .line 3613
    const/4 v3, -0x2

    .line 3612
    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3617
    .local v0, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3618
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 3619
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 3618
    invoke-virtual {v1, v2, v5, v3, v5}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 3621
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 3623
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penAlphaLayout()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarView:Landroid/view/View;

    .line 3624
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penSizeLayout()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarView:Landroid/view/View;

    .line 3625
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->beautifyAdvanceSettingSeekbars()Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSettingLayout:Landroid/view/View;

    .line 3626
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3627
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3628
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSettingLayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3629
    return-object v1
.end method

.method private penSelectIndex(I)V
    .locals 6
    .param p1, "index"    # I

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3709
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginCount:I

    if-lt v0, v1, :cond_0

    .line 3762
    return-void

    .line 3712
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    const-string v2, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v1

    if-ne p1, v1, :cond_2

    .line 3713
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 3715
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 3718
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->previousPenMagicSelected:Z

    if-nez v1, :cond_1

    .line 3719
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->currentOrtherPenHeight:I

    .line 3722
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->currenMagicPenHeight:I

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setMagicPenMode(I)V

    .line 3723
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->previousPenMagicSelected:Z

    .line 3726
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->alphaDrawable:Landroid/graphics/drawable/Drawable;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlpha:I

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 3751
    :goto_1
    if-ne p1, v0, :cond_6

    .line 3752
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setSelected(Z)V

    .line 3753
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->performClick()Z

    .line 3709
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3728
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 3731
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->previousPenMagicSelected:Z

    if-eqz v1, :cond_3

    .line 3732
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->currenMagicPenHeight:I

    .line 3735
    :cond_3
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMagicPenEnable:Z

    .line 3737
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->alphaDrawable:Landroid/graphics/drawable/Drawable;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 3739
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 3741
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->currentOrtherPenHeight:I

    const v2, 0x1869f

    if-ne v1, v2, :cond_5

    .line 3742
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x43d60000    # 428.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    .line 3743
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->currentOrtherPenHeight:I

    .line 3748
    :cond_4
    :goto_3
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->previousPenMagicSelected:Z

    goto :goto_1

    .line 3744
    :cond_5
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->previousPenMagicSelected:Z

    if-eqz v1, :cond_4

    .line 3745
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->currentOrtherPenHeight:I

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto :goto_3

    .line 3755
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setSelected(Z)V

    goto :goto_2
.end method

.method private penSelection(Landroid/view/View;)V
    .locals 12
    .param p1, "selectedPen"    # Landroid/view/View;

    .prologue
    .line 5778
    const/4 v2, 0x0

    .line 5779
    .local v2, "isBeautifyPen":Z
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-nez v7, :cond_1

    .line 5932
    :cond_0
    :goto_0
    return-void

    .line 5782
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    const-string v8, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v0

    .line 5783
    .local v0, "chinesePenIndex":I
    const/4 v7, -0x1

    if-eq v0, v7, :cond_0

    .line 5787
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginCount:I

    if-lt v1, v7, :cond_2

    .line 5929
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->drawExpendImage(Ljava/lang/String;)V

    .line 5930
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->playScrollAnimationChoosePen()V

    .line 5931
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 5788
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-le v7, v1, :cond_3

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_4

    :cond_3
    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isBeautifyPen(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 5789
    :cond_4
    if-eqz v2, :cond_7

    .line 5790
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setSelected(Z)V

    .line 5796
    :goto_2
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-le v7, v1, :cond_6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 5797
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    .line 5798
    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    if-ne v0, v7, :cond_5

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    invoke-virtual {v7}, Landroid/widget/Switch;->isChecked()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 5799
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    const-string v8, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    .line 5801
    :cond_5
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 5803
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v7

    if-nez v7, :cond_8

    .line 5804
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v9, v9, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->loadPenPlugin(Landroid/content/Context;Ljava/lang/String;)V

    .line 5805
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v7

    if-nez v7, :cond_8

    .line 5787
    :cond_6
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 5792
    :cond_7
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setSelected(Z)V

    goto :goto_2

    .line 5810
    :cond_8
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenDataList:Ljava/util/List;

    if-eqz v7, :cond_11

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenDataList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v7, v8, :cond_11

    .line 5811
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->isLoaded()Z

    move-result v7

    if-nez v7, :cond_11

    .line 5812
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_11

    .line 5813
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenDataList:Ljava/util/List;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iput v7, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 5814
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenDataList:Ljava/util/List;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-boolean v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    iput-boolean v7, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    .line 5815
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenDataList:Ljava/util/List;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iput v7, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 5816
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenDataList:Ljava/util/List;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    iput-object v7, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    .line 5828
    :cond_9
    :goto_4
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->isLoaded()Z

    move-result v7

    if-nez v7, :cond_a

    .line 5829
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->setLoaded(Z)V

    .line 5832
    :cond_a
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v7, :cond_13

    .line 5833
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v7}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v8}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v8

    if-ge v7, v8, :cond_12

    .line 5834
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v7}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v7

    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    .line 5838
    :goto_5
    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    if-nez v7, :cond_b

    .line 5839
    const/16 v7, 0x438

    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    .line 5845
    :cond_b
    :goto_6
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v7

    invoke-interface {v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getMinSettingValue()F

    move-result v4

    .line 5846
    .local v4, "min":F
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v7

    invoke-interface {v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getMaxSettingValue()F

    move-result v3

    .line 5848
    .local v3, "max":F
    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    int-to-float v7, v7

    mul-float/2addr v7, v3

    float-to-double v8, v7

    const-wide v10, 0x4076800000000000L    # 360.0

    div-double/2addr v8, v10

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    float-to-double v10, v7

    cmpg-double v7, v8, v10

    if-gez v7, :cond_c

    .line 5849
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    int-to-float v8, v8

    mul-float/2addr v8, v3

    float-to-double v8, v8

    const-wide v10, 0x4076800000000000L    # 360.0

    div-double/2addr v8, v10

    double-to-float v8, v8

    iput v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 5850
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-interface {v7, v8}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setSize(F)V

    .line 5852
    :cond_c
    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    int-to-float v7, v7

    mul-float/2addr v7, v4

    float-to-double v8, v7

    const-wide v10, 0x4076800000000000L    # 360.0

    div-double/2addr v8, v10

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    float-to-double v10, v7

    cmpl-double v7, v8, v10

    if-lez v7, :cond_d

    .line 5853
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    int-to-float v8, v8

    mul-float/2addr v8, v4

    float-to-double v8, v8

    const-wide v10, 0x4076800000000000L    # 360.0

    div-double/2addr v8, v10

    double-to-float v8, v8

    iput v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 5854
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-interface {v7, v8}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setSize(F)V

    .line 5856
    :cond_d
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-interface {v7, v8}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setColor(I)V

    .line 5858
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v7

    .line 5859
    const/4 v8, 0x4

    invoke-interface {v7, v8}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 5860
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v7

    .line 5861
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-interface {v7, v8}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setAdvancedSetting(Ljava/lang/String;)V

    .line 5862
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setBeautifyAdvanceStringToCurrentAdvanceData(Ljava/lang/String;)V

    .line 5865
    :cond_e
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v7, :cond_f

    .line 5866
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-interface {v7, v8}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 5869
    :cond_f
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v6, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 5870
    .local v6, "tempSize":F
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    sub-float v8, v3, v4

    const/high16 v9, 0x41200000    # 10.0f

    mul-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/SeekBar;->setMax(I)V

    .line 5871
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iput v6, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 5873
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    const/high16 v8, 0x43b40000    # 360.0f

    mul-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    sub-float/2addr v7, v4

    const/high16 v8, 0x41200000    # 10.0f

    mul-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 5877
    .local v5, "progress":I
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 5879
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v7

    .line 5880
    const/4 v8, 0x1

    invoke-interface {v7, v8}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v7

    if-eqz v7, :cond_14

    .line 5881
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    shr-int/lit8 v7, v7, 0x18

    and-int/lit16 v7, v7, 0xff

    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlpha:I

    .line 5883
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlpha:I

    int-to-float v8, v8

    const/high16 v9, 0x437f0000    # 255.0f

    div-float/2addr v8, v9

    const/high16 v9, 0x42c60000    # 99.0f

    mul-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 5884
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v7, v8}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 5886
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    const v9, 0xffffff

    and-int/2addr v8, v9

    const/high16 v9, -0x1000000

    or-int/2addr v8, v9

    invoke-virtual {v7, v8}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 5896
    :goto_7
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    const/16 v8, 0xff

    invoke-virtual {v7, v8}, Landroid/graphics/drawable/GradientDrawable;->setAlpha(I)V

    .line 5898
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setColor(I)V

    .line 5900
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setPenType(Ljava/lang/String;)V

    .line 5901
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeSize(F)V

    .line 5902
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeColor(I)V

    .line 5903
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeAdvancedSetting(Ljava/lang/String;)V

    .line 5905
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->invalidate()V

    .line 5907
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isBeautifyPen(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_15

    .line 5908
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v7

    .line 5909
    const/4 v8, 0x4

    invoke-interface {v7, v8}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v7

    if-eqz v7, :cond_15

    .line 5910
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mAdvancedSettingButton:Landroid/view/View;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 5917
    :goto_8
    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    if-nez v7, :cond_10

    .line 5918
    const/16 v7, 0xa

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v8}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v8

    int-to-float v8, v8

    const/4 v9, 0x0

    invoke-direct {p0, v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->playScrollAnimation(IFF)V

    .line 5920
    :cond_10
    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    const/4 v8, 0x5

    if-ne v7, v8, :cond_6

    .line 5921
    const/16 v7, 0xa

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v8}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v8

    int-to-float v8, v8

    .line 5922
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v10, 0x425c0000    # 55.0f

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v9

    int-to-float v9, v9

    .line 5921
    invoke-direct {p0, v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->playScrollAnimation(IFF)V

    goto/16 :goto_3

    .line 5818
    .end local v3    # "max":F
    .end local v4    # "min":F
    .end local v5    # "progress":I
    .end local v6    # "tempSize":F
    :cond_11
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v7

    invoke-interface {v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getColor()I

    move-result v7

    iput v7, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 5819
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v7

    invoke-interface {v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getSize()F

    move-result v7

    iput v7, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 5821
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v7

    .line 5822
    const/4 v8, 0x4

    invoke-interface {v7, v8}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 5823
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v7

    .line 5824
    invoke-interface {v7}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getAdvancedSetting()Ljava/lang/String;

    move-result-object v7

    .line 5823
    iput-object v7, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    goto/16 :goto_4

    .line 5836
    :cond_12
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v7}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v7

    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    goto/16 :goto_5

    .line 5842
    :cond_13
    const/16 v7, 0x438

    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    goto/16 :goto_6

    .line 5888
    .restart local v3    # "max":F
    .restart local v4    # "min":F
    .restart local v5    # "progress":I
    .restart local v6    # "tempSize":F
    :cond_14
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v7, v8}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 5890
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v7, v8}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 5891
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7}, Landroid/widget/SeekBar;->invalidate()V

    .line 5892
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7}, Landroid/widget/SeekBar;->invalidate()V

    .line 5893
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7}, Landroid/widget/SeekBar;->invalidate()V

    .line 5894
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7}, Landroid/widget/SeekBar;->invalidate()V

    goto/16 :goto_7

    .line 5912
    :cond_15
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mAdvancedSettingButton:Landroid/view/View;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_8
.end method

.method private penSizeLayout()Landroid/view/ViewGroup;
    .locals 14

    .prologue
    .line 3918
    new-instance v12, Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v12, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 3920
    .local v12, "sizeDisplayLayout":Landroid/widget/RelativeLayout;
    new-instance v13, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    .line 3921
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42780000    # 62.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 3920
    invoke-direct {v13, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3922
    .local v13, "sizeDisplayLayoutParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v12, v13}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3923
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 3927
    new-instance v8, Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v8, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 3928
    .local v8, "imgDivider":Landroid/widget/ImageView;
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    .line 3929
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 3928
    invoke-direct {v7, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3930
    .local v7, "dividerParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 3931
    const/4 v0, 0x6

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3932
    const/16 v0, 0xe

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3933
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3934
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 3935
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3936
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->lineDivider:Ljava/lang/String;

    invoke-virtual {v0, v8, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 3937
    invoke-virtual {v12, v8}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 3939
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizePlusButton:Landroid/view/View;

    .line 3940
    new-instance v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3941
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x42100000    # 36.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42100000    # 36.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 3940
    invoke-direct {v11, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3942
    .local v11, "plusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 3943
    const/4 v0, 0x1

    iput-boolean v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 3944
    const/16 v0, 0xb

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3945
    const/16 v0, 0x8

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3947
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    const/4 v1, 0x0

    .line 3948
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 3947
    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 3950
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizePlusButton:Landroid/view/View;

    invoke-virtual {v0, v11}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3951
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizePlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_plus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3953
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizePlusButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgPressPath:Ljava/lang/String;

    .line 3954
    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->plusBgFocusPath:Ljava/lang/String;

    const/16 v5, 0x24

    const/16 v6, 0x24

    .line 3953
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 3956
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeMinusButton:Landroid/view/View;

    .line 3957
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3958
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x42100000    # 36.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42100000    # 36.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 3957
    invoke-direct {v9, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3959
    .local v9, "minusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 3960
    const/4 v0, 0x1

    iput-boolean v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 3961
    const/16 v0, 0x9

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3962
    const/16 v0, 0x8

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3964
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    const/4 v1, 0x0

    .line 3965
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 3964
    invoke-virtual {v9, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 3967
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeMinusButton:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3968
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_minus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3970
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeMinusButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgPressPath:Ljava/lang/String;

    .line 3971
    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->minusBgFocusPath:Ljava/lang/String;

    const/16 v5, 0x24

    const/16 v6, 0x24

    .line 3970
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 3973
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penSizeSeekbar()Landroid/widget/SeekBar;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    .line 3975
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeTextView:Landroid/widget/TextView;

    .line 3976
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeTextView:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 3977
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeTextView:Landroid/widget/TextView;

    const/16 v1, 0x56

    const/16 v2, 0x57

    const/16 v3, 0x5b

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 3978
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41400000    # 12.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 3979
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x2

    .line 3980
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41500000    # 13.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 3979
    invoke-direct {v10, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3981
    .local v10, "penAlphaTextParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x4

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3982
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3983
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 3985
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 3986
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizePlusButton:Landroid/view/View;

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 3987
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeMinusButton:Landroid/view/View;

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 3989
    return-object v12
.end method

.method private penSizeSeekbar()Landroid/widget/SeekBar;
    .locals 13

    .prologue
    .line 4017
    new-instance v10, Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v10, v3}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 4019
    .local v10, "seekBar":Landroid/widget/SeekBar;
    new-instance v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 4020
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43650000    # 229.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42040000    # 33.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 4019
    invoke-direct {v11, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4021
    .local v11, "seekBarParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 4022
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x40a00000    # 5.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 4021
    invoke-virtual {v11, v3, v4, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 4024
    const/4 v3, 0x1

    iput-boolean v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 4025
    const/16 v3, 0xe

    invoke-virtual {v11, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4026
    const/16 v3, 0x8

    invoke-virtual {v11, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4028
    invoke-virtual {v10, v11}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4029
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    const/4 v4, 0x0

    .line 4030
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x40000000    # 2.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    const/4 v6, 0x0

    .line 4029
    invoke-virtual {v10, v3, v4, v5, v6}, Landroid/widget/SeekBar;->setPadding(IIII)V

    .line 4032
    const/16 v3, 0x63

    invoke-virtual {v10, v3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 4033
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->handelPath:Ljava/lang/String;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->handelPressPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->handelFocusPath:Ljava/lang/String;

    const/16 v4, 0x21

    const/16 v5, 0x21

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 4034
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40400000    # 3.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v10, v3}, Landroid/widget/SeekBar;->setThumbOffset(I)V

    .line 4035
    new-instance v3, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v3}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    .line 4036
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 4038
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40e00000    # 7.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 4039
    new-instance v8, Landroid/graphics/drawable/ClipDrawable;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    const/4 v4, 0x3

    const/4 v5, 0x1

    invoke-direct {v8, v3, v4, v5}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    .line 4040
    .local v8, "localClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->progressBgPath:Ljava/lang/String;

    const/16 v5, 0xe5

    const/16 v6, 0xc

    invoke-virtual {v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 4041
    .local v1, "bgDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->progressShadowPath:Ljava/lang/String;

    const/16 v5, 0xe5

    const/16 v6, 0xc

    invoke-virtual {v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    .line 4042
    .local v12, "shadowDrawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 4043
    .local v0, "bgInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    new-instance v2, Landroid/graphics/drawable/InsetDrawable;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, v12

    invoke-direct/range {v2 .. v7}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 4045
    .local v2, "shadowInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    new-instance v9, Landroid/graphics/drawable/LayerDrawable;

    const/4 v3, 0x3

    new-array v3, v3, [Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object v2, v3, v4

    const/4 v4, 0x2

    .line 4046
    aput-object v8, v3, v4

    .line 4045
    invoke-direct {v9, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 4047
    .local v9, "localLayerDrawable":Landroid/graphics/drawable/LayerDrawable;
    invoke-virtual {v10, v9}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 4049
    new-instance v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$52;

    invoke-direct {v3, p0, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$52;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Landroid/widget/SeekBar;)V

    invoke-virtual {v10, v3}, Landroid/widget/SeekBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 4067
    return-object v10
.end method

.method private penTypeButton()Landroid/view/View;
    .locals 17

    .prologue
    .line 3021
    new-instance v11, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v11, v13}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 3022
    .local v11, "penButtonLayout":Landroid/widget/LinearLayout;
    new-instance v10, Landroid/widget/LinearLayout$LayoutParams;

    .line 3023
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v14, 0x43a48000    # 329.0f

    invoke-virtual {v13, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v13

    .line 3024
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v15, 0x42280000    # 42.0f

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    .line 3022
    invoke-direct {v10, v13, v14}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3029
    .local v10, "penBtnLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v11, v10}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3030
    const/4 v13, 0x1

    invoke-virtual {v11, v13}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 3033
    new-instance v8, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v8, v13}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 3035
    .local v8, "mPenButton":Landroid/widget/TextView;
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v13, -0x1

    .line 3036
    const/4 v14, -0x2

    .line 3035
    invoke-direct {v7, v13, v14}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3037
    .local v7, "fontButtonParams":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v13, 0x3f800000    # 1.0f

    iput v13, v7, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 3040
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3041
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v14, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->tabLinePath:Ljava/lang/String;

    sget-object v15, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->tabLineSelectPath:Ljava/lang/String;

    sget-object v16, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->tabLineFocusPath:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v13, v8, v14, v15, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3043
    invoke-virtual {v8}, Landroid/widget/TextView;->setSingleLine()V

    .line 3044
    const/16 v13, 0x11

    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setGravity(I)V

    .line 3045
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v14, "string_pen"

    invoke-virtual {v13, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3050
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v15, 0x41980000    # 19.0f

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    int-to-float v14, v14

    invoke-virtual {v8, v13, v14}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 3051
    sget-object v13, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v14, 0x1

    invoke-virtual {v8, v13, v14}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 3053
    new-instance v5, Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v5, v13}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 3054
    .local v5, "bottomView":Landroid/view/View;
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v13, -0x1

    .line 3055
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v15, 0x40000000    # 2.0f

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    .line 3054
    invoke-direct {v6, v13, v14}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3057
    .local v6, "bottomViewParam":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v14, 0x41300000    # 11.0f

    invoke-virtual {v13, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v13

    iput v13, v6, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 3058
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v14, 0x41700000    # 15.0f

    invoke-virtual {v13, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v13

    iput v13, v6, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 3059
    invoke-virtual {v5, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3060
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v14, "vienna_subtitle_line"

    invoke-virtual {v13, v5, v14}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 3064
    new-instance v13, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v15, "string_pen_tab"

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3065
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v15, "string_selected"

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 3064
    invoke-virtual {v11, v13}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3068
    const/4 v13, 0x3

    new-array v1, v13, [[I

    .line 3069
    .local v1, "arrayOfStates":[[I
    const/4 v13, 0x2

    new-array v2, v13, [I

    .line 3070
    .local v2, "arrayOfStates1":[I
    const/4 v13, 0x0

    const v14, -0x10100a7

    aput v14, v2, v13

    .line 3071
    const/4 v13, 0x1

    const v14, -0x10100a1

    aput v14, v2, v13

    .line 3072
    const/4 v13, 0x0

    aput-object v2, v1, v13

    .line 3073
    const/4 v13, 0x1

    new-array v3, v13, [I

    .line 3074
    .local v3, "arrayOfStates2":[I
    const/4 v13, 0x0

    const v14, 0x10100a7

    aput v14, v3, v13

    .line 3075
    const/4 v13, 0x1

    aput-object v3, v1, v13

    .line 3076
    const/4 v13, 0x1

    new-array v4, v13, [I

    .line 3077
    .local v4, "arrayOfStates3":[I
    const/4 v13, 0x0

    const v14, 0x10100a1

    aput v14, v4, v13

    .line 3078
    const/4 v13, 0x2

    aput-object v4, v1, v13

    .line 3079
    const/4 v13, 0x3

    new-array v12, v13, [I

    .line 3080
    .local v12, "textColor":[I
    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Landroid/graphics/Color;->rgb(III)I

    move-result v14

    aput v14, v12, v13

    .line 3084
    const/4 v13, 0x1

    const/4 v14, 0x1

    const/16 v15, 0x8b

    const/16 v16, 0xd2

    invoke-static/range {v14 .. v16}, Landroid/graphics/Color;->rgb(III)I

    move-result v14

    aput v14, v12, v13

    .line 3085
    const/4 v13, 0x2

    const/4 v14, 0x1

    const/16 v15, 0x8b

    const/16 v16, 0xd2

    invoke-static/range {v14 .. v16}, Landroid/graphics/Color;->rgb(III)I

    move-result v14

    aput v14, v12, v13

    .line 3087
    new-instance v9, Landroid/content/res/ColorStateList;

    invoke-direct {v9, v1, v12}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 3088
    .local v9, "mTextColorStateList":Landroid/content/res/ColorStateList;
    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 3089
    const/4 v13, 0x1

    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setClickable(Z)V

    .line 3090
    const/4 v13, 0x1

    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 3091
    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v8, v13, v14, v15, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 3092
    const/4 v13, 0x0

    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 3093
    invoke-virtual {v11, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3094
    invoke-virtual {v11, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3095
    return-object v11
.end method

.method private penTypeLayout()Landroid/view/ViewGroup;
    .locals 23

    .prologue
    .line 3771
    new-instance v2, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v2, v7}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penTypeLayout:Landroid/widget/RelativeLayout;

    .line 3776
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3777
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x43c80000    # 400.0f

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x42c20000    # 97.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 3776
    invoke-direct {v12, v2, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3780
    .local v12, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penTypeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v12}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3785
    new-instance v2, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v2, v7}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    .line 3786
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    invoke-virtual {v2, v12}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3789
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginInfoList()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    .line 3790
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 3791
    .local v11, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;>;"
    const/4 v10, 0x0

    .line 3793
    .local v10, "i":I
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 3866
    new-instance v15, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v15, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 3869
    .local v15, "penTouchView":Landroid/widget/ImageButton;
    new-instance v16, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    .line 3870
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x42b20000    # 89.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 3869
    move-object/from16 v0, v16

    invoke-direct {v0, v2, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3872
    .local v16, "penTouchViewParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 3873
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    .line 3874
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v15, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 3875
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v15, v2}, Landroid/widget/ImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 3876
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penTypeLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 3878
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penTypeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 3889
    new-instance v2, Landroid/widget/HorizontalScrollView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v2, v7}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    .line 3890
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    invoke-virtual {v2, v7}, Landroid/widget/HorizontalScrollView;->addView(Landroid/view/View;)V

    .line 3891
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Landroid/widget/HorizontalScrollView;->setFadingEdgeLength(I)V

    .line 3892
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Landroid/widget/HorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 3893
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v21, v0

    const/high16 v22, 0x41000000    # 8.0f

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v2, v7, v8, v0, v1}, Landroid/widget/HorizontalScrollView;->setPadding(IIII)V

    .line 3894
    new-instance v2, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v2, v7}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView3:Landroid/view/ViewGroup;

    .line 3896
    new-instance v13, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3897
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v7, 0x43a48000    # 329.0f

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 3898
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x42c20000    # 97.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 3896
    invoke-direct {v13, v2, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3900
    .local v13, "layoutParamsNew":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView3:Landroid/view/ViewGroup;

    invoke-virtual {v2, v13}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3903
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView3:Landroid/view/ViewGroup;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v2, v7, v8, v0, v1}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 3904
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->preview()Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviewLayout:Landroid/view/View;

    .line 3905
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView3:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 3906
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView3:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v2, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 3908
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView3:Landroid/view/ViewGroup;

    return-object v2

    .line 3796
    .end local v13    # "layoutParamsNew":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v15    # "penTouchView":Landroid/widget/ImageButton;
    .end local v16    # "penTouchViewParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    .line 3798
    .local v14, "penPluginInfo":Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;
    new-instance v3, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v3, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 3800
    .local v3, "penButton":Landroid/widget/ImageButton;
    invoke-virtual {v14}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v2

    iget-object v4, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->iconImageUri:Ljava/lang/String;

    .line 3801
    .local v4, "localUnselectImage":Ljava/lang/String;
    invoke-virtual {v14}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v2

    iget-object v5, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->selectedIconImageUri:Ljava/lang/String;

    .line 3802
    .local v5, "localSelectImage":Ljava/lang/String;
    invoke-virtual {v14}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v2

    iget-object v6, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->focusedIconImageUri:Ljava/lang/String;

    .line 3804
    .local v6, "localFocusImage":Ljava/lang/String;
    new-instance v19, Ljava/lang/String;

    const-string v2, "iconImageUri"

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 3805
    .local v19, "unselectImage":Ljava/lang/String;
    new-instance v18, Ljava/lang/String;

    const-string v2, "selectedIconImageURI"

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 3806
    .local v18, "selectImage":Ljava/lang/String;
    new-instance v9, Ljava/lang/String;

    const-string/jumbo v2, "uriInfo"

    invoke-direct {v9, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 3808
    .local v9, "focusImage":Ljava/lang/String;
    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3809
    const-string/jumbo v4, "snote_popup_pensetting_brush"

    .line 3810
    const/16 v19, 0x0

    .line 3813
    :cond_1
    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3814
    const-string/jumbo v5, "snote_popup_pensetting_brush_select"

    .line 3815
    const/16 v18, 0x0

    .line 3818
    :cond_2
    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3819
    const-string/jumbo v6, "snote_popup_pensetting_brush_focus"

    .line 3820
    const/4 v9, 0x0

    .line 3823
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 3824
    const/16 v7, 0x59

    const/16 v8, 0x59

    .line 3823
    invoke-virtual/range {v2 .. v8}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 3826
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penTypeLayout:Landroid/widget/RelativeLayout;

    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 3828
    const/4 v2, 0x1

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 3829
    add-int/lit16 v2, v10, 0x3e8

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setId(I)V

    .line 3830
    invoke-virtual {v14}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    const-string v7, "Brush"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 3831
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v7, "string_brush"

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3844
    :cond_4
    :goto_1
    invoke-virtual {v14}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isBeautifyPen(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 3845
    const/16 v2, 0x8

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3847
    :cond_5
    new-instance v17, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3848
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x42b20000    # 89.0f

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x42b20000    # 89.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 3847
    move-object/from16 v0, v17

    invoke-direct {v0, v2, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3849
    .local v17, "relativeParams":Landroid/widget/RelativeLayout$LayoutParams;
    if-lez v10, :cond_b

    .line 3850
    const/4 v2, 0x1

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getId()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 3852
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, -0x3e2c0000    # -26.5f

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    move-object/from16 v0, v17

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 3854
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penTypeLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v0, v17

    invoke-virtual {v2, v3, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 3862
    :goto_2
    add-int/lit8 v10, v10, 0x1

    .line 3864
    goto/16 :goto_0

    .line 3832
    .end local v17    # "relativeParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_6
    invoke-virtual {v14}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    const-string v7, "ChineseBrush"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 3833
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v7, "string_chinese_brush"

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 3834
    :cond_7
    invoke-virtual {v14}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    const-string v7, "InkPen"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 3835
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v7, "string_pen"

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 3836
    :cond_8
    invoke-virtual {v14}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    const-string v7, "Marker"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 3837
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v7, "string_marker"

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 3838
    :cond_9
    invoke-virtual {v14}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    const-string v7, "Pencil"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 3839
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v7, "string_pencil"

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 3840
    :cond_a
    invoke-virtual {v14}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    const-string v7, "MagicPen"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3841
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v7, "string_magic_pen"

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 3857
    .restart local v17    # "relativeParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_b
    const/16 v2, 0x9

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3858
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x40c00000    # 6.0f

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    move-object/from16 v0, v17

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 3859
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penTypeLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v0, v17

    invoke-virtual {v2, v3, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_2
.end method

.method private playScrollAnimation(IFF)V
    .locals 9
    .param p1, "delay"    # I
    .param p2, "from"    # F
    .param p3, "to"    # F

    .prologue
    .line 6706
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimerForScrollPen:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 6707
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimerForScrollPen:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 6709
    :cond_0
    move v6, p2

    .line 6710
    .local v6, "fromFinal":F
    move v8, p3

    .line 6711
    .local v8, "toFinal":F
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    int-to-float v7, v0

    .line 6712
    .local v7, "step":F
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimerForScrollPen:Ljava/util/Timer;

    .line 6713
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCountForScrollPen:I

    .line 6714
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimerForScrollPen:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    invoke-direct {v1, p0, v6, v8, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;FFF)V

    .line 6750
    const-wide/16 v2, 0xa

    int-to-long v4, p1

    .line 6714
    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 6751
    return-void
.end method

.method private playScrollAnimationChoosePen()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 6689
    const/4 v0, 0x0

    .line 6690
    .local v0, "bodyLayoutHeight":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    .line 6691
    const/16 v0, 0x16e

    .line 6700
    :goto_0
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getHeight()I

    move-result v2

    .line 6701
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    add-int/lit8 v4, v0, 0x29

    add-int/lit16 v4, v4, 0xc8

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 6700
    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->playScrollAnimationForBottomBar(III)V

    .line 6702
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    .line 6703
    return-void

    .line 6692
    :cond_0
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMagicPenEnable:Z

    if-eqz v1, :cond_1

    .line 6693
    const/16 v0, 0x10b

    .line 6694
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 6695
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    goto :goto_0

    .line 6697
    :cond_1
    const/16 v0, 0x1ac

    goto :goto_0
.end method

.method private playScrollAnimationForBottomBar(III)V
    .locals 8
    .param p1, "delay"    # I
    .param p2, "from"    # I
    .param p3, "to"    # I

    .prologue
    .line 6755
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 6756
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 6757
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimer:Ljava/util/Timer;

    .line 6759
    :cond_0
    move v6, p2

    .line 6760
    .local v6, "fromFinal":I
    move v7, p3

    .line 6761
    .local v7, "toFinal":I
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimer:Ljava/util/Timer;

    .line 6762
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCount:I

    .line 6763
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimer:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;

    invoke-direct {v1, p0, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$56;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;II)V

    .line 6799
    const-wide/16 v2, 0xa

    int-to-long v4, p1

    .line 6763
    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 6800
    return-void
.end method

.method private popupMaxButton()Landroid/view/View;
    .locals 8

    .prologue
    const/16 v5, 0x2a

    const/high16 v3, 0x42280000    # 42.0f

    .line 2805
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 2806
    .local v1, "popupMaxButton":Landroid/widget/ImageButton;
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2807
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 2806
    invoke-direct {v7, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2808
    .local v7, "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v0, 0xb

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2810
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2811
    invoke-virtual {v1, v7}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2812
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2813
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_close"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2815
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->popupMaxPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->popupMaxPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->popupMaxPath:Ljava/lang/String;

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 2818
    return-object v1
.end method

.method private popupMinButton()Landroid/view/View;
    .locals 8

    .prologue
    const/16 v5, 0x2a

    const/high16 v3, 0x42280000    # 42.0f

    .line 2825
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 2826
    .local v1, "popupMinButton":Landroid/widget/ImageButton;
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2827
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 2826
    invoke-direct {v7, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2828
    .local v7, "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v0, 0xb

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2830
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2831
    invoke-virtual {v1, v7}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2832
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2833
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_close"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2835
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->popupMinPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->popupMinPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->popupMinPath:Ljava/lang/String;

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 2838
    return-object v1
.end method

.method private presetAddButton()Landroid/view/View;
    .locals 8

    .prologue
    const/16 v5, 0x20

    const/high16 v3, 0x42280000    # 42.0f

    .line 2846
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 2847
    .local v1, "presetButton":Landroid/widget/ImageButton;
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2848
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 2847
    invoke-direct {v7, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2849
    .local v7, "presetButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v0, 0xb

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2853
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2855
    invoke-virtual {v1, v7}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2856
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2857
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_add_preset"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2859
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->presetAddPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->presetAddPressPath:Ljava/lang/String;

    .line 2860
    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->presetAddFocusPath:Ljava/lang/String;

    move v6, v5

    .line 2859
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 2861
    return-object v1
.end method

.method private presetDisplay()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 5401
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    if-nez v0, :cond_0

    .line 5414
    :goto_0
    return-void

    .line 5405
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 5406
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setVisibility(I)V

    .line 5407
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 5410
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVisibility(I)V

    .line 5411
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private presetLayout()Landroid/widget/LinearLayout;
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/16 v10, 0x8b

    const/4 v9, -0x1

    const/4 v8, 0x0

    .line 3239
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 3240
    .local v1, "presetLayout":Landroid/widget/LinearLayout;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41900000    # 18.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 3241
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42280000    # 42.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41700000    # 15.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 3242
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x40a00000    # 5.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 3240
    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 3244
    new-instance v3, Landroid/widget/GridView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/GridView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    .line 3245
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v9, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3248
    .local v0, "gridViewParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v3, v0}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3249
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v3, v8}, Landroid/widget/GridView;->setBackgroundColor(I)V

    .line 3250
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v3, v8}, Landroid/widget/GridView;->setCacheColorHint(I)V

    .line 3251
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setVerticalScrollBarEnabled(Z)V

    .line 3252
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v3, v8}, Landroid/widget/GridView;->setHorizontalScrollBarEnabled(Z)V

    .line 3253
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    .line 3254
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setHorizontalSpacing(I)V

    .line 3255
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v3, v8}, Landroid/widget/GridView;->setFocusable(Z)V

    .line 3256
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 3258
    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetTextView:Landroid/widget/TextView;

    .line 3259
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 3260
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v4, 0x438d8000    # 283.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 3259
    invoke-direct {v2, v3, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3263
    .local v2, "textViewParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41000000    # 8.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v2, v3, v8, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 3265
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3266
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetTextView:Landroid/widget/TextView;

    invoke-static {v10, v10, v10}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 3267
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41400000    # 12.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v8, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 3268
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetTextView:Landroid/widget/TextView;

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 3271
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v5, "string_no_preset"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3272
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 3273
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetTextView:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3275
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v5, "string_no_preset"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3276
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetTextView:Landroid/widget/TextView;

    .line 3279
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42c80000    # 100.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 3276
    invoke-virtual {v3, v8, v4, v8, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 3281
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3282
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3284
    return-object v1
.end method

.method private presetTypeButton()Landroid/view/View;
    .locals 17

    .prologue
    .line 3105
    new-instance v9, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v9, v13}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 3106
    .local v9, "presetTypeButtonLayout":Landroid/widget/LinearLayout;
    new-instance v10, Landroid/widget/LinearLayout$LayoutParams;

    .line 3107
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v14, 0x43a48000    # 329.0f

    invoke-virtual {v13, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v13

    .line 3108
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v15, 0x42280000    # 42.0f

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    .line 3106
    invoke-direct {v10, v13, v14}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3112
    .local v10, "presetTypeButtonLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3113
    const/4 v13, 0x1

    invoke-virtual {v9, v13}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 3116
    new-instance v7, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v7, v13}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 3118
    .local v7, "mPresetButton":Landroid/widget/TextView;
    new-instance v11, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v13, -0x1

    .line 3119
    const/4 v14, -0x2

    .line 3118
    invoke-direct {v11, v13, v14}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3120
    .local v11, "presetTypeButtonParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3121
    const/high16 v13, 0x3f800000    # 1.0f

    iput v13, v11, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 3123
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v14, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->tabLinePath:Ljava/lang/String;

    sget-object v15, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->tabLineSelectPath:Ljava/lang/String;

    .line 3124
    sget-object v16, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->tabLineSelectPath:Ljava/lang/String;

    .line 3123
    move-object/from16 v0, v16

    invoke-virtual {v13, v7, v14, v15, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3126
    invoke-virtual {v7}, Landroid/widget/TextView;->setSingleLine()V

    .line 3129
    const/16 v13, 0x11

    invoke-virtual {v7, v13}, Landroid/widget/TextView;->setGravity(I)V

    .line 3131
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v14, "string_preset"

    invoke-virtual {v13, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3136
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v15, 0x41980000    # 19.0f

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    int-to-float v14, v14

    invoke-virtual {v7, v13, v14}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 3141
    new-instance v5, Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v5, v13}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 3142
    .local v5, "bottomView":Landroid/view/View;
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v13, -0x1

    .line 3143
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v15, 0x40000000    # 2.0f

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    .line 3142
    invoke-direct {v6, v13, v14}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3145
    .local v6, "bottomViewParam":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v14, 0x41400000    # 12.0f

    invoke-virtual {v13, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v13

    iput v13, v6, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 3146
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v14, 0x41700000    # 15.0f

    invoke-virtual {v13, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v13

    iput v13, v6, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 3147
    invoke-virtual {v5, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3148
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v14, "vienna_subtitle_line"

    invoke-virtual {v13, v5, v14}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 3153
    new-instance v13, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v15, "string_preset_tab"

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3154
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v15, "string_not_selected"

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 3153
    invoke-virtual {v9, v13}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3157
    const/4 v13, 0x3

    new-array v1, v13, [[I

    .line 3158
    .local v1, "arrayOfStates":[[I
    const/4 v13, 0x2

    new-array v2, v13, [I

    .line 3159
    .local v2, "arrayOfStates1":[I
    const/4 v13, 0x0

    const v14, -0x10100a7

    aput v14, v2, v13

    .line 3160
    const/4 v13, 0x1

    const v14, -0x10100a1

    aput v14, v2, v13

    .line 3161
    const/4 v13, 0x0

    aput-object v2, v1, v13

    .line 3162
    const/4 v13, 0x1

    new-array v3, v13, [I

    .line 3163
    .local v3, "arrayOfStates2":[I
    const/4 v13, 0x0

    const v14, 0x10100a7

    aput v14, v3, v13

    .line 3164
    const/4 v13, 0x1

    aput-object v3, v1, v13

    .line 3165
    const/4 v13, 0x1

    new-array v4, v13, [I

    .line 3166
    .local v4, "arrayOfStates3":[I
    const/4 v13, 0x0

    const v14, 0x10100a1

    aput v14, v4, v13

    .line 3167
    const/4 v13, 0x2

    aput-object v4, v1, v13

    .line 3168
    const/4 v13, 0x3

    new-array v12, v13, [I

    .line 3169
    .local v12, "textColor":[I
    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Landroid/graphics/Color;->rgb(III)I

    move-result v14

    aput v14, v12, v13

    .line 3170
    const/4 v13, 0x1

    const/16 v14, 0x1c

    const/16 v15, 0x7e

    const/16 v16, 0xc4

    invoke-static/range {v14 .. v16}, Landroid/graphics/Color;->rgb(III)I

    move-result v14

    aput v14, v12, v13

    .line 3171
    const/4 v13, 0x2

    const/16 v14, 0x1c

    const/16 v15, 0x7e

    const/16 v16, 0xc4

    invoke-static/range {v14 .. v16}, Landroid/graphics/Color;->rgb(III)I

    move-result v14

    aput v14, v12, v13

    .line 3172
    new-instance v8, Landroid/content/res/ColorStateList;

    invoke-direct {v8, v1, v12}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 3173
    .local v8, "mTextColorStateList":Landroid/content/res/ColorStateList;
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 3174
    const/4 v13, 0x0

    invoke-virtual {v7, v13}, Landroid/widget/TextView;->setClickable(Z)V

    .line 3175
    const/4 v13, 0x1

    invoke-virtual {v7, v13}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 3176
    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v7, v13, v14, v15, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 3177
    invoke-virtual {v9, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3178
    invoke-virtual {v9, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3179
    const/4 v13, 0x0

    invoke-virtual {v9, v13}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 3180
    return-object v9
.end method

.method private preview()Landroid/view/View;
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x0

    const/4 v8, -0x1

    .line 3640
    new-instance v3, Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 3641
    .local v3, "previewLayout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3642
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42c20000    # 97.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 3641
    invoke-direct {v0, v8, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3643
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3646
    invoke-virtual {v3, v9}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 3648
    new-instance v4, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    .line 3649
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setPenPlugin(Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;)V

    .line 3650
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3651
    const/4 v4, -0x2

    .line 3650
    invoke-direct {v1, v8, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3654
    .local v1, "penPreviewLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v5, -0x3f466666    # -5.8f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 3655
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, -0x40800000    # -1.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, -0x3fc00000    # -3.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 3654
    invoke-virtual {v1, v4, v5, v6, v9}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 3657
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    invoke-virtual {v4, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3659
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    invoke-virtual {v4, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setBackgroundColor(I)V

    .line 3662
    new-instance v4, Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    .line 3663
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3664
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41f00000    # 30.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 3663
    invoke-direct {v2, v8, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3666
    .local v2, "penPreviewLayoutParams02":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3667
    const/16 v4, 0x9

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3668
    const/16 v4, 0xc

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3669
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41900000    # 18.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 3670
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 3671
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40a00000    # 5.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 3672
    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSdkVersion:I

    const/16 v5, 0x10

    if-ge v4, v5, :cond_0

    .line 3673
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->previewAlphaPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3678
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 3679
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 3682
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 3685
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->advancedSettingButton()Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mAdvancedSettingButton:Landroid/view/View;

    .line 3686
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mAdvancedSettingButton:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    .line 3687
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mAdvancedSettingButton:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 3690
    return-object v3

    .line 3675
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->previewAlphaPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private resetBeautifyAdvanceDataAndUpdateSeekBarUi(I)V
    .locals 1
    .param p1, "beautifyStyleIndex"    # I

    .prologue
    .line 4907
    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->BEAUTIFY_ADVANCE_DEFAULT_SETTING_VALUES:[[I

    aget-object v0, v0, p1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->updateBeautifySeekBarsFromArray([I)V

    .line 4908
    return-void
.end method

.method private rotatePosition()V
    .locals 15

    .prologue
    const v14, 0x3f7d70a4    # 0.99f

    const/4 v13, 0x0

    const/4 v12, 0x0

    .line 6135
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 6137
    .local v4, "newMovableRect":Landroid/graphics/Rect;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTotalTopMargin:I

    if-eq v10, v11, :cond_2

    .line 6138
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTotalTopMargin:I

    sub-int/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTotalLeftMargin:I

    add-int/2addr v10, v11

    iput v10, v4, Landroid/graphics/Rect;->left:I

    .line 6142
    :goto_0
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTotalLeftMargin:I

    if-eq v10, v11, :cond_3

    .line 6143
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTotalLeftMargin:I

    sub-int/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTotalTopMargin:I

    add-int/2addr v10, v11

    iput v10, v4, Landroid/graphics/Rect;->top:I

    .line 6148
    :goto_1
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    iput v10, v4, Landroid/graphics/Rect;->right:I

    .line 6149
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    iput v10, v4, Landroid/graphics/Rect;->bottom:I

    .line 6151
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 6152
    .local v6, "r":Landroid/graphics/Rect;
    const/4 v10, 0x2

    new-array v3, v10, [I

    .line 6153
    .local v3, "location":[I
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getLocationOnScreen([I)V

    .line 6155
    aget v10, v3, v12

    iput v10, v6, Landroid/graphics/Rect;->left:I

    .line 6156
    const/4 v10, 0x1

    aget v10, v3, v10

    iput v10, v6, Landroid/graphics/Rect;->top:I

    .line 6157
    iget v10, v6, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getWidth()I

    move-result v11

    add-int/2addr v10, v11

    iput v10, v6, Landroid/graphics/Rect;->right:I

    .line 6158
    iget v10, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getHeight()I

    move-result v11

    add-int/2addr v10, v11

    iput v10, v6, Landroid/graphics/Rect;->bottom:I

    .line 6160
    iget v10, v6, Landroid/graphics/Rect;->left:I

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->left:I

    sub-int/2addr v10, v11

    int-to-float v2, v10

    .line 6161
    .local v2, "left":F
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    iget v11, v6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v10, v11

    int-to-float v7, v10

    .line 6162
    .local v7, "right":F
    iget v10, v6, Landroid/graphics/Rect;->top:I

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->top:I

    sub-int/2addr v10, v11

    int-to-float v8, v10

    .line 6163
    .local v8, "top":F
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    iget v11, v6, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v10, v11

    int-to-float v0, v10

    .line 6165
    .local v0, "bottom":F
    add-float v10, v2, v7

    div-float v1, v2, v10

    .line 6166
    .local v1, "hRatio":F
    add-float v10, v8, v0

    div-float v9, v8, v10

    .line 6168
    .local v9, "vRatio":F
    cmpl-float v10, v1, v14

    if-lez v10, :cond_4

    .line 6169
    const/high16 v1, 0x3f800000    # 1.0f

    .line 6174
    :cond_0
    :goto_2
    cmpl-float v10, v9, v14

    if-lez v10, :cond_5

    .line 6175
    const/high16 v9, 0x3f800000    # 1.0f

    .line 6180
    :cond_1
    :goto_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 6182
    .local v5, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v10

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v11

    if-ge v10, v11, :cond_6

    .line 6183
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v10

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v1

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    iput v10, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 6188
    :goto_4
    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v10

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v11

    if-ge v10, v11, :cond_7

    .line 6189
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v10

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v9

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    iput v10, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 6194
    :goto_5
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6195
    return-void

    .line 6140
    .end local v0    # "bottom":F
    .end local v1    # "hRatio":F
    .end local v2    # "left":F
    .end local v3    # "location":[I
    .end local v5    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v6    # "r":Landroid/graphics/Rect;
    .end local v7    # "right":F
    .end local v8    # "top":F
    .end local v9    # "vRatio":F
    :cond_2
    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTotalLeftMargin:I

    iput v10, v4, Landroid/graphics/Rect;->left:I

    goto/16 :goto_0

    .line 6145
    :cond_3
    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTotalTopMargin:I

    iput v10, v4, Landroid/graphics/Rect;->top:I

    goto/16 :goto_1

    .line 6170
    .restart local v0    # "bottom":F
    .restart local v1    # "hRatio":F
    .restart local v2    # "left":F
    .restart local v3    # "location":[I
    .restart local v6    # "r":Landroid/graphics/Rect;
    .restart local v7    # "right":F
    .restart local v8    # "top":F
    .restart local v9    # "vRatio":F
    :cond_4
    cmpg-float v10, v1, v13

    if-gez v10, :cond_0

    .line 6171
    const/4 v1, 0x0

    goto :goto_2

    .line 6176
    :cond_5
    cmpg-float v10, v9, v13

    if-gez v10, :cond_1

    .line 6177
    const/4 v9, 0x0

    goto :goto_3

    .line 6185
    .restart local v5    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_6
    iput v12, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_4

    .line 6191
    :cond_7
    iput v12, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_5
.end method

.method private setBeautifyAdvanceStringToCurrentAdvanceData(Ljava/lang/String;)V
    .locals 6
    .param p1, "advancedSetting"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0xa

    .line 4807
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getBeautifyStyleBtnIndex(Ljava/lang/String;)I

    move-result v2

    .line 4808
    .local v2, "beautifyStyleBtnIdx":I
    const-string v3, ";"

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 4810
    .local v1, "advanceStyleData":[Ljava/lang/String;
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCurrentBeautifyStyle:I

    .line 4812
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    array-length v3, v1

    if-ne v3, v5, :cond_1

    .line 4813
    :cond_0
    const/4 v0, 0x0

    .local v0, "advanceIdx":I
    :goto_0
    if-lt v0, v5, :cond_2

    .line 4818
    .end local v0    # "advanceIdx":I
    :cond_1
    return-void

    .line 4814
    .restart local v0    # "advanceIdx":I
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCurrentBeautifyAdvanceSettingValues:[[I

    aget-object v3, v3, v2

    .line 4815
    aget-object v4, v1, v0

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 4814
    aput v4, v3, v0

    .line 4813
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setBeautifyAdvancedDataToPlugin(II)V
    .locals 3
    .param p1, "beautifyParamIndex"    # I
    .param p2, "settingValue"    # I

    .prologue
    .line 4955
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    .line 4956
    const/4 v2, 0x4

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4957
    const/4 v0, 0x0

    .line 4958
    .local v0, "advancedSetting":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCurrentBeautifyAdvanceSettingValues:[[I

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCurrentBeautifyStyle:I

    aget-object v1, v1, v2

    aput p2, v1, p1

    .line 4959
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCurrentBeautifyAdvanceSettingValues:[[I

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCurrentBeautifyStyle:I

    aget-object v1, v1, v2

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getBeautifyAdvanceArrayDataToString([I)Ljava/lang/String;

    move-result-object v0

    .line 4960
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setAdvancedSetting(Ljava/lang/String;)V

    .line 4962
    .end local v0    # "advancedSetting":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private setExpandBarPosition(I)V
    .locals 19
    .param p1, "position"    # I

    .prologue
    .line 5953
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMagicPenEnable:Z

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isPresetClicked:Z

    if-nez v14, :cond_0

    .line 5954
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v15, 0x43858000    # 267.0f

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    move/from16 v0, p1

    if-le v0, v14, :cond_0

    .line 5955
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v15, 0x43858000    # 267.0f

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    .line 5957
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v14, -0x1

    .line 5958
    const/4 v15, -0x2

    .line 5957
    invoke-direct {v5, v14, v15}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5959
    .local v5, "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyBg:Landroid/view/View;

    invoke-virtual {v14, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5961
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 5962
    const/4 v14, -0x1

    const/4 v15, -0x2

    .line 5961
    invoke-direct {v8, v14, v15}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5963
    .local v8, "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v16, 0x43d60000    # 428.0f

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v15

    .line 5964
    const/16 v16, 0x0

    const/16 v17, 0x0

    .line 5963
    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v8, v14, v15, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 5966
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v14, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5971
    .end local v5    # "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v8    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getVisibility()I

    move-result v14

    if-nez v14, :cond_7

    .line 5975
    if-gez p1, :cond_1

    .line 5976
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->deltaOfMiniMode:I

    rsub-int v15, v15, 0x1ac

    int-to-float v15, v15

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    .line 5978
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->deltaOfMiniMode:I

    rsub-int v15, v15, 0x1ac

    int-to-float v15, v15

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    move/from16 v0, p1

    if-le v0, v14, :cond_2

    .line 5979
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->deltaOfMiniMode:I

    rsub-int v15, v15, 0x1ac

    int-to-float v15, v15

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    .line 5981
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v14, -0x1

    .line 5982
    const/4 v15, -0x2

    .line 5981
    invoke-direct {v5, v14, v15}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5983
    .restart local v5    # "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyBg:Landroid/view/View;

    invoke-virtual {v14, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5985
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 5986
    const/4 v14, -0x1

    const/4 v15, -0x2

    .line 5985
    invoke-direct {v8, v14, v15}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5987
    .restart local v8    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v14, 0x0

    .line 5988
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->deltaOfMiniMode:I

    move/from16 v16, v0

    move/from16 v0, v16

    rsub-int v0, v0, 0x1ac

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v15

    const/16 v16, 0x0

    const/16 v17, 0x0

    .line 5987
    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v8, v14, v15, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 5990
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v14, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6059
    .end local v5    # "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v8    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    const-string/jumbo v15, "window"

    invoke-virtual {v14, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/view/WindowManager;

    .line 6060
    invoke-interface {v14}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v10

    .line 6061
    .local v10, "nowDisplay":Landroid/view/Display;
    new-instance v11, Landroid/graphics/Point;

    invoke-direct {v11}, Landroid/graphics/Point;-><init>()V

    .line 6062
    .local v11, "p":Landroid/graphics/Point;
    invoke-virtual {v10, v11}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 6064
    iget v12, v11, Landroid/graphics/Point;->y:I

    .line 6065
    .local v12, "windowHeight":I
    iget v13, v11, Landroid/graphics/Point;->x:I

    .line 6067
    .local v13, "windowWidth":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v14}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v14

    if-le v14, v12, :cond_d

    move v3, v12

    .line 6068
    .local v3, "actualHeight":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v14}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v14

    if-le v14, v13, :cond_e

    move v4, v13

    .line 6069
    .local v4, "actualWidth":I
    :goto_2
    if-lez v3, :cond_3

    move/from16 v0, p1

    if-le v0, v3, :cond_3

    .line 6071
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v15, 0x3f800000    # 1.0f

    sget v16, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v15, v15, v16

    const/high16 v16, 0x42240000    # 41.0f

    add-float v15, v15, v16

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    .line 6070
    sub-int p1, v3, v14

    .line 6074
    :cond_3
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOrientation:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_f

    .line 6075
    if-lez v3, :cond_4

    .line 6077
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v15, 0x42240000    # 41.0f

    const/high16 v16, 0x3f800000    # 1.0f

    .line 6078
    sget v17, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v16, v16, v17

    add-float v15, v15, v16

    .line 6077
    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    .line 6076
    add-int v14, v14, p1

    .line 6078
    if-le v14, v3, :cond_4

    .line 6080
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v15, 0x42240000    # 41.0f

    const/high16 v16, 0x3f800000    # 1.0f

    sget v17, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v16, v16, v17

    add-float v15, v15, v16

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    .line 6079
    sub-int p1, v3, v14

    .line 6095
    :cond_4
    :goto_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v15, 0x42d20000    # 105.0f

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    move/from16 v0, p1

    if-ge v0, v14, :cond_5

    .line 6096
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v15, 0x42d20000    # 105.0f

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    .line 6098
    :cond_5
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    .line 6099
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v14, -0x1

    .line 6100
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x3f800000    # 1.0f

    sget v18, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v17, v17, v18

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    add-int v15, v15, v16

    .line 6099
    invoke-direct {v7, v14, v15}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 6101
    .local v7, "bodyParam":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v14, v7}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6104
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v14, -0x1

    .line 6105
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x3f800000    # 1.0f

    sget v18, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v17, v17, v18

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    add-int v15, v15, v16

    .line 6104
    invoke-direct {v5, v14, v15}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 6106
    .restart local v5    # "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyBg:Landroid/view/View;

    invoke-virtual {v14, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6110
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getVisibility()I

    move-result v14

    if-nez v14, :cond_10

    .line 6111
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 6112
    const/4 v14, -0x1

    .line 6113
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v16, 0x43230000    # 163.0f

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v15

    .line 6114
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x43d60000    # 428.0f

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    move/from16 v17, v0

    sub-int v16, v16, v17

    .line 6113
    sub-int v15, v15, v16

    .line 6115
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollAxis:I

    move/from16 v16, v0

    .line 6113
    add-int v15, v15, v16

    .line 6111
    invoke-direct {v2, v14, v15}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 6122
    .local v2, "PaletteBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    :goto_4
    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v2, v14, v15, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 6123
    iget v14, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    if-ltz v14, :cond_6

    .line 6124
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v14, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6127
    :cond_6
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v14, -0x1

    .line 6128
    const/4 v15, -0x2

    .line 6127
    invoke-direct {v8, v14, v15}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 6130
    .restart local v8    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v8, v14, v15, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 6131
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v14, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6132
    return-void

    .line 5992
    .end local v2    # "PaletteBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v3    # "actualHeight":I
    .end local v4    # "actualWidth":I
    .end local v5    # "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v7    # "bodyParam":Landroid/widget/LinearLayout$LayoutParams;
    .end local v8    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v10    # "nowDisplay":Landroid/view/Display;
    .end local v11    # "p":Landroid/graphics/Point;
    .end local v12    # "windowHeight":I
    .end local v13    # "windowWidth":I
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSupportBeautifyPen:Z

    if-eqz v14, :cond_b

    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->hasBeautifyPen()Z

    move-result v14

    if-eqz v14, :cond_b

    .line 5993
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v14, v14, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isBeautifyPen(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v14, v14, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isChinesePen(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 5994
    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v14, v14, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isBeautifyPen(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 5996
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v15, 0x43fc0000    # 504.0f

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 5998
    .local v6, "bodyHeight":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v15, 0x3f800000    # 1.0f

    sget v16, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v15, v15, v16

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    .line 5997
    sub-int v9, v6, v14

    .line 6000
    .local v9, "bottomTopMargin":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v14}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v14

    if-lez v14, :cond_9

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v14}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v14

    if-ge v14, v6, :cond_9

    .line 6001
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v14}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v14

    .line 6002
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v16, 0x42240000    # 41.0f

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v15

    .line 6001
    sub-int v6, v14, v15

    .line 6004
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v15, 0x3f800000    # 1.0f

    sget v16, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v15, v15, v16

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    .line 6003
    sub-int v9, v6, v14

    .line 6007
    :cond_9
    move/from16 v0, p1

    if-le v0, v9, :cond_2

    .line 6008
    move/from16 p1, v9

    .line 6009
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 6010
    const/4 v14, -0x1

    const/4 v15, -0x2

    .line 6009
    invoke-direct {v8, v14, v15}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 6011
    .restart local v8    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v8, v14, v9, v15, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 6012
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v14, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6015
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 6016
    const/4 v14, -0x1

    .line 6015
    invoke-direct {v5, v14, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 6017
    .restart local v5    # "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyBg:Landroid/view/View;

    invoke-virtual {v14, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 6022
    .end local v5    # "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v6    # "bodyHeight":I
    .end local v8    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v9    # "bottomTopMargin":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v15, 0x43d08000    # 417.0f

    .line 6023
    const/high16 v16, 0x3f800000    # 1.0f

    sget v17, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v16, v16, v17

    add-float v15, v15, v16

    .line 6022
    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    .line 6023
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v16, 0x3f800000    # 1.0f

    .line 6024
    sget v17, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v16, v16, v17

    .line 6023
    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v15

    sub-int/2addr v14, v15

    move/from16 v0, p1

    if-le v0, v14, :cond_2

    .line 6025
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v15, 0x43d08000    # 417.0f

    .line 6026
    const/high16 v16, 0x3f800000    # 1.0f

    sget v17, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v16, v16, v17

    add-float v15, v15, v16

    .line 6025
    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    .line 6026
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 6027
    const/high16 v16, 0x3f800000    # 1.0f

    sget v17, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v16, v16, v17

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v15

    .line 6025
    sub-int v9, v14, v15

    .line 6028
    .restart local v9    # "bottomTopMargin":I
    move/from16 p1, v9

    .line 6029
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 6030
    const/4 v14, -0x1

    const/4 v15, -0x2

    .line 6029
    invoke-direct {v8, v14, v15}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 6031
    .restart local v8    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v8, v14, v9, v15, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 6032
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v14, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6033
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 6034
    const/4 v14, -0x1

    const/4 v15, -0x2

    .line 6033
    invoke-direct {v5, v14, v15}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 6035
    .restart local v5    # "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyBg:Landroid/view/View;

    invoke-virtual {v14, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 6040
    .end local v5    # "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v8    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v9    # "bottomTopMargin":I
    :cond_b
    if-gez p1, :cond_c

    .line 6041
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->deltaOfMiniMode:I

    rsub-int v15, v15, 0x16e

    int-to-float v15, v15

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    .line 6043
    :cond_c
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->deltaOfMiniMode:I

    rsub-int v15, v15, 0x16e

    int-to-float v15, v15

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    move/from16 v0, p1

    if-le v0, v14, :cond_2

    .line 6045
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->deltaOfMiniMode:I

    rsub-int v15, v15, 0x16e

    int-to-float v15, v15

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    .line 6047
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v14, -0x1

    .line 6048
    const/4 v15, -0x2

    .line 6047
    invoke-direct {v5, v14, v15}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 6049
    .restart local v5    # "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyBg:Landroid/view/View;

    invoke-virtual {v14, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6050
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 6051
    const/4 v14, -0x1

    const/4 v15, -0x2

    .line 6050
    invoke-direct {v8, v14, v15}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 6052
    .restart local v8    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v14, 0x0

    .line 6053
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v16, 0x43b68000    # 365.0f

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v15

    const/16 v16, 0x0

    const/16 v17, 0x0

    .line 6052
    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v8, v14, v15, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 6055
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v14, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 6067
    .end local v5    # "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v8    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v10    # "nowDisplay":Landroid/view/Display;
    .restart local v11    # "p":Landroid/graphics/Point;
    .restart local v12    # "windowHeight":I
    .restart local v13    # "windowWidth":I
    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v14}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    goto/16 :goto_1

    .line 6068
    .restart local v3    # "actualHeight":I
    :cond_e
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v14}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v4

    goto/16 :goto_2

    .line 6084
    .restart local v4    # "actualWidth":I
    :cond_f
    if-lez v4, :cond_4

    .line 6086
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v15, 0x42240000    # 41.0f

    const/high16 v16, 0x3f800000    # 1.0f

    .line 6087
    sget v17, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v16, v16, v17

    add-float v15, v15, v16

    .line 6086
    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    .line 6085
    add-int v14, v14, p1

    .line 6087
    if-le v14, v4, :cond_4

    .line 6089
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v15, 0x42240000    # 41.0f

    const/high16 v16, 0x3f800000    # 1.0f

    sget v17, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v16, v16, v17

    add-float v15, v15, v16

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    .line 6088
    sub-int p1, v4, v14

    goto/16 :goto_3

    .line 6117
    .restart local v5    # "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v7    # "bodyParam":Landroid/widget/LinearLayout$LayoutParams;
    :cond_10
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v14, -0x1

    .line 6118
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v16, 0x43230000    # 163.0f

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v15

    .line 6119
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x43b70000    # 366.0f

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    move/from16 v17, v0

    sub-int v16, v16, v17

    .line 6118
    sub-int v15, v15, v16

    .line 6120
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollAxis:I

    move/from16 v16, v0

    .line 6118
    add-int v15, v15, v16

    .line 6117
    invoke-direct {v2, v14, v15}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .restart local v2    # "PaletteBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    goto/16 :goto_4
.end method

.method private setListener()V
    .locals 5

    .prologue
    .line 2477
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTitleLayout:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 2478
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTitleLayout:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2479
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTitleLayout:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 2486
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupMaxButton:Landroid/view/View;

    if-eqz v3, :cond_1

    .line 2487
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupMaxButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2489
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupMinButton:Landroid/view/View;

    if-eqz v3, :cond_2

    .line 2490
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupMinButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2493
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    .line 2494
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v0, v3, :cond_21

    .line 2501
    .end local v0    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    if-eqz v3, :cond_4

    .line 2502
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 2505
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2509
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    if-eqz v3, :cond_5

    .line 2510
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 2513
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2517
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    if-eqz v3, :cond_6

    .line 2518
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setColorPickerColorChangeListener(Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;)V

    .line 2521
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    if-eqz v3, :cond_7

    .line 2522
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mExpendBarListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2526
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizePlusButton:Landroid/view/View;

    if-eqz v3, :cond_8

    .line 2527
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizePlusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizePlusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2528
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizePlusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizePlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2529
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizePlusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizePlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2531
    :cond_8
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeMinusButton:Landroid/view/View;

    if-eqz v3, :cond_9

    .line 2532
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeMinusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeMinusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2533
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeMinusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2534
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeMinusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2536
    :cond_9
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOpacityPlusButton:Landroid/view/View;

    if-eqz v3, :cond_a

    .line 2537
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOpacityPlusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPlusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2538
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOpacityPlusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2539
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOpacityPlusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2542
    :cond_a
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOpacityMinusButton:Landroid/view/View;

    if-eqz v3, :cond_b

    .line 2543
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOpacityMinusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaMinusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2544
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOpacityMinusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2545
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOpacityMinusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2548
    :cond_b
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteRightButton:Landroid/view/View;

    if-eqz v3, :cond_c

    .line 2549
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteRightButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2552
    :cond_c
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteLeftButton:Landroid/view/View;

    if-eqz v3, :cond_d

    .line 2553
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteLeftButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2557
    :cond_d
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetAddButton:Landroid/view/View;

    if-eqz v3, :cond_e

    .line 2558
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetAddButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreSetAddButtonListner:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2561
    :cond_e
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mAdvancedSettingButton:Landroid/view/View;

    if-eqz v3, :cond_f

    .line 2562
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mAdvancedSettingButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mAdvancedSettingButtonListner:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2566
    :cond_f
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    if-eqz v3, :cond_10

    .line 2567
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOnClickPresetItemListener:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->OnClickPresetItemListener(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;)V

    .line 2571
    :cond_10
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    if-eqz v3, :cond_11

    .line 2572
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnablecheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v3, v4}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2575
    :cond_11
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    if-eqz v3, :cond_12

    .line 2576
    const/4 v2, 0x0

    .local v2, "styleIndex":I
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_23

    .line 2582
    .end local v2    # "styleIndex":I
    :cond_12
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    if-eqz v3, :cond_13

    .line 2583
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 2586
    :cond_13
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    if-eqz v3, :cond_14

    .line 2587
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 2590
    :cond_14
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    if-eqz v3, :cond_15

    .line 2591
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 2594
    :cond_15
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    if-eqz v3, :cond_16

    .line 2595
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 2598
    :cond_16
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    if-eqz v3, :cond_17

    .line 2599
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursivePlusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2602
    :cond_17
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    if-eqz v3, :cond_18

    .line 2603
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenancePlusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2606
    :cond_18
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    if-eqz v3, :cond_19

    .line 2607
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyPlusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2610
    :cond_19
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    if-eqz v3, :cond_1a

    .line 2611
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationPlusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2614
    :cond_1a
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    if-eqz v3, :cond_1b

    .line 2615
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveMinusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2618
    :cond_1b
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    if-eqz v3, :cond_1c

    .line 2619
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceMinusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2622
    :cond_1c
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    if-eqz v3, :cond_1d

    .line 2623
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyMinusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2626
    :cond_1d
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    if-eqz v3, :cond_1e

    .line 2627
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationMinusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2630
    :cond_1e
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    if-eqz v3, :cond_1f

    .line 2631
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceResetButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2634
    :cond_1f
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    if-eqz v3, :cond_20

    .line 2635
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setOnScrollChangedListener(Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;)V

    .line 2639
    :cond_20
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->horizontalScrollViewTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/widget/HorizontalScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2641
    return-void

    .line 2495
    .restart local v0    # "i":I
    :cond_21
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_22

    .line 2496
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeListner:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2494
    :cond_22
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 2577
    .end local v0    # "i":I
    .restart local v2    # "styleIndex":I
    :cond_23
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 2578
    .local v1, "styleBtn":Landroid/widget/ImageButton;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnsListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2576
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1
.end method

.method private setMagicPenMode(I)V
    .locals 3
    .param p1, "height"    # I

    .prologue
    const/4 v2, 0x0

    .line 5936
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMagicPenEnable:Z

    .line 5937
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 5938
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 5939
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 5940
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 5942
    const v0, 0x1869f

    if-ne p1, v0, :cond_0

    .line 5943
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v1, 0x43858000    # 267.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    .line 5947
    :goto_0
    return-void

    .line 5945
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto :goto_0
.end method

.method private showAdvanceSettingLayout(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 3439
    const/16 v0, 0x8

    .line 3440
    .local v0, "visibility":I
    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSupportBeautifyPen:Z

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->hasBeautifyPen()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3441
    const/4 v0, 0x0

    .line 3446
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSettingLayout:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSettingLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 3447
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSettingLayout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 3449
    :cond_0
    return-void

    .line 3443
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private showBeautifyEnableLayout(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 3401
    const/16 v0, 0x8

    .line 3402
    .local v0, "visibility":I
    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSupportBeautifyPen:Z

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->hasBeautifyPen()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3403
    const/4 v0, 0x0

    .line 3408
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableLayout:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 3409
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableLayout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 3411
    :cond_0
    return-void

    .line 3405
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private showBeautifySettingViews(Z)V
    .locals 0
    .param p1, "show"    # Z

    .prologue
    .line 3390
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->showBeautifyStyleBtnsLayout(Z)V

    .line 3391
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->showAdvanceSettingLayout(Z)V

    .line 3392
    return-void
.end method

.method private showBeautifyStyleBtnsLayout(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 3420
    const/16 v0, 0x8

    .line 3421
    .local v0, "visibility":I
    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSupportBeautifyPen:Z

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->hasBeautifyPen()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3422
    const/4 v0, 0x0

    .line 3427
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnsLayout:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnsLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 3428
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnsLayout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 3430
    :cond_0
    return-void

    .line 3424
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private titleBg()Landroid/view/View;
    .locals 10

    .prologue
    const/16 v9, 0xa

    const/4 v8, 0x1

    const/4 v7, -0x1

    .line 2055
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2056
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v5, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2058
    new-instance v1, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v1, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 2059
    .local v1, "titleBg":Landroid/widget/ImageView;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2061
    .local v2, "titleBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v8, v2, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 2062
    const/16 v5, 0x9

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2063
    invoke-virtual {v2, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2066
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2068
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->titleLeftPath:Ljava/lang/String;

    invoke-virtual {v5, v1, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 2074
    new-instance v3, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v3, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 2075
    .local v3, "titleRight":Landroid/widget/ImageView;
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2076
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v6, 0x42fa999a    # 125.3f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 2075
    invoke-direct {v4, v5, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2077
    .local v4, "titleRightParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v8, v4, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 2078
    const/16 v5, 0xb

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2079
    invoke-virtual {v4, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2080
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2081
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->titleLeftPath:Ljava/lang/String;

    invoke-virtual {v5, v1, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 2082
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2083
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 2084
    return-object v0
.end method

.method private titleLayout()Landroid/view/ViewGroup;
    .locals 5

    .prologue
    .line 2029
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2030
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 2031
    const/high16 v4, 0x42240000    # 41.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2030
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2033
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->popupMaxButton()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupMaxButton:Landroid/view/View;

    .line 2034
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->popupMinButton()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupMinButton:Landroid/view/View;

    .line 2035
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->lineButton2()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mLine2Button:Landroid/view/View;

    .line 2036
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->presetAddButton()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetAddButton:Landroid/view/View;

    .line 2037
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->lineButton1()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mLine1Button:Landroid/view/View;

    .line 2039
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->titleBg()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2040
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->titleText()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2041
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mLine1Button:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2042
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mLine2Button:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2044
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupMaxButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2045
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupMinButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2046
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetAddButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2048
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupMaxButton:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2050
    return-object v0
.end method

.method private titleText()Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    const/16 v2, 0xfa

    const/4 v3, 0x0

    .line 2761
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->titleTextView:Landroid/widget/TextView;

    .line 2762
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->titleTextView:Landroid/widget/TextView;

    .line 2763
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2764
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->titleTextView:Landroid/widget/TextView;

    invoke-static {v2, v2, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2765
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->titleTextView:Landroid/widget/TextView;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 2766
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->titleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 2767
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->titleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 2768
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->titleTextView:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2769
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->titleTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_pen_settings"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2770
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->titleTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2771
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->titleTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_pen_settings"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2772
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->titleTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41500000    # 13.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2773
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->titleTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method private totalLayout()V
    .locals 3

    .prologue
    .line 2013
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2014
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v2, 0x43a48000    # 329.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    const/4 v2, -0x2

    .line 2013
    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2015
    .local v0, "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2017
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setOrientation(I)V

    .line 2018
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->titleLayout()Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTitleLayout:Landroid/view/View;

    .line 2019
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->bodyLayout()Landroid/widget/RelativeLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout:Landroid/widget/RelativeLayout;

    .line 2020
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->bodyLayout2()Landroid/widget/RelativeLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout2:Landroid/widget/RelativeLayout;

    .line 2022
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTitleLayout:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->addView(Landroid/view/View;)V

    .line 2023
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->addView(Landroid/view/View;)V

    .line 2024
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout2:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->addView(Landroid/view/View;)V

    .line 2025
    return-void
.end method

.method private typeSelectorlayout()Landroid/widget/LinearLayout;
    .locals 5

    .prologue
    .line 2970
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2971
    .local v0, "typeButtonLayout":Landroid/widget/LinearLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 2972
    const/high16 v4, 0x42280000    # 42.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2971
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2973
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2976
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penTypeButton()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2980
    return-object v0
.end method

.method private typeSelectorlayout2()Landroid/widget/LinearLayout;
    .locals 5

    .prologue
    .line 2984
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2985
    .local v0, "typeButtonLayout":Landroid/widget/LinearLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 2986
    const/high16 v4, 0x42280000    # 42.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2985
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2987
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2990
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->presetTypeButton()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2994
    return-object v0
.end method

.method private updateBeautifySeekBars(IIII)V
    .locals 1
    .param p1, "cursive"    # I
    .param p2, "sustenance"    # I
    .param p3, "dummy"    # I
    .param p4, "modulation"    # I

    .prologue
    .line 4859
    const/16 v0, 0xc

    if-ge v0, p1, :cond_8

    .line 4860
    const/16 p1, 0xc

    .line 4865
    :cond_0
    :goto_0
    const/16 v0, 0x10

    if-ge v0, p2, :cond_9

    .line 4866
    const/16 p2, 0x10

    .line 4871
    :cond_1
    :goto_1
    const/16 v0, 0x14

    if-ge v0, p3, :cond_a

    .line 4872
    const/16 p3, 0x14

    .line 4877
    :cond_2
    :goto_2
    const/16 v0, 0x64

    if-ge v0, p4, :cond_b

    .line 4878
    const/16 p4, 0x64

    .line 4883
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_4

    .line 4884
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 4887
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_5

    .line 4888
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 4891
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_6

    .line 4892
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 4895
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_7

    .line 4896
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p4}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 4898
    :cond_7
    return-void

    .line 4861
    :cond_8
    if-gez p1, :cond_0

    .line 4862
    const/4 p1, 0x0

    goto :goto_0

    .line 4867
    :cond_9
    if-gez p2, :cond_1

    .line 4868
    const/4 p2, 0x0

    goto :goto_1

    .line 4873
    :cond_a
    if-gez p3, :cond_2

    .line 4874
    const/4 p3, 0x0

    goto :goto_2

    .line 4879
    :cond_b
    if-gez p4, :cond_3

    .line 4880
    const/4 p4, 0x0

    goto :goto_3
.end method

.method private updateBeautifySeekBarsFromArray([I)V
    .locals 5
    .param p1, "advanceData"    # [I

    .prologue
    .line 4844
    const/4 v4, 0x2

    invoke-direct {p0, p1, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getBeautifyAdvanceParamDataFromArray([II)I

    move-result v0

    .line 4845
    .local v0, "cursive":I
    const/4 v4, 0x3

    invoke-direct {p0, p1, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getBeautifyAdvanceParamDataFromArray([II)I

    move-result v3

    .line 4846
    .local v3, "sustenance":I
    const/4 v4, 0x4

    invoke-direct {p0, p1, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getBeautifyAdvanceParamDataFromArray([II)I

    move-result v1

    .line 4847
    .local v1, "dummy":I
    const/4 v4, 0x6

    invoke-direct {p0, p1, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getBeautifyAdvanceParamDataFromArray([II)I

    move-result v2

    .line 4849
    .local v2, "modulation":I
    invoke-direct {p0, v0, v3, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->updateBeautifySeekBars(IIII)V

    .line 4850
    return-void
.end method

.method private updateBeautifySeekBarsFromString(Ljava/lang/String;)V
    .locals 5
    .param p1, "advanceSetting"    # Ljava/lang/String;

    .prologue
    .line 4828
    const/4 v4, 0x2

    invoke-direct {p0, p1, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getBeautifyAdvanceParamDataFromString(Ljava/lang/String;I)I

    move-result v0

    .line 4829
    .local v0, "cursive":I
    const/4 v4, 0x3

    invoke-direct {p0, p1, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getBeautifyAdvanceParamDataFromString(Ljava/lang/String;I)I

    move-result v3

    .line 4830
    .local v3, "sustenance":I
    const/4 v4, 0x4

    invoke-direct {p0, p1, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getBeautifyAdvanceParamDataFromString(Ljava/lang/String;I)I

    move-result v1

    .line 4831
    .local v1, "dummy":I
    const/4 v4, 0x6

    invoke-direct {p0, p1, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getBeautifyAdvanceParamDataFromString(Ljava/lang/String;I)I

    move-result v2

    .line 4833
    .local v2, "modulation":I
    invoke-direct {p0, v0, v3, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->updateBeautifySeekBars(IIII)V

    .line 4834
    return-void
.end method

.method private updateBeautifySettingData()V
    .locals 4

    .prologue
    .line 4916
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    if-nez v1, :cond_1

    .line 4917
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->loadPenPlugin(Landroid/content/Context;Ljava/lang/String;)V

    .line 4918
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    if-nez v1, :cond_1

    .line 4940
    :cond_0
    :goto_0
    return-void

    .line 4922
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getSize()F

    move-result v1

    iput v1, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 4924
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    .line 4925
    const/4 v2, 0x4

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4926
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    .line 4927
    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getAdvancedSetting()Ljava/lang/String;

    move-result-object v1

    .line 4926
    iput-object v1, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    .line 4930
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v1, :cond_0

    .line 4931
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v0

    .line 4932
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    if-eqz v0, :cond_0

    .line 4933
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 4934
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 4935
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 4936
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    .line 4937
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto :goto_0
.end method

.method private updateBeautifyStyleBtnFromString(Ljava/lang/String;)V
    .locals 4
    .param p1, "advanceSetting"    # Ljava/lang/String;

    .prologue
    .line 3589
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getBeautifyStyleBtnIndex(Ljava/lang/String;)I

    move-result v0

    .line 3591
    .local v0, "newStyleBtnIdx":I
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCurrentBeautifyStyle:I

    .line 3593
    const/4 v2, 0x0

    .local v2, "styleBtnIndex":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 3602
    return-void

    .line 3594
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 3596
    .local v1, "styleBtn":Landroid/widget/ImageButton;
    if-ne v0, v2, :cond_1

    .line 3597
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3593
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3599
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_1
.end method


# virtual methods
.method public close()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2151
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->savePreferences()V

    .line 2153
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    if-nez v3, :cond_0

    .line 2342
    :goto_0
    return-void

    .line 2157
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginCount:I

    if-lt v0, v3, :cond_b

    .line 2165
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2166
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2167
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    .line 2168
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    if-eqz v3, :cond_1

    .line 2169
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->close()V

    .line 2170
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2171
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    .line 2173
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2174
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeLayout:Landroid/view/ViewGroup;

    .line 2175
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2176
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    .line 2177
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2178
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    .line 2179
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    if-eqz v3, :cond_2

    .line 2180
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->close()V

    .line 2181
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2182
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    .line 2184
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    if-eqz v3, :cond_3

    .line 2185
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->close()V

    .line 2186
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2187
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    .line 2190
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2191
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarView:Landroid/view/View;

    .line 2192
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2193
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    .line 2196
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarView:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2197
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarView:Landroid/view/View;

    .line 2198
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2199
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviewLayout:Landroid/view/View;

    .line 2200
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2201
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    .line 2202
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2203
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSeekbarLayout:Landroid/view/View;

    .line 2205
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2206
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableTextView:Landroid/widget/TextView;

    .line 2207
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2208
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    .line 2209
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableLayout:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2210
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableLayout:Landroid/view/View;

    .line 2212
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    if-eqz v3, :cond_4

    .line 2213
    const/4 v0, 0x0

    :goto_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v0, v3, :cond_d

    .line 2220
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 2221
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    .line 2223
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnsLayout:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2224
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnsLayout:Landroid/view/View;

    .line 2226
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2227
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    .line 2228
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2229
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    .line 2230
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyDummyTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2231
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyDummyTextView:Landroid/widget/TextView;

    .line 2232
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyModulationTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2233
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyModulationTextView:Landroid/widget/TextView;

    .line 2235
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2236
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    .line 2237
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2238
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    .line 2239
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2240
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    .line 2241
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2242
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    .line 2243
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2244
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    .line 2245
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2246
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    .line 2247
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2248
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    .line 2249
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2250
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    .line 2251
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2252
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    .line 2253
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2254
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    .line 2255
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2256
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    .line 2257
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2258
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    .line 2259
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2260
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    .line 2262
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSettingLayout:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2263
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSettingLayout:Landroid/view/View;

    .line 2265
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2266
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    .line 2267
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2268
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetTextView:Landroid/widget/TextView;

    .line 2272
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetAddButton:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2273
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetAddButton:Landroid/view/View;

    .line 2274
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2275
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerColorImage:Landroid/view/View;

    .line 2276
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2277
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    .line 2278
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSettingExitButton:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2279
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSettingExitButton:Landroid/view/View;

    .line 2280
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2281
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    .line 2283
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2284
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTitleLayout:Landroid/view/View;

    .line 2285
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyBg:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2286
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyBg:Landroid/view/View;

    .line 2287
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2288
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout:Landroid/widget/RelativeLayout;

    .line 2290
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->close()V

    .line 2291
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2292
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    .line 2293
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    if-eqz v3, :cond_5

    .line 2294
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 2295
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    .line 2298
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    if-eqz v3, :cond_6

    .line 2299
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->close()V

    .line 2300
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    .line 2303
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    if-eqz v3, :cond_7

    .line 2304
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 2305
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    .line 2307
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetDataList:Ljava/util/List;

    if-eqz v3, :cond_8

    .line 2308
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 2309
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetDataList:Ljava/util/List;

    .line 2311
    :cond_8
    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    if-eqz v3, :cond_9

    .line 2312
    const/4 v0, 0x0

    :goto_3
    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v0, v3, :cond_e

    .line 2317
    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 2318
    sput-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    .line 2320
    :cond_9
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenDataList:Ljava/util/List;

    if-eqz v3, :cond_a

    .line 2321
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 2322
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenDataList:Ljava/util/List;

    .line 2325
    :cond_a
    sput-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDefaultPath:Ljava/lang/String;

    .line 2326
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    .line 2327
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    .line 2328
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    .line 2330
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PresetListener;

    .line 2331
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 2332
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    .line 2333
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 2334
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .line 2335
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2336
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 2338
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->close()V

    .line 2339
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 2341
    const/4 v3, 0x0

    sput-boolean v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerShow:Z

    goto/16 :goto_0

    .line 2158
    :cond_b
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 2159
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v3

    if-eqz v3, :cond_c

    .line 2160
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v3

    invoke-interface {v3, v5}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2161
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->unloadPlugin(Ljava/lang/Object;)V

    .line 2157
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 2214
    :cond_d
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 2216
    .local v2, "styleBtn":Landroid/widget/ImageButton;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 2213
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    .line 2313
    .end local v2    # "styleBtn":Landroid/widget/ImageButton;
    :cond_e
    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    .line 2314
    .local v1, "presetInfo":Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->close()V

    .line 2312
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_3
.end method

.method protected drawExpendImage(Ljava/lang/String;)V
    .locals 14
    .param p1, "penName"    # Ljava/lang/String;

    .prologue
    .line 5188
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    if-nez v8, :cond_1

    .line 5397
    :cond_0
    :goto_0
    return-void

    .line 5191
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual {v8, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v7

    .line 5192
    .local v7, "penPluginIndex":I
    const/4 v8, -0x1

    if-eq v7, v8, :cond_0

    .line 5195
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v8

    if-nez v8, :cond_2

    .line 5196
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->loadPenPlugin(Landroid/content/Context;Ljava/lang/String;)V

    .line 5197
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 5206
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isBeautifyPen(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isChinesePen(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_f

    :cond_3
    iget-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSupportBeautifyPen:Z

    if-eqz v8, :cond_f

    .line 5207
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->hasBeautifyPen()Z

    move-result v8

    if-eqz v8, :cond_f

    .line 5208
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isBeautifyPen(Ljava/lang/String;)Z

    move-result v5

    .line 5209
    .local v5, "isBeautify":Z
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    invoke-virtual {v8}, Landroid/widget/Switch;->isChecked()Z

    move-result v8

    if-eq v8, v5, :cond_4

    .line 5210
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    invoke-virtual {v8, v5}, Landroid/widget/Switch;->setChecked(Z)V

    .line 5212
    :cond_4
    const/4 v8, 0x1

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->showBeautifyEnableLayout(Z)V

    .line 5214
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnsLayout:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-eqz v8, :cond_6

    .line 5215
    if-eqz v5, :cond_5

    .line 5216
    const/4 v8, 0x1

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->showBeautifyStyleBtnsLayout(Z)V

    .line 5218
    :cond_5
    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->showAdvanceSettingLayout(Z)V

    .line 5221
    :cond_6
    if-eqz v5, :cond_7

    .line 5222
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->updateBeautifyStyleBtnFromString(Ljava/lang/String;)V

    .line 5223
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->updateBeautifySeekBarsFromString(Ljava/lang/String;)V

    .line 5227
    :cond_7
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-nez v8, :cond_8

    .line 5228
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarView:Landroid/view/View;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 5231
    :cond_8
    if-eqz v5, :cond_c

    .line 5233
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v10, 0x3f800000    # 1.0f

    sget v11, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float/2addr v10, v11

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v9

    sub-int/2addr v8, v9

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 5234
    const/high16 v10, 0x43b70000    # 366.0f

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v9

    if-ge v8, v9, :cond_9

    .line 5235
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    const/4 v9, -0x2

    if-eq v8, v9, :cond_9

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    if-nez v8, :cond_b

    .line 5236
    :cond_9
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x43fc0000    # 504.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 5238
    .local v1, "bodyHeight":I
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x3f800000    # 1.0f

    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float/2addr v9, v10

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 5237
    sub-int v4, v1, v8

    .line 5240
    .local v4, "bottomTopMargin":I
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v8

    if-lez v8, :cond_a

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v8

    if-ge v8, v1, :cond_a

    .line 5241
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v8

    .line 5242
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v10, 0x42240000    # 41.0f

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v9

    .line 5241
    sub-int v1, v8, v9

    .line 5244
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x3f800000    # 1.0f

    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float/2addr v9, v10

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 5243
    sub-int v4, v1, v8

    .line 5247
    :cond_a
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    .line 5248
    const/4 v8, -0x1

    .line 5247
    invoke-direct {v6, v8, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 5249
    .local v6, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v6}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5251
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 5252
    const/4 v8, -0x1

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v10, 0x43020000    # 130.0f

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v9

    sub-int v9, v1, v9

    .line 5251
    invoke-direct {v0, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5253
    .local v0, "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyBg:Landroid/view/View;

    invoke-virtual {v8, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5255
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 5256
    const/4 v8, -0x1

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v10, 0x3f800000    # 1.0f

    .line 5257
    sget v11, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float/2addr v10, v11

    .line 5256
    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v9

    .line 5255
    invoke-direct {v3, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5258
    .local v3, "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v3, v8, v4, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 5259
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v8, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5261
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    .line 5264
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x43fc0000    # 504.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_0

    .line 5266
    .end local v0    # "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v1    # "bodyHeight":I
    .end local v3    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v4    # "bottomTopMargin":I
    .end local v6    # "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_b
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v8, -0x1

    .line 5267
    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    .line 5266
    invoke-direct {v2, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 5268
    .local v2, "bodyParam":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5269
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 5270
    const/4 v8, -0x1

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v10, 0x3f800000    # 1.0f

    .line 5271
    sget v11, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float/2addr v10, v11

    .line 5270
    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v9

    .line 5269
    invoke-direct {v3, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5272
    .restart local v3    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v8, 0x0

    .line 5273
    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v11, 0x3f800000    # 1.0f

    sget v12, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float/2addr v11, v12

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v10

    sub-int/2addr v9, v10

    .line 5274
    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 5272
    invoke-virtual {v3, v8, v9, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 5275
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v8, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5276
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x43fc0000    # 504.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_0

    .line 5280
    .end local v2    # "bodyParam":Landroid/widget/LinearLayout$LayoutParams;
    .end local v3    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_c
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v10, 0x3f800000    # 1.0f

    sget v11, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float/2addr v10, v11

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v9

    sub-int/2addr v8, v9

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 5281
    const/high16 v10, 0x43b70000    # 366.0f

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v9

    if-ge v8, v9, :cond_d

    .line 5282
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    const/4 v9, -0x2

    if-eq v8, v9, :cond_d

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    if-nez v8, :cond_0

    .line 5283
    :cond_d
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x43d60000    # 428.0f

    .line 5284
    const/high16 v10, 0x3f800000    # 1.0f

    sget v11, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float/2addr v10, v11

    add-float/2addr v9, v10

    .line 5283
    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 5286
    .restart local v1    # "bodyHeight":I
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x3f800000    # 1.0f

    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float/2addr v9, v10

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 5285
    sub-int v4, v1, v8

    .line 5287
    .restart local v4    # "bottomTopMargin":I
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    .line 5288
    const/4 v8, -0x1

    const/4 v9, -0x2

    .line 5287
    invoke-direct {v6, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 5289
    .restart local v6    # "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v6}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5291
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v8

    if-lez v8, :cond_e

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v8

    if-ge v8, v1, :cond_e

    .line 5292
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v8

    .line 5293
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v10, 0x42240000    # 41.0f

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v9

    .line 5292
    sub-int v1, v8, v9

    .line 5295
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x3f800000    # 1.0f

    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float/2addr v9, v10

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 5294
    sub-int v4, v1, v8

    .line 5298
    :cond_e
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 5299
    const/4 v8, -0x1

    .line 5298
    invoke-direct {v0, v8, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5300
    .restart local v0    # "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyBg:Landroid/view/View;

    invoke-virtual {v8, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5302
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 5303
    const/4 v8, -0x1

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v10, 0x3f800000    # 1.0f

    .line 5304
    sget v11, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float/2addr v10, v11

    .line 5303
    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v9

    .line 5302
    invoke-direct {v3, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5305
    .restart local v3    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v3, v8, v4, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 5306
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v8, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5308
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x43d60000    # 428.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_0

    .line 5331
    .end local v0    # "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v1    # "bodyHeight":I
    .end local v3    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v4    # "bottomTopMargin":I
    .end local v5    # "isBeautify":Z
    .end local v6    # "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_f
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v8

    .line 5332
    const/4 v9, 0x1

    invoke-interface {v8, v9}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v8

    if-eqz v8, :cond_15

    .line 5334
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-nez v8, :cond_11

    .line 5335
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableLayout:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-nez v8, :cond_10

    .line 5336
    const/4 v8, 0x0

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->showBeautifyEnableLayout(Z)V

    .line 5337
    const/4 v8, 0x0

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->showBeautifySettingViews(Z)V

    .line 5341
    :cond_10
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_0

    .line 5346
    :cond_11
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarView:Landroid/view/View;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 5348
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableLayout:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-nez v8, :cond_12

    .line 5349
    const/4 v8, 0x0

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->showBeautifyEnableLayout(Z)V

    .line 5350
    const/4 v8, 0x0

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->showBeautifySettingViews(Z)V

    .line 5355
    :cond_12
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    if-gtz v8, :cond_13

    .line 5356
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x43d60000    # 428.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_0

    .line 5357
    :cond_13
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v10, 0x43b70000    # 366.0f

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v9

    if-lt v8, v9, :cond_0

    .line 5361
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mWindowHeight:I

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    .line 5362
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v11, 0x42240000    # 41.0f

    const/high16 v12, 0x3f800000    # 1.0f

    .line 5363
    sget v13, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float/2addr v12, v13

    add-float/2addr v11, v12

    const/high16 v12, 0x42780000    # 62.0f

    add-float/2addr v11, v12

    .line 5362
    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v10

    add-int/2addr v9, v10

    if-le v8, v9, :cond_14

    .line 5364
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    .line 5365
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v10, 0x42780000    # 62.0f

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v9

    add-int/2addr v8, v9

    .line 5364
    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_0

    .line 5366
    :cond_14
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mViewMode:I

    const/16 v9, 0x8

    if-ne v8, v9, :cond_0

    .line 5367
    const/16 v8, 0x3e8

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_0

    .line 5375
    :cond_15
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v8

    const/16 v9, 0x8

    if-ne v8, v9, :cond_16

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableLayout:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v8

    const/16 v9, 0x8

    if-ne v8, v9, :cond_16

    .line 5376
    const/4 v8, 0x0

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->showBeautifyEnableLayout(Z)V

    .line 5377
    const/4 v8, 0x0

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->showBeautifySettingViews(Z)V

    .line 5380
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_0

    .line 5384
    :cond_16
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarView:Landroid/view/View;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 5386
    const/4 v8, 0x0

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->showBeautifyEnableLayout(Z)V

    .line 5387
    const/4 v8, 0x0

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->showBeautifySettingViews(Z)V

    .line 5390
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    if-gtz v8, :cond_17

    .line 5391
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x43b70000    # 366.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_0

    .line 5393
    :cond_17
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_0
.end method

.method public getInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .locals 1

    .prologue
    .line 6315
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    return-object v0
.end method

.method public getViewMode()I
    .locals 1

    .prologue
    .line 5436
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mViewMode:I

    return v0
.end method

.method protected initView(Ljava/lang/String;)V
    .locals 8
    .param p1, "customImagePath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 2352
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    invoke-direct {v0, v1, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;-><init>(Landroid/content/Context;Ljava/util/ArrayList;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    .line 2353
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->getPenDataList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenDataList:Ljava/util/List;

    .line 2354
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    const/16 v1, 0x24

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->getPresetData(I)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    .line 2356
    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2357
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    .line 2359
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenDataList:Ljava/util/List;

    if-nez v0, :cond_1

    .line 2360
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenDataList:Ljava/util/List;

    .line 2362
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetDataList:Ljava/util/List;

    if-nez v0, :cond_2

    .line 2363
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetDataList:Ljava/util/List;

    .line 2366
    :cond_2
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    .line 2367
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;-><init>(Landroid/content/Context;ILjava/util/List;Ljava/lang/String;Ljava/util/ArrayList;F)V

    .line 2366
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    .line 2368
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->setPenPlugin(Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;)V

    .line 2369
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->totalLayout()V

    .line 2372
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->titleTextView:Landroid/widget/TextView;

    .line 2373
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->EXIT_BUTTON_WIDTH:I

    mul-int/lit8 v3, v3, 0x2

    rsub-int v3, v3, 0x149

    add-int/lit8 v3, v3, -0x8

    int-to-float v3, v3

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 2372
    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->findMinValue(Landroid/widget/TextView;I)V

    .line 2375
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    if-eqz v0, :cond_3

    .line 2376
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetGridView:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2379
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    .line 2380
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-lt v7, v0, :cond_4

    .line 2383
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->showBeautifySettingViews(Z)V

    .line 2385
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->initColorSelecteView()V

    .line 2387
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->ColorPickerSettingInit()V

    .line 2388
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->notifyDataSetChanged()V

    .line 2389
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->presetDisplay()V

    .line 2391
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setVisibility(I)V

    .line 2392
    return-void

    .line 2381
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2380
    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public loadPreferences()V
    .locals 3

    .prologue
    .line 2399
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 2400
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-nez v2, :cond_1

    .line 2414
    :cond_0
    :goto_0
    return-void

    .line 2403
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->getCurrentPenName()Ljava/lang/String;

    move-result-object v0

    .line 2404
    .local v0, "currentPenName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v1

    .line 2406
    .local v1, "penTypeViewIndex":I
    if-gez v1, :cond_2

    .line 2407
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->removeCurrentPenData()V

    .line 2408
    const/4 v1, 0x1

    .line 2412
    :cond_2
    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penSelectIndex(I)V

    .line 2413
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->clearSharedPenData()V

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 113
    :try_start_0
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mOrientation:I

    .line 115
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mFirstLongPress:Z

    if-nez v1, :cond_0

    .line 116
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 118
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    .line 119
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->rotatePosition()V

    .line 123
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 131
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    if-eqz v1, :cond_1

    .line 132
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->rotatePosition()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :cond_1
    :goto_0
    return-void

    .line 136
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onScroll(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .prologue
    .line 6832
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 7
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 6838
    :try_start_0
    iput p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mWindowHeight:I

    .line 6839
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mExpandFlag:Z

    if-nez v4, :cond_0

    .line 6841
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x3f800000    # 1.0f

    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float/2addr v5, v6

    .line 6842
    const/high16 v6, 0x42240000    # 41.0f

    add-float/2addr v5, v6

    .line 6841
    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    .line 6846
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x437a0000    # 250.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 6847
    .local v3, "minWidth":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43230000    # 163.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 6849
    .local v2, "minHeight":I
    if-lt p1, v3, :cond_1

    if-ge p2, v2, :cond_2

    .line 6850
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 6851
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->checkPosition()V

    .line 6854
    :cond_2
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 6855
    .local v1, "handler":Landroid/os/Handler;
    new-instance v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$57;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$57;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    invoke-virtual {v1, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 6863
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6869
    .end local v1    # "handler":Landroid/os/Handler;
    .end local v2    # "minHeight":I
    .end local v3    # "minWidth":I
    :goto_0
    return-void

    .line 6865
    :catch_0
    move-exception v0

    .line 6866
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 513
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 10
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 64
    :try_start_0
    sget-boolean v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMagicPenRemoved:Z

    if-nez v3, :cond_0

    .line 65
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-lt v1, v3, :cond_2

    .line 94
    .end local v1    # "i":I
    :cond_0
    :goto_1
    if-ne p1, p0, :cond_1

    if-nez p2, :cond_1

    .line 96
    const/4 v3, 0x2

    new-array v2, v3, [I

    .line 97
    .local v2, "location":[I
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getLocationOnScreen([I)V

    .line 99
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 101
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mMovableRect:Landroid/graphics/Rect;

    new-instance v4, Landroid/graphics/Rect;

    aget v5, v2, v8

    aget v6, v2, v9

    aget v7, v2, v8

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getWidth()I

    move-result v8

    add-int/2addr v7, v8

    aget v8, v2, v9

    .line 102
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getHeight()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 101
    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v3

    .line 102
    if-nez v3, :cond_1

    .line 103
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->checkPosition()V

    .line 107
    .end local v2    # "location":[I
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onVisibilityChanged(Landroid/view/View;I)V

    .line 108
    return-void

    .line 66
    .restart local v1    # "i":I
    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    const/16 v4, 0x3ed

    if-ne v3, v4, :cond_4

    .line 67
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->currenMagicPenHeight:I

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setMagicPenMode(I)V

    .line 68
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 72
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    if-nez v3, :cond_3

    .line 73
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaTextView:Landroid/widget/TextView;

    const-string v4, "1%"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42340000    # 45.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setX(F)V

    .line 75
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40a00000    # 5.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setY(F)V

    .line 78
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->handler:Landroid/os/Handler;

    new-instance v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$51;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$51;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    .line 84
    const-wide/16 v6, 0xc8

    .line 78
    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    .line 65
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 89
    .end local v1    # "i":I
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_1
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 4
    .param p1, "visibility"    # I

    .prologue
    .line 6805
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    if-nez v1, :cond_1

    .line 6822
    :cond_0
    :goto_0
    return-void

    .line 6808
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 6811
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->isShown()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 6812
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->hide()V

    .line 6813
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v1, :cond_2

    .line 6814
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v2, 0x2

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreCanvasPenAction:I

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 6815
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v2, 0x1

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreCanvasFingerAction:I

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6821
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onWindowVisibilityChanged(I)V

    goto :goto_0

    .line 6818
    :catch_0
    move-exception v0

    .line 6819
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method public removePen(I)V
    .locals 10
    .param p1, "penIndex"    # I

    .prologue
    const/high16 v9, 0x43700000    # 240.0f

    const/high16 v8, 0x42960000    # 75.0f

    const/high16 v7, 0x428a0000    # 69.0f

    .line 6548
    const/4 v5, 0x5

    if-ne p1, v5, :cond_0

    .line 6549
    const/4 v5, 0x1

    sput-boolean v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMagicPenRemoved:Z

    .line 6552
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    if-eqz v5, :cond_1

    .line 6553
    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mNumberOfPenExist:I

    if-lt p1, v5, :cond_2

    .line 6580
    :cond_1
    :goto_0
    return-void

    .line 6556
    :cond_2
    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mNumberOfPenExist:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mNumberOfPenExist:I

    .line 6558
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 6559
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 6558
    invoke-direct {v1, v5, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 6560
    .local v1, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 6561
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 6560
    invoke-direct {v2, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 6562
    .local v2, "layoutParams2":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v5, 0xd

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 6563
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->penTypeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6564
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    invoke-virtual {v5, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6566
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    .line 6567
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, p1}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getId()I

    move-result v0

    .line 6568
    .local v0, "id":I
    if-eqz p1, :cond_3

    .line 6569
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, p1}, Landroid/widget/RelativeLayout;->removeViewAt(I)V

    .line 6570
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    add-int/lit8 v6, p1, -0x1

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/view/View;->setId(I)V

    goto :goto_0

    .line 6572
    :cond_3
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 6573
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 6572
    invoke-direct {v3, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 6574
    .local v3, "relativeParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    add-int/lit8 v6, p1, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 6575
    .local v4, "v":Landroid/view/View;
    const/16 v5, 0x9

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 6576
    invoke-virtual {v4, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6577
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, p1}, Landroid/widget/RelativeLayout;->removeViewAt(I)V

    goto/16 :goto_0
.end method

.method public savePreferences()V
    .locals 9

    .prologue
    .line 2421
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-nez v6, :cond_1

    .line 2460
    :cond_0
    :goto_0
    return-void

    .line 2425
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2426
    .local v4, "penDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;>;"
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2428
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;>;"
    const/4 v3, 0x0

    .local v3, "penCount":I
    :goto_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v3, v6, :cond_3

    .line 2450
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-eqz v6, :cond_0

    .line 2451
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    invoke-virtual {v6, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->setPenDataList(Ljava/util/ArrayList;)V

    .line 2452
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->setCurrentPenName(Ljava/lang/String;)V

    .line 2453
    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    if-eqz v6, :cond_0

    .line 2454
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_2
    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lt v0, v6, :cond_5

    .line 2457
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    sget-object v7, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->setPresetData(Ljava/util/List;)V

    goto :goto_0

    .line 2429
    .end local v0    # "count":I
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2432
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    .line 2434
    .local v5, "penPluginInfo":Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;
    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    if-nez v6, :cond_4

    .line 2435
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->loadPenPlugin(Landroid/content/Context;Ljava/lang/String;)V

    .line 2436
    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    if-nez v6, :cond_4

    .line 2428
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2440
    :cond_4
    new-instance v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    .line 2442
    .local v2, "localPenData":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 2443
    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    invoke-interface {v6}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getSize()F

    move-result v6

    iput v6, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 2444
    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    invoke-interface {v6}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getColor()I

    move-result v6

    iput v6, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 2447
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2455
    .end local v2    # "localPenData":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .end local v5    # "penPluginInfo":Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;
    .restart local v0    # "count":I
    :cond_5
    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->setPresetIndex(I)V

    .line 2454
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public setActionListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$ActionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$ActionListener;

    .prologue
    .line 6647
    return-void
.end method

.method public setBeautifyOptionEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 6595
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->hasBeautifyPen()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6596
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSupportBeautifyPen:Z

    .line 6598
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    .line 6599
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 6602
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isChinesePen(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isBeautifyPen(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6603
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->drawExpendImage(Ljava/lang/String;)V

    .line 6606
    :cond_2
    return-void
.end method

.method public setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V
    .locals 3
    .param p1, "canvasView"    # Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .prologue
    const/16 v2, 0x438

    .line 6262
    if-nez p1, :cond_1

    .line 6300
    :cond_0
    :goto_0
    return-void

    .line 6265
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$54;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$54;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V

    invoke-interface {p1, p0, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setBackgroundColorChangeListener(Ljava/lang/Object;Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;)V

    .line 6275
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-nez v0, :cond_5

    .line 6276
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .line 6278
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_4

    .line 6279
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 6280
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    .line 6284
    :goto_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    if-nez v0, :cond_2

    .line 6285
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    .line 6296
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->loadPreferences()V

    .line 6297
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-eqz v0, :cond_0

    .line 6298
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto :goto_0

    .line 6282
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    goto :goto_1

    .line 6288
    :cond_4
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    goto :goto_2

    .line 6291
    :cond_5
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .line 6292
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-eqz v0, :cond_2

    .line 6293
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto :goto_2
.end method

.method public setColorPickerPosition(II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 2706
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    if-eqz v0, :cond_0

    .line 2707
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->movePosition(II)V

    .line 2710
    :cond_0
    return-void
.end method

.method public setIndicatorPosition(I)V
    .locals 0
    .param p1, "x"    # I

    .prologue
    .line 2687
    return-void
.end method

.method public setInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 14
    .param p1, "settingInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .prologue
    .line 6334
    if-nez p1, :cond_0

    .line 6335
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "E_INVALID_ARG : parameter \'settingInfo\' is null."

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 6338
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v9, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v5

    .line 6340
    .local v5, "penIndex":I
    if-ltz v5, :cond_e

    .line 6342
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v9, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iput v9, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 6343
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-boolean v9, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    iput-boolean v9, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    .line 6344
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v9, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iput-object v9, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 6345
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v9, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iput v9, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 6346
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v9, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    iput-object v9, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    .line 6348
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    .line 6350
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v8

    if-nez v8, :cond_1

    .line 6351
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenContext:Landroid/content/Context;

    iget-object v10, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->loadPenPlugin(Landroid/content/Context;Ljava/lang/String;)V

    .line 6352
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v8

    if-nez v8, :cond_1

    .line 6450
    :goto_0
    return-void

    .line 6356
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v8

    invoke-interface {v8}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getMinSettingValue()F

    move-result v4

    .line 6357
    .local v4, "min":F
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v8

    invoke-interface {v8}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getMaxSettingValue()F

    move-result v3

    .line 6359
    .local v3, "max":F
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v8, :cond_a

    .line 6360
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v8}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v9}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v9

    if-ge v8, v9, :cond_9

    .line 6361
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v8}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v8

    iput v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    .line 6365
    :goto_1
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    if-nez v8, :cond_2

    .line 6366
    const/16 v8, 0x438

    iput v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    .line 6372
    :cond_2
    :goto_2
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    int-to-float v8, v8

    mul-float/2addr v8, v3

    float-to-double v8, v8

    const-wide v10, 0x4076800000000000L    # 360.0

    div-double/2addr v8, v10

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v10, v10, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    float-to-double v10, v10

    cmpg-double v8, v8, v10

    if-gez v8, :cond_3

    .line 6373
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    int-to-float v9, v9

    mul-float/2addr v9, v3

    float-to-double v10, v9

    const-wide v12, 0x4076800000000000L    # 360.0

    div-double/2addr v10, v12

    double-to-float v9, v10

    iput v9, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 6375
    :cond_3
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    int-to-float v8, v8

    mul-float/2addr v8, v4

    float-to-double v8, v8

    const-wide v10, 0x4076800000000000L    # 360.0

    div-double/2addr v8, v10

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v10, v10, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    float-to-double v10, v10

    cmpl-double v8, v8, v10

    if-lez v8, :cond_4

    .line 6376
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    int-to-float v9, v9

    mul-float/2addr v9, v4

    float-to-double v10, v9

    const-wide v12, 0x4076800000000000L    # 360.0

    div-double/2addr v10, v12

    double-to-float v9, v10

    iput v9, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 6378
    :cond_4
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-interface {v8, v9}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setSize(F)V

    .line 6379
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-interface {v8, v9}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setColor(I)V

    .line 6380
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v8

    .line 6381
    const/4 v9, 0x1

    invoke-interface {v8, v9}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 6382
    iget v8, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    shr-int/lit8 v8, v8, 0x18

    and-int/lit16 v8, v8, 0xff

    iput v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlpha:I

    .line 6384
    :cond_5
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v8

    .line 6385
    const/4 v9, 0x4

    invoke-interface {v8, v9}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 6386
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v8

    .line 6387
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v9, v9, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-interface {v8, v9}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setAdvancedSetting(Ljava/lang/String;)V

    .line 6388
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setBeautifyAdvanceStringToCurrentAdvanceData(Ljava/lang/String;)V

    .line 6391
    :cond_6
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->isLoaded()Z

    move-result v8

    if-nez v8, :cond_7

    .line 6392
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->setLoaded(Z)V

    .line 6394
    :cond_7
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginCount:I

    if-lt v1, v8, :cond_b

    .line 6399
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isBeautifyPen(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 6400
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    const-string v9, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v0

    .line 6401
    .local v0, "chinesePenIndex":I
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/view/View;->setSelected(Z)V

    .line 6405
    .end local v0    # "chinesePenIndex":I
    :goto_4
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v7, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 6406
    .local v7, "tempSize":F
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    sub-float v9, v3, v4

    const/high16 v10, 0x41200000    # 10.0f

    mul-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/SeekBar;->setMax(I)V

    .line 6407
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iput v7, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 6408
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    const/high16 v9, 0x43b40000    # 360.0f

    mul-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    sub-float/2addr v8, v4

    const/high16 v9, 0x41200000    # 10.0f

    mul-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 6410
    .local v6, "progress":I
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v8, v6}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 6411
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    const v10, 0xffffff

    and-int/2addr v9, v10

    const/high16 v10, -0x1000000

    or-int/2addr v9, v10

    invoke-virtual {v8, v9}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 6412
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    const/16 v9, 0xff

    invoke-virtual {v8, v9}, Landroid/graphics/drawable/GradientDrawable;->setAlpha(I)V

    .line 6414
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlpha:I

    int-to-float v9, v9

    const/high16 v10, 0x437f0000    # 255.0f

    div-float/2addr v9, v10

    const/high16 v10, 0x42c60000    # 99.0f

    mul-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 6415
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v8, v9}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 6417
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    iget v9, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setColor(I)V

    .line 6419
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v8, v9}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 6420
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v8}, Landroid/widget/SeekBar;->invalidate()V

    .line 6421
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v8}, Landroid/widget/SeekBar;->invalidate()V

    .line 6422
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v8}, Landroid/widget/SeekBar;->invalidate()V

    .line 6423
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v8}, Landroid/widget/SeekBar;->invalidate()V

    .line 6425
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v9, v9, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setPenType(Ljava/lang/String;)V

    .line 6426
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeSize(F)V

    .line 6427
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeColor(I)V

    .line 6428
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v9, v9, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeAdvancedSetting(Ljava/lang/String;)V

    .line 6429
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->invalidate()V

    .line 6431
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->setColorPickerColor(I)V

    .line 6432
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v8, :cond_8

    .line 6433
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v8}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v2

    .line 6434
    .local v2, "info":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    if-eqz v2, :cond_8

    .line 6435
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iput v8, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 6436
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-boolean v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    iput-boolean v8, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    .line 6437
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iput-object v8, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 6438
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iput v8, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 6439
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    iput-object v8, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    .line 6440
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v8, v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 6444
    .end local v2    # "info":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    :cond_8
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->drawExpendImage(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6363
    .end local v1    # "i":I
    .end local v6    # "progress":I
    .end local v7    # "tempSize":F
    :cond_9
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v8}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v8

    iput v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    goto/16 :goto_1

    .line 6369
    :cond_a
    const/16 v8, 0x438

    iput v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasSize:I

    goto/16 :goto_2

    .line 6395
    .restart local v1    # "i":I
    :cond_b
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-le v8, v1, :cond_c

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_c

    .line 6396
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setSelected(Z)V

    .line 6394
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_3

    .line 6403
    :cond_d
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/view/View;->setSelected(Z)V

    goto/16 :goto_4

    .line 6447
    .end local v1    # "i":I
    .end local v3    # "max":F
    .end local v4    # "min":F
    :cond_e
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "E_INVALID_ARG : parameter \'SettingPenInfo.name\' is incorrect."

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8
.end method

.method public setLayoutHeight(I)V
    .locals 1
    .param p1, "height"    # I

    .prologue
    .line 495
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 496
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 497
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 498
    return-void
.end method

.method public setPenInfoList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 6525
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;>;"
    if-eqz p1, :cond_0

    .line 6526
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenDataList:Ljava/util/List;

    .line 6528
    :cond_0
    return-void
.end method

.method public setPopup(Z)V
    .locals 5
    .param p1, "open"    # Z

    .prologue
    const/16 v3, 0x8

    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 1411
    if-eqz p1, :cond_2

    .line 1412
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupMaxButton:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1413
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupMinButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1418
    const/4 v0, 0x0

    .line 1419
    .local v0, "bodyLayoutHeight":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1420
    const/16 v0, 0x1ac

    .line 1427
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getHeight()I

    move-result v1

    .line 1428
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    add-int/lit8 v3, v0, 0x29

    add-int/lit16 v3, v3, 0xc8

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1427
    invoke-direct {p0, v4, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->playScrollAnimationForBottomBar(III)V

    .line 1437
    .end local v0    # "bodyLayoutHeight":I
    :goto_1
    return-void

    .line 1421
    .restart local v0    # "bodyLayoutHeight":I
    :cond_0
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMagicPenEnable:Z

    if-eqz v1, :cond_1

    .line 1422
    const/16 v0, 0x10b

    .line 1423
    goto :goto_0

    .line 1424
    :cond_1
    const/16 v0, 0x16e

    goto :goto_0

    .line 1430
    .end local v0    # "bodyLayoutHeight":I
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupMaxButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1431
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupMinButton:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1435
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42240000    # 41.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {p0, v4, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->playScrollAnimationForBottomBar(III)V

    goto :goto_1
.end method

.method public setPopupListenr(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PopupListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PopupListener;

    .prologue
    .line 505
    if-eqz p1, :cond_0

    .line 506
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PopupListener;

    .line 508
    :cond_0
    return-void
.end method

.method public setPosition(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 2729
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2731
    .local v0, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2732
    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2734
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2736
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mFirstLongPress:Z

    if-eqz v1, :cond_0

    .line 2737
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mLeftMargin:I

    sub-int/2addr v1, p1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mLeftMargin:I

    .line 2738
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTopMargin:I

    sub-int/2addr v1, p2

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTopMargin:I

    .line 2741
    :cond_0
    return-void
.end method

.method public setPresetListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PresetListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PresetListener;

    .prologue
    .line 6625
    if-eqz p1, :cond_0

    .line 6626
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PresetListener;

    .line 6628
    :cond_0
    return-void
.end method

.method public setViewMode(I)V
    .locals 11
    .param p1, "viewMode"    # I

    .prologue
    const/high16 v10, 0x425c0000    # 55.0f

    const/high16 v9, 0x42240000    # 41.0f

    const/high16 v8, 0x3f800000    # 1.0f

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 5468
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mViewMode:I

    .line 5469
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->isShown()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5470
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->hide()V

    .line 5471
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_0

    .line 5472
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v3, 0x2

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreCanvasPenAction:I

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 5473
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v3, 0x1

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreCanvasFingerAction:I

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 5480
    :cond_0
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mViewMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    .line 5481
    const/16 v2, 0x34

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->deltaOfMiniMode:I

    .line 5482
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    .line 5483
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42e20000    # 113.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 5482
    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 5484
    .local v0, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5485
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMinimumMode:Z

    .line 5492
    .end local v0    # "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    :goto_0
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    .line 5493
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43230000    # 163.0f

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->deltaOfMiniMode:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 5492
    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 5494
    .restart local v0    # "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5497
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mViewMode:I

    packed-switch v2, :pswitch_data_0

    .line 5709
    iput v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mViewMode:I

    .line 5710
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 5711
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 5712
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 5713
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5714
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5715
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setVisibility(I)V

    .line 5716
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5721
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->drawExpendImage(Ljava/lang/String;)V

    .line 5722
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    if-lez v2, :cond_d

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    if-le v2, v3, :cond_d

    .line 5723
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    .line 5724
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v4, v8, v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v2, v3

    .line 5725
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v2, v3

    .line 5723
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    .line 5732
    :goto_1
    :pswitch_0
    iput v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCount:I

    .line 5734
    const/4 v1, -0x1

    .line 5735
    .local v1, "realIndex":I
    const/4 v1, 0x0

    :goto_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_e

    .line 5741
    :cond_1
    sget-boolean v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isHighlightPenRemoved:Z

    if-nez v2, :cond_2

    sget-boolean v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMagicPenRemoved:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    if-eqz v2, :cond_2

    .line 5742
    const/4 v2, 0x5

    if-ge v1, v2, :cond_f

    .line 5743
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 5744
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    .line 5745
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 5744
    invoke-virtual {v2, v3, v6}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    .line 5746
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v2, v6}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 5747
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->playScrollAnimation(IFF)V

    .line 5758
    :cond_2
    :goto_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    int-to-double v4, v3

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    div-double/2addr v4, v6

    double-to-float v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 5759
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    if-nez v2, :cond_3

    .line 5760
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaTextView:Landroid/widget/TextView;

    const-string v3, "1%"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5761
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42340000    # 45.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setX(F)V

    .line 5762
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setY(F)V

    .line 5765
    :cond_3
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMagicPenEnable:Z

    if-eqz v2, :cond_4

    .line 5766
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->currenMagicPenHeight:I

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setMagicPenMode(I)V

    .line 5769
    :cond_4
    return-void

    .line 5487
    .end local v0    # "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v1    # "realIndex":I
    :cond_5
    iput v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->deltaOfMiniMode:I

    .line 5488
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setVisibility(I)V

    .line 5489
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMinimumMode:Z

    goto/16 :goto_0

    .line 5499
    .restart local v0    # "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 5500
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 5501
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5502
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 5503
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5504
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5505
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setVisibility(I)V

    .line 5506
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5511
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->drawExpendImage(Ljava/lang/String;)V

    .line 5512
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    if-lez v2, :cond_6

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    if-le v2, v3, :cond_6

    .line 5513
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    .line 5514
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v4, v8, v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v2, v3

    .line 5515
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v2, v3

    .line 5513
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_1

    .line 5517
    :cond_6
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_1

    .line 5522
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 5523
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 5524
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5525
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 5526
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5527
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5528
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setVisibility(I)V

    .line 5529
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5535
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->drawExpendImage(Ljava/lang/String;)V

    .line 5536
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_7

    .line 5537
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x43450000    # 197.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_1

    .line 5539
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x43170000    # 151.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_1

    .line 5543
    :pswitch_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 5544
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 5545
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5546
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 5547
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5548
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5549
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setVisibility(I)V

    .line 5550
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5555
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->drawExpendImage(Ljava/lang/String;)V

    .line 5557
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    if-lez v2, :cond_8

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    if-le v2, v3, :cond_8

    .line 5558
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    .line 5559
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v4, v8, v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v2, v3

    .line 5560
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v2, v3

    .line 5558
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_1

    .line 5562
    :cond_8
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_1

    .line 5567
    :pswitch_4
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 5568
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 5569
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5570
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 5571
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5572
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5573
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setVisibility(I)V

    .line 5574
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setVisibility(I)V

    .line 5575
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5578
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetAddButton:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 5581
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->drawExpendImage(Ljava/lang/String;)V

    .line 5582
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    if-lez v2, :cond_9

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    if-le v2, v3, :cond_9

    .line 5583
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    .line 5584
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v4, v8, v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v2, v3

    .line 5585
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v2, v3

    .line 5583
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_1

    .line 5587
    :cond_9
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_1

    .line 5591
    :pswitch_5
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 5592
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 5593
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5594
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 5595
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5596
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5597
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setVisibility(I)V

    .line 5598
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setVisibility(I)V

    .line 5599
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 5602
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPresetAddButton:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 5604
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mLine1Button:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 5613
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->drawExpendImage(Ljava/lang/String;)V

    .line 5614
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    if-lez v2, :cond_a

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    if-le v2, v3, :cond_a

    .line 5615
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    .line 5616
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v4, v8, v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v2, v3

    .line 5617
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v2, v3

    .line 5615
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_1

    .line 5619
    :cond_a
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_1

    .line 5623
    :pswitch_6
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 5624
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 5625
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5626
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 5627
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 5628
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 5629
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5634
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->drawExpendImage(Ljava/lang/String;)V

    .line 5636
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42960000    # 75.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_1

    .line 5640
    :pswitch_7
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 5641
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 5642
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 5643
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 5644
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5645
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 5646
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5651
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->drawExpendImage(Ljava/lang/String;)V

    .line 5652
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_b

    .line 5653
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42b80000    # 92.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_1

    .line 5655
    :cond_b
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42380000    # 46.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_1

    .line 5659
    :pswitch_8
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 5660
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 5661
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 5662
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 5663
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 5664
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setVisibility(I)V

    .line 5665
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setVisibility(I)V

    .line 5666
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5667
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5672
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->drawExpendImage(Ljava/lang/String;)V

    .line 5674
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42e40000    # 114.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_1

    .line 5684
    :pswitch_9
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 5685
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 5687
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 5688
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 5689
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 5690
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 5691
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setVisibility(I)V

    .line 5692
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5697
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->drawExpendImage(Ljava/lang/String;)V

    .line 5698
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    if-lez v2, :cond_c

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    if-le v2, v3, :cond_c

    .line 5699
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    .line 5700
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScale:F

    div-float v4, v8, v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v2, v3

    .line 5701
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v2, v3

    .line 5699
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_1

    .line 5703
    :cond_c
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_1

    .line 5727
    :cond_d
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBodyLayoutHeight:I

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V

    goto/16 :goto_1

    .line 5736
    .restart local v1    # "realIndex":I
    :cond_e
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->isSelected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 5735
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    .line 5750
    :cond_f
    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 5751
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v3}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v3

    int-to-float v3, v3

    .line 5752
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    .line 5751
    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->playScrollAnimation(IFF)V

    goto/16 :goto_3

    .line 5497
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public setVisibility(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    .line 6658
    if-nez p1, :cond_0

    .line 6659
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mLoaded:Z

    if-nez v0, :cond_0

    .line 6660
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->loadImage()V

    .line 6664
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    if-nez v0, :cond_2

    .line 6685
    :cond_1
    :goto_0
    return-void

    .line 6667
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 6670
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 6671
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->hide()V

    .line 6672
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_3

    .line 6673
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v1, 0x2

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreCanvasPenAction:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 6674
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v1, 0x1

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreCanvasFingerAction:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 6683
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isPresetClicked:Z

    .line 6684
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method
