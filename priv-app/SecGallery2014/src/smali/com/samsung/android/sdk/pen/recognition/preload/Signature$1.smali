.class Lcom/samsung/android/sdk/pen/recognition/preload/Signature$1;
.super Landroid/os/Handler;
.source "Signature.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/recognition/preload/Signature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/recognition/preload/Signature;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$1;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/Signature;

    .line 288
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 291
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$HandleInfo;

    .line 292
    .local v0, "info":Lcom/samsung/android/sdk/pen/recognition/preload/Signature$HandleInfo;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$1;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/Signature;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->access$0(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 293
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$1;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/Signature;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->access$0(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/Signature$HandleInfo;->mInput:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$HandleInfo;->access$0(Lcom/samsung/android/sdk/pen/recognition/preload/Signature$HandleInfo;)Ljava/util/List;

    move-result-object v2

    iget-boolean v3, v0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$HandleInfo;->mResult:Z

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;->onResult(Ljava/util/List;Z)V

    .line 295
    :cond_0
    return-void
.end method
