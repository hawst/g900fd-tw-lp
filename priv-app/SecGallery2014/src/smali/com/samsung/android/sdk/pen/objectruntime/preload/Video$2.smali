.class Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;
.super Ljava/lang/Object;
.source "Video.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->start(Ljava/lang/Object;Landroid/graphics/RectF;Landroid/graphics/PointF;FLandroid/graphics/PointF;Landroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDestroy()V
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    const/4 v1, 0x0

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cancel(I)V
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$1(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;I)V

    .line 268
    return-void
.end method

.method public onResult(Landroid/content/Intent;)V
    .locals 14
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 188
    if-nez p1, :cond_0

    .line 189
    const-string v0, "Video"

    const-string v2, "Video file was not selected."

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cancel(I)V
    invoke-static {v0, v11}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$1(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;I)V

    .line 263
    :goto_0
    return-void

    .line 193
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 194
    .local v1, "uri":Landroid/net/Uri;
    if-nez v1, :cond_1

    .line 195
    const-string v0, "Video"

    const-string v2, "The extra data of video file is null."

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .end local v1    # "uri":Landroid/net/Uri;
    check-cast v1, Landroid/net/Uri;

    .line 199
    .restart local v1    # "uri":Landroid/net/Uri;
    :cond_1
    const/4 v10, 0x0

    .line 201
    .local v10, "videoCursor":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$2(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 202
    new-array v2, v13, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v2, v11

    const-string v4, "_data"

    aput-object v4, v2, v12

    move-object v4, v3

    move-object v5, v3

    .line 201
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 205
    if-eqz v10, :cond_5

    .line 206
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 207
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 208
    const-string v0, "Video"

    const-string v2, "The extra data of video file is empty."

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 210
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cancel(I)V
    invoke-static {v0, v11}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$1(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;I)V

    goto :goto_0

    .line 214
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    .line 215
    const-string v2, "_data"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 214
    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$3(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Ljava/lang/String;)V

    .line 216
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 218
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoFilePath:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$4(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v12}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 220
    .local v9, "srcBitmap":Landroid/graphics/Bitmap;
    if-nez v9, :cond_3

    .line 221
    const-string v0, "Video"

    const-string v2, "The bitmap of video is null"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cancel(I)V
    invoke-static {v0, v11}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$1(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;I)V

    goto :goto_0

    .line 226
    :cond_3
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8}, Landroid/graphics/RectF;-><init>()V

    .line 227
    .local v8, "noneRatioRect":Landroid/graphics/RectF;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->getRatioRect(IILandroid/graphics/RectF;)Landroid/graphics/RectF;
    invoke-static {v2, v3, v4, v8}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$5(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;IILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$6(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Landroid/graphics/RectF;)V

    .line 228
    new-array v6, v13, [Ljava/lang/Object;

    .line 229
    .local v6, "args":[Ljava/lang/Object;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$7(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Landroid/graphics/RectF;

    move-result-object v2

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->convertAbsolute(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$8(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    aput-object v0, v6, v11

    .line 230
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v6, v12

    .line 231
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$9(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "setRect"

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETRECT_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;
    invoke-static {}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$10()[Ljava/lang/Class;

    move-result-object v4

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    invoke-static {v0, v2, v3, v4, v6}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$11(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->createPlayVideoBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;
    invoke-static {v0, v9, v8}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$12(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 234
    .local v7, "bitmap":Landroid/graphics/Bitmap;
    if-nez v7, :cond_4

    .line 235
    const-string v0, "Video"

    const-string v2, "PlayVideo merge process is failed"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cancel(I)V
    invoke-static {v0, v11}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$1(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;I)V

    goto/16 :goto_0

    .line 240
    :cond_4
    new-array v6, v12, [Ljava/lang/Object;

    .line 241
    aput-object v7, v6, v11

    .line 242
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$9(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "setImage"

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETIMAGE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;
    invoke-static {}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$13()[Ljava/lang/Class;

    move-result-object v4

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    invoke-static {v0, v2, v3, v4, v6}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$11(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    new-array v6, v13, [Ljava/lang/Object;

    .line 245
    const-string v0, "VideoPath"

    aput-object v0, v6, v11

    .line 246
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoFilePath:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$4(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v12

    .line 247
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$9(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "setExtraDataString"

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETEXTRADATASTRING_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;
    invoke-static {}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$14()[Ljava/lang/Class;

    move-result-object v4

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    invoke-static {v0, v2, v3, v4, v6}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$11(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    new-array v6, v12, [Ljava/lang/Object;

    .line 250
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v6, v11

    .line 251
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$9(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "setRotatable"

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETROTATABLE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;
    invoke-static {}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$15()[Ljava/lang/Class;

    move-result-object v4

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    invoke-static {v0, v2, v3, v4, v6}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$11(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    new-array v6, v12, [Ljava/lang/Object;

    .line 254
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v11

    .line 255
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$9(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "setResizeOption"

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETRESIZEOPTION_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;
    invoke-static {}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$16()[Ljava/lang/Class;

    move-result-object v4

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    invoke-static {v0, v2, v3, v4, v6}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$11(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$17(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$7(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Landroid/graphics/RectF;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$9(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;->onObjectUpdated(Landroid/graphics/RectF;Ljava/lang/Object;)V

    .line 258
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->complete()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$18(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)V

    goto/16 :goto_0

    .line 260
    .end local v6    # "args":[Ljava/lang/Object;
    .end local v7    # "bitmap":Landroid/graphics/Bitmap;
    .end local v8    # "noneRatioRect":Landroid/graphics/RectF;
    .end local v9    # "srcBitmap":Landroid/graphics/Bitmap;
    :cond_5
    const-string v0, "Video"

    const-string v2, "The extra data of video file query is failed."

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cancel(I)V
    invoke-static {v0, v11}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$1(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;I)V

    goto/16 :goto_0
.end method
