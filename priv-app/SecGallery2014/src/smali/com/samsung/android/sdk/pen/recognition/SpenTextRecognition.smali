.class public final Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;
.super Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;
.source "SpenTextRecognition.java"


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pluginObject"    # Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;)V

    .line 28
    return-void
.end method


# virtual methods
.method public getLanguage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : Recognition is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenLanguageRecognitionInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenLanguageRecognitionInterface;->getLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSupportedLanguage()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : Recognition is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenLanguageRecognitionInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenLanguageRecognitionInterface;->getSupportedLanguage()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 2
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    if-nez v0, :cond_0

    .line 85
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : Recognition is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenLanguageRecognitionInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenLanguageRecognitionInterface;->setLanguage(Ljava/lang/String;)V

    .line 89
    return-void
.end method
