.class Lcom/samsung/android/sdk/pen/engine/SpenInView$SpenIconMoreHandler;
.super Landroid/os/Handler;
.source "SpenInView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenInView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SpenIconMoreHandler"
.end annotation


# static fields
.field static isSpenIconMore:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6404
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SpenIconMoreHandler;->isSpenIconMore:Z

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 6403
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 6415
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$62()Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    move-result-object v1

    if-nez v1, :cond_1

    .line 6451
    :cond_0
    :goto_0
    return-void

    .line 6418
    :cond_1
    sget-boolean v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$SpenIconMoreHandler;->isSpenIconMore:Z

    if-eqz v1, :cond_2

    .line 6419
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->getIconType()I

    move-result v1

    if-eq v1, v2, :cond_0

    .line 6421
    :try_start_0
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$62()Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->setHoveringSpenIcon(I)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 6422
    :catch_0
    move-exception v0

    .line 6423
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v1, "SpenInView"

    const-string v2, "SpenIconMoreHandler ClassNotFoundException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 6424
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v0

    .line 6425
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v1, "SpenInView"

    const-string v2, "SpenIconMoreHandler NoSuchMethodException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 6426
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v0

    .line 6427
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "SpenInView"

    const-string v2, "SpenIconMoreHandler IllegalArgumentException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 6428
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 6429
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v1, "SpenInView"

    const-string v2, "SpenIconMoreHandler IllegalAccessException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 6430
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v0

    .line 6431
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v1, "SpenInView"

    const-string v2, "SpenIconMoreHandler InvocationTargetException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 6435
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :cond_2
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->getIconType()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 6437
    :try_start_1
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$62()Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->setHoveringSpenIcon(I)V
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_9

    goto :goto_0

    .line 6438
    :catch_5
    move-exception v0

    .line 6439
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v1, "SpenInView"

    const-string v2, "SpenIconMoreHandler ClassNotFoundException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 6440
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_6
    move-exception v0

    .line 6441
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v1, "SpenInView"

    const-string v2, "SpenIconMoreHandler NoSuchMethodException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 6442
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_7
    move-exception v0

    .line 6443
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "SpenInView"

    const-string v2, "SpenIconMoreHandler IllegalArgumentException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 6444
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_8
    move-exception v0

    .line 6445
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v1, "SpenInView"

    const-string v2, "SpenIconMoreHandler IllegalAccessException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 6446
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_9
    move-exception v0

    .line 6447
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v1, "SpenInView"

    const-string v2, "SpenIconMoreHandler InvocationTargetException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method setSpenIconMore(Z)V
    .locals 1
    .param p1, "bValue"    # Z

    .prologue
    .line 6407
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$62()Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    move-result-object v0

    if-nez v0, :cond_0

    .line 6411
    :goto_0
    return-void

    .line 6410
    :cond_0
    sput-boolean p1, Lcom/samsung/android/sdk/pen/engine/SpenInView$SpenIconMoreHandler;->isSpenIconMore:Z

    goto :goto_0
.end method
