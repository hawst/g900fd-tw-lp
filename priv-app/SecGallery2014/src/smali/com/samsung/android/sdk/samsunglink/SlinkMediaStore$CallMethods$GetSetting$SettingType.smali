.class public final enum Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;
.super Ljava/lang/Enum;
.source "SlinkMediaStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SettingType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

.field public static final enum BOOLEAN:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

.field public static final enum DOUBLE:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

.field public static final enum FLOAT:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

.field public static final enum INT:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

.field public static final enum LONG:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

.field public static final enum STRING:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4372
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    const-string v1, "BOOLEAN"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->BOOLEAN:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    .line 4373
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    const-string v1, "DOUBLE"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->DOUBLE:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    .line 4374
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    const-string v1, "FLOAT"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->FLOAT:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    .line 4375
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    const-string v1, "INT"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->INT:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    .line 4376
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    const-string v1, "LONG"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->LONG:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    .line 4377
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    const-string v1, "STRING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->STRING:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    .line 4371
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->BOOLEAN:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->DOUBLE:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->FLOAT:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->INT:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->LONG:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->STRING:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->$VALUES:[Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 4371
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 4371
    const-class v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;
    .locals 1

    .prologue
    .line 4371
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->$VALUES:[Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    return-object v0
.end method
