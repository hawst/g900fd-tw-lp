.class Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;
.super Ljava/lang/Object;
.source "NRRSignatureRecognitionPlugin.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SignatureRecognitionTask"
.end annotation


# instance fields
.field private final mSpenObjectStrokes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;"
        }
    .end annotation
.end field

.field private final mVerifierStrictLevel:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;Ljava/util/List;I)V
    .locals 1
    .param p3, "strictLevel"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 109
    .local p2, "spenObjectStrokes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->mSpenObjectStrokes:Ljava/util/List;

    .line 112
    packed-switch p3, :pswitch_data_0

    .line 127
    sget-object v0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->StrictnessLevel_High:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->mVerifierStrictLevel:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    .line 130
    :goto_0
    return-void

    .line 114
    :pswitch_0
    sget-object v0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->StrictnessLevel_Low:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->mVerifierStrictLevel:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    goto :goto_0

    .line 118
    :pswitch_1
    sget-object v0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->StrictnessLevel_High:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->mVerifierStrictLevel:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    goto :goto_0

    .line 122
    :pswitch_2
    sget-object v0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->StrictnessLevel_Medium:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->mVerifierStrictLevel:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    goto :goto_0

    .line 112
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;Ljava/util/List;ILcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;Ljava/util/List;I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 134
    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mModel:Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;
    invoke-static {}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->access$0()Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->mVerifierStrictLevel:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->mSpenObjectStrokes:Ljava/util/List;

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRUserModel;->verify(Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;Ljava/util/List;)Z

    move-result v0

    .line 135
    .local v0, "result":Z
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mUiHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->access$1(Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$ResultNotifier;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$SignatureRecognitionTask;->mSpenObjectStrokes:Ljava/util/List;

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v0, v5}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$ResultNotifier;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;Ljava/util/List;ZLcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$ResultNotifier;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 136
    return-void
.end method
