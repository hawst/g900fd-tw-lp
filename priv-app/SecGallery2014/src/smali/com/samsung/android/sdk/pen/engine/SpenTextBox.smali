.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
.super Landroid/view/View;
.source "SpenTextBox.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;,
        Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    }
.end annotation


# static fields
.field private static final BITMAP_CUE_BOTTOM:I = 0x1

.field private static final BITMAP_CUE_MAX:I = 0x2

.field private static final BITMAP_CUE_TOP:I = 0x0

.field private static final BITMAP_HANDLE_CENTER_BOTTOM:I = 0x5

.field private static final BITMAP_HANDLE_CENTER_TOP:I = 0x4

.field private static final BITMAP_HANDLE_LEFT_BOTTOM:I = 0x1

.field private static final BITMAP_HANDLE_LEFT_TOP:I = 0x0

.field private static final BITMAP_HANDLE_MAX:I = 0x6

.field private static final BITMAP_HANDLE_RIGHT_BOTTOM:I = 0x3

.field private static final BITMAP_HANDLE_RIGHT_TOP:I = 0x2

.field private static final BORDER_STATIC_LINE_WIDTH:I = 0x4

.field private static final BORDER_STATIC_LINE_WIDTH_DENSITY_1:I = 0x1

.field private static final CR_CHAR:C = '\r'

.field private static final CUE_BOTTOM:I = 0x1

.field private static final CUE_MAX:I = 0x2

.field private static final CUE_TOP:I = 0x0

.field private static final CURSOR_WIDTH:I = 0x2

.field private static final CURSOR_WIDTH_DENSITY_1:I = 0x1

.field private static final DBG:Z = false

.field private static final DEFAULT_CONTEXTMENU_DELAY:I = 0x1388

.field private static final DEFAULT_CUE_FILE_NAME:[Ljava/lang/String;

.field private static final DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

.field private static final DEFAULT_LINE_SPACING:F = 1.3f

.field private static final DEFAULT_PADDING:I = 0x5

.field private static final DEFAULT_PADDING_DENSITY_1:I = 0x2

.field private static final DEFAULT_SIZE_FONT:F = 36.0f

.field private static final DEFAULT_SPAN:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

.field private static final DRAGTEXT_MAX_LENGTH:I = 0x14

.field private static final EPSILON:F = 1.0E-7f

.field private static final HANDLE_END:I = 0x2

.field private static final HANDLE_MAX:I = 0x3

.field private static final HANDLE_MIDDLE:I = 0x1

.field private static final HANDLE_START:I = 0x0

.field static final ID_COPY:I = 0x1020021

.field static final ID_CUT:I = 0x1020020

.field static final ID_PASTE:I = 0x1020022

.field static final ID_SELECT_ALL:I = 0x102001f

.field private static final ITALIC_ANGLE_IN_DEGREE:F = 15.0f

.field private static final LF_CHAR:C = '\n'

.field private static final PRESS_AREA_COLOR_DARK:I = -0xe69570

.field private static final PRESS_AREA_COLOR_LIGHT:I = -0x7f86350e

.field private static final SCROLL_BAR_COLOR:I = -0x7fa0a0a1

.field private static final SCROLL_BAR_WIDTH:I = 0xa

.field private static final SCROLL_BAR_WIDTH_DENSITY_1:I = 0x4

.field private static final SIN_15_DEGREE:F

.field private static final SPACE_CHAR:C = ' '

.field private static final TAB_CHAR:C = '\t'

.field private static final TAG:Ljava/lang/String; = "SpenTextBox"

.field private static final TEXT_INPUT_LIMITED:I = 0x1388

.field private static final VOICE_INPUT_METHOD_ID:Ljava/lang/String; = "com.google.android.googlequicksearchbox/com.google.android.voicesearch.ime.VoiceInputMethodService"

.field private static mContextMenuItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;",
            ">;"
        }
    .end annotation
.end field

.field protected static mHoverCursorEnable:Z


# instance fields
.field private CONTROL_BOTTOM_MARGIN:I

.field private CONTROL_LEFT_MARGIN:I

.field private CONTROL_RIGHT_MARGIN:I

.field private CONTROL_TOP_MARGIN:I

.field private final MAX_OBJECT_HEIGHT:F

.field private final MAX_OBJECT_WIDTH:F

.field private final hideDeletePopupRunable:Ljava/lang/Runnable;

.field private isAltPressed:Z

.field private isCtrlPressed:Z

.field private isShiftPressed:Z

.field private lastText:Ljava/lang/String;

.field private last_index:I

.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mActionDownTime:J

.field private mAfterCurPos:I

.field private mBitmap:Landroid/graphics/Bitmap;

.field private final mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

.field private mBorderLineWidth:I

.field private mCanvasScroll:F

.field private mChangeWatcher:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

.field private mClipboardMessage:Landroid/widget/Toast;

.field private mContext:Landroid/content/Context;

.field private final mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

.field private mContextMenuVisible:Z

.field private mCueBitmap:[Landroid/graphics/Bitmap;

.field private mCueButton:[Landroid/widget/ImageButton;

.field private mCurrentOrientation:I

.field private mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

.field private mCursorHandleVisible:Z

.field private mCursorVisible:Z

.field private mCursorWidth:I

.field private mDefaultPadding:I

.field private mDeletePopupHandler:Landroid/os/Handler;

.field private mDeleteTextPopup:Landroid/widget/TextView;

.field private mDeltaY:F

.field private mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

.field private mDragText:Landroid/widget/TextView;

.field private mEditable:Landroid/text/Editable;

.field private mEndCurCut:I

.field private mEndIndex:[I

.field private mEndIndexInput:I

.field protected mFirstDraw:Z

.field protected mGestureDetector:Landroid/view/GestureDetector;

.field private mHandleBitmap:[Landroid/graphics/Bitmap;

.field private mHandleButton:[Landroid/widget/ImageButton;

.field private mHandleButtonRect:[Landroid/graphics/RectF;

.field mHandleListener:[Landroid/view/View$OnTouchListener;

.field private mHandlePressed:Z

.field private mHardKeyboardHidden:I

.field private mHasWindowFocus:Z

.field private mHighLightPaint:Landroid/graphics/Paint;

.field private mHighlightPathBogus:Z

.field private mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

.field private mInputConnection:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;

.field private final mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

.field private mIs64:Z

.field private mIsCanvasScroll:Z

.field private mIsChangedByKeyShortCut:Z

.field private mIsCheckCursorOnScroll:Z

.field private mIsCommitText:Z

.field private mIsComposingText:Z

.field private mIsDeletedText:Z

.field private mIsEditable:Z

.field private mIsEditableClear:Z

.field private mIsFirstCharLF:Z

.field private mIsFitOnSizeChanged:Z

.field mIsHandleRevese:[Z

.field private mIsHardKeyboardConnected:Z

.field private mIsMoveText:Z

.field private mIsSelectByKey:Z

.field private mIsSetEmptyText:Z

.field private mIsShowSoftInputEnable:Z

.field private mIsSwipeOnKeyboard:Z

.field private mIsTyping:Z

.field private mIsUndoOrRedo:Z

.field private mIsViewMode:Z

.field private mKeyListener:Landroid/text/method/KeyListener;

.field private mKeypadFilter:Landroid/content/IntentFilter;

.field private mLineCount:I

.field private mLineL2R:[Z

.field private mLinePosition:[Landroid/graphics/PointF;

.field private mMetricsRect:Landroid/graphics/Rect;

.field private mMoveEnd:I

.field private mMoveIndex:I

.field private mMoveSpanList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mMoveStart:I

.field private mMoveText:Ljava/lang/String;

.field private mNativeTextView:J

.field private mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

.field private mPrevCurPos:I

.field private mPrevLineIndex:I

.field private mPrevRelativeWidth:I

.field private mPreventShowSoftInput:Z

.field private mRequestObjectChange:Ljava/lang/Runnable;

.field private mScaleMatrix:Landroid/graphics/Matrix;

.field private mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

.field private mScrollBarPaint:Landroid/graphics/Paint;

.field private mScrollBarVisible:Z

.field private mScrollBarWidth:I

.field private mSelectPaint:Landroid/graphics/Paint;

.field private mShowCursor:J

.field private mStartIndex:[I

.field private mStartIndexInput:I

.field private mSurroundingTextLength:I

.field private mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

.field private mTempMatrix:Landroid/graphics/Matrix;

.field private mTempRectF:Landroid/graphics/RectF;

.field private mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

.field protected mTextEraserEnable:Z

.field private mTextItalic:[Z

.field private mTextLimit:I

.field private mTextRect:[Landroid/graphics/RectF;

.field private mTextSize:[F

.field private mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

.field private mTouchEnable:Z

.field private final mTyping:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

.field private mUpdateHandler:Landroid/os/Handler;

.field private nextText:Ljava/lang/String;

.field private recordedDelete:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 97
    const-wide/high16 v0, 0x402e000000000000L    # 15.0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->SIN_15_DEGREE:F

    .line 174
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    .line 184
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_CUE_FILE_NAME:[Ljava/lang/String;

    .line 190
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_SPAN:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 400
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverCursorEnable:Z

    .line 402
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenuItemList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;II)V
    .locals 18
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # Landroid/view/ViewGroup;
    .param p3, "parentWidth"    # I
    .param p4, "ParentHeight"    # I

    .prologue
    .line 432
    invoke-direct/range {p0 .. p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 146
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_LEFT_MARGIN:I

    .line 147
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    .line 148
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_TOP_MARGIN:I

    .line 149
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    .line 167
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isCtrlPressed:Z

    .line 168
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isAltPressed:Z

    .line 169
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    .line 172
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsShowSoftInputEnable:Z

    .line 176
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/String;

    const-string/jumbo v8, "text_select_handle_left_2_browser"

    invoke-direct {v5, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v5, v3, v4

    .line 177
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/String;

    const-string/jumbo v8, "text_select_handle_left_browser"

    invoke-direct {v5, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v5, v3, v4

    .line 178
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    const/4 v4, 0x2

    new-instance v5, Ljava/lang/String;

    const-string/jumbo v8, "text_select_handle_right_2_browser"

    invoke-direct {v5, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v5, v3, v4

    .line 179
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    const/4 v4, 0x3

    new-instance v5, Ljava/lang/String;

    const-string/jumbo v8, "text_select_handle_right_browser"

    invoke-direct {v5, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v5, v3, v4

    .line 180
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    const/4 v4, 0x4

    new-instance v5, Ljava/lang/String;

    const-string/jumbo v8, "text_select_handle_reverse"

    invoke-direct {v5, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v5, v3, v4

    .line 181
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    const/4 v4, 0x5

    new-instance v5, Ljava/lang/String;

    const-string/jumbo v8, "text_select_handle_middle"

    invoke-direct {v5, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v5, v3, v4

    .line 186
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_CUE_FILE_NAME:[Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/String;

    const-string v8, ""

    invoke-direct {v5, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v5, v3, v4

    .line 187
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_CUE_FILE_NAME:[Ljava/lang/String;

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/String;

    const-string v8, ""

    invoke-direct {v5, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v5, v3, v4

    .line 198
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .line 199
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    .line 202
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevRelativeWidth:I

    .line 203
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCurrentOrientation:I

    .line 206
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighLightPaint:Landroid/graphics/Paint;

    .line 207
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    .line 208
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarPaint:Landroid/graphics/Paint;

    .line 227
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighlightPathBogus:Z

    .line 230
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeypadFilter:Landroid/content/IntentFilter;

    .line 233
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHardKeyboardHidden:I

    .line 234
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHardKeyboardConnected:Z

    .line 235
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSwipeOnKeyboard:Z

    .line 236
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPreventShowSoftInput:Z

    .line 239
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditable:Z

    .line 240
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    .line 241
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleVisible:Z

    .line 242
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarVisible:Z

    .line 243
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTouchEnable:Z

    .line 244
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandlePressed:Z

    .line 245
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    .line 246
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHasWindowFocus:Z

    .line 247
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    .line 248
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mFirstDraw:Z

    .line 249
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenuVisible:Z

    .line 250
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCanvasScroll:Z

    .line 251
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFitOnSizeChanged:Z

    .line 253
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    .line 254
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 257
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    .line 259
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mChangeWatcher:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    .line 260
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    .line 262
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    .line 263
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCommitText:Z

    .line 264
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    .line 265
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsComposingText:Z

    .line 266
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSetEmptyText:Z

    .line 267
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditableClear:Z

    .line 268
    const/16 v3, 0x1388

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    .line 269
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I

    .line 270
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsUndoOrRedo:Z

    .line 273
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCheckCursorOnScroll:Z

    .line 274
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F

    .line 278
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsMoveText:Z

    .line 279
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    .line 280
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    .line 281
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    .line 282
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveIndex:I

    .line 284
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    .line 285
    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mActionDownTime:J

    .line 288
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    .line 289
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevLineIndex:I

    .line 290
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 291
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAfterCurPos:I

    .line 292
    const/4 v3, -0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    .line 293
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeltaY:F

    .line 294
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    .line 295
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    .line 296
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    .line 297
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineL2R:[Z

    .line 298
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    .line 299
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextSize:[F

    .line 300
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextItalic:[Z

    .line 301
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFirstCharLF:Z

    .line 303
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsChangedByKeyShortCut:Z

    .line 306
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .line 307
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    .line 308
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    .line 309
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    .line 310
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMetricsRect:Landroid/graphics/Rect;

    .line 313
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    .line 316
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mUpdateHandler:Landroid/os/Handler;

    .line 317
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mRequestObjectChange:Ljava/lang/Runnable;

    .line 318
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    .line 321
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    .line 323
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 325
    const-string v3, ""

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->lastText:Ljava/lang/String;

    .line 326
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->nextText:Ljava/lang/String;

    .line 327
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z

    .line 329
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    .line 330
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndexInput:I

    .line 331
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndexInput:I

    .line 332
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeletePopupHandler:Landroid/os/Handler;

    .line 334
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 336
    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    .line 338
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    .line 399
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 560
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 1516
    const/4 v3, 0x3

    new-array v3, v3, [Landroid/view/View$OnTouchListener;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleListener:[Landroid/view/View$OnTouchListener;

    .line 1518
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleListener:[Landroid/view/View$OnTouchListener;

    const/4 v4, 0x0

    new-instance v5, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    aput-object v5, v3, v4

    .line 1603
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleListener:[Landroid/view/View$OnTouchListener;

    const/4 v4, 0x1

    new-instance v5, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    aput-object v5, v3, v4

    .line 1697
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleListener:[Landroid/view/View$OnTouchListener;

    const/4 v4, 0x2

    new-instance v5, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$4;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$4;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    aput-object v5, v3, v4

    .line 7003
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$5;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$5;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideDeletePopupRunable:Ljava/lang/Runnable;

    .line 434
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    .line 436
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    const-string v4, "accessibility"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/accessibility/AccessibilityManager;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 438
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v3

    const/16 v4, 0x20

    if-ne v3, v4, :cond_4

    const/4 v3, 0x0

    :goto_0
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    .line 440
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_init()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    .line 442
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    .line 443
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 444
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    const v4, -0x7f86350e

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 445
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 447
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighLightPaint:Landroid/graphics/Paint;

    .line 448
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighLightPaint:Landroid/graphics/Paint;

    const v4, -0xff6901

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 450
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarPaint:Landroid/graphics/Paint;

    .line 451
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 452
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarPaint:Landroid/graphics/Paint;

    const v4, -0x7fa0a0a1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 453
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarPaint:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 455
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    .line 456
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    .line 458
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    .line 460
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mChangeWatcher:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    .line 461
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-nez v3, :cond_0

    .line 462
    invoke-static {}, Landroid/text/Editable$Factory;->getInstance()Landroid/text/Editable$Factory;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/text/Editable$Factory;->newEditable(Ljava/lang/CharSequence;)Landroid/text/Editable;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    .line 463
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 465
    const/4 v3, 0x1

    new-array v2, v3, [Landroid/text/InputFilter;

    .line 466
    .local v2, "arrayOfInputFilter":[Landroid/text/InputFilter;
    const/4 v3, 0x0

    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v5

    const/16 v8, 0x1388

    invoke-direct {v4, v5, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;-><init>(Landroid/content/Context;I)V

    aput-object v4, v2, v3

    .line 468
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v3, v2}, Landroid/text/Editable;->setFilters([Landroid/text/InputFilter;)V

    .line 471
    .end local v2    # "arrayOfInputFilter":[Landroid/text/InputFilter;
    :cond_0
    const/4 v3, 0x0

    sget-object v4, Landroid/text/method/TextKeyListener$Capitalize;->SENTENCES:Landroid/text/method/TextKeyListener$Capitalize;

    invoke-static {v3, v4}, Landroid/text/method/TextKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    .line 473
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTyping:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

    .line 475
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mUpdateHandler:Landroid/os/Handler;

    .line 477
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$6;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$6;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mRequestObjectChange:Ljava/lang/Runnable;

    .line 484
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_construct(JLandroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 485
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 488
    :cond_1
    move/from16 v0, p3

    int-to-float v3, v0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    .line 489
    move/from16 v0, p4

    int-to-float v3, v0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    .line 491
    const/4 v3, 0x2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    .line 492
    const/16 v3, 0xa

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarWidth:I

    .line 493
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDefaultPadding:I

    .line 494
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBorderLineWidth:I

    .line 496
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v15

    .line 497
    .local v15, "displayMetrics":Landroid/util/DisplayMetrics;
    if-eqz v15, :cond_3

    .line 498
    const/4 v6, 0x1

    .line 499
    .local v6, "textOnCanvasCommand":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    if-nez v3, :cond_2

    .line 500
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    .line 503
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    iget v3, v15, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v5, v15, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v3, v5, :cond_5

    iget v3, v15, Landroid/util/DisplayMetrics;->heightPixels:I

    :goto_1
    iput v3, v4, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;->baseRate:I

    .line 506
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 507
    .local v7, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 508
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const/4 v8, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 510
    iget v3, v15, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v4, v15, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v3, v4, :cond_6

    iget v0, v15, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v17, v0

    .line 512
    .local v17, "width":I
    :goto_2
    new-instance v3, Landroid/graphics/Rect;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move/from16 v0, v17

    move/from16 v1, v17

    invoke-direct {v3, v4, v5, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMetricsRect:Landroid/graphics/Rect;

    .line 514
    iget v3, v15, Landroid/util/DisplayMetrics;->density:F

    const/high16 v4, 0x40000000    # 2.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_3

    .line 515
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    .line 516
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarWidth:I

    .line 517
    const/4 v3, 0x2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDefaultPadding:I

    .line 518
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBorderLineWidth:I

    .line 522
    .end local v6    # "textOnCanvasCommand":I
    .end local v7    # "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v17    # "width":I
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCurrentOrientation:I

    .line 523
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHardKeyboardHidden:I

    .line 525
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 527
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 528
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$7;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$7;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 542
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeypadFilter:Landroid/content/IntentFilter;

    .line 543
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeypadFilter:Landroid/content/IntentFilter;

    const-string v4, "ResponseAxT9Info"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 544
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeypadFilter:Landroid/content/IntentFilter;

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 546
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initTextBox(Landroid/view/ViewGroup;)V

    .line 548
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->makeDeletePopup()Landroid/widget/TextView;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    .line 550
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeletePopupHandler:Landroid/os/Handler;

    .line 552
    const/4 v6, 0x3

    .line 553
    .restart local v6    # "textOnCanvasCommand":I
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v16

    .line 554
    .local v16, "locale":Ljava/lang/String;
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 555
    .local v13, "tempObjectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 556
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const/4 v14, 0x0

    move-object/from16 v9, p0

    move v12, v6

    invoke-direct/range {v9 .. v14}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 558
    return-void

    .line 438
    .end local v6    # "textOnCanvasCommand":I
    .end local v13    # "tempObjectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v15    # "displayMetrics":Landroid/util/DisplayMetrics;
    .end local v16    # "locale":Ljava/lang/String;
    :cond_4
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 504
    .restart local v6    # "textOnCanvasCommand":I
    .restart local v15    # "displayMetrics":Landroid/util/DisplayMetrics;
    :cond_5
    iget v3, v15, Landroid/util/DisplayMetrics;->widthPixels:I

    goto/16 :goto_1

    .line 511
    .restart local v7    # "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_6
    iget v0, v15, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v17, v0

    goto/16 :goto_2
.end method

.method private Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "command"    # I
    .param p5, "length"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 7454
    .local p4, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7455
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 7457
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_construct(JLandroid/content/Context;)Z
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 7342
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7343
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_construct(JLandroid/content/Context;)Z

    move-result v0

    .line 7345
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_construct(ILandroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_finalize(J)V
    .locals 1
    .param p1, "nativeTextView"    # J

    .prologue
    .line 7334
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7335
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_finalize(J)V

    .line 7339
    :goto_0
    return-void

    .line 7337
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_finalize(I)V

    goto :goto_0
.end method

.method private Native_getHeight(J)I
    .locals 1
    .param p1, "nativeTextView"    # J

    .prologue
    .line 7382
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7383
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHeight(J)I

    move-result v0

    .line 7385
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHeight(I)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getHintTextWidth(J)F
    .locals 1
    .param p1, "nativeTextView"    # J

    .prologue
    .line 7422
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7423
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHintTextWidth(J)F

    move-result v0

    .line 7425
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHintTextWidth(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getLineCount(J)I
    .locals 1
    .param p1, "nativeTextView"    # J

    .prologue
    .line 7374
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7375
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineCount(J)I

    move-result v0

    .line 7377
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineCount(I)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getLineEndIndex(JI)I
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "lineNumber"    # I

    .prologue
    .line 7414
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7415
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineEndIndex(JI)I

    move-result v0

    .line 7417
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineEndIndex(II)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getLinePosition(JILandroid/graphics/PointF;)Z
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "lineNumber"    # I
    .param p4, "position"    # Landroid/graphics/PointF;

    .prologue
    .line 7390
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7391
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLinePosition(JILandroid/graphics/PointF;)Z

    move-result v0

    .line 7393
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLinePosition(IILandroid/graphics/PointF;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_getLineStartIndex(JI)I
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "lineNumber"    # I

    .prologue
    .line 7406
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7407
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineStartIndex(JI)I

    move-result v0

    .line 7409
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineStartIndex(II)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getPan(J)F
    .locals 1
    .param p1, "nativeTextView"    # J

    .prologue
    .line 7438
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7439
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(J)F

    move-result v0

    .line 7441
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getTextRect(JILandroid/graphics/RectF;)Z
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "textIndex"    # I
    .param p4, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 7398
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7399
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getTextRect(JILandroid/graphics/RectF;)Z

    move-result v0

    .line 7401
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getTextRect(IILandroid/graphics/RectF;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_init()J
    .locals 2

    .prologue
    .line 7317
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7318
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_init_64()J

    move-result-wide v0

    .line 7320
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_init()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method private Native_measure(JI)Z
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "width"    # I

    .prologue
    .line 7366
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7367
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_measure(JI)Z

    move-result v0

    .line 7369
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_measure(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setBitmap(JLandroid/graphics/Bitmap;)Z
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 7350
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7351
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setBitmap(JLandroid/graphics/Bitmap;)Z

    move-result v0

    .line 7353
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setBitmap(ILandroid/graphics/Bitmap;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setObjectText(JLcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "ObjectTextBox"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .prologue
    .line 7358
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7359
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setObjectText(JLcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z

    move-result v0

    .line 7361
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setObjectText(ILcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setPan(JF)V
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "deltaY"    # F

    .prologue
    .line 7430
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7431
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setPan(JF)V

    .line 7435
    :goto_0
    return-void

    .line 7433
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setPan(IF)V

    goto :goto_0
.end method

.method private Native_update(J)Z
    .locals 1
    .param p1, "nativeTextView"    # J

    .prologue
    .line 7446
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIs64:Z

    if-eqz v0, :cond_0

    .line 7447
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_update(J)Z

    move-result v0

    .line 7449
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_update(I)Z

    move-result v0

    goto :goto_0
.end method

.method private absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
    .locals 2
    .param p1, "dstRect"    # Landroid/graphics/RectF;
    .param p2, "srcRect"    # Landroid/graphics/RectF;
    .param p3, "coordinateInfo"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .prologue
    .line 5349
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 5350
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    .line 5351
    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v1

    .line 5350
    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 5352
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 5353
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    .line 5354
    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    .line 5353
    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 5355
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;FF)[F
    .locals 1

    .prologue
    .line 5936
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getAbsolutePoint(FF)[F

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 4645
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSelection()V

    return-void
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)F
    .locals 1

    .prologue
    .line 3381
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->checkForVerticalScroll(I)F

    move-result v0

    return v0
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 3857
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onDrawHandle()V

    return-void
.end method

.method static synthetic access$14(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 1791
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextmenu()V

    return-void
.end method

.method static synthetic access$15(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 5825
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showCursorHandle()V

    return-void
.end method

.method static synthetic access$16(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    return-object v0
.end method

.method static synthetic access$17(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 7010
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideDeleteTextPopup()V

    return-void
.end method

.method static synthetic access$18(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/method/KeyListener;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    return-object v0
.end method

.method static synthetic access$19(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 264
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    return-object v0
.end method

.method static synthetic access$20(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 263
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCommitText:Z

    return-void
.end method

.method static synthetic access$21(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 265
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsComposingText:Z

    return-void
.end method

.method static synthetic access$22(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$23(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 240
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    return-void
.end method

.method static synthetic access$24(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 7039
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->selectAll()Z

    move-result v0

    return v0
.end method

.method static synthetic access$25(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/content/ClipboardManager;II)Z
    .locals 1

    .prologue
    .line 7044
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->cut(Landroid/content/ClipboardManager;II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$26(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/content/ClipboardManager;II)Z
    .locals 1

    .prologue
    .line 7108
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->copy(Landroid/content/ClipboardManager;II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$27(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/content/ClipboardManager;II)Z
    .locals 1

    .prologue
    .line 7162
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->paste(Landroid/content/ClipboardManager;II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$28(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 227
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighlightPathBogus:Z

    return v0
.end method

.method static synthetic access$29(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 4622
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSettingInfo()V

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 4322
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$30(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 262
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    return-void
.end method

.method static synthetic access$31(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 241
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleVisible:Z

    return-void
.end method

.method static synthetic access$32(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 242
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarVisible:Z

    return-void
.end method

.method static synthetic access$33(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 245
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    return v0
.end method

.method static synthetic access$34(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 262
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    return v0
.end method

.method static synthetic access$35(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 334
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    return-void
.end method

.method static synthetic access$36(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I
    .locals 1

    .prologue
    .line 288
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    return v0
.end method

.method static synthetic access$37(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$38(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$39(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)J
    .locals 2

    .prologue
    .line 336
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    return-wide v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    return-object v0
.end method

.method static synthetic access$40(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;J)F
    .locals 1

    .prologue
    .line 7437
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v0

    return v0
.end method

.method static synthetic access$41(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;IF)I
    .locals 1

    .prologue
    .line 5180
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getOffsetForHorizontal(IF)I

    move-result v0

    return v0
.end method

.method static synthetic access$42(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$43(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[I
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    return-object v0
.end method

.method static synthetic access$44(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z
    .locals 1

    .prologue
    .line 4511
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->findWord(ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$45(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I
    .locals 1

    .prologue
    .line 3068
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getTextLength()I

    move-result v0

    return v0
.end method

.method static synthetic access$46(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)F
    .locals 1

    .prologue
    .line 274
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F

    return v0
.end method

.method static synthetic access$47(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;F)V
    .locals 0

    .prologue
    .line 274
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F

    return-void
.end method

.method static synthetic access$48(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$49(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 5997
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onCueTopButtonDown(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    return-object v0
.end method

.method static synthetic access$50(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 6026
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onCueBottomButtonDown(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$51(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 236
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPreventShowSoftInput:Z

    return-void
.end method

.method static synthetic access$52(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 5847
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideCursorHandle()V

    return-void
.end method

.method static synthetic access$53(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTyping:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

    return-object v0
.end method

.method static synthetic access$54(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 3035
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initMeasureInfo()V

    return-void
.end method

.method static synthetic access$55(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/view/accessibility/AccessibilityManager;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method

.method static synthetic access$56(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 6769
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->sendAccessibilityEventTypeViewTextChanged(Ljava/lang/CharSequence;III)V

    return-void
.end method

.method static synthetic access$57(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 270
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsUndoOrRedo:Z

    return v0
.end method

.method static synthetic access$58(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 303
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsChangedByKeyShortCut:Z

    return v0
.end method

.method static synthetic access$59(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 325
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->lastText:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 227
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighlightPathBogus:Z

    return-void
.end method

.method static synthetic access$60(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V
    .locals 0

    .prologue
    .line 290
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    return-void
.end method

.method static synthetic access$61(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 303
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsChangedByKeyShortCut:Z

    return-void
.end method

.method static synthetic access$62(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 327
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z

    return v0
.end method

.method static synthetic access$63(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 327
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z

    return-void
.end method

.method static synthetic access$64(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 267
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditableClear:Z

    return v0
.end method

.method static synthetic access$65(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 263
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCommitText:Z

    return v0
.end method

.method static synthetic access$66(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 265
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsComposingText:Z

    return v0
.end method

.method static synthetic access$67(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 264
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    return v0
.end method

.method static synthetic access$68(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 266
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSetEmptyText:Z

    return v0
.end method

.method static synthetic access$69(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 2391
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateEditable()V

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    .prologue
    .line 244
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandlePressed:Z

    return-void
.end method

.method static synthetic access$70(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    .prologue
    .line 240
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    return v0
.end method

.method static synthetic access$71(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I
    .locals 1

    .prologue
    .line 269
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I

    return v0
.end method

.method static synthetic access$72(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V
    .locals 0

    .prologue
    .line 269
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I

    return-void
.end method

.method static synthetic access$73(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V
    .locals 0

    .prologue
    .line 291
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAfterCurPos:I

    return-void
.end method

.method static synthetic access$74(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$75(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V
    .locals 0

    .prologue
    .line 330
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndexInput:I

    return-void
.end method

.method static synthetic access$76(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V
    .locals 0

    .prologue
    .line 331
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndexInput:I

    return-void
.end method

.method static synthetic access$77(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 6964
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showDeletePopupWindow()V

    return-void
.end method

.method static synthetic access$78(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)Z
    .locals 1

    .prologue
    .line 7279
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setHoveringSpenIcon(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$79(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 7016
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->deleteLastSpeechInput()V

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;F)I
    .locals 1

    .prologue
    .line 5161
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v0

    return v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;IF)I
    .locals 1

    .prologue
    .line 5233
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v0

    return v0
.end method

.method private adjustCursorSize(Landroid/graphics/Rect;II)V
    .locals 12
    .param p1, "rect"    # Landroid/graphics/Rect;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 3229
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v9, :cond_1

    .line 3281
    :cond_0
    :goto_0
    return-void

    .line 3233
    :cond_1
    const/4 v0, 0x0

    .local v0, "currentFontSize":F
    const/4 v6, 0x0

    .line 3235
    .local v6, "maxFontSize":F
    move v7, p2

    .line 3237
    .local v7, "pos":I
    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v10, v11}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineCount(J)I

    move-result v3

    .line 3238
    .local v3, "lineCount":I
    const/4 v5, 0x0

    .local v5, "lineStart":I
    const/4 v4, 0x0

    .line 3240
    .local v4, "lineEnd":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-lt v2, v3, :cond_6

    .line 3249
    :cond_2
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9, v5, v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v8

    .line 3250
    .local v8, "spans":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    if-eqz v8, :cond_3

    .line 3251
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v2, v9, :cond_8

    .line 3262
    :cond_3
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9, v7, v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v8

    .line 3263
    if-eqz v8, :cond_4

    .line 3264
    const/4 v2, 0x0

    :goto_3
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v2, v9, :cond_a

    .line 3272
    :cond_4
    const/4 v9, 0x0

    cmpl-float v9, v6, v9

    if-eqz v9, :cond_5

    .line 3273
    iget v9, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v9, v9

    sub-float v10, v6, v0

    const/high16 v11, 0x40800000    # 4.0f

    div-float/2addr v10, v11

    sub-float/2addr v9, v10

    float-to-int v9, v9

    iput v9, p1, Landroid/graphics/Rect;->bottom:I

    .line 3276
    :cond_5
    const/4 v9, 0x0

    cmpl-float v9, v0, v9

    if-lez v9, :cond_0

    .line 3277
    iget v9, p1, Landroid/graphics/Rect;->bottom:I

    const v10, 0x3fa66666    # 1.3f

    mul-float/2addr v10, v0

    float-to-int v10, v10

    sub-int/2addr v9, v10

    iput v9, p1, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 3241
    .end local v8    # "spans":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    :cond_6
    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v10, v11, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineStartIndex(JI)I

    move-result v5

    .line 3242
    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v10, v11, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineEndIndex(JI)I

    move-result v4

    .line 3243
    if-gt v5, v7, :cond_7

    add-int/lit8 v9, v4, 0x1

    if-ge v9, v7, :cond_2

    .line 3240
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3252
    .restart local v8    # "spans":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    :cond_8
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    instance-of v9, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v9, :cond_9

    .line 3253
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iget v1, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    .line 3254
    .local v1, "fontSize":F
    cmpg-float v9, v6, v1

    if-gtz v9, :cond_9

    .line 3255
    move v6, v1

    .line 3251
    .end local v1    # "fontSize":F
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 3265
    :cond_a
    if-eqz v7, :cond_b

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-eq v9, v7, :cond_c

    .line 3266
    :cond_b
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    instance-of v9, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v9, :cond_c

    .line 3267
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iget v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    .line 3264
    :cond_c
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method private adjustTextBox()V
    .locals 15

    .prologue
    const/4 v14, 0x3

    const/4 v13, 0x0

    const/4 v12, 0x0

    .line 3128
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v8, :cond_1

    .line 3220
    :cond_0
    :goto_0
    return-void

    .line 3132
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v5

    .line 3134
    .local v5, "prevObjectWidth":F
    iget-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v8, :cond_6

    .line 3135
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 3140
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v1

    .line 3141
    .local v1, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v1, :cond_0

    .line 3156
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v4

    .line 3158
    .local v4, "objectRect":Landroid/graphics/RectF;
    new-instance v7, Landroid/graphics/RectF;

    invoke-direct {v7}, Landroid/graphics/RectF;-><init>()V

    .line 3159
    .local v7, "relativeRect":Landroid/graphics/RectF;
    invoke-direct {p0, v7, v4, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3161
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v0

    .line 3162
    .local v0, "absoluteHeight":I
    const/4 v6, 0x0

    .line 3163
    .local v6, "relativeHeight":I
    if-nez v0, :cond_7

    .line 3164
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDefaultHeight()I

    move-result v6

    .line 3169
    :goto_2
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v8

    const/4 v9, 0x2

    if-eq v8, v9, :cond_2

    .line 3170
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v8

    if-ne v8, v14, :cond_3

    .line 3171
    :cond_2
    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v8

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v8, v8

    if-ge v6, v8, :cond_a

    .line 3172
    if-lez v6, :cond_3

    .line 3173
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMinHeight()F

    move-result v3

    .line 3174
    .local v3, "minHeight":F
    cmpg-float v8, v3, v12

    if-gtz v8, :cond_8

    .line 3175
    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v8

    int-to-float v9, v6

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    .line 3207
    .end local v3    # "minHeight":F
    :cond_3
    :goto_3
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v8

    const/4 v9, 0x1

    if-eq v8, v9, :cond_4

    .line 3208
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v8

    if-ne v8, v14, :cond_5

    .line 3209
    :cond_4
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    cmpg-float v8, v5, v8

    if-gez v8, :cond_5

    .line 3210
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onObjectChanged()V

    .line 3214
    :cond_5
    iget-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCanvasScroll:Z

    if-eqz v8, :cond_e

    iget-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFitOnSizeChanged:Z

    if-eqz v8, :cond_e

    .line 3215
    iput-boolean v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCanvasScroll:Z

    .line 3216
    iput-boolean v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFitOnSizeChanged:Z

    goto/16 :goto_0

    .line 3137
    .end local v0    # "absoluteHeight":I
    .end local v1    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .end local v4    # "objectRect":Landroid/graphics/RectF;
    .end local v6    # "relativeHeight":I
    .end local v7    # "relativeRect":Landroid/graphics/RectF;
    :cond_6
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/RectF;->width()F

    move-result v10

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-int v10, v10

    invoke-direct {p0, v8, v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_measure(JI)Z

    goto/16 :goto_1

    .line 3166
    .restart local v0    # "absoluteHeight":I
    .restart local v1    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .restart local v4    # "objectRect":Landroid/graphics/RectF;
    .restart local v6    # "relativeHeight":I
    .restart local v7    # "relativeRect":Landroid/graphics/RectF;
    :cond_7
    int-to-float v8, v0

    iget v9, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v8, v9

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v6, v8

    goto/16 :goto_2

    .line 3177
    .restart local v3    # "minHeight":F
    :cond_8
    iget v8, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v3, v8

    .line 3178
    int-to-float v8, v6

    cmpg-float v8, v3, v8

    if-gez v8, :cond_9

    .line 3179
    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v8

    int-to-float v9, v6

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    goto :goto_3

    .line 3181
    :cond_9
    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v8

    invoke-direct {p0, v8, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    goto :goto_3

    .line 3185
    .end local v3    # "minHeight":F
    :cond_a
    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v8

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v8, v8

    if-le v6, v8, :cond_3

    .line 3186
    int-to-float v8, v0

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v9

    iget v9, v9, Landroid/graphics/RectF;->top:F

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    add-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    int-to-float v10, v10

    sub-float/2addr v9, v10

    cmpl-float v8, v8, v9

    if-ltz v8, :cond_b

    .line 3187
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    int-to-float v9, v9

    sub-float/2addr v8, v9

    .line 3188
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v9

    iget v9, v9, Landroid/graphics/RectF;->top:F

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    .line 3187
    sub-float/2addr v8, v9

    .line 3188
    iget v9, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    .line 3187
    mul-float/2addr v8, v9

    float-to-int v6, v8

    .line 3190
    :cond_b
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMaxHeight()F

    move-result v2

    .line 3191
    .local v2, "maxHeight":F
    cmpg-float v8, v2, v12

    if-gtz v8, :cond_c

    .line 3192
    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v8

    int-to-float v9, v6

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    goto/16 :goto_3

    .line 3194
    :cond_c
    iget v8, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v2, v8

    .line 3195
    int-to-float v8, v6

    cmpl-float v8, v2, v8

    if-lez v8, :cond_d

    .line 3196
    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v8

    int-to-float v9, v6

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    goto/16 :goto_3

    .line 3198
    :cond_d
    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v8

    invoke-direct {p0, v8, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    goto/16 :goto_3

    .line 3218
    .end local v2    # "maxHeight":F
    :cond_e
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->checkCursorPosition()V

    goto/16 :goto_0
.end method

.method private adjustTextBoxRect()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 3082
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v5, :cond_1

    .line 3122
    :cond_0
    :goto_0
    return-void

    .line 3086
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v0

    .line 3087
    .local v0, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v0, :cond_0

    .line 3091
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    .line 3092
    .local v3, "objectRect":Landroid/graphics/RectF;
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v4

    .line 3093
    .local v4, "width":F
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 3095
    .local v1, "height":F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMinWidth()F

    move-result v5

    cmpg-float v5, v4, v5

    if-gez v5, :cond_2

    .line 3096
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMinWidth()F

    move-result v4

    .line 3098
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMinHeight()F

    move-result v5

    cmpg-float v5, v1, v5

    if-gez v5, :cond_3

    .line 3099
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMinHeight()F

    move-result v1

    .line 3101
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMaxWidth()F

    move-result v5

    cmpl-float v5, v5, v7

    if-lez v5, :cond_4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMaxWidth()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMinWidth()F

    move-result v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_4

    .line 3102
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMaxWidth()F

    move-result v5

    cmpl-float v5, v4, v5

    if-lez v5, :cond_4

    .line 3103
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMaxWidth()F

    move-result v4

    .line 3105
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMaxHeight()F

    move-result v5

    cmpl-float v5, v5, v7

    if-lez v5, :cond_5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMaxHeight()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMinHeight()F

    move-result v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_5

    .line 3106
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMaxHeight()F

    move-result v5

    cmpl-float v5, v1, v5

    if-lez v5, :cond_5

    .line 3107
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMaxHeight()F

    move-result v1

    .line 3109
    :cond_5
    iget v5, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v5, v4

    iput v5, v3, Landroid/graphics/RectF;->right:F

    .line 3110
    iget v5, v3, Landroid/graphics/RectF;->top:F

    add-float/2addr v5, v1

    iput v5, v3, Landroid/graphics/RectF;->bottom:F

    .line 3112
    const/4 v2, 0x0

    .line 3113
    .local v2, "isRectChange":Z
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    cmpl-float v5, v5, v6

    if-nez v5, :cond_6

    .line 3114
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_7

    .line 3115
    :cond_6
    const/4 v2, 0x1

    .line 3117
    :cond_7
    if-eqz v2, :cond_0

    .line 3118
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setRect(Landroid/graphics/RectF;Z)V

    .line 3119
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onObjectChanged()V

    .line 3120
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    goto/16 :goto_0
.end method

.method private applyScaledRect(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 4130
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 4131
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 4132
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 4133
    return-void
.end method

.method private checkCursorPosition()V
    .locals 24

    .prologue
    .line 3494
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v20, v0

    if-nez v20, :cond_1

    .line 3654
    :cond_0
    :goto_0
    return-void

    .line 3499
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    move/from16 v20, v0

    if-nez v20, :cond_0

    .line 3503
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v4

    .line 3504
    .local v4, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v4, :cond_0

    .line 3509
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v10

    .line 3510
    .local v10, "objectRect":Landroid/graphics/RectF;
    if-eqz v10, :cond_0

    .line 3515
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v12

    .line 3517
    .local v12, "pos":I
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v16

    .line 3518
    .local v16, "temp":Landroid/graphics/Rect;
    if-eqz v16, :cond_0

    .line 3523
    new-instance v14, Landroid/graphics/RectF;

    invoke-direct {v14}, Landroid/graphics/RectF;-><init>()V

    .line 3524
    .local v14, "rect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2, v12, v14}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getTextRect(JILandroid/graphics/RectF;)Z

    .line 3525
    invoke-virtual {v14}, Landroid/graphics/RectF;->width()F

    move-result v20

    const/high16 v21, 0x3f800000    # 1.0f

    add-float v17, v20, v21

    .line 3526
    .local v17, "width":F
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    add-float v20, v20, v17

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, v16

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 3528
    new-instance v6, Landroid/graphics/RectF;

    move-object/from16 v0, v16

    invoke-direct {v6, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 3530
    .local v6, "cursorRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v20

    move/from16 v0, v20

    int-to-float v9, v0

    .line 3531
    .local v9, "minHeight":F
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v7

    .line 3532
    .local v7, "deltaY":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTopMargin()F

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getBottomMargin()F

    move-result v21

    add-float v8, v20, v21

    .line 3533
    .local v8, "margin":F
    const/16 v20, 0x0

    cmpl-float v20, v8, v20

    if-lez v20, :cond_2

    .line 3534
    const/4 v8, 0x0

    .line 3537
    :cond_2
    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v20

    cmpl-float v20, v9, v20

    if-lez v20, :cond_d

    .line 3538
    iget v0, v6, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v21

    add-float v21, v21, v7

    cmpl-float v20, v20, v21

    if-lez v20, :cond_c

    .line 3539
    iget v0, v6, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v21

    sub-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDefaultPadding:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    add-float v7, v20, v21

    .line 3546
    :cond_3
    :goto_1
    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v20

    add-float v20, v20, v8

    add-float v20, v20, v7

    cmpg-float v20, v9, v20

    if-gez v20, :cond_4

    .line 3547
    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v20

    add-float v20, v20, v8

    add-float v20, v20, v7

    sub-float v20, v20, v9

    sub-float v7, v7, v20

    .line 3553
    :cond_4
    :goto_2
    const/16 v20, 0x0

    cmpg-float v20, v7, v20

    if-gez v20, :cond_5

    .line 3554
    const/4 v7, 0x0

    .line 3557
    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCurrnetLineIndex(I)I

    move-result v5

    .line 3558
    .local v5, "currentLineIndex":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevLineIndex:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-ne v0, v5, :cond_6

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v20

    sub-float v20, v7, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x33d6bf95    # 1.0E-7f

    cmpl-float v20, v20, v21

    if-lez v20, :cond_7

    .line 3559
    :cond_6
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_setPan(JF)V

    .line 3561
    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v20

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    float-to-double v0, v9

    move-wide/from16 v22, v0

    cmpg-double v20, v20, v22

    if-gez v20, :cond_7

    .line 3562
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showScrollBar()V

    .line 3566
    :cond_7
    iget v0, v10, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v22

    sub-float v21, v21, v22

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 3568
    iget v0, v6, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getRelativePoint(FF)[F

    move-result-object v13

    .line 3569
    .local v13, "pts":[F
    if-eqz v13, :cond_0

    .line 3574
    const/16 v20, 0x1

    aget v20, v13, v20

    move/from16 v0, v20

    iput v0, v6, Landroid/graphics/RectF;->top:F

    .line 3576
    iget v0, v6, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getRelativePoint(FF)[F

    move-result-object v13

    .line 3577
    if-eqz v13, :cond_0

    .line 3582
    const/16 v20, 0x1

    aget v20, v13, v20

    move/from16 v0, v20

    iput v0, v6, Landroid/graphics/RectF;->bottom:F

    .line 3584
    new-instance v15, Landroid/graphics/RectF;

    invoke-direct {v15}, Landroid/graphics/RectF;-><init>()V

    .line 3585
    .local v15, "relativeCursorRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v6, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3595
    const/16 v18, 0x0

    .line 3596
    .local v18, "xDiff":F
    const/16 v19, 0x0

    .line 3598
    .local v19, "yDiff":F
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    .line 3599
    .local v11, "parentLayout":Landroid/view/ViewGroup;
    if-eqz v11, :cond_0

    .line 3603
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v20

    if-eqz v20, :cond_0

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getWidth()I

    move-result v20

    if-eqz v20, :cond_0

    .line 3607
    iget v0, v15, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_e

    .line 3609
    iget v0, v15, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    sub-float v19, v20, v21

    .line 3610
    iget v0, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v20, v0

    div-float v19, v19, v20

    .line 3611
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDefaultPadding:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBorderLineWidth:I

    move/from16 v21, v0

    add-int v20, v20, v21

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    add-float v19, v19, v20

    .line 3624
    :cond_8
    :goto_3
    iget v0, v15, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getWidth()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_f

    .line 3626
    iget v0, v15, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getWidth()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    sub-float v18, v20, v21

    .line 3627
    iget v0, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v20, v0

    div-float v18, v18, v20

    .line 3628
    iget v0, v6, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDefaultPadding:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    sub-float v21, v21, v22

    cmpg-float v20, v20, v21

    if-gez v20, :cond_9

    .line 3629
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDefaultPadding:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    add-float v18, v18, v20

    .line 3640
    :cond_9
    :goto_4
    const/16 v20, 0x0

    cmpl-float v20, v18, v20

    if-nez v20, :cond_a

    const/16 v20, 0x0

    cmpl-float v20, v19, v20

    if-eqz v20, :cond_b

    .line 3641
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCheckCursorOnScroll:Z

    move/from16 v20, v0

    if-eqz v20, :cond_10

    .line 3642
    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onRequestScroll(FF)V

    .line 3653
    :cond_b
    :goto_5
    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevLineIndex:I

    goto/16 :goto_0

    .line 3540
    .end local v5    # "currentLineIndex":I
    .end local v11    # "parentLayout":Landroid/view/ViewGroup;
    .end local v13    # "pts":[F
    .end local v15    # "relativeCursorRect":Landroid/graphics/RectF;
    .end local v18    # "xDiff":F
    .end local v19    # "yDiff":F
    :cond_c
    iget v0, v6, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    cmpg-float v20, v20, v7

    if-gez v20, :cond_3

    .line 3541
    iget v0, v6, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDefaultPadding:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    add-float v20, v20, v21

    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v21

    cmpg-float v20, v20, v21

    if-gez v20, :cond_3

    .line 3542
    iget v0, v6, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDefaultPadding:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    sub-float v7, v20, v21

    goto/16 :goto_1

    .line 3550
    :cond_d
    const/4 v7, 0x0

    goto/16 :goto_2

    .line 3612
    .restart local v5    # "currentLineIndex":I
    .restart local v11    # "parentLayout":Landroid/view/ViewGroup;
    .restart local v13    # "pts":[F
    .restart local v15    # "relativeCursorRect":Landroid/graphics/RectF;
    .restart local v18    # "xDiff":F
    .restart local v19    # "yDiff":F
    :cond_e
    iget v0, v15, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    const/16 v21, 0x0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_8

    .line 3614
    iget v0, v15, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v15, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    const/high16 v21, 0x40000000    # 2.0f

    iget v0, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    .line 3615
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDefaultPadding:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBorderLineWidth:I

    move/from16 v23, v0

    add-int v22, v22, v23

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    mul-float v21, v21, v22

    .line 3614
    add-float v20, v20, v21

    .line 3615
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gtz v20, :cond_8

    .line 3616
    iget v0, v15, Landroid/graphics/RectF;->top:F

    move/from16 v19, v0

    .line 3617
    iget v0, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v20, v0

    div-float v19, v19, v20

    .line 3618
    iget v0, v6, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDefaultPadding:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_8

    .line 3619
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDefaultPadding:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBorderLineWidth:I

    move/from16 v21, v0

    add-int v20, v20, v21

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    sub-float v19, v19, v20

    goto/16 :goto_3

    .line 3631
    :cond_f
    iget v0, v15, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    const/16 v21, 0x0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_9

    .line 3633
    iget v0, v15, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    .line 3634
    iget v0, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v20, v0

    div-float v18, v18, v20

    .line 3635
    iget v0, v6, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDefaultPadding:I

    move/from16 v21, v0

    mul-int/lit8 v21, v21, 0x7

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_9

    .line 3636
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDefaultPadding:I

    move/from16 v20, v0

    mul-int/lit8 v20, v20, 0x7

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    sub-float v18, v18, v20

    goto/16 :goto_4

    .line 3644
    :cond_10
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideCursorHandle()V

    .line 3645
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aget-object v20, v20, v21

    const/16 v21, 0x8

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3646
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    move-object/from16 v20, v0

    const/16 v21, 0x2

    aget-object v20, v20, v21

    const/16 v21, 0x8

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3647
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v20, v0

    if-eqz v20, :cond_b

    .line 3648
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    goto/16 :goto_5
.end method

.method private checkForHorizontalScroll()Z
    .locals 10
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 3341
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_1

    .line 3378
    :cond_0
    :goto_0
    return v4

    .line 3346
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v6, :cond_0

    .line 3350
    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandlePressed:Z

    if-nez v6, :cond_0

    .line 3351
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v6

    const/4 v7, 0x3

    if-eq v6, v7, :cond_2

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v6

    if-ne v6, v5, :cond_0

    .line 3356
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v2

    .line 3358
    .local v2, "objectRect":Landroid/graphics/RectF;
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v0

    .line 3359
    .local v0, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v0, :cond_0

    .line 3364
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 3365
    .local v3, "relativeRect":Landroid/graphics/RectF;
    invoke-direct {p0, v3, v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3367
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMaximumWidth()F

    move-result v1

    .line 3368
    .local v1, "lineRelativeWidth":F
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevRelativeWidth:I

    int-to-float v4, v4

    cmpl-float v4, v4, v1

    if-eqz v4, :cond_3

    .line 3369
    float-to-int v4, v1

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevRelativeWidth:I

    .line 3371
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-direct {p0, v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    .line 3373
    iget v4, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v4, v1

    iput v4, v3, Landroid/graphics/RectF;->right:F

    :cond_3
    move v4, v5

    .line 3378
    goto :goto_0
.end method

.method private checkForVerticalScroll(I)F
    .locals 14
    .param p1, "index"    # I

    .prologue
    .line 3382
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3383
    .local v4, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    if-nez v4, :cond_0

    .line 3385
    const/4 v2, 0x0

    .line 3486
    :goto_0
    return v2

    .line 3388
    :cond_0
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v12, :cond_1

    .line 3389
    const/4 v2, 0x0

    goto :goto_0

    .line 3392
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v0

    .line 3393
    .local v0, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-nez v0, :cond_2

    .line 3395
    const/4 v2, 0x0

    goto :goto_0

    .line 3398
    :cond_2
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v7

    .line 3399
    .local v7, "objectRect":Landroid/graphics/RectF;
    if-nez v7, :cond_3

    .line 3401
    const/4 v2, 0x0

    goto :goto_0

    .line 3404
    :cond_3
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v12

    invoke-direct {p0, v12}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v11

    .line 3405
    .local v11, "temp":Landroid/graphics/Rect;
    if-nez v11, :cond_4

    .line 3407
    const/4 v2, 0x0

    goto :goto_0

    .line 3410
    :cond_4
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v11}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 3412
    .local v1, "cursorRect":Landroid/graphics/RectF;
    iget-wide v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v12, v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v3

    .line 3413
    .local v3, "deltaY":F
    iget-wide v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v12, v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v12

    int-to-float v6, v12

    .line 3414
    .local v6, "minHeight":F
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTopMargin()F

    move-result v12

    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v13}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getBottomMargin()F

    move-result v13

    add-float v5, v12, v13

    .line 3415
    .local v5, "margin":F
    const/4 v12, 0x0

    cmpl-float v12, v5, v12

    if-lez v12, :cond_5

    .line 3416
    const/4 v5, 0x0

    .line 3419
    :cond_5
    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v12

    cmpl-float v12, v6, v12

    if-lez v12, :cond_a

    .line 3420
    iget v12, v1, Landroid/graphics/RectF;->top:F

    cmpg-float v12, v12, v3

    if-gez v12, :cond_9

    .line 3421
    iget v3, v1, Landroid/graphics/RectF;->top:F

    .line 3426
    :cond_6
    :goto_1
    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v12

    add-float/2addr v12, v5

    add-float/2addr v12, v3

    cmpg-float v12, v6, v12

    if-gez v12, :cond_7

    .line 3427
    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v12

    add-float/2addr v12, v5

    add-float/2addr v12, v3

    sub-float/2addr v12, v6

    sub-float/2addr v3, v12

    .line 3433
    :cond_7
    :goto_2
    const/4 v12, 0x0

    cmpg-float v12, v3, v12

    if-gez v12, :cond_8

    .line 3434
    const/4 v3, 0x0

    .line 3437
    :cond_8
    iget v12, v7, Landroid/graphics/RectF;->left:F

    iget v13, v7, Landroid/graphics/RectF;->top:F

    sub-float/2addr v13, v3

    invoke-virtual {v1, v12, v13}, Landroid/graphics/RectF;->offset(FF)V

    .line 3439
    iget v12, v1, Landroid/graphics/RectF;->left:F

    iget v13, v1, Landroid/graphics/RectF;->top:F

    invoke-direct {p0, v12, v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getRelativePoint(FF)[F

    move-result-object v9

    .line 3440
    .local v9, "pts":[F
    if-nez v9, :cond_b

    .line 3442
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3422
    .end local v9    # "pts":[F
    :cond_9
    iget v12, v1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v13

    add-float/2addr v13, v3

    cmpl-float v12, v12, v13

    if-lez v12, :cond_6

    .line 3423
    iget v12, v1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v13

    sub-float v3, v12, v13

    goto :goto_1

    .line 3430
    :cond_a
    const/4 v3, 0x0

    goto :goto_2

    .line 3445
    .restart local v9    # "pts":[F
    :cond_b
    const/4 v12, 0x1

    aget v12, v9, v12

    iput v12, v1, Landroid/graphics/RectF;->top:F

    .line 3447
    iget v12, v1, Landroid/graphics/RectF;->left:F

    iget v13, v1, Landroid/graphics/RectF;->bottom:F

    invoke-direct {p0, v12, v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getRelativePoint(FF)[F

    move-result-object v9

    .line 3448
    if-nez v9, :cond_c

    .line 3450
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3453
    :cond_c
    const/4 v12, 0x1

    aget v12, v9, v12

    iput v12, v1, Landroid/graphics/RectF;->bottom:F

    .line 3455
    new-instance v10, Landroid/graphics/RectF;

    invoke-direct {v10}, Landroid/graphics/RectF;-><init>()V

    .line 3456
    .local v10, "relativeCursorRect":Landroid/graphics/RectF;
    invoke-direct {p0, v10, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3458
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    .line 3459
    .local v8, "parentLayout":Landroid/view/ViewGroup;
    if-nez v8, :cond_d

    .line 3460
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3463
    :cond_d
    invoke-virtual {v8}, Landroid/view/ViewGroup;->getHeight()I

    move-result v12

    if-eqz v12, :cond_e

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getWidth()I

    move-result v12

    if-nez v12, :cond_f

    .line 3464
    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3467
    :cond_f
    const/4 v2, 0x0

    .line 3468
    .local v2, "delta":F
    iget v12, v10, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getHeight()I

    move-result v13

    int-to-float v13, v13

    cmpl-float v12, v12, v13

    if-lez v12, :cond_11

    .line 3470
    iget v12, v10, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getHeight()I

    move-result v13

    int-to-float v13, v13

    sub-float v2, v12, v13

    .line 3471
    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v2, v12

    .line 3472
    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDefaultPadding:I

    iget v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBorderLineWidth:I

    add-int/2addr v12, v13

    int-to-float v12, v12

    add-float/2addr v2, v12

    .line 3482
    :cond_10
    :goto_3
    const/4 v12, 0x1

    invoke-virtual {p0, v12}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    goto/16 :goto_0

    .line 3473
    :cond_11
    iget v12, v10, Landroid/graphics/RectF;->top:F

    const/4 v13, 0x0

    cmpg-float v12, v12, v13

    if-gez v12, :cond_10

    .line 3475
    iget v2, v10, Landroid/graphics/RectF;->top:F

    .line 3476
    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v2, v12

    .line 3477
    iget v12, v1, Landroid/graphics/RectF;->top:F

    iget v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDefaultPadding:I

    int-to-float v13, v13

    cmpl-float v12, v12, v13

    if-lez v12, :cond_10

    .line 3478
    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDefaultPadding:I

    iget v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBorderLineWidth:I

    add-int/2addr v12, v13

    int-to-float v12, v12

    sub-float/2addr v2, v12

    goto :goto_3
.end method

.method private checkObjectBounds()V
    .locals 9
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 3658
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v5, :cond_1

    .line 3711
    :cond_0
    :goto_0
    return-void

    .line 3662
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    .line 3664
    .local v3, "objectRect":Landroid/graphics/RectF;
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v0

    .line 3665
    .local v0, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v0, :cond_0

    .line 3670
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 3671
    .local v4, "relativeRect":Landroid/graphics/RectF;
    invoke-direct {p0, v4, v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3673
    iget v5, v3, Landroid/graphics/RectF;->left:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_LEFT_MARGIN:I

    add-int/lit8 v6, v6, 0x0

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2

    .line 3674
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_LEFT_MARGIN:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    iput v5, v3, Landroid/graphics/RectF;->right:F

    .line 3675
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_LEFT_MARGIN:I

    int-to-float v5, v5

    iput v5, v3, Landroid/graphics/RectF;->left:F

    .line 3677
    :cond_2
    iget v5, v3, Landroid/graphics/RectF;->top:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_TOP_MARGIN:I

    add-int/lit8 v6, v6, 0x0

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_3

    .line 3678
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_TOP_MARGIN:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    iput v5, v3, Landroid/graphics/RectF;->bottom:F

    .line 3679
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_TOP_MARGIN:I

    int-to-float v5, v5

    iput v5, v3, Landroid/graphics/RectF;->top:F

    .line 3681
    :cond_3
    iget v5, v3, Landroid/graphics/RectF;->right:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    int-to-float v7, v7

    sub-float/2addr v6, v7

    cmpl-float v5, v5, v6

    if-lez v5, :cond_6

    .line 3682
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v5

    if-eqz v5, :cond_4

    .line 3683
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5

    .line 3684
    :cond_4
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v6

    sub-float/2addr v5, v6

    iput v5, v3, Landroid/graphics/RectF;->left:F

    .line 3686
    :cond_5
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    iput v5, v3, Landroid/graphics/RectF;->right:F

    .line 3688
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDefaultWidth()I

    move-result v2

    .line 3689
    .local v2, "minWidth":I
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v5

    int-to-float v6, v2

    cmpg-float v5, v5, v6

    if-gez v5, :cond_6

    .line 3690
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    int-to-float v6, v2

    sub-float/2addr v5, v6

    iput v5, v3, Landroid/graphics/RectF;->left:F

    .line 3693
    .end local v2    # "minWidth":I
    :cond_6
    iget v5, v3, Landroid/graphics/RectF;->bottom:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    int-to-float v7, v7

    sub-float/2addr v6, v7

    cmpl-float v5, v5, v6

    if-lez v5, :cond_9

    .line 3694
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v5

    if-eqz v5, :cond_7

    .line 3695
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v5

    if-ne v5, v8, :cond_8

    .line 3696
    :cond_7
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v6

    sub-float/2addr v5, v6

    iput v5, v3, Landroid/graphics/RectF;->top:F

    .line 3698
    :cond_8
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    iput v5, v3, Landroid/graphics/RectF;->bottom:F

    .line 3700
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDefaultHeight()I

    move-result v1

    .line 3701
    .local v1, "minHeight":I
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v5

    int-to-float v6, v1

    cmpg-float v5, v5, v6

    if-gez v5, :cond_9

    .line 3702
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    int-to-float v6, v1

    sub-float/2addr v5, v6

    iput v5, v3, Landroid/graphics/RectF;->top:F

    .line 3706
    .end local v1    # "minHeight":I
    :cond_9
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5, v3, v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setRect(Landroid/graphics/RectF;Z)V

    .line 3708
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v5

    if-nez v5, :cond_0

    .line 3709
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onObjectChanged()V

    goto/16 :goto_0
.end method

.method private copy(Landroid/content/ClipboardManager;II)Z
    .locals 12
    .param p1, "clipboard"    # Landroid/content/ClipboardManager;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/16 v11, 0x96

    const/16 v10, 0x50

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 7109
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v6}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p2, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 7110
    .local v5, "str":Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 7111
    invoke-virtual {p1}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v0

    .line 7112
    .local v0, "clip":Landroid/content/ClipData;
    if-eqz v0, :cond_0

    .line 7113
    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    move-result v3

    .line 7114
    .local v3, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v3, :cond_2

    .line 7138
    .end local v1    # "i":I
    .end local v3    # "length":I
    :cond_0
    if-eqz v5, :cond_1

    .line 7139
    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 7140
    const-string v6, "clipData"

    invoke-static {v6, v5}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v0

    .line 7141
    invoke-virtual {p1, v0}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 7143
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    if-nez v6, :cond_5

    .line 7144
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 7145
    const-string/jumbo v7, "string_copied_to_clipboard"

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 7144
    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    .line 7150
    :goto_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6, v10, v8, v11}, Landroid/widget/Toast;->setGravity(III)V

    .line 7151
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 7154
    if-le p2, p3, :cond_6

    .end local p2    # "start":I
    :goto_2
    invoke-virtual {p0, p2, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 7158
    .end local v0    # "clip":Landroid/content/ClipData;
    :cond_1
    const/4 v5, 0x0

    .line 7159
    :goto_3
    return v9

    .line 7115
    .restart local v0    # "clip":Landroid/content/ClipData;
    .restart local v1    # "i":I
    .restart local v3    # "length":I
    .restart local p2    # "start":I
    :cond_2
    invoke-virtual {v0, v8}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v2

    .line 7116
    .local v2, "item":Landroid/content/ClipData$Item;
    if-eqz v2, :cond_4

    .line 7117
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    .line 7118
    .local v4, "sequence":Ljava/lang/CharSequence;
    if-eqz v4, :cond_4

    .line 7119
    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_4

    .line 7120
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    if-nez v6, :cond_3

    .line 7121
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 7122
    const-string/jumbo v7, "string_already_exists"

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 7121
    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    .line 7127
    :goto_4
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6, v10, v8, v11}, Landroid/widget/Toast;->setGravity(III)V

    .line 7128
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 7129
    const/4 v0, 0x0

    .line 7130
    const/4 v5, 0x0

    .line 7131
    goto :goto_3

    .line 7124
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    const-string/jumbo v7, "string_already_exists"

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 7125
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6, v8}, Landroid/widget/Toast;->setDuration(I)V

    goto :goto_4

    .line 7114
    .end local v4    # "sequence":Ljava/lang/CharSequence;
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 7147
    .end local v1    # "i":I
    .end local v2    # "item":Landroid/content/ClipData$Item;
    .end local v3    # "length":I
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    const-string/jumbo v7, "string_copied_to_clipboard"

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 7148
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6, v8}, Landroid/widget/Toast;->setDuration(I)V

    goto :goto_1

    :cond_6
    move p2, p3

    .line 7154
    goto :goto_2
.end method

.method private cut(Landroid/content/ClipboardManager;II)Z
    .locals 12
    .param p1, "clipboard"    # Landroid/content/ClipboardManager;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/16 v11, 0x96

    const/16 v10, 0x50

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 7045
    const/4 v5, 0x0

    .line 7046
    .local v5, "str":Ljava/lang/String;
    if-eq p2, p3, :cond_2

    .line 7047
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->lastText:Ljava/lang/String;

    .line 7049
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v6}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p2, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 7050
    if-eqz v5, :cond_1

    .line 7051
    invoke-virtual {p1}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v0

    .line 7052
    .local v0, "clip":Landroid/content/ClipData;
    if-eqz v0, :cond_0

    .line 7053
    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    move-result v3

    .line 7054
    .local v3, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v3, :cond_3

    .line 7084
    .end local v1    # "i":I
    .end local v3    # "length":I
    :cond_0
    const-string v6, "clipData"

    invoke-static {v6, v5}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v0

    .line 7085
    invoke-virtual {p1, v0}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 7087
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    if-nez v6, :cond_6

    .line 7088
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string/jumbo v7, "string_copied_to_clipboard"

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    .line 7094
    :goto_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6, v10, v8, v11}, Landroid/widget/Toast;->setGravity(III)V

    .line 7095
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 7097
    .end local v0    # "clip":Landroid/content/ClipData;
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v6}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v6

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 7098
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v6}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v6

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    .line 7099
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsChangedByKeyShortCut:Z

    .line 7100
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->removeText()V

    .line 7101
    iput-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsChangedByKeyShortCut:Z

    .line 7104
    :cond_2
    const/4 v5, 0x0

    .line 7105
    :goto_2
    return v9

    .line 7055
    .restart local v0    # "clip":Landroid/content/ClipData;
    .restart local v1    # "i":I
    .restart local v3    # "length":I
    :cond_3
    invoke-virtual {v0, v8}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v2

    .line 7056
    .local v2, "item":Landroid/content/ClipData$Item;
    if-eqz v2, :cond_5

    .line 7057
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    .line 7058
    .local v4, "sequence":Ljava/lang/CharSequence;
    if-eqz v4, :cond_5

    .line 7059
    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_5

    .line 7060
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    if-nez v6, :cond_4

    .line 7061
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 7062
    const-string/jumbo v7, "string_already_exists"

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 7061
    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    .line 7068
    :goto_3
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6, v10, v8, v11}, Landroid/widget/Toast;->setGravity(III)V

    .line 7069
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 7070
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v6}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v6

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 7071
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v6}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v6

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    .line 7072
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsChangedByKeyShortCut:Z

    .line 7073
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->removeText()V

    .line 7074
    iput-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsChangedByKeyShortCut:Z

    .line 7075
    const/4 v0, 0x0

    .line 7076
    const/4 v5, 0x0

    .line 7077
    goto :goto_2

    .line 7064
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    const-string/jumbo v7, "string_already_exists"

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 7065
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6, v8}, Landroid/widget/Toast;->setDuration(I)V

    goto :goto_3

    .line 7054
    .end local v4    # "sequence":Ljava/lang/CharSequence;
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 7091
    .end local v1    # "i":I
    .end local v2    # "item":Landroid/content/ClipData$Item;
    .end local v3    # "length":I
    :cond_6
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    const-string/jumbo v7, "string_copied_to_clipboard"

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 7092
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v6, v8}, Landroid/widget/Toast;->setDuration(I)V

    goto/16 :goto_1
.end method

.method private delete(II)Z
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v1, 0x1

    .line 7212
    if-eq p1, p2, :cond_1

    .line 7213
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    .line 7214
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsChangedByKeyShortCut:Z

    .line 7215
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z

    if-nez v0, :cond_0

    .line 7216
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->lastText:Ljava/lang/String;

    .line 7217
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z

    .line 7219
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 7220
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    .line 7221
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->removeText()V

    .line 7223
    :cond_1
    return v1
.end method

.method private deleteLastSpeechInput()V
    .locals 3

    .prologue
    .line 7017
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    .line 7018
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndexInput:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndexInput:I

    invoke-interface {v0, v1, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 7019
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndexInput:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 7020
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 7021
    return-void
.end method

.method private drawSelectRect(Landroid/graphics/Canvas;IILandroid/graphics/RectF;Landroid/graphics/Rect;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "startIndex"    # I
    .param p3, "endIndex"    # I
    .param p4, "result"    # Landroid/graphics/RectF;
    .param p5, "drawRect"    # Landroid/graphics/Rect;

    .prologue
    .line 4168
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-nez v5, :cond_1

    .line 4195
    :cond_0
    return-void

    .line 4172
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v5, :cond_0

    .line 4176
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v2

    .line 4178
    .local v2, "deltaY":F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    .line 4179
    .local v3, "objectRect":Landroid/graphics/RectF;
    if-eqz v3, :cond_0

    .line 4184
    move v1, p2

    .local v1, "cnt":I
    :goto_0
    if-ge v1, p3, :cond_0

    .line 4185
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/RectF;->top:F

    sub-float/2addr v5, v2

    float-to-int v4, v5

    .line 4186
    .local v4, "top":I
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v5, v2

    float-to-int v0, v5

    .line 4188
    .local v0, "bottom":I
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/RectF;->left:F

    float-to-int v6, v5

    if-gez v4, :cond_2

    const/4 v4, 0x0

    .end local v4    # "top":I
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/RectF;->right:F

    float-to-int v7, v5

    .line 4189
    int-to-float v5, v0

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v8

    cmpl-float v5, v5, v8

    if-lez v5, :cond_3

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v5

    :goto_1
    float-to-int v5, v5

    .line 4188
    invoke-virtual {p5, v6, v4, v7, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 4191
    invoke-direct {p0, p5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->applyScaledRect(Landroid/graphics/Rect;)V

    .line 4193
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p5, v5}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 4184
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4189
    :cond_3
    int-to-float v5, v0

    goto :goto_1
.end method

.method private drawSelectedLine(Landroid/graphics/Canvas;IILandroid/graphics/RectF;Landroid/graphics/Rect;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "startIndex"    # I
    .param p3, "endIndex"    # I
    .param p4, "result"    # Landroid/graphics/RectF;
    .param p5, "drawRect"    # Landroid/graphics/Rect;

    .prologue
    .line 4136
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-nez v5, :cond_1

    .line 4165
    :cond_0
    :goto_0
    return-void

    .line 4140
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, p2

    invoke-virtual {p4, v5}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 4141
    add-int/lit8 v1, p2, 0x1

    .local v1, "cnt":I
    :goto_1
    if-le v1, p3, :cond_3

    .line 4148
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v2

    .line 4150
    .local v2, "deltaY":F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    .line 4151
    .local v3, "objectRect":Landroid/graphics/RectF;
    if-eqz v3, :cond_0

    .line 4156
    iget v5, p4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v5, v2

    float-to-int v4, v5

    .line 4157
    .local v4, "top":I
    iget v5, p4, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v5, v2

    float-to-int v0, v5

    .line 4159
    .local v0, "bottom":I
    iget v5, p4, Landroid/graphics/RectF;->left:F

    float-to-int v6, v5

    if-gez v4, :cond_2

    const/4 v4, 0x0

    .end local v4    # "top":I
    :cond_2
    iget v5, p4, Landroid/graphics/RectF;->right:F

    float-to-int v7, v5

    .line 4160
    int-to-float v5, v0

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v8

    cmpl-float v5, v5, v8

    if-lez v5, :cond_8

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v5

    :goto_2
    float-to-int v5, v5

    .line 4159
    invoke-virtual {p5, v6, v4, v7, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 4162
    invoke-direct {p0, p5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->applyScaledRect(Landroid/graphics/Rect;)V

    .line 4164
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p5, v5}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 4142
    .end local v0    # "bottom":I
    .end local v2    # "deltaY":F
    .end local v3    # "objectRect":Landroid/graphics/RectF;
    :cond_3
    iget v5, p4, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v6, v6, v1

    iget v6, v6, Landroid/graphics/RectF;->left:F

    cmpg-float v5, v5, v6

    if-gez v5, :cond_4

    iget v5, p4, Landroid/graphics/RectF;->left:F

    :goto_3
    iput v5, p4, Landroid/graphics/RectF;->left:F

    .line 4143
    iget v5, p4, Landroid/graphics/RectF;->top:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v6, v6, v1

    iget v6, v6, Landroid/graphics/RectF;->top:F

    cmpg-float v5, v5, v6

    if-gez v5, :cond_5

    iget v5, p4, Landroid/graphics/RectF;->top:F

    :goto_4
    iput v5, p4, Landroid/graphics/RectF;->top:F

    .line 4144
    iget v5, p4, Landroid/graphics/RectF;->right:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v6, v6, v1

    iget v6, v6, Landroid/graphics/RectF;->right:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_6

    iget v5, p4, Landroid/graphics/RectF;->right:F

    :goto_5
    iput v5, p4, Landroid/graphics/RectF;->right:F

    .line 4145
    iget v5, p4, Landroid/graphics/RectF;->bottom:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v6, v6, v1

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_7

    iget v5, p4, Landroid/graphics/RectF;->bottom:F

    :goto_6
    iput v5, p4, Landroid/graphics/RectF;->bottom:F

    .line 4141
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4142
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/RectF;->left:F

    goto :goto_3

    .line 4143
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/RectF;->top:F

    goto :goto_4

    .line 4144
    :cond_6
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/RectF;->right:F

    goto :goto_5

    .line 4145
    :cond_7
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    goto :goto_6

    .line 4160
    .restart local v0    # "bottom":I
    .restart local v2    # "deltaY":F
    .restart local v3    # "objectRect":Landroid/graphics/RectF;
    :cond_8
    int-to-float v5, v0

    goto :goto_2
.end method

.method private findWord(ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z
    .locals 10
    .param p1, "index"    # I
    .param p2, "word"    # Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;

    .prologue
    const/16 v9, 0x20

    const/16 v8, 0xd

    const/16 v7, 0xa

    const/16 v6, 0x9

    const/4 v4, 0x0

    .line 4512
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v5, :cond_1

    .line 4559
    :cond_0
    :goto_0
    return v4

    .line 4515
    :cond_1
    iget-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v5, :cond_2

    .line 4516
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 4519
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getTextLength()I

    move-result v5

    if-ge p1, v5, :cond_0

    if-ltz p1, :cond_0

    .line 4523
    iput v4, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    iput v4, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    .line 4524
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineFromIndex(I)I

    move-result v1

    .line 4525
    .local v1, "line":I
    if-ltz v1, :cond_0

    .line 4529
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-eqz v5, :cond_0

    .line 4535
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v3

    .line 4536
    .local v3, "str":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    iput v4, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    .line 4537
    add-int/lit8 v0, p1, -0x1

    .local v0, "cnt":I
    :goto_1
    iget v4, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    if-ge v0, v4, :cond_4

    .line 4545
    :goto_2
    invoke-virtual {v3, p1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 4546
    .local v2, "oneChar":C
    if-eq v2, v9, :cond_3

    if-eq v2, v6, :cond_3

    if-eq v2, v7, :cond_3

    if-ne v2, v8, :cond_7

    .line 4547
    :cond_3
    iput p1, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    .line 4548
    const/4 v4, 0x1

    goto :goto_0

    .line 4538
    .end local v2    # "oneChar":C
    :cond_4
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 4539
    .restart local v2    # "oneChar":C
    if-eq v2, v9, :cond_5

    if-eq v2, v6, :cond_5

    if-eq v2, v7, :cond_5

    if-ne v2, v8, :cond_6

    .line 4540
    :cond_5
    add-int/lit8 v4, v0, 0x1

    iput v4, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    goto :goto_2

    .line 4537
    :cond_6
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 4551
    :cond_7
    add-int/lit8 v0, p1, 0x1

    :goto_3
    iget v4, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    if-lt v0, v4, :cond_8

    .line 4559
    :goto_4
    const/4 v4, 0x1

    goto :goto_0

    .line 4552
    :cond_8
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 4553
    if-eq v2, v9, :cond_9

    if-eq v2, v6, :cond_9

    if-eq v2, v7, :cond_9

    if-ne v2, v8, :cond_a

    .line 4554
    :cond_9
    iput v0, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    goto :goto_4

    .line 4551
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method private getAbsolutePoint(FF)[F
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v4, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    .line 5937
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v5, :cond_0

    move-object v2, v4

    .line 5961
    :goto_0
    return-object v2

    .line 5940
    :cond_0
    const/4 v5, 0x2

    new-array v2, v5, [F

    const/4 v5, 0x0

    aput p1, v2, v5

    const/4 v5, 0x1

    aput p2, v2, v5

    .line 5942
    .local v2, "point":[F
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 5944
    .local v3, "relativeRect":Landroid/graphics/RectF;
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v0

    .line 5945
    .local v0, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-nez v0, :cond_1

    move-object v2, v4

    .line 5947
    goto :goto_0

    .line 5950
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v4

    invoke-direct {p0, v3, v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 5952
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 5953
    .local v1, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 5954
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v5

    div-float/2addr v5, v7

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v6

    div-float/2addr v6, v7

    invoke-virtual {v1, v4, v5, v6}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 5955
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1, v4}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 5956
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v4, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 5958
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 5959
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v4, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    goto :goto_0
.end method

.method private getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .locals 2

    .prologue
    .line 3047
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 3048
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v0, :cond_0

    .line 3049
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3051
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    return-object v0
.end method

.method private getCurrnetLineIndex(I)I
    .locals 4
    .param p1, "pos"    # I

    .prologue
    .line 4581
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineCount(J)I

    move-result v1

    .line 4583
    .local v1, "lineCount":I
    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-gez v0, :cond_1

    .line 4589
    const/4 v0, 0x0

    .end local v0    # "i":I
    :cond_0
    return v0

    .line 4584
    .restart local v0    # "i":I
    :cond_1
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v2, v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineStartIndex(JI)I

    move-result v2

    if-ge p1, v2, :cond_0

    .line 4583
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private getCursorIndex(IF)I
    .locals 12
    .param p1, "line"    # I
    .param p2, "h"    # F

    .prologue
    const/4 v11, 0x0

    const/4 v1, 0x0

    .line 5236
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v9, :cond_1

    .line 5343
    :cond_0
    :goto_0
    return v1

    .line 5240
    :cond_1
    iget-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v9, :cond_2

    .line 5241
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 5244
    :cond_2
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-eqz v9, :cond_0

    .line 5249
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-lez v9, :cond_0

    .line 5251
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-lt p1, v9, :cond_3

    .line 5252
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    add-int/lit8 v10, v10, -0x1

    aget v1, v9, v10

    goto :goto_0

    .line 5253
    :cond_3
    if-ltz p1, :cond_0

    .line 5258
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v8

    .line 5259
    .local v8, "str":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 5263
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v9, v9, p1

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v10, v10, p1

    if-ne v9, v10, :cond_4

    iget-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFirstCharLF:Z

    if-eqz v9, :cond_4

    .line 5264
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v9, v9, p1

    add-int/lit8 v1, v9, 0x1

    goto :goto_0

    .line 5267
    :cond_4
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v9, v9, p1

    invoke-virtual {v8, v9}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 5268
    .local v4, "oneChar":C
    const/16 v9, 0xa

    if-eq v4, v9, :cond_5

    const/16 v9, 0xd

    if-ne v4, v9, :cond_6

    .line 5269
    :cond_5
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v9, v9, p1

    add-int/lit8 v5, v9, 0x1

    .line 5273
    .local v5, "start":I
    :goto_1
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v0, v9, p1

    .line 5275
    .local v0, "end":I
    if-le v5, v0, :cond_7

    move v1, v5

    .line 5276
    goto :goto_0

    .line 5271
    .end local v0    # "end":I
    .end local v5    # "start":I
    :cond_6
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v5, v9, p1

    .restart local v5    # "start":I
    goto :goto_1

    .line 5282
    .restart local v0    # "end":I
    :cond_7
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v6, v9, p1

    .local v6, "startIndex":I
    move v2, v6

    .line 5283
    .local v2, "lastIndex":I
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v10, v10, p1

    aget-object v9, v9, v10

    iget v7, v9, Landroid/graphics/RectF;->left:F

    .local v7, "startX":F
    move v3, v7

    .line 5285
    .local v3, "lastX":F
    move v1, v5

    .local v1, "index":I
    :goto_2
    if-le v1, v0, :cond_8

    .line 5333
    cmpg-float v9, p2, v7

    if-gez v9, :cond_10

    .line 5334
    invoke-direct {p0, v8, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isL2R(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_f

    move v1, v6

    .line 5335
    goto/16 :goto_0

    .line 5286
    :cond_8
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v9, v9, Landroid/graphics/RectF;->left:F

    cmpl-float v9, p2, v9

    if-ltz v9, :cond_c

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v9, v9, Landroid/graphics/RectF;->right:F

    cmpg-float v9, p2, v9

    if-gtz v9, :cond_c

    .line 5288
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    invoke-virtual {v9}, Landroid/graphics/RectF;->centerX()F

    move-result v9

    cmpl-float v9, p2, v9

    if-ltz v9, :cond_a

    .line 5289
    invoke-direct {p0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isL2R(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 5290
    if-gt v1, v0, :cond_9

    .line 5291
    add-int/lit8 v1, v1, 0x1

    .line 5294
    :cond_9
    :goto_3
    if-gt v1, v0, :cond_0

    .line 5295
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v9

    cmpl-float v9, v9, v11

    if-nez v9, :cond_0

    .line 5294
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 5305
    :cond_a
    invoke-direct {p0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isL2R(Ljava/lang/String;I)Z

    move-result v9

    if-nez v9, :cond_0

    .line 5308
    if-gt v1, v0, :cond_b

    .line 5309
    add-int/lit8 v1, v1, 0x1

    .line 5312
    :cond_b
    :goto_4
    if-gt v1, v0, :cond_0

    .line 5313
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v9

    cmpl-float v9, v9, v11

    if-nez v9, :cond_0

    .line 5312
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 5322
    :cond_c
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v9, v9, Landroid/graphics/RectF;->left:F

    cmpg-float v9, v3, v9

    if-gez v9, :cond_d

    .line 5323
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v3, v9, Landroid/graphics/RectF;->left:F

    .line 5324
    move v2, v1

    .line 5326
    :cond_d
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v9, v9, Landroid/graphics/RectF;->left:F

    cmpl-float v9, v7, v9

    if-lez v9, :cond_e

    .line 5327
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v7, v9, Landroid/graphics/RectF;->left:F

    .line 5328
    move v6, v1

    .line 5285
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    .line 5337
    :cond_f
    add-int/lit8 v1, v6, 0x1

    goto/16 :goto_0

    .line 5340
    :cond_10
    invoke-direct {p0, v8, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isL2R(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 5341
    add-int/lit8 v1, v2, 0x1

    goto/16 :goto_0

    :cond_11
    move v1, v2

    .line 5343
    goto/16 :goto_0
.end method

.method private getCursorRect(I)Landroid/graphics/Rect;
    .locals 12
    .param p1, "pos"    # I

    .prologue
    .line 4324
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-nez v8, :cond_1

    .line 4326
    const/4 v4, 0x0

    .line 4480
    :cond_0
    :goto_0
    return-object v4

    .line 4329
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v8, :cond_2

    .line 4330
    const/4 v4, 0x0

    goto :goto_0

    .line 4333
    :cond_2
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 4335
    .local v4, "rect":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    .line 4337
    .local v6, "str":Ljava/lang/String;
    if-eqz v6, :cond_3

    if-ltz p1, :cond_3

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-le p1, v8, :cond_4

    .line 4338
    :cond_3
    const/4 v4, 0x0

    goto :goto_0

    .line 4341
    :cond_4
    if-lez p1, :cond_e

    .line 4342
    const/4 v7, 0x0

    .line 4343
    .local v7, "textRect":Landroid/graphics/RectF;
    iget-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v8, :cond_8

    .line 4346
    new-instance v7, Landroid/graphics/RectF;

    .end local v7    # "textRect":Landroid/graphics/RectF;
    invoke-direct {v7}, Landroid/graphics/RectF;-><init>()V

    .line 4347
    .restart local v7    # "textRect":Landroid/graphics/RectF;
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFrontPosition(I)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 4348
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9, p1, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getTextRect(JILandroid/graphics/RectF;)Z

    .line 4350
    const/4 v8, 0x0

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getGravity()I

    move-result v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDeltaY(I)F

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/RectF;->offset(FF)V

    .line 4352
    add-int/lit8 v8, p1, -0x1

    invoke-direct {p0, v6, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isL2R(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 4353
    iget v8, v7, Landroid/graphics/RectF;->left:F

    float-to-int v8, v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    sub-int/2addr v8, v9

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    iget v10, v7, Landroid/graphics/RectF;->left:F

    float-to-int v10, v10

    .line 4354
    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    add-int/2addr v10, v11

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4353
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 4356
    :cond_5
    iget v8, v7, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    sub-int/2addr v8, v9

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    iget v10, v7, Landroid/graphics/RectF;->right:F

    float-to-int v10, v10

    .line 4357
    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    add-int/2addr v10, v11

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4356
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 4360
    :cond_6
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    add-int/lit8 v10, p1, -0x1

    invoke-direct {p0, v8, v9, v10, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getTextRect(JILandroid/graphics/RectF;)Z

    .line 4362
    const/4 v8, 0x0

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getGravity()I

    move-result v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDeltaY(I)F

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/RectF;->offset(FF)V

    .line 4364
    add-int/lit8 v8, p1, -0x1

    invoke-direct {p0, v6, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isL2R(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 4365
    iget v8, v7, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    sub-int/2addr v8, v9

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    iget v10, v7, Landroid/graphics/RectF;->right:F

    float-to-int v10, v10

    .line 4366
    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    add-int/2addr v10, v11

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4365
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 4368
    :cond_7
    iget v8, v7, Landroid/graphics/RectF;->left:F

    float-to-int v8, v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    sub-int/2addr v8, v9

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    iget v10, v7, Landroid/graphics/RectF;->left:F

    float-to-int v10, v10

    .line 4369
    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    add-int/2addr v10, v11

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4368
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 4374
    :cond_8
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-eqz v8, :cond_9

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    if-nez v8, :cond_a

    .line 4375
    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 4377
    :cond_a
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFrontPosition(I)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 4378
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v7, v8, p1

    .line 4380
    add-int/lit8 v8, p1, -0x1

    invoke-direct {p0, v6, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isL2R(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 4381
    iget v8, v7, Landroid/graphics/RectF;->left:F

    float-to-int v8, v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    sub-int/2addr v8, v9

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    iget v10, v7, Landroid/graphics/RectF;->left:F

    float-to-int v10, v10

    .line 4382
    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    add-int/2addr v10, v11

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4381
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 4384
    :cond_b
    iget v8, v7, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    sub-int/2addr v8, v9

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    iget v10, v7, Landroid/graphics/RectF;->right:F

    float-to-int v10, v10

    .line 4385
    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    add-int/2addr v10, v11

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4384
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 4388
    :cond_c
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    add-int/lit8 v9, p1, -0x1

    aget-object v7, v8, v9

    .line 4389
    add-int/lit8 v8, p1, -0x1

    invoke-direct {p0, v6, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isL2R(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 4390
    iget v8, v7, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    sub-int/2addr v8, v9

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    iget v10, v7, Landroid/graphics/RectF;->right:F

    float-to-int v10, v10

    .line 4391
    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    add-int/2addr v10, v11

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4390
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 4393
    :cond_d
    iget v8, v7, Landroid/graphics/RectF;->left:F

    float-to-int v8, v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    sub-int/2addr v8, v9

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    iget v10, v7, Landroid/graphics/RectF;->left:F

    float-to-int v10, v10

    .line 4394
    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    add-int/2addr v10, v11

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4393
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 4399
    .end local v7    # "textRect":Landroid/graphics/RectF;
    :cond_e
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_11

    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const/4 v10, 0x0

    invoke-direct {p0, v8, v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineStartIndex(JI)I

    move-result v8

    const/4 v9, -0x1

    if-eq v8, v9, :cond_11

    .line 4400
    const/4 v7, 0x0

    .line 4401
    .restart local v7    # "textRect":Landroid/graphics/RectF;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-nez v8, :cond_f

    .line 4402
    new-instance v7, Landroid/graphics/RectF;

    .end local v7    # "textRect":Landroid/graphics/RectF;
    invoke-direct {v7}, Landroid/graphics/RectF;-><init>()V

    .line 4403
    .restart local v7    # "textRect":Landroid/graphics/RectF;
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const/4 v10, 0x0

    invoke-direct {p0, v8, v9, v10, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getTextRect(JILandroid/graphics/RectF;)Z

    .line 4409
    :goto_1
    invoke-direct {p0, v6, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isL2R(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 4410
    iget v8, v7, Landroid/graphics/RectF;->left:F

    float-to-int v8, v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    sub-int/2addr v8, v9

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    .line 4411
    iget v10, v7, Landroid/graphics/RectF;->left:F

    float-to-int v10, v10

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    add-int/2addr v10, v11

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4410
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 4406
    :cond_f
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    const/4 v9, 0x0

    aget-object v7, v8, v9

    goto :goto_1

    .line 4413
    :cond_10
    iget v8, v7, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    sub-int/2addr v8, v9

    iget v9, v7, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    iget v10, v7, Landroid/graphics/RectF;->right:F

    float-to-int v10, v10

    .line 4414
    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    add-int/2addr v10, v11

    iget v11, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v11, v11

    .line 4413
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 4419
    .end local v7    # "textRect":Landroid/graphics/RectF;
    :cond_11
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v5

    .line 4420
    .local v5, "spans":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    if-eqz v5, :cond_12

    .line 4421
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v0, v8, -0x1

    .local v0, "cnt":I
    :goto_2
    if-gez v0, :cond_17

    .line 4433
    .end local v0    # "cnt":I
    :cond_12
    :goto_3
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getParagraph()Ljava/util/ArrayList;

    move-result-object v3

    .line 4434
    .local v3, "pInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    if-eqz v3, :cond_13

    .line 4435
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .restart local v0    # "cnt":I
    :goto_4
    if-lt v1, v0, :cond_19

    .line 4465
    .end local v0    # "cnt":I
    .end local v1    # "i":I
    :cond_13
    if-eqz v3, :cond_14

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-gtz v8, :cond_15

    .line 4466
    :cond_14
    iget v8, v4, Landroid/graphics/Rect;->right:I

    iget v9, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    int-to-float v8, v8

    const v9, 0x33d6bf95    # 1.0E-7f

    cmpl-float v8, v8, v9

    if-lez v8, :cond_15

    iget v8, v4, Landroid/graphics/Rect;->bottom:I

    iget v9, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    int-to-float v8, v8

    const v9, 0x33d6bf95    # 1.0E-7f

    cmpl-float v8, v8, v9

    if-lez v8, :cond_15

    .line 4467
    iget v8, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    const v10, 0x3fa66666    # 1.3f

    mul-float/2addr v9, v10

    float-to-int v9, v9

    add-int/2addr v8, v9

    iput v8, v4, Landroid/graphics/Rect;->bottom:I

    .line 4471
    :cond_15
    iget v8, v4, Landroid/graphics/Rect;->right:I

    iget v9, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    int-to-float v8, v8

    const v9, 0x33d6bf95    # 1.0E-7f

    cmpg-float v8, v8, v9

    if-ltz v8, :cond_16

    iget v8, v4, Landroid/graphics/Rect;->bottom:I

    iget v9, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    int-to-float v8, v8

    const v9, 0x33d6bf95    # 1.0E-7f

    cmpg-float v8, v8, v9

    if-gez v8, :cond_0

    .line 4472
    :cond_16
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTopMargin()F

    move-result v8

    float-to-int v8, v8

    iput v8, v4, Landroid/graphics/Rect;->top:I

    .line 4473
    iget v8, v4, Landroid/graphics/Rect;->top:I

    int-to-float v8, v8

    const v9, 0x423b3333    # 46.8f

    add-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, v4, Landroid/graphics/Rect;->bottom:I

    .line 4474
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getLeftMargin()F

    move-result v8

    float-to-int v8, v8

    iput v8, v4, Landroid/graphics/Rect;->left:I

    .line 4475
    iget v8, v4, Landroid/graphics/Rect;->left:I

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    mul-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    iput v8, v4, Landroid/graphics/Rect;->right:I

    goto/16 :goto_0

    .line 4422
    .end local v3    # "pInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    .restart local v0    # "cnt":I
    :cond_17
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    instance-of v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v8, :cond_18

    .line 4423
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTopMargin()F

    move-result v8

    float-to-int v8, v8

    iput v8, v4, Landroid/graphics/Rect;->top:I

    .line 4424
    iget v9, v4, Landroid/graphics/Rect;->top:I

    .line 4425
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    float-to-int v8, v8

    add-int/2addr v8, v9

    .line 4424
    iput v8, v4, Landroid/graphics/Rect;->bottom:I

    .line 4426
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getLeftMargin()F

    move-result v8

    float-to-int v8, v8

    iput v8, v4, Landroid/graphics/Rect;->left:I

    .line 4427
    iget v8, v4, Landroid/graphics/Rect;->left:I

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorWidth:I

    mul-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    iput v8, v4, Landroid/graphics/Rect;->right:I

    goto/16 :goto_3

    .line 4421
    :cond_18
    add-int/lit8 v0, v0, -0x1

    goto/16 :goto_2

    .line 4436
    .restart local v1    # "i":I
    .restart local v3    # "pInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    :cond_19
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    instance-of v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    if-eqz v8, :cond_1a

    .line 4437
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    if-nez v8, :cond_1b

    .line 4438
    iget v9, v4, Landroid/graphics/Rect;->top:I

    .line 4439
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    float-to-int v8, v8

    add-int/2addr v8, v9

    .line 4438
    iput v8, v4, Landroid/graphics/Rect;->bottom:I

    .line 4447
    :cond_1a
    :goto_5
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    instance-of v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    if-eqz v8, :cond_1d

    .line 4448
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v2

    .line 4449
    .local v2, "objectRect":Landroid/graphics/RectF;
    if-nez v2, :cond_1c

    .line 4451
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 4441
    .end local v2    # "objectRect":Landroid/graphics/RectF;
    :cond_1b
    iget v9, v4, Landroid/graphics/Rect;->top:I

    .line 4442
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v8

    int-to-float v10, v8

    .line 4443
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    .line 4442
    mul-float/2addr v8, v10

    float-to-int v8, v8

    add-int/2addr v8, v9

    .line 4441
    iput v8, v4, Landroid/graphics/Rect;->bottom:I

    goto :goto_5

    .line 4453
    .restart local v2    # "objectRect":Landroid/graphics/RectF;
    :cond_1c
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_1e

    .line 4454
    iget v8, v4, Landroid/graphics/Rect;->left:I

    int-to-float v8, v8

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v9

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, v4, Landroid/graphics/Rect;->left:I

    .line 4455
    iget v8, v4, Landroid/graphics/Rect;->right:I

    int-to-float v8, v8

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v9

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, v4, Landroid/graphics/Rect;->right:I

    .line 4435
    .end local v2    # "objectRect":Landroid/graphics/RectF;
    :cond_1d
    :goto_6
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_4

    .line 4456
    .restart local v2    # "objectRect":Landroid/graphics/RectF;
    :cond_1e
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_1d

    .line 4457
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRightMargin()F

    move-result v9

    sub-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBorderLineWidth:I

    int-to-float v9, v9

    sub-float/2addr v8, v9

    .line 4458
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    sub-float/2addr v8, v9

    float-to-int v8, v8

    .line 4457
    iput v8, v4, Landroid/graphics/Rect;->left:I

    .line 4459
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRightMargin()F

    move-result v9

    sub-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBorderLineWidth:I

    int-to-float v9, v9

    sub-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, v4, Landroid/graphics/Rect;->right:I

    goto :goto_6
.end method

.method private getDeltaY(I)F
    .locals 9
    .param p1, "gravity"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 4283
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    .line 4284
    .local v3, "objectRect":Landroid/graphics/RectF;
    if-nez v3, :cond_1

    .line 4319
    :cond_0
    :goto_0
    return v2

    .line 4289
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v1

    .line 4290
    .local v1, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v1, :cond_0

    .line 4295
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    .line 4296
    .local v5, "relativeRect":Landroid/graphics/RectF;
    invoke-direct {p0, v5, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 4298
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v0

    .line 4299
    .local v0, "absoluteHeight":I
    const/4 v4, 0x0

    .line 4300
    .local v4, "relativeHeight":I
    if-nez v0, :cond_4

    .line 4301
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDefaultHeight()I

    move-result v4

    .line 4306
    :goto_1
    const/4 v2, 0x0

    .line 4308
    .local v2, "diffY":F
    if-eq p1, v8, :cond_2

    const/4 v6, 0x2

    if-ne p1, v6, :cond_3

    .line 4309
    :cond_2
    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v6

    int-to-float v7, v4

    sub-float v2, v6, v7

    .line 4310
    if-ne p1, v8, :cond_3

    .line 4311
    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    .line 4315
    :cond_3
    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v6

    int-to-float v7, v4

    cmpg-float v6, v6, v7

    if-gez v6, :cond_0

    .line 4316
    const/4 v2, 0x0

    goto :goto_0

    .line 4303
    .end local v2    # "diffY":F
    :cond_4
    int-to-float v6, v0

    iget v7, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v6, v7

    float-to-int v4, v6

    goto :goto_1
.end method

.method private getDrawableImage(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "drawableName"    # Ljava/lang/String;

    .prologue
    .line 5123
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 5124
    .local v3, "manager":Landroid/content/pm/PackageManager;
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v1

    .line 5126
    .local v1, "mApk1Resources":Landroid/content/res/Resources;
    const-string v5, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, p1, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 5128
    .local v2, "mDrawableResID":I
    const/4 v4, 0x0

    .line 5130
    .local v4, "tmp":Landroid/graphics/Bitmap;
    :try_start_1
    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    .line 5139
    .end local v1    # "mApk1Resources":Landroid/content/res/Resources;
    .end local v2    # "mDrawableResID":I
    .end local v3    # "manager":Landroid/content/pm/PackageManager;
    .end local v4    # "tmp":Landroid/graphics/Bitmap;
    :goto_0
    return-object v4

    .line 5131
    .restart local v1    # "mApk1Resources":Landroid/content/res/Resources;
    .restart local v2    # "mDrawableResID":I
    .restart local v3    # "manager":Landroid/content/pm/PackageManager;
    .restart local v4    # "tmp":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v0

    .line 5132
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 5136
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    .end local v1    # "mApk1Resources":Landroid/content/res/Resources;
    .end local v2    # "mDrawableResID":I
    .end local v3    # "manager":Landroid/content/pm/PackageManager;
    .end local v4    # "tmp":Landroid/graphics/Bitmap;
    :catch_1
    move-exception v0

    .line 5137
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 5139
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private getInvMatrix()Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 4942
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 4943
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method private getLineForVertical(F)I
    .locals 3
    .param p1, "v"    # F

    .prologue
    const/4 v1, 0x0

    .line 5163
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v2, :cond_0

    .line 5164
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 5167
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    if-nez v2, :cond_2

    move v0, v1

    .line 5177
    :cond_1
    :goto_0
    return v0

    .line 5171
    :cond_2
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    add-int/lit8 v0, v2, -0x1

    .local v0, "line":I
    :goto_1
    if-gez v0, :cond_3

    move v0, v1

    .line 5177
    goto :goto_0

    .line 5172
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/graphics/PointF;->y:F

    cmpl-float v2, p1, v2

    if-gez v2, :cond_1

    .line 5171
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method private getLineFromIndex(I)I
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/4 v1, 0x0

    .line 4563
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-nez v2, :cond_1

    move v0, v1

    .line 4577
    :cond_0
    :goto_0
    return v0

    .line 4567
    :cond_1
    if-gez p1, :cond_2

    move v0, v1

    .line 4568
    goto :goto_0

    .line 4571
    :cond_2
    const/4 v0, 0x0

    .local v0, "line":I
    :goto_1
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-lt v0, v2, :cond_3

    move v0, v1

    .line 4577
    goto :goto_0

    .line 4572
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v2, v2, v0

    if-le p1, v2, :cond_0

    .line 4571
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "strName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 5145
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 5146
    .local v1, "manager":Landroid/content/pm/PackageManager;
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v2

    .line 5148
    .local v2, "sdkResources":Landroid/content/res/Resources;
    const-string/jumbo v5, "string"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, p1, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 5149
    .local v3, "strID":I
    if-nez v3, :cond_0

    .line 5157
    .end local v1    # "manager":Landroid/content/pm/PackageManager;
    .end local v2    # "sdkResources":Landroid/content/res/Resources;
    .end local v3    # "strID":I
    :goto_0
    return-object v4

    .line 5152
    .restart local v1    # "manager":Landroid/content/pm/PackageManager;
    .restart local v2    # "sdkResources":Landroid/content/res/Resources;
    .restart local v3    # "strID":I
    :cond_0
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 5153
    .local v4, "string":Ljava/lang/String;
    goto :goto_0

    .line 5155
    .end local v1    # "manager":Landroid/content/pm/PackageManager;
    .end local v2    # "sdkResources":Landroid/content/res/Resources;
    .end local v3    # "strID":I
    .end local v4    # "string":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 5156
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private getOffsetForHorizontal(IF)I
    .locals 11
    .param p1, "line"    # I
    .param p2, "h"    # F

    .prologue
    const/4 v1, 0x0

    .line 5181
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v9, :cond_1

    .line 5230
    :cond_0
    :goto_0
    return v1

    .line 5184
    :cond_1
    iget-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v9, :cond_2

    .line 5185
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 5188
    :cond_2
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-eqz v9, :cond_0

    .line 5192
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-ge p1, v9, :cond_0

    if-ltz p1, :cond_0

    .line 5196
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v9, v9, p1

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v10, v10, p1

    if-ne v9, v10, :cond_3

    .line 5197
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v9, v9, p1

    add-int/lit8 v1, v9, 0x1

    goto :goto_0

    .line 5201
    :cond_3
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v8

    .line 5202
    .local v8, "str":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v9, v9, p1

    invoke-virtual {v8, v9}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 5203
    .local v4, "oneChar":C
    const/16 v9, 0xa

    if-eq v4, v9, :cond_4

    const/16 v9, 0xd

    if-ne v4, v9, :cond_5

    .line 5204
    :cond_4
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v9, v9, p1

    add-int/lit8 v5, v9, 0x1

    .line 5208
    .local v5, "start":I
    :goto_1
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v0, v9, p1

    .line 5213
    .local v0, "end":I
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v6, v9, p1

    .local v6, "startIndex":I
    move v2, v6

    .line 5214
    .local v2, "lastIndex":I
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v10, v10, p1

    aget-object v9, v9, v10

    iget v7, v9, Landroid/graphics/RectF;->left:F

    .local v7, "startX":F
    move v3, v7

    .line 5216
    .local v3, "lastX":F
    move v1, v5

    .local v1, "index":I
    :goto_2
    if-le v1, v0, :cond_6

    .line 5230
    cmpg-float v9, p2, v7

    if-gez v9, :cond_a

    .end local v6    # "startIndex":I
    :goto_3
    move v1, v6

    goto :goto_0

    .line 5206
    .end local v0    # "end":I
    .end local v1    # "index":I
    .end local v2    # "lastIndex":I
    .end local v3    # "lastX":F
    .end local v5    # "start":I
    .end local v7    # "startX":F
    :cond_5
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v5, v9, p1

    .restart local v5    # "start":I
    goto :goto_1

    .line 5217
    .restart local v0    # "end":I
    .restart local v1    # "index":I
    .restart local v2    # "lastIndex":I
    .restart local v3    # "lastX":F
    .restart local v6    # "startIndex":I
    .restart local v7    # "startX":F
    :cond_6
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v9, v9, Landroid/graphics/RectF;->left:F

    cmpl-float v9, p2, v9

    if-ltz v9, :cond_7

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v9, v9, Landroid/graphics/RectF;->right:F

    cmpg-float v9, p2, v9

    if-lez v9, :cond_0

    .line 5220
    :cond_7
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v9, v9, Landroid/graphics/RectF;->left:F

    cmpg-float v9, v3, v9

    if-gez v9, :cond_8

    .line 5221
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v3, v9, Landroid/graphics/RectF;->left:F

    .line 5222
    move v2, v1

    .line 5224
    :cond_8
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v9, v9, Landroid/graphics/RectF;->left:F

    cmpl-float v9, v7, v9

    if-lez v9, :cond_9

    .line 5225
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v9, v9, v1

    iget v7, v9, Landroid/graphics/RectF;->left:F

    .line 5226
    move v6, v1

    .line 5216
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_a
    move v6, v2

    .line 5230
    goto :goto_3
.end method

.method private getRelativePoint(FF)[F
    .locals 6
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v3, 0x0

    .line 5978
    const/4 v4, 0x2

    new-array v2, v4, [F

    const/4 v4, 0x0

    aput p1, v2, v4

    const/4 v4, 0x1

    aput p2, v2, v4

    .line 5979
    .local v2, "pts":[F
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v4, :cond_0

    move-object v2, v3

    .line 5994
    .end local v2    # "pts":[F
    :goto_0
    return-object v2

    .line 5983
    .restart local v2    # "pts":[F
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    .line 5984
    .local v1, "objectRect":Landroid/graphics/RectF;
    if-nez v1, :cond_1

    move-object v2, v3

    .line 5985
    goto :goto_0

    .line 5988
    :cond_1
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 5989
    .local v0, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 5990
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 5992
    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    goto :goto_0
.end method

.method private getRotatePoint(IIFFD)Landroid/graphics/PointF;
    .locals 21
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "pivotX"    # F
    .param p4, "pivotY"    # F
    .param p5, "degrees"    # D

    .prologue
    .line 5966
    invoke-static/range {p5 .. p6}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    .line 5967
    .local v4, "dSetDegree":D
    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    .line 5968
    .local v2, "cosq":D
    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    .line 5969
    .local v10, "sinq":D
    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v16, v0

    sub-float v16, v16, p3

    move/from16 v0, v16

    float-to-double v12, v0

    .line 5970
    .local v12, "sx":D
    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v16, v0

    sub-float v16, v16, p4

    move/from16 v0, v16

    float-to-double v14, v0

    .line 5971
    .local v14, "sy":D
    mul-double v16, v12, v2

    mul-double v18, v14, v10

    sub-double v16, v16, v18

    move/from16 v0, p3

    float-to-double v0, v0

    move-wide/from16 v18, v0

    add-double v6, v16, v18

    .line 5972
    .local v6, "rx":D
    mul-double v16, v12, v10

    mul-double v18, v14, v2

    add-double v16, v16, v18

    move/from16 v0, p4

    float-to-double v0, v0

    move-wide/from16 v18, v0

    add-double v8, v16, v18

    .line 5974
    .local v8, "ry":D
    new-instance v16, Landroid/graphics/PointF;

    double-to-float v0, v6

    move/from16 v17, v0

    double-to-float v0, v8

    move/from16 v18, v0

    invoke-direct/range {v16 .. v18}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v16
.end method

.method private getScrollBarRect()Landroid/graphics/Rect;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 3796
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_1

    .line 3816
    :cond_0
    :goto_0
    return-object v2

    .line 3800
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    .line 3801
    .local v1, "objectRect":Landroid/graphics/RectF;
    if-eqz v1, :cond_0

    .line 3806
    iget v3, v1, Landroid/graphics/RectF;->left:F

    neg-float v3, v3

    iget v4, v1, Landroid/graphics/RectF;->top:F

    neg-float v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/graphics/RectF;->offset(FF)V

    .line 3808
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v0

    .line 3810
    .local v0, "deltaY":F
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 3811
    .local v2, "rect":Landroid/graphics/Rect;
    iget v3, v1, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDefaultPadding:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBorderLineWidth:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarWidth:I

    add-int/2addr v4, v5

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 3812
    iget v3, v1, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDefaultPadding:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBorderLineWidth:I

    add-int/2addr v4, v5

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 3813
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v4

    int-to-float v4, v4

    div-float v4, v0, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 3814
    iget v3, v2, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v5

    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    goto :goto_0
.end method

.method private getSelectionRect(II)Landroid/graphics/RectF;
    .locals 6
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v4, 0x0

    .line 4484
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    move-object v3, v4

    .line 4508
    :goto_0
    return-object v3

    .line 4488
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v2

    .line 4489
    .local v2, "len":I
    if-ge p1, p2, :cond_2

    if-ltz p1, :cond_2

    if-ltz p2, :cond_2

    if-gt p1, v2, :cond_2

    if-le p2, v2, :cond_3

    :cond_2
    move-object v3, v4

    .line 4490
    goto :goto_0

    .line 4493
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-nez v5, :cond_4

    move-object v3, v4

    .line 4494
    goto :goto_0

    .line 4497
    :cond_4
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 4498
    .local v3, "rect":Landroid/graphics/RectF;
    move v1, p1

    .local v1, "i":I
    :goto_1
    if-lt v1, p2, :cond_5

    .line 4502
    invoke-virtual {v3}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_6

    move-object v3, v4

    .line 4503
    goto :goto_0

    .line 4499
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    invoke-virtual {v3, v5}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 4498
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4506
    :cond_6
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v0

    .line 4507
    .local v0, "deltaY":F
    const/4 v4, 0x0

    neg-float v5, v0

    invoke-virtual {v3, v4, v5}, Landroid/graphics/RectF;->offset(FF)V

    goto :goto_0
.end method

.method private getSettingInfo(II)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 17
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 4947
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v15, :cond_1

    .line 4948
    const/4 v14, 0x0

    .line 5118
    :cond_0
    return-object v14

    .line 4951
    :cond_1
    new-instance v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v14}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    .line 4953
    .local v14, "textInfo":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    const/high16 v15, 0x42100000    # 36.0f

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 4955
    const/4 v12, 0x0

    .local v12, "sLength":I
    const/4 v4, 0x0

    .local v4, "cLength":I
    const/4 v2, 0x0

    .local v2, "bgLength":I
    const/4 v6, 0x0

    .line 4956
    .local v6, "fLength":I
    const/4 v13, 0x0

    .local v13, "sMatch":Z
    const/4 v5, 0x0

    .local v5, "cMatch":Z
    const/4 v3, 0x0

    .local v3, "bgMatch":Z
    const/4 v7, 0x0

    .line 4958
    .local v7, "fMatch":Z
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v15, v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v11

    .line 4959
    .local v11, "sInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    if-eqz v11, :cond_2

    .line 4960
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v15

    add-int/lit8 v8, v15, -0x1

    .local v8, "i":I
    :goto_0
    if-gez v8, :cond_4

    .line 5102
    .end local v8    # "i":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v15}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getParagraph()Ljava/util/ArrayList;

    move-result-object v10

    .line 5103
    .local v10, "pInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    if-eqz v10, :cond_0

    .line 5104
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_3
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;

    .line 5105
    .local v9, "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    instance-of v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    if-eqz v15, :cond_20

    .line 5106
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    goto :goto_1

    .line 4961
    .end local v10    # "pInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    .restart local v8    # "i":I
    :cond_4
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 4963
    .local v9, "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    instance-of v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v15, :cond_b

    if-nez v13, :cond_b

    .line 4964
    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_8

    .line 4965
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iget v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v15, v0, :cond_6

    .line 4966
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 4967
    const/4 v13, 0x1

    .line 4960
    :cond_5
    :goto_2
    add-int/lit8 v8, v8, -0x1

    goto :goto_0

    .line 4971
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_6
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-ne v15, v0, :cond_7

    if-nez p2, :cond_5

    .line 4972
    :cond_7
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 4973
    add-int/lit8 v12, v12, 0x1

    .line 4976
    goto :goto_2

    .line 4977
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_8
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-gt v15, v0, :cond_9

    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-lt v15, v0, :cond_9

    .line 4978
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 4979
    const/4 v13, 0x1

    .line 4981
    goto :goto_2

    .line 4983
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_9
    if-nez v12, :cond_a

    .line 4984
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 4985
    add-int/lit8 v12, v12, 0x1

    .line 4986
    goto :goto_2

    .line 4987
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_a
    iget v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    move/from16 v16, v0

    cmpl-float v15, v15, v16

    if-eqz v15, :cond_5

    .line 4988
    const/4 v15, 0x0

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    goto :goto_2

    .line 4992
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_b
    instance-of v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    if-eqz v15, :cond_10

    if-nez v5, :cond_10

    .line 4993
    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_d

    .line 4994
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iget v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v15, v0, :cond_c

    .line 4995
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 4996
    const/4 v5, 0x1

    .line 4998
    goto :goto_2

    .line 5000
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_c
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-eq v15, v0, :cond_5

    .line 5001
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 5002
    add-int/lit8 v4, v4, 0x1

    .line 5005
    goto :goto_2

    .line 5006
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_d
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-gt v15, v0, :cond_e

    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-lt v15, v0, :cond_e

    .line 5007
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 5008
    const/4 v5, 0x1

    .line 5010
    goto/16 :goto_2

    .line 5012
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_e
    if-nez v4, :cond_f

    .line 5013
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 5014
    add-int/lit8 v4, v4, 0x1

    .line 5015
    goto/16 :goto_2

    .line 5016
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_f
    iget v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-eq v15, v0, :cond_5

    .line 5017
    const/4 v15, 0x0

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    goto/16 :goto_2

    .line 5021
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_10
    instance-of v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    if-eqz v15, :cond_15

    if-nez v3, :cond_15

    .line 5022
    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_12

    .line 5023
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iget v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 5024
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->bgColor:I

    .line 5025
    const/4 v3, 0x1

    .line 5027
    goto/16 :goto_2

    .line 5029
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_11
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-eq v15, v0, :cond_5

    .line 5030
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->bgColor:I

    .line 5031
    add-int/lit8 v2, v2, 0x1

    .line 5034
    goto/16 :goto_2

    .line 5035
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_12
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-gt v15, v0, :cond_13

    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-lt v15, v0, :cond_13

    .line 5036
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->bgColor:I

    .line 5037
    const/4 v3, 0x1

    .line 5039
    goto/16 :goto_2

    .line 5041
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_13
    if-nez v2, :cond_14

    .line 5042
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->bgColor:I

    .line 5043
    add-int/lit8 v2, v2, 0x1

    .line 5044
    goto/16 :goto_2

    .line 5045
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_14
    iget v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->bgColor:I

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-eq v15, v0, :cond_5

    .line 5046
    const/4 v15, 0x0

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->bgColor:I

    goto/16 :goto_2

    .line 5050
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_15
    instance-of v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    if-eqz v15, :cond_1a

    if-nez v7, :cond_1a

    .line 5051
    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_17

    .line 5052
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iget v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 5053
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget-object v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    iput-object v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 5054
    const/4 v7, 0x1

    .line 5056
    goto/16 :goto_2

    .line 5058
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_16
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-eq v15, v0, :cond_5

    .line 5059
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget-object v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    iput-object v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 5060
    add-int/lit8 v6, v6, 0x1

    .line 5063
    goto/16 :goto_2

    .line 5064
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_17
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-gt v15, v0, :cond_18

    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-lt v15, v0, :cond_18

    .line 5065
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget-object v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    iput-object v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 5066
    const/4 v7, 0x1

    .line 5068
    goto/16 :goto_2

    .line 5070
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_18
    if-nez v6, :cond_19

    .line 5071
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget-object v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    iput-object v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 5072
    add-int/lit8 v6, v6, 0x1

    .line 5073
    goto/16 :goto_2

    .line 5074
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_19
    iget-object v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget-object v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v15

    if-eqz v15, :cond_5

    .line 5075
    const-string v15, ""

    iput-object v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    goto/16 :goto_2

    .line 5079
    .restart local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_1a
    instance-of v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;

    if-eqz v15, :cond_1c

    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-ne v15, v0, :cond_1b

    if-nez p2, :cond_1c

    :cond_1b
    move-object v15, v9

    .line 5080
    check-cast v15, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;

    iget-boolean v15, v15, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;->isBold:Z

    if-eqz v15, :cond_5

    .line 5081
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-lt v0, v15, :cond_5

    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-gt v0, v15, :cond_5

    .line 5082
    iget v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    or-int/lit8 v15, v15, 0x1

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    goto/16 :goto_2

    .line 5085
    :cond_1c
    instance-of v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    if-eqz v15, :cond_1e

    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-ne v15, v0, :cond_1d

    if-nez p2, :cond_1e

    :cond_1d
    move-object v15, v9

    .line 5086
    check-cast v15, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    iget-boolean v15, v15, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;->isItalic:Z

    if-eqz v15, :cond_5

    .line 5087
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-lt v0, v15, :cond_5

    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-gt v0, v15, :cond_5

    .line 5088
    iget v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    or-int/lit8 v15, v15, 0x2

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    goto/16 :goto_2

    .line 5091
    :cond_1e
    instance-of v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;

    if-eqz v15, :cond_5

    .line 5092
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-ne v15, v0, :cond_1f

    if-nez p2, :cond_5

    :cond_1f
    move-object v15, v9

    .line 5093
    check-cast v15, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;

    iget-boolean v15, v15, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;->isUnderline:Z

    if-eqz v15, :cond_5

    .line 5094
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-lt v0, v15, :cond_5

    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-gt v0, v15, :cond_5

    .line 5095
    iget v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    or-int/lit8 v15, v15, 0x4

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    goto/16 :goto_2

    .line 5107
    .end local v8    # "i":I
    .local v9, "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    .restart local v10    # "pInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    :cond_20
    instance-of v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    if-eqz v15, :cond_3

    move-object v15, v9

    .line 5108
    check-cast v15, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v15, v15, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacingType:I

    .line 5109
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    iget v15, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    iput v15, v14, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    goto/16 :goto_1
.end method

.method private getTextLength()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3069
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v2, :cond_1

    .line 3078
    :cond_0
    :goto_0
    return v1

    .line 3073
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v0

    .line 3074
    .local v0, "str":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 3078
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_0
.end method

.method private hideCursorHandle()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 5848
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v0, v0, v2

    if-nez v0, :cond_1

    .line 5855
    :cond_0
    :goto_0
    return-void

    .line 5852
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5853
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleVisible:Z

    .line 5854
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v0, v0, v2

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private hideDeleteTextPopup()V
    .locals 2

    .prologue
    .line 7011
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 7012
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 7014
    :cond_0
    return-void
.end method

.method private initCue()V
    .locals 11
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v9, -0x2

    const/4 v8, 0x2

    .line 2803
    new-array v5, v8, [Landroid/graphics/Bitmap;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    .line 2804
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v8, :cond_1

    .line 2818
    new-array v5, v8, [Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    .line 2820
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 2821
    .local v3, "parentLayout":Landroid/view/ViewGroup;
    const/4 v0, 0x0

    .line 2822
    .local v0, "bitmapIndex":I
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v8, :cond_2

    .line 2844
    .end local v0    # "bitmapIndex":I
    .end local v3    # "parentLayout":Landroid/view/ViewGroup;
    :cond_0
    return-void

    .line 2805
    :cond_1
    sget-object v5, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_CUE_FILE_NAME:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDrawableImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2806
    .local v4, "temp":Landroid/graphics/Bitmap;
    if-eqz v4, :cond_0

    .line 2809
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    invoke-static {v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v6

    aput-object v6, v5, v1

    .line 2810
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v6, v6, v1

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v6, v7, v10}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    aput-object v6, v5, v1

    .line 2811
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 2812
    const/4 v4, 0x0

    .line 2813
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 2804
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2823
    .end local v4    # "temp":Landroid/graphics/Bitmap;
    .restart local v0    # "bitmapIndex":I
    .restart local v3    # "parentLayout":Landroid/view/ViewGroup;
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    new-instance v6, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    aput-object v6, v5, v1

    .line 2824
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v1

    invoke-virtual {v5, v10}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    .line 2826
    packed-switch v1, :pswitch_data_0

    .line 2834
    :goto_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v0

    if-eqz v5, :cond_3

    .line 2835
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v1

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v6, v6, v0

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2836
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2839
    .local v2, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v1

    invoke-virtual {v3, v5, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2842
    .end local v2    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v1

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2822
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2828
    :pswitch_0
    const/4 v0, 0x0

    .line 2829
    goto :goto_2

    .line 2831
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_2

    .line 2826
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private initDragText()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2899
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 2900
    .local v1, "parentLayout":Landroid/view/ViewGroup;
    new-instance v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    .line 2901
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 2902
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    .line 2903
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    .line 2902
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2904
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2905
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 2906
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setPivotX(F)V

    .line 2907
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setPivotY(F)V

    .line 2908
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v2, v4, v4, v4, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2909
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 2910
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2911
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2912
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2913
    return-void
.end method

.method private initEditable()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2972
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v5, :cond_0

    .line 3033
    :goto_0
    return-void

    .line 2976
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v3

    .line 2980
    .local v3, "strBefore":Ljava/lang/String;
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditableClear:Z

    .line 2982
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v5}, Landroid/text/method/TextKeyListener;->clear(Landroid/text/Editable;)V

    .line 2983
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_7

    .line 2984
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0xa

    if-eq v5, v6, :cond_6

    .line 2985
    sget-object v5, Landroid/text/method/TextKeyListener$Capitalize;->NONE:Landroid/text/method/TextKeyListener$Capitalize;

    invoke-static {v7, v5}, Landroid/text/method/TextKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    .line 2993
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setChangeWatcher()V

    .line 2995
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    .line 2996
    .local v1, "str":Ljava/lang/String;
    const/4 v2, 0x0

    .line 2997
    .local v2, "strAfter":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 2998
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v0

    .line 2999
    .local v0, "pos":I
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-le v0, v5, :cond_1

    .line 3000
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    .line 3002
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    .line 3005
    :cond_1
    invoke-virtual {v1, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 3006
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 3009
    .end local v0    # "pos":I
    :cond_2
    const-string v4, ""

    .line 3010
    .local v4, "strContent":Ljava/lang/String;
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_3

    .line 3011
    move-object v4, v3

    .line 3013
    :cond_3
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_4

    .line 3014
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 3020
    :cond_4
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_8

    .line 3021
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v6

    invoke-interface {v5, v7, v6, v4}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 3022
    if-eqz v3, :cond_5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-lt v5, v6, :cond_5

    .line 3023
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    invoke-static {v5, v6}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 3030
    :cond_5
    :goto_2
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I

    .line 3032
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditableClear:Z

    goto/16 :goto_0

    .line 2987
    .end local v1    # "str":Ljava/lang/String;
    .end local v2    # "strAfter":Ljava/lang/String;
    .end local v4    # "strContent":Ljava/lang/String;
    :cond_6
    sget-object v5, Landroid/text/method/TextKeyListener$Capitalize;->SENTENCES:Landroid/text/method/TextKeyListener$Capitalize;

    invoke-static {v7, v5}, Landroid/text/method/TextKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    goto/16 :goto_1

    .line 2990
    :cond_7
    sget-object v5, Landroid/text/method/TextKeyListener$Capitalize;->SENTENCES:Landroid/text/method/TextKeyListener$Capitalize;

    invoke-static {v7, v5}, Landroid/text/method/TextKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    goto/16 :goto_1

    .line 3026
    .restart local v1    # "str":Ljava/lang/String;
    .restart local v2    # "strAfter":Ljava/lang/String;
    .restart local v4    # "strContent":Ljava/lang/String;
    :cond_8
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v5}, Landroid/text/Editable;->clear()V

    .line 3027
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v5, v7}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    goto :goto_2
.end method

.method private initHandle()V
    .locals 12

    .prologue
    const/4 v11, 0x6

    const/4 v10, 0x0

    const/4 v9, -0x2

    const/4 v8, 0x3

    .line 2847
    new-array v5, v11, [Landroid/graphics/Bitmap;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    .line 2848
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v11, :cond_1

    .line 2862
    new-array v5, v8, [Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    .line 2863
    new-array v5, v8, [Landroid/graphics/RectF;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    .line 2864
    new-array v5, v8, [Z

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    .line 2866
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 2867
    .local v3, "parentLayout":Landroid/view/ViewGroup;
    const/4 v0, 0x0

    .line 2868
    .local v0, "bitmapIndex":I
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v8, :cond_2

    .line 2896
    .end local v0    # "bitmapIndex":I
    .end local v3    # "parentLayout":Landroid/view/ViewGroup;
    :cond_0
    return-void

    .line 2849
    :cond_1
    sget-object v5, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDrawableImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2850
    .local v4, "temp":Landroid/graphics/Bitmap;
    if-eqz v4, :cond_0

    .line 2853
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    invoke-static {v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v6

    aput-object v6, v5, v1

    .line 2854
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aget-object v6, v6, v1

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v6, v7, v10}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    aput-object v6, v5, v1

    .line 2855
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 2856
    const/4 v4, 0x0

    .line 2857
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 2848
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2869
    .end local v4    # "temp":Landroid/graphics/Bitmap;
    .restart local v0    # "bitmapIndex":I
    .restart local v3    # "parentLayout":Landroid/view/ViewGroup;
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    aput-object v6, v5, v1

    .line 2871
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    new-instance v6, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    aput-object v6, v5, v1

    .line 2872
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v1

    invoke-virtual {v5, v10}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    .line 2874
    packed-switch v1, :pswitch_data_0

    .line 2885
    :goto_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v0

    if-eqz v5, :cond_3

    .line 2886
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v1

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aget-object v6, v6, v0

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2887
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2890
    .local v2, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v1

    invoke-virtual {v3, v5, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2893
    .end local v2    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v1

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleListener:[Landroid/view/View$OnTouchListener;

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2894
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v1

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2868
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 2876
    :pswitch_0
    const/4 v0, 0x0

    .line 2877
    goto :goto_2

    .line 2879
    :pswitch_1
    const/4 v0, 0x5

    .line 2880
    goto :goto_2

    .line 2882
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_2

    .line 2874
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private initMeasureInfo()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3036
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    .line 3037
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    .line 3039
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    .line 3040
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    .line 3041
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    .line 3042
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextSize:[F

    .line 3043
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextItalic:[Z

    .line 3044
    return-void
.end method

.method private initTextBox(Landroid/view/ViewGroup;)V
    .locals 6
    .param p1, "layout"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v2, -0x2

    .line 2772
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .line 2773
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    .line 2774
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    .line 2776
    if-eqz p1, :cond_0

    .line 2777
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2783
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p1, p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2786
    .end local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    new-instance v1, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;

    invoke-direct {v3, p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;)V

    invoke-direct {v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mGestureDetector:Landroid/view/GestureDetector;

    .line 2787
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mGestureDetector:Landroid/view/GestureDetector;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;

    invoke-direct {v2, p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;)V

    invoke-virtual {v1, v2}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 2788
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mGestureDetector:Landroid/view/GestureDetector;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 2790
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusableInTouchMode(Z)V

    .line 2791
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setVisibility(I)V

    .line 2792
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusable(Z)V

    .line 2796
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initHandle()V

    .line 2798
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initDragText()V

    .line 2799
    return-void
.end method

.method private isCursorVisible()Z
    .locals 1

    .prologue
    .line 5677
    const/4 v0, 0x1

    return v0
.end method

.method private isFrontPosition(I)Z
    .locals 4
    .param p1, "pos"    # I

    .prologue
    const/4 v2, 0x0

    .line 7024
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-eqz v3, :cond_0

    .line 7025
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-lt v0, v3, :cond_1

    .line 7036
    .end local v0    # "index":I
    :cond_0
    :goto_1
    return v2

    .line 7026
    .restart local v0    # "index":I
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v3, v3, v0

    add-int/lit8 v3, v3, 0x1

    if-ne v3, p1, :cond_2

    .line 7027
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 7028
    .local v1, "oneChar":C
    const/16 v3, 0xa

    if-eq v1, v3, :cond_0

    const/16 v3, 0xd

    if-eq v1, v3, :cond_0

    .line 7031
    const/4 v2, 0x1

    goto :goto_1

    .line 7025
    .end local v1    # "oneChar":C
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private isHapticEnabled()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 7325
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "spen_feedback_haptic"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 7326
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 7327
    const-string/jumbo v3, "spen_feedback_haptic_pen_gesture"

    .line 7326
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 7330
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private isHighSurrogateByte(I)Z
    .locals 2
    .param p1, "c"    # I

    .prologue
    .line 2768
    const v0, 0xfc00

    and-int/2addr v0, p1

    const v1, 0xd800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isItalicAttribute(I)Z
    .locals 6
    .param p1, "pos"    # I

    .prologue
    const/4 v4, 0x0

    .line 5367
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_0

    move v3, v4

    .line 5388
    :goto_0
    return v3

    .line 5371
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3, p1, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v2

    .line 5372
    .local v2, "spans":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    if-eqz v2, :cond_1

    .line 5373
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .local v0, "cnt":I
    :goto_1
    if-gez v0, :cond_2

    .end local v0    # "cnt":I
    :cond_1
    move v3, v4

    .line 5388
    goto :goto_0

    .line 5374
    .restart local v0    # "cnt":I
    :cond_2
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 5375
    .local v1, "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    instance-of v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    if-eqz v3, :cond_5

    .line 5376
    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ne v3, p1, :cond_3

    if-eqz p1, :cond_3

    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iget v5, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-ne v3, v5, :cond_5

    :cond_3
    move-object v3, v1

    .line 5377
    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    iget-boolean v3, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;->isItalic:Z

    if-eqz v3, :cond_4

    .line 5378
    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-lt p1, v3, :cond_5

    iget v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-gt p1, v3, :cond_5

    .line 5379
    const/4 v3, 0x1

    goto :goto_0

    :cond_4
    move v3, v4

    .line 5382
    goto :goto_0

    .line 5373
    :cond_5
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method private isL2R(Ljava/lang/String;I)Z
    .locals 5
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    const/4 v3, 0x1

    .line 5393
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 5427
    :cond_0
    :goto_0
    :pswitch_0
    return v3

    .line 5397
    :cond_1
    invoke-direct {p0, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineFromIndex(I)I

    move-result v2

    .line 5398
    .local v2, "line":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-eqz v4, :cond_0

    if-ltz v2, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    array-length v4, v4

    if-ge v2, v4, :cond_0

    .line 5403
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v4, v4, v2

    if-le p2, v4, :cond_3

    .line 5427
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineL2R:[Z

    aget-boolean v3, v3, v2

    goto :goto_0

    .line 5404
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge p2, v4, :cond_2

    .line 5407
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 5408
    .local v0, "c":C
    invoke-static {v0}, Ljava/lang/Character;->getDirectionality(C)B

    move-result v1

    .line 5409
    .local v1, "dir":I
    packed-switch v1, :pswitch_data_0

    .line 5422
    :pswitch_1
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    .line 5420
    :pswitch_2
    const/4 v3, 0x0

    goto :goto_0

    .line 5409
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private makeDeletePopup()Landroid/widget/TextView;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 6928
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 6929
    .local v3, "res":Landroid/content/res/Resources;
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 6930
    .local v2, "dm":Landroid/util/DisplayMetrics;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 6931
    const/high16 v5, 0x42aa0000    # 85.0f

    invoke-static {v7, v5, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v5

    float-to-int v5, v5

    .line 6932
    const/high16 v6, 0x420c0000    # 35.0f

    .line 6931
    invoke-static {v7, v6, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v6

    float-to-int v6, v6

    .line 6930
    invoke-direct {v0, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 6933
    .local v0, "deleteLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v1, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    invoke-direct {v1, v5}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 6934
    .local v1, "deletePopup":Landroid/widget/TextView;
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6935
    const/16 v5, 0x11

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setGravity(I)V

    .line 6936
    const v5, -0x1a1a1b

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 6937
    const/high16 v5, 0x41700000    # 15.0f

    invoke-virtual {v1, v7, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 6938
    const/4 v5, 0x0

    invoke-virtual {v1, v5, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 6939
    const-string/jumbo v5, "string_delete_preset"

    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 6940
    .local v4, "stringDeleteText":Ljava/lang/String;
    if-nez v4, :cond_0

    .line 6941
    const-string v4, "Delete"

    .line 6943
    :cond_0
    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 6944
    const/high16 v5, -0x1000000

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 6946
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v5, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 6947
    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 6949
    new-instance v5, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$10;

    invoke-direct {v5, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$10;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 6961
    return-object v1
.end method

.method private static native native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_construct(ILandroid/content/Context;)Z
.end method

.method private static native native_construct(JLandroid/content/Context;)Z
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_finalize(J)V
.end method

.method private static native native_getHeight(I)I
.end method

.method private static native native_getHeight(J)I
.end method

.method private static native native_getHintTextWidth(I)F
.end method

.method private static native native_getHintTextWidth(J)F
.end method

.method private static native native_getLineCount(I)I
.end method

.method private static native native_getLineCount(J)I
.end method

.method private static native native_getLineEndIndex(II)I
.end method

.method private static native native_getLineEndIndex(JI)I
.end method

.method private static native native_getLinePosition(IILandroid/graphics/PointF;)Z
.end method

.method private static native native_getLinePosition(JILandroid/graphics/PointF;)Z
.end method

.method private static native native_getLineStartIndex(II)I
.end method

.method private static native native_getLineStartIndex(JI)I
.end method

.method private static native native_getPan(I)F
.end method

.method private static native native_getPan(J)F
.end method

.method private static native native_getTextRect(IILandroid/graphics/RectF;)Z
.end method

.method private static native native_getTextRect(JILandroid/graphics/RectF;)Z
.end method

.method private static native native_init()I
.end method

.method private static native native_init_64()J
.end method

.method private static native native_measure(II)Z
.end method

.method private static native native_measure(JI)Z
.end method

.method private static native native_setBitmap(ILandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setBitmap(JLandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setObjectText(ILcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z
.end method

.method private static native native_setObjectText(JLcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z
.end method

.method private static native native_setPan(IF)V
.end method

.method private static native native_setPan(JF)V
.end method

.method private static native native_update(I)Z
.end method

.method private static native native_update(J)Z
.end method

.method private onCueBottomButtonDown(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 6027
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v0

    .line 6028
    .local v0, "deltaY":F
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v5

    int-to-float v2, v5

    .line 6029
    .local v2, "minHeight":F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v5, :cond_0

    .line 6030
    const/4 v5, 0x0

    .line 6053
    :goto_0
    return v5

    .line 6033
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    .line 6035
    .local v3, "objectRect":Landroid/graphics/RectF;
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v5

    cmpl-float v5, v2, v5

    if-lez v5, :cond_1

    .line 6036
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 6038
    .local v1, "endTextRect":Landroid/graphics/RectF;
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    .line 6039
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v10, v11}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineCount(J)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-direct {p0, v8, v9, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineEndIndex(JI)I

    move-result v5

    .line 6038
    invoke-direct {p0, v6, v7, v5, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getTextRect(JILandroid/graphics/RectF;)Z

    .line 6041
    const/4 v5, 0x2

    new-array v4, v5, [F

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    aput v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    aput v6, v4, v5

    .line 6042
    .local v4, "point":[F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 6043
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v5, v4}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 6045
    iget v5, v1, Landroid/graphics/RectF;->top:F

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v6

    add-float/2addr v6, v0

    cmpl-float v5, v5, v6

    if-lez v5, :cond_1

    .line 6046
    const/4 v5, 0x0

    aget v5, v4, v5

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;->baseRate:I

    mul-int/lit8 v7, v7, 0x32

    int-to-float v7, v7

    const/high16 v8, 0x44a00000    # 1280.0f

    div-float/2addr v7, v8

    sub-float/2addr v6, v7

    cmpl-float v5, v5, v6

    if-lez v5, :cond_1

    .line 6047
    const/4 v5, 0x1

    aget v5, v4, v5

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;->baseRate:I

    mul-int/lit8 v7, v7, 0x32

    int-to-float v7, v7

    const/high16 v8, 0x44a00000    # 1280.0f

    div-float/2addr v7, v8

    sub-float/2addr v6, v7

    cmpl-float v5, v5, v6

    if-lez v5, :cond_1

    .line 6048
    const/4 v5, 0x1

    goto :goto_0

    .line 6053
    .end local v1    # "endTextRect":Landroid/graphics/RectF;
    .end local v4    # "point":[F
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private onCueBottonDown(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/16 v10, 0x8

    const/4 v0, 0x0

    const/4 v9, 0x1

    .line 6058
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v1, :cond_1

    .line 6101
    :cond_0
    :goto_0
    return v0

    .line 6061
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v8

    .line 6062
    .local v8, "objectRect":Landroid/graphics/RectF;
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    .line 6064
    .local v6, "relativeRect":Landroid/graphics/RectF;
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v7

    .line 6065
    .local v7, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v7, :cond_0

    .line 6070
    invoke-direct {p0, v6, v8, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 6072
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onCueTopButtonDown(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 6073
    iget v1, v6, Landroid/graphics/RectF;->right:F

    float-to-int v1, v1

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v10, v10, v0

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    sub-int v2, v1, v10

    .line 6074
    .local v2, "x":I
    iget v1, v6, Landroid/graphics/RectF;->top:F

    float-to-int v3, v1

    .line 6076
    .local v3, "y":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 6077
    .local v4, "width":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 6079
    .local v5, "height":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v1, v1, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setHandlePos(Landroid/view/View;IIIILandroid/graphics/RectF;)V

    move v0, v9

    .line 6081
    goto :goto_0

    .line 6084
    .end local v2    # "x":I
    .end local v3    # "y":I
    .end local v4    # "width":I
    .end local v5    # "height":I
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onCueBottomButtonDown(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 6085
    iget v0, v6, Landroid/graphics/RectF;->right:F

    float-to-int v0, v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v9

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v2, v0, 0x2

    .line 6086
    .restart local v2    # "x":I
    iget v0, v6, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v9

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v3, v0, 0x4

    .line 6088
    .restart local v3    # "y":I
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v9

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 6089
    .restart local v4    # "width":I
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v9

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 6091
    .restart local v5    # "height":I
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v1, v0, v9

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setHandlePos(Landroid/view/View;IIIILandroid/graphics/RectF;)V

    move v0, v9

    .line 6093
    goto :goto_0

    .line 6096
    .end local v2    # "x":I
    .end local v3    # "y":I
    .end local v4    # "width":I
    .end local v5    # "height":I
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v9, :cond_4

    .line 6097
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v0, v1, v0

    invoke-virtual {v0, v10}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 6098
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v0, v0, v9

    invoke-virtual {v0, v10}, Landroid/widget/ImageButton;->setVisibility(I)V

    :cond_4
    move v0, v9

    .line 6101
    goto/16 :goto_0
.end method

.method private onCueTopButtonDown(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v10, 0x44a00000    # 1280.0f

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 5998
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v0

    .line 5999
    .local v0, "deltaY":F
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v7

    int-to-float v2, v7

    .line 6000
    .local v2, "minHeight":F
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v7, :cond_1

    .line 6023
    :cond_0
    :goto_0
    return v5

    .line 6004
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    .line 6006
    .local v3, "objectRect":Landroid/graphics/RectF;
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v7

    cmpl-float v7, v2, v7

    if-lez v7, :cond_0

    .line 6007
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 6009
    .local v1, "firstTextRect":Landroid/graphics/RectF;
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9, v5, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getTextRect(JILandroid/graphics/RectF;)Z

    .line 6011
    const/4 v7, 0x2

    new-array v4, v7, [F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    aput v7, v4, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    aput v7, v4, v6

    .line 6012
    .local v4, "point":[F
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v7, v8}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 6013
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v7, v4}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 6015
    iget v7, v1, Landroid/graphics/RectF;->bottom:F

    cmpg-float v7, v7, v0

    if-gez v7, :cond_0

    .line 6016
    aget v7, v4, v5

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;->baseRate:I

    mul-int/lit8 v9, v9, 0x32

    int-to-float v9, v9

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    cmpl-float v7, v7, v8

    if-lez v7, :cond_0

    .line 6017
    aget v7, v4, v6

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;->baseRate:I

    mul-int/lit8 v8, v8, 0x32

    int-to-float v8, v8

    div-float/2addr v8, v10

    cmpg-float v7, v7, v8

    if-gez v7, :cond_0

    move v5, v6

    .line 6018
    goto :goto_0
.end method

.method private onDrawCursor(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 3715
    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v7

    if-eqz v7, :cond_0

    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    if-eqz v7, :cond_1

    .line 3775
    :cond_0
    :goto_0
    return-void

    .line 3718
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v7, :cond_0

    .line 3722
    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighlightPathBogus:Z

    if-eqz v7, :cond_0

    .line 3723
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v7}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v5

    .line 3724
    .local v5, "start":I
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v7}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v3

    .line 3726
    .local v3, "end":I
    if-ne v5, v3, :cond_0

    .line 3730
    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v1

    .line 3731
    .local v1, "cursorRect":Landroid/graphics/Rect;
    if-eqz v1, :cond_0

    .line 3736
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v4

    .line 3737
    .local v4, "objectRect":Landroid/graphics/RectF;
    if-eqz v4, :cond_0

    .line 3742
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v2

    .line 3743
    .local v2, "deltaY":F
    iget v7, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v7

    cmpg-float v7, v7, v2

    if-ltz v7, :cond_0

    iget v7, v1, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v8

    add-float/2addr v8, v2

    cmpl-float v7, v7, v8

    if-gtz v7, :cond_0

    .line 3747
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7, v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    .line 3748
    invoke-direct {p0, v1, v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->adjustCursorSize(Landroid/graphics/Rect;II)V

    .line 3750
    iget v7, v1, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    sub-float/2addr v7, v2

    float-to-int v6, v7

    .line 3751
    .local v6, "top":I
    iget v7, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v7

    sub-float/2addr v7, v2

    float-to-int v0, v7

    .line 3753
    .local v0, "bottom":I
    if-gez v6, :cond_2

    const/4 v6, 0x0

    .end local v6    # "top":I
    :cond_2
    iput v6, v1, Landroid/graphics/Rect;->top:I

    .line 3754
    int-to-float v7, v0

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v8

    cmpl-float v7, v7, v8

    if-lez v7, :cond_4

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v7

    :goto_1
    float-to-int v7, v7

    iput v7, v1, Landroid/graphics/Rect;->bottom:I

    .line 3756
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getEllipsisType()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_3

    .line 3757
    iget v7, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v7

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v8

    cmpl-float v7, v7, v8

    if-eqz v7, :cond_0

    .line 3761
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v7, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 3762
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v7, v8}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 3763
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v7, v1}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 3765
    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isItalicAttribute(I)Z

    move-result v7

    if-nez v7, :cond_5

    .line 3766
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighLightPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v7}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 3754
    :cond_4
    int-to-float v7, v0

    goto :goto_1

    .line 3768
    :cond_5
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 3769
    sget v7, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->SIN_15_DEGREE:F

    float-to-double v8, v7

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v7

    int-to-double v10, v7

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    div-double/2addr v10, v12

    mul-double/2addr v8, v10

    double-to-float v7, v8

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3770
    const/high16 v7, 0x41700000    # 15.0f

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {p1, v7, v8, v9}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 3771
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighLightPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v7}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 3772
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0
.end method

.method private onDrawHandle()V
    .locals 24

    .prologue
    .line 3858
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    if-eqz v2, :cond_1

    .line 4127
    :cond_0
    :goto_0
    return-void

    .line 3862
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v2, :cond_0

    .line 3866
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v16

    .line 3867
    .local v16, "start":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v12

    .line 3868
    .local v12, "end":I
    move/from16 v0, v16

    if-ne v0, v12, :cond_3

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleVisible:Z

    if-nez v2, :cond_3

    .line 3869
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/widget/ImageButton;->isShown()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3870
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3872
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/widget/ImageButton;->isShown()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3873
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 3878
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v14

    .line 3879
    .local v14, "objectRect":Landroid/graphics/RectF;
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8}, Landroid/graphics/RectF;-><init>()V

    .line 3881
    .local v8, "relativeRect":Landroid/graphics/RectF;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v9

    .line 3882
    .local v9, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v9, :cond_0

    .line 3887
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v14, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3893
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v10

    .line 3894
    .local v10, "cursorRect":Landroid/graphics/Rect;
    if-eqz v10, :cond_0

    .line 3899
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v11

    .line 3901
    .local v11, "deltaY":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getEllipsisType()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_4

    .line 3902
    iget v2, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    sub-float/2addr v2, v11

    invoke-virtual {v14}, Landroid/graphics/RectF;->height()F

    move-result v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_4

    .line 3903
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3904
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3905
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 3912
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x1

    aget-object v3, v2, v4

    .line 3914
    .local v3, "handle":Landroid/widget/ImageButton;
    move/from16 v0, v16

    if-ne v0, v12, :cond_d

    .line 3915
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    if-nez v2, :cond_5

    iget v2, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    cmpg-float v2, v2, v11

    if-ltz v2, :cond_5

    iget v2, v10, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {v14}, Landroid/graphics/RectF;->height()F

    move-result v4

    add-float/2addr v4, v11

    cmpl-float v2, v2, v4

    if-lez v2, :cond_6

    .line 3916
    :cond_5
    const/16 v2, 0x8

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 3921
    :cond_6
    const/4 v2, 0x0

    neg-float v4, v11

    float-to-int v4, v4

    invoke-virtual {v10, v2, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 3923
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v10}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 3924
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 3925
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v10}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 3927
    iget v2, v8, Landroid/graphics/RectF;->left:F

    invoke-virtual {v10}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    int-to-float v4, v4

    add-float v18, v2, v4

    .line 3928
    .local v18, "x":F
    iget v2, v8, Landroid/graphics/RectF;->top:F

    iget v4, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    add-float v19, v2, v4

    .line 3930
    .local v19, "y":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x5

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 3931
    .local v6, "width":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x5

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 3933
    .local v7, "height":I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleVisible:Z

    if-eqz v2, :cond_a

    .line 3934
    int-to-float v2, v7

    add-float v2, v2, v19

    iget-object v4, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    cmpl-float v2, v2, v4

    if-lez v2, :cond_b

    .line 3935
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getHeight()I

    move-result v4

    add-int/2addr v2, v4

    int-to-float v2, v2

    sub-float v19, v19, v2

    .line 3936
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x4

    aget-object v2, v2, v4

    if-eqz v2, :cond_7

    .line 3937
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x4

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 3938
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x1

    const/4 v5, 0x1

    aput-boolean v5, v2, v4

    .line 3946
    :cond_7
    :goto_1
    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    cmpg-float v2, v19, v2

    if-gez v2, :cond_c

    .line 3947
    :goto_2
    move/from16 v0, v18

    float-to-double v4, v0

    int-to-double v0, v6

    move-wide/from16 v20, v0

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    div-double v20, v20, v22

    sub-double v4, v4, v20

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v4, v4

    move/from16 v0, v19

    float-to-int v5, v0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setHandlePos(Landroid/view/View;IIIILandroid/graphics/RectF;)V

    .line 3949
    move/from16 v0, v18

    float-to-double v4, v0

    int-to-double v0, v6

    move-wide/from16 v20, v0

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    div-double v20, v20, v22

    sub-double v4, v4, v20

    double-to-float v13, v4

    .line 3950
    .local v13, "left":F
    move/from16 v17, v19

    .line 3952
    .local v17, "top":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 3953
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v13

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x33d6bf95    # 1.0E-7f

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_8

    .line 3954
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    iget v2, v2, Landroid/graphics/RectF;->top:F

    sub-float v2, v2, v17

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x33d6bf95    # 1.0E-7f

    cmpl-float v2, v2, v4

    if-lez v2, :cond_9

    .line 3955
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    int-to-float v4, v6

    add-float/2addr v4, v13

    int-to-float v5, v7

    add-float v5, v5, v17

    move/from16 v0, v17

    invoke-virtual {v2, v13, v0, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 3956
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setDirty()V

    .line 3957
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    .line 3958
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextmenu()V

    .line 3962
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    int-to-float v4, v6

    add-float/2addr v4, v13

    int-to-float v5, v7

    add-float v5, v5, v17

    move/from16 v0, v17

    invoke-virtual {v2, v13, v0, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 3964
    .end local v13    # "left":F
    .end local v17    # "top":F
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3965
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 3941
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x5

    aget-object v2, v2, v4

    if-eqz v2, :cond_7

    .line 3942
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x5

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 3943
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x1

    const/4 v5, 0x0

    aput-boolean v5, v2, v4

    goto/16 :goto_1

    .line 3946
    :cond_c
    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v4, 0x40800000    # 4.0f

    sub-float v19, v2, v4

    goto/16 :goto_2

    .line 3970
    .end local v6    # "width":I
    .end local v7    # "height":I
    .end local v18    # "x":F
    .end local v19    # "y":F
    :cond_d
    const/16 v2, 0x8

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3975
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v10

    .line 3976
    if-eqz v10, :cond_0

    .line 3981
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x0

    aget-object v3, v2, v4

    .line 3983
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    if-nez v2, :cond_e

    iget v2, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    cmpg-float v2, v2, v11

    if-ltz v2, :cond_e

    iget v2, v10, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {v14}, Landroid/graphics/RectF;->height()F

    move-result v4

    add-float/2addr v4, v11

    cmpl-float v2, v2, v4

    if-lez v2, :cond_10

    .line 3984
    :cond_e
    const/16 v2, 0x8

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 4054
    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v10

    .line 4055
    if-eqz v10, :cond_0

    .line 4060
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x2

    aget-object v3, v2, v4

    .line 4062
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    if-nez v2, :cond_f

    iget v2, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    cmpg-float v2, v2, v11

    if-ltz v2, :cond_f

    iget v2, v10, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {v14}, Landroid/graphics/RectF;->height()F

    move-result v4

    add-float/2addr v4, v11

    cmpl-float v2, v2, v4

    if-lez v2, :cond_18

    .line 4063
    :cond_f
    const/16 v2, 0x8

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 3987
    :cond_10
    const/4 v2, 0x0

    neg-float v4, v11

    float-to-int v4, v4

    invoke-virtual {v10, v2, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 3989
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v10}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 3990
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 3991
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v10}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 3993
    iget v2, v8, Landroid/graphics/RectF;->left:F

    iget v4, v10, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    add-float v18, v2, v4

    .line 3994
    .restart local v18    # "x":F
    iget v2, v8, Landroid/graphics/RectF;->top:F

    iget v4, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    add-float v19, v2, v4

    .line 3996
    .restart local v19    # "y":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 3997
    .restart local v6    # "width":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 3999
    .restart local v7    # "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v15

    check-cast v15, Landroid/view/ViewGroup;

    .line 4000
    .local v15, "parentLayout":Landroid/view/ViewGroup;
    if-eqz v15, :cond_0

    .line 4004
    invoke-virtual {v15}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    if-eqz v2, :cond_0

    .line 4008
    int-to-float v2, v6

    sub-float v2, v18, v2

    const/4 v4, 0x0

    cmpg-float v2, v2, v4

    if-gez v2, :cond_14

    int-to-float v2, v7

    add-float v2, v2, v19

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_14

    .line 4009
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v2, v7

    int-to-float v2, v2

    sub-float v19, v19, v2

    .line 4010
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    if-eqz v2, :cond_11

    .line 4011
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 4012
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x0

    const/4 v5, 0x1

    aput-boolean v5, v2, v4

    .line 4035
    :cond_11
    :goto_4
    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    cmpg-float v2, v19, v2

    if-gez v2, :cond_17

    .line 4036
    :goto_5
    move/from16 v0, v18

    float-to-int v4, v0

    move/from16 v0, v19

    float-to-int v5, v0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setHandlePos(Landroid/view/View;IIIILandroid/graphics/RectF;)V

    .line 4038
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 4039
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float v2, v2, v18

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x33d6bf95    # 1.0E-7f

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_12

    .line 4040
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    iget v2, v2, Landroid/graphics/RectF;->top:F

    sub-float v2, v2, v19

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x33d6bf95    # 1.0E-7f

    cmpl-float v2, v2, v4

    if-lez v2, :cond_13

    .line 4041
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    int-to-float v4, v6

    add-float v4, v4, v18

    int-to-float v5, v7

    add-float v5, v5, v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 4042
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setDirty()V

    .line 4043
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    .line 4044
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextmenu()V

    .line 4048
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    int-to-float v4, v6

    add-float v4, v4, v18

    int-to-float v5, v7

    add-float v5, v5, v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_3

    .line 4014
    :cond_14
    int-to-float v2, v7

    add-float v2, v2, v19

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_15

    .line 4015
    int-to-float v2, v6

    sub-float v18, v18, v2

    .line 4016
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v2, v7

    int-to-float v2, v2

    sub-float v19, v19, v2

    .line 4017
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    if-eqz v2, :cond_11

    .line 4018
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 4019
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x0

    const/4 v5, 0x1

    aput-boolean v5, v2, v4

    goto/16 :goto_4

    .line 4021
    :cond_15
    int-to-float v2, v6

    sub-float v2, v18, v2

    const/4 v4, 0x0

    cmpg-float v2, v2, v4

    if-gez v2, :cond_16

    .line 4023
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x3

    aget-object v2, v2, v4

    if-eqz v2, :cond_11

    .line 4024
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 4025
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput-boolean v5, v2, v4

    goto/16 :goto_4

    .line 4028
    :cond_16
    int-to-float v2, v6

    sub-float v18, v18, v2

    .line 4030
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    if-eqz v2, :cond_11

    .line 4031
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 4032
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput-boolean v5, v2, v4

    goto/16 :goto_4

    .line 4035
    :cond_17
    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v4, 0x40800000    # 4.0f

    sub-float v19, v2, v4

    goto/16 :goto_5

    .line 4066
    .end local v6    # "width":I
    .end local v7    # "height":I
    .end local v15    # "parentLayout":Landroid/view/ViewGroup;
    .end local v18    # "x":F
    .end local v19    # "y":F
    :cond_18
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v4

    neg-float v4, v4

    float-to-int v4, v4

    invoke-virtual {v10, v2, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 4068
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v10}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 4069
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 4070
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v10}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 4072
    iget v2, v8, Landroid/graphics/RectF;->left:F

    iget v4, v10, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    add-float v18, v2, v4

    .line 4073
    .restart local v18    # "x":F
    iget v2, v8, Landroid/graphics/RectF;->top:F

    iget v4, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    add-float v19, v2, v4

    .line 4075
    .restart local v19    # "y":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 4076
    .restart local v6    # "width":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 4078
    .restart local v7    # "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v15

    check-cast v15, Landroid/view/ViewGroup;

    .line 4079
    .restart local v15    # "parentLayout":Landroid/view/ViewGroup;
    if-eqz v15, :cond_0

    .line 4083
    invoke-virtual {v15}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    if-eqz v2, :cond_0

    .line 4087
    int-to-float v2, v6

    add-float v2, v2, v18

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_1c

    int-to-float v2, v7

    add-float v2, v2, v19

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_1c

    .line 4088
    int-to-float v2, v6

    sub-float v18, v18, v2

    .line 4089
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v2, v7

    int-to-float v2, v2

    sub-float v19, v19, v2

    .line 4090
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    if-eqz v2, :cond_19

    .line 4091
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 4092
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x2

    const/4 v5, 0x1

    aput-boolean v5, v2, v4

    .line 4112
    :cond_19
    :goto_6
    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    cmpg-float v2, v19, v2

    if-gez v2, :cond_1f

    .line 4113
    :goto_7
    move/from16 v0, v18

    float-to-int v4, v0

    move/from16 v0, v19

    float-to-int v5, v0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setHandlePos(Landroid/view/View;IIIILandroid/graphics/RectF;)V

    .line 4115
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 4116
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float v2, v2, v18

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x33d6bf95    # 1.0E-7f

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_1a

    .line 4117
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    iget v2, v2, Landroid/graphics/RectF;->top:F

    sub-float v2, v2, v19

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x33d6bf95    # 1.0E-7f

    cmpl-float v2, v2, v4

    if-lez v2, :cond_1b

    .line 4118
    :cond_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    int-to-float v4, v6

    add-float v4, v4, v18

    int-to-float v5, v7

    add-float v5, v5, v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 4119
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setDirty()V

    .line 4120
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    .line 4121
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextmenu()V

    .line 4125
    :cond_1b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    int-to-float v4, v6

    add-float v4, v4, v18

    int-to-float v5, v7

    add-float v5, v5, v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_0

    .line 4094
    :cond_1c
    int-to-float v2, v7

    add-float v2, v2, v19

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_1d

    .line 4095
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v2, v7

    int-to-float v2, v2

    sub-float v19, v19, v2

    .line 4096
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    if-eqz v2, :cond_19

    .line 4097
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 4098
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x2

    const/4 v5, 0x1

    aput-boolean v5, v2, v4

    goto/16 :goto_6

    .line 4100
    :cond_1d
    int-to-float v2, v6

    add-float v2, v2, v18

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_1e

    .line 4101
    int-to-float v2, v6

    sub-float v18, v18, v2

    .line 4102
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    if-eqz v2, :cond_19

    .line 4103
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 4104
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x2

    const/4 v5, 0x0

    aput-boolean v5, v2, v4

    goto/16 :goto_6

    .line 4107
    :cond_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x3

    aget-object v2, v2, v4

    if-eqz v2, :cond_19

    .line 4108
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 4109
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    const/4 v4, 0x2

    const/4 v5, 0x0

    aput-boolean v5, v2, v4

    goto/16 :goto_6

    .line 4112
    :cond_1f
    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v4, 0x40800000    # 4.0f

    sub-float v19, v2, v4

    goto/16 :goto_7
.end method

.method private onDrawScrollBar(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 3779
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarVisible:Z

    if-nez v1, :cond_1

    .line 3793
    :cond_0
    :goto_0
    return-void

    .line 3783
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getScrollBarRect()Landroid/graphics/Rect;

    move-result-object v0

    .line 3784
    .local v0, "scrollbarRect":Landroid/graphics/Rect;
    if-eqz v0, :cond_0

    .line 3788
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 3789
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 3790
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 3792
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private onDrawSelect(Landroid/graphics/Canvas;)V
    .locals 21
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 4198
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    if-eqz v2, :cond_1

    .line 4280
    :cond_0
    :goto_0
    return-void

    .line 4202
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v4

    .line 4203
    .local v4, "startIndex":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v5

    .line 4204
    .local v5, "endIndex":I
    if-eq v5, v4, :cond_0

    .line 4208
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v15

    .line 4209
    .local v15, "cursorRect":Landroid/graphics/Rect;
    if-eqz v15, :cond_0

    .line 4213
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v18

    .line 4214
    .local v18, "objectRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v16

    .line 4215
    .local v16, "deltaY":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getEllipsisType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 4216
    iget v2, v15, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    sub-float v2, v2, v16

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->height()F

    move-result v3

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_0

    .line 4220
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v2, :cond_3

    .line 4221
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 4224
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-eqz v2, :cond_0

    .line 4230
    if-le v4, v5, :cond_4

    .line 4231
    move/from16 v20, v4

    .line 4232
    .local v20, "temp":I
    move v4, v5

    .line 4233
    move/from16 v5, v20

    .line 4238
    .end local v20    # "temp":I
    :cond_4
    const/16 v17, -0x1

    .local v17, "endLine":I
    move/from16 v19, v17

    .line 4241
    .local v19, "startLine":I
    const/4 v14, 0x0

    .local v14, "cnt":I
    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-lt v14, v2, :cond_5

    .line 4261
    :goto_2
    const/4 v2, -0x1

    move/from16 v0, v19

    if-eq v0, v2, :cond_0

    const/4 v2, -0x1

    move/from16 v0, v17

    if-eq v0, v2, :cond_0

    .line 4265
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    .line 4266
    .local v6, "selectRect":Landroid/graphics/RectF;
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 4268
    .local v7, "drawRect":Landroid/graphics/Rect;
    move/from16 v0, v19

    move/from16 v1, v17

    if-ne v0, v1, :cond_8

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    .line 4269
    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->drawSelectRect(Landroid/graphics/Canvas;IILandroid/graphics/RectF;Landroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 4244
    .end local v6    # "selectRect":Landroid/graphics/RectF;
    .end local v7    # "drawRect":Landroid/graphics/Rect;
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v2, v2, v14

    if-gt v2, v4, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v2, v2, v14

    if-lt v2, v4, :cond_6

    .line 4245
    move/from16 v19, v14

    .line 4247
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v2, v2, v14

    add-int/lit8 v3, v5, -0x1

    if-gt v2, v3, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v2, v2, v14

    add-int/lit8 v3, v5, -0x1

    if-lt v2, v3, :cond_7

    .line 4248
    move/from16 v17, v14

    .line 4249
    goto :goto_2

    .line 4241
    :cond_7
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 4273
    .restart local v6    # "selectRect":Landroid/graphics/RectF;
    .restart local v7    # "drawRect":Landroid/graphics/Rect;
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v2, v2, v19

    add-int/lit8 v11, v2, 0x1

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move v10, v4

    move-object v12, v6

    move-object v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->drawSelectRect(Landroid/graphics/Canvas;IILandroid/graphics/RectF;Landroid/graphics/Rect;)V

    .line 4275
    add-int/lit8 v14, v19, 0x1

    :goto_3
    move/from16 v0, v17

    if-lt v14, v0, :cond_9

    .line 4279
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v10, v2, v17

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move v11, v5

    move-object v12, v6

    move-object v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->drawSelectRect(Landroid/graphics/Canvas;IILandroid/graphics/RectF;Landroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 4276
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v10, v2, v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v11, v2, v14

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object v12, v6

    move-object v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->drawSelectedLine(Landroid/graphics/Canvas;IILandroid/graphics/RectF;Landroid/graphics/Rect;)V

    .line 4275
    add-int/lit8 v14, v14, 0x1

    goto :goto_3
.end method

.method private onMoveByKey(ILandroid/view/KeyEvent;)V
    .locals 24
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2408
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v19

    .line 2409
    .local v19, "tempStart":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v18

    .line 2411
    .local v18, "tempEnd":I
    move/from16 v0, v19

    move/from16 v1, v18

    if-ge v0, v1, :cond_1

    move/from16 v14, v19

    .line 2412
    .local v14, "start":I
    :goto_0
    move/from16 v0, v19

    move/from16 v1, v18

    if-ge v0, v1, :cond_2

    move/from16 v6, v18

    .line 2414
    .local v6, "end":I
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorPos()I

    move-result v12

    .local v12, "pos":I
    const/4 v9, 0x0

    .line 2415
    .local v9, "line":I
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v4

    .line 2416
    .local v4, "cursorRect":Landroid/graphics/Rect;
    if-nez v4, :cond_3

    .line 2765
    :cond_0
    :goto_2
    return-void

    .end local v4    # "cursorRect":Landroid/graphics/Rect;
    .end local v6    # "end":I
    .end local v9    # "line":I
    .end local v12    # "pos":I
    .end local v14    # "start":I
    :cond_1
    move/from16 v14, v18

    .line 2411
    goto :goto_0

    .restart local v14    # "start":I
    :cond_2
    move/from16 v6, v19

    .line 2412
    goto :goto_1

    .line 2421
    .restart local v4    # "cursorRect":Landroid/graphics/Rect;
    .restart local v6    # "end":I
    .restart local v9    # "line":I
    .restart local v12    # "pos":I
    :cond_3
    invoke-virtual {v4}, Landroid/graphics/Rect;->centerY()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v9

    .line 2423
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    move-object/from16 v21, v0

    if-eqz v21, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    move-object/from16 v21, v0

    if-eqz v21, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v21, v0

    if-eqz v21, :cond_0

    .line 2428
    sparse-switch p1, :sswitch_data_0

    .line 2750
    :cond_4
    :goto_3
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSwipeOnKeyboard:Z

    .line 2751
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v21

    if-nez v21, :cond_5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v21, v0

    if-eqz v21, :cond_0

    .line 2752
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHardKeyboardConnected:Z

    move/from16 v21, v0

    if-nez v21, :cond_4f

    .line 2753
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getText(Z)Ljava/lang/String;

    move-result-object v21

    if-eqz v21, :cond_0

    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getText(Z)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    if-lez v21, :cond_0

    .line 2754
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSwipeOnKeyboard:Z

    .line 2755
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideCursorHandle()V

    .line 2756
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->reset()V

    .line 2757
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    goto/16 :goto_2

    .line 2431
    :sswitch_0
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 2432
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v21

    if-nez v21, :cond_6

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isAltPressed:Z

    move/from16 v21, v0

    if-eqz v21, :cond_7

    .line 2433
    :cond_6
    const/16 v21, 0x0

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto :goto_3

    .line 2434
    :cond_7
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v21

    if-nez v21, :cond_8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v21, v0

    if-eqz v21, :cond_c

    .line 2435
    :cond_8
    if-lez v9, :cond_4

    .line 2436
    add-int/lit8 v21, v9, -0x1

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerX()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v10

    .line 2437
    .local v10, "newCursorPos":I
    if-ne v14, v12, :cond_9

    .line 2438
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v10, v6, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2440
    :cond_9
    if-ge v10, v14, :cond_a

    .line 2441
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v10, v14, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2442
    :cond_a
    if-ne v10, v14, :cond_b

    .line 2443
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2445
    :cond_b
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v14, v10, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 2446
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2451
    .end local v10    # "newCursorPos":I
    :cond_c
    if-lez v9, :cond_4

    .line 2452
    add-int/lit8 v21, v9, -0x1

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerX()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v21

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2459
    :sswitch_1
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 2460
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v21

    if-nez v21, :cond_d

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isAltPressed:Z

    move/from16 v21, v0

    if-eqz v21, :cond_e

    .line 2461
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2462
    :cond_e
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v21

    if-nez v21, :cond_f

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v21, v0

    if-eqz v21, :cond_13

    .line 2463
    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, -0x1

    move/from16 v0, v21

    if-ge v9, v0, :cond_4

    .line 2464
    add-int/lit8 v21, v9, 0x1

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerX()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v10

    .line 2465
    .restart local v10    # "newCursorPos":I
    if-ne v6, v12, :cond_10

    .line 2466
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v14, v10, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 2467
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2469
    :cond_10
    if-ge v10, v6, :cond_11

    .line 2470
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v10, v6, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2471
    :cond_11
    if-ne v10, v6, :cond_12

    .line 2472
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2474
    :cond_12
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v6, v10, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 2475
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2480
    .end local v10    # "newCursorPos":I
    :cond_13
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, -0x1

    move/from16 v0, v21

    if-ge v9, v0, :cond_4

    .line 2481
    add-int/lit8 v21, v9, 0x1

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerX()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v21

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2488
    :sswitch_2
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 2489
    if-ltz v9, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v9, v0, :cond_0

    .line 2492
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v21

    if-nez v21, :cond_14

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isAltPressed:Z

    move/from16 v21, v0

    if-eqz v21, :cond_15

    .line 2493
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    move-object/from16 v22, v0

    aget v22, v22, v9

    aget-object v21, v21, v22

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v9, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v21

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2494
    :cond_15
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v21

    if-nez v21, :cond_16

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isCtrlPressed:Z

    move/from16 v21, v0

    if-eqz v21, :cond_22

    .line 2495
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v15

    .line 2496
    .local v15, "str":Ljava/lang/String;
    if-eqz v15, :cond_4

    .line 2497
    if-ne v14, v12, :cond_1b

    .line 2498
    const/4 v13, 0x0

    .line 2499
    .local v13, "processed":Z
    new-instance v20, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;

    const/16 v21, 0x0

    invoke-direct/range {v20 .. v21}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)V

    .line 2500
    .local v20, "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    move v8, v14

    .line 2501
    .local v8, "index":I
    :cond_17
    if-gtz v8, :cond_19

    .line 2510
    :goto_4
    if-eqz v13, :cond_4

    .line 2511
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v21

    if-nez v21, :cond_18

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v21, v0

    if-eqz v21, :cond_1a

    .line 2512
    :cond_18
    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v6, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2502
    :cond_19
    add-int/lit8 v8, v8, -0x1

    .line 2503
    invoke-virtual {v15, v8}, Ljava/lang/String;->charAt(I)C

    move-result v11

    .line 2504
    .local v11, "oneChar":C
    const/16 v21, 0xa

    move/from16 v0, v21

    if-eq v11, v0, :cond_17

    const/16 v21, 0xd

    move/from16 v0, v21

    if-eq v11, v0, :cond_17

    const/16 v21, 0x20

    move/from16 v0, v21

    if-eq v11, v0, :cond_17

    const/16 v21, 0x9

    move/from16 v0, v21

    if-eq v11, v0, :cond_17

    .line 2505
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->findWord(ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z

    .line 2506
    const/4 v13, 0x1

    .line 2507
    goto :goto_4

    .line 2514
    .end local v11    # "oneChar":C
    :cond_1a
    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2518
    .end local v8    # "index":I
    .end local v13    # "processed":Z
    .end local v20    # "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    :cond_1b
    const/4 v13, 0x0

    .line 2519
    .restart local v13    # "processed":Z
    new-instance v20, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;

    const/16 v21, 0x0

    invoke-direct/range {v20 .. v21}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)V

    .line 2520
    .restart local v20    # "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    move v8, v6

    .line 2521
    .restart local v8    # "index":I
    :cond_1c
    if-gtz v8, :cond_1e

    .line 2530
    :goto_5
    if-eqz v13, :cond_4

    .line 2531
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v21

    if-nez v21, :cond_1d

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v21, v0

    if-eqz v21, :cond_21

    .line 2532
    :cond_1d
    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v14, v0, :cond_1f

    .line 2533
    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v14, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 2534
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2522
    :cond_1e
    add-int/lit8 v8, v8, -0x1

    .line 2523
    invoke-virtual {v15, v8}, Ljava/lang/String;->charAt(I)C

    move-result v11

    .line 2524
    .restart local v11    # "oneChar":C
    const/16 v21, 0xa

    move/from16 v0, v21

    if-eq v11, v0, :cond_1c

    const/16 v21, 0xd

    move/from16 v0, v21

    if-eq v11, v0, :cond_1c

    const/16 v21, 0x20

    move/from16 v0, v21

    if-eq v11, v0, :cond_1c

    const/16 v21, 0x9

    move/from16 v0, v21

    if-eq v11, v0, :cond_1c

    .line 2525
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->findWord(ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z

    .line 2526
    const/4 v13, 0x1

    .line 2527
    goto :goto_5

    .line 2535
    .end local v11    # "oneChar":C
    :cond_1f
    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ne v14, v0, :cond_20

    .line 2536
    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2538
    :cond_20
    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v14, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2541
    :cond_21
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2546
    .end local v8    # "index":I
    .end local v13    # "processed":Z
    .end local v15    # "str":Ljava/lang/String;
    .end local v20    # "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    :cond_22
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v21

    if-nez v21, :cond_23

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v21, v0

    if-eqz v21, :cond_27

    .line 2547
    :cond_23
    const/16 v21, 0x1

    move/from16 v0, v21

    if-le v12, v0, :cond_24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v21

    add-int/lit8 v22, v12, -0x2

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->charAt(I)C

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isHighSurrogateByte(I)Z

    move-result v21

    if-eqz v21, :cond_24

    const/4 v5, 0x2

    .line 2548
    .local v5, "d":I
    :goto_6
    if-ne v14, v12, :cond_25

    .line 2549
    if-lez v14, :cond_4

    .line 2550
    sub-int v21, v14, v5

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v6, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2547
    .end local v5    # "d":I
    :cond_24
    const/4 v5, 0x1

    goto :goto_6

    .line 2553
    .restart local v5    # "d":I
    :cond_25
    sub-int v21, v6, v5

    move/from16 v0, v21

    if-ge v14, v0, :cond_26

    .line 2554
    sub-int v21, v6, v5

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v14, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 2555
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    sub-int v22, v6, v5

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2556
    :cond_26
    sub-int v21, v6, v5

    move/from16 v0, v21

    if-ne v14, v0, :cond_4

    .line 2557
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2561
    .end local v5    # "d":I
    :cond_27
    if-eq v14, v6, :cond_28

    .line 2562
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v14, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2564
    :cond_28
    if-lez v12, :cond_4

    .line 2565
    const/16 v21, 0x1

    move/from16 v0, v21

    if-le v12, v0, :cond_29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v21

    add-int/lit8 v22, v12, -0x2

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->charAt(I)C

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isHighSurrogateByte(I)Z

    move-result v21

    if-eqz v21, :cond_29

    const/4 v5, 0x2

    .line 2566
    .restart local v5    # "d":I
    :goto_7
    sub-int v21, v12, v5

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2565
    .end local v5    # "d":I
    :cond_29
    const/4 v5, 0x1

    goto :goto_7

    .line 2574
    :sswitch_3
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 2575
    if-ltz v9, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v9, v0, :cond_0

    .line 2578
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v21

    if-nez v21, :cond_2a

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isAltPressed:Z

    move/from16 v21, v0

    if-eqz v21, :cond_2b

    .line 2579
    :cond_2a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v22, v0

    aget v22, v22, v9

    aget-object v21, v21, v22

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v9, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v21

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2580
    :cond_2b
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v21

    if-nez v21, :cond_2c

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isCtrlPressed:Z

    move/from16 v21, v0

    if-eqz v21, :cond_38

    .line 2581
    :cond_2c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v15

    .line 2582
    .restart local v15    # "str":Ljava/lang/String;
    if-eqz v15, :cond_4

    .line 2583
    if-ne v6, v12, :cond_31

    .line 2584
    const/4 v13, 0x0

    .line 2585
    .restart local v13    # "processed":Z
    new-instance v20, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;

    const/16 v21, 0x0

    invoke-direct/range {v20 .. v21}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)V

    .line 2586
    .restart local v20    # "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    move v8, v6

    .line 2587
    .restart local v8    # "index":I
    :goto_8
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    if-lt v8, v0, :cond_2e

    .line 2596
    :goto_9
    if-eqz v13, :cond_4

    .line 2597
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v21

    if-nez v21, :cond_2d

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v21, v0

    if-eqz v21, :cond_30

    .line 2598
    :cond_2d
    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v14, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 2599
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2588
    :cond_2e
    invoke-virtual {v15, v8}, Ljava/lang/String;->charAt(I)C

    move-result v11

    .line 2589
    .restart local v11    # "oneChar":C
    const/16 v21, 0xa

    move/from16 v0, v21

    if-eq v11, v0, :cond_2f

    const/16 v21, 0xd

    move/from16 v0, v21

    if-eq v11, v0, :cond_2f

    const/16 v21, 0x20

    move/from16 v0, v21

    if-eq v11, v0, :cond_2f

    const/16 v21, 0x9

    move/from16 v0, v21

    if-eq v11, v0, :cond_2f

    .line 2590
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->findWord(ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z

    .line 2591
    const/4 v13, 0x1

    .line 2592
    goto :goto_9

    .line 2594
    :cond_2f
    add-int/lit8 v8, v8, 0x1

    goto :goto_8

    .line 2601
    .end local v11    # "oneChar":C
    :cond_30
    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2605
    .end local v8    # "index":I
    .end local v13    # "processed":Z
    .end local v20    # "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    :cond_31
    const/4 v13, 0x0

    .line 2606
    .restart local v13    # "processed":Z
    new-instance v20, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;

    const/16 v21, 0x0

    invoke-direct/range {v20 .. v21}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)V

    .line 2607
    .restart local v20    # "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    move v8, v14

    .line 2608
    .restart local v8    # "index":I
    :goto_a
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    if-lt v8, v0, :cond_33

    .line 2617
    :goto_b
    if-eqz v13, :cond_4

    .line 2618
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v21

    if-nez v21, :cond_32

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v21, v0

    if-eqz v21, :cond_37

    .line 2619
    :cond_32
    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v0, v6, :cond_35

    .line 2620
    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v6, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2609
    :cond_33
    invoke-virtual {v15, v8}, Ljava/lang/String;->charAt(I)C

    move-result v11

    .line 2610
    .restart local v11    # "oneChar":C
    const/16 v21, 0xa

    move/from16 v0, v21

    if-eq v11, v0, :cond_34

    const/16 v21, 0xd

    move/from16 v0, v21

    if-eq v11, v0, :cond_34

    const/16 v21, 0x20

    move/from16 v0, v21

    if-eq v11, v0, :cond_34

    const/16 v21, 0x9

    move/from16 v0, v21

    if-eq v11, v0, :cond_34

    .line 2611
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->findWord(ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z

    .line 2612
    const/4 v13, 0x1

    .line 2613
    goto :goto_b

    .line 2615
    :cond_34
    add-int/lit8 v8, v8, 0x1

    goto :goto_a

    .line 2621
    .end local v11    # "oneChar":C
    :cond_35
    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ne v0, v6, :cond_36

    .line 2622
    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2624
    :cond_36
    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v6, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 2625
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2628
    :cond_37
    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2633
    .end local v8    # "index":I
    .end local v13    # "processed":Z
    .end local v15    # "str":Ljava/lang/String;
    .end local v20    # "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    :cond_38
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v21

    if-nez v21, :cond_39

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v21, v0

    if-eqz v21, :cond_3d

    .line 2634
    :cond_39
    add-int/lit8 v21, v12, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_3a

    .line 2635
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/String;->charAt(I)C

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isHighSurrogateByte(I)Z

    move-result v21

    if-eqz v21, :cond_3a

    const/4 v5, 0x2

    .line 2636
    .restart local v5    # "d":I
    :goto_c
    if-ne v6, v12, :cond_3b

    .line 2637
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Landroid/text/Editable;->length()I

    move-result v21

    move/from16 v0, v21

    if-ge v6, v0, :cond_4

    .line 2638
    add-int v21, v6, v5

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v14, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 2639
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    add-int v22, v6, v5

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2635
    .end local v5    # "d":I
    :cond_3a
    const/4 v5, 0x1

    goto :goto_c

    .line 2642
    .restart local v5    # "d":I
    :cond_3b
    add-int v21, v14, v5

    move/from16 v0, v21

    if-ge v0, v6, :cond_3c

    .line 2643
    add-int v21, v14, v5

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v6, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2644
    :cond_3c
    add-int v21, v14, v5

    move/from16 v0, v21

    if-ne v0, v6, :cond_4

    .line 2645
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto/16 :goto_3

    .line 2649
    .end local v5    # "d":I
    :cond_3d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    move/from16 v21, v0

    if-lez v21, :cond_4

    .line 2650
    add-int/lit8 v21, v12, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_3e

    .line 2651
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/String;->charAt(I)C

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isHighSurrogateByte(I)Z

    move-result v21

    if-eqz v21, :cond_3e

    const/4 v5, 0x2

    .line 2652
    .restart local v5    # "d":I
    :goto_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, -0x1

    aget v21, v21, v22

    add-int v21, v21, v5

    move/from16 v0, v21

    if-ge v12, v0, :cond_4

    .line 2653
    if-eq v14, v6, :cond_3f

    .line 2654
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v6, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2651
    .end local v5    # "d":I
    :cond_3e
    const/4 v5, 0x1

    goto :goto_d

    .line 2656
    .restart local v5    # "d":I
    :cond_3f
    add-int v21, v12, v5

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2665
    .end local v5    # "d":I
    :sswitch_4
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 2666
    if-ltz v9, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v9, v0, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    move-object/from16 v21, v0

    aget v21, v21, v9

    if-ltz v21, :cond_0

    .line 2670
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    move-object/from16 v22, v0

    aget v22, v22, v9

    aget-object v21, v21, v22

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v9, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v16

    .line 2671
    .local v16, "temp":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v21

    if-nez v21, :cond_40

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v21, v0

    if-eqz v21, :cond_41

    :cond_40
    move/from16 v0, v16

    if-eq v0, v14, :cond_41

    .line 2672
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v21

    invoke-virtual {v0, v1, v14, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2674
    :cond_41
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2680
    .end local v16    # "temp":I
    :sswitch_5
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 2681
    if-ltz v9, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v9, v0, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v21, v0

    aget v21, v21, v9

    if-ltz v21, :cond_0

    .line 2685
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v22, v0

    aget v22, v22, v9

    aget-object v21, v21, v22

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v9, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v3

    .line 2688
    .local v3, "cursorPos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v21, v0

    if-eqz v21, :cond_43

    .line 2689
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    if-lez v21, :cond_43

    .line 2690
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    if-lt v7, v0, :cond_45

    .line 2704
    :goto_f
    if-lez v3, :cond_43

    .line 2705
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v22, v0

    aget v22, v22, v9

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->charAt(I)C

    move-result v21

    const/16 v22, 0x20

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_42

    .line 2706
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v22, v0

    aget v22, v22, v9

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->charAt(I)C

    move-result v21

    const/16 v22, 0x9

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_49

    .line 2708
    :cond_42
    add-int/lit8 v3, v3, -0x1

    .line 2718
    .end local v7    # "i":I
    :cond_43
    :goto_10
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v21

    if-nez v21, :cond_44

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v21, v0

    if-eqz v21, :cond_4a

    :cond_44
    if-eq v3, v6, :cond_4a

    .line 2719
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v6, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2691
    .restart local v7    # "i":I
    :cond_45
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v21, v0

    aget v21, v21, v7

    move/from16 v0, v21

    if-ne v3, v0, :cond_48

    .line 2692
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    add-int/lit8 v21, v21, -0x1

    move/from16 v0, v21

    if-ne v7, v0, :cond_46

    .line 2693
    add-int/lit8 v3, v3, 0x1

    .line 2694
    goto :goto_f

    .line 2696
    :cond_46
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v21

    add-int/lit8 v22, v3, 0x1

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->charAt(I)C

    move-result v11

    .line 2697
    .restart local v11    # "oneChar":C
    const/16 v21, 0xa

    move/from16 v0, v21

    if-eq v11, v0, :cond_47

    const/16 v21, 0xd

    move/from16 v0, v21

    if-ne v11, v0, :cond_48

    .line 2698
    :cond_47
    add-int/lit8 v3, v3, 0x1

    .line 2699
    goto/16 :goto_f

    .line 2690
    .end local v11    # "oneChar":C
    :cond_48
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_e

    .line 2709
    :cond_49
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    add-int/lit8 v21, v21, -0x1

    move/from16 v0, v21

    if-ge v9, v0, :cond_43

    .line 2710
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    move-object/from16 v22, v0

    add-int/lit8 v23, v9, 0x1

    aget v22, v22, v23

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->charAt(I)C

    move-result v21

    const/16 v22, 0xa

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_43

    .line 2711
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    move-object/from16 v22, v0

    add-int/lit8 v23, v9, 0x1

    aget v22, v22, v23

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->charAt(I)C

    move-result v21

    const/16 v22, 0xd

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_43

    .line 2712
    add-int/lit8 v3, v3, -0x1

    goto/16 :goto_10

    .line 2721
    .end local v7    # "i":I
    :cond_4a
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2727
    .end local v3    # "cursorPos":I
    :sswitch_6
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 2728
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v21

    if-nez v21, :cond_4b

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v21, v0

    if-eqz v21, :cond_4c

    :cond_4b
    if-eqz v14, :cond_4c

    .line 2729
    const/16 v21, 0x0

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v14, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2731
    :cond_4c
    const/16 v21, 0x0

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2737
    :sswitch_7
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 2738
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    move/from16 v21, v0

    if-lez v21, :cond_4

    .line 2739
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, -0x1

    aget v21, v21, v22

    add-int/lit8 v17, v21, 0x1

    .line 2740
    .local v17, "temp1":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v21

    if-nez v21, :cond_4d

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    move/from16 v21, v0

    if-eqz v21, :cond_4e

    :cond_4d
    move/from16 v0, v17

    if-eq v6, v0, :cond_4e

    .line 2741
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v21

    invoke-virtual {v0, v6, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 2743
    :cond_4e
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_3

    .line 2760
    .end local v17    # "temp1":I
    :cond_4f
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideCursorHandle()V

    .line 2761
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->reset()V

    .line 2762
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    goto/16 :goto_2

    .line 2428
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x5c -> :sswitch_6
        0x5d -> :sswitch_7
        0x7a -> :sswitch_4
        0x7b -> :sswitch_5
        0x91 -> :sswitch_5
        0x92 -> :sswitch_1
        0x93 -> :sswitch_7
        0x94 -> :sswitch_2
        0x96 -> :sswitch_3
        0x97 -> :sswitch_4
        0x98 -> :sswitch_0
        0x99 -> :sswitch_6
    .end sparse-switch
.end method

.method private paste(Landroid/content/ClipboardManager;II)Z
    .locals 11
    .param p1, "clipboard"    # Landroid/content/ClipboardManager;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v10, 0x0

    .line 7163
    const/4 v6, 0x0

    .line 7164
    .local v6, "str":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v0

    .line 7165
    .local v0, "clip":Landroid/content/ClipData;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->lastText:Ljava/lang/String;

    .line 7167
    if-eqz v0, :cond_2

    .line 7168
    invoke-virtual {v0, v10}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    .line 7169
    .local v4, "item":Landroid/content/ClipData$Item;
    if-eqz v4, :cond_1

    .line 7170
    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    .line 7171
    .local v5, "sequence":Ljava/lang/CharSequence;
    if-eqz v5, :cond_1

    .line 7172
    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 7173
    if-eqz v6, :cond_1

    .line 7174
    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 7175
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, " "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 7176
    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->appendText(Ljava/lang/String;)V

    .line 7177
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    if-nez v7, :cond_3

    .line 7178
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 7179
    const-string/jumbo v8, "string_pasted_to_clipboard"

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 7178
    invoke-static {v7, v8, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    .line 7184
    :goto_0
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    const/16 v8, 0x50

    const/16 v9, 0x96

    invoke-virtual {v7, v8, v10, v9}, Landroid/widget/Toast;->setGravity(III)V

    .line 7185
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 7188
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v7}, Landroid/text/Editable;->length()I

    move-result v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    sub-int v3, v7, v8

    .line 7189
    .local v3, "diff":I
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    add-int p3, p2, v7

    .line 7190
    if-ltz v3, :cond_0

    .line 7191
    iget p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    .line 7193
    :cond_0
    if-le p2, p3, :cond_4

    .end local p2    # "start":I
    :goto_1
    invoke-virtual {p0, p2, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 7199
    .end local v3    # "diff":I
    .end local v5    # "sequence":Ljava/lang/CharSequence;
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v7}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    .line 7200
    .local v2, "curSelectStart":I
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v7}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    .line 7201
    .local v1, "curSelectEnd":I
    if-le v1, v2, :cond_2

    .line 7202
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v7

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 7205
    .end local v1    # "curSelectEnd":I
    .end local v2    # "curSelectStart":I
    .end local v4    # "item":Landroid/content/ClipData$Item;
    :cond_2
    iput-boolean v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsChangedByKeyShortCut:Z

    .line 7206
    const/4 v6, 0x0

    .line 7207
    const/4 v0, 0x0

    .line 7208
    const/4 v7, 0x1

    return v7

    .line 7181
    .restart local v4    # "item":Landroid/content/ClipData$Item;
    .restart local v5    # "sequence":Ljava/lang/CharSequence;
    .restart local p2    # "start":I
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    const-string/jumbo v8, "string_pasted_to_clipboard"

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 7182
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v7, v10}, Landroid/widget/Toast;->setDuration(I)V

    goto :goto_0

    .restart local v3    # "diff":I
    :cond_4
    move p2, p3

    .line 7193
    goto :goto_1
.end method

.method private redo()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 7263
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->nextText:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 7264
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    .line 7265
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsUndoOrRedo:Z

    .line 7266
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->nextText:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setText(Ljava/lang/String;)V

    .line 7267
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsUndoOrRedo:Z

    .line 7268
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAfterCurPos:I

    if-gez v0, :cond_0

    .line 7269
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAfterCurPos:I

    .line 7271
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAfterCurPos:I

    invoke-virtual {p0, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 7272
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 7273
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->nextText:Ljava/lang/String;

    .line 7275
    :cond_1
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsChangedByKeyShortCut:Z

    .line 7276
    return v1
.end method

.method private relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
    .locals 2
    .param p1, "dstRect"    # Landroid/graphics/RectF;
    .param p2, "srcRect"    # Landroid/graphics/RectF;
    .param p3, "coordinateInfo"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .prologue
    .line 5358
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 5359
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    .line 5360
    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    .line 5359
    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 5361
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 5362
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    .line 5363
    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v1

    .line 5362
    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 5364
    return-void
.end method

.method private resize(FF)V
    .locals 10
    .param p1, "relativeWidth"    # F
    .param p2, "relativeHeight"    # F

    .prologue
    .line 3284
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 3285
    .local v3, "prevRelativeRect":Landroid/graphics/RectF;
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 3287
    .local v4, "relativeRect":Landroid/graphics/RectF;
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v0

    .line 3288
    .local v0, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-nez v0, :cond_1

    .line 3336
    :cond_0
    :goto_0
    return-void

    .line 3293
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v7, :cond_0

    .line 3297
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 3298
    .local v2, "objectRelativeRect":Landroid/graphics/RectF;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    .line 3299
    .local v1, "objectRect":Landroid/graphics/RectF;
    if-eqz v1, :cond_0

    .line 3303
    invoke-direct {p0, v2, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3305
    invoke-virtual {v3, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 3307
    invoke-virtual {v4, v3}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 3308
    iget v7, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v7, p2

    iput v7, v4, Landroid/graphics/RectF;->bottom:F

    .line 3309
    iget v7, v4, Landroid/graphics/RectF;->left:F

    add-float/2addr v7, p1

    iput v7, v4, Landroid/graphics/RectF;->right:F

    .line 3311
    invoke-virtual {v2, v4}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 3313
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v5

    .line 3314
    .local v5, "rotation":F
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setRotation(F)V

    .line 3316
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    .line 3318
    .local v6, "tempAbsoluteRect":Landroid/graphics/RectF;
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v7

    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setPivotX(F)V

    .line 3319
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v7

    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setPivotY(F)V

    .line 3320
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 3322
    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    move-result v7

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v8

    sub-float/2addr v7, v8

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerY()F

    move-result v8

    .line 3323
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v9

    sub-float/2addr v8, v9

    .line 3322
    invoke-virtual {v2, v7, v8}, Landroid/graphics/RectF;->offset(FF)V

    .line 3325
    invoke-direct {p0, v6, v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3327
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    const/4 v8, 0x1

    invoke-virtual {v7, v6, v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setRect(Landroid/graphics/RectF;Z)V

    .line 3329
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onObjectChanged()V

    goto :goto_0
.end method

.method private selectAll()Z
    .locals 1

    .prologue
    .line 7040
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelectionAll()V

    .line 7041
    const/4 v0, 0x1

    return v0
.end method

.method private sendAccessibilityEventTypeViewTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "beforeText"    # Ljava/lang/CharSequence;
    .param p2, "fromIndex"    # I
    .param p3, "removedCount"    # I
    .param p4, "addedCount"    # I

    .prologue
    .line 6771
    const/16 v1, 0x10

    invoke-static {v1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 6772
    .local v0, "event":Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v0, p2}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    .line 6773
    invoke-virtual {v0, p3}, Landroid/view/accessibility/AccessibilityEvent;->setRemovedCount(I)V

    .line 6774
    invoke-virtual {v0, p4}, Landroid/view/accessibility/AccessibilityEvent;->setAddedCount(I)V

    .line 6775
    invoke-virtual {v0, p1}, Landroid/view/accessibility/AccessibilityEvent;->setBeforeText(Ljava/lang/CharSequence;)V

    .line 6776
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 6777
    return-void
.end method

.method private setChangeWatcher()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 4593
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-nez v6, :cond_1

    .line 4620
    :cond_0
    :goto_0
    return-void

    .line 4597
    :cond_1
    const/16 v0, 0x64

    .line 4598
    .local v0, "PRIORITY":I
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v4

    .line 4600
    .local v4, "textLength":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    .line 4602
    .local v3, "sp":Landroid/text/Spannable;
    invoke-interface {v3}, Landroid/text/Spannable;->length()I

    move-result v6

    const-class v7, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    invoke-interface {v3, v8, v6, v7}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    .line 4603
    .local v5, "watchers":[Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;
    array-length v1, v5

    .line 4604
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-lt v2, v1, :cond_3

    .line 4608
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mChangeWatcher:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    if-nez v6, :cond_2

    .line 4609
    new-instance v6, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    const/4 v7, 0x0

    invoke-direct {v6, p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;)V

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mChangeWatcher:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    .line 4614
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mChangeWatcher:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    const v7, 0x640012

    invoke-interface {v3, v6, v8, v4, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 4617
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    if-eqz v6, :cond_0

    .line 4618
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    const/16 v7, 0x12

    invoke-interface {v3, v6, v8, v4, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 4605
    :cond_3
    aget-object v6, v5, v2

    invoke-interface {v3, v6}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 4604
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private setHandlePos(Landroid/view/View;IIIILandroid/graphics/RectF;)V
    .locals 8
    .param p1, "handle"    # Landroid/view/View;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "relativeObjectRect"    # Landroid/graphics/RectF;

    .prologue
    .line 3822
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v4, :cond_1

    .line 3855
    :cond_0
    :goto_0
    return-void

    .line 3826
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 3827
    .local v2, "parentLayout":Landroid/view/ViewGroup;
    if-eqz v2, :cond_0

    .line 3831
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3833
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 3834
    .local v0, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 3835
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v4

    invoke-virtual {p6}, Landroid/graphics/RectF;->centerX()F

    move-result v5

    invoke-virtual {p6}, Landroid/graphics/RectF;->centerY()F

    move-result v6

    invoke-virtual {v0, v4, v5, v6}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 3837
    const/4 v4, 0x2

    new-array v3, v4, [F

    const/4 v4, 0x0

    int-to-float v5, p2

    aput v5, v3, v4

    const/4 v4, 0x1

    int-to-float v5, p3

    aput v5, v3, v4

    .line 3838
    .local v3, "points":[F
    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 3840
    const/4 v4, 0x0

    aget v4, v3, v4

    float-to-int v4, v4

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 3841
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    iget v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    add-int/2addr v5, p4

    sub-int/2addr v4, v5

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 3842
    const/4 v4, 0x1

    aget v4, v3, v4

    float-to-int v4, v4

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3843
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    iget v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    add-int/2addr v5, p5

    sub-int/2addr v4, v5

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 3844
    iput p4, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 3845
    iput p5, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 3847
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/view/View;->setPivotX(F)V

    .line 3848
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/view/View;->setPivotY(F)V

    .line 3849
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v4

    invoke-virtual {p1, v4}, Landroid/view/View;->setRotation(F)V

    .line 3850
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/view/View;->setPadding(IIII)V

    .line 3852
    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3853
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 3854
    invoke-virtual {p1}, Landroid/view/View;->bringToFront()V

    goto :goto_0
.end method

.method private setHoveringSpenIcon(I)Z
    .locals 5
    .param p1, "typeIcon"    # I

    .prologue
    const/4 v2, 0x0

    .line 7280
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    if-nez v3, :cond_1

    .line 7313
    :cond_0
    :goto_0
    return v2

    .line 7283
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 7284
    .local v1, "pm":Landroid/content/pm/PackageManager;
    if-eqz v1, :cond_0

    .line 7285
    const-string v3, "com.sec.feature.hovering_ui"

    invoke-virtual {v1, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 7293
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    invoke-virtual {v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->setHoveringSpenIcon(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5

    .line 7313
    const/4 v2, 0x1

    goto :goto_0

    .line 7294
    :catch_0
    move-exception v0

    .line 7295
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "SpenTextBox"

    const-string v4, "setHoveringSpenIcon() IllegalArgumentException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 7297
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 7298
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    const-string v3, "SpenTextBox"

    const-string v4, "setHoveringSpenIcon() NotFoundException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 7300
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    :catch_2
    move-exception v0

    .line 7301
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v3, "SpenTextBox"

    const-string v4, "setHoveringSpenIcon() ClassNotFoundException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 7303
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_3
    move-exception v0

    .line 7304
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v3, "SpenTextBox"

    const-string v4, "setHoveringSpenIcon() NoSuchMethodException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 7306
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_4
    move-exception v0

    .line 7307
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v3, "SpenTextBox"

    const-string v4, "setHoveringSpenIcon() IllegalAccessException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 7309
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_5
    move-exception v0

    .line 7310
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v3, "SpenTextBox"

    const-string v4, "setHoveringSpenIcon() InvocationTargetException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setParagraphAlign(I)V
    .locals 8
    .param p1, "align"    # I

    .prologue
    .line 4838
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v7, :cond_1

    .line 4869
    :cond_0
    :goto_0
    return-void

    .line 4842
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getParagraph()Ljava/util/ArrayList;

    move-result-object v5

    .line 4843
    .local v5, "paragraphs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    if-eqz v5, :cond_0

    .line 4848
    const/4 v4, 0x0

    .line 4849
    .local v4, "paragraphNumber":I
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v6

    .line 4850
    .local v6, "text":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 4851
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v2

    .line 4852
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v2, :cond_3

    .line 4859
    .end local v1    # "i":I
    .end local v2    # "length":I
    :cond_2
    add-int/lit8 v4, v4, 0x1

    .line 4861
    new-instance v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;-><init>()V

    .line 4862
    .local v3, "para":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;
    const/4 v7, 0x0

    iput v7, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->startPos:I

    .line 4863
    iput v4, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->endPos:I

    .line 4864
    iput p1, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    .line 4866
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4868
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7, v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setParagraph(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 4853
    .end local v3    # "para":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;
    .restart local v1    # "i":I
    .restart local v2    # "length":I
    :cond_3
    invoke-virtual {v6, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 4854
    .local v0, "c":C
    const/16 v7, 0xa

    if-eq v0, v7, :cond_4

    const/16 v7, 0xd

    if-ne v0, v7, :cond_5

    .line 4855
    :cond_4
    add-int/lit8 v4, v4, 0x1

    .line 4852
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private setParagraphIndent(I)V
    .locals 0
    .param p1, "indent"    # I

    .prologue
    .line 4872
    return-void
.end method

.method private setParagraphSpacing(IF)V
    .locals 5
    .param p1, "type"    # I
    .param p2, "spacing"    # F

    .prologue
    const/4 v4, 0x0

    .line 4916
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_1

    .line 4939
    :cond_0
    :goto_0
    return-void

    .line 4920
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getParagraph()Ljava/util/ArrayList;

    move-result-object v1

    .line 4921
    .local v1, "spans":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    if-eqz v1, :cond_0

    .line 4925
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;-><init>()V

    .line 4926
    .local v0, "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->startPos:I

    .line 4927
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->endPos:I

    .line 4928
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v2

    .line 4929
    .local v2, "str":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 4930
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    iput v3, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->endPos:I

    .line 4933
    :cond_2
    iput p1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    .line 4934
    iput p2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    .line 4936
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4938
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setParagraph(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private setTextBackgroundColor(III)V
    .locals 5
    .param p1, "color"    # I
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    .line 4746
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_0

    .line 4766
    :goto_0
    return-void

    .line 4750
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;-><init>()V

    .line 4751
    .local v0, "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v3

    if-nez v3, :cond_2

    .line 4752
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    .line 4753
    .local v1, "str":Ljava/lang/String;
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4754
    if-nez v1, :cond_1

    :goto_1
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4755
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    .end local v1    # "str":Ljava/lang/String;
    :goto_2
    move-object v2, v0

    .line 4763
    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    iput p1, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    .line 4765
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    .line 4754
    .restart local v1    # "str":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    goto :goto_1

    .line 4758
    .end local v1    # "str":Ljava/lang/String;
    :cond_2
    iput p2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4759
    iput p3, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4760
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_2
.end method

.method private setTextBold(ZII)V
    .locals 5
    .param p1, "bold"    # Z
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    .line 4769
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_0

    .line 4789
    :goto_0
    return-void

    .line 4773
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;-><init>()V

    .line 4774
    .local v0, "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v3

    if-nez v3, :cond_2

    .line 4775
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    .line 4776
    .local v1, "str":Ljava/lang/String;
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4777
    if-nez v1, :cond_1

    :goto_1
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4778
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    .end local v1    # "str":Ljava/lang/String;
    :goto_2
    move-object v2, v0

    .line 4786
    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;

    iput-boolean p1, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;->isBold:Z

    .line 4788
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    .line 4777
    .restart local v1    # "str":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    goto :goto_1

    .line 4781
    .end local v1    # "str":Ljava/lang/String;
    :cond_2
    iput p2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4782
    iput p3, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4783
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_2
.end method

.method private setTextFontColor(III)V
    .locals 8
    .param p1, "color"    # I
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v7, 0x3

    const/4 v5, 0x0

    .line 4688
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v4, :cond_0

    .line 4720
    :goto_0
    return-void

    .line 4692
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getSpan()Ljava/util/ArrayList;

    move-result-object v2

    .line 4693
    .local v2, "spans":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    if-eqz v2, :cond_2

    .line 4694
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 4705
    :cond_2
    :goto_1
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;-><init>()V

    .line 4706
    .local v1, "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v4

    if-nez v4, :cond_5

    .line 4707
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v3

    .line 4708
    .local v3, "str":Ljava/lang/String;
    iput v5, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4709
    if-nez v3, :cond_4

    move v4, v5

    :goto_2
    iput v4, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4710
    iput v7, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    .end local v3    # "str":Ljava/lang/String;
    :goto_3
    move-object v4, v1

    .line 4718
    check-cast v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    iput p1, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    .line 4719
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    .line 4694
    .end local v1    # "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 4695
    .local v0, "inf":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    instance-of v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    if-eqz v4, :cond_1

    .line 4696
    iget v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ne v4, p2, :cond_1

    iget v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-ne v4, p3, :cond_1

    move-object v4, v0

    .line 4697
    check-cast v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    if-eq v4, p1, :cond_1

    .line 4698
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->removeSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_1

    .line 4709
    .end local v0    # "inf":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    .restart local v1    # "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    .restart local v3    # "str":Ljava/lang/String;
    :cond_4
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    goto :goto_2

    .line 4713
    .end local v3    # "str":Ljava/lang/String;
    :cond_5
    iput p2, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4714
    iput p3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4715
    iput v7, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_3
.end method

.method private setTextFontName(Ljava/lang/String;II)V
    .locals 5
    .param p1, "font"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    .line 4723
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_0

    .line 4743
    :goto_0
    return-void

    .line 4727
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;-><init>()V

    .line 4728
    .local v0, "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v3

    if-nez v3, :cond_2

    .line 4729
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    .line 4730
    .local v1, "str":Ljava/lang/String;
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4731
    if-nez v1, :cond_1

    :goto_1
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4732
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    .end local v1    # "str":Ljava/lang/String;
    :goto_2
    move-object v2, v0

    .line 4740
    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    iput-object p1, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    .line 4742
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    .line 4731
    .restart local v1    # "str":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    goto :goto_1

    .line 4735
    .end local v1    # "str":Ljava/lang/String;
    :cond_2
    iput p2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4736
    iput p3, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4737
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_2
.end method

.method private setTextFontSize(FII)V
    .locals 5
    .param p1, "size"    # F
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    .line 4665
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_0

    .line 4685
    :goto_0
    return-void

    .line 4669
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;-><init>()V

    .line 4670
    .local v0, "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v3

    if-nez v3, :cond_2

    .line 4671
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    .line 4672
    .local v1, "str":Ljava/lang/String;
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4673
    if-nez v1, :cond_1

    :goto_1
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4674
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    .end local v1    # "str":Ljava/lang/String;
    :goto_2
    move-object v2, v0

    .line 4682
    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iput p1, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    .line 4684
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    .line 4673
    .restart local v1    # "str":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    goto :goto_1

    .line 4677
    .end local v1    # "str":Ljava/lang/String;
    :cond_2
    iput p2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4678
    iput p3, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4679
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_2
.end method

.method private setTextItalic(ZII)V
    .locals 5
    .param p1, "italic"    # Z
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    .line 4792
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_0

    .line 4812
    :goto_0
    return-void

    .line 4796
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;-><init>()V

    .line 4797
    .local v0, "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v3

    if-nez v3, :cond_2

    .line 4798
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    .line 4799
    .local v1, "str":Ljava/lang/String;
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4800
    if-nez v1, :cond_1

    :goto_1
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4801
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    .end local v1    # "str":Ljava/lang/String;
    :goto_2
    move-object v2, v0

    .line 4809
    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    iput-boolean p1, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;->isItalic:Z

    .line 4811
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    .line 4800
    .restart local v1    # "str":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    goto :goto_1

    .line 4804
    .end local v1    # "str":Ljava/lang/String;
    :cond_2
    iput p2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4805
    iput p3, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4806
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_2
.end method

.method private setTextUnderline(ZII)V
    .locals 5
    .param p1, "underline"    # Z
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    .line 4815
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_0

    .line 4835
    :goto_0
    return-void

    .line 4819
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;-><init>()V

    .line 4820
    .local v0, "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v3

    if-nez v3, :cond_2

    .line 4821
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    .line 4822
    .local v1, "str":Ljava/lang/String;
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4823
    if-nez v1, :cond_1

    :goto_1
    iput v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4824
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    .end local v1    # "str":Ljava/lang/String;
    :goto_2
    move-object v2, v0

    .line 4832
    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;

    iput-boolean p1, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;->isUnderline:Z

    .line 4834
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    .line 4823
    .restart local v1    # "str":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    goto :goto_1

    .line 4827
    .end local v1    # "str":Ljava/lang/String;
    :cond_2
    iput p2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 4828
    iput p3, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 4829
    iput v4, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_2
.end method

.method private shouldBlink()Z
    .locals 1

    .prologue
    .line 5681
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isCursorVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v0

    if-nez v0, :cond_1

    .line 5682
    :cond_0
    const/4 v0, 0x0

    .line 5684
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private showCursorHandle()V
    .locals 6

    .prologue
    .line 5826
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    if-nez v0, :cond_0

    .line 5827
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    .line 5830
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5831
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x1388

    add-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;->postAtTime(Ljava/lang/Runnable;J)Z

    .line 5833
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleVisible:Z

    .line 5834
    return-void
.end method

.method private showDeletePopupWindow()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    .line 6965
    new-instance v10, Landroid/graphics/RectF;

    invoke-direct {v10}, Landroid/graphics/RectF;-><init>()V

    .line 6966
    .local v10, "objectRelativeRect":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v2

    invoke-direct {p0, v10, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 6967
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v13

    .line 6968
    .local v13, "pos":I
    invoke-direct {p0, v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 6969
    .local v0, "cRect":Landroid/graphics/Rect;
    if-nez v0, :cond_0

    .line 7001
    :goto_0
    return-void

    .line 6972
    :cond_0
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 6973
    .local v8, "cursorRect":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->top:F

    const/high16 v3, 0x40a00000    # 5.0f

    add-float/2addr v2, v3

    invoke-virtual {v8, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 6974
    new-instance v9, Landroid/graphics/RectF;

    invoke-direct {v9}, Landroid/graphics/RectF;-><init>()V

    .line 6975
    .local v9, "cursorRelativeRect":Landroid/graphics/RectF;
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v1

    invoke-direct {p0, v9, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 6976
    invoke-virtual {v9}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    float-to-int v2, v1

    iget v1, v9, Landroid/graphics/RectF;->bottom:F

    float-to-int v3, v1

    .line 6977
    invoke-virtual {v10}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual {v10}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v1

    float-to-double v6, v1

    move-object v1, p0

    .line 6976
    invoke-direct/range {v1 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v12

    .line 6979
    .local v12, "point":Landroid/graphics/PointF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 6980
    .local v11, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, v12, Landroid/graphics/PointF;->x:F

    float-to-int v1, v1

    .line 6981
    const/4 v2, 0x1

    const/high16 v3, 0x42200000    # 40.0f

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 6982
    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 6981
    invoke-static {v2, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    sub-int/2addr v1, v2

    .line 6980
    iput v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 6983
    iget v1, v12, Landroid/graphics/PointF;->y:F

    float-to-int v1, v1

    iput v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 6984
    iget v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-gez v1, :cond_3

    .line 6985
    iput v14, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 6989
    :cond_1
    :goto_1
    iget v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    if-gez v1, :cond_4

    .line 6990
    iput v14, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 6994
    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6996
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    invoke-virtual {v1, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 6997
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->bringToFront()V

    .line 6999
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeletePopupHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideDeletePopupRunable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 7000
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeletePopupHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideDeletePopupRunable:Ljava/lang/Runnable;

    const-wide/16 v4, 0xfa0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 6986
    :cond_3
    iget v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getWidth()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    if-le v2, v1, :cond_1

    .line 6987
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    goto :goto_1

    .line 6991
    :cond_4
    iget v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getHeight()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    if-le v2, v1, :cond_2

    .line 6992
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto :goto_2
.end method

.method private showDragText(ZII)V
    .locals 15
    .param p1, "visible"    # Z
    .param p2, "posX"    # I
    .param p3, "posY"    # I

    .prologue
    .line 2916
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 2969
    :cond_0
    :goto_0
    return-void

    .line 2919
    :cond_1
    if-eqz p1, :cond_8

    .line 2920
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v6, 0x14

    if-le v3, v6, :cond_7

    .line 2921
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    const/16 v6, 0x13

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isHighSurrogateByte(I)Z

    move-result v3

    if-eqz v3, :cond_6

    const/16 v10, 0x13

    .line 2923
    .local v10, "length":I
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2927
    .end local v10    # "length":I
    :goto_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/widget/TextView;->measure(II)V

    .line 2928
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    .line 2929
    .local v2, "height":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v14

    .line 2931
    .local v14, "width":I
    new-instance v13, Landroid/graphics/RectF;

    invoke-direct {v13}, Landroid/graphics/RectF;-><init>()V

    .line 2932
    .local v13, "relativeRect":Landroid/graphics/RectF;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v6

    invoke-direct {p0, v13, v3, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 2934
    move/from16 v0, p2

    int-to-float v3, v0

    iget v6, v13, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, v6

    int-to-float v6, v14

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    sub-float/2addr v3, v6

    float-to-int v4, v3

    .line 2935
    .local v4, "pivotX":I
    move/from16 v0, p3

    int-to-float v3, v0

    iget v6, v13, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v6

    int-to-float v6, v2

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    sub-float/2addr v3, v6

    float-to-int v5, v3

    .line 2937
    .local v5, "pivotY":I
    iget v3, v13, Landroid/graphics/RectF;->left:F

    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v6

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v6, v3

    .line 2938
    iget v3, v13, Landroid/graphics/RectF;->top:F

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    add-float/2addr v7, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v3

    float-to-double v8, v3

    move-object v3, p0

    .line 2937
    invoke-direct/range {v3 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v12

    .line 2940
    .local v12, "point":Landroid/graphics/PointF;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2941
    .local v11, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget v3, v12, Landroid/graphics/PointF;->x:F

    float-to-int v3, v3

    iput v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2942
    iget v3, v12, Landroid/graphics/PointF;->y:F

    float-to-int v3, v3

    iput v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2944
    iget v6, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getLeft()I

    move-result v3

    if-ge v6, v3, :cond_2

    .line 2945
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getLeft()I

    move-result v3

    iput v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2947
    :cond_2
    iget v6, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getTop()I

    move-result v3

    if-ge v6, v3, :cond_3

    .line 2948
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getTop()I

    move-result v3

    iput v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2950
    :cond_3
    iget v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    add-int v6, v3, v14

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getRight()I

    move-result v3

    if-le v6, v3, :cond_4

    .line 2951
    iget v6, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    add-int v7, v3, v14

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getRight()I

    move-result v3

    sub-int v3, v7, v3

    sub-int v3, v6, v3

    iput v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2953
    :cond_4
    iget v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    add-int v6, v3, v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getBottom()I

    move-result v3

    if-le v6, v3, :cond_5

    .line 2954
    iget v6, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    add-int v7, v3, v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getBottom()I

    move-result v3

    sub-int v3, v7, v3

    sub-int v3, v6, v3

    iput v3, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2957
    :cond_5
    iput v14, v11, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 2958
    iput v2, v11, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 2960
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2962
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setRotation(F)V

    .line 2964
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2965
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->bringToFront()V

    goto/16 :goto_0

    .line 2922
    .end local v2    # "height":I
    .end local v4    # "pivotX":I
    .end local v5    # "pivotY":I
    .end local v11    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v12    # "point":Landroid/graphics/PointF;
    .end local v13    # "relativeRect":Landroid/graphics/RectF;
    .end local v14    # "width":I
    :cond_6
    const/16 v10, 0x14

    goto/16 :goto_1

    .line 2925
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 2967
    :cond_8
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private showScrollBar()V
    .locals 6

    .prologue
    .line 5837
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    if-nez v0, :cond_0

    .line 5838
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    .line 5841
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5842
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x1f4

    add-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;->postAtTime(Ljava/lang/Runnable;J)Z

    .line 5844
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarVisible:Z

    .line 5845
    return-void
.end method

.method private undo()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 7227
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->lastText:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 7228
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    .line 7229
    const/4 v0, 0x0

    .line 7230
    .local v0, "same":Z
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    .line 7231
    .local v1, "text":Ljava/lang/String;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->nextText:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 7232
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->lastText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 7233
    const/4 v0, 0x1

    .line 7237
    :cond_0
    if-nez v0, :cond_1

    .line 7238
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->nextText:Ljava/lang/String;

    .line 7241
    :cond_1
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsUndoOrRedo:Z

    .line 7243
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->lastText:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setText(Ljava/lang/String;)V

    .line 7245
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsUndoOrRedo:Z

    .line 7247
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    if-gez v2, :cond_2

    .line 7248
    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 7250
    :cond_2
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_4

    .line 7251
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    invoke-virtual {p0, v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 7256
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 7257
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 7259
    .end local v0    # "same":Z
    .end local v1    # "text":Ljava/lang/String;
    :cond_3
    return v5

    .line 7253
    .restart local v0    # "same":Z
    .restart local v1    # "text":Ljava/lang/String;
    :cond_4
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    invoke-virtual {p0, v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto :goto_0
.end method

.method private updateContextmenu()V
    .locals 22

    .prologue
    .line 1792
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v19, v0

    if-eqz v19, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mDirtyFlag:Z

    move/from16 v19, v0

    if-eqz v19, :cond_a

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenuVisible:Z

    move/from16 v19, v0

    if-eqz v19, :cond_a

    .line 1793
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->reset()V

    .line 1796
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v12

    check-cast v12, Landroid/view/ViewGroup;

    .line 1797
    .local v12, "parentLayout":Landroid/view/ViewGroup;
    new-instance v13, Landroid/graphics/Rect;

    invoke-direct {v13}, Landroid/graphics/Rect;-><init>()V

    .line 1798
    .local v13, "parentRect":Landroid/graphics/Rect;
    invoke-virtual {v12, v13}, Landroid/view/ViewGroup;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1801
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1803
    .local v10, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v18, Landroid/graphics/Rect;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Rect;-><init>()V

    .line 1804
    .local v18, "textRect":Landroid/graphics/Rect;
    iget v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/Rect;->left:I

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 1805
    iget v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1806
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    iget v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 1807
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    iget v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1809
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v16

    .line 1810
    .local v16, "selStart":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v15

    .line 1811
    .local v15, "selEnd":I
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v17

    .line 1812
    .local v17, "startCursorRect":Landroid/graphics/Rect;
    if-nez v17, :cond_1

    .line 1885
    .end local v10    # "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v12    # "parentLayout":Landroid/view/ViewGroup;
    .end local v13    # "parentRect":Landroid/graphics/Rect;
    .end local v15    # "selEnd":I
    .end local v16    # "selStart":I
    .end local v17    # "startCursorRect":Landroid/graphics/Rect;
    .end local v18    # "textRect":Landroid/graphics/Rect;
    :cond_0
    :goto_0
    return-void

    .line 1816
    .restart local v10    # "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v12    # "parentLayout":Landroid/view/ViewGroup;
    .restart local v13    # "parentRect":Landroid/graphics/Rect;
    .restart local v15    # "selEnd":I
    .restart local v16    # "selStart":I
    .restart local v17    # "startCursorRect":Landroid/graphics/Rect;
    .restart local v18    # "textRect":Landroid/graphics/Rect;
    :cond_1
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v6

    .line 1817
    .local v6, "endCursorRect":Landroid/graphics/Rect;
    if-eqz v6, :cond_0

    .line 1822
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v5

    .line 1824
    .local v5, "deltaY":F
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    sub-float v20, v20, v5

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move-object/from16 v0, v17

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 1825
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    sub-float v20, v20, v5

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 1828
    new-instance v14, Landroid/graphics/Rect;

    invoke-direct {v14}, Landroid/graphics/Rect;-><init>()V

    .line 1829
    .local v14, "rect":Landroid/graphics/Rect;
    const/16 v19, 0x3

    move/from16 v0, v19

    new-array v9, v0, [Landroid/graphics/Rect;

    .line 1831
    .local v9, "listHandleRect":[Landroid/graphics/Rect;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .line 1832
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->height()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v4, v0

    .line 1833
    .local v4, "cursorHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->getMenuHeight()F

    move-result v19

    move/from16 v0, v19

    float-to-int v11, v0

    .line 1834
    .local v11, "menuHeight":I
    const/4 v8, 0x0

    .line 1836
    .local v8, "isHandleButtonShown":Z
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    const/16 v19, 0x3

    move/from16 v0, v19

    if-lt v7, v0, :cond_3

    .line 1856
    if-eqz v8, :cond_7

    .line 1857
    iget v0, v14, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-gt v0, v1, :cond_2

    .line 1859
    iget v0, v14, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/Rect;->bottom:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_6

    .line 1860
    iget v0, v14, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v14, Landroid/graphics/Rect;->top:I

    .line 1861
    iget v0, v14, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    add-int v19, v19, v11

    move/from16 v0, v19

    iput v0, v14, Landroid/graphics/Rect;->bottom:I

    .line 1880
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setRect(Landroid/graphics/Rect;)V

    .line 1881
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->show()V

    goto/16 :goto_0

    .line 1837
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    move-object/from16 v19, v0

    aget-object v19, v19, v7

    if-eqz v19, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    move-object/from16 v19, v0

    aget-object v19, v19, v7

    invoke-virtual/range {v19 .. v19}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v19

    if-nez v19, :cond_4

    .line 1838
    new-instance v19, Landroid/graphics/Rect;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Rect;-><init>()V

    aput-object v19, v9, v7

    .line 1839
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButtonRect:[Landroid/graphics/RectF;

    move-object/from16 v19, v0

    aget-object v19, v19, v7

    aget-object v20, v9, v7

    invoke-virtual/range {v19 .. v20}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 1841
    aget-object v19, v9, v7

    iget v0, v13, Landroid/graphics/Rect;->left:I

    move/from16 v20, v0

    iget v0, v13, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/Rect;->offset(II)V

    .line 1843
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHandleRevese:[Z

    move-object/from16 v19, v0

    aget-boolean v19, v19, v7

    if-eqz v19, :cond_5

    .line 1844
    aget-object v19, v9, v7

    aget-object v20, v9, v7

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    sub-int v20, v20, v11

    add-int/lit8 v20, v20, -0xf

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1845
    aget-object v19, v9, v7

    aget-object v20, v9, v7

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v20, v0

    add-int v20, v20, v4

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1850
    :goto_3
    const/4 v8, 0x1

    .line 1852
    aget-object v19, v9, v7

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 1836
    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    .line 1847
    :cond_5
    aget-object v19, v9, v7

    aget-object v20, v9, v7

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    sub-int v20, v20, v11

    sub-int v20, v20, v4

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/Rect;->top:I

    goto :goto_3

    .line 1863
    :cond_6
    iget v0, v13, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v14, Landroid/graphics/Rect;->top:I

    goto/16 :goto_2

    .line 1868
    :cond_7
    move-object/from16 v14, v17

    .line 1869
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    move/from16 v0, v19

    add-int/lit16 v0, v0, -0x190

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_8

    .line 1870
    const/16 v19, 0x0

    const/16 v20, -0x190

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v14, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    goto/16 :goto_2

    .line 1871
    :cond_8
    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    move/from16 v0, v19

    add-int/lit16 v0, v0, 0x140

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/Rect;->bottom:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_9

    .line 1872
    move-object v14, v6

    .line 1873
    const/16 v19, 0x0

    const/16 v20, 0xc8

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v14, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    goto/16 :goto_2

    .line 1875
    :cond_9
    iget v0, v13, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v14, Landroid/graphics/Rect;->top:I

    goto/16 :goto_2

    .line 1882
    .end local v4    # "cursorHeight":I
    .end local v5    # "deltaY":F
    .end local v6    # "endCursorRect":Landroid/graphics/Rect;
    .end local v7    # "i":I
    .end local v8    # "isHandleButtonShown":Z
    .end local v9    # "listHandleRect":[Landroid/graphics/Rect;
    .end local v10    # "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v11    # "menuHeight":I
    .end local v12    # "parentLayout":Landroid/view/ViewGroup;
    .end local v13    # "parentRect":Landroid/graphics/Rect;
    .end local v14    # "rect":Landroid/graphics/Rect;
    .end local v15    # "selEnd":I
    .end local v16    # "selStart":I
    .end local v17    # "startCursorRect":Landroid/graphics/Rect;
    .end local v18    # "textRect":Landroid/graphics/Rect;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenuVisible:Z

    move/from16 v19, v0

    if-nez v19, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isContextMenuShowing()Z

    move-result v19

    if-eqz v19, :cond_0

    .line 1883
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    goto/16 :goto_0
.end method

.method private updateEditable()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2392
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditableClear:Z

    .line 2393
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2394
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 2395
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v5, v3, v4}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 2397
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v0

    .line 2398
    .local v0, "cursorPos":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 2399
    .local v1, "len":I
    if-ltz v0, :cond_1

    if-gt v0, v1, :cond_1

    .line 2400
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v3}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 2401
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 2404
    .end local v0    # "cursorPos":I
    .end local v1    # "len":I
    :cond_1
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditableClear:Z

    .line 2405
    return-void
.end method

.method private updateSelection()V
    .locals 7

    .prologue
    .line 4646
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v6, "input_method"

    invoke-virtual {v1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 4647
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_1

    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4648
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    .line 4649
    .local v2, "selStart":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v3

    .line 4650
    .local v3, "selEnd":I
    const/4 v4, -0x1

    .line 4651
    .local v4, "candStart":I
    const/4 v5, -0x1

    .line 4652
    .local v5, "candEnd":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 4653
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    move-result v4

    .line 4654
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v5

    :cond_0
    move-object v1, p0

    .line 4657
    invoke-virtual/range {v0 .. v5}, Landroid/view/inputmethod/InputMethodManager;->updateSelection(Landroid/view/View;IIII)V

    .line 4662
    .end local v2    # "selStart":I
    .end local v3    # "selEnd":I
    .end local v4    # "candStart":I
    .end local v5    # "candEnd":I
    :cond_1
    return-void
.end method

.method private updateSettingInfo()V
    .locals 3

    .prologue
    .line 4623
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v2, :cond_1

    .line 4643
    :cond_0
    :goto_0
    return-void

    .line 4626
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v2, :cond_0

    .line 4627
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v0

    .line 4628
    .local v0, "pos":I
    invoke-direct {p0, v0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getSettingInfo(II)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v1

    .line 4630
    .local v1, "textInfo":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    goto :goto_0
.end method


# virtual methods
.method public appendText(Ljava/lang/String;)V
    .locals 4
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 892
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    .line 893
    .local v1, "start":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    .line 895
    .local v0, "end":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v2, v1, v0, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 897
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    .line 899
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 900
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    .line 901
    invoke-virtual {p0, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 902
    return-void

    .line 897
    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public close()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 572
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 573
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideSoftInput()V

    .line 575
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 577
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 578
    .local v2, "layout":Landroid/view/ViewGroup;
    if-eqz v2, :cond_1

    .line 579
    invoke-virtual {v2, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 580
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v3, 0x3

    if-lt v1, v3, :cond_5

    .line 583
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 584
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 585
    invoke-virtual {v2}, Landroid/view/ViewGroup;->invalidate()V

    .line 588
    .end local v1    # "i":I
    :cond_1
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    cmp-long v3, v4, v8

    if-eqz v3, :cond_2

    .line 589
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_finalize(J)V

    .line 590
    iput-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    .line 592
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_2

    .line 593
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 594
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    .line 598
    :cond_2
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    const/4 v3, 0x6

    if-lt v1, v3, :cond_6

    .line 605
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    if-eqz v3, :cond_3

    .line 606
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    .line 607
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->close()V

    .line 610
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v3, :cond_4

    .line 611
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v3}, Landroid/text/Editable;->getFilters()[Landroid/text/InputFilter;

    move-result-object v0

    .line 612
    .local v0, "arrayOfInputFilter":[Landroid/text/InputFilter;
    if-eqz v0, :cond_4

    .line 613
    const/4 v3, 0x0

    aget-object v3, v0, v3

    check-cast v3, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->close()V

    .line 617
    .end local v0    # "arrayOfInputFilter":[Landroid/text/InputFilter;
    :cond_4
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 619
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    .line 620
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .line 621
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 622
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDragText:Landroid/widget/TextView;

    .line 623
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeleteTextPopup:Landroid/widget/TextView;

    .line 624
    return-void

    .line 581
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 580
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 599
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v1

    if-eqz v3, :cond_7

    .line 600
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 601
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aput-object v6, v3, v1

    .line 598
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2038
    invoke-super {p0, p1}, Landroid/view/View;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public enableTextInput(Z)V
    .locals 7
    .param p1, "option"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    .line 1133
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditable:Z

    .line 1135
    if-eqz p1, :cond_0

    .line 1136
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateEditable()V

    .line 1137
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusableInTouchMode(Z)V

    .line 1138
    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setVisibility(I)V

    .line 1139
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusable(Z)V

    .line 1140
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCheckCursorOnScroll(Z)V

    .line 1142
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->requestFocus()Z

    .line 1144
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsShowSoftInputEnable:Z

    if-eqz v1, :cond_1

    .line 1145
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showSoftInput()V

    .line 1153
    :cond_0
    :goto_0
    const/4 v4, 0x2

    .line 1154
    .local v4, "textOnCanvasCommand":I
    new-instance v0, Ljava/lang/Boolean;

    invoke-direct {v0, p1}, Ljava/lang/Boolean;-><init>(Z)V

    .line 1155
    .local v0, "optionObject":Ljava/lang/Boolean;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1156
    .local v5, "tempObjectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1157
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 1158
    return-void

    .line 1146
    .end local v0    # "optionObject":Ljava/lang/Boolean;
    .end local v4    # "textOnCanvasCommand":I
    .end local v5    # "tempObjectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsShowSoftInputEnable:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHardKeyboardHidden:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 1147
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showSoftInput()V

    goto :goto_0

    .line 1149
    :cond_2
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsShowSoftInputEnable:Z

    goto :goto_0
.end method

.method public enableTouch(Z)V
    .locals 0
    .param p1, "touchEnable"    # Z

    .prologue
    .line 5866
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTouchEnable:Z

    .line 5867
    return-void
.end method

.method public fit(Z)V
    .locals 20
    .param p1, "checkCursorPos"    # Z

    .prologue
    .line 645
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const-wide/16 v18, 0x0

    cmp-long v3, v4, v18

    if-nez v3, :cond_1

    .line 750
    :cond_0
    :goto_0
    return-void

    .line 650
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v3, :cond_0

    .line 654
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->adjustTextBoxRect()V

    .line 658
    const/4 v2, 0x0

    .line 659
    .local v2, "content":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v3, :cond_2

    .line 660
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 662
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-nez v3, :cond_3

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 663
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateEditable()V

    .line 664
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 667
    :cond_3
    if-eqz p1, :cond_4

    .line 668
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->adjustTextBox()V

    .line 671
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v9

    .line 672
    .local v9, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v9, :cond_0

    .line 677
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v13

    .line 678
    .local v13, "objectRect":Landroid/graphics/RectF;
    if-eqz v13, :cond_0

    .line 683
    new-instance v15, Landroid/graphics/RectF;

    invoke-direct {v15}, Landroid/graphics/RectF;-><init>()V

    .line 685
    .local v15, "relativeRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 687
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v14

    check-cast v14, Landroid/view/ViewGroup;

    .line 688
    .local v14, "parentLayout":Landroid/view/ViewGroup;
    if-eqz v14, :cond_7

    .line 689
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Landroid/widget/RelativeLayout$LayoutParams;

    .line 691
    .local v12, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget v3, v15, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    iput v3, v12, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 692
    iget v3, v15, Landroid/graphics/RectF;->top:F

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    iput v3, v12, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 693
    invoke-virtual {v14}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget v4, v15, Landroid/graphics/RectF;->left:F

    invoke-virtual {v15}, Landroid/graphics/RectF;->width()F

    move-result v5

    add-float/2addr v4, v5

    sub-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v12, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 695
    invoke-virtual {v14}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, v15, Landroid/graphics/RectF;->top:F

    invoke-virtual {v15}, Landroid/graphics/RectF;->height()F

    move-result v5

    add-float/2addr v4, v5

    sub-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 694
    iput v3, v12, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 696
    invoke-virtual {v15}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    iput v3, v12, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 697
    invoke-virtual {v15}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-int v3, v3

    iput v3, v12, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 699
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_a

    .line 700
    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    move/from16 v16, v0

    .line 701
    .local v16, "width":I
    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v11, v4

    .line 702
    .local v11, "height":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    move/from16 v0, v16

    if-ne v3, v0, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-eq v3, v11, :cond_9

    .line 704
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 706
    if-lez v16, :cond_6

    if-lez v11, :cond_6

    .line 708
    :try_start_0
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v16

    invoke-static {v0, v11, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    .line 709
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_setBitmap(JLandroid/graphics/Bitmap;)Z
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 732
    :cond_6
    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 735
    .end local v11    # "height":I
    .end local v12    # "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v16    # "width":I
    :cond_7
    invoke-virtual {v15}, Landroid/graphics/RectF;->width()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setPivotX(F)V

    .line 736
    invoke-virtual {v15}, Landroid/graphics/RectF;->height()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setPivotY(F)V

    .line 737
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setRotation(F)V

    .line 739
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget v4, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    iget v5, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    const/4 v8, 0x0

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v3, v4, v5, v8, v0}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 740
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    if-eqz v3, :cond_8

    .line 741
    const/4 v6, 0x1

    .line 742
    .local v6, "textOnCanvasCommand":I
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 743
    .local v7, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 744
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const/4 v8, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 747
    .end local v6    # "textOnCanvasCommand":I
    .end local v7    # "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_8
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_update(J)Z

    .line 748
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->restartBlink()V

    .line 749
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    goto/16 :goto_0

    .line 710
    .restart local v11    # "height":I
    .restart local v12    # "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v16    # "width":I
    :catch_0
    move-exception v10

    .line 711
    .local v10, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v10}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_1

    .line 717
    .end local v10    # "e":Ljava/lang/OutOfMemoryError;
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    const/16 v17, 0x0

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v8

    invoke-virtual {v3, v4, v5, v8}, Landroid/graphics/Bitmap;->setPixel(III)V

    goto/16 :goto_1

    .line 720
    .end local v11    # "height":I
    .end local v16    # "width":I
    :cond_a
    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    move/from16 v16, v0

    .line 721
    .restart local v16    # "width":I
    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v11, v4

    .line 722
    .restart local v11    # "height":I
    if-lez v16, :cond_6

    if-lez v11, :cond_6

    .line 724
    :try_start_1
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v16

    invoke-static {v0, v11, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    .line 725
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_setBitmap(JLandroid/graphics/Bitmap;)Z
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 726
    :catch_1
    move-exception v10

    .line 727
    .restart local v10    # "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v10}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto/16 :goto_1
.end method

.method public getCursorPos()I
    .locals 1

    .prologue
    .line 1191
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_0

    .line 1192
    const/4 v0, 0x0

    .line 1195
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v0

    goto :goto_0
.end method

.method public getDefaultHeight()I
    .locals 14

    .prologue
    .line 1457
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v8, :cond_0

    .line 1458
    const/4 v8, 0x0

    .line 1506
    :goto_0
    return v8

    .line 1461
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTopMargin()F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getBottomMargin()F

    move-result v9

    add-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1463
    .local v0, "absoluteHeight":I
    const/4 v3, 0x0

    .line 1464
    .local v3, "fontSize":F
    const/4 v7, 0x0

    .local v7, "startLineIndex":I
    const/4 v2, 0x0

    .line 1465
    .local v2, "endLineIndex":I
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineCount(J)I

    move-result v8

    if-lez v8, :cond_1

    .line 1466
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const/4 v10, 0x0

    invoke-direct {p0, v8, v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineStartIndex(JI)I

    move-result v7

    .line 1467
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const/4 v10, 0x0

    invoke-direct {p0, v8, v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineEndIndex(JI)I

    move-result v2

    .line 1469
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8, v7, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v6

    .line 1470
    .local v6, "sInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    if-eqz v6, :cond_3

    .line 1471
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_8

    .line 1479
    :cond_3
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getParagraph()Ljava/util/ArrayList;

    move-result-object v5

    .line 1480
    .local v5, "pInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    if-eqz v5, :cond_5

    .line 1481
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_4
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_9

    .line 1492
    :cond_5
    if-eqz v5, :cond_6

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-gtz v8, :cond_7

    .line 1493
    :cond_6
    const/4 v8, 0x0

    cmpl-float v8, v3, v8

    if-lez v8, :cond_b

    .line 1494
    int-to-double v8, v0

    float-to-double v10, v3

    const-wide v12, 0x3ff4cccccccccccdL    # 1.3

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    double-to-int v0, v8

    .line 1500
    :cond_7
    :goto_3
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v1

    .line 1501
    .local v1, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-nez v1, :cond_c

    .line 1503
    const/4 v8, 0x0

    goto :goto_0

    .line 1471
    .end local v1    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .end local v5    # "pInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    :cond_8
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 1472
    .local v4, "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    instance-of v8, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v8, :cond_2

    move-object v8, v4

    .line 1473
    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    cmpg-float v8, v3, v8

    if-gez v8, :cond_2

    .line 1474
    check-cast v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    .end local v4    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v3, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    goto :goto_1

    .line 1481
    .restart local v5    # "pInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    :cond_9
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;

    .line 1482
    .local v4, "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    instance-of v8, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    if-eqz v8, :cond_4

    move-object v8, v4

    .line 1483
    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    const/4 v10, 0x1

    if-ne v8, v10, :cond_a

    .line 1484
    int-to-float v8, v0

    check-cast v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    .end local v4    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    iget v10, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    mul-float/2addr v10, v3

    add-float/2addr v8, v10

    float-to-int v0, v8

    .line 1485
    goto :goto_2

    .line 1486
    .restart local v4    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    :cond_a
    int-to-float v8, v0

    check-cast v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    .end local v4    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    iget v10, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    add-float/2addr v8, v10

    float-to-int v0, v8

    goto :goto_2

    .line 1496
    :cond_b
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v8

    float-to-int v0, v8

    goto :goto_3

    .line 1506
    .restart local v1    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    :cond_c
    int-to-float v8, v0

    iget v9, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v8, v9

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v8, v8

    goto/16 :goto_0
.end method

.method public getDefaultWidth()I
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 1429
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v7, :cond_1

    .line 1453
    :cond_0
    :goto_0
    return v6

    .line 1433
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getLeftMargin()F

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRightMargin()F

    move-result v8

    add-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1434
    .local v0, "absoluteWidth":I
    const/4 v2, 0x0

    .line 1436
    .local v2, "fontWidth":F
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getTextLength()I

    move-result v5

    .line 1437
    .local v5, "stringLength":I
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 1438
    .local v4, "rect":Landroid/graphics/RectF;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-lt v3, v5, :cond_2

    .line 1445
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBorderLineWidth:I

    int-to-float v7, v7

    add-float/2addr v2, v7

    .line 1447
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v1

    .line 1448
    .local v1, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-eqz v1, :cond_0

    .line 1453
    int-to-float v6, v0

    add-float/2addr v6, v2

    iget v7, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v6, v6

    goto :goto_0

    .line 1439
    .end local v1    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    :cond_2
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getTextRect(JILandroid/graphics/RectF;)Z

    .line 1440
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v7

    cmpg-float v7, v2, v7

    if-gez v7, :cond_3

    .line 1441
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v2

    .line 1438
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public getMaximumHeight()F
    .locals 2

    .prologue
    .line 1425
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public getMaximumWidth()F
    .locals 24

    .prologue
    .line 1337
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v13

    .line 1338
    .local v13, "objectRect":Landroid/graphics/RectF;
    if-nez v13, :cond_0

    .line 1339
    const/4 v12, 0x0

    .line 1421
    :goto_0
    return v12

    .line 1342
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v7

    .line 1343
    .local v7, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-nez v7, :cond_1

    .line 1345
    const/4 v12, 0x0

    goto :goto_0

    .line 1348
    :cond_1
    new-instance v15, Landroid/graphics/RectF;

    invoke-direct {v15}, Landroid/graphics/RectF;-><init>()V

    .line 1349
    .local v15, "relativeRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 1353
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    .line 1354
    .local v6, "absoluteTextRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v5

    .line 1356
    .local v5, "absoluteRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getLeftMargin()F

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRightMargin()F

    move-result v22

    add-float v4, v21, v22

    .line 1357
    .local v4, "absoluteMargin":F
    const/4 v14, 0x0

    .line 1358
    .local v14, "relativeMargin":F
    const/16 v16, 0x0

    .line 1360
    .local v16, "relativeTextWidth":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v18

    .line 1361
    .local v18, "str":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->isHintTextEnabled()Z

    move-result v21

    if-eqz v21, :cond_5

    if-eqz v18, :cond_2

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v21

    if-nez v21, :cond_5

    .line 1362
    :cond_2
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHintTextWidth(J)F

    move-result v21

    add-float v4, v4, v21

    .line 1380
    :cond_3
    :goto_1
    iget v0, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v21, v0

    mul-float v14, v21, v4

    .line 1382
    move v11, v4

    .line 1383
    .local v11, "lineAbsoluteWidth":F
    move v12, v14

    .line 1384
    .local v12, "lineRelativeWidth":F
    move/from16 v19, v4

    .line 1385
    .local v19, "tempLineAbsoluteWidth":F
    move/from16 v20, v14

    .line 1387
    .local v20, "tempLineRelativeWidth":F
    if-eqz v18, :cond_4

    .line 1388
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v10

    .line 1390
    .local v10, "length":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_2
    if-lt v8, v10, :cond_8

    .line 1419
    .end local v8    # "i":I
    .end local v10    # "length":I
    :cond_4
    :goto_3
    float-to-double v0, v12

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v12, v0

    .line 1421
    goto/16 :goto_0

    .line 1364
    .end local v11    # "lineAbsoluteWidth":F
    .end local v12    # "lineRelativeWidth":F
    .end local v19    # "tempLineAbsoluteWidth":F
    .end local v20    # "tempLineRelativeWidth":F
    :cond_5
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineCount(J)I

    move-result v21

    if-nez v21, :cond_7

    .line 1365
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v22

    .line 1366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v23

    .line 1365
    invoke-virtual/range {v21 .. v23}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v17

    .line 1367
    .local v17, "sInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    if-eqz v17, :cond_3

    .line 1368
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_6
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_3

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 1369
    .local v9, "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    instance-of v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    move/from16 v22, v0

    if-eqz v22, :cond_6

    .line 1370
    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    .end local v9    # "info":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v0, v9, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    move/from16 v21, v0

    add-float v4, v4, v21

    .line 1371
    goto :goto_1

    .line 1376
    .end local v17    # "sInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDefaultPadding:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    add-float v4, v4, v21

    goto/16 :goto_1

    .line 1391
    .restart local v8    # "i":I
    .restart local v10    # "length":I
    .restart local v11    # "lineAbsoluteWidth":F
    .restart local v12    # "lineRelativeWidth":F
    .restart local v19    # "tempLineAbsoluteWidth":F
    .restart local v20    # "tempLineRelativeWidth":F
    :cond_8
    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/String;->charAt(I)C

    move-result v21

    const/16 v22, 0xa

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_b

    .line 1392
    move/from16 v19, v4

    .line 1393
    move/from16 v20, v14

    .line 1410
    :goto_4
    cmpl-float v21, v19, v11

    if-lez v21, :cond_9

    .line 1411
    move/from16 v11, v19

    .line 1413
    :cond_9
    cmpl-float v21, v20, v12

    if-lez v21, :cond_a

    .line 1414
    move/from16 v12, v20

    .line 1390
    :cond_a
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_2

    .line 1395
    :cond_b
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-direct {v0, v1, v2, v8, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getTextRect(JILandroid/graphics/RectF;)Z

    .line 1396
    iget v0, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v21, v0

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v22

    mul-float v16, v21, v22

    .line 1397
    iget v0, v5, Landroid/graphics/RectF;->left:F

    move/from16 v21, v0

    add-float v21, v21, v19

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v22

    add-float v21, v21, v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    move/from16 v22, v0

    .line 1398
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    sub-float v22, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBorderLineWidth:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    sub-float v22, v22, v23

    cmpg-float v21, v21, v22

    if-gez v21, :cond_c

    .line 1399
    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v21

    add-float v19, v19, v21

    .line 1400
    add-float v20, v20, v16

    .line 1401
    goto :goto_4

    .line 1402
    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    sub-float v21, v21, v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBorderLineWidth:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    sub-float v21, v21, v22

    .line 1403
    iget v0, v5, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    .line 1402
    sub-float v11, v21, v22

    .line 1404
    iget v0, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v21, v0

    mul-float v12, v21, v11

    .line 1406
    goto/16 :goto_3
.end method

.method public getObjectText()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    .locals 1

    .prologue
    .line 1061
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    return-object v0
.end method

.method public getPixel(II)I
    .locals 7
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v4, 0x0

    .line 1318
    const/4 v3, 0x0

    .line 1319
    .local v3, "res":I
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_1

    .line 1320
    int-to-float v5, p1

    int-to-float v6, p2

    invoke-direct {p0, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getAbsolutePoint(FF)[F

    move-result-object v2

    .line 1321
    .local v2, "pts":[F
    if-nez v2, :cond_0

    .line 1333
    .end local v2    # "pts":[F
    :goto_0
    return v4

    .line 1326
    .restart local v2    # "pts":[F
    :cond_0
    aget v4, v2, v4

    float-to-int v0, v4

    .line 1327
    .local v0, "nX":I
    const/4 v4, 0x1

    aget v4, v2, v4

    float-to-int v1, v4

    .line 1329
    .local v1, "nY":I
    if-ltz v0, :cond_1

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    if-ge v0, v4, :cond_1

    if-ltz v1, :cond_1

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 1330
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v3

    .end local v0    # "nX":I
    .end local v1    # "nY":I
    .end local v2    # "pts":[F
    :cond_1
    move v4, v3

    .line 1333
    goto :goto_0
.end method

.method public getText(Z)Ljava/lang/String;
    .locals 5
    .param p1, "option"    # Z

    .prologue
    const/4 v3, 0x0

    .line 943
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v4, :cond_2

    .line 944
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v4}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    .line 945
    .local v1, "start":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v4}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    .line 947
    .local v0, "end":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 949
    .local v2, "str":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 950
    if-eqz v2, :cond_1

    if-gt v1, v0, :cond_1

    .line 951
    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 959
    .end local v0    # "end":I
    .end local v1    # "start":I
    .end local v2    # "str":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v2

    .restart local v0    # "end":I
    .restart local v1    # "start":I
    .restart local v2    # "str":Ljava/lang/String;
    :cond_1
    move-object v2, v3

    .line 953
    goto :goto_0

    .end local v0    # "end":I
    .end local v1    # "start":I
    .end local v2    # "str":Ljava/lang/String;
    :cond_2
    move-object v2, v3

    .line 959
    goto :goto_0
.end method

.method public getTextLimit()I
    .locals 1

    .prologue
    .line 1314
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    return v0
.end method

.method public hideSoftInput()V
    .locals 3

    .prologue
    .line 1124
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1125
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1126
    return-void
.end method

.method public hideTextBox()V
    .locals 1

    .prologue
    .line 887
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mFirstDraw:Z

    .line 888
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setVisibility(I)V

    .line 889
    return-void
.end method

.method public isAccessoryKeyboardState()I
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 1065
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v6, "input_method"

    invoke-virtual {v4, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    .line 1066
    .local v2, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 1068
    .local v0, "cls":Ljava/lang/Class;
    :try_start_0
    const-string v4, "isAccessoryKeyboardState"

    const/4 v6, 0x0

    invoke-virtual {v0, v4, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v3

    .line 1070
    .local v3, "mtd":Ljava/lang/reflect/Method;
    const/4 v4, 0x0

    :try_start_1
    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v4

    .line 1082
    .end local v3    # "mtd":Ljava/lang/reflect/Method;
    :goto_0
    return v4

    .line 1071
    .restart local v3    # "mtd":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v1

    .line 1072
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    .end local v3    # "mtd":Ljava/lang/reflect/Method;
    :goto_1
    move v4, v5

    .line 1082
    goto :goto_0

    .line 1073
    .restart local v3    # "mtd":Ljava/lang/reflect/Method;
    :catch_1
    move-exception v1

    .line 1074
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 1078
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    .end local v3    # "mtd":Ljava/lang/reflect/Method;
    :catch_2
    move-exception v1

    .line 1079
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v1}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1

    .line 1075
    .end local v1    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v3    # "mtd":Ljava/lang/reflect/Method;
    :catch_3
    move-exception v1

    .line 1076
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1
.end method

.method public isContextMenuShowing()Z
    .locals 1

    .prologue
    .line 1179
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    if-eqz v0, :cond_0

    .line 1180
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->isShowing()Z

    move-result v0

    .line 1183
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isContextMenuVisible()Z
    .locals 1

    .prologue
    .line 1013
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenuVisible:Z

    return v0
.end method

.method protected isSelectByKey()Z
    .locals 1

    .prologue
    .line 1788
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    return v0
.end method

.method public isTextInputable()Z
    .locals 1

    .prologue
    .line 1784
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v0

    return v0
.end method

.method public isTouchEnabled()Z
    .locals 1

    .prologue
    .line 5877
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTouchEnable:Z

    return v0
.end method

.method public isViewModeEnabled()Z
    .locals 1

    .prologue
    .line 1175
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    return v0
.end method

.method public measureText()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 754
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-nez v8, :cond_1

    .line 880
    :cond_0
    :goto_0
    return-void

    .line 759
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v8, :cond_0

    .line 763
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/RectF;->width()F

    move-result v10

    float-to-int v10, v10

    invoke-direct {p0, v8, v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_measure(JI)Z

    .line 765
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getGravity()I

    move-result v8

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDeltaY(I)F

    move-result v2

    .line 767
    .local v2, "diffY":F
    const/4 v7, 0x0

    .line 769
    .local v7, "stringLength":I
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v6

    .line 770
    .local v6, "str":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 771
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    .line 774
    :cond_2
    if-lez v7, :cond_8

    .line 775
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineCount(J)I

    move-result v8

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    .line 776
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    new-array v8, v8, [Landroid/graphics/PointF;

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    .line 777
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    .line 778
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    .line 779
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    new-array v8, v8, [Z

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineL2R:[Z

    .line 780
    new-array v8, v7, [Landroid/graphics/RectF;

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    .line 781
    new-array v8, v7, [F

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextSize:[F

    .line 782
    new-array v8, v7, [Z

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextItalic:[Z

    .line 784
    const/4 v1, 0x0

    .line 785
    .local v1, "cnt":I
    const/4 v1, 0x0

    :goto_1
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-lt v1, v8, :cond_3

    .line 826
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9, v12}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineStartIndex(JI)I

    move-result v8

    const/4 v9, -0x1

    if-ne v8, v9, :cond_7

    .line 827
    iput-boolean v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFirstCharLF:Z

    .line 832
    :goto_2
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v8

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeltaY:F

    .line 834
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v7, :cond_0

    .line 835
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    new-instance v9, Landroid/graphics/RectF;

    invoke-direct {v9}, Landroid/graphics/RectF;-><init>()V

    aput-object v9, v8, v1

    .line 836
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v10, v10, v1

    invoke-direct {p0, v8, v9, v1, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getTextRect(JILandroid/graphics/RectF;)Z

    .line 837
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v8, v8, v1

    invoke-virtual {v8, v14, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 838
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextItalic:[Z

    aput-boolean v12, v8, v1

    .line 834
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 786
    :cond_3
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    new-instance v9, Landroid/graphics/PointF;

    invoke-direct {v9}, Landroid/graphics/PointF;-><init>()V

    aput-object v9, v8, v1

    .line 787
    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    aget-object v10, v10, v1

    invoke-direct {p0, v8, v9, v1, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLinePosition(JILandroid/graphics/PointF;)Z

    .line 788
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    aget-object v8, v8, v1

    invoke-virtual {v8, v14, v2}, Landroid/graphics/PointF;->offset(FF)V

    .line 789
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v10, v11, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineStartIndex(JI)I

    move-result v9

    aput v9, v8, v1

    .line 790
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-direct {p0, v10, v11, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getLineEndIndex(JI)I

    move-result v9

    aput v9, v8, v1

    .line 793
    const/4 v5, 0x0

    .line 794
    .local v5, "stop":Z
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineL2R:[Z

    aput-boolean v13, v8, v1

    .line 795
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v8, v8, v1

    if-ltz v8, :cond_5

    .line 796
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v4, v8, v1

    .line 797
    .local v4, "index":I
    :cond_4
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v8, v8, v1

    if-le v4, v8, :cond_6

    .line 785
    .end local v4    # "index":I
    :cond_5
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 798
    .restart local v4    # "index":I
    :cond_6
    invoke-virtual {v6, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 799
    .local v0, "c":C
    invoke-static {v0}, Ljava/lang/Character;->getDirectionality(C)B

    move-result v3

    .line 800
    .local v3, "direction":I
    sparse-switch v3, :sswitch_data_0

    .line 815
    add-int/lit8 v4, v4, 0x1

    .line 818
    :goto_5
    if-eqz v5, :cond_4

    goto :goto_4

    .line 804
    :sswitch_0
    const/4 v5, 0x1

    .line 805
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineL2R:[Z

    aput-boolean v13, v8, v1

    goto :goto_5

    .line 811
    :sswitch_1
    const/4 v5, 0x1

    .line 812
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineL2R:[Z

    aput-boolean v12, v8, v1

    goto :goto_5

    .line 829
    .end local v0    # "c":C
    .end local v3    # "direction":I
    .end local v4    # "index":I
    .end local v5    # "stop":Z
    :cond_7
    iput-boolean v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFirstCharLF:Z

    goto/16 :goto_2

    .line 876
    .end local v1    # "cnt":I
    :cond_8
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initMeasureInfo()V

    goto/16 :goto_0

    .line 800
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0xe -> :sswitch_0
        0xf -> :sswitch_0
        0x10 -> :sswitch_1
        0x11 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCheckIsTextEditor()Z
    .locals 1

    .prologue
    .line 2283
    const/4 v0, 0x1

    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2004
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCurrentOrientation:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_1

    .line 2005
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCheckCursorOnScroll(Z)V

    .line 2007
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setDirty()V

    .line 2009
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2010
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->reset()V

    .line 2013
    :cond_0
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCurrentOrientation:I

    .line 2015
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideDeleteTextPopup()V

    .line 2018
    :cond_1
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHardKeyboardConnected:Z

    .line 2019
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHardKeyboardHidden:I

    iget v1, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-eq v0, v1, :cond_2

    .line 2020
    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHardKeyboardHidden:I

    .line 2021
    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v0, v2, :cond_3

    .line 2023
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHardKeyboardConnected:Z

    .line 2024
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideSoftInput()V

    .line 2032
    :cond_2
    :goto_0
    invoke-super {p0, p1}, Landroid/view/View;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2033
    return-void

    .line 2025
    :cond_3
    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 2027
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHardKeyboardConnected:Z

    .line 2028
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showSoftInput()V

    goto :goto_0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 4
    .param p1, "outAttrs"    # Landroid/view/inputmethod/EditorInfo;

    .prologue
    .line 2288
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getIMEActionType()I

    move-result v0

    .line 2289
    .local v0, "imeOption":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTextInputType()I

    move-result v1

    .line 2291
    .local v1, "inputType":I
    packed-switch v0, :pswitch_data_0

    .line 2314
    const/4 v0, 0x1

    .line 2318
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2335
    const/4 v1, 0x1

    .line 2339
    :goto_1
    const/4 v2, 0x0

    iput-object v2, p1, Landroid/view/inputmethod/EditorInfo;->actionLabel:Ljava/lang/CharSequence;

    .line 2340
    const-string v2, "SPenSDK"

    iput-object v2, p1, Landroid/view/inputmethod/EditorInfo;->label:Ljava/lang/CharSequence;

    .line 2341
    iget v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const/high16 v3, 0x10000000

    or-int/2addr v3, v0

    or-int/2addr v2, v3

    iput v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 2344
    or-int/lit16 v2, v1, 0x4000

    iput v2, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 2346
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mInputConnection:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;

    if-nez v2, :cond_0

    .line 2347
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;

    invoke-direct {v2, p0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/view/View;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mInputConnection:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;

    .line 2350
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    iput v2, p1, Landroid/view/inputmethod/EditorInfo;->initialSelStart:I

    .line 2351
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v2

    iput v2, p1, Landroid/view/inputmethod/EditorInfo;->initialSelEnd:I

    .line 2353
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mInputConnection:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;

    return-object v2

    .line 2293
    :pswitch_0
    const/4 v0, 0x6

    .line 2294
    goto :goto_0

    .line 2296
    :pswitch_1
    const/4 v0, 0x2

    .line 2297
    goto :goto_0

    .line 2299
    :pswitch_2
    const/4 v0, 0x5

    .line 2300
    goto :goto_0

    .line 2302
    :pswitch_3
    const/4 v0, 0x1

    .line 2303
    goto :goto_0

    .line 2305
    :pswitch_4
    const/4 v0, 0x3

    .line 2306
    goto :goto_0

    .line 2308
    :pswitch_5
    const/4 v0, 0x4

    .line 2309
    goto :goto_0

    .line 2311
    :pswitch_6
    const/4 v0, 0x0

    .line 2312
    goto :goto_0

    .line 2320
    :pswitch_7
    const/4 v1, 0x4

    .line 2321
    goto :goto_1

    .line 2323
    :pswitch_8
    const/4 v1, 0x0

    .line 2324
    goto :goto_1

    .line 2326
    :pswitch_9
    const/4 v1, 0x2

    .line 2327
    goto :goto_1

    .line 2329
    :pswitch_a
    const/4 v1, 0x3

    .line 2330
    goto :goto_1

    .line 2332
    :pswitch_b
    const/4 v1, 0x1

    .line 2333
    goto :goto_1

    .line 2291
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_2
    .end packed-switch

    .line 2318
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_8
        :pswitch_b
        :pswitch_9
        :pswitch_a
        :pswitch_7
    .end packed-switch
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 1923
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->stopBlink()V

    .line 1925
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTyping:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->stopInput()V

    .line 1927
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 1928
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    .line 1897
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mFirstDraw:Z

    if-eqz v0, :cond_0

    .line 1898
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mFirstDraw:Z

    .line 1899
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onVisibleUpdated(Z)V

    .line 1902
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 1919
    :cond_1
    :goto_0
    return-void

    .line 1906
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onDrawSelect(Landroid/graphics/Canvas;)V

    .line 1908
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 1910
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onDrawHandle()V

    .line 1912
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onDrawCursor(Landroid/graphics/Canvas;)V

    .line 1914
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onDrawScrollBar(Landroid/graphics/Canvas;)V

    .line 1916
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextmenu()V

    .line 1918
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 7
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    const/16 v2, 0x8

    const/4 v6, 0x0

    .line 1932
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v0, :cond_0

    .line 1933
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onFocusChanged(Z)V

    .line 1936
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mShowCursor:J

    .line 1937
    if-eqz p1, :cond_2

    .line 1938
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->startBlink()V

    .line 1940
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v0, :cond_1

    .line 1941
    const/4 v4, 0x0

    .line 1942
    .local v4, "textOnCanvasCommand":I
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1943
    .local v5, "tempObjectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1944
    const-wide/16 v2, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 1958
    .end local v4    # "textOnCanvasCommand":I
    .end local v5    # "tempObjectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_1
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 1959
    return-void

    .line 1948
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->stopBlink()V

    .line 1949
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTyping:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->stopInput()V

    .line 1951
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideCursorHandle()V

    .line 1952
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v0, v0, v6

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1953
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1955
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideDeleteTextPopup()V

    goto :goto_0
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 6106
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_0

    .line 6107
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 6117
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/view/View;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    return v2

    .line 6109
    :pswitch_0
    const/16 v2, 0x9

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    .line 6110
    .local v0, "vscroll":F
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_0

    .line 6111
    const/high16 v2, 0x41200000    # 10.0f

    mul-float/2addr v2, v0

    neg-float v1, v2

    .line 6112
    .local v1, "y":F
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->scrollTextbox(F)V

    goto :goto_0

    .line 6107
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 11
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v7, 0x0

    const/4 v10, -0x1

    const/4 v8, 0x1

    .line 2045
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v9}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v5

    .line 2046
    .local v5, "start":I
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v9}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    .line 2048
    .local v1, "end":I
    sparse-switch p1, :sswitch_data_0

    .line 2174
    :goto_0
    const/16 v7, 0x43

    if-ne p1, v7, :cond_0

    .line 2175
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    .line 2176
    iput v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    .line 2177
    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z

    if-nez v7, :cond_0

    .line 2178
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v7}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v7

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 2179
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->lastText:Ljava/lang/String;

    .line 2180
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z

    .line 2184
    :cond_0
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    if-eqz v7, :cond_17

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v7, :cond_17

    .line 2185
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v7, p0, v9, p1, p2}, Landroid/text/method/KeyListener;->onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    move-result v7

    if-eqz v7, :cond_17

    move v7, v8

    .line 2190
    :cond_1
    :goto_1
    return v7

    .line 2054
    :sswitch_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v7

    if-nez v7, :cond_2

    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isCtrlPressed:Z

    if-eqz v7, :cond_3

    :cond_2
    const/16 v7, 0x7a

    if-ne p1, v7, :cond_3

    .line 2055
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->restartBlink()V

    .line 2056
    const/16 v7, 0x5c

    invoke-direct {p0, v7, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onMoveByKey(ILandroid/view/KeyEvent;)V

    move v7, v8

    .line 2057
    goto :goto_1

    .line 2060
    :cond_3
    :sswitch_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v7

    if-nez v7, :cond_4

    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isCtrlPressed:Z

    if-eqz v7, :cond_5

    :cond_4
    const/16 v7, 0x7b

    if-ne p1, v7, :cond_5

    .line 2061
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->restartBlink()V

    .line 2062
    const/16 v7, 0x5d

    invoke-direct {p0, v7, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onMoveByKey(ILandroid/view/KeyEvent;)V

    move v7, v8

    .line 2063
    goto :goto_1

    .line 2067
    :cond_5
    :sswitch_2
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->restartBlink()V

    .line 2068
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onMoveByKey(ILandroid/view/KeyEvent;)V

    move v7, v8

    .line 2069
    goto :goto_1

    .line 2074
    :sswitch_3
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    goto :goto_0

    .line 2079
    :sswitch_4
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    .line 2080
    iput v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    .line 2081
    if-ne v5, v1, :cond_11

    .line 2082
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v9

    if-nez v9, :cond_6

    iget-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isAltPressed:Z

    if-eqz v9, :cond_10

    .line 2083
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorPos()I

    move-result v4

    .local v4, "pos":I
    const/4 v2, 0x0

    .line 2084
    .local v2, "line":I
    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 2085
    .local v0, "cursorRect":Landroid/graphics/Rect;
    if-eqz v0, :cond_b

    .line 2086
    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v9

    int-to-float v9, v9

    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v2

    .line 2087
    if-ltz v2, :cond_1

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-ge v2, v9, :cond_1

    .line 2090
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-eqz v9, :cond_b

    .line 2091
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v9, v9, v2

    if-ltz v9, :cond_7

    .line 2092
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v10, v10, v2

    aget-object v9, v9, v10

    iget v9, v9, Landroid/graphics/RectF;->left:F

    invoke-direct {p0, v2, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v5

    .line 2094
    :cond_7
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-eqz v9, :cond_b

    .line 2095
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v9, v9, v2

    if-ltz v9, :cond_8

    .line 2096
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v10, v10, v2

    aget-object v9, v9, v10

    iget v9, v9, Landroid/graphics/RectF;->right:F

    invoke-direct {p0, v2, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v1

    .line 2099
    :cond_8
    add-int/lit8 v9, v2, 0x1

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-ge v9, v10, :cond_a

    .line 2100
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v6

    .line 2101
    .local v6, "str":Ljava/lang/String;
    if-eqz v6, :cond_a

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v1, v9, :cond_a

    .line 2102
    invoke-virtual {v6, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 2103
    .local v3, "oneChar":C
    const/16 v9, 0xa

    if-eq v3, v9, :cond_9

    const/16 v9, 0xd

    if-ne v3, v9, :cond_a

    .line 2104
    :cond_9
    add-int/lit8 v1, v1, 0x1

    .line 2109
    .end local v3    # "oneChar":C
    .end local v6    # "str":Ljava/lang/String;
    :cond_a
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    .line 2113
    :cond_b
    iget-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z

    if-nez v9, :cond_c

    .line 2114
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->lastText:Ljava/lang/String;

    .line 2115
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z

    .line 2117
    :cond_c
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v9, :cond_d

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-le v1, v9, :cond_d

    .line 2118
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v1

    .line 2120
    :cond_d
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v9, :cond_e

    .line 2121
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v9, v5, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 2123
    :cond_e
    invoke-virtual {p0, v5, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .end local v0    # "cursorRect":Landroid/graphics/Rect;
    .end local v2    # "line":I
    .end local v4    # "pos":I
    :cond_f
    :goto_2
    move v7, v8

    .line 2141
    goto/16 :goto_1

    .line 2124
    :cond_10
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getTextLength()I

    move-result v7

    if-ge v5, v7, :cond_f

    .line 2125
    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 2126
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v7, :cond_f

    .line 2127
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    add-int/lit8 v9, v5, 0x1

    invoke-interface {v7, v5, v9}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_2

    .line 2131
    :cond_11
    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z

    if-nez v7, :cond_12

    .line 2132
    if-ge v5, v1, :cond_13

    move v7, v1

    :goto_3
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 2133
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v7}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->lastText:Ljava/lang/String;

    .line 2134
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z

    .line 2136
    :cond_12
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v7, :cond_f

    .line 2137
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-ge v5, v1, :cond_14

    move v9, v5

    :goto_4
    if-ge v5, v1, :cond_15

    move v7, v1

    :goto_5
    invoke-interface {v10, v9, v7}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_2

    :cond_13
    move v7, v5

    .line 2132
    goto :goto_3

    :cond_14
    move v9, v1

    .line 2137
    goto :goto_4

    :cond_15
    move v7, v5

    goto :goto_5

    .line 2153
    :sswitch_5
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isNumLockOn()Z

    move-result v7

    if-eqz v7, :cond_16

    .line 2154
    add-int/lit16 v7, p1, -0x90

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->appendText(Ljava/lang/String;)V

    :goto_6
    move v7, v8

    .line 2158
    goto/16 :goto_1

    .line 2156
    :cond_16
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onMoveByKey(ILandroid/view/KeyEvent;)V

    goto :goto_6

    .line 2162
    :sswitch_6
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isAltPressed:Z

    goto/16 :goto_0

    .line 2166
    :sswitch_7
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isCtrlPressed:Z

    goto/16 :goto_0

    .line 2170
    :sswitch_8
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    goto/16 :goto_0

    .line 2190
    :cond_17
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v7

    goto/16 :goto_1

    .line 2048
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x39 -> :sswitch_6
        0x3a -> :sswitch_6
        0x3b -> :sswitch_8
        0x3c -> :sswitch_8
        0x3d -> :sswitch_3
        0x3e -> :sswitch_3
        0x42 -> :sswitch_3
        0x5c -> :sswitch_2
        0x5d -> :sswitch_2
        0x70 -> :sswitch_4
        0x71 -> :sswitch_7
        0x72 -> :sswitch_7
        0x7a -> :sswitch_0
        0x7b -> :sswitch_1
        0x90 -> :sswitch_5
        0x91 -> :sswitch_5
        0x92 -> :sswitch_5
        0x93 -> :sswitch_5
        0x94 -> :sswitch_5
        0x95 -> :sswitch_5
        0x96 -> :sswitch_5
        0x97 -> :sswitch_5
        0x98 -> :sswitch_5
        0x99 -> :sswitch_5
    .end sparse-switch
.end method

.method public onKeyShortcut(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v5, 0x0

    .line 2241
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v3

    .line 2243
    .local v3, "state":I
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v6

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isCtrlPressed:Z

    if-nez v6, :cond_0

    or-int/lit16 v6, v3, 0x1000

    if-eqz v6, :cond_1

    .line 2244
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "clipboard"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 2245
    .local v0, "clipboard":Landroid/content/ClipboardManager;
    if-nez v0, :cond_2

    .line 2278
    .end local v0    # "clipboard":Landroid/content/ClipboardManager;
    :cond_1
    :goto_0
    return v5

    .line 2249
    .restart local v0    # "clipboard":Landroid/content/ClipboardManager;
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v6}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    .line 2250
    .local v2, "start":I
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v6}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    .line 2251
    .local v1, "end":I
    if-le v2, v1, :cond_3

    .line 2252
    move v4, v2

    .line 2253
    .local v4, "temp":I
    move v2, v1

    .line 2254
    move v1, v4

    .line 2257
    .end local v4    # "temp":I
    :cond_3
    const/16 v6, 0x36

    if-eq p1, v6, :cond_4

    .line 2258
    const/4 v6, -0x1

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndCurCut:I

    .line 2260
    :cond_4
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 2262
    :sswitch_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->selectAll()Z

    .line 2263
    const/4 v5, 0x1

    goto :goto_0

    .line 2265
    :sswitch_1
    invoke-direct {p0, v0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->copy(Landroid/content/ClipboardManager;II)Z

    move-result v5

    goto :goto_0

    .line 2267
    :sswitch_2
    invoke-direct {p0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->delete(II)Z

    move-result v5

    goto :goto_0

    .line 2269
    :sswitch_3
    invoke-direct {p0, v0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->cut(Landroid/content/ClipboardManager;II)Z

    move-result v5

    goto :goto_0

    .line 2271
    :sswitch_4
    invoke-direct {p0, v0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->paste(Landroid/content/ClipboardManager;II)Z

    move-result v5

    goto :goto_0

    .line 2273
    :sswitch_5
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->undo()Z

    move-result v5

    goto :goto_0

    .line 2275
    :sswitch_6
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->redo()Z

    move-result v5

    goto :goto_0

    .line 2260
    nop

    :sswitch_data_0
    .sparse-switch
        0x1d -> :sswitch_0
        0x1f -> :sswitch_1
        0x20 -> :sswitch_2
        0x32 -> :sswitch_4
        0x34 -> :sswitch_3
        0x35 -> :sswitch_6
        0x36 -> :sswitch_5
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    .line 2196
    sparse-switch p1, :sswitch_data_0

    .line 2229
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    if-eqz v2, :cond_1

    .line 2230
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v2, p0, v3, p1, p2}, Landroid/text/method/KeyListener;->onKeyUp(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2231
    const/4 v2, 0x1

    .line 2235
    :goto_1
    :sswitch_0
    return v2

    .line 2207
    :sswitch_1
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isAltPressed:Z

    goto :goto_0

    .line 2211
    :sswitch_2
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isCtrlPressed:Z

    goto :goto_0

    .line 2215
    :sswitch_3
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShiftPressed:Z

    .line 2216
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSwipeOnKeyboard:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsHardKeyboardConnected:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v3, :cond_0

    .line 2217
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSelectByKey:Z

    .line 2218
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showCursorHandle()V

    .line 2219
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setDirty()V

    .line 2220
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v3}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    .line 2221
    .local v1, "start":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v3}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    .line 2222
    .local v0, "end":I
    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onSelectionChanged(II)V

    .line 2223
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    .line 2224
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSwipeOnKeyboard:Z

    goto :goto_0

    .line 2235
    .end local v0    # "end":I
    .end local v1    # "start":I
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_1

    .line 2196
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x39 -> :sswitch_1
        0x3a -> :sswitch_1
        0x3b -> :sswitch_3
        0x3c -> :sswitch_3
        0x3d -> :sswitch_0
        0x42 -> :sswitch_0
        0x43 -> :sswitch_0
        0x71 -> :sswitch_2
        0x72 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onObjectChanged()V
    .locals 2

    .prologue
    .line 1962
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v0, :cond_0

    .line 1963
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onObjectChanged(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    .line 1965
    :cond_0
    return-void
.end method

.method public onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 2370
    invoke-super {p0, p1}, Landroid/view/View;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 2372
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2373
    return-void
.end method

.method protected onRequestScroll(FF)V
    .locals 2
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    .line 1986
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v0, :cond_0

    .line 1987
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onRequestScroll(FF)V

    .line 1988
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onObjectChanged(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    .line 2000
    :cond_0
    return-void
.end method

.method protected onSelectionChanged(II)V
    .locals 1
    .param p1, "selStart"    # I
    .param p2, "selEnd"    # I

    .prologue
    .line 3056
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-nez v0, :cond_1

    .line 3066
    :cond_0
    :goto_0
    return-void

    .line 3060
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onSelectionChanged(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3064
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 3065
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setDirty()V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 22
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 6211
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTouchEnable:Z

    move/from16 v18, v0

    if-nez v18, :cond_0

    .line 6212
    const/16 v18, 0x0

    .line 6532
    :goto_0
    return v18

    .line 6215
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideDeleteTextPopup()V

    .line 6219
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v18

    if-nez v18, :cond_2

    .line 6220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1

    .line 6221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    .line 6224
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    move/from16 v18, v0

    if-eqz v18, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v18

    if-nez v18, :cond_2

    .line 6225
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->bringToFront()V

    .line 6226
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->requestFocus()Z

    .line 6229
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v18

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 6230
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    if-nez v18, :cond_3

    .line 6231
    const/16 v18, 0x0

    goto :goto_0

    .line 6233
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v10

    .line 6234
    .local v10, "objectRect":Landroid/graphics/RectF;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v4

    .line 6235
    .local v4, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-nez v4, :cond_4

    .line 6237
    const/16 v18, 0x0

    goto :goto_0

    .line 6239
    :cond_4
    new-instance v13, Landroid/graphics/RectF;

    invoke-direct {v13}, Landroid/graphics/RectF;-><init>()V

    .line 6240
    .local v13, "relativeRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    invoke-direct {v0, v13, v10, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 6243
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v11, v0, [F

    const/16 v18, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v19

    aput v19, v11, v18

    const/16 v18, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v19

    aput v19, v11, v18

    .line 6244
    .local v11, "pts":[F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 6245
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 6246
    const/16 v18, 0x1

    aget v19, v11, v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v20

    add-float v19, v19, v20

    aput v19, v11, v18

    .line 6248
    const/4 v8, 0x0

    .local v8, "line":I
    const/4 v7, 0x0

    .local v7, "index":I
    const/4 v15, 0x0

    .line 6249
    .local v15, "start":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v18

    packed-switch v18, :pswitch_data_0

    .line 6286
    :cond_5
    :goto_1
    if-ne v7, v15, :cond_6

    .line 6287
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 6288
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showCursorHandle()V

    .line 6289
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    .line 6290
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->reset()V

    .line 6291
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    .line 6294
    :cond_6
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 6251
    :pswitch_0
    const/16 v18, 0x1

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6252
    const/16 v18, 0x0

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6253
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v7, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto :goto_1

    .line 6256
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v15

    .line 6257
    const/16 v18, 0x1

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6258
    const/16 v18, 0x0

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6259
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->last_index:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-eq v0, v7, :cond_5

    .line 6260
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v15, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 6261
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    .line 6262
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->checkForVerticalScroll(I)F

    .line 6263
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->last_index:I

    .line 6265
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isHapticEnabled()Z

    move-result v18

    if-eqz v18, :cond_5

    .line 6266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    move-object/from16 v18, v0

    const/16 v19, 0x16

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->performHapticFeedback(I)Z

    goto/16 :goto_1

    .line 6271
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v15

    .line 6272
    const/16 v18, 0x1

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6273
    const/16 v18, 0x0

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6274
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->last_index:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-eq v0, v7, :cond_7

    .line 6275
    invoke-static {v15, v7}, Ljava/lang/Math;->min(II)I

    move-result v18

    invoke-static {v15, v7}, Ljava/lang/Math;->max(II)I

    move-result v19

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 6276
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    .line 6277
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->checkForVerticalScroll(I)F

    .line 6278
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->last_index:I

    goto/16 :goto_1

    .line 6279
    :cond_7
    if-ge v7, v15, :cond_5

    .line 6280
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v7, v15, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 6281
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    .line 6282
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->checkForVerticalScroll(I)F

    goto/16 :goto_1

    .line 6295
    .end local v4    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .end local v7    # "index":I
    .end local v8    # "line":I
    .end local v10    # "objectRect":Landroid/graphics/RectF;
    .end local v11    # "pts":[F
    .end local v13    # "relativeRect":Landroid/graphics/RectF;
    .end local v15    # "start":I
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    move/from16 v18, v0

    if-eqz v18, :cond_b

    .line 6296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    if-nez v18, :cond_9

    .line 6297
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 6299
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v10

    .line 6301
    .restart local v10    # "objectRect":Landroid/graphics/RectF;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v4

    .line 6302
    .restart local v4    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-nez v4, :cond_a

    .line 6304
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 6307
    :cond_a
    new-instance v13, Landroid/graphics/RectF;

    invoke-direct {v13}, Landroid/graphics/RectF;-><init>()V

    .line 6308
    .restart local v13    # "relativeRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    invoke-direct {v0, v13, v10, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 6310
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v11, v0, [F

    const/16 v18, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v19

    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    sub-float v19, v19, v20

    aput v19, v11, v18

    const/16 v18, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v19

    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    sub-float v19, v19, v20

    aput v19, v11, v18

    .line 6311
    .restart local v11    # "pts":[F
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getInvMatrix()Landroid/graphics/Matrix;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 6313
    const/4 v8, 0x0

    .restart local v8    # "line":I
    const/4 v7, 0x0

    .restart local v7    # "index":I
    const/4 v15, 0x0

    .line 6315
    .restart local v15    # "start":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v18

    packed-switch v18, :pswitch_data_1

    .line 6341
    :goto_2
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 6317
    :pswitch_3
    const/16 v18, 0x1

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6318
    const/16 v18, 0x0

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6319
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v7, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto :goto_2

    .line 6323
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v15

    .line 6324
    const/16 v18, 0x1

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6325
    const/16 v18, 0x0

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6327
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v15, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto :goto_2

    .line 6331
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v15

    .line 6332
    const/16 v18, 0x1

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6333
    const/16 v18, 0x0

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6335
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v15, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 6337
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->removeText()V

    goto :goto_2

    .line 6342
    .end local v4    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .end local v7    # "index":I
    .end local v8    # "line":I
    .end local v10    # "objectRect":Landroid/graphics/RectF;
    .end local v11    # "pts":[F
    .end local v13    # "relativeRect":Landroid/graphics/RectF;
    .end local v15    # "start":I
    :cond_b
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v18

    const/16 v19, 0x3

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_10

    .line 6343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    if-nez v18, :cond_c

    .line 6344
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 6346
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v10

    .line 6348
    .restart local v10    # "objectRect":Landroid/graphics/RectF;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v4

    .line 6349
    .restart local v4    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    if-nez v4, :cond_d

    .line 6351
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 6354
    :cond_d
    new-instance v13, Landroid/graphics/RectF;

    invoke-direct {v13}, Landroid/graphics/RectF;-><init>()V

    .line 6355
    .restart local v13    # "relativeRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    invoke-direct {v0, v13, v10, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 6358
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v11, v0, [F

    const/16 v18, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v19

    aput v19, v11, v18

    const/16 v18, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v19

    aput v19, v11, v18

    .line 6359
    .restart local v11    # "pts":[F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 6360
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 6361
    const/16 v18, 0x1

    aget v19, v11, v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v20

    add-float v19, v19, v20

    aput v19, v11, v18

    .line 6363
    const/4 v8, 0x0

    .restart local v8    # "line":I
    const/4 v7, 0x0

    .restart local v7    # "index":I
    const/4 v15, 0x0

    .line 6365
    .restart local v15    # "start":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v18

    packed-switch v18, :pswitch_data_2

    .line 6532
    .end local v4    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .end local v7    # "index":I
    .end local v8    # "line":I
    .end local v10    # "objectRect":Landroid/graphics/RectF;
    .end local v11    # "pts":[F
    .end local v13    # "relativeRect":Landroid/graphics/RectF;
    .end local v15    # "start":I
    :cond_e
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mGestureDetector:Landroid/view/GestureDetector;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v18

    goto/16 :goto_0

    .line 6367
    .restart local v4    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .restart local v7    # "index":I
    .restart local v8    # "line":I
    .restart local v10    # "objectRect":Landroid/graphics/RectF;
    .restart local v11    # "pts":[F
    .restart local v13    # "relativeRect":Landroid/graphics/RectF;
    .restart local v15    # "start":I
    :pswitch_6
    const/16 v18, 0x1

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6368
    const/16 v18, 0x0

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6369
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v7, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto :goto_3

    .line 6373
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v15

    .line 6374
    const/16 v18, 0x1

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6375
    const/16 v18, 0x0

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6376
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->last_index:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-eq v0, v7, :cond_e

    .line 6377
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v15, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 6378
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->last_index:I

    .line 6380
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isHapticEnabled()Z

    move-result v18

    if-eqz v18, :cond_e

    .line 6381
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    move-object/from16 v18, v0

    const/16 v19, 0x16

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->performHapticFeedback(I)Z

    goto :goto_3

    .line 6387
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v15

    .line 6388
    const/16 v18, 0x1

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6389
    const/16 v18, 0x0

    aget v18, v11, v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6390
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->last_index:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-eq v0, v7, :cond_f

    .line 6391
    invoke-static {v15, v7}, Ljava/lang/Math;->min(II)I

    move-result v18

    invoke-static {v15, v7}, Ljava/lang/Math;->max(II)I

    move-result v19

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 6392
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->last_index:I

    goto/16 :goto_3

    .line 6393
    :cond_f
    if-ge v7, v15, :cond_e

    .line 6394
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v7, v15, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_3

    .line 6398
    .end local v4    # "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .end local v7    # "index":I
    .end local v8    # "line":I
    .end local v10    # "objectRect":Landroid/graphics/RectF;
    .end local v11    # "pts":[F
    .end local v13    # "relativeRect":Landroid/graphics/RectF;
    .end local v15    # "start":I
    :cond_10
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    move/from16 v18, v0

    if-nez v18, :cond_e

    .line 6399
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    if-nez v18, :cond_11

    .line 6400
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 6403
    :cond_11
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v18

    packed-switch v18, :pswitch_data_3

    goto/16 :goto_3

    .line 6405
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    .line 6406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    .line 6408
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_e

    .line 6411
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mActionDownTime:J

    .line 6418
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getSelectionRect(II)Landroid/graphics/RectF;

    move-result-object v12

    .line 6419
    .local v12, "rect":Landroid/graphics/RectF;
    if-eqz v12, :cond_e

    .line 6423
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsMoveText:Z

    .line 6424
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    .line 6425
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v18

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v12, v0, v1}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v18

    if-eqz v18, :cond_e

    .line 6426
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v20, v0

    invoke-interface/range {v18 .. v20}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    .line 6427
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v20, v0

    invoke-virtual/range {v18 .. v20}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    .line 6433
    .end local v12    # "rect":Landroid/graphics/RectF;
    :pswitch_a
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mActionDownTime:J

    move-wide/from16 v20, v0

    sub-long v16, v18, v20

    .line 6434
    .local v16, "time":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_12

    const-wide/16 v18, 0x258

    cmp-long v18, v16, v18

    if-ltz v18, :cond_12

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mActionDownTime:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-lez v18, :cond_12

    .line 6435
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsMoveText:Z

    .line 6437
    :cond_12
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsMoveText:Z

    move/from16 v18, v0

    if-eqz v18, :cond_e

    .line 6440
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    if-eqz v18, :cond_e

    .line 6447
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v5

    .line 6448
    .local v5, "deltaY":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v18

    add-float v18, v18, v5

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v8

    .line 6449
    .restart local v8    # "line":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v7

    .line 6451
    .restart local v7    # "index":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Landroid/text/Editable;->length()I

    move-result v18

    move/from16 v0, v18

    if-lt v0, v7, :cond_e

    .line 6454
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveIndex:I

    .line 6455
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 6456
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v0, v7}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 6458
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    .line 6459
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSelection()V

    .line 6461
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v18, v0

    if-eqz v18, :cond_13

    .line 6462
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    .line 6465
    :cond_13
    const/16 v18, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v19

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showDragText(ZII)V

    .line 6467
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    .line 6469
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 6472
    .end local v5    # "deltaY":F
    .end local v7    # "index":I
    .end local v8    # "line":I
    .end local v16    # "time":J
    :pswitch_b
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mActionDownTime:J

    .line 6474
    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showDragText(ZII)V

    .line 6476
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsMoveText:Z

    move/from16 v18, v0

    if-eqz v18, :cond_e

    .line 6480
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveIndex:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveIndex:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-gt v0, v1, :cond_14

    .line 6481
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsMoveText:Z

    goto/16 :goto_3

    .line 6485
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->appendText(Ljava/lang/String;)V

    .line 6487
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveIndex:I

    .line 6488
    .local v9, "newPos":I
    const/4 v14, 0x0

    .line 6489
    .local v14, "spanOffset":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveIndex:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_17

    .line 6490
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    add-int v19, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v20, v20, v21

    invoke-static/range {v18 .. v20}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 6491
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    add-int v9, v9, v18

    .line 6492
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveIndex:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    move/from16 v19, v0

    sub-int v14, v18, v19

    .line 6498
    :cond_15
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->removeText()V

    .line 6500
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    if-eqz v18, :cond_16

    .line 6501
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    if-lt v6, v0, :cond_18

    .line 6514
    .end local v6    # "i":I
    :cond_16
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v9, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 6516
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsMoveText:Z

    .line 6517
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    .line 6519
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 6494
    :cond_17
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveIndex:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_15

    .line 6495
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveStart:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v20, v0

    invoke-static/range {v18 .. v20}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 6496
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveIndex:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveEnd:I

    move/from16 v19, v0

    sub-int v14, v18, v19

    goto :goto_4

    .line 6502
    .restart local v6    # "i":I
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v19, v0

    add-int v19, v19, v14

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 6503
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    sub-int v19, v9, v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_19

    .line 6504
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    sub-int v19, v9, v19

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    .line 6506
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v19, v0

    add-int v19, v19, v14

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 6507
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-le v0, v9, :cond_1a

    .line 6508
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    move-object/from16 v0, v18

    iput v9, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    .line 6510
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveSpanList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    .line 6501
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_5

    .line 6522
    .end local v6    # "i":I
    .end local v9    # "newPos":I
    .end local v14    # "spanOffset":I
    :pswitch_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsMoveText:Z

    move/from16 v18, v0

    if-eqz v18, :cond_e

    .line 6523
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mActionDownTime:J

    .line 6524
    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showDragText(ZII)V

    .line 6525
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsMoveText:Z

    .line 6526
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMoveText:Ljava/lang/String;

    .line 6527
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 6249
    nop

    :pswitch_data_0
    .packed-switch 0xd3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 6315
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch

    .line 6365
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_6
        :pswitch_8
        :pswitch_7
    .end packed-switch

    .line 6403
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_9
        :pswitch_b
        :pswitch_a
        :pswitch_c
    .end packed-switch
.end method

.method protected onVisibleUpdated(Z)V
    .locals 2
    .param p1, "bVisible"    # Z

    .prologue
    .line 1968
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v0, :cond_0

    .line 1969
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onVisibleUpdated(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;Z)V

    .line 1971
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 2358
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHasWindowFocus:Z

    .line 2360
    if-eqz p1, :cond_0

    .line 2361
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->startBlink()V

    .line 2365
    :goto_0
    invoke-super {p0, p1}, Landroid/view/View;->onWindowFocusChanged(Z)V

    .line 2366
    return-void

    .line 2363
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->stopBlink()V

    goto :goto_0
.end method

.method public removeText()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 905
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    .line 906
    .local v1, "start":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    .line 908
    .local v0, "end":I
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    .line 910
    if-eq v1, v0, :cond_2

    .line 911
    if-ge v1, v0, :cond_1

    .line 912
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v2, v1, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 913
    invoke-virtual {p0, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 925
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 926
    return-void

    .line 915
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v2, v0, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 916
    invoke-virtual {p0, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto :goto_0

    .line 920
    :cond_2
    if-lez v1, :cond_0

    .line 921
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    add-int/lit8 v3, v1, -0x1

    invoke-interface {v2, v3, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_0
.end method

.method public scrollTextbox(F)V
    .locals 18
    .param p1, "y"    # F

    .prologue
    .line 6121
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v3, :cond_1

    .line 6207
    :cond_0
    :goto_0
    return-void

    .line 6124
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v9

    .line 6125
    .local v9, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    iget v3, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float p1, p1, v3

    .line 6127
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getHeight(J)I

    move-result v3

    int-to-float v12, v3

    .line 6128
    .local v12, "minimumHeight":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v13

    .line 6129
    .local v13, "objectRect":Landroid/graphics/RectF;
    if-eqz v13, :cond_0

    .line 6133
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v14

    check-cast v14, Landroid/view/ViewGroup;

    .line 6134
    .local v14, "parentLayout":Landroid/view/ViewGroup;
    if-eqz v14, :cond_0

    .line 6137
    new-instance v15, Landroid/graphics/RectF;

    invoke-direct {v15}, Landroid/graphics/RectF;-><init>()V

    .line 6138
    .local v15, "relativeRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 6140
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F

    move-result v10

    .line 6141
    .local v10, "deltaY":F
    const/4 v3, 0x0

    cmpl-float v3, p1, v3

    if-lez v3, :cond_9

    .line 6142
    add-float v10, v10, p1

    .line 6144
    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float v3, v12, v3

    cmpg-float v3, v10, v3

    if-gez v3, :cond_5

    .line 6145
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_setPan(JF)V

    .line 6187
    :cond_2
    :goto_1
    float-to-double v4, v12

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-double v0, v3

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v16

    cmpl-double v3, v4, v16

    if-lez v3, :cond_0

    .line 6189
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    if-eqz v3, :cond_3

    .line 6190
    const/4 v6, 0x1

    .line 6191
    .local v6, "textOnCanvasCommand":I
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 6192
    .local v7, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DisplayInfo;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6193
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const/4 v8, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 6195
    .end local v6    # "textOnCanvasCommand":I
    .end local v7    # "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_3
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_update(J)Z

    .line 6197
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_4

    .line 6198
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v8, v0, v1}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v8

    invoke-virtual {v3, v4, v5, v8}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 6201
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showScrollBar()V

    .line 6205
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    goto/16 :goto_0

    .line 6147
    :cond_5
    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v3

    cmpg-float v3, v12, v3

    if-gez v3, :cond_7

    .line 6148
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_setPan(JF)V

    .line 6150
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditable:Z

    if-eqz v3, :cond_2

    iget v3, v15, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v14}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_2

    .line 6151
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCheckCursorOnScroll(Z)V

    .line 6152
    iget v3, v15, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v14}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget v4, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float v11, v3, v4

    .line 6153
    .local v11, "maxScroll":F
    cmpg-float v3, p1, v11

    if-gez v3, :cond_6

    move/from16 v11, p1

    .end local v11    # "maxScroll":F
    :cond_6
    move-object/from16 v0, p0

    iput v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F

    .line 6154
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCanvasScroll:Z

    .line 6155
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onRequestScroll(FF)V

    goto/16 :goto_1

    .line 6158
    :cond_7
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float v3, v12, v3

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_setPan(JF)V

    .line 6160
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 6161
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditable:Z

    if-eqz v3, :cond_2

    iget v3, v15, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v14}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_2

    .line 6162
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCheckCursorOnScroll(Z)V

    .line 6163
    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float v3, v12, v3

    sub-float v2, v10, v3

    .line 6164
    .local v2, "canvasScroll":F
    iget v3, v15, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v14}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget v4, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float v11, v3, v4

    .line 6165
    .restart local v11    # "maxScroll":F
    cmpg-float v3, v2, v11

    if-gez v3, :cond_8

    .end local v2    # "canvasScroll":F
    :goto_2
    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F

    .line 6166
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCanvasScroll:Z

    .line 6167
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onRequestScroll(FF)V

    goto/16 :goto_1

    .restart local v2    # "canvasScroll":F
    :cond_8
    move v2, v11

    .line 6165
    goto :goto_2

    .line 6171
    .end local v2    # "canvasScroll":F
    .end local v11    # "maxScroll":F
    :cond_9
    add-float v10, v10, p1

    .line 6172
    const/4 v3, 0x0

    cmpg-float v3, v10, v3

    if-gez v3, :cond_b

    .line 6173
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditable:Z

    if-eqz v3, :cond_a

    iget-object v3, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_a

    .line 6174
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCheckCursorOnScroll(Z)V

    .line 6175
    iget-object v3, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    neg-float v11, v3

    .line 6176
    .restart local v11    # "maxScroll":F
    cmpl-float v3, v10, v11

    if-lez v3, :cond_c

    .end local v10    # "deltaY":F
    :goto_3
    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F

    .line 6177
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCanvasScroll:Z

    .line 6178
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onRequestScroll(FF)V

    .line 6180
    .end local v11    # "maxScroll":F
    :cond_a
    const/4 v10, 0x0

    .line 6183
    .restart local v10    # "deltaY":F
    :cond_b
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_setPan(JF)V

    goto/16 :goto_1

    .restart local v11    # "maxScroll":F
    :cond_c
    move v10, v11

    .line 6176
    goto :goto_3
.end method

.method protected seFitOnSizeChanged()V
    .locals 1

    .prologue
    .line 3223
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFitOnSizeChanged:Z

    .line 3224
    return-void
.end method

.method public setCheckCursorOnScroll(Z)V
    .locals 0
    .param p1, "isCheck"    # Z

    .prologue
    .line 3490
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCheckCursorOnScroll:Z

    .line 3491
    return-void
.end method

.method public setContextMenu(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V
    .locals 1
    .param p1, "contextMenu"    # Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    .line 1005
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setInstance(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V

    .line 1006
    return-void
.end method

.method public setContextMenuVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 1009
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenuVisible:Z

    .line 1010
    return-void
.end method

.method public setCursorPos(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1187
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 1188
    return-void
.end method

.method public setEraserMode(Z)V
    .locals 0
    .param p1, "option"    # Z

    .prologue
    .line 1040
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    .line 1041
    return-void
.end method

.method public setMargin(IIII)V
    .locals 0
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 1510
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_LEFT_MARGIN:I

    .line 1511
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    .line 1512
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_TOP_MARGIN:I

    .line 1513
    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    .line 1514
    return-void
.end method

.method public setObjectText(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 4
    .param p1, "textObj"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .prologue
    .line 1045
    if-eqz p1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1058
    :cond_0
    :goto_0
    return-void

    .line 1050
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .line 1051
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_setObjectText(JLcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z

    .line 1053
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getVerticalPan()F

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_setPan(JF)V

    .line 1055
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initEditable()V

    .line 1057
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    goto :goto_0
.end method

.method public setParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)V
    .locals 2
    .param p1, "paragraphInfo"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;

    .prologue
    .line 1017
    if-nez p1, :cond_0

    .line 1037
    .end local p1    # "paragraphInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    :goto_0
    return-void

    .line 1021
    .restart local p1    # "paragraphInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    :cond_0
    instance-of v0, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    if-eqz v0, :cond_2

    .line 1022
    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    .end local p1    # "paragraphInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    iget v0, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setParagraphAlign(I)V

    .line 1034
    :cond_1
    :goto_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 1036
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    goto :goto_0

    .line 1029
    .restart local p1    # "paragraphInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    :cond_2
    instance-of v0, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 1030
    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    .line 1031
    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    .end local p1    # "paragraphInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;
    iget v1, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    .line 1030
    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setParagraphSpacing(IF)V

    goto :goto_1
.end method

.method public setSelection(IIZ)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "isForce"    # Z

    .prologue
    .line 1240
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v1, :cond_1

    .line 1269
    :cond_0
    :goto_0
    return-void

    .line 1244
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-lt v1, p2, :cond_0

    .line 1248
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1, p1, p2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 1250
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    .line 1252
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSelection()V

    .line 1254
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v1, :cond_2

    .line 1255
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getSettingInfo(II)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    .line 1257
    .local v0, "textInfo":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 1259
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onSelectionChanged(II)V

    .line 1262
    .end local v0    # "textInfo":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    .line 1264
    if-eqz p3, :cond_3

    .line 1265
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 1267
    :cond_3
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevCurPos:I

    .line 1268
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    goto :goto_0
.end method

.method public setSelection(IZ)V
    .locals 7
    .param p1, "pos"    # I
    .param p2, "isForce"    # Z

    .prologue
    const/16 v6, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1203
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v4

    if-ge v4, p1, :cond_1

    .line 1236
    :cond_0
    :goto_0
    return-void

    .line 1207
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v4, p1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 1209
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v4, :cond_0

    .line 1213
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    .line 1215
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSelection()V

    .line 1217
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v4, v4, v2

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1218
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1220
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v4, :cond_2

    .line 1221
    invoke-direct {p0, p1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getSettingInfo(II)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v1

    .line 1223
    .local v1, "textInfo":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v4, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 1226
    .end local v1    # "textInfo":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    :cond_2
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    if-eqz v4, :cond_4

    :goto_1
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    .line 1228
    if-eqz p2, :cond_3

    .line 1229
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1230
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    if-lez v2, :cond_3

    iget v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    if-lez v2, :cond_3

    .line 1231
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 1235
    .end local v0    # "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    goto :goto_0

    :cond_4
    move v2, v3

    .line 1226
    goto :goto_1
.end method

.method public setSelectionAll()V
    .locals 4

    .prologue
    .line 1272
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v1, :cond_1

    .line 1286
    :cond_0
    :goto_0
    return-void

    .line 1276
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v0

    .line 1277
    .local v0, "str":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1280
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1284
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto :goto_0
.end method

.method protected setShowSoftInputEnable(Z)V
    .locals 0
    .param p1, "isShowSoftInputEnable"    # Z

    .prologue
    .line 1129
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsShowSoftInputEnable:Z

    .line 1130
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 3
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 929
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v1, :cond_0

    .line 930
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSetEmptyText:Z

    .line 933
    :cond_0
    if-eqz p1, :cond_1

    .line 934
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-interface {v0, v2, v1, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 937
    :cond_1
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSetEmptyText:Z

    .line 939
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 940
    return-void
.end method

.method public setTextBoxListener(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;)V
    .locals 4
    .param p1, "actionListener"    # Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    .prologue
    .line 631
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    .line 633
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v3, :cond_0

    .line 634
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v3}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    .line 635
    .local v1, "start":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v3}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    .line 637
    .local v0, "end":I
    invoke-direct {p0, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getSettingInfo(II)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v2

    .line 638
    .local v2, "textInfo":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 640
    .end local v0    # "end":I
    .end local v1    # "start":I
    .end local v2    # "textInfo":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    :cond_0
    return-void
.end method

.method public setTextFont(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V
    .locals 3
    .param p1, "textSpan"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .prologue
    .line 964
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    .line 965
    .local v1, "start":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    .line 967
    .local v0, "end":I
    instance-of v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    if-eqz v2, :cond_1

    .line 968
    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    .end local p1    # "textSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    invoke-direct {p0, v2, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFontColor(III)V

    .line 977
    :cond_0
    :goto_0
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 979
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 980
    return-void

    .line 969
    .restart local p1    # "textSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_1
    instance-of v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v2, :cond_2

    .line 970
    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    .end local p1    # "textSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    invoke-direct {p0, v2, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFontSize(FII)V

    goto :goto_0

    .line 971
    .restart local p1    # "textSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_2
    instance-of v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    if-eqz v2, :cond_3

    .line 972
    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    .end local p1    # "textSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    invoke-direct {p0, v2, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextBackgroundColor(III)V

    goto :goto_0

    .line 973
    .restart local p1    # "textSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_3
    instance-of v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    if-eqz v2, :cond_0

    .line 974
    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    .end local p1    # "textSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    iget-object v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    invoke-direct {p0, v2, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFontName(Ljava/lang/String;II)V

    goto :goto_0
.end method

.method public setTextLimit(I)V
    .locals 5
    .param p1, "count"    # I

    .prologue
    const/4 v4, 0x1

    .line 1289
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-nez v1, :cond_0

    .line 1311
    :goto_0
    return-void

    .line 1293
    :cond_0
    const/16 v1, 0x1388

    if-ge v1, p1, :cond_1

    .line 1294
    const/16 p1, 0x1388

    .line 1297
    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    .line 1299
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    if-le v1, v2, :cond_2

    .line 1300
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    .line 1302
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 1303
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 1307
    :cond_2
    new-array v0, v4, [Landroid/text/InputFilter;

    .line 1308
    .local v0, "arrayOfInputFilter":[Landroid/text/InputFilter;
    const/4 v1, 0x0

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;-><init>(Landroid/content/Context;I)V

    aput-object v2, v0, v1

    .line 1310
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1, v0}, Landroid/text/Editable;->setFilters([Landroid/text/InputFilter;)V

    goto :goto_0
.end method

.method public setTextStyle(IZ)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "option"    # Z

    .prologue
    const/4 v3, 0x1

    .line 986
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    .line 987
    .local v1, "start":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    .line 989
    .local v0, "end":I
    if-ne p1, v3, :cond_1

    .line 990
    invoke-direct {p0, p2, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextBold(ZII)V

    .line 997
    :cond_0
    :goto_0
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 999
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 1000
    return-void

    .line 991
    :cond_1
    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    .line 992
    invoke-direct {p0, p2, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextItalic(ZII)V

    goto :goto_0

    .line 993
    :cond_2
    const/4 v2, 0x4

    if-ne p1, v2, :cond_0

    .line 994
    invoke-direct {p0, p2, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextUnderline(ZII)V

    goto :goto_0
.end method

.method public setViewModeEnabled(Z)V
    .locals 2
    .param p1, "option"    # Z

    .prologue
    const/4 v1, 0x1

    .line 1161
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    .line 1163
    if-eqz p1, :cond_0

    .line 1164
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusableInTouchMode(Z)V

    .line 1165
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setVisibility(I)V

    .line 1166
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusable(Z)V

    .line 1168
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->requestFocus()Z

    .line 1170
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 1172
    :cond_0
    return-void
.end method

.method public showSoftInput()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x64

    const/4 v4, 0x1

    .line 1086
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 1087
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPreventShowSoftInput:Z

    if-eqz v2, :cond_1

    .line 1121
    :cond_0
    :goto_0
    return-void

    .line 1090
    :cond_1
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPreventShowSoftInput:Z

    .line 1091
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCheckCursorOnScroll(Z)V

    .line 1093
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHasWindowFocus:Z

    if-eqz v2, :cond_2

    .line 1094
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 1096
    :try_start_0
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1097
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$8;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$8;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    .line 1105
    const-wide/16 v4, 0x64

    .line 1097
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1107
    :catch_0
    move-exception v0

    .line 1108
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto :goto_0

    .line 1112
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :cond_2
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$9;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$9;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    invoke-virtual {v2, v3, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public showTextBox()V
    .locals 1

    .prologue
    .line 883
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setVisibility(I)V

    .line 884
    return-void
.end method

.method protected updateContextMenuLocation()V
    .locals 1

    .prologue
    .line 1888
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    .line 1889
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setDirty()V

    .line 1890
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextmenu()V

    .line 1892
    :cond_0
    return-void
.end method
