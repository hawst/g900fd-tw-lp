.class Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;
.super Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;
.source "SPenFontSizeDropdown2.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$ListAdapter;,
        Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$SizeDropdownSelectListner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private final mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private final mItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mListView:Landroid/widget/ListView;

.field private mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$SizeDropdownSelectListner;

.field private final mRunnable:Ljava/lang/Runnable;

.field private final root:Landroid/view/View;

.field private windowHeight:I

.field private windowWidth:I


# direct methods
.method public constructor <init>(Landroid/view/View;Ljava/util/ArrayList;IIF)V
    .locals 9
    .param p1, "anchor"    # Landroid/view/View;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "ratio"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;IIF)V"
        }
    .end annotation

    .prologue
    .local p2, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v5, 0x0

    const/4 v8, -0x1

    const/4 v7, -0x2

    .line 95
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;-><init>(Landroid/view/View;)V

    .line 29
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$SizeDropdownSelectListner;

    .line 32
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->windowWidth:I

    .line 33
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->windowHeight:I

    .line 137
    new-instance v4, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$1;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mRunnable:Ljava/lang/Runnable;

    .line 96
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mHandler:Landroid/os/Handler;

    .line 97
    iput p3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->windowWidth:I

    .line 98
    iput p4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->windowHeight:I

    .line 99
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->context:Landroid/content/Context;

    .line 100
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mItemList:Ljava/util/ArrayList;

    .line 101
    new-instance v4, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->context:Landroid/content/Context;

    const-string v6, ""

    invoke-direct {v4, v5, v6, p5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 103
    new-instance v2, Landroid/widget/FrameLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->context:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 104
    .local v2, "dropdownLayout":Landroid/widget/FrameLayout;
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 106
    .local v3, "dropdownLayoutParam":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 108
    new-instance v4, Landroid/widget/ListView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->context:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mListView:Landroid/widget/ListView;

    .line 114
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mListView:Landroid/widget/ListView;

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v5, v8, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 116
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 118
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->root:Landroid/view/View;

    .line 119
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->root:Landroid/view/View;

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->setContentView(Landroid/view/View;)V

    .line 123
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$ListAdapter;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->context:Landroid/content/Context;

    const v5, 0x1090003

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mItemList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$ListAdapter;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;Landroid/content/Context;ILjava/util/List;)V

    .line 124
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 125
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 126
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mListView:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v6, "tw_list_divider_holo_light"

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 127
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mListView:Landroid/widget/ListView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 128
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v5, "tw_menu_dropdown_panel_holo_light"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 129
    .local v1, "background":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 130
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v4, p3}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 131
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v4, p4}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 133
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->windowHeight:I

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->windowWidth:I

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    return-object v0
.end method


# virtual methods
.method public changeOrientation(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->dismiss()V

    .line 190
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 194
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$SizeDropdownSelectListner;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$SizeDropdownSelectListner;

    invoke-interface {v0, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$SizeDropdownSelectListner;->onSelectItem(I)V

    .line 197
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->dismiss()V

    .line 198
    return-void
.end method

.method public setOnItemSelectListner(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$SizeDropdownSelectListner;)V
    .locals 0
    .param p1, "listner"    # Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$SizeDropdownSelectListner;

    .prologue
    .line 201
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$SizeDropdownSelectListner;

    .line 202
    return-void
.end method

.method public show(IILjava/lang/String;)V
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "setItem"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x2

    .line 169
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->show()V

    .line 170
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 171
    .local v0, "index":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 172
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setSelection(I)V

    .line 174
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->root:Landroid/view/View;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 175
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->root:Landroid/view/View;

    invoke-virtual {v2, v4, v4}, Landroid/view/View;->measure(II)V

    .line 177
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->windowHeight:I

    .line 178
    .local v1, "tempHeight":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->window:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->anchor:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;)I

    move-result v2

    if-le v1, v2, :cond_1

    .line 179
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->window:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->anchor:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;)I

    move-result v1

    .line 183
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->window:Landroid/widget/PopupWindow;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->windowWidth:I

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 184
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 185
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->window:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->anchor:Landroid/view/View;

    invoke-virtual {v2, v3, p1, p2}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    .line 186
    return-void

    .line 181
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->windowHeight:I

    goto :goto_0
.end method

.method public updatePosition()V
    .locals 4

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 164
    :cond_0
    return-void
.end method
