.class Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;
.super Ljava/lang/Object;
.source "SpenInView.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenInView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnGestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 0

    .prologue
    .line 5814
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;)V
    .locals 0

    .prologue
    .line 5814
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 5818
    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5893
    if-eqz p1, :cond_0

    .line 5894
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v2

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$37(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 5895
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsCancelFling:Z
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$38(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 5896
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$37(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V

    .line 5900
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 11
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/high16 v8, 0x40000000    # 2.0f

    const/high16 v7, -0x3d380000    # -100.0f

    const/4 v6, 0x0

    .line 5841
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 5889
    :cond_0
    :goto_0
    return-void

    .line 5844
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 5848
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsLongPressEnable:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$20(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5852
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStrokeDrawing:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v1

    if-ne v1, v9, :cond_3

    .line 5853
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMeasure:Landroid/graphics/PathMeasure;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$22(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/PathMeasure;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPath:Landroid/graphics/Path;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$23(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/Path;

    move-result-object v2

    invoke-virtual {v1, v2, v6}, Landroid/graphics/PathMeasure;->setPath(Landroid/graphics/Path;Z)V

    .line 5854
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMeasure:Landroid/graphics/PathMeasure;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$22(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/PathMeasure;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/PathMeasure;->getLength()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->MIN_STROKE_LENGTH:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$24(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v2

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    .line 5859
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 5860
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;->onLongPressed(Landroid/view/MotionEvent;)V

    .line 5862
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v2

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v4

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getToolTypeAction(JI)I
    invoke-static {v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$26(Lcom/samsung/android/sdk/pen/engine/SpenInView;JI)I

    move-result v0

    .line 5863
    .local v0, "toolTypeAction":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$27(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 5864
    if-eq v0, v9, :cond_5

    if-ne v0, v10, :cond_7

    .line 5865
    :cond_5
    if-ne v0, v9, :cond_8

    .line 5866
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    move-result-object v2

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v3

    mul-float/2addr v2, v3

    div-float/2addr v2, v8

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$29(Lcom/samsung/android/sdk/pen/engine/SpenInView;F)V

    .line 5874
    :cond_6
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/PointF;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/PointF;->x:F

    .line 5875
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/PointF;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/PointF;->y:F

    .line 5876
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$33(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/Paint;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v2

    mul-float/2addr v2, v8

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 5886
    :cond_7
    :goto_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-static {v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$35(Lcom/samsung/android/sdk/pen/engine/SpenInView;Z)V

    .line 5888
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v2

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v4

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_onLongPress(JLandroid/view/MotionEvent;I)Z
    invoke-static {v1, v2, v3, p1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$36(Lcom/samsung/android/sdk/pen/engine/SpenInView;JLandroid/view/MotionEvent;I)Z

    goto/16 :goto_0

    .line 5867
    :cond_8
    if-ne v0, v10, :cond_6

    .line 5868
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v1

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-nez v1, :cond_9

    .line 5869
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/high16 v2, 0x41a00000    # 20.0f

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v3

    mul-float/2addr v2, v3

    div-float/2addr v2, v8

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$31(Lcom/samsung/android/sdk/pen/engine/SpenInView;F)V

    goto :goto_1

    .line 5870
    :cond_9
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v1

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    .line 5871
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/high16 v2, 0x42200000    # 40.0f

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v3

    mul-float/2addr v2, v3

    div-float/2addr v2, v8

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$31(Lcom/samsung/android/sdk/pen/engine/SpenInView;F)V

    goto/16 :goto_1

    .line 5879
    :cond_a
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-static {v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$34(Lcom/samsung/android/sdk/pen/engine/SpenInView;Z)V

    .line 5880
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-static {v1, v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$29(Lcom/samsung/android/sdk/pen/engine/SpenInView;F)V

    .line 5881
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-static {v1, v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$31(Lcom/samsung/android/sdk/pen/engine/SpenInView;F)V

    .line 5882
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/PointF;

    move-result-object v1

    iput v7, v1, Landroid/graphics/PointF;->x:F

    .line 5883
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/PointF;

    move-result-object v1

    iput v7, v1, Landroid/graphics/PointF;->y:F

    goto :goto_2
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 5836
    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 5823
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    .line 5827
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5831
    :goto_0
    return v4

    .line 5830
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J

    move-result-wide v2

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_onSingleTapUp(JLandroid/view/MotionEvent;I)Z
    invoke-static {v0, v2, v3, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenInView;JLandroid/view/MotionEvent;I)Z

    goto :goto_0
.end method
