.class public Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;
.super Ljava/lang/Object;
.source "EquationRecognitionPlugin.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;,
        Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$HandleInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "recognition-EquationRecognitionPlugin"

.field private static mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;


# instance fields
.field private final mHandler:Landroid/os/Handler;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation
.end field

.field private mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    .line 149
    new-instance v0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$1;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mHandler:Landroid/os/Handler;

    .line 60
    const-string v0, "recognition-EquationRecognitionPlugin"

    const-string v1, "creating equation recognition plugin"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    return-object v0
.end method

.method static synthetic access$1()Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public getNativeHandle()J
    .locals 2

    .prologue
    .line 249
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 233
    return-void
.end method

.method public initRecognizer(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 68
    const-class v1, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;

    monitor-enter v1

    .line 69
    :try_start_0
    sget-object v2, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    if-nez v2, :cond_1

    .line 70
    new-instance v2, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    invoke-direct {v2, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    .line 72
    sget-object v2, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    if-nez v2, :cond_0

    .line 73
    const-string v2, "recognition-EquationRecognitionPlugin"

    const-string v3, "Fail to create Equation recognition instance."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    monitor-exit v1

    .line 83
    :goto_0
    return v0

    .line 77
    :cond_0
    sget-object v2, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->init(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 78
    const-string v2, "recognition-EquationRecognitionPlugin"

    const-string v3, "Fail to initialize."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    monitor-exit v1

    goto :goto_0

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 187
    sget-boolean v4, Lcom/samsung/android/sdk/pen/Spen;->IS_SPEN_PRELOAD_MODE:Z

    if-eqz v4, :cond_1

    .line 189
    :try_start_0
    const-string v4, "SPenVIRecognition"

    invoke-static {v4}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->initRecognizer(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 209
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Fail to load Equation recognition engine"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 190
    :catch_0
    move-exception v1

    .line 191
    .local v1, "error":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 192
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "libSPenVIRecognition.so is not loaded."

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 195
    .end local v1    # "error":Ljava/lang/Exception;
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "/data/data/"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/lib/lib"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "SPen"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 196
    const-string v5, "VIRecognition"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".so"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 195
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 197
    .local v3, "libFullName":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 198
    .local v2, "libFilePath":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 200
    :try_start_1
    invoke-static {v3}, Ljava/lang/System;->load(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 201
    :catch_1
    move-exception v0

    .line 202
    .local v0, "e":Ljava/lang/Throwable;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 203
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "libSPenVIRecognition.so is not loaded."

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 211
    .end local v0    # "e":Ljava/lang/Throwable;
    .end local v2    # "libFilePath":Ljava/io/File;
    .end local v3    # "libFullName":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public onUnload()V
    .locals 2

    .prologue
    .line 219
    const-class v1, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;

    monitor-enter v1

    .line 220
    :try_start_0
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    if-eqz v0, :cond_0

    .line 221
    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->dispose()V

    .line 222
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    .line 219
    :cond_0
    monitor-exit v1

    .line 225
    return-void

    .line 219
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public request(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 165
    .local p1, "input":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    if-nez v1, :cond_0

    .line 166
    const-string v1, "recognition-EquationRecognitionPlugin"

    const-string v2, "The result listener isn\'t set yet!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 171
    :cond_0
    sget-object v1, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    if-eqz v1, :cond_1

    .line 172
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;Ljava/util/List;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 173
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 179
    .end local v0    # "thread":Ljava/lang/Thread;
    :goto_0
    return-void

    .line 176
    :cond_1
    const-string v1, "recognition-EquationRecognitionPlugin"

    const-string v2, "The recognition engine is null!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 241
    return-void
.end method

.method public setResultListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    .prologue
    .line 258
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    .line 259
    return-void
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 267
    const/4 v0, 0x1

    return v0
.end method
