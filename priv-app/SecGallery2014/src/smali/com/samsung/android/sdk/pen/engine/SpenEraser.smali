.class Lcom/samsung/android/sdk/pen/engine/SpenEraser;
.super Ljava/lang/Object;
.source "SpenEraser.java"


# static fields
.field private static final TOUCH_TOLERANCE_ERASER:F = 1.0f


# instance fields
.field private mCanvas:Landroid/graphics/Canvas;

.field private mPaint:Landroid/graphics/Paint;

.field private mQuadLastX:F

.field private mQuadLastY:F

.field private mQuadStartX:F

.field private mQuadStartY:F

.field private mSize:F

.field private mUnitPath:Landroid/graphics/Path;

.field private mX:F

.field private mY:F


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    .line 22
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    .line 23
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mCanvas:Landroid/graphics/Canvas;

    .line 24
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mSize:F

    .line 27
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    .line 28
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 29
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 30
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 31
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 32
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mSize:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 33
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 35
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    .line 36
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->incReserve(I)V

    .line 37
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 122
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    .line 123
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    .line 124
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mCanvas:Landroid/graphics/Canvas;

    .line 125
    return-void
.end method

.method public endPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->getSize()F

    move-result v0

    .line 95
    .local v0, "curStrokeWidth":F
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->rewind()V

    .line 96
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadStartX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadStartY:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 97
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 98
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mCanvas:Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 99
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    const/4 v3, 0x0

    invoke-virtual {v2, p2, v3}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 101
    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v0, v2

    const/high16 v3, 0x3f800000    # 1.0f

    add-float v1, v2, v3

    .line 102
    .local v1, "halfSize":F
    iget v2, p2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v1

    iget v3, p2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v1

    iget v4, p2, Landroid/graphics/RectF;->right:F

    add-float/2addr v4, v1

    iget v5, p2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v5, v1

    invoke-virtual {p2, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 103
    return-void
.end method

.method public getSize()F
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mSize:F

    return v0
.end method

.method public movePen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 13
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 49
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    sub-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 50
    .local v2, "dx":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    sub-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 51
    .local v3, "dy":F
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->getSize()F

    move-result v1

    .line 53
    .local v1, "curStrokeWidth":F
    const/high16 v8, 0x3f800000    # 1.0f

    cmpg-float v8, v2, v8

    if-gez v8, :cond_0

    const/high16 v8, 0x3f800000    # 1.0f

    cmpg-float v8, v3, v8

    if-gez v8, :cond_0

    .line 89
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    invoke-virtual {v8}, Landroid/graphics/Path;->rewind()V

    .line 58
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadStartX:F

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadStartY:F

    invoke-virtual {v8, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 59
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v0

    .line 62
    .local v0, "N":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-lt v5, v0, :cond_1

    .line 75
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    add-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastX:F

    .line 76
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    add-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastY:F

    .line 78
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastX:F

    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastY:F

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 79
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mCanvas:Landroid/graphics/Canvas;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v8, v9, v10}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 80
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    const/4 v9, 0x0

    invoke-virtual {v8, p2, v9}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 82
    const/high16 v8, 0x40000000    # 2.0f

    div-float v8, v1, v8

    const/high16 v9, 0x3f800000    # 1.0f

    add-float v4, v8, v9

    .line 83
    .local v4, "halfSize":F
    iget v8, p2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v8, v4

    iget v9, p2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v9, v4

    iget v10, p2, Landroid/graphics/RectF;->right:F

    add-float/2addr v10, v4

    iget v11, p2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v11, v4

    invoke-virtual {p2, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 85
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastX:F

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadStartX:F

    .line 86
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastY:F

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadStartY:F

    .line 87
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    .line 88
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    goto :goto_0

    .line 63
    .end local v4    # "halfSize":F
    :cond_1
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v6

    .line 64
    .local v6, "ipX":F
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v7

    .line 66
    .local v7, "ipY":F
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    add-float/2addr v8, v6

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastX:F

    .line 67
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    add-float/2addr v8, v7

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastY:F

    .line 69
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastX:F

    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastY:F

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 71
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    .line 72
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    .line 62
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 106
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mCanvas:Landroid/graphics/Canvas;

    .line 107
    return-void
.end method

.method public setSize(F)V
    .locals 2
    .param p1, "size"    # F

    .prologue
    .line 110
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mSize:F

    .line 112
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mSize:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 115
    :cond_0
    return-void
.end method

.method public startPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 40
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    .line 41
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    .line 42
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadStartX:F

    .line 43
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadStartY:F

    .line 44
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastX:F

    .line 45
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastY:F

    .line 46
    return-void
.end method
