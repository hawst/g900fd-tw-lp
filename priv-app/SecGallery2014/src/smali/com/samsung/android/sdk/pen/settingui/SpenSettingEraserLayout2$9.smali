.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$9;
.super Ljava/lang/Object;
.source "SpenSettingEraserLayout2.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$9;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    .line 316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 319
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 320
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$9;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoDecrement:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 322
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$9;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 324
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$9;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;Z)V

    .line 326
    :cond_1
    return v2
.end method
