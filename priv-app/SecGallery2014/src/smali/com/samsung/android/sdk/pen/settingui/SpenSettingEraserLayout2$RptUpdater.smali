.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;
.super Ljava/lang/Object;
.source "SpenSettingEraserLayout2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RptUpdater"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x14

    const/high16 v2, 0x3f800000    # 1.0f

    .line 259
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoIncrement:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 260
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    aget-object v0, v0, v1

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    const/high16 v1, 0x42c60000    # 99.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    aget-object v0, v0, v1

    iget v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    add-float/2addr v1, v2

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 262
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 263
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->repeatUpdateHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 274
    :cond_1
    :goto_0
    return-void

    .line 266
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoDecrement:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 267
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    aget-object v0, v0, v1

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 268
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    aget-object v0, v0, v1

    iget v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    sub-float/2addr v1, v2

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 269
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 270
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 272
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->repeatUpdateHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$RptUpdater;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
