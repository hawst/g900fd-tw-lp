.class public Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;
.super Landroid/widget/LinearLayout;
.source "SpenSettingRemoverLayout.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;
    }
.end annotation


# static fields
.field private static final ERASER_PROGRESS_MAX:I = 0x9

.field private static final ERASER_SIZE_MAX:I = 0xa

.field private static final EXIT_BUTTON_HEIGHT:I = 0x24

.field private static EXIT_BUTTON_WIDTH:I = 0x0

.field private static final IB_REMOVER_EXIT_ID:I = 0xb82f91

.field private static final IB_REMOVER_SIZE_MINUS_ID:I = 0xb82f93

.field private static final IB_REMOVER_SIZE_PLUS_ID:I = 0xb82f92

.field private static final LL_VERSION_CODE:I = 0x15

.field private static final REP_DELAY:I = 0x14

.field private static final TABS2_SCREEN_WIDTH:I = 0x600

.field private static final TAB_A_SCREEN_WIDTH:I = 0x300

.field private static final TAG:Ljava/lang/String; = "settingui-settingRemover"

.field private static final TITLE_LAYOUT_HEIGHT:I = 0x29

.field private static final TOTAL_LAYOUT_WIDTH:I = 0xf7

.field private static final VIENNA_SCREEN_WIDTH:I = 0x640

.field public static final VIEW_MODE_NORMAL:I = 0x0

.field public static final VIEW_MODE_SIZE:I = 0x2

.field public static final VIEW_MODE_TITLE:I = 0x3

.field public static final VIEW_MODE_TYPE:I = 0x1

.field private static arabicChars:[C = null

.field private static final bodyLeftPath:Ljava/lang/String; = "snote_popup_bg_left"

.field private static final bodyRightPath:Ljava/lang/String; = "snote_popup_bg_right"

.field private static final btnFocusPath:Ljava/lang/String; = "snote_popup_btn_focus"

.field private static final btnNoramlPath:Ljava/lang/String; = "snote_popup_btn_normal"

.field private static final btnPressPath:Ljava/lang/String; = "snote_popup_btn_press"

.field private static final cutterPopupDrawPress:Ljava/lang/String; = "snote_eraser_popup_draw_press"

.field private static final cutterPopupDrawUnselect:Ljava/lang/String; = "snote_eraser_popup_draw"

.field private static final cutterPopupTextPress:Ljava/lang/String; = "snote_eraser_popup_text_press"

.field private static final cutterPopupTextUnselect:Ljava/lang/String; = "snote_eraser_popup_text"

.field private static final exitPath:Ljava/lang/String; = "snote_popup_close"

.field private static final exitPressPath:Ljava/lang/String; = "snote_popup_close_press"

.field private static final exitfocusPath:Ljava/lang/String; = "snote_popup_close_focus"

.field private static final handelFocusPath:Ljava/lang/String; = "progress_handle_focus"

.field private static final handelPath:Ljava/lang/String; = "progress_handle_normal"

.field private static final handelPressPath:Ljava/lang/String; = "progress_handle_press"

.field private static final lineDivider:Ljava/lang/String; = "snote_popup_line"

.field private static final mDefaultPath:Ljava/lang/String; = ""

.field private static final mSdkVersion:I

.field private static final minusBgDimPath:Ljava/lang/String; = "snote_popup_progress_btn_minus_dim"

.field private static final minusBgFocusPath:Ljava/lang/String; = "snote_popup_progress_btn_minus_focus"

.field private static final minusBgPath:Ljava/lang/String; = "snote_popup_progress_btn_minus_normal"

.field private static final minusBgPressPath:Ljava/lang/String; = "snote_popup_progress_btn_minus_press"

.field private static final plusBgDimPath:Ljava/lang/String; = "snote_popup_progress_btn_plus_dim"

.field private static final plusBgFocusPath:Ljava/lang/String; = "snote_popup_progress_btn_plus_focus"

.field private static final plusBgPath:Ljava/lang/String; = "snote_popup_progress_btn_plus_normal"

.field private static final plusBgPressPath:Ljava/lang/String; = "snote_popup_progress_btn_plus_press"

.field private static final progressBgPath:Ljava/lang/String; = "progress_bg"

.field private static final progressShadowPath:Ljava/lang/String; = "progress_shadow"

.field private static final titleCenterPath:Ljava/lang/String; = "snote_popup_title_center"

.field private static final titleLeftPath:Ljava/lang/String; = "snote_popup_title_left"

.field private static final titleRightIndicatorPath:Ljava/lang/String; = "snote_popup_title_bended"

.field private static final titleRightPath:Ljava/lang/String; = "snote_popup_title_right"


# instance fields
.field private final EXIT_BUTTON_RIGHT_MARGIN:I

.field private EXIT_BUTTON_TOP_MARGIN:I

.field private RIGHT_INDICATOR_WIDTH:I

.field private final handlerRotate:Landroid/os/Handler;

.field private isFirstClickChexBox:Z

.field localDisplayMetrics:Landroid/util/DisplayMetrics;

.field private localLinearLayout:Landroid/widget/LinearLayout;

.field private mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;

.field private mAutoDecrement:Z

.field private mAutoIncrement:Z

.field private mBodyLayout:Landroid/view/View;

.field private mCanvasLayout:Landroid/widget/RelativeLayout;

.field private mCanvasSize:I

.field private mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

.field private final mCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mClearAllButton:Landroid/view/View;

.field private mClearAllListener:Landroid/view/View$OnClickListener;

.field private mCurrentCutterType:I

.field private mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

.field private mCutterKeyListener:Landroid/view/View$OnKeyListener;

.field private mCutterListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

.field private mCutterMinusButton:Landroid/widget/ImageButton;

.field private mCutterPlusButton:Landroid/widget/ImageButton;

.field private mCutterSizeButton:Landroid/view/ViewGroup;

.field private mCutterSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mCutterSizeSeekBar:Landroid/widget/SeekBar;

.field private mCutterType01:Landroid/widget/ImageButton;

.field private mCutterType02:Landroid/widget/ImageButton;

.field private mCutterTypeChekBox:Landroid/view/View;

.field private mCutterTypeLayout:Landroid/view/ViewGroup;

.field private mCutterTypeListner:Landroid/view/View$OnClickListener;

.field private mCutterTypeView:[Landroid/view/View;

.field private mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private mEraserSizeTextView:Landroid/widget/TextView;

.field private mExitButton:Landroid/view/View;

.field private mExitButtonListener:Landroid/view/View$OnClickListener;

.field private mFirstLongPress:Z

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

.field private mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

.field private mIndicator:Landroid/widget/ImageView;

.field private mIsFirstShown:Z

.field private mIsRotated:Z

.field private mIsShowing:Z

.field mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

.field private mLeftMargin:I

.field private mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

.field private mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

.field private final mMinusButtonKeyListener:Landroid/view/View$OnKeyListener;

.field private mMinusButtonListener:Landroid/view/View$OnClickListener;

.field private final mMovableRect:Landroid/graphics/Rect;

.field private mMoveSettingLayout:Z

.field private mNeedCalculateMargin:Z

.field private mNeedRecalculateRotate:Z

.field private mNeedRotateWhenSetPosition:Z

.field private final mOldLocation:[I

.field private final mOldMovableRect:Landroid/graphics/Rect;

.field private final mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

.field private mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

.field private mOnTouchListener:Landroid/view/View$OnTouchListener;

.field private final mPlusButtonKeyListener:Landroid/view/View$OnKeyListener;

.field private mPlusButtonListener:Landroid/view/View$OnClickListener;

.field private mRemoverContext:Landroid/content/Context;

.field private mRightIndicator:Landroid/widget/ImageView;

.field private mScale:F

.field private mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

.field private mSettingSizeLayout:Landroid/view/ViewGroup;

.field private mStandardCanvasSize:I

.field private mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

.field private mTitleLayout:Landroid/view/View;

.field private mTitleView:Landroid/widget/TextView;

.field private mTopMargin:I

.field private mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

.field private mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

.field private mViewMode:I

.field private mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;

.field private mXDelta:I

.field private mYDelta:I

.field private repeatUpdateHandler:Landroid/os/Handler;

.field private requestLayoutDisable:Z

.field private final runnableRotate:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 141
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    .line 170
    const/16 v0, 0x26

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_WIDTH:I

    .line 2638
    const/16 v0, 0xa

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->arabicChars:[C

    return-void

    :array_0
    .array-data 2
        0x660s
        0x661s
        0x662s
        0x663s
        0x664s
        0x665s
        0x666s
        0x667s
        0x668s
        0x669s
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "customImagePath"    # Ljava/lang/String;
    .param p3, "relativeLayout"    # Landroid/widget/RelativeLayout;

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v4, 0x3fc00000    # 1.5f

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 849
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 71
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRecalculateRotate:Z

    .line 72
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsFirstShown:Z

    .line 73
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsShowing:Z

    .line 105
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    .line 112
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    .line 133
    const/16 v0, 0x438

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    .line 134
    const/16 v0, 0x5a0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    .line 136
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    .line 137
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 138
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 139
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mGestureDetector:Landroid/view/GestureDetector;

    .line 145
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMoveSettingLayout:Z

    .line 147
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedCalculateMargin:Z

    .line 148
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mFirstLongPress:Z

    .line 149
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->isFirstClickChexBox:Z

    .line 155
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsRotated:Z

    .line 159
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 161
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

    .line 162
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;

    .line 163
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;

    .line 176
    const/16 v0, 0x15

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_TOP_MARGIN:I

    .line 177
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_RIGHT_MARGIN:I

    .line 179
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mViewMode:I

    .line 181
    const/16 v0, 0x13

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->RIGHT_INDICATOR_WIDTH:I

    .line 319
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 399
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    .line 408
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllListener:Landroid/view/View$OnClickListener;

    .line 420
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 449
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 523
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    .line 532
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mPlusButtonListener:Landroid/view/View$OnClickListener;

    .line 548
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 563
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->repeatUpdateHandler:Landroid/os/Handler;

    .line 564
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoIncrement:Z

    .line 565
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoDecrement:Z

    .line 596
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 607
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

    .line 620
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mPlusButtonKeyListener:Landroid/view/View$OnKeyListener;

    .line 639
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 650
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$13;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$13;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

    .line 664
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$14;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$14;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMinusButtonKeyListener:Landroid/view/View$OnKeyListener;

    .line 682
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 771
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$16;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$16;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeListner:Landroid/view/View$OnClickListener;

    .line 778
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$17;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$17;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    .line 789
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$18;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$18;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterKeyListener:Landroid/view/View$OnKeyListener;

    .line 1445
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRotateWhenSetPosition:Z

    .line 1446
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->handlerRotate:Landroid/os/Handler;

    .line 1447
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$19;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$19;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->runnableRotate:Ljava/lang/Runnable;

    .line 2152
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$20;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$20;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    .line 850
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    .line 851
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, p1, p2, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 852
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 853
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 854
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    .line 855
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 856
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 857
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 858
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    cmpg-float v0, v0, v4

    if-gez v0, :cond_5

    .line 859
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_TOP_MARGIN:I

    .line 863
    :cond_0
    :goto_0
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 864
    const/16 v0, 0x24

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_WIDTH:I

    .line 866
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    cmpg-float v0, v0, v4

    if-gez v0, :cond_2

    .line 867
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->RIGHT_INDICATOR_WIDTH:I

    .line 870
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v0, v1, :cond_6

    .line 871
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    .line 876
    :goto_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    const/16 v1, 0x5fc

    if-ne v0, v1, :cond_4

    .line 877
    :cond_3
    const/16 v0, 0x5a0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    .line 880
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->initView()V

    .line 881
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setListener()V

    .line 882
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    .line 883
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    .line 884
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldLocation:[I

    .line 885
    return-void

    .line 860
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    cmpl-float v0, v0, v4

    if-nez v0, :cond_0

    .line 861
    const/16 v0, 0x10

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_TOP_MARGIN:I

    goto :goto_0

    .line 873
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "customImagePath"    # Ljava/lang/String;
    .param p3, "relativeLayout"    # Landroid/widget/RelativeLayout;
    .param p4, "ratio"    # F

    .prologue
    const v1, 0x3f59999a    # 0.85f

    const/high16 v5, 0x3fc00000    # 1.5f

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 924
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 71
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRecalculateRotate:Z

    .line 72
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsFirstShown:Z

    .line 73
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsShowing:Z

    .line 105
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    .line 112
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    .line 133
    const/16 v0, 0x438

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    .line 134
    const/16 v0, 0x5a0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    .line 136
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    .line 137
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 138
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 139
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mGestureDetector:Landroid/view/GestureDetector;

    .line 145
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMoveSettingLayout:Z

    .line 147
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedCalculateMargin:Z

    .line 148
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mFirstLongPress:Z

    .line 149
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->isFirstClickChexBox:Z

    .line 155
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsRotated:Z

    .line 159
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 161
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

    .line 162
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;

    .line 163
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;

    .line 176
    const/16 v0, 0x15

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_TOP_MARGIN:I

    .line 177
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_RIGHT_MARGIN:I

    .line 179
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mViewMode:I

    .line 181
    const/16 v0, 0x13

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->RIGHT_INDICATOR_WIDTH:I

    .line 319
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 399
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    .line 408
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllListener:Landroid/view/View$OnClickListener;

    .line 420
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 449
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 523
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    .line 532
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mPlusButtonListener:Landroid/view/View$OnClickListener;

    .line 548
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 563
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->repeatUpdateHandler:Landroid/os/Handler;

    .line 564
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoIncrement:Z

    .line 565
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoDecrement:Z

    .line 596
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 607
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

    .line 620
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mPlusButtonKeyListener:Landroid/view/View$OnKeyListener;

    .line 639
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 650
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$13;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$13;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

    .line 664
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$14;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$14;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMinusButtonKeyListener:Landroid/view/View$OnKeyListener;

    .line 682
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$15;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 771
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$16;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$16;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeListner:Landroid/view/View$OnClickListener;

    .line 778
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$17;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$17;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    .line 789
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$18;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$18;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterKeyListener:Landroid/view/View$OnKeyListener;

    .line 1445
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRotateWhenSetPosition:Z

    .line 1446
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->handlerRotate:Landroid/os/Handler;

    .line 1447
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$19;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$19;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->runnableRotate:Ljava/lang/Runnable;

    .line 2152
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$20;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$20;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    .line 925
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    .line 926
    iput p4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    .line 927
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    const/high16 v2, 0x40000000    # 2.0f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_5

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    :goto_0
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    .line 928
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_6

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    :goto_1
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    .line 929
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mScale:F

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 930
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 931
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 932
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    .line 933
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 934
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 935
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 936
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    cmpg-float v0, v0, v5

    if-gez v0, :cond_7

    .line 937
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_TOP_MARGIN:I

    .line 941
    :cond_0
    :goto_2
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 942
    const/16 v0, 0x24

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_WIDTH:I

    .line 944
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    cmpg-float v0, v0, v5

    if-gez v0, :cond_2

    .line 945
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->RIGHT_INDICATOR_WIDTH:I

    .line 948
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v0, v1, :cond_8

    .line 949
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    .line 954
    :goto_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    const/16 v1, 0x5fc

    if-ne v0, v1, :cond_4

    .line 955
    :cond_3
    const/16 v0, 0x5a0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    .line 958
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->initView()V

    .line 959
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setListener()V

    .line 960
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    .line 961
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    .line 962
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldLocation:[I

    .line 963
    return-void

    .line 927
    :cond_5
    const/high16 v0, 0x40000000    # 2.0f

    goto/16 :goto_0

    :cond_6
    move v0, v1

    .line 928
    goto/16 :goto_1

    .line 938
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    cmpl-float v0, v0, v5

    if-nez v0, :cond_0

    .line 939
    const/16 v0, 0x10

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_TOP_MARGIN:I

    goto :goto_2

    .line 951
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    goto :goto_3
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/view/GestureDetector;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mGestureDetector:Landroid/view/GestureDetector;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMoveSettingLayout:Z

    return v0
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    return-object v0
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    return-object v0
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Z)V
    .locals 0

    .prologue
    .line 1727
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setEnableSizeSeekbar(Z)V

    return-void
.end method

.method static synthetic access$14(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsShowing:Z

    return v0
.end method

.method static synthetic access$15(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedCalculateMargin:Z

    return v0
.end method

.method static synthetic access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;I)V
    .locals 0

    .prologue
    .line 151
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLeftMargin:I

    return-void
.end method

.method static synthetic access$17(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;I)V
    .locals 0

    .prologue
    .line 150
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTopMargin:I

    return-void
.end method

.method static synthetic access$18(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Z)V
    .locals 0

    .prologue
    .line 147
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedCalculateMargin:Z

    return-void
.end method

.method static synthetic access$19(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mFirstLongPress:Z

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)I
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mXDelta:I

    return v0
.end method

.method static synthetic access$20(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleLayout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Z)V
    .locals 0

    .prologue
    .line 148
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mFirstLongPress:Z

    return-void
.end method

.method static synthetic access$22(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Z)V
    .locals 0

    .prologue
    .line 145
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMoveSettingLayout:Z

    return-void
.end method

.method static synthetic access$23(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$24(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$25(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;I)V
    .locals 0

    .prologue
    .line 143
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mXDelta:I

    return-void
.end method

.method static synthetic access$26(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;I)V
    .locals 0

    .prologue
    .line 144
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mYDelta:I

    return-void
.end method

.method static synthetic access$27(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/widget/SeekBar;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$28(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    return-object v0
.end method

.method static synthetic access$29(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)I
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mYDelta:I

    return v0
.end method

.method static synthetic access$30(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    return v0
.end method

.method static synthetic access$31(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)I
    .locals 1

    .prologue
    .line 134
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    return v0
.end method

.method static synthetic access$32(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Z)V
    .locals 0

    .prologue
    .line 564
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoIncrement:Z

    return-void
.end method

.method static synthetic access$33(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$34(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 563
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->repeatUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$35(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z
    .locals 1

    .prologue
    .line 564
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoIncrement:Z

    return v0
.end method

.method static synthetic access$36(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Z)V
    .locals 0

    .prologue
    .line 565
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoDecrement:Z

    return-void
.end method

.method static synthetic access$37(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$38(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z
    .locals 1

    .prologue
    .line 565
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoDecrement:Z

    return v0
.end method

.method static synthetic access$39(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Z)V
    .locals 0

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$40(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$41(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2592
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->convertToArabicNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$42(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    return-object v0
.end method

.method static synthetic access$43(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2014
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->cutterTypeSetting(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$44(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Z)V
    .locals 0

    .prologue
    .line 1445
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRotateWhenSetPosition:Z

    return-void
.end method

.method static synthetic access$45(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z
    .locals 1

    .prologue
    .line 155
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsRotated:Z

    return v0
.end method

.method static synthetic access$46(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Z)V
    .locals 0

    .prologue
    .line 155
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsRotated:Z

    return-void
.end method

.method static synthetic access$47(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z
    .locals 1

    .prologue
    .line 1445
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRotateWhenSetPosition:Z

    return v0
.end method

.method static synthetic access$48(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V
    .locals 0

    .prologue
    .line 2059
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->rotatePosition()V

    return-void
.end method

.method static synthetic access$49(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRecalculateRotate:Z

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$50(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V
    .locals 0

    .prologue
    .line 2118
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->checkPosition()V

    return-void
.end method

.method static synthetic access$51(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType01:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$52(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType02:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 2578
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)[I
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldLocation:[I

    return-object v0
.end method

.method private addCutterLayout()V
    .locals 3

    .prologue
    .line 1558
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType01:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    .line 1559
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType01:Landroid/widget/ImageButton;

    .line 1560
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType01:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v2, "snote_eraser_popup_draw"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1561
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType01:Landroid/widget/ImageButton;

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$21;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$21;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1578
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localLinearLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType01:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1580
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType02:Landroid/widget/ImageButton;

    if-nez v0, :cond_1

    .line 1582
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType02:Landroid/widget/ImageButton;

    .line 1583
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType02:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v2, "snote_eraser_popup_text"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1584
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType02:Landroid/widget/ImageButton;

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$22;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$22;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1601
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localLinearLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType02:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1603
    :cond_1
    return-void
.end method

.method private bodyBg()Landroid/view/ViewGroup;
    .locals 12

    .prologue
    const/16 v11, 0xa

    const/4 v10, 0x1

    const/high16 v9, 0x43080000    # 136.0f

    const/high16 v8, 0x42f60000    # 123.0f

    .line 1180
    new-instance v4, Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v4, v6}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1181
    .local v4, "layout":Landroid/widget/RelativeLayout;
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x1

    .line 1182
    const/4 v7, -0x2

    .line 1181
    invoke-direct {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1184
    .local v5, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1186
    new-instance v0, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1187
    .local v0, "bodyLeft":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1188
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1189
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v7, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 1187
    invoke-direct {v1, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1190
    .local v1, "bodyLeftParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v10, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1191
    const/16 v6, 0x9

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1192
    invoke-virtual {v1, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1193
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1194
    new-instance v2, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v2, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1195
    .local v2, "bodyRight":Landroid/widget/ImageView;
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1196
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x43770000    # 247.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1197
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 1196
    sub-int/2addr v6, v7

    .line 1198
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v7, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 1195
    invoke-direct {v3, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1199
    .local v3, "bodyRightParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v10, v3, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1200
    const/16 v6, 0xb

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1201
    invoke-virtual {v3, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1202
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1204
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v7, "snote_popup_bg_left"

    invoke-virtual {v6, v0, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1205
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v7, "snote_popup_bg_right"

    invoke-virtual {v6, v2, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1206
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1207
    invoke-virtual {v4, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1208
    invoke-virtual {v4, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1209
    return-object v4
.end method

.method private bodyLayout()Landroid/view/ViewGroup;
    .locals 11

    .prologue
    const/4 v10, -0x1

    const/high16 v8, 0x40a00000    # 5.0f

    const/4 v9, 0x0

    .line 1127
    new-instance v4, Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v4, v6}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1128
    .local v4, "layout":Landroid/widget/RelativeLayout;
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1129
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x43770000    # 247.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    const/4 v7, -0x2

    .line 1128
    invoke-direct {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1130
    .local v5, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v6, 0x9

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1131
    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1132
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->bodyBg()Landroid/view/ViewGroup;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1134
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->cutterSettingClearAllButton()Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    .line 1135
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->cutterTypeLayout()Landroid/view/ViewGroup;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    .line 1136
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->cutterSettingSize()Landroid/view/ViewGroup;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingSizeLayout:Landroid/view/ViewGroup;

    .line 1139
    new-instance v3, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v3, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1140
    .local v3, "imgDivider":Landroid/widget/ImageView;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1141
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1140
    invoke-direct {v2, v10, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1142
    .local v2, "dividerParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v6, 0x1

    iput-boolean v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1143
    const/4 v6, 0x6

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1144
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    iput v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1145
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    iput v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1146
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x42480000    # 50.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    iput v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1148
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1149
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v7, "snote_popup_line"

    invoke-virtual {v6, v3, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1150
    invoke-virtual {v4, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1153
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->eraseByStroke()Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeChekBox:Landroid/view/View;

    .line 1154
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v6}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1155
    .local v0, "checkBoxLayout":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1156
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x421c0000    # 39.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1155
    invoke-direct {v1, v10, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1157
    .local v1, "checkBoxParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x424c0000    # 51.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1158
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41200000    # 10.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 1157
    invoke-virtual {v1, v9, v6, v9, v7}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1159
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1160
    invoke-virtual {v0, v9}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 1161
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeChekBox:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1163
    invoke-direct {p0, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setEnableSizeSeekbar(Z)V

    .line 1164
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1165
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1166
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1167
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1168
    invoke-virtual {v4, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1169
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1170
    return-object v4
.end method

.method private checkPosition()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2119
    const/4 v4, 0x2

    new-array v0, v4, [I

    .line 2120
    .local v0, "location":[I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43770000    # 247.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->RIGHT_INDICATOR_WIDTH:I

    add-int/2addr v4, v5

    add-int/lit8 v2, v4, 0x2

    .line 2121
    .local v2, "minWidth":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x432c0000    # 172.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 2123
    .local v1, "minHeight":I
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getLocationOnScreen([I)V

    .line 2125
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2127
    .local v3, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    aget v4, v0, v6

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    if-ge v4, v5, :cond_0

    .line 2128
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2130
    :cond_0
    aget v4, v0, v7

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    if-ge v4, v5, :cond_1

    .line 2131
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2134
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    aget v5, v0, v6

    sub-int/2addr v4, v5

    if-ge v4, v2, :cond_2

    .line 2135
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int/2addr v4, v2

    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2137
    iget v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-gez v4, :cond_2

    .line 2138
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2141
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    aget v5, v0, v7

    sub-int/2addr v4, v5

    if-ge v4, v1, :cond_3

    .line 2142
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v4, v1

    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2144
    iget v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-gez v4, :cond_3

    .line 2145
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2149
    :cond_3
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2150
    return-void
.end method

.method private convertToArabicNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 2593
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2594
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 2601
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 2595
    :cond_0
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2596
    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->arabicChars:[C

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    add-int/lit8 v3, v3, -0x30

    aget-char v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2594
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2598
    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private cutterSettingClearAllButton()Landroid/view/View;
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    const/high16 v4, 0x41500000    # 13.0f

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v9, 0x0

    .line 1895
    new-instance v1, Landroid/widget/Button;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 1896
    .local v1, "clearAllButton":Landroid/widget/Button;
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    .line 1897
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42040000    # 33.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1896
    invoke-direct {v6, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1898
    .local v6, "clearAllButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 1899
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1900
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 1898
    invoke-virtual {v6, v0, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1901
    invoke-virtual {v1, v6}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1902
    invoke-virtual {v1, v11}, Landroid/widget/Button;->setFocusable(Z)V

    .line 1903
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_clear_all"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1904
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x3fc00000    # 1.5f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    .line 1905
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v0, v2, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1906
    :goto_0
    const/16 v2, 0x300

    .line 1905
    if-ne v0, v2, :cond_1

    .line 1907
    const v0, -0xdadadb

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 1912
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    cmpl-float v0, v0, v10

    if-nez v0, :cond_3

    .line 1913
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v0, v2, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1914
    :goto_2
    const/16 v2, 0x640

    .line 1913
    if-ne v0, v2, :cond_3

    .line 1916
    :try_start_0
    const-string v0, "/system/fonts/SECRobotoLight-Regular.ttf"

    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v8

    .line 1917
    .local v8, "type":Landroid/graphics/Typeface;
    invoke-virtual {v1, v8}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1921
    .end local v8    # "type":Landroid/graphics/Typeface;
    :goto_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v2, 0x41366666    # 11.4f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v9, v0}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1938
    :goto_4
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v2, 0x15

    if-ge v0, v2, :cond_6

    .line 1939
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 1940
    const-string/jumbo v2, "snote_popup_btn_normal"

    const-string/jumbo v3, "snote_popup_btn_press"

    const-string/jumbo v4, "snote_popup_btn_focus"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1949
    :goto_5
    return-object v1

    .line 1906
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_0

    .line 1909
    :cond_1
    const/high16 v0, -0x1000000

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_1

    .line 1914
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_2

    .line 1918
    :catch_0
    move-exception v7

    .line 1919
    .local v7, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v7}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_3

    .line 1923
    .end local v7    # "e":Ljava/lang/RuntimeException;
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    cmpl-float v0, v0, v10

    if-nez v0, :cond_5

    .line 1924
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v0, v2, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1925
    :goto_6
    const/16 v2, 0x600

    .line 1924
    if-ne v0, v2, :cond_5

    .line 1927
    :try_start_1
    const-string v0, "/system/fonts/SECRobotoLight-Regular.ttf"

    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v8

    .line 1928
    .restart local v8    # "type":Landroid/graphics/Typeface;
    invoke-virtual {v1, v8}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1932
    .end local v8    # "type":Landroid/graphics/Typeface;
    :goto_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v2, 0x4153851f    # 13.22f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v9, v0}, Landroid/widget/Button;->setTextSize(IF)V

    goto :goto_4

    .line 1925
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_6

    .line 1929
    :catch_1
    move-exception v7

    .line 1930
    .restart local v7    # "e":Ljava/lang/RuntimeException;
    invoke-virtual {v7}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_7

    .line 1934
    .end local v7    # "e":Ljava/lang/RuntimeException;
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41600000    # 14.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v9, v0}, Landroid/widget/Button;->setTextSize(IF)V

    goto :goto_4

    .line 1942
    :cond_6
    new-instance v0, Landroid/graphics/drawable/RippleDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v3, 0x40

    invoke-static {v3, v9, v9, v9}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 1943
    invoke-direct {v0, v2, v12, v12}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1942
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1944
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v2, "snote_popup_btn_normal"

    const-string/jumbo v3, "snote_popup_btn_press"

    .line 1945
    const-string/jumbo v4, "snote_popup_btn_focus"

    const/16 v5, 0x3d

    invoke-static {v5, v9, v9, v9}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    .line 1944
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1946
    invoke-virtual {v1, v11}, Landroid/widget/Button;->setAllCaps(Z)V

    goto/16 :goto_5
.end method

.method private cutterSettingSize()Landroid/view/ViewGroup;
    .locals 8

    .prologue
    const/high16 v6, 0x40a00000    # 5.0f

    const/4 v7, 0x0

    .line 1612
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1615
    .local v0, "cutterSettingSizeLayout":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1616
    const/4 v3, -0x1

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42400000    # 48.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 1615
    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1617
    .local v1, "cutterSettingSizeLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1618
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41000000    # 8.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 1617
    invoke-virtual {v1, v3, v7, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1619
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1621
    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    .line 1622
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1623
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    const/16 v4, 0x56

    const/16 v5, 0x57

    const/16 v6, 0x5b

    invoke-static {v4, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1624
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41400000    # 12.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v7, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1625
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    const/16 v4, 0x33

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 1627
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1628
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41600000    # 14.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 1627
    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1629
    .local v2, "eraserSizeTextParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1631
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1632
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1633
    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 1634
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->cutterSettingSizeDisplay()Landroid/view/ViewGroup;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeButton:Landroid/view/ViewGroup;

    .line 1635
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeButton:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1636
    return-object v0
.end method

.method private cutterSettingSizeDisplay()Landroid/view/ViewGroup;
    .locals 13
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1820
    new-instance v9, Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v9, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1821
    .local v9, "cutterSettingSizeDisplayLayout":Landroid/widget/RelativeLayout;
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    .line 1822
    const/4 v1, -0x1

    .line 1821
    invoke-direct {v10, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1823
    .local v10, "mParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1824
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    const/4 v1, 0x0

    .line 1825
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1824
    invoke-virtual {v9, v0, v1, v2, v3}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 1826
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    .line 1827
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1828
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41c00000    # 24.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c00000    # 24.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1827
    invoke-direct {v8, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1829
    .local v8, "cutterPlusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v8, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1830
    const/16 v0, 0xb

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1831
    const/16 v0, 0x8

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1833
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v8}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1834
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_plus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1835
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 1836
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_popup_progress_btn_plus_normal"

    const-string/jumbo v2, "snote_popup_progress_btn_plus_press"

    .line 1837
    const-string/jumbo v3, "snote_popup_progress_btn_plus_focus"

    const-string/jumbo v4, "snote_popup_progress_btn_plus_dim"

    const/16 v5, 0x18

    const/16 v6, 0x18

    .line 1836
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1848
    :goto_0
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    .line 1849
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1850
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41c00000    # 24.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c00000    # 24.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1849
    invoke-direct {v7, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1851
    .local v7, "cutterMinusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1852
    const/16 v0, 0x9

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1853
    const/16 v0, 0x8

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1855
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1856
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_minus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1857
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_2

    .line 1858
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_popup_progress_btn_minus_normal"

    const-string/jumbo v2, "snote_popup_progress_btn_minus_press"

    .line 1859
    const-string/jumbo v3, "snote_popup_progress_btn_minus_focus"

    const-string/jumbo v4, "snote_popup_progress_btn_minus_dim"

    const/16 v5, 0x18

    const/16 v6, 0x18

    .line 1858
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1871
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->cutterSettingSizeSeekBar()Landroid/widget/SeekBar;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    .line 1872
    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 1873
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1874
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1875
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1877
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    const v1, 0xb82f93

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setId(I)V

    .line 1878
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    const v1, 0xb82f92

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setId(I)V

    .line 1880
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    const v1, 0xb82f91

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setNextFocusUpId(I)V

    .line 1881
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    const v1, 0xb82f91

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setNextFocusUpId(I)V

    .line 1882
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    const v1, 0xb82f91

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setNextFocusUpId(I)V

    .line 1883
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    const v1, 0xb82f93

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setNextFocusLeftId(I)V

    .line 1884
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    const v1, 0xb82f92

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setNextFocusRightId(I)V

    .line 1885
    return-object v9

    .line 1838
    .end local v7    # "cutterMinusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 1839
    new-instance v11, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v0, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v0}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    invoke-direct {v11, v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 1840
    .local v11, "shapeDrawable":Landroid/graphics/drawable/ShapeDrawable;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    new-instance v1, Landroid/graphics/drawable/RippleDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v3, 0x2d

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 1841
    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v11}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1840
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1842
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_popup_progress_btn_plus_normal"

    const-string/jumbo v2, "snote_popup_progress_btn_plus_normal"

    const-string/jumbo v3, "snote_popup_progress_btn_plus_focus"

    .line 1843
    const-string/jumbo v4, "snote_popup_progress_btn_plus_dim"

    const/16 v5, 0x18

    const/16 v6, 0x18

    .line 1842
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 1845
    .end local v11    # "shapeDrawable":Landroid/graphics/drawable/ShapeDrawable;
    :cond_1
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_popup_progress_btn_plus_normal"

    const-string/jumbo v2, "snote_popup_progress_btn_plus_press"

    .line 1846
    const-string/jumbo v3, "snote_popup_progress_btn_plus_focus"

    const-string/jumbo v4, "snote_popup_progress_btn_plus_dim"

    const/16 v5, 0x18

    const/16 v6, 0x18

    .line 1845
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 1860
    .restart local v7    # "cutterMinusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_3

    .line 1861
    new-instance v11, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v0, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v0}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    invoke-direct {v11, v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 1862
    .restart local v11    # "shapeDrawable":Landroid/graphics/drawable/ShapeDrawable;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    new-instance v1, Landroid/graphics/drawable/RippleDrawable;

    .line 1863
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v3, 0x2d

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v11}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1862
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1864
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_popup_progress_btn_minus_normal"

    const-string/jumbo v2, "snote_popup_progress_btn_minus_normal"

    .line 1865
    const-string/jumbo v3, "snote_popup_progress_btn_minus_focus"

    const-string/jumbo v4, "snote_popup_progress_btn_minus_dim"

    const/16 v5, 0x18

    const/16 v6, 0x18

    .line 1864
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 1867
    .end local v11    # "shapeDrawable":Landroid/graphics/drawable/ShapeDrawable;
    :cond_3
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_popup_progress_btn_minus_normal"

    const-string/jumbo v2, "snote_popup_progress_btn_minus_press"

    .line 1868
    const-string/jumbo v3, "snote_popup_progress_btn_minus_focus"

    const-string/jumbo v4, "snote_popup_progress_btn_minus_dim"

    const/16 v5, 0x18

    const/16 v6, 0x18

    .line 1867
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1
.end method

.method private cutterSettingSizeSeekBar()Landroid/widget/SeekBar;
    .locals 20

    .prologue
    .line 1646
    new-instance v16, Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 1650
    .local v16, "seekBar":Landroid/widget/SeekBar;
    new-instance v17, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1651
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x433b0000    # 187.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 1652
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41b00000    # 22.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 1650
    move-object/from16 v0, v17

    invoke-direct {v0, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1654
    .local v17, "seekBarParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v4, 0x1

    move-object/from16 v0, v17

    iput-boolean v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1655
    const/16 v4, 0xe

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1656
    const/16 v4, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1657
    invoke-virtual/range {v16 .. v17}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1658
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x3fc00000    # 1.5f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    const/4 v5, 0x0

    .line 1659
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x3fc00000    # 1.5f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    const/4 v7, 0x0

    .line 1658
    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/widget/SeekBar;->setPadding(IIII)V

    .line 1661
    const/16 v4, 0x9

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1663
    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v5, 0x15

    if-lt v4, v5, :cond_1

    .line 1664
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v2, "progress_handle_normal"

    const-string v3, "progress_handle_normal"

    const-string v4, "progress_handle_focus"

    const/16 v5, 0x16

    const/16 v6, 0x16

    const/4 v7, 0x1

    invoke-virtual/range {v1 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 1670
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setThumbOffset(I)V

    .line 1672
    new-instance v11, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v11}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 1673
    .local v11, "localGradientDrawable":Landroid/graphics/drawable/GradientDrawable;
    const/4 v4, 0x0

    invoke-virtual {v11, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 1674
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40900000    # 4.5f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v11, v4}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 1676
    new-instance v10, Landroid/graphics/drawable/ClipDrawable;

    const/4 v4, 0x3

    const/4 v5, 0x1

    invoke-direct {v10, v11, v4, v5}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    .line 1677
    .local v10, "localClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v5, "progress_bg"

    const/16 v6, 0xbe

    const/16 v7, 0x9

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1678
    .local v2, "bgDrawable":Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v5, "progress_shadow"

    const/16 v6, 0xbe

    const/16 v7, 0x9

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v18

    .line 1679
    .local v18, "shadowDrawable":Landroid/graphics/drawable/Drawable;
    new-instance v1, Landroid/graphics/drawable/InsetDrawable;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 1680
    .local v1, "bgInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    new-instance v3, Landroid/graphics/drawable/InsetDrawable;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v4, v18

    invoke-direct/range {v3 .. v8}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 1682
    .local v3, "shadowInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    new-instance v12, Landroid/graphics/drawable/LayerDrawable;

    const/4 v4, 0x3

    new-array v4, v4, [Landroid/graphics/drawable/Drawable;

    const/4 v5, 0x0

    aput-object v10, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    const/4 v5, 0x2

    .line 1683
    aput-object v3, v4, v5

    .line 1682
    invoke-direct {v12, v4}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 1684
    .local v12, "localLayerDrawable":Landroid/graphics/drawable/LayerDrawable;
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1686
    :try_start_0
    const-class v4, Landroid/widget/ProgressBar;

    const-string v5, "mMinHeight"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v15

    .line 1687
    .local v15, "minHeight":Ljava/lang/reflect/Field;
    const/4 v4, 0x1

    invoke-virtual {v15, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 1688
    const-class v4, Landroid/widget/ProgressBar;

    const-string v5, "mMaxHeight"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v13

    .line 1689
    .local v13, "maxHeight":Ljava/lang/reflect/Field;
    const/4 v4, 0x1

    invoke-virtual {v13, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1692
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41100000    # 9.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v4}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 1693
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41100000    # 9.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    move-object/from16 v0, v16

    invoke-virtual {v13, v0, v4}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1703
    .end local v13    # "maxHeight":Ljava/lang/reflect/Field;
    .end local v15    # "minHeight":Ljava/lang/reflect/Field;
    :goto_1
    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v5, 0x15

    if-lt v4, v5, :cond_0

    .line 1705
    :try_start_2
    const-class v4, Landroid/widget/AbsSeekBar;

    const-string v5, "setSplitTrack"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    sget-object v8, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_4

    move-result-object v14

    .line 1708
    .local v14, "method":Ljava/lang/reflect/Method;
    const/4 v4, 0x1

    :try_start_3
    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_4

    .line 1721
    .end local v14    # "method":Ljava/lang/reflect/Method;
    :goto_2
    new-instance v4, Landroid/graphics/drawable/RippleDrawable;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v6, 0x40

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-static {v6, v7, v8, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    const/4 v6, 0x0

    .line 1722
    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, v7}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1721
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1724
    :cond_0
    return-object v16

    .line 1666
    .end local v1    # "bgInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    .end local v2    # "bgDrawable":Landroid/graphics/drawable/Drawable;
    .end local v3    # "shadowInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    .end local v10    # "localClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    .end local v11    # "localGradientDrawable":Landroid/graphics/drawable/GradientDrawable;
    .end local v12    # "localLayerDrawable":Landroid/graphics/drawable/LayerDrawable;
    .end local v18    # "shadowDrawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v2, "progress_handle_normal"

    const-string v3, "progress_handle_press"

    const-string v4, "progress_handle_focus"

    const/16 v5, 0x16

    const/16 v6, 0x16

    .line 1667
    const/4 v7, 0x1

    .line 1666
    invoke-virtual/range {v1 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 1694
    .restart local v1    # "bgInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    .restart local v2    # "bgDrawable":Landroid/graphics/drawable/Drawable;
    .restart local v3    # "shadowInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    .restart local v10    # "localClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    .restart local v11    # "localGradientDrawable":Landroid/graphics/drawable/GradientDrawable;
    .restart local v12    # "localLayerDrawable":Landroid/graphics/drawable/LayerDrawable;
    .restart local v13    # "maxHeight":Ljava/lang/reflect/Field;
    .restart local v15    # "minHeight":Ljava/lang/reflect/Field;
    .restart local v18    # "shadowDrawable":Landroid/graphics/drawable/Drawable;
    :catch_0
    move-exception v9

    .line 1695
    .local v9, "e":Ljava/lang/IllegalAccessException;
    :try_start_4
    invoke-virtual {v9}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/NoSuchFieldException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 1699
    .end local v9    # "e":Ljava/lang/IllegalAccessException;
    .end local v13    # "maxHeight":Ljava/lang/reflect/Field;
    .end local v15    # "minHeight":Ljava/lang/reflect/Field;
    :catch_1
    move-exception v9

    .line 1700
    .local v9, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v9}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_1

    .line 1696
    .end local v9    # "e":Ljava/lang/NoSuchFieldException;
    .restart local v13    # "maxHeight":Ljava/lang/reflect/Field;
    .restart local v15    # "minHeight":Ljava/lang/reflect/Field;
    :catch_2
    move-exception v9

    .line 1697
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    :try_start_5
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_5
    .catch Ljava/lang/NoSuchFieldException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 1709
    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    .end local v13    # "maxHeight":Ljava/lang/reflect/Field;
    .end local v15    # "minHeight":Ljava/lang/reflect/Field;
    .restart local v14    # "method":Ljava/lang/reflect/Method;
    :catch_3
    move-exception v9

    .line 1710
    .local v9, "e":Ljava/lang/IllegalAccessException;
    :try_start_6
    invoke-virtual {v9}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_6
    .catch Ljava/lang/NoSuchMethodException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_2

    .line 1717
    .end local v9    # "e":Ljava/lang/IllegalAccessException;
    .end local v14    # "method":Ljava/lang/reflect/Method;
    :catch_4
    move-exception v9

    .line 1718
    .local v9, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v9}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_2

    .line 1711
    .end local v9    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v14    # "method":Ljava/lang/reflect/Method;
    :catch_5
    move-exception v9

    .line 1712
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    :try_start_7
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 1713
    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    :catch_6
    move-exception v9

    .line 1714
    .local v9, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v9}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
    :try_end_7
    .catch Ljava/lang/NoSuchMethodException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_2
.end method

.method private cutterTypeLayout()Landroid/view/ViewGroup;
    .locals 3

    .prologue
    .line 1547
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localLinearLayout:Landroid/widget/LinearLayout;

    .line 1548
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localLinearLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1550
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1551
    const/4 v1, -0x1

    const/4 v2, -0x2

    .line 1550
    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1552
    .local v0, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1554
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localLinearLayout:Landroid/widget/LinearLayout;

    return-object v1
.end method

.method private cutterTypeSetting(Landroid/view/View;)V
    .locals 5
    .param p1, "selectedView"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2015
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 2056
    invoke-virtual {p1, v3}, Landroid/view/View;->setSelected(Z)V

    .line 2057
    return-void

    .line 2016
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    .line 2017
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2, v4}, Landroid/view/View;->setSelected(Z)V

    .line 2019
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2021
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    .line 2022
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    .line 2024
    if-nez v0, :cond_3

    .line 2025
    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    .line 2030
    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 2015
    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2026
    :cond_3
    if-ne v0, v3, :cond_1

    .line 2027
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    goto :goto_1

    .line 2032
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_2

    .line 2033
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v1

    .line 2034
    .local v1, "info":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    if-eqz v1, :cond_2

    .line 2035
    iput v4, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    .line 2036
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    goto :goto_2

    .line 2042
    .end local v1    # "info":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_2

    .line 2043
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v1

    .line 2044
    .restart local v1    # "info":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    if-eqz v1, :cond_2

    .line 2045
    iput v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    .line 2046
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    goto :goto_2

    .line 2030
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private eraseByStroke()Landroid/view/View;
    .locals 15

    .prologue
    .line 1954
    new-instance v13, Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v13, v0}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 1955
    .local v13, "typeCheckBox":Landroid/widget/CheckBox;
    new-instance v14, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    .line 1956
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x421c0000    # 39.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1955
    invoke-direct {v14, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1957
    .local v14, "typeCheckBoxParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41500000    # 13.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    add-int/lit8 v0, v0, 0x5

    const/4 v1, 0x0

    .line 1958
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41500000    # 13.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    const/4 v3, 0x0

    .line 1957
    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1959
    invoke-virtual {v13, v14}, Landroid/widget/CheckBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1960
    const/4 v0, 0x1

    invoke-virtual {v13, v0}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 1961
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v10

    .line 1962
    .local v10, "curLang":Ljava/lang/String;
    const-string/jumbo v0, "ur"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ar"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-le v0, v1, :cond_0

    .line 1963
    const/4 v0, 0x1

    invoke-virtual {v13, v0}, Landroid/widget/CheckBox;->setSingleLine(Z)V

    .line 1965
    :cond_0
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v13, v0}, Landroid/widget/CheckBox;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1966
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-le v0, v1, :cond_1

    .line 1967
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41500000    # 13.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v13, v0, v1, v2, v3}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 1974
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v1, "string_erase_line_by_line"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 1975
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_2

    .line 1976
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_btn_check_off"

    .line 1977
    const-string/jumbo v2, "snote_btn_check_on"

    const-string/jumbo v3, "snote_btn_check_on_focused"

    const-string/jumbo v4, "snote_btn_check_off_focused"

    .line 1978
    const-string/jumbo v5, "snote_btn_check_on_pressed"

    const-string/jumbo v6, "snote_btn_check_off_pressed"

    const/16 v7, 0x13

    const/16 v8, 0x13

    const/4 v9, 0x1

    .line 1976
    invoke-virtual/range {v0 .. v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableCheckedImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v13, v0}, Landroid/widget/CheckBox;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1983
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x3fc00000    # 1.5f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    .line 1984
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v0, v1, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1985
    :goto_2
    const/16 v1, 0x300

    .line 1984
    if-ne v0, v1, :cond_4

    .line 1986
    const v0, -0xdadadb

    invoke-virtual {v13, v0}, Landroid/widget/CheckBox;->setTextColor(I)V

    .line 1990
    :goto_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_6

    .line 1991
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v0, v1, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1992
    :goto_4
    const/16 v1, 0x640

    .line 1991
    if-ne v0, v1, :cond_6

    .line 1994
    :try_start_0
    const-string v0, "/system/fonts/SECRobotoLight-Regular.ttf"

    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v12

    .line 1995
    .local v12, "type":Landroid/graphics/Typeface;
    invoke-virtual {v13, v12}, Landroid/widget/CheckBox;->setTypeface(Landroid/graphics/Typeface;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1999
    .end local v12    # "type":Landroid/graphics/Typeface;
    :goto_5
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v2, 0x414e6666    # 12.9f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v13, v0, v1}, Landroid/widget/CheckBox;->setTextSize(IF)V

    .line 2003
    :goto_6
    const/4 v0, 0x1

    invoke-virtual {v13, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2005
    return-object v13

    .line 1970
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41f80000    # 31.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v13, v0, v1, v2, v3}, Landroid/widget/CheckBox;->setPadding(IIII)V

    goto/16 :goto_0

    .line 1980
    :cond_2
    new-instance v0, Landroid/graphics/drawable/RippleDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v2, 0x29

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 1981
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1980
    invoke-virtual {v13, v0}, Landroid/widget/CheckBox;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 1985
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_2

    .line 1988
    :cond_4
    const/high16 v0, -0x1000000

    invoke-virtual {v13, v0}, Landroid/widget/CheckBox;->setTextColor(I)V

    goto :goto_3

    .line 1992
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_4

    .line 1996
    :catch_0
    move-exception v11

    .line 1997
    .local v11, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v11}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_5

    .line 2001
    .end local v11    # "e":Ljava/lang/RuntimeException;
    :cond_6
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v13, v0, v1}, Landroid/widget/CheckBox;->setTextSize(IF)V

    goto :goto_6
.end method

.method private exitButton()Landroid/view/View;
    .locals 10

    .prologue
    const/16 v9, 0x29

    const/16 v6, 0x24

    const/4 v4, -0x1

    const/high16 v3, 0x42240000    # 41.0f

    const/16 v7, 0xff

    .line 1512
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1514
    .local v1, "exitButton":Landroid/widget/ImageButton;
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v2, 0x15

    if-ge v0, v2, :cond_0

    .line 1515
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1516
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_TOP_MARGIN:I

    sub-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x26

    div-int/lit8 v0, v0, 0x24

    .line 1515
    invoke-direct {v8, v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1518
    .local v8, "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v2, "snote_popup_close"

    const-string/jumbo v3, "snote_popup_close_press"

    const-string/jumbo v4, "snote_popup_close_focus"

    .line 1519
    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_WIDTH:I

    .line 1518
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1529
    :goto_0
    const/16 v0, 0xb

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1531
    invoke-virtual {v1, v8}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1532
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1533
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_close"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1535
    return-object v1

    .line 1521
    .end local v8    # "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1522
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_TOP_MARGIN:I

    sub-int/2addr v0, v2

    .line 1521
    invoke-direct {v8, v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1524
    .restart local v8    # "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v0, Landroid/graphics/drawable/RippleDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-static {v9, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 1525
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1524
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1526
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v2, "snote_popup_close"

    const-string/jumbo v3, "snote_popup_close_press"

    const-string/jumbo v4, "snote_popup_close_focus"

    .line 1527
    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_WIDTH:I

    invoke-static {v9, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    .line 1526
    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    goto :goto_0
.end method

.method private findMinValue(Landroid/widget/TextView;IF)V
    .locals 3
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "maxWidth"    # I
    .param p3, "currentFloat"    # F

    .prologue
    const/4 v2, 0x0

    .line 1303
    :goto_0
    invoke-virtual {p1, v2, v2}, Landroid/widget/TextView;->measure(II)V

    .line 1304
    invoke-virtual {p1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v0

    .line 1306
    .local v0, "width":I
    if-le v0, p2, :cond_0

    .line 1307
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr p3, v1

    .line 1308
    invoke-virtual {p1, v2, p3}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 1310
    :cond_0
    invoke-virtual {p1, v2, p3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1314
    return-void
.end method

.method private getMovableRect()Landroid/graphics/Rect;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2579
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 2580
    .local v0, "location":[I
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 2582
    .local v1, "r":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getLocationOnScreen([I)V

    .line 2584
    aget v2, v0, v4

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLeftMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 2585
    aget v2, v0, v5

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTopMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 2586
    aget v2, v0, v4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 2587
    aget v2, v0, v5

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 2589
    return-object v1
.end method

.method private initView()V
    .locals 3

    .prologue
    .line 1318
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->totalLayout()V

    .line 1319
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    .line 1320
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_WIDTH:I

    rsub-int v2, v2, 0xf7

    add-int/lit8 v2, v2, -0xe

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    add-int/lit8 v1, v1, -0x12

    .line 1321
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTextSize()F

    move-result v2

    .line 1319
    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->findMinValue(Landroid/widget/TextView;IF)V

    .line 1322
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1323
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 1325
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    if-nez v0, :cond_0

    .line 1326
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 1327
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    const/4 v1, 0x0

    new-instance v2, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    aput-object v2, v0, v1

    .line 1328
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    const/4 v1, 0x1

    new-instance v2, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    aput-object v2, v0, v1

    .line 1331
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 1332
    return-void
.end method

.method private rotatePosition()V
    .locals 14

    .prologue
    const v13, 0x3f7d70a4    # 0.99f

    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 2061
    const-string/jumbo v8, "settingui-settingRemover"

    const-string v9, "==== SettingRemover ===="

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2062
    const-string/jumbo v8, "settingui-settingRemover"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "old  = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 2063
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2062
    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2064
    const-string/jumbo v8, "settingui-settingRemover"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "new  = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 2065
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2064
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2067
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 2069
    .local v4, "r":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldLocation:[I

    aget v8, v8, v11

    iput v8, v4, Landroid/graphics/Rect;->left:I

    .line 2070
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldLocation:[I

    const/4 v9, 0x1

    aget v8, v8, v9

    iput v8, v4, Landroid/graphics/Rect;->top:I

    .line 2071
    iget v8, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getWidth()I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v4, Landroid/graphics/Rect;->right:I

    .line 2072
    iget v8, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getHeight()I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v4, Landroid/graphics/Rect;->bottom:I

    .line 2074
    const-string/jumbo v8, "settingui-settingRemover"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "view = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v4, Landroid/graphics/Rect;->right:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2076
    iget v8, v4, Landroid/graphics/Rect;->left:I

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v9

    int-to-float v2, v8

    .line 2077
    .local v2, "left":F
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    iget v9, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, v9

    int-to-float v5, v8

    .line 2078
    .local v5, "right":F
    iget v8, v4, Landroid/graphics/Rect;->top:I

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    sub-int/2addr v8, v9

    int-to-float v6, v8

    .line 2079
    .local v6, "top":F
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    iget v9, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v8, v9

    int-to-float v0, v8

    .line 2081
    .local v0, "bottom":F
    add-float v8, v2, v5

    div-float v1, v2, v8

    .line 2082
    .local v1, "hRatio":F
    add-float v8, v6, v0

    div-float v7, v6, v8

    .line 2084
    .local v7, "vRatio":F
    const-string/jumbo v8, "settingui-settingRemover"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "left :"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", right :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2085
    const-string/jumbo v8, "settingui-settingRemover"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "top :"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", bottom :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2086
    const-string/jumbo v8, "settingui-settingRemover"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "hRatio = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", vRatio = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2088
    cmpl-float v8, v1, v13

    if-lez v8, :cond_2

    .line 2089
    const/high16 v1, 0x3f800000    # 1.0f

    .line 2094
    :cond_0
    :goto_0
    cmpl-float v8, v7, v13

    if-lez v8, :cond_3

    .line 2095
    const/high16 v7, 0x3f800000    # 1.0f

    .line 2100
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2102
    .local v3, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    if-ge v8, v9, :cond_4

    .line 2103
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v9

    sub-int/2addr v8, v9

    int-to-float v8, v8

    mul-float/2addr v8, v1

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    iput v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2108
    :goto_2
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    if-ge v8, v9, :cond_5

    .line 2109
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v8

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v9

    sub-int/2addr v8, v9

    int-to-float v8, v8

    mul-float/2addr v8, v7

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    iput v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2114
    :goto_3
    const-string/jumbo v8, "settingui-settingRemover"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "lMargin = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", tMargin = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2115
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2116
    return-void

    .line 2090
    .end local v3    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_2
    cmpg-float v8, v1, v12

    if-gez v8, :cond_0

    .line 2091
    const/4 v1, 0x0

    goto :goto_0

    .line 2096
    :cond_3
    cmpg-float v8, v7, v12

    if-gez v8, :cond_1

    .line 2097
    const/4 v7, 0x0

    goto :goto_1

    .line 2105
    .restart local v3    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_4
    iput v11, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_2

    .line 2111
    :cond_5
    iput v11, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_3
.end method

.method private setEnableSizeSeekbar(Z)V
    .locals 11
    .param p1, "enabled"    # Z

    .prologue
    const/high16 v10, 0x41100000    # 9.0f

    const/high16 v9, 0x40a00000    # 5.0f

    const/high16 v8, 0x40800000    # 4.0f

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1728
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    if-eqz v2, :cond_6

    .line 1729
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v6}, Landroid/widget/SeekBar;->setPressed(Z)V

    .line 1733
    if-eqz p1, :cond_10

    .line 1734
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->isFirstClickChexBox:Z

    if-eqz v2, :cond_1

    .line 1735
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1736
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    .line 1737
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v3

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    mul-float/2addr v3, v4

    float-to-int v3, v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1739
    :cond_0
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->isFirstClickChexBox:Z

    .line 1741
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v7}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 1742
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1743
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1744
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1745
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ar"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "fa"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1746
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    .line 1747
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->convertToArabicNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1753
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    .line 1755
    .local v0, "progress":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    const/high16 v3, 0x40000000    # 2.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_d

    .line 1756
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v2, v3, :cond_8

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1757
    :goto_1
    const/16 v3, 0x640

    .line 1756
    if-ne v2, v3, :cond_d

    .line 1759
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42040000    # 33.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1760
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43250000    # 165.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v0

    div-float/2addr v4, v10

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1759
    add-int v1, v2, v3

    .line 1761
    .local v1, "seek_label_pos":I
    add-int/lit8 v2, v0, 0x1

    const/16 v3, 0xa

    if-lt v2, v3, :cond_9

    .line 1762
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    sub-int/2addr v1, v2

    .line 1775
    :cond_3
    :goto_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    int-to-float v3, v1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setX(F)V

    .line 1776
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setY(F)V

    .line 1777
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    int-to-float v3, v1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setX(F)V

    .line 1778
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setY(F)V

    .line 1780
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeChekBox:Landroid/view/View;

    check-cast v2, Landroid/widget/CompoundButton;

    invoke-virtual {v2, v6}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 1781
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-eq v2, v7, :cond_5

    .line 1782
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getMax()I

    move-result v3

    if-ne v2, v3, :cond_e

    .line 1783
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1784
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1785
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoIncrement:Z

    if-eqz v2, :cond_4

    .line 1786
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoIncrement:Z

    .line 1791
    :cond_4
    :goto_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    if-nez v2, :cond_f

    .line 1792
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1793
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1794
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoDecrement:Z

    if-eqz v2, :cond_5

    .line 1795
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoDecrement:Z

    .line 1801
    :cond_5
    :goto_4
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1811
    .end local v0    # "progress":I
    .end local v1    # "seek_label_pos":I
    :cond_6
    :goto_5
    return-void

    .line 1749
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1757
    .restart local v0    # "progress":I
    :cond_8
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    goto/16 :goto_1

    .line 1763
    .restart local v1    # "seek_label_pos":I
    :cond_9
    if-eqz v0, :cond_a

    const/4 v2, 0x4

    if-ne v0, v2, :cond_b

    .line 1764
    :cond_a
    add-int/lit8 v1, v1, -0x1

    .line 1765
    goto/16 :goto_2

    :cond_b
    const/4 v2, 0x2

    if-eq v0, v2, :cond_c

    const/16 v2, 0x8

    if-eq v0, v2, :cond_c

    const/4 v2, 0x7

    if-ne v0, v2, :cond_3

    .line 1766
    :cond_c
    add-int/lit8 v1, v1, 0x1

    .line 1768
    goto/16 :goto_2

    .line 1769
    .end local v1    # "seek_label_pos":I
    :cond_d
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41f00000    # 30.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 1770
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43290000    # 169.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v0

    div-float/2addr v4, v10

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1769
    add-int v1, v2, v3

    .line 1771
    .restart local v1    # "seek_label_pos":I
    add-int/lit8 v2, v0, 0x1

    const/16 v3, 0xa

    if-lt v2, v3, :cond_3

    .line 1772
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    sub-int/2addr v1, v2

    goto/16 :goto_2

    .line 1789
    :cond_e
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto/16 :goto_3

    .line 1798
    :cond_f
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_4

    .line 1803
    .end local v0    # "progress":I
    .end local v1    # "seek_label_pos":I
    :cond_10
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v6}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 1804
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1805
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1806
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1807
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeChekBox:Landroid/view/View;

    check-cast v2, Landroid/widget/CompoundButton;

    invoke-virtual {v2, v7}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 1808
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_5
.end method

.method private setListener()V
    .locals 4

    .prologue
    .line 1335
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleLayout:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 1336
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleLayout:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1339
    :cond_0
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mGestureDetector:Landroid/view/GestureDetector;

    .line 1341
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButton:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 1342
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1345
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_2

    .line 1347
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMinusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1348
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1349
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1350
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMinusButtonKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1353
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_3

    .line 1354
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mPlusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1355
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1356
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1357
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mPlusButtonKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1360
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_4

    .line 1361
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 1364
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    if-eqz v1, :cond_5

    .line 1365
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1367
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeChekBox:Landroid/view/View;

    if-eqz v1, :cond_6

    .line 1368
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeChekBox:Landroid/view/View;

    check-cast v1, Landroid/widget/CompoundButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1371
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    array-length v1, v1

    if-lez v1, :cond_7

    .line 1372
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x1

    if-le v0, v1, :cond_9

    .line 1379
    .end local v0    # "i":I
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_8

    .line 1380
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1383
    :cond_8
    return-void

    .line 1373
    .restart local v0    # "i":I
    :cond_9
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    aget-object v1, v1, v0

    if-eqz v1, :cond_a

    .line 1374
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeListner:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1372
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private titleBg()Landroid/view/View;
    .locals 14

    .prologue
    const/16 v13, 0x8

    const/4 v12, 0x1

    const/4 v11, -0x2

    const/high16 v10, 0x42f60000    # 123.0f

    const/4 v9, -0x1

    .line 1023
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v7}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1024
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v7, v9, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1026
    new-instance v2, Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v2, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1027
    .local v2, "titleLeft":Landroid/widget/ImageView;
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1028
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v7, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 1027
    invoke-direct {v3, v7, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1029
    .local v3, "titleLeftParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v12, v3, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1030
    const/16 v7, 0x9

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1031
    const/16 v7, 0xa

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1033
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1035
    new-instance v7, Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    .line 1036
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v11, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1038
    .local v1, "titleCenterParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1040
    new-instance v4, Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v4, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1041
    .local v4, "titleRight":Landroid/widget/ImageView;
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1042
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x43770000    # 247.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 1043
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v8, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 1042
    sub-int/2addr v7, v8

    .line 1041
    invoke-direct {v6, v7, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1044
    .local v6, "titleRightParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v12, v6, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1045
    const/16 v7, 0xb

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1046
    const/16 v7, 0xa

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1047
    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->RIGHT_INDICATOR_WIDTH:I

    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1048
    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1050
    new-instance v7, Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    .line 1051
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, v11, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1053
    .local v5, "titleRightIndicatorParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v7, 0xb

    invoke-virtual {v5, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1054
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1056
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v8, "snote_popup_title_left"

    invoke-virtual {v7, v2, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1057
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    const-string/jumbo v9, "snote_popup_title_center"

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1058
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    const-string/jumbo v9, "snote_popup_title_bended"

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1059
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v8, "snote_popup_title_right"

    invoke-virtual {v7, v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1061
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1062
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1063
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1064
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1065
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1067
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1068
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1069
    return-object v0
.end method

.method private titleLayout()Landroid/view/ViewGroup;
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/high16 v6, 0x42240000    # 41.0f

    const/4 v5, 0x0

    .line 994
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 995
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 996
    invoke-virtual {v4, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-direct {v3, v7, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 995
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 997
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->exitButton()Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButton:Landroid/view/View;

    .line 998
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButton:Landroid/view/View;

    const v4, 0xb82f91

    invoke-virtual {v3, v4}, Landroid/view/View;->setId(I)V

    .line 999
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->titleBg()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1000
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->titleText()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1001
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1002
    .local v1, "mButtonLayout":Landroid/widget/RelativeLayout;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1003
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1002
    invoke-direct {v2, v7, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1004
    .local v2, "mButtonLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1005
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 1006
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_TOP_MARGIN:I

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1007
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->RIGHT_INDICATOR_WIDTH:I

    add-int/lit8 v3, v3, 0x5

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1008
    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_0

    .line 1009
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v1, v5, v5, v3, v5}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 1011
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButton:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1012
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1013
    return-object v0
.end method

.method private titleText()Landroid/view/View;
    .locals 11

    .prologue
    const/16 v10, 0x13

    const/4 v9, 0x1

    const/4 v8, -0x1

    const/high16 v6, 0x40000000    # 2.0f

    const/4 v7, 0x0

    .line 1074
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1075
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42240000    # 41.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 1074
    invoke-direct {v2, v8, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1076
    .local v2, "titleLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->EXIT_BUTTON_TOP_MARGIN:I

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1077
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->RIGHT_INDICATOR_WIDTH:I

    add-int/lit8 v4, v4, 0x5

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1078
    new-instance v4, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    .line 1079
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1080
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setGravity(I)V

    .line 1081
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 1082
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v5, "string_eraser_settings"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1083
    .local v1, "eraserText":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1084
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1085
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1087
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    cmpl-float v4, v4, v6

    if-nez v4, :cond_1

    .line 1088
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v4, v5, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1089
    :goto_0
    const/16 v5, 0x640

    .line 1088
    if-ne v4, v5, :cond_1

    .line 1091
    :try_start_0
    const-string v4, "/system/fonts/SECRobotoLight-Regular.ttf"

    invoke-static {v4}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    .line 1092
    .local v3, "type":Landroid/graphics/Typeface;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1096
    .end local v3    # "type":Landroid/graphics/Typeface;
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    const v5, -0xa0a0b

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1102
    :goto_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x1c

    if-le v4, v5, :cond_2

    .line 1103
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41400000    # 12.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4, v7, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1116
    :goto_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v6, "string_eraser_settings"

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1117
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41200000    # 10.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    add-int/lit8 v5, v5, 0x9

    invoke-virtual {v4, v5, v7, v7, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1118
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    return-object v4

    .line 1089
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_0

    .line 1093
    :catch_0
    move-exception v0

    .line 1094
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_1

    .line 1098
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 1104
    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v10, :cond_3

    .line 1105
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41600000    # 14.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4, v7, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_3

    .line 1107
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    cmpl-float v4, v4, v6

    if-nez v4, :cond_5

    .line 1108
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v4, v5, :cond_4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1109
    :goto_4
    const/16 v5, 0x640

    .line 1108
    if-ne v4, v5, :cond_5

    .line 1110
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v6, 0x41633333    # 14.2f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4, v7, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_3

    .line 1109
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_4

    .line 1112
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41800000    # 16.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4, v7, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    goto/16 :goto_3
.end method

.method private totalLayout()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/high16 v4, 0x43770000    # 247.0f

    .line 970
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    float-to-double v0, v0

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    .line 971
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 972
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->RIGHT_INDICATOR_WIDTH:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x432f0000    # 175.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 971
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 977
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setOrientation(I)V

    .line 978
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSdkVersion:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 979
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setLayoutDirection(I)V

    .line 981
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->titleLayout()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleLayout:Landroid/view/View;

    .line 982
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->bodyLayout()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mBodyLayout:Landroid/view/View;

    .line 983
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleLayout:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->addView(Landroid/view/View;)V

    .line 984
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->addView(Landroid/view/View;)V

    .line 985
    return-void

    .line 974
    :cond_1
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 975
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->RIGHT_INDICATOR_WIDTH:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x432c0000    # 172.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 974
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1219
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .line 1220
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 1221
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRemoverContext:Landroid/content/Context;

    .line 1222
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 1223
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    .line 1224
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllListener:Landroid/view/View$OnClickListener;

    .line 1225
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 1226
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    .line 1227
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mPlusButtonListener:Landroid/view/View$OnClickListener;

    .line 1228
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMinusButtonListener:Landroid/view/View$OnClickListener;

    .line 1229
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 1230
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

    .line 1231
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

    .line 1232
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

    .line 1233
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 1234
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeListner:Landroid/view/View$OnClickListener;

    .line 1235
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterKeyListener:Landroid/view/View$OnKeyListener;

    .line 1237
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->repeatUpdateHandler:Landroid/os/Handler;

    .line 1238
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    if-nez v1, :cond_0

    .line 1299
    :goto_0
    return-void

    .line 1242
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1243
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1244
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    .line 1245
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeButton:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1246
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeButton:Landroid/view/ViewGroup;

    .line 1247
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1248
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    .line 1249
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1250
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    .line 1251
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1252
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButton:Landroid/view/View;

    .line 1253
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1254
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterPlusButton:Landroid/widget/ImageButton;

    .line 1255
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1256
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterMinusButton:Landroid/widget/ImageButton;

    .line 1257
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1258
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    .line 1259
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeChekBox:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1260
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeChekBox:Landroid/view/View;

    .line 1262
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1263
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingSizeLayout:Landroid/view/ViewGroup;

    .line 1264
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType01:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1265
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType01:Landroid/widget/ImageButton;

    .line 1266
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType02:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1267
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType02:Landroid/widget/ImageButton;

    .line 1268
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    .line 1269
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_3

    .line 1274
    .end local v0    # "i":I
    :cond_1
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mEraserSizeTextView:Landroid/widget/TextView;

    .line 1275
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    .line 1276
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1277
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    .line 1278
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1279
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleLayout:Landroid/view/View;

    .line 1280
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1281
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mBodyLayout:Landroid/view/View;

    .line 1283
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 1284
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    if-eqz v1, :cond_2

    .line 1285
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    const/4 v2, 0x0

    aput-object v3, v1, v2

    .line 1286
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    const/4 v2, 0x1

    aput-object v3, v1, v2

    .line 1287
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 1289
    :cond_2
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mGestureDetector:Landroid/view/GestureDetector;

    .line 1290
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1291
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

    .line 1292
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;

    .line 1293
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;

    .line 1294
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1295
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 1296
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->close()V

    .line 1297
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 1298
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->close()V

    goto/16 :goto_0

    .line 1270
    .restart local v0    # "i":I
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1271
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    aput-object v3, v1, v0

    .line 1269
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    .locals 1

    .prologue
    .line 2301
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    return-object v0
.end method

.method public getViewMode()I
    .locals 1

    .prologue
    .line 2285
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mViewMode:I

    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v1, 0x1

    .line 83
    const-string/jumbo v2, "settingui-settingRemover"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onConfig remover "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getVisibility()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 87
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 88
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldLocation:[I

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getLocationOnScreen([I)V

    .line 97
    :cond_0
    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsRotated:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 102
    return-void

    .line 91
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldLocation:[I

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getLocationOnScreen([I)V

    .line 92
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsFirstShown:Z

    if-nez v2, :cond_0

    .line 93
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRecalculateRotate:Z

    if-eqz v2, :cond_2

    const/4 v1, 0x0

    :cond_2
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRecalculateRotate:Z

    .line 94
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 98
    :catch_0
    move-exception v0

    .line 99
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 11
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 2207
    if-ne p1, p0, :cond_0

    .line 2208
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;

    if-eqz v4, :cond_0

    .line 2210
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;

    invoke-interface {v4, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;->onVisibilityChanged(I)V

    .line 2213
    :cond_0
    if-ne p1, p0, :cond_4

    .line 2214
    if-nez p2, :cond_7

    .line 2215
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButton:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2216
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2217
    const/4 v4, 0x2

    new-array v1, v4, [I

    .line 2218
    .local v1, "location":[I
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getLocationInWindow([I)V

    .line 2219
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsFirstShown:Z

    if-eqz v4, :cond_1

    .line 2220
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsFirstShown:Z

    .line 2222
    :cond_1
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRecalculateRotate:Z

    if-eqz v4, :cond_5

    .line 2224
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2226
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRotateWhenSetPosition:Z

    if-eqz v4, :cond_2

    .line 2227
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->rotatePosition()V

    .line 2230
    :cond_2
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRecalculateRotate:Z

    .line 2253
    :cond_3
    :goto_0
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsRotated:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2265
    .end local v1    # "location":[I
    :cond_4
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onVisibilityChanged(Landroid/view/View;I)V

    .line 2266
    return-void

    .line 2234
    .restart local v1    # "location":[I
    :cond_5
    :try_start_1
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedCalculateMargin:Z

    if-eqz v4, :cond_6

    .line 2235
    const/4 v4, 0x2

    new-array v2, v4, [I

    .line 2236
    .local v2, "parentLocation":[I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v2}, Landroid/widget/RelativeLayout;->getLocationInWindow([I)V

    .line 2238
    const/4 v4, 0x0

    aget v4, v1, v4

    const/4 v5, 0x0

    aget v5, v2, v5

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mLeftMargin:I

    .line 2239
    const/4 v4, 0x1

    aget v4, v1, v4

    const/4 v5, 0x1

    aget v5, v2, v5

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTopMargin:I

    .line 2241
    const/4 v4, 0x2

    new-array v3, v4, [I

    .line 2242
    .local v3, "rootLocation":[I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getRootView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2244
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedCalculateMargin:Z

    .line 2246
    .end local v2    # "parentLocation":[I
    .end local v3    # "rootLocation":[I
    :cond_6
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2248
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    aget v6, v1, v6

    const/4 v7, 0x1

    aget v7, v1, v7

    const/4 v8, 0x0

    aget v8, v1, v8

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getWidth()I

    move-result v9

    add-int/2addr v8, v9

    .line 2249
    const/4 v9, 0x1

    aget v9, v1, v9

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getHeight()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2248
    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v4

    .line 2249
    if-nez v4, :cond_3

    .line 2250
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->checkPosition()V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2262
    .end local v1    # "location":[I
    :catch_0
    move-exception v0

    .line 2263
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 2256
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_7
    :try_start_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mExitButton:Landroid/view/View;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2257
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2258
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRotateWhenSetPosition:Z

    .line 2259
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRecalculateRotate:Z
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    if-nez v0, :cond_0

    .line 65
    invoke-super {p0}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 68
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    .line 69
    return-void
.end method

.method public setActionListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;

    .prologue
    .line 2552
    if-eqz p1, :cond_0

    .line 2553
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;

    .line 2555
    :cond_0
    return-void
.end method

.method public setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V
    .locals 2
    .param p1, "canvasView"    # Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .prologue
    .line 2403
    if-eqz p1, :cond_1

    .line 2404
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .line 2405
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    if-eqz v0, :cond_3

    .line 2406
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v0, v1, :cond_2

    .line 2407
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    .line 2412
    :goto_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    const/16 v1, 0x5f0

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    const/16 v1, 0x5fc

    if-ne v0, v1, :cond_1

    .line 2413
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    .line 2419
    :cond_1
    :goto_1
    return-void

    .line 2409
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    goto :goto_0

    .line 2416
    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    goto :goto_1
.end method

.method public setIndicatorPosition(I)V
    .locals 11
    .param p1, "x"    # I

    .prologue
    const/high16 v10, 0x43770000    # 247.0f

    const/high16 v9, 0x41f00000    # 30.0f

    const/4 v8, 0x1

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 1401
    if-gez p1, :cond_1

    .line 1402
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1403
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1404
    const/16 v1, -0x63

    if-ne p1, v1, :cond_0

    .line 1405
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMoveSettingLayout:Z

    .line 1406
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mFirstLongPress:Z

    .line 1443
    :goto_0
    return-void

    .line 1408
    :cond_0
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMoveSettingLayout:Z

    goto :goto_0

    .line 1411
    :cond_1
    const/16 v1, 0x9

    if-ge p1, v1, :cond_2

    .line 1412
    const/16 p1, 0x9

    .line 1414
    :cond_2
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMoveSettingLayout:Z

    .line 1415
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1416
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    float-to-double v2, v1

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_4

    .line 1417
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1418
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x9

    if-le p1, v1, :cond_3

    .line 1419
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1420
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1441
    :goto_1
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mFirstLongPress:Z

    goto :goto_0

    .line 1422
    :cond_3
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1423
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    const/4 v2, -0x1

    .line 1422
    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1424
    .local v0, "titleCenterParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, p1, v6, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1425
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1426
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 1429
    .end local v0    # "titleCenterParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    add-int/lit8 v1, v1, -0x2d

    add-int/lit8 v1, v1, -0x9

    if-le p1, v1, :cond_5

    .line 1430
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1431
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 1433
    :cond_5
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1434
    const/4 v1, -0x2

    const/4 v2, -0x1

    .line 1433
    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1435
    .restart local v0    # "titleCenterParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, p1, v6, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1436
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1437
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public setInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 5
    .param p1, "settingCutterInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 2437
    if-nez p1, :cond_0

    .line 2468
    :goto_0
    return-void

    .line 2441
    :cond_0
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    .line 2442
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    if-eqz v0, :cond_5

    .line 2443
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v0, v1, :cond_4

    .line 2444
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    .line 2449
    :goto_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    const/16 v1, 0x5f0

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    const/16 v1, 0x5fc

    if-ne v0, v1, :cond_2

    .line 2450
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    .line 2455
    :cond_2
    :goto_2
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_3

    .line 2456
    iput v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 2458
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    aget-object v0, v0, v1

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 2460
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    float-to-int v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 2461
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-ne v0, v4, :cond_6

    .line 2462
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setEnableSizeSeekbar(Z)V

    .line 2466
    :goto_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 2467
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    goto :goto_0

    .line 2446
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    goto :goto_1

    .line 2453
    :cond_5
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mStandardCanvasSize:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    goto :goto_2

    .line 2464
    :cond_6
    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setEnableSizeSeekbar(Z)V

    goto :goto_3
.end method

.method public setPosition(II)V
    .locals 9
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v8, 0x0

    .line 1475
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mNeedRotateWhenSetPosition:Z

    .line 1476
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->handlerRotate:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->runnableRotate:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1477
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->handlerRotate:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->runnableRotate:Ljava/lang/Runnable;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1479
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1481
    .local v2, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43770000    # 247.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->RIGHT_INDICATOR_WIDTH:I

    add-int/2addr v3, v4

    add-int/lit8 v1, v3, 0x2

    .line 1482
    .local v1, "minWidth":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x432c0000    # 172.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 1484
    .local v0, "minHeight":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int/2addr v3, v1

    if-le p1, v3, :cond_0

    .line 1485
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int p1, v3, v1

    .line 1487
    :cond_0
    if-gez p1, :cond_1

    .line 1488
    const/4 p1, 0x0

    .line 1490
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int/2addr v3, v0

    if-le p2, v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-le v3, v0, :cond_2

    .line 1491
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int p2, v3, v0

    .line 1493
    :cond_2
    if-ltz p2, :cond_3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-gt v3, v0, :cond_4

    .line 1494
    :cond_3
    const/4 p2, 0x0

    .line 1497
    :cond_4
    iput p1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1498
    iput p2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1499
    const-string/jumbo v3, "settingui-settingRemover"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "set Position x,y : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1500
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1501
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1502
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldLocation:[I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, p1

    aput v4, v3, v8

    .line 1503
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldLocation:[I

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, p2

    aput v5, v3, v4

    .line 1504
    return-void
.end method

.method public setRemoverInfoList([Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 0
    .param p1, "list"    # [Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    .line 2511
    if-eqz p1, :cond_0

    .line 2512
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 2514
    :cond_0
    return-void
.end method

.method public setRemoverListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

    .prologue
    .line 2531
    if-eqz p1, :cond_0

    .line 2532
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

    .line 2534
    :cond_0
    return-void
.end method

.method public setViewMode(I)V
    .locals 7
    .param p1, "viewMode"    # I

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 2321
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mViewMode:I

    .line 2324
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    .line 2325
    .local v2, "tempRequestLayoutDisable":Z
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    .line 2328
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mViewMode:I

    if-nez v3, :cond_0

    .line 2329
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2330
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2331
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2358
    :goto_0
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->requestLayoutDisable:Z

    .line 2360
    return-void

    .line 2332
    :cond_0
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mViewMode:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 2333
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->addCutterLayout()V

    .line 2334
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2335
    .local v0, "cutterTypeViewGroup":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    new-array v3, v3, [Landroid/view/View;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    .line 2336
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 2339
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2340
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2341
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 2337
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeView:[Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    aput-object v4, v3, v1

    .line 2336
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2342
    .end local v0    # "cutterTypeViewGroup":Landroid/widget/LinearLayout;
    .end local v1    # "i":I
    :cond_2
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mViewMode:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 2344
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2345
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2346
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 2347
    :cond_3
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mViewMode:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    .line 2348
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2349
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2351
    :cond_4
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mViewMode:I

    .line 2352
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2353
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2354
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    .line 2372
    if-nez p1, :cond_1

    .line 2373
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-boolean v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mLoaded:Z

    if-nez v1, :cond_0

    .line 2374
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->loadImage()V

    .line 2376
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsShowing:Z

    .line 2377
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterTypeChekBox:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    .line 2381
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2385
    :goto_1
    return-void

    .line 2379
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsShowing:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2382
    :catch_0
    move-exception v0

    .line 2383
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method public setVisibilityChangedListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;

    .prologue
    .line 2573
    if-eqz p1, :cond_0

    .line 2574
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ViewListener;

    .line 2576
    :cond_0
    return-void
.end method
