.class public final Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;
.super Ljava/lang/Object;
.source "SlinkNetworkManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SlinkWakeLock"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock$WakeLockRevokedReceiver;,
        Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock$MyServiceConnection;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SlinkWakeLock"


# instance fields
.field private final context:Landroid/content/Context;

.field private final finalizerGuardian:Ljava/lang/Object;

.field private held:Z

.field private final mWakeLockRevokedListener:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock$WakeLockRevokedReceiver;

.field private final serviceConnection:Landroid/content/ServiceConnection;

.field private final tag:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->held:Z

    .line 219
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock$MyServiceConnection;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock$MyServiceConnection;-><init>(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$1;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->serviceConnection:Landroid/content/ServiceConnection;

    .line 221
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock$WakeLockRevokedReceiver;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock$WakeLockRevokedReceiver;-><init>(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$1;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->mWakeLockRevokedListener:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock$WakeLockRevokedReceiver;

    .line 226
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock$1;-><init>(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->finalizerGuardian:Ljava/lang/Object;

    .line 240
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "tag must be non-empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 243
    :cond_0
    iput-object p2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->tag:Ljava/lang/String;

    .line 244
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->context:Landroid/content/Context;

    .line 245
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$1;

    .prologue
    .line 211
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->tag:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public acquire()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 257
    iget-object v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v3

    .line 258
    .local v3, "siUtils":Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isUpgradeAvailable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 259
    invoke-virtual {v3}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getPlatformUpgradeIntent()Landroid/content/Intent;

    move-result-object v2

    .line 260
    .local v2, "intent":Landroid/content/Intent;
    const/high16 v4, 0x10000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 261
    iget-object v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->context:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 264
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    iget-boolean v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->held:Z

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->context:Landroid/content/Context;

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.samsung.android.sdk.samsunglink.SlinkNetworkManager.NETWORK_LOCK_SERVICE"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->serviceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v4, v5, v6, v8}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 269
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 270
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v4, "com.samsung.android.sdk.samsunglink.SlinkNetworkManager.BROADCAST_WAKE_LOCKS_REVOKED"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 271
    iget-object v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->context:Landroid/content/Context;

    iget-object v5, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->mWakeLockRevokedListener:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock$WakeLockRevokedReceiver;

    const-string v6, "com.samsung.android.sdk.samsunglink.permission.BROADCAST_SAMSUNG_LINK"

    invoke-virtual {v4, v5, v1, v6, v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 276
    iput-boolean v8, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->held:Z

    .line 283
    .end local v1    # "filter":Landroid/content/IntentFilter;
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    const-string v6, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.NetworkLockRequested.NAME"

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 293
    :goto_0
    sget-boolean v4, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v4, :cond_2

    .line 294
    const-string v4, "SlinkWakeLock"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "::acquire success = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->held:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " tag = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->tag:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    :cond_2
    return-void

    .line 288
    :catch_0
    move-exception v0

    .line 290
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string/jumbo v4, "slinklib"

    const-string v5, "::acquire maybe platform is disabled"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isHeld()Z
    .locals 1

    .prologue
    .line 322
    iget-boolean v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->held:Z

    return v0
.end method

.method public release()V
    .locals 3

    .prologue
    .line 306
    iget-boolean v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->held:Z

    if-eqz v0, :cond_0

    .line 307
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->held:Z

    .line 308
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->mWakeLockRevokedListener:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock$WakeLockRevokedReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 309
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->serviceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 310
    sget-boolean v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v0, :cond_0

    .line 311
    const-string v0, "SlinkWakeLock"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "::release tag = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    :cond_0
    return-void
.end method
