.class Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$ListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SPenFontSizeDropdown2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p4, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;

    .line 41
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$ListAdapter;->items:Ljava/util/List;

    .line 42
    iput-object p4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$ListAdapter;->items:Ljava/util/List;

    .line 43
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$ListAdapter;->context:Landroid/content/Context;

    .line 44
    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 48
    if-nez p2, :cond_0

    .line 49
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$ListAdapter;->context:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 50
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x1090008

    invoke-virtual {v0, v2, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 52
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v2, 0x1020014

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 54
    .local v1, "tv":Landroid/widget/TextView;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 55
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 56
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$ListAdapter;->items:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 58
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x41900000    # 18.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 59
    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 60
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x41100000    # 9.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2, v4, v4, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 61
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x42240000    # 41.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMinimumHeight(I)V

    .line 63
    return-object p2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 69
    if-nez p2, :cond_0

    .line 70
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$ListAdapter;->context:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 71
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x1090008

    invoke-virtual {v0, v2, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 73
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v2, 0x1020014

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 75
    .local v1, "tv":Landroid/widget/TextView;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 76
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 77
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$ListAdapter;->items:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 79
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x41900000    # 18.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 80
    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 81
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x41100000    # 9.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2, v4, v4, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 82
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x42240000    # 41.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMinimumHeight(I)V

    .line 90
    return-object p2
.end method
