.class Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;
.super Ljava/lang/Object;
.source "SpenStrokeFrame.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->startRecognition()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    .line 1198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Ljava/util/List;Ljava/util/List;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1203
    .local p1, "input":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    .local p2, "output":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez p2, :cond_1

    .line 1204
    const-string v11, "SpenStrokeFrame"

    const-string v12, "Beautify result is null. so this action is canceled"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1205
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/16 v12, 0x8

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    .line 1307
    :cond_0
    :goto_0
    return-void

    .line 1209
    :cond_1
    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    const-string v12, "ShapeType"

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getExtraDataString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "Line"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1210
    const-string v11, "SpenStrokeFrame"

    const-string v12, "The result is a line. so this action is canceled"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1211
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/4 v12, 0x4

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto :goto_0

    .line 1215
    :cond_2
    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .line 1216
    .local v9, "resultContainer":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_4

    .line 1220
    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 1221
    .local v4, "otherPath":Landroid/graphics/Path;
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    .line 1222
    .local v5, "otherRect":Landroid/graphics/RectF;
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v11

    if-nez v11, :cond_5

    .line 1223
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v13}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v13

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    invoke-static {v12, v13}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V

    .line 1224
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v12

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    invoke-static {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v4

    .line 1230
    :goto_2
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v11

    if-eqz v11, :cond_3

    if-nez v4, :cond_6

    .line 1231
    :cond_3
    const-string v11, "SpenStrokeFrame"

    const-string v12, "Cancel cause failed to recognize"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1232
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/16 v12, 0x8

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    .line 1216
    .end local v4    # "otherPath":Landroid/graphics/Path;
    .end local v5    # "otherRect":Landroid/graphics/RectF;
    :cond_4
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 1217
    .local v3, "o":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v12

    invoke-virtual {v12, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto :goto_1

    .line 1226
    .end local v3    # "o":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .restart local v4    # "otherPath":Landroid/graphics/Path;
    .restart local v5    # "otherRect":Landroid/graphics/RectF;
    :cond_5
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v13}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v13

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    invoke-static {v12, v13}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V

    .line 1227
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v12

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    invoke-static {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v4

    goto :goto_2

    .line 1235
    :cond_6
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 1236
    const/4 v11, 0x0

    invoke-virtual {v4, v5, v11}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 1239
    new-instance v1, Landroid/graphics/RectF;

    const/4 v11, 0x0

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageWidth:I
    invoke-static {v13}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$57(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v13

    int-to-float v13, v13

    iget-object v14, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageHeight:I
    invoke-static {v14}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$58(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v14

    int-to-float v14, v14

    invoke-direct {v1, v11, v12, v13, v14}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1240
    .local v1, "absolutePageRect":Landroid/graphics/RectF;
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertRelative(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    invoke-static {v11, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v6

    .line 1241
    .local v6, "relativePageRect":Landroid/graphics/RectF;
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertAbsolute(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    invoke-static {v11, v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$59(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v1

    .line 1242
    invoke-virtual {v1, v5}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v11

    if-eqz v11, :cond_7

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v11

    invoke-virtual {v1, v11}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v11

    if-nez v11, :cond_8

    .line 1243
    :cond_7
    const-string v11, "SpenStrokeFrame"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "StrokeFrame Rect is outside of Page. StrokeFrameType = "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I
    invoke-static {v13}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 1244
    const-string v13, " Rect 2 = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 1243
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1245
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/16 v12, 0x10

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    .line 1250
    :cond_8
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v11

    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v11

    const/high16 v12, 0x41200000    # 10.0f

    cmpg-float v11, v11, v12

    if-ltz v11, :cond_9

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v11

    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v11

    const/high16 v12, 0x41200000    # 10.0f

    cmpg-float v11, v11, v12

    if-gez v11, :cond_a

    .line 1251
    :cond_9
    const-string v11, "SpenStrokeFrame"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "StrokeFrame Rect is too small. StrokeFrameType = "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I
    invoke-static {v13}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 1252
    const-string v13, " Rect 1 = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v13}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 1251
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1253
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/4 v12, 0x4

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    .line 1257
    :cond_a
    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v11

    const/high16 v12, 0x41200000    # 10.0f

    cmpg-float v11, v11, v12

    if-ltz v11, :cond_b

    .line 1258
    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v11

    const/high16 v12, 0x41200000    # 10.0f

    cmpg-float v11, v11, v12

    if-gez v11, :cond_c

    .line 1259
    :cond_b
    const-string v11, "SpenStrokeFrame"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "StrokeFrame Rect is too small. StrokeFrameType = "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I
    invoke-static {v13}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 1260
    const-string v13, " Rect 2 = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 1259
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1261
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/16 v12, 0x8

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    .line 1265
    :cond_c
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageWidth:I
    invoke-static {v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$57(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v12

    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageHeight:I
    invoke-static {v13}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$58(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v13

    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v12, v13, v14}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$45(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)V

    .line 1266
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$44(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v11

    if-nez v11, :cond_d

    .line 1267
    const-string v11, "SpenStrokeFrame"

    const-string/jumbo v12, "workBitmap create is failed"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1268
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/16 v12, 0x20

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    .line 1272
    :cond_d
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$60(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v12

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    .line 1273
    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v13}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$60(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 1272
    invoke-static {v12, v13, v14}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$48(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)V

    .line 1274
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$17(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v11

    if-nez v11, :cond_e

    .line 1275
    const-string v11, "SpenStrokeFrame"

    const-string v12, "ShapeMaskBitmap create is failed"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1276
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/16 v12, 0x20

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    .line 1280
    :cond_e
    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    .line 1281
    .local v7, "relativePath":Landroid/graphics/Path;
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v12

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertRelative(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    invoke-static {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v8

    .line 1282
    .local v8, "relativeRect":Landroid/graphics/RectF;
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 1283
    .local v2, "m":Landroid/graphics/Matrix;
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v11

    sget-object v12, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v2, v11, v8, v12}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1284
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v11

    invoke-virtual {v11, v2, v7}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    .line 1285
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createShapeMaskBitmap(Landroid/graphics/Path;)V
    invoke-static {v11, v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V

    .line 1287
    const/4 v10, 0x0

    .line 1288
    .local v10, "resultValue":Z
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v11

    if-nez v11, :cond_f

    .line 1289
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;
    invoke-static {v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$17(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v12

    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v13}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v13

    iget-object v14, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v14}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v14

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->drawPen(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    invoke-static {v11, v12, v13, v14, v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v10

    .line 1294
    :goto_3
    if-nez v10, :cond_10

    .line 1295
    const-string v11, "SpenStrokeFrame"

    const-string v12, "drawPen is failed"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1296
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/16 v12, 0x20

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    .line 1291
    :cond_f
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;
    invoke-static {v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$17(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v12

    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v13}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v13

    iget-object v14, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v14}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v14

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->drawPen(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    invoke-static {v11, v12, v13, v14, v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v10

    goto :goto_3

    .line 1300
    :cond_10
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->setRect(Landroid/graphics/RectF;)V
    invoke-static {v11, v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$20(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)V

    .line 1302
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->startCamera()Z
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$61(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 1303
    const-string v11, "SpenStrokeFrame"

    const-string/jumbo v12, "startCamera is failed"

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1304
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$7;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/16 v12, 0x20

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0
.end method
