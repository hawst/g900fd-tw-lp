.class Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;
.super Ljava/lang/Object;
.source "SpenSharedPreferencesManager.java"


# static fields
.field public static final CURRENT_PEN:Ljava/lang/String; = "CURRENT"

.field public static final PEN_ADVANCE:Ljava/lang/String; = "_PEN_ADVANCE"

.field public static final PEN_COLOR:Ljava/lang/String; = "_PEN_COLOR"

.field public static final PEN_NAME:Ljava/lang/String; = "_PEN_NAME"

.field public static final PEN_SIZE:Ljava/lang/String; = "_PEN_SIZE"

.field public static final PRESET_FILE_PATH:Ljava/lang/String;

.field public static final PRESET_IMAGE:Ljava/lang/String; = "_PRESET_IMAGE"

.field public static final PRESET_ORDER:Ljava/lang/String; = "PRESET"

.field public static final PRESET_PRE:Ljava/lang/String; = "_PRESET_"


# instance fields
.field private final mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private final mPackageName:Ljava/lang/String;

.field private final mPenPluginInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPresetInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mSharedPreferences:Landroid/content/SharedPreferences;

.field private final mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

.field private subdata:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 21
    const-string v1, "/.SPenSDK30/preset"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 20
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->PRESET_FILE_PATH:Ljava/lang/String;

    .line 31
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;F)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "scale"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;",
            ">;F)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p2, "penPluginInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 323
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->subdata:Ljava/lang/String;

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mPackageName:Ljava/lang/String;

    .line 54
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mPackageName:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 55
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    .line 56
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mPenPluginInfoList:Ljava/util/ArrayList;

    .line 57
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, ""

    invoke-direct {v0, p1, v1, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 58
    return-void
.end method

.method private getPenData(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .locals 4
    .param p1, "penName"    # Ljava/lang/String;

    .prologue
    .line 87
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "_PEN_NAME"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    .line 89
    .local v0, "penData":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "_PEN_NAME"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 90
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "_PEN_SIZE"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 92
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "_PEN_COLOR"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 93
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "_PEN_ADVANCE"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    .line 98
    :goto_0
    return-object v0

    .line 96
    .end local v0    # "penData":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "penData":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    goto :goto_0
.end method

.method private setPenData(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;Ljava/lang/String;)V
    .locals 3
    .param p1, "mPenData"    # Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "_PEN_NAME"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 70
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "_PEN_SIZE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 71
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "_PEN_COLOR"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 72
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "_PEN_ADVANCE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 74
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 75
    return-void
.end method


# virtual methods
.method public clearSharedPenData()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 104
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 105
    return-void
.end method

.method public getCurrentPenName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 168
    const/4 v0, 0x0

    .line 169
    .local v0, "penName":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "CURRENT_PEN_NAME"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 170
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "CURRENT_PEN_NAME"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 172
    :cond_0
    return-object v0
.end method

.method public getPenDataList()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 141
    .local v2, "localPenDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;>;"
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 144
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 158
    return-object v2

    .line 147
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    .line 149
    .local v4, "penPluginInfo":Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;
    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v3

    .line 150
    .local v3, "localPenName":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->getPenData(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v1

    .line 152
    .local v1, "localPenData":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    if-eqz v1, :cond_0

    .line 153
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getPresetData(I)Ljava/util/ArrayList;
    .locals 16
    .param p1, "maximum"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187
    const/4 v6, 0x0

    .line 188
    .local v6, "is":Ljava/io/FileInputStream;
    new-instance v9, Ljava/io/File;

    sget-object v13, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->PRESET_FILE_PATH:Ljava/lang/String;

    invoke-direct {v9, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 189
    .local v9, "sdk30File":Ljava/io/File;
    new-instance v10, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    sget-object v14, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->PRESET_FILE_PATH:Ljava/lang/String;

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, "_"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mPackageName:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->subdata:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v10, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 192
    .local v10, "sdk31File":Ljava/io/File;
    :try_start_0
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v13

    if-nez v13, :cond_1

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->subdata:Ljava/lang/String;

    const-string v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 193
    new-instance v7, Ljava/io/FileInputStream;

    new-instance v13, Ljava/io/File;

    sget-object v14, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->PRESET_FILE_PATH:Ljava/lang/String;

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v7, v13}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3

    .end local v6    # "is":Ljava/io/FileInputStream;
    .local v7, "is":Ljava/io/FileInputStream;
    move-object v6, v7

    .line 202
    .end local v7    # "is":Ljava/io/FileInputStream;
    .restart local v6    # "is":Ljava/io/FileInputStream;
    :goto_0
    const/16 v13, 0x1000

    :try_start_1
    new-array v2, v13, [B
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3

    .line 203
    .local v2, "bytes":[B
    const/4 v11, 0x0

    .line 206
    .local v11, "size":I
    :try_start_2
    invoke-virtual {v6, v2}, Ljava/io/FileInputStream;->read([B)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v11

    .line 211
    if-eqz v6, :cond_0

    .line 213
    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_3

    .line 221
    :cond_0
    :goto_1
    if-gez v11, :cond_4

    .line 222
    const/4 v13, 0x0

    .line 259
    .end local v2    # "bytes":[B
    .end local v11    # "size":I
    :goto_2
    return-object v13

    .line 195
    :cond_1
    :try_start_4
    new-instance v7, Ljava/io/FileInputStream;

    new-instance v13, Ljava/io/File;

    new-instance v14, Ljava/lang/StringBuilder;

    sget-object v15, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->PRESET_FILE_PATH:Ljava/lang/String;

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v15, "_"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mPackageName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->subdata:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v7, v13}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_3

    .end local v6    # "is":Ljava/io/FileInputStream;
    .restart local v7    # "is":Ljava/io/FileInputStream;
    move-object v6, v7

    .line 197
    .end local v7    # "is":Ljava/io/FileInputStream;
    .restart local v6    # "is":Ljava/io/FileInputStream;
    goto :goto_0

    :catch_0
    move-exception v3

    .line 198
    .local v3, "e":Ljava/io/FileNotFoundException;
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_3

    .line 199
    const/4 v13, 0x0

    goto :goto_2

    .line 207
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    .restart local v2    # "bytes":[B
    .restart local v11    # "size":I
    :catch_1
    move-exception v3

    .line 209
    .local v3, "e":Ljava/io/IOException;
    :try_start_6
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 211
    if-eqz v6, :cond_0

    .line 213
    :try_start_7
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    .line 214
    :catch_2
    move-exception v4

    .line 215
    .local v4, "ex":Ljava/io/IOException;
    :try_start_8
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catch Ljava/lang/NullPointerException; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_1

    .line 256
    .end local v2    # "bytes":[B
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "ex":Ljava/io/IOException;
    .end local v11    # "size":I
    :catch_3
    move-exception v3

    .line 257
    .local v3, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v3}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 259
    .end local v3    # "e":Ljava/lang/NullPointerException;
    :cond_2
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mPresetInfoList:Ljava/util/ArrayList;

    goto :goto_2

    .line 210
    .restart local v2    # "bytes":[B
    .restart local v11    # "size":I
    :catchall_0
    move-exception v13

    .line 211
    if-eqz v6, :cond_3

    .line 213
    :try_start_9
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/lang/NullPointerException; {:try_start_9 .. :try_end_9} :catch_3

    .line 218
    :cond_3
    :goto_4
    :try_start_a
    throw v13

    .line 214
    :catch_4
    move-exception v4

    .line 215
    .restart local v4    # "ex":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 214
    .end local v4    # "ex":Ljava/io/IOException;
    :catch_5
    move-exception v4

    .line 215
    .restart local v4    # "ex":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 225
    .end local v4    # "ex":Ljava/io/IOException;
    :cond_4
    new-instance v13, Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "UTF-8"

    invoke-static {v15}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v15

    invoke-direct {v13, v2, v14, v11, v15}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 227
    .local v1, "buffer":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mPresetInfoList:Ljava/util/ArrayList;

    if-nez v13, :cond_5

    .line 228
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mPresetInfoList:Ljava/util/ArrayList;
    :try_end_a
    .catch Ljava/lang/NullPointerException; {:try_start_a .. :try_end_a} :catch_3

    .line 232
    :cond_5
    const/4 v5, 0x1

    .local v5, "i":I
    :goto_5
    const/4 v13, 0x0

    :try_start_b
    aget-object v13, v1, v13

    const/16 v14, 0xa

    invoke-static {v13, v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v13

    if-gt v5, v13, :cond_2

    move/from16 v0, p1

    if-gt v5, v0, :cond_2

    .line 233
    new-instance v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    invoke-direct {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;-><init>()V

    .line 235
    .local v8, "presetInfo":Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;
    aget-object v13, v1, v5

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 236
    .local v12, "token":[Ljava/lang/String;
    add-int/lit8 v13, v5, -0x1

    invoke-virtual {v8, v13}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->setPresetIndex(I)V

    .line 237
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v14, 0x42820000    # 65.0f

    invoke-virtual {v13, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v13

    .line 238
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v15, 0x42820000    # 65.0f

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v14

    .line 237
    invoke-virtual {v8, v13, v14}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->setBitmapSize(II)V

    .line 239
    const/4 v13, 0x0

    aget-object v13, v12, v13

    invoke-virtual {v8, v13}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->setPenName(Ljava/lang/String;)V

    .line 240
    const/4 v13, 0x1

    aget-object v13, v12, v13

    invoke-static {v13}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v13

    invoke-virtual {v8, v13}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->setPenSize(F)V

    .line 241
    const/4 v13, 0x2

    aget-object v13, v12, v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v8, v13}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->setColor(I)V

    .line 242
    const/4 v13, 0x3

    aget-object v13, v12, v13

    invoke-virtual {v8, v13}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->setPresetImageName(Ljava/lang/String;)V

    .line 246
    array-length v13, v12

    const/4 v14, 0x5

    if-lt v13, v14, :cond_6

    .line 247
    const/4 v13, 0x4

    aget-object v13, v12, v13

    invoke-virtual {v8, v13}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->setAdvancedSetting(Ljava/lang/String;)V

    .line 251
    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mPresetInfoList:Ljava/util/ArrayList;

    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_b
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_b .. :try_end_b} :catch_6
    .catch Ljava/lang/NullPointerException; {:try_start_b .. :try_end_b} :catch_3

    .line 232
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 253
    .end local v8    # "presetInfo":Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;
    .end local v12    # "token":[Ljava/lang/String;
    :catch_6
    move-exception v3

    .line 254
    .local v3, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :try_start_c
    invoke-virtual {v3}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V
    :try_end_c
    .catch Ljava/lang/NullPointerException; {:try_start_c .. :try_end_c} :catch_3

    goto/16 :goto_3
.end method

.method public removeCurrentPenData()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "CURRENT_PEN_NAME"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 178
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 179
    return-void
.end method

.method public setCurrentPenName(Ljava/lang/String;)V
    .locals 2
    .param p1, "currentpenName"    # Ljava/lang/String;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "CURRENT_PEN_NAME"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 116
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 117
    return-void
.end method

.method protected setExtendedPresetMode(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 326
    if-eqz p1, :cond_0

    .line 327
    const-string v0, "_extended"

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->subdata:Ljava/lang/String;

    .line 331
    :goto_0
    return-void

    .line 329
    :cond_0
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->subdata:Ljava/lang/String;

    goto :goto_0
.end method

.method public setPenDataList(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 126
    .local p1, "penData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;>;"
    const/4 v0, 0x0

    .line 127
    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 128
    return-void

    .line 130
    :cond_0
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->setPenData(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;Ljava/lang/String;)V

    .line 126
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setPresetData(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 271
    .local p1, "mList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;>;"
    const/4 v4, 0x0

    .line 273
    .local v4, "os":Ljava/io/FileOutputStream;
    if-nez p1, :cond_0

    .line 321
    :goto_0
    return-void

    .line 278
    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v7}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 281
    .local v0, "buffer":Ljava/lang/StringBuffer;
    :try_start_1
    new-instance v5, Ljava/io/FileOutputStream;

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    sget-object v9, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->PRESET_FILE_PATH:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->mPackageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->subdata:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    .line 287
    .end local v4    # "os":Ljava/io/FileOutputStream;
    .local v5, "os":Ljava/io/FileOutputStream;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    :try_start_2
    invoke-interface {p1}, Ljava/util/List;->size()I
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_4

    move-result v7

    if-lt v3, v7, :cond_1

    .line 305
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "UTF-8"

    invoke-static {v8}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v5, v7, v8, v9}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 309
    if-eqz v5, :cond_3

    .line 311
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_4

    move-object v4, v5

    .line 312
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v4    # "os":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 282
    .end local v3    # "i":I
    :catch_0
    move-exception v1

    .line 283
    .local v1, "e":Ljava/io/FileNotFoundException;
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    .line 318
    .end local v0    # "buffer":Ljava/lang/StringBuffer;
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 319
    .local v1, "e":Ljava/lang/NullPointerException;
    :goto_2
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 288
    .end local v1    # "e":Ljava/lang/NullPointerException;
    .end local v4    # "os":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":Ljava/lang/StringBuffer;
    .restart local v3    # "i":I
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    :cond_1
    :try_start_6
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    .line 290
    .local v6, "presetData":Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;
    const-string v7, "\n"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 291
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 292
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenSize()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 293
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getColor()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 298
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPresetImageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 299
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getAdvancedSetting()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_4

    .line 287
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 306
    .end local v6    # "presetData":Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;
    :catch_2
    move-exception v1

    .line 307
    .local v1, "e":Ljava/io/IOException;
    :try_start_7
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 309
    if-eqz v5, :cond_3

    .line 311
    :try_start_8
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_8 .. :try_end_8} :catch_4

    move-object v4, v5

    .line 312
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v4    # "os":Ljava/io/FileOutputStream;
    goto/16 :goto_0

    .end local v4    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v2

    .line 313
    .local v2, "ex":Ljava/io/IOException;
    :try_start_9
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catch Ljava/lang/NullPointerException; {:try_start_9 .. :try_end_9} :catch_4

    move-object v4, v5

    .line 314
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v4    # "os":Ljava/io/FileOutputStream;
    goto/16 :goto_0

    .line 308
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "ex":Ljava/io/IOException;
    .end local v4    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v7

    .line 309
    if-eqz v5, :cond_2

    .line 311
    :try_start_a
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_a .. :try_end_a} :catch_4

    .line 317
    :cond_2
    :try_start_b
    throw v7

    .line 318
    :catch_4
    move-exception v1

    move-object v4, v5

    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v4    # "os":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 312
    .end local v4    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v2

    .line 313
    .restart local v2    # "ex":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v4, v5

    .line 314
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v4    # "os":Ljava/io/FileOutputStream;
    goto/16 :goto_0

    .line 312
    .end local v2    # "ex":Ljava/io/IOException;
    .end local v4    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v2

    .line 313
    .restart local v2    # "ex":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b
    .catch Ljava/lang/NullPointerException; {:try_start_b .. :try_end_b} :catch_4

    move-object v4, v5

    .line 314
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v4    # "os":Ljava/io/FileOutputStream;
    goto/16 :goto_0

    .end local v2    # "ex":Ljava/io/IOException;
    .end local v4    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    :cond_3
    move-object v4, v5

    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v4    # "os":Ljava/io/FileOutputStream;
    goto/16 :goto_0
.end method
