.class public Lcom/samsung/android/sdk/pen/pen/preload/Beautify;
.super Ljava/lang/Object;
.source "Beautify.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "SdCardPath"
    }
.end annotation


# static fields
.field private static final BEAUTIFY_PARAMETER_CURSIVE:I = 0x2

.field private static final BEAUTIFY_PARAMETER_DUMMY:I = 0x4

.field private static final BEAUTIFY_PARAMETER_LINETYPE:I = 0x1

.field private static final BEAUTIFY_PARAMETER_MODULATION:I = 0x6

.field private static final BEAUTIFY_PARAMETER_SCRATCH:I = 0x7

.field private static final BEAUTIFY_PARAMETER_SLANT:I = 0x9

.field private static final BEAUTIFY_PARAMETER_SLANTINDEX:I = 0x8

.field private static final BEAUTIFY_PARAMETER_STYLEID:I = 0x0

.field private static final BEAUTIFY_PARAMETER_SUSTENANCE:I = 0x3

.field private static final BEAUTIFY_PARAMETER_THICKNESS:I = 0x5

.field private static final BEAUTIFY_STYLEID_CURSIVE_LM:I = 0xb

.field private static final BEAUTIFY_STYLEID_HUAI:I = 0xc

.field private static final BEAUTIFY_STYLEID_HUANG:I = 0x5

.field private static final BEAUTIFY_STYLEID_HUI:I = 0x6

.field private static final BEAUTIFY_STYLEID_RUNNING_HAND_S:I = 0x1

.field private static final BEAUTIFY_STYLEID_WANG:I = 0x3

.field private static final DEFAULT_SETTING_VALUES:[[I

.field private static final MAX_PARAMETER_INDEX:I = 0xa

.field private static final MAX_STYLEID_COUNT:I = 0x6

.field public static final TAG:Ljava/lang/String; = "Beautify"

.field private static mDensity:F = 0.0f

.field private static final mImagePath_eraser_bar:Ljava/lang/String; = "eraser_bar"

.field private static final mImagePath_eraser_handel:Ljava/lang/String; = "eraser_handel"

.field private static final mImagePath_eraser_handel_press:Ljava/lang/String; = "eraser_handel_press"

.field private static final mImagePath_snote_option_in_bg:Ljava/lang/String; = "snote_option_in_bg"


# instance fields
.field private mAdvancedSettingLayout:Landroid/widget/LinearLayout;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mButtonList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageButton;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrentButtonIndex:I

.field private mCursiveSeekBar:Landroid/widget/SeekBar;

.field private mDummySeekBar:Landroid/widget/SeekBar;

.field private mIs64:Z

.field private mModulationSeekBar:Landroid/widget/SeekBar;

.field private mResetButton:Landroid/view/View;

.field private final mResetButtonListener:Landroid/view/View$OnClickListener;

.field private final mSeekBarListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

.field private final mSettingValues:[[I

.field private mSustenanceSeekBar:Landroid/widget/SeekBar;

.field private final nativeBeautify:J


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x6

    const/4 v8, 0x5

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 87
    new-array v0, v9, [[I

    const/4 v1, 0x0

    .line 88
    const/16 v2, 0xa

    new-array v2, v2, [I

    const/4 v3, 0x0

    const/16 v4, 0xb

    aput v4, v2, v3

    aput v6, v2, v5

    aput v8, v2, v6

    const/16 v3, 0x8

    aput v3, v2, v7

    const/4 v3, 0x4

    aput v5, v2, v3

    const/16 v3, 0xf

    aput v3, v2, v8

    const/16 v3, 0x46

    aput v3, v2, v9

    const/4 v3, 0x7

    const/16 v4, 0xb

    aput v4, v2, v3

    const/16 v3, 0x9

    aput v5, v2, v3

    aput-object v2, v0, v1

    .line 89
    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v2, 0x0

    const/16 v3, 0xc

    aput v3, v1, v2

    aput v6, v1, v5

    aput v8, v1, v6

    aput v5, v1, v7

    const/4 v2, 0x4

    aput v5, v1, v2

    const/16 v2, 0x12

    aput v2, v1, v8

    const/16 v2, 0x46

    aput v2, v1, v9

    const/4 v2, 0x7

    const/16 v3, 0xd

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v5, v1, v2

    aput-object v1, v0, v5

    .line 90
    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput v8, v1, v2

    aput v6, v1, v5

    aput v6, v1, v6

    aput v6, v1, v7

    const/4 v2, 0x4

    aput v5, v1, v2

    const/16 v2, 0x12

    aput v2, v1, v8

    const/16 v2, 0x46

    aput v2, v1, v9

    const/4 v2, 0x7

    aput v8, v1, v2

    const/16 v2, 0x9

    aput v5, v1, v2

    aput-object v1, v0, v6

    .line 91
    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput v9, v1, v2

    aput v6, v1, v5

    aput v7, v1, v6

    aput v7, v1, v7

    const/4 v2, 0x4

    aput v5, v1, v2

    const/16 v2, 0xc

    aput v2, v1, v8

    const/16 v2, 0x46

    aput v2, v1, v9

    const/4 v2, 0x7

    aput v9, v1, v2

    const/16 v2, 0x9

    aput v5, v1, v2

    aput-object v1, v0, v7

    const/4 v1, 0x4

    .line 92
    const/16 v2, 0xa

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput v5, v2, v3

    aput v6, v2, v5

    aput v6, v2, v6

    const/16 v3, 0x8

    aput v3, v2, v7

    const/4 v3, 0x4

    aput v5, v2, v3

    const/16 v3, 0xf

    aput v3, v2, v8

    const/16 v3, 0x46

    aput v3, v2, v9

    const/4 v3, 0x7

    aput v7, v2, v3

    const/16 v3, 0x9

    aput v5, v2, v3

    aput-object v2, v0, v1

    .line 93
    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput v7, v1, v2

    aput v5, v1, v5

    aput v7, v1, v6

    aput v8, v1, v7

    const/4 v2, 0x4

    aput v5, v1, v2

    const/16 v2, 0xc

    aput v2, v1, v8

    const/16 v2, 0x46

    aput v2, v1, v9

    const/4 v2, 0x7

    aput v5, v1, v2

    const/16 v2, 0x9

    aput v5, v1, v2

    aput-object v1, v0, v8

    .line 87
    sput-object v0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->DEFAULT_SETTING_VALUES:[[I

    .line 1356
    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    .line 95
    sget-object v4, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->DEFAULT_SETTING_VALUES:[[I

    invoke-virtual {v4}, [[I->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [[I

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I

    .line 98
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    .line 880
    new-instance v4, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;-><init>(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mResetButtonListener:Landroid/view/View$OnClickListener;

    .line 934
    new-instance v4, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;-><init>(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSeekBarListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 106
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v4

    const/16 v6, 0x20

    if-ne v4, v6, :cond_0

    move v4, v5

    :goto_0
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    .line 108
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "/data/data/"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/lib/libSPenBeautify.so"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 109
    .local v2, "latestLib":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 110
    .local v3, "libFilePath":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 112
    :try_start_0
    invoke-static {v2}, Ljava/lang/System;->load(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_init()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    .line 130
    return-void

    .line 106
    .end local v2    # "latestLib":Ljava/lang/String;
    .end local v3    # "libFilePath":Ljava/io/File;
    :cond_0
    const/4 v4, 0x1

    goto :goto_0

    .line 113
    .restart local v2    # "latestLib":Ljava/lang/String;
    .restart local v3    # "libFilePath":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/lang/Throwable;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 116
    :try_start_1
    const-string v4, "SPenBeautify"

    invoke-static {v4}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 117
    :catch_1
    move-exception v1

    .line 118
    .local v1, "error":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Beautify library is not initialized."

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 124
    .end local v0    # "e":Ljava/lang/Throwable;
    .end local v1    # "error":Ljava/lang/Exception;
    :cond_1
    :try_start_2
    const-string v4, "SPenBeautify"

    invoke-static {v4}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 125
    :catch_2
    move-exception v1

    .line 126
    .restart local v1    # "error":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Beautify library is not initialized."

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method private Native_construct(J)Z
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 1077
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1078
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_construct(J)Z

    move-result v0

    .line 1080
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_construct(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_draw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "event"    # Landroid/view/MotionEvent;
    .param p4, "rect"    # Landroid/graphics/RectF;
    .param p5, "toolType"    # I

    .prologue
    .line 1117
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1118
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_draw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    .line 1120
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_draw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_getAdvancedSetting(J)Ljava/lang/String;
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 1213
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1214
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getAdvancedSetting(J)Ljava/lang/String;

    move-result-object v0

    .line 1216
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getAdvancedSetting(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_getColor(J)I
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 1181
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1182
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getColor(J)I

    move-result v0

    .line 1184
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getMaxSettingValue(J)F
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 1165
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1166
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getMaxSettingValue(J)F

    move-result v0

    .line 1168
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getMaxSettingValue(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getMinSettingValue(J)F
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 1157
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1158
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getMinSettingValue(J)F

    move-result v0

    .line 1160
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getMinSettingValue(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getPenAttribute(JI)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "attribute"    # I

    .prologue
    .line 1232
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1233
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getPenAttribute(JI)Z

    move-result v0

    .line 1235
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getPenAttribute(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_getProperty(JLandroid/os/Bundle;)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "propertyMap"    # Landroid/os/Bundle;

    .prologue
    .line 1109
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1110
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getProperty(JLandroid/os/Bundle;)Z

    move-result v0

    .line 1112
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getProperty(ILandroid/os/Bundle;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_getSize(J)F
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 1149
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1150
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getSize(J)F

    move-result v0

    .line 1152
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getSize(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getStrokeRect(J[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;
    .locals 7
    .param p1, "nativePen"    # J
    .param p3, "points"    # [Landroid/graphics/PointF;
    .param p4, "pressures"    # [F
    .param p5, "timestamps"    # [I
    .param p6, "size"    # F
    .param p7, "isCurvable"    # Z
    .param p8, "advanced"    # Ljava/lang/String;

    .prologue
    .line 1222
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1223
    invoke-static/range {p1 .. p8}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getStrokeRect(J[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;

    move-result-object v0

    .line 1226
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move v4, p6

    move v5, p7

    move-object v6, p8

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getStrokeRect(I[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_init()J
    .locals 2

    .prologue
    .line 1069
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1070
    invoke-static {}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_init_64()J

    move-result-wide v0

    .line 1072
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_init()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method private Native_isCurveEnabled(J)Z
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 1197
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1198
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_isCurveEnabled(J)Z

    move-result v0

    .line 1200
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_isCurveEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_onLoad(J)V
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 1085
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1086
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_onLoad(J)V

    .line 1090
    :goto_0
    return-void

    .line 1088
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_onLoad(I)V

    goto :goto_0
.end method

.method private Native_onUnload(J)V
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 1093
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1094
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_onUnload(J)V

    .line 1098
    :goto_0
    return-void

    .line 1096
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_onUnload(I)V

    goto :goto_0
.end method

.method private Native_redraw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "event"    # Landroid/view/MotionEvent;
    .param p4, "rect"    # Landroid/graphics/RectF;
    .param p5, "toolType"    # I

    .prologue
    .line 1125
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1126
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_redraw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    .line 1128
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_redraw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setAdvancedSetting(JLjava/lang/String;)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "advancedSetting"    # Ljava/lang/String;

    .prologue
    .line 1205
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1206
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_setAdvancedSetting(JLjava/lang/String;)Z

    move-result v0

    .line 1208
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_setAdvancedSetting(ILjava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setBitmap(JLandroid/graphics/Bitmap;)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1133
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1134
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_setBitmap(JLandroid/graphics/Bitmap;)Z

    move-result v0

    .line 1136
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_setBitmap(ILandroid/graphics/Bitmap;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setColor(JI)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "color"    # I

    .prologue
    .line 1173
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1174
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_setColor(JI)Z

    move-result v0

    .line 1176
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_setColor(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setCurveEnabled(JZ)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "curve"    # Z

    .prologue
    .line 1189
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1190
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_setCurveEnabled(JZ)Z

    move-result v0

    .line 1192
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_setCurveEnabled(IZ)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setProperty(JLandroid/os/Bundle;)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "propertyMap"    # Landroid/os/Bundle;

    .prologue
    .line 1101
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1102
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_setProperty(JLandroid/os/Bundle;)Z

    move-result v0

    .line 1104
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_setProperty(ILandroid/os/Bundle;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setSize(JF)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "size"    # F

    .prologue
    .line 1141
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mIs64:Z

    if-eqz v0, :cond_0

    .line 1142
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_setSize(JF)Z

    move-result v0

    .line 1144
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_setSize(IF)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    return-object v0
.end method

.method static synthetic access$1()[[I
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->DEFAULT_SETTING_VALUES:[[I

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCursiveSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSustenanceSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mDummySeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mModulationSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mButtonList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;I)V
    .locals 0

    .prologue
    .line 1024
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setBeautifyType(I)V

    return-void
.end method

.method private beautifyTypeLayout()Landroid/widget/LinearLayout;
    .locals 12

    .prologue
    const/4 v10, -0x2

    const/high16 v11, 0x41f00000    # 30.0f

    .line 491
    new-instance v6, Landroid/widget/LinearLayout;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v6, v9}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 492
    .local v6, "linearLayout":Landroid/widget/LinearLayout;
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v5, v10, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 494
    .local v5, "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v6, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 495
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mButtonList:Ljava/util/ArrayList;

    .line 497
    const/4 v4, 0x0

    .local v4, "index":I
    :goto_0
    const/4 v9, 0x6

    if-lt v4, v9, :cond_0

    .line 517
    return-object v6

    .line 499
    :cond_0
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v0, v9}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 500
    .local v0, "button":Landroid/widget/ImageButton;
    add-int/lit8 v3, v4, 0x1

    .line 501
    .local v3, "imageNameIndex":I
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "chinabrush_mode_0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 502
    .local v2, "imageName":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 503
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {p0, v11}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v9

    .line 504
    invoke-direct {p0, v11}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v10

    .line 503
    invoke-direct {v1, v9, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 505
    .local v1, "buttonLayoutParam":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 507
    const-string v8, "chinabrush_effect_btn_normal"

    .line 508
    .local v8, "localUnselectImage":Ljava/lang/String;
    const-string v7, "chinabrush_effect_btn_press"

    .line 510
    .local v7, "localSelectImage":Ljava/lang/String;
    invoke-direct {p0, v8, v7, v7}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 512
    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 513
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 514
    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 497
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private calculatePixel(F)I
    .locals 1
    .param p1, "paramFloat"    # F

    .prologue
    .line 983
    sget v0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mDensity:F

    mul-float/2addr v0, p1

    float-to-int v0, v0

    return v0
.end method

.method private cursiveLayout()Landroid/view/View;
    .locals 8

    .prologue
    const/high16 v7, 0x41400000    # 12.0f

    const/4 v6, -0x2

    const/4 v5, 0x0

    .line 522
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 523
    .local v1, "localLinearLayout":Landroid/widget/LinearLayout;
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 525
    .local v0, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v4, 0x40a00000    # 5.0f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v4

    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 526
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 528
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 529
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->cursiveSeekbar()Landroid/widget/SeekBar;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 531
    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 532
    .local v3, "textView":Landroid/widget/TextView;
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 534
    .local v2, "textLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 536
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setTextSize(F)V

    .line 537
    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 538
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 539
    const-string v4, "cursive"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 540
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 541
    const/high16 v4, 0x40900000    # 4.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v4

    invoke-virtual {v3, v5, v4, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 542
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 543
    return-object v1
.end method

.method private cursiveSeekbar()Landroid/widget/SeekBar;
    .locals 15

    .prologue
    const/16 v14, 0xa0

    const/4 v13, 0x1

    const/4 v2, 0x0

    .line 548
    new-instance v10, Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v10, v4}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 549
    .local v10, "localSeekBar":Landroid/widget/SeekBar;
    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v4, 0x42dc0000    # 110.0f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v4

    .line 550
    const/4 v5, -0x2

    .line 549
    invoke-direct {v9, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 551
    .local v9, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v10, v9}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 552
    const/16 v4, 0xc

    invoke-virtual {v10, v4}, Landroid/widget/SeekBar;->setMax(I)V

    .line 553
    const-string v4, "eraser_handel"

    const-string v5, "eraser_handel_press"

    const/4 v12, 0x0

    invoke-direct {p0, v4, v5, v12}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v4

    invoke-virtual {v10, v4}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 554
    new-instance v8, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v8}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 556
    .local v8, "localGradientDrawable":Landroid/graphics/drawable/GradientDrawable;
    const/high16 v4, 0x40a00000    # 5.0f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v8, v4}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 557
    new-instance v7, Landroid/graphics/drawable/ClipDrawable;

    const/4 v4, 0x3

    invoke-direct {v7, v8, v4, v13}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    .line 558
    .local v7, "localClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    const-string v4, "eraser_bar"

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 559
    .local v1, "localDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v11, v4, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    .line 562
    .local v11, "targetSdkVersion":I
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xe

    if-lt v4, v5, :cond_3

    .line 563
    const/16 v4, 0xa

    if-gt v11, v4, :cond_1

    .line 564
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v4, v14, :cond_0

    .line 565
    const/high16 v4, 0x40d00000    # 6.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .line 581
    .local v3, "insetMargin":I
    :goto_0
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    move v4, v2

    move v5, v3

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 582
    .local v0, "localInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    const/4 v4, 0x2

    new-array v6, v4, [Landroid/graphics/drawable/Drawable;

    .line 583
    .local v6, "arrayOfDrawable":[Landroid/graphics/drawable/Drawable;
    aput-object v0, v6, v2

    .line 584
    aput-object v7, v6, v13

    .line 585
    new-instance v2, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v2, v6}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v10, v2}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 587
    iput-object v10, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCursiveSeekBar:Landroid/widget/SeekBar;

    .line 588
    return-object v10

    .line 567
    .end local v0    # "localInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    .end local v3    # "insetMargin":I
    .end local v6    # "arrayOfDrawable":[Landroid/graphics/drawable/Drawable;
    :cond_0
    const/high16 v4, 0x40b00000    # 5.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .line 569
    .restart local v3    # "insetMargin":I
    goto :goto_0

    .line 570
    .end local v3    # "insetMargin":I
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v4, v14, :cond_2

    .line 571
    const/high16 v4, 0x40600000    # 3.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .line 572
    .restart local v3    # "insetMargin":I
    goto :goto_0

    .line 573
    .end local v3    # "insetMargin":I
    :cond_2
    const/high16 v4, 0x40200000    # 2.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .line 576
    .restart local v3    # "insetMargin":I
    goto :goto_0

    .line 577
    .end local v3    # "insetMargin":I
    :cond_3
    const/high16 v4, 0x40000000    # 2.0f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .restart local v3    # "insetMargin":I
    goto :goto_0
.end method

.method private dummyLayout()Landroid/view/View;
    .locals 8

    .prologue
    const/high16 v7, 0x41400000    # 12.0f

    const/4 v6, -0x2

    const/4 v5, 0x0

    .line 670
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 671
    .local v1, "localLinearLayout":Landroid/widget/LinearLayout;
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 674
    .local v0, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v4, 0x40a00000    # 5.0f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v4

    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 677
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 680
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 681
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->dummySeekbar()Landroid/widget/SeekBar;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 683
    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 684
    .local v3, "textView":Landroid/widget/TextView;
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 686
    .local v2, "textLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 688
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setTextSize(F)V

    .line 689
    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 690
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 691
    const-string v4, "dummy"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 692
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 693
    const/high16 v4, 0x40900000    # 4.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v4

    invoke-virtual {v3, v5, v4, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 694
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 695
    return-object v1
.end method

.method private dummySeekbar()Landroid/widget/SeekBar;
    .locals 15

    .prologue
    const/16 v14, 0xa0

    const/4 v13, 0x1

    const/4 v2, 0x0

    .line 700
    new-instance v10, Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v10, v4}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 701
    .local v10, "localSeekBar":Landroid/widget/SeekBar;
    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v4, 0x42dc0000    # 110.0f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v4

    .line 702
    const/4 v5, -0x2

    .line 701
    invoke-direct {v9, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 703
    .local v9, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v10, v9}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 704
    const/16 v4, 0x14

    invoke-virtual {v10, v4}, Landroid/widget/SeekBar;->setMax(I)V

    .line 705
    const-string v4, "eraser_handel"

    const-string v5, "eraser_handel_press"

    const/4 v12, 0x0

    invoke-direct {p0, v4, v5, v12}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v4

    invoke-virtual {v10, v4}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 706
    new-instance v8, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v8}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 708
    .local v8, "localGradientDrawable":Landroid/graphics/drawable/GradientDrawable;
    const/high16 v4, 0x40a00000    # 5.0f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v8, v4}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 709
    new-instance v7, Landroid/graphics/drawable/ClipDrawable;

    const/4 v4, 0x3

    invoke-direct {v7, v8, v4, v13}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    .line 710
    .local v7, "localClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    const-string v4, "eraser_bar"

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 711
    .local v1, "localDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v11, v4, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    .line 714
    .local v11, "targetSdkVersion":I
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xe

    if-lt v4, v5, :cond_3

    .line 715
    const/16 v4, 0xa

    if-gt v11, v4, :cond_1

    .line 716
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v4, v14, :cond_0

    .line 717
    const/high16 v4, 0x40d00000    # 6.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .line 733
    .local v3, "insetMargin":I
    :goto_0
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    move v4, v2

    move v5, v3

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 734
    .local v0, "localInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    const/4 v4, 0x2

    new-array v6, v4, [Landroid/graphics/drawable/Drawable;

    .line 735
    .local v6, "arrayOfDrawable":[Landroid/graphics/drawable/Drawable;
    aput-object v0, v6, v2

    .line 736
    aput-object v7, v6, v13

    .line 737
    new-instance v2, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v2, v6}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v10, v2}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 739
    iput-object v10, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mDummySeekBar:Landroid/widget/SeekBar;

    .line 740
    return-object v10

    .line 719
    .end local v0    # "localInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    .end local v3    # "insetMargin":I
    .end local v6    # "arrayOfDrawable":[Landroid/graphics/drawable/Drawable;
    :cond_0
    const/high16 v4, 0x40b00000    # 5.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .line 721
    .restart local v3    # "insetMargin":I
    goto :goto_0

    .line 722
    .end local v3    # "insetMargin":I
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v4, v14, :cond_2

    .line 723
    const/high16 v4, 0x40600000    # 3.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .line 724
    .restart local v3    # "insetMargin":I
    goto :goto_0

    .line 725
    .end local v3    # "insetMargin":I
    :cond_2
    const/high16 v4, 0x40200000    # 2.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .line 728
    .restart local v3    # "insetMargin":I
    goto :goto_0

    .line 729
    .end local v3    # "insetMargin":I
    :cond_3
    const/high16 v4, 0x40000000    # 2.0f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .restart local v3    # "insetMargin":I
    goto :goto_0
.end method

.method private initView()Landroid/widget/LinearLayout;
    .locals 4

    .prologue
    .line 467
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 468
    .local v1, "linearLayout":Landroid/widget/LinearLayout;
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    .line 469
    const v3, 0x43ac8000    # 345.0f

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .line 468
    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 470
    .local v0, "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 472
    const v2, -0x969697

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 474
    const/high16 v2, 0x42200000    # 40.0f

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setX(F)V

    .line 475
    const/high16 v2, 0x43070000    # 135.0f

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setY(F)V

    .line 476
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 478
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->beautifyTypeLayout()Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 479
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->cursiveLayout()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 480
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->sustenanceLayout()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 481
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->dummyLayout()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 482
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->modulationLayout()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 483
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->resetButtonLayout()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 485
    return-object v1
.end method

.method private modulationLayout()Landroid/view/View;
    .locals 8

    .prologue
    const/high16 v7, 0x41400000    # 12.0f

    const/4 v6, -0x2

    const/4 v5, 0x0

    .line 746
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 747
    .local v1, "localLinearLayout":Landroid/widget/LinearLayout;
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 750
    .local v0, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v4, 0x40a00000    # 5.0f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v4

    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 753
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 756
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 757
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->modulationSeekbar()Landroid/widget/SeekBar;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 759
    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 760
    .local v3, "textView":Landroid/widget/TextView;
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 762
    .local v2, "textLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 764
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setTextSize(F)V

    .line 765
    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 766
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 767
    const-string v4, "modulation"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 768
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 769
    const/high16 v4, 0x40900000    # 4.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v4

    invoke-virtual {v3, v5, v4, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 770
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 771
    return-object v1
.end method

.method private modulationSeekbar()Landroid/widget/SeekBar;
    .locals 15

    .prologue
    const/16 v14, 0xa0

    const/4 v13, 0x1

    const/4 v2, 0x0

    .line 776
    new-instance v10, Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v10, v4}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 777
    .local v10, "localSeekBar":Landroid/widget/SeekBar;
    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v4, 0x42dc0000    # 110.0f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v4

    .line 778
    const/4 v5, -0x2

    .line 777
    invoke-direct {v9, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 779
    .local v9, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v10, v9}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 780
    const/16 v4, 0x64

    invoke-virtual {v10, v4}, Landroid/widget/SeekBar;->setMax(I)V

    .line 781
    const-string v4, "eraser_handel"

    const-string v5, "eraser_handel_press"

    const/4 v12, 0x0

    invoke-direct {p0, v4, v5, v12}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v4

    invoke-virtual {v10, v4}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 782
    new-instance v8, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v8}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 784
    .local v8, "localGradientDrawable":Landroid/graphics/drawable/GradientDrawable;
    const/high16 v4, 0x40a00000    # 5.0f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v8, v4}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 785
    new-instance v7, Landroid/graphics/drawable/ClipDrawable;

    const/4 v4, 0x3

    invoke-direct {v7, v8, v4, v13}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    .line 786
    .local v7, "localClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    const-string v4, "eraser_bar"

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 787
    .local v1, "localDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v11, v4, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    .line 790
    .local v11, "targetSdkVersion":I
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xe

    if-lt v4, v5, :cond_3

    .line 791
    const/16 v4, 0xa

    if-gt v11, v4, :cond_1

    .line 792
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v4, v14, :cond_0

    .line 793
    const/high16 v4, 0x40d00000    # 6.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .line 809
    .local v3, "insetMargin":I
    :goto_0
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    move v4, v2

    move v5, v3

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 810
    .local v0, "localInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    const/4 v4, 0x2

    new-array v6, v4, [Landroid/graphics/drawable/Drawable;

    .line 811
    .local v6, "arrayOfDrawable":[Landroid/graphics/drawable/Drawable;
    aput-object v0, v6, v2

    .line 812
    aput-object v7, v6, v13

    .line 813
    new-instance v2, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v2, v6}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v10, v2}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 815
    iput-object v10, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mModulationSeekBar:Landroid/widget/SeekBar;

    .line 816
    return-object v10

    .line 795
    .end local v0    # "localInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    .end local v3    # "insetMargin":I
    .end local v6    # "arrayOfDrawable":[Landroid/graphics/drawable/Drawable;
    :cond_0
    const/high16 v4, 0x40b00000    # 5.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .line 797
    .restart local v3    # "insetMargin":I
    goto :goto_0

    .line 798
    .end local v3    # "insetMargin":I
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v4, v14, :cond_2

    .line 799
    const/high16 v4, 0x40600000    # 3.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .line 800
    .restart local v3    # "insetMargin":I
    goto :goto_0

    .line 801
    .end local v3    # "insetMargin":I
    :cond_2
    const/high16 v4, 0x40200000    # 2.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .line 804
    .restart local v3    # "insetMargin":I
    goto :goto_0

    .line 805
    .end local v3    # "insetMargin":I
    :cond_3
    const/high16 v4, 0x40000000    # 2.0f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .restart local v3    # "insetMargin":I
    goto :goto_0
.end method

.method private static native native_command(IILjava/util/ArrayList;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_command(JILjava/util/ArrayList;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_construct(I)Z
.end method

.method private static native native_construct(J)Z
.end method

.method private static native native_draw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_draw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_end(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_end(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_finalize(J)V
.end method

.method private static native native_getAdvancedSetting(I)Ljava/lang/String;
.end method

.method private static native native_getAdvancedSetting(J)Ljava/lang/String;
.end method

.method private static native native_getColor(I)I
.end method

.method private static native native_getColor(J)I
.end method

.method private static native native_getMaxSettingValue(I)F
.end method

.method private static native native_getMaxSettingValue(J)F
.end method

.method private static native native_getMinSettingValue(I)F
.end method

.method private static native native_getMinSettingValue(J)F
.end method

.method private static native native_getPenAttribute(II)Z
.end method

.method private static native native_getPenAttribute(JI)Z
.end method

.method private static native native_getProperty(ILandroid/os/Bundle;)Z
.end method

.method private static native native_getProperty(JLandroid/os/Bundle;)Z
.end method

.method private static native native_getSize(I)F
.end method

.method private static native native_getSize(J)F
.end method

.method private static native native_getStrokeRect(I[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;
.end method

.method private static native native_getStrokeRect(J[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;
.end method

.method private static native native_init()I
.end method

.method private static native native_init_64()J
.end method

.method private static native native_isCurveEnabled(I)Z
.end method

.method private static native native_isCurveEnabled(J)Z
.end method

.method private static native native_move(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_move(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_onLoad(I)V
.end method

.method private static native native_onLoad(J)V
.end method

.method private static native native_onUnload(I)V
.end method

.method private static native native_onUnload(J)V
.end method

.method private static native native_redraw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_redraw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_setAdvancedSetting(ILjava/lang/String;)Z
.end method

.method private static native native_setAdvancedSetting(JLjava/lang/String;)Z
.end method

.method private static native native_setBitmap(ILandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setBitmap(JLandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setColor(II)Z
.end method

.method private static native native_setColor(JI)Z
.end method

.method private static native native_setCurveEnabled(IZ)Z
.end method

.method private static native native_setCurveEnabled(JZ)Z
.end method

.method private static native native_setProperty(ILandroid/os/Bundle;)Z
.end method

.method private static native native_setProperty(JLandroid/os/Bundle;)Z
.end method

.method private static native native_setReferenceBitmap(ILandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setReferenceBitmap(JLandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setSize(IF)Z
.end method

.method private static native native_setSize(JF)Z
.end method

.method private static native native_start(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_start(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private resetButtonLayout()Landroid/view/View;
    .locals 5

    .prologue
    const/high16 v4, 0x40a00000    # 5.0f

    .line 822
    new-instance v0, Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 823
    .local v0, "button":Landroid/widget/Button;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    .line 824
    const/high16 v3, 0x42200000    # 40.0f

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .line 823
    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 825
    .local v1, "buttonLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v2, 0x11

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 826
    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 827
    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 828
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 830
    const-string v2, "reset"

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 832
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mResetButton:Landroid/view/View;

    .line 833
    return-object v0
.end method

.method private setBeautifyType(I)V
    .locals 6
    .param p1, "buttonIndex"    # I

    .prologue
    .line 1026
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    if-eqz v4, :cond_0

    .line 1027
    iput p1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I

    .line 1029
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1030
    .local v1, "s":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "settingIndex":I
    :goto_0
    const/16 v4, 0xa

    if-lt v2, v4, :cond_1

    .line 1054
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setAdvancedSetting(Ljava/lang/String;)V

    .line 1055
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;->onChanged(Ljava/lang/String;)V

    .line 1057
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_1
    const/4 v4, 0x6

    if-lt v0, v4, :cond_2

    .line 1065
    .end local v0    # "index":I
    .end local v1    # "s":Ljava/lang/StringBuffer;
    .end local v2    # "settingIndex":I
    :cond_0
    return-void

    .line 1031
    .restart local v1    # "s":Ljava/lang/StringBuffer;
    .restart local v2    # "settingIndex":I
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I

    aget-object v4, v4, p1

    aget v3, v4, v2

    .line 1033
    .local v3, "settingValue":I
    packed-switch v2, :pswitch_data_0

    .line 1050
    :goto_2
    :pswitch_0
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1051
    const/16 v4, 0x3b

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1030
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1035
    :pswitch_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCursiveSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_2

    .line 1038
    :pswitch_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSustenanceSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_2

    .line 1041
    :pswitch_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mDummySeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_2

    .line 1044
    :pswitch_4
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mModulationSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_2

    .line 1058
    .end local v3    # "settingValue":I
    .restart local v0    # "index":I
    :cond_2
    if-ne p1, v0, :cond_3

    .line 1059
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1057
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1061
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_3

    .line 1033
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "defaultImg"    # Ljava/lang/String;
    .param p2, "pressedImg"    # Ljava/lang/String;
    .param p3, "selectedImg"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 988
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 990
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 992
    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 993
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 991
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 995
    :cond_0
    if-eqz p2, :cond_1

    .line 996
    new-array v1, v4, [I

    const v2, 0x10100a7

    aput v2, v1, v3

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 998
    :cond_1
    if-eqz p3, :cond_2

    .line 999
    new-array v1, v4, [I

    const v2, 0x10100a1

    aput v2, v1, v3

    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1001
    :cond_2
    return-object v0

    .line 992
    nop

    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
    .end array-data
.end method

.method private setListener()V
    .locals 3

    .prologue
    .line 837
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    const/4 v1, 0x6

    if-lt v0, v1, :cond_5

    .line 859
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCursiveSeekBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_0

    .line 860
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCursiveSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSeekBarListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 863
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSustenanceSeekBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_1

    .line 864
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSustenanceSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSeekBarListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 867
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mDummySeekBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_2

    .line 868
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mDummySeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSeekBarListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 871
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mModulationSeekBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_3

    .line 872
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mModulationSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSeekBarListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 875
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mResetButton:Landroid/view/View;

    if-eqz v1, :cond_4

    .line 876
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mResetButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mResetButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 878
    :cond_4
    return-void

    .line 838
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    new-instance v2, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$3;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$3;-><init>(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 837
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private sustenanceLayout()Landroid/view/View;
    .locals 8

    .prologue
    const/high16 v7, 0x41400000    # 12.0f

    const/4 v6, -0x2

    const/4 v5, 0x0

    .line 594
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 595
    .local v1, "localLinearLayout":Landroid/widget/LinearLayout;
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 598
    .local v0, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v4, 0x40a00000    # 5.0f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v4

    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 601
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 604
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 605
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->sustenanceSeekbar()Landroid/widget/SeekBar;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 607
    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 608
    .local v3, "textView":Landroid/widget/TextView;
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 610
    .local v2, "textLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 612
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setTextSize(F)V

    .line 613
    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 614
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 615
    const-string/jumbo v4, "sustenance"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 616
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 617
    const/high16 v4, 0x40900000    # 4.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v4

    invoke-virtual {v3, v5, v4, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 618
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 619
    return-object v1
.end method

.method private sustenanceSeekbar()Landroid/widget/SeekBar;
    .locals 15

    .prologue
    const/16 v14, 0xa0

    const/4 v13, 0x1

    const/4 v2, 0x0

    .line 624
    new-instance v10, Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v10, v4}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 625
    .local v10, "localSeekBar":Landroid/widget/SeekBar;
    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v4, 0x42dc0000    # 110.0f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v4

    .line 626
    const/4 v5, -0x2

    .line 625
    invoke-direct {v9, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 627
    .local v9, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v10, v9}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 628
    const/16 v4, 0x10

    invoke-virtual {v10, v4}, Landroid/widget/SeekBar;->setMax(I)V

    .line 629
    const-string v4, "eraser_handel"

    const-string v5, "eraser_handel_press"

    const/4 v12, 0x0

    invoke-direct {p0, v4, v5, v12}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v4

    invoke-virtual {v10, v4}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 630
    new-instance v8, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v8}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 632
    .local v8, "localGradientDrawable":Landroid/graphics/drawable/GradientDrawable;
    const/high16 v4, 0x40a00000    # 5.0f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v8, v4}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 633
    new-instance v7, Landroid/graphics/drawable/ClipDrawable;

    const/4 v4, 0x3

    invoke-direct {v7, v8, v4, v13}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    .line 634
    .local v7, "localClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    const-string v4, "eraser_bar"

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 635
    .local v1, "localDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v11, v4, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    .line 638
    .local v11, "targetSdkVersion":I
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xe

    if-lt v4, v5, :cond_3

    .line 639
    const/16 v4, 0xa

    if-gt v11, v4, :cond_1

    .line 640
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v4, v14, :cond_0

    .line 641
    const/high16 v4, 0x40d00000    # 6.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .line 657
    .local v3, "insetMargin":I
    :goto_0
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    move v4, v2

    move v5, v3

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 658
    .local v0, "localInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    const/4 v4, 0x2

    new-array v6, v4, [Landroid/graphics/drawable/Drawable;

    .line 659
    .local v6, "arrayOfDrawable":[Landroid/graphics/drawable/Drawable;
    aput-object v0, v6, v2

    .line 660
    aput-object v7, v6, v13

    .line 661
    new-instance v2, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v2, v6}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v10, v2}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 663
    iput-object v10, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSustenanceSeekBar:Landroid/widget/SeekBar;

    .line 664
    return-object v10

    .line 643
    .end local v0    # "localInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    .end local v3    # "insetMargin":I
    .end local v6    # "arrayOfDrawable":[Landroid/graphics/drawable/Drawable;
    :cond_0
    const/high16 v4, 0x40b00000    # 5.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .line 645
    .restart local v3    # "insetMargin":I
    goto :goto_0

    .line 646
    .end local v3    # "insetMargin":I
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v4, v14, :cond_2

    .line 647
    const/high16 v4, 0x40600000    # 3.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .line 648
    .restart local v3    # "insetMargin":I
    goto :goto_0

    .line 649
    .end local v3    # "insetMargin":I
    :cond_2
    const/high16 v4, 0x40200000    # 2.5f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .line 652
    .restart local v3    # "insetMargin":I
    goto :goto_0

    .line 653
    .end local v3    # "insetMargin":I
    :cond_3
    const/high16 v4, 0x40000000    # 2.0f

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    .restart local v3    # "insetMargin":I
    goto :goto_0
.end method


# virtual methods
.method public construct()V
    .locals 2

    .prologue
    .line 137
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_construct(J)Z

    .line 138
    return-void
.end method

.method public draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v1, 0x0

    .line 211
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 213
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    .line 225
    :cond_0
    :goto_0
    return-void

    .line 216
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v1, v1}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 222
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_draw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 223
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public getAdvancedSetting()Ljava/lang/String;
    .locals 2

    .prologue
    .line 434
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_getAdvancedSetting(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getColor()I
    .locals 2

    .prologue
    .line 338
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_getColor(J)I

    move-result v0

    return v0
.end method

.method public getMaxSettingValue()F
    .locals 2

    .prologue
    .line 318
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_getMaxSettingValue(J)F

    move-result v0

    return v0
.end method

.method public getMinSettingValue()F
    .locals 2

    .prologue
    .line 309
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_getMinSettingValue(J)F

    move-result v0

    return v0
.end method

.method public getNativeHandle()J
    .locals 2

    .prologue
    .line 202
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    return-wide v0
.end method

.method public getPenAttribute(I)Z
    .locals 2
    .param p1, "attribute"    # I

    .prologue
    .line 453
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_getPenAttribute(JI)Z

    move-result v0

    return v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "propertyMap"    # Landroid/os/Bundle;

    .prologue
    .line 193
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_getProperty(JLandroid/os/Bundle;)Z

    .line 194
    return-void
.end method

.method public getSize()F
    .locals 2

    .prologue
    .line 300
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_getSize(J)F

    move-result v0

    return v0
.end method

.method public getStrokeRect([Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;
    .locals 10
    .param p1, "points"    # [Landroid/graphics/PointF;
    .param p2, "pressures"    # [F
    .param p3, "timestamps"    # [I
    .param p4, "size"    # F
    .param p5, "isCurvable"    # Z
    .param p6, "advanced"    # Ljava/lang/String;

    .prologue
    .line 444
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move v7, p4

    move v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v1 .. v9}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_getStrokeRect(J[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public hideAdvancedSetting()V
    .locals 2

    .prologue
    .line 394
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 401
    :cond_0
    :goto_0
    return-void

    .line 398
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public isCurveEnabled()Z
    .locals 2

    .prologue
    .line 358
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_isCurveEnabled(J)Z

    move-result v0

    return v0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 146
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_onLoad(J)V

    .line 147
    return-void
.end method

.method public onUnload()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 173
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    .line 174
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    .line 175
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_onUnload(J)V

    .line 176
    return-void
.end method

.method public redrawPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 7
    .param p1, "allEvent"    # Landroid/view/MotionEvent;
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v1, 0x0

    .line 233
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 235
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v1, v1}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 244
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_redraw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 245
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setAdvancedSetting(Ljava/lang/String;)V
    .locals 4
    .param p1, "advancedSetting"    # Ljava/lang/String;

    .prologue
    .line 409
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 426
    :cond_0
    :goto_0
    return-void

    .line 412
    :cond_1
    const-string v1, ";"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 413
    .local v0, "s":[Ljava/lang/String;
    array-length v1, v0

    if-eqz v1, :cond_0

    .line 423
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    invoke-direct {p0, v2, v3, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_setAdvancedSetting(JLjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 424
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 255
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    .line 256
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 259
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_setBitmap(JLandroid/graphics/Bitmap;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 260
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 327
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_setColor(JI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 328
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 330
    :cond_0
    return-void
.end method

.method public setCurveEnabled(Z)V
    .locals 2
    .param p1, "curve"    # Z

    .prologue
    .line 347
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_setCurveEnabled(JZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 348
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 350
    :cond_0
    return-void
.end method

.method protected setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p1, "paramString"    # Ljava/lang/String;

    .prologue
    .line 1010
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 1011
    .local v3, "manager":Landroid/content/pm/PackageManager;
    const/4 v1, 0x0

    .line 1013
    .local v1, "mApk1Resources":Landroid/content/res/Resources;
    :try_start_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1019
    const-string v4, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 1021
    .local v2, "mDrawableResID":I
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .end local v2    # "mDrawableResID":I
    :goto_0
    return-object v4

    .line 1014
    :catch_0
    move-exception v0

    .line 1015
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v4, "TAG"

    const-string v5, "Resource loading is Failed"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1016
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 1017
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "propertyMap"    # Landroid/os/Bundle;

    .prologue
    .line 184
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_setProperty(JLandroid/os/Bundle;)Z

    .line 185
    return-void
.end method

.method public setReferenceBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 281
    return-void
.end method

.method public setSize(F)V
    .locals 2
    .param p1, "size"    # F

    .prologue
    .line 289
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->Native_setSize(JF)Z

    move-result v0

    if-nez v0, :cond_0

    .line 290
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 292
    :cond_0
    return-void
.end method

.method public showAdvancedSetting(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;Landroid/view/ViewGroup;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;
    .param p3, "parentView"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v2, 0x0

    .line 367
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    if-nez v1, :cond_0

    .line 368
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    .line 369
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 370
    .local v0, "localDisplayMetrics":Landroid/util/DisplayMetrics;
    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    sput v1, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mDensity:F

    .line 372
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->initView()Landroid/widget/LinearLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    .line 373
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p3, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 374
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p3, v1}, Landroid/view/ViewGroup;->bringChildToFront(Landroid/view/View;)V

    .line 375
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 377
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setListener()V

    .line 378
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    .line 380
    iput v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I

    .line 381
    iget v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setBeautifyType(I)V

    .line 386
    .end local v0    # "localDisplayMetrics":Landroid/util/DisplayMetrics;
    :goto_0
    return-void

    .line 383
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p3, v1}, Landroid/view/ViewGroup;->bringChildToFront(Landroid/view/View;)V

    .line 384
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 164
    const/4 v0, 0x1

    return v0
.end method
