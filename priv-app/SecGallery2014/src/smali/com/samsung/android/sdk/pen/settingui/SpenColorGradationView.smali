.class Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;
.super Landroid/widget/ImageView;
.source "SpenColorGradationView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;
    }
.end annotation


# instance fields
.field private mBorderPaint:Landroid/graphics/Paint;

.field private mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

.field private mCurrentColor:I

.field private mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

.field private mCursorRect:Landroid/graphics/Rect;

.field private mCursorVisible:Z

.field private mCustom_imagepath:Ljava/lang/String;

.field mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private mHeight:I

.field private mRatio:F

.field private mSpectrum:Landroid/graphics/Bitmap;

.field private mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;F)V
    .locals 2
    .param p1, "paramContext"    # Landroid/content/Context;
    .param p2, "custom_imagepath"    # Ljava/lang/String;
    .param p3, "ratio"    # F

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorVisible:Z

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCurrentColor:I

    .line 34
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mRatio:F

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCustom_imagepath:Ljava/lang/String;

    .line 50
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCustom_imagepath:Ljava/lang/String;

    .line 51
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCustom_imagepath:Ljava/lang/String;

    invoke-direct {v0, p1, v1, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 52
    iput p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mRatio:F

    .line 53
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->initView()V

    .line 54
    return-void
.end method

.method private initView()V
    .locals 6

    .prologue
    const/16 v2, 0xa

    const/4 v5, 0x1

    .line 256
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_color_select_kit"

    invoke-virtual {v0, v1, v2, v2, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mWidth:I

    .line 260
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mHeight:I

    .line 261
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mWidth:I

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mHeight:I

    neg-int v2, v2

    div-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mWidth:I

    div-int/lit8 v3, v3, 0x2

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mHeight:I

    div-int/lit8 v4, v4, 0x2

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorRect:Landroid/graphics/Rect;

    .line 262
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 270
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mBorderPaint:Landroid/graphics/Paint;

    .line 271
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mBorderPaint:Landroid/graphics/Paint;

    const v1, -0x737374

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 272
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mBorderPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 273
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mBorderPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 274
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 60
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_2

    .line 69
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 70
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 72
    :cond_1
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 76
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_4

    .line 77
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 78
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 80
    :cond_3
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 83
    :cond_4
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

    .line 85
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 87
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorRect:Landroid/graphics/Rect;

    .line 88
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mBorderPaint:Landroid/graphics/Paint;

    .line 90
    return-void
.end method

.method protected getGradientCursorRectVisibility()Z
    .locals 1

    .prologue
    .line 282
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorVisible:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    .line 169
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 170
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorVisible:Z

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 174
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mBorderPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 180
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 4
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 208
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 210
    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_2

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 212
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 213
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_color_box"

    const/16 v2, 0xc2

    const/16 v3, 0x2f

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 218
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    .line 220
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 222
    :cond_2
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x0

    const/high16 v12, -0x2000000

    const/high16 v8, 0x40000000    # 2.0f

    const v11, 0xffffff

    const/4 v10, 0x1

    .line 101
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 102
    .local v1, "i":I
    const/4 v5, 0x3

    if-ne v1, v5, :cond_0

    .line 103
    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->playSoundEffect(I)V

    .line 104
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

    if-eqz v5, :cond_0

    .line 105
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCurrentColor:I

    invoke-interface {v5, v10, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;->onActionUp(ZI)V

    .line 156
    :goto_0
    return v10

    .line 109
    :cond_0
    const/4 v5, 0x2

    if-ne v1, v5, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 110
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    invoke-interface {v5, v10}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 112
    :cond_1
    if-ne v1, v10, :cond_2

    .line 113
    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->playSoundEffect(I)V

    .line 114
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

    if-eqz v5, :cond_2

    .line 115
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCurrentColor:I

    invoke-interface {v5, v10, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;->onActionUp(ZI)V

    .line 119
    :cond_2
    const/4 v0, -0x1

    .line 121
    .local v0, "color":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v2, v5

    .line 122
    .local v2, "mCurX":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v3, v5

    .line 123
    .local v3, "mCurY":I
    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mRatio:F

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v4, v6

    .line 125
    .local v4, "minValue":I
    if-ge v2, v4, :cond_3

    .line 126
    move v2, v4

    .line 128
    :cond_3
    if-ge v3, v4, :cond_4

    .line 129
    move v3, v4

    .line 131
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_7

    .line 132
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    add-int v6, v2, v4

    if-gt v5, v6, :cond_5

    .line 133
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mRatio:F

    mul-float/2addr v6, v8

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v6, v6

    sub-int v2, v5, v6

    .line 135
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    add-int v6, v3, v4

    if-gt v5, v6, :cond_6

    .line 136
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mRatio:F

    mul-float/2addr v6, v8

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v6, v6

    sub-int v3, v5, v6

    .line 138
    :cond_6
    mul-int/lit8 v5, v4, 0x2

    if-lt v3, v5, :cond_7

    .line 139
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v2, v3}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    .line 143
    :cond_7
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorRect:Landroid/graphics/Rect;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mWidth:I

    div-int/lit8 v6, v6, 0x2

    sub-int v6, v2, v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mHeight:I

    div-int/lit8 v7, v7, 0x2

    sub-int v7, v3, v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mWidth:I

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v8, v2

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mHeight:I

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v9, v3

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 145
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorRect:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 147
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

    if-eqz v5, :cond_9

    .line 148
    and-int v5, v0, v11

    if-ne v5, v11, :cond_8

    .line 149
    const v0, 0xfefefe

    .line 151
    :cond_8
    and-int v5, v11, v0

    or-int/2addr v5, v12

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCurrentColor:I

    .line 152
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

    and-int v6, v11, v0

    or-int/2addr v6, v12

    invoke-interface {v5, v6, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;->onColorChanged(III)V

    .line 153
    iput-boolean v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorVisible:Z

    .line 155
    :cond_9
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->invalidate()V

    goto/16 :goto_0
.end method

.method public selectColorForGradiation(ID)V
    .locals 11
    .param p1, "color"    # I
    .param p2, "deltaColor"    # D

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/high16 v7, 0x43b40000    # 360.0f

    const/4 v6, 0x1

    const/high16 v8, 0x40000000    # 2.0f

    .line 226
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorVisible:Z

    .line 227
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v2, :cond_0

    .line 228
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v3, "snote_color_box"

    const/16 v4, 0xc2

    const/16 v5, 0x2f

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 230
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    if-nez v2, :cond_1

    .line 231
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    .line 233
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-nez v2, :cond_2

    .line 234
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 237
    :cond_2
    const/4 v2, 0x3

    new-array v0, v2, [F

    .line 238
    .local v0, "hsv":[F
    invoke-static {p1, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 241
    aget v2, v0, v6

    aget v3, v0, v10

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    .line 242
    aget v2, v0, v6

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    div-float/2addr v2, v8

    float-to-int v1, v2

    .line 247
    .local v1, "mPosY":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    aget v4, v0, v9

    mul-float/2addr v3, v4

    div-float/2addr v3, v7

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mWidth:I

    int-to-float v4, v4

    div-float/2addr v4, v8

    sub-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v4, v1

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mHeight:I

    int-to-float v5, v5

    div-float/2addr v5, v8

    sub-float/2addr v4, v5

    float-to-int v4, v4

    .line 248
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    aget v6, v0, v9

    mul-float/2addr v5, v6

    div-float/2addr v5, v7

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mWidth:I

    int-to-float v6, v6

    div-float/2addr v6, v8

    add-float/2addr v5, v6

    float-to-int v5, v5

    int-to-float v6, v1

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mHeight:I

    int-to-float v7, v7

    div-float/2addr v7, v8

    add-float/2addr v6, v7

    float-to-int v6, v6

    .line 247
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 249
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 250
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->invalidate()V

    .line 251
    return-void

    .line 244
    .end local v1    # "mPosY":I
    :cond_3
    aget v2, v0, v10

    sub-float v2, v8, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    div-float/2addr v2, v8

    float-to-int v1, v2

    .restart local v1    # "mPosY":I
    goto :goto_0
.end method

.method public setColorPickerColorChangeListener(Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;)V
    .locals 0
    .param p1, "parama"    # Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

    .line 191
    return-void
.end method

.method protected setGradientCursorRectVisibility(Z)V
    .locals 0
    .param p1, "isVisible"    # Z

    .prologue
    .line 277
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->mCursorVisible:Z

    .line 278
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->invalidate()V

    .line 279
    return-void
.end method
