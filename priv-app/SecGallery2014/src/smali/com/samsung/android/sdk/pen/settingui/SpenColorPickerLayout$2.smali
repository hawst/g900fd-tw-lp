.class Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;
.super Ljava/lang/Object;
.source "SpenColorPickerLayout.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    .line 644
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 649
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Landroid/widget/RelativeLayout;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->requestDisallowInterceptTouchEvent(Z)V

    .line 651
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v10

    float-to-int v8, v10

    .line 652
    .local v8, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v10

    float-to-int v9, v10

    .line 654
    .local v9, "y":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    .line 720
    :goto_0
    :pswitch_0
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    check-cast v10, Landroid/view/ViewGroup;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 721
    const/4 v10, 0x1

    return v10

    .line 656
    :pswitch_1
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerHandle:Landroid/view/View;

    .line 657
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 656
    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 658
    .local v5, "spoidSettingParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget v11, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    sub-int v11, v8, v11

    invoke-static {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;I)V

    .line 659
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget v11, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    sub-int v11, v9, v11

    invoke-static {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;I)V

    goto :goto_0

    .line 664
    .end local v5    # "spoidSettingParams":Landroid/widget/RelativeLayout$LayoutParams;
    :pswitch_2
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerHandle:Landroid/view/View;

    .line 665
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    .line 664
    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 666
    .local v6, "spoidSettinghandleParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitdBG:Landroid/view/View;

    .line 667
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 666
    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 668
    .local v4, "spoidSettingBgParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerColorImage:Landroid/view/View;

    .line 669
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 668
    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 670
    .local v2, "spoidColorImageParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerdExitBtn:Landroid/view/View;

    .line 671
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 670
    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 672
    .local v3, "spoidExitBtnParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerCurrentColor:Landroid/view/View;

    .line 673
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    .line 672
    check-cast v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 675
    .local v7, "spuitCurrentColorParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mXDelta:I
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)I

    move-result v10

    sub-int v10, v8, v10

    iput v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 676
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mYDelta:I
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$7(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)I

    move-result v10

    sub-int v10, v9, v10

    iput v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 678
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-gez v10, :cond_0

    .line 679
    const/4 v10, 0x0

    iput v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 681
    :cond_0
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    if-gez v10, :cond_1

    .line 682
    const/4 v10, 0x0

    iput v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 685
    :cond_1
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v10

    const/high16 v11, 0x42d40000    # 106.0f

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 686
    .local v1, "minWidth":I
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v10

    const/high16 v11, 0x42140000    # 37.0f

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 688
    .local v0, "minHeight":I
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v11

    sub-int/2addr v11, v1

    if-le v10, v11, :cond_2

    .line 689
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Landroid/widget/RelativeLayout;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v10

    sub-int/2addr v10, v1

    iput v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 691
    :cond_2
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v11

    sub-int/2addr v11, v0

    if-le v10, v11, :cond_3

    .line 692
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Landroid/widget/RelativeLayout;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v10

    sub-int/2addr v10, v0

    iput v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 695
    :cond_3
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 696
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v11

    const/high16 v12, 0x41b00000    # 22.0f

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v11

    add-int/2addr v10, v11

    .line 695
    iput v10, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 697
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    add-int/lit8 v10, v10, 0x0

    iput v10, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 699
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 700
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v11

    const/high16 v12, 0x41c80000    # 25.0f

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v11

    add-int/2addr v10, v11

    .line 699
    iput v10, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 701
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 702
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v11

    const/high16 v12, 0x40e00000    # 7.0f

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v11

    add-int/2addr v10, v11

    .line 701
    iput v10, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 704
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 705
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v11

    const/high16 v12, 0x42480000    # 50.0f

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v11

    add-int/2addr v10, v11

    .line 704
    iput v10, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 706
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 707
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v11

    const/high16 v12, 0x40e00000    # 7.0f

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v11

    add-int/2addr v10, v11

    .line 706
    iput v10, v7, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 709
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 710
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v11

    const/high16 v12, 0x429c0000    # 78.0f

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v11

    add-int/2addr v10, v11

    .line 709
    iput v10, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 711
    iget v10, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v10, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 713
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v10, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 714
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v10, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 715
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v10, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 716
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v10, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 717
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v10, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 654
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
