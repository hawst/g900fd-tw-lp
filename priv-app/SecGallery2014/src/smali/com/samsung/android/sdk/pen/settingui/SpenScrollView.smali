.class Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;
.super Landroid/widget/ScrollView;
.source "SpenScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;
    }
.end annotation


# instance fields
.field private mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private mScrollable:Z

.field private onScrollChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 11
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    .line 25
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 71
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->mScrollable:Z

    .line 26
    new-instance v6, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v7, ""

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-direct {v6, p1, v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 27
    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v7, "SM-T80"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v7, "SM-T70"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 29
    :cond_0
    :try_start_0
    const-class v6, Landroid/view/View;

    const-string v7, "mScrollCache"

    invoke-virtual {v6, v7}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 30
    .local v2, "mScrollCacheField":Ljava/lang/reflect/Field;
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 31
    invoke-virtual {v2, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 32
    .local v1, "mScrollCache":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    const-string v7, "scrollBar"

    invoke-virtual {v6, v7}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    .line 33
    .local v5, "scrollBarField":Ljava/lang/reflect/Field;
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 34
    invoke-virtual {v5, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 35
    .local v4, "scrollBar":Ljava/lang/Object;
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    const-string/jumbo v7, "setVerticalThumbDrawable"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, Landroid/graphics/drawable/Drawable;

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 36
    .local v3, "method":Ljava/lang/reflect/Method;
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 37
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v9, "tw_scrollbar_holo_light"

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v4, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    .end local v1    # "mScrollCache":Ljava/lang/Object;
    .end local v2    # "mScrollCacheField":Ljava/lang/reflect/Field;
    .end local v3    # "method":Ljava/lang/reflect/Method;
    .end local v4    # "scrollBar":Ljava/lang/Object;
    .end local v5    # "scrollBarField":Ljava/lang/reflect/Field;
    :cond_1
    :goto_0
    return-void

    .line 39
    :catch_0
    move-exception v0

    .line 40
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public computeVerticalScrollRange()I
    .locals 1

    .prologue
    .line 47
    invoke-super {p0}, Landroid/widget/ScrollView;->computeVerticalScrollRange()I

    move-result v0

    return v0
.end method

.method public isScrollable()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->mScrollable:Z

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->mScrollable:Z

    if-nez v0, :cond_0

    .line 100
    const/4 v0, 0x0

    .line 102
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 52
    invoke-super/range {p0 .. p5}, Landroid/widget/ScrollView;->onLayout(ZIIII)V

    .line 53
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 1
    .param p1, "mScrollX"    # I
    .param p2, "mScrollY"    # I
    .param p3, "oldX"    # I
    .param p4, "oldY"    # I

    .prologue
    .line 57
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 58
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->onScrollChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->onScrollChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

    invoke-interface {v0, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;->scrollChanged(I)V

    .line 61
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 83
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 91
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 86
    :pswitch_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->mScrollable:Z

    if-eqz v0, :cond_0

    .line 87
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 89
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->mScrollable:Z

    goto :goto_0

    .line 83
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public setOnScrollChangedListener(Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;)V
    .locals 0
    .param p1, "mScrollChangedListener"    # Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->onScrollChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

    .line 65
    return-void
.end method

.method public setScrollingEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->mScrollable:Z

    .line 75
    return-void
.end method
