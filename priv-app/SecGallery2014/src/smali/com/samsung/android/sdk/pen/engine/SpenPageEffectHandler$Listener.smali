.class public interface abstract Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;
.super Ljava/lang/Object;
.source "SpenPageEffectHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onFinish()V
.end method

.method public abstract onUpdate()V
.end method

.method public abstract onUpdateCanvasLayer(Landroid/graphics/Canvas;)V
.end method

.method public abstract onUpdateCanvasLayer2(Landroid/graphics/Canvas;)V
.end method
