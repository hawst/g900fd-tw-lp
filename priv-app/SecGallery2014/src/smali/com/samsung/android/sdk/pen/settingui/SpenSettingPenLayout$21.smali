.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;
.super Ljava/lang/Object;
.source "SpenSettingPenLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    .line 2013
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 2019
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mViewMode:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$68(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v3

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mViewMode:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$68(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v3

    const/4 v4, 0x6

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mViewMode:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$68(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v3

    const/4 v4, 0x7

    if-ne v3, v4, :cond_1

    .line 2142
    :cond_0
    :goto_0
    return-void

    .line 2022
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$47(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 2024
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$87(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    move-result-object v4

    const-string/jumbo v5, "string_pen"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$87(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    move-result-object v4

    const-string/jumbo v5, "string_tab_selected_tts"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 2025
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 2023
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2026
    .local v2, "strPenTabChar":Ljava/lang/CharSequence;
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$87(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    move-result-object v4

    const-string/jumbo v5, "string_preset"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 2027
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$87(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    move-result-object v4

    const-string/jumbo v5, "string_tab_selected_tts"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 2026
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 2027
    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 2026
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2029
    .local v1, "strPenPresetChar":Ljava/lang/CharSequence;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$89(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$41(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2030
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$89(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2031
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$41(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2033
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$106(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 2035
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$89(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setSelected(Z)V

    .line 2036
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$41(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setSelected(Z)V

    .line 2037
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-boolean v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    if-eqz v3, :cond_4

    .line 2038
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v4

    const v5, 0x44bb8000    # 1500.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$25(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    .line 2042
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->drawExpendImage(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$90(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/lang/String;)V

    .line 2043
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewLayout:Landroid/view/View;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$92(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2044
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeLayout:Landroid/view/ViewGroup;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$93(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/ViewGroup;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2045
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSeekbarLayout:Landroid/view/View;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$94(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2046
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$22(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2047
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$40(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setVisibility(I)V

    .line 2048
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$97(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2049
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setColorSelectorViewForBeautifyPen()V
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$107(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    .line 2051
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$102()I

    move-result v3

    const/16 v4, 0x10

    if-ge v3, v4, :cond_5

    .line 2052
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$99(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$103()Ljava/lang/String;

    move-result-object v5

    .line 2053
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$108()Ljava/lang/String;

    move-result-object v6

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$108()Ljava/lang/String;

    move-result-object v7

    .line 2052
    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2063
    :goto_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$99(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageView;->invalidate()V

    .line 2066
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isBeautifyPen(Ljava/lang/String;)Z
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2067
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v4, 0x1

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyEnableLayout(Z)V
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$95(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 2068
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v4, 0x1

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyStyleBtnsLayout(Z)V
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$96(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 2076
    :goto_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMontblancMode:Z
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$111(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v4, "com.samsung.android.sdk.pen.pen.preload.MontblancFountainPen"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2077
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMontblancStyleBtnsLayout:Landroid/view/View;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$105(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2079
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetLayout:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$104(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/LinearLayout;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2081
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMagicPenEnable:Z
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$44(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2082
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$97(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getY()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$112(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    .line 2083
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->currenMagicPenHeight:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$113(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v4

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setMagicPenMode(I)V
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$114(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    .line 2138
    :cond_3
    :goto_4
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setButtonFocus(Landroid/view/View;)V
    invoke-static {v3, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$88(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 2139
    .end local v1    # "strPenPresetChar":Ljava/lang/CharSequence;
    .end local v2    # "strPenTabChar":Ljava/lang/CharSequence;
    :catch_0
    move-exception v0

    .line 2140
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_0

    .line 2040
    .end local v0    # "e":Ljava/lang/NullPointerException;
    .restart local v1    # "strPenPresetChar":Ljava/lang/CharSequence;
    .restart local v2    # "strPenTabChar":Ljava/lang/CharSequence;
    :cond_4
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$91(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v4

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$25(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    goto/16 :goto_1

    .line 2054
    :cond_5
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$102()I

    move-result v3

    const/16 v4, 0x15

    if-lt v3, v4, :cond_6

    .line 2055
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$99(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v3

    new-instance v4, Landroid/graphics/drawable/RippleDrawable;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v5

    const/16 v6, 0x29

    .line 2056
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 2055
    invoke-static {v6, v7, v8, v9}, Landroid/graphics/Color;->argb(IIII)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    .line 2056
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v6

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$103()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, v7}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2055
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 2059
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$99(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$103()Ljava/lang/String;

    move-result-object v5

    .line 2060
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$108()Ljava/lang/String;

    move-result-object v6

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$108()Ljava/lang/String;

    move-result-object v7

    .line 2059
    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 2069
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isChinesePen(Ljava/lang/String;)Z
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$109(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2070
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v4, 0x1

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyEnableLayout(Z)V
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$95(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    goto/16 :goto_3

    .line 2072
    :cond_8
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v4, 0x0

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyEnableLayout(Z)V
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$95(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 2073
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v4, 0x0

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifySettingViews(Z)V
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$110(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    goto/16 :goto_3

    .line 2085
    :cond_9
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$41(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2086
    const/4 v3, 0x1

    sput-boolean v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsSwichTab:Z

    .line 2087
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v3

    if-eqz v3, :cond_a

    .line 2088
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v3

    sget-boolean v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsSwichTab:Z

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->setSwitchTabFlag(Z)V

    .line 2090
    :cond_a
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$89(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2091
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$41(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2093
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$106(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 2095
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$89(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setSelected(Z)V

    .line 2096
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$41(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setSelected(Z)V

    .line 2097
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->drawExpendImage(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$90(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/lang/String;)V

    .line 2098
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-boolean v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    if-eqz v3, :cond_d

    .line 2099
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v4

    const v5, 0x44bb8000    # 1500.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$25(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    .line 2103
    :goto_5
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewLayout:Landroid/view/View;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$92(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2104
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeLayout:Landroid/view/ViewGroup;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$93(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/ViewGroup;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2105
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSeekbarLayout:Landroid/view/View;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$94(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2106
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$22(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2107
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$40(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setVisibility(I)V

    .line 2108
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v4, 0x0

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyEnableLayout(Z)V
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$95(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 2109
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v4, 0x0

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyStyleBtnsLayout(Z)V
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$96(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 2110
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$97(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2111
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMontblancStyleBtnsLayout:Landroid/view/View;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$105(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2112
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$98(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$99(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$100()Ljava/lang/String;

    move-result-object v5

    .line 2113
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$101()Ljava/lang/String;

    move-result-object v6

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$101()Ljava/lang/String;

    move-result-object v7

    .line 2112
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2114
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$102()I

    move-result v3

    const/16 v4, 0x10

    if-ge v3, v4, :cond_e

    .line 2115
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$99(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$100()Ljava/lang/String;

    move-result-object v5

    .line 2116
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$101()Ljava/lang/String;

    move-result-object v6

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$101()Ljava/lang/String;

    move-result-object v7

    .line 2115
    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2126
    :goto_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$99(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageView;->invalidate()V

    .line 2127
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_b

    .line 2128
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$85(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/util/List;)V

    .line 2130
    :cond_b
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->updatePresetAdapter()V
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$43(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    .line 2132
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMagicPenEnable:Z
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$44(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 2133
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$97(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getY()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$112(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    .line 2135
    :cond_c
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->presetDisplay()V
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    .line 2136
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetLayout:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$104(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/LinearLayout;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_4

    .line 2101
    :cond_d
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$91(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v4

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$25(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    goto/16 :goto_5

    .line 2117
    :cond_e
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$102()I

    move-result v3

    const/16 v4, 0x15

    if-lt v3, v4, :cond_f

    .line 2118
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$99(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v3

    new-instance v4, Landroid/graphics/drawable/RippleDrawable;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v5

    const/16 v6, 0x29

    .line 2119
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 2118
    invoke-static {v6, v7, v8, v9}, Landroid/graphics/Color;->argb(IIII)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    .line 2119
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v6

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$103()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, v7}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2118
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_6

    .line 2122
    :cond_f
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$99(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$100()Ljava/lang/String;

    move-result-object v5

    .line 2123
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$101()Ljava/lang/String;

    move-result-object v6

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$101()Ljava/lang/String;

    move-result-object v7

    .line 2122
    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_6
.end method
