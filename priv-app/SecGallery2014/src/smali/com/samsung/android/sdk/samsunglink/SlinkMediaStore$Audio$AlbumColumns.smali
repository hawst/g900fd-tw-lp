.class public interface abstract Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$AlbumColumns;
.super Ljava/lang/Object;
.source "SlinkMediaStore.java"

# interfaces
.implements Landroid/provider/MediaStore$Audio$AlbumColumns;
.implements Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$BaseSamsungLinkColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AlbumColumns"
.end annotation


# static fields
.field public static final ALBUM_ARTIST:Ljava/lang/String; = "album_artist"

.field public static final ALBUM_ARTIST_KEY:Ljava/lang/String; = "album_artist_key"

.field public static final ALBUM_INDEX_CHAR:Ljava/lang/String; = "album_index_char"

.field public static final ARTIST_ID:Ljava/lang/String; = "artist_id"

.field public static final AUDIO_ID:Ljava/lang/String; = "audio_id"

.field public static final LOCAL_SOURCE_ALBUM_ID:Ljava/lang/String; = "local_source_album_id"

.field public static final NUMBER_OF_DUP_REDUCED_SONGS:Ljava/lang/String; = "num_dup_reduced_songs"

.field public static final PHYSICAL_TYPE:Ljava/lang/String; = "physical_type"

.field public static final TRANSPORT_TYPE:Ljava/lang/String; = "transport_type"
