.class public Lcom/samsung/android/sdk/pen/Spen;
.super Ljava/lang/Object;
.source "Spen.java"

# interfaces
.implements Lcom/samsung/android/sdk/SsdkInterface;


# static fields
.field private static final DEFAULT_MAX_CACHE_SIZE:I = 0x5

.field public static final DEVICE_PEN:I = 0x0

.field public static IS_SPEN_PRELOAD_MODE:Z = false

.field private static final LOG_TAG:Ljava/lang/String; = "SpenSdk"

.field public static final SPEN_DYNAMIC_LIB_MODE:I = 0x0

.field public static final SPEN_NATIVE_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.android.sdk.spenv10"

.field private static final SPEN_NATIVE_PACKAGE_NAME_64BIT:Ljava/lang/String; = "com.samsung.android.sdk.spen30_64"

.field private static final SPEN_NATIVE_PACKAGE_NAME_PRELOAD:Ljava/lang/String; = "com.samsung.android.sdk.spenv10"

.field public static final SPEN_STATIC_LIB_MODE:I = 0x1

.field private static final VERSION:Ljava/lang/String; = "3.0.259"

.field private static final VERSION_LEVEL:I = 0x8f88b13

.field private static mIsInitialized:Z

.field private static mSpenPackageName:Ljava/lang/String;

.field private static os:I


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/sdk/pen/Spen;->IS_SPEN_PRELOAD_MODE:Z

    .line 71
    sput-boolean v1, Lcom/samsung/android/sdk/pen/Spen;->mIsInitialized:Z

    .line 75
    const-string v0, "com.samsung.android.sdk.spenv10"

    sput-object v0, Lcom/samsung/android/sdk/pen/Spen;->mSpenPackageName:Ljava/lang/String;

    .line 79
    sput v1, Lcom/samsung/android/sdk/pen/Spen;->os:I

    .line 81
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static native SPenSdk_OSType()I
.end method

.method private static native SPenSdk_init(Ljava/lang/String;II)Z
.end method

.method private static native SPenSdk_init2(Ljava/lang/String;III)Z
.end method

.method public static getSpenPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/samsung/android/sdk/pen/Spen;->mSpenPackageName:Ljava/lang/String;

    return-object v0
.end method

.method private insertLog(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 105
    const/4 v6, -0x1

    .line 108
    .local v6, "version":I
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const-string v8, "com.samsung.android.providers.context"

    const/16 v9, 0x80

    invoke-virtual {v7, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    .line 109
    .local v5, "pInfo":Landroid/content/pm/PackageInfo;
    iget v6, v5, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    .end local v5    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    const-string v7, "SpenSdk"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "versionCode: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    const/4 v7, 0x1

    if-le v6, v7, :cond_0

    .line 115
    const-string v7, "com.samsung.android.providers.context.permission.WRITE_USE_APP_FEATURE_SURVEY"

    invoke-virtual {p1, v7}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v7

    if-eqz v7, :cond_1

    .line 117
    new-instance v7, Ljava/lang/SecurityException;

    invoke-direct {v7}, Ljava/lang/SecurityException;-><init>()V

    throw v7

    .line 110
    :catch_0
    move-exception v3

    .line 111
    .local v3, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v7, "SpenSdk"

    const-string v8, "Could not find ContextProvider"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 120
    .end local v3    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    const-string v7, "SpenSdk"

    const-string v8, "Add com.samsung.android.providers.context.permission.WRITE_USE_APP_FEATURE_SURVEY permission"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    :goto_1
    return-void

    .line 124
    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 126
    .local v2, "cv":Landroid/content/ContentValues;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v0

    .line 127
    .local v0, "appId":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "#"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/Spen;->getVersionCode()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 129
    .local v4, "feature":Ljava/lang/String;
    const-string v7, "app_id"

    invoke-virtual {v2, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const-string v7, "feature"

    invoke-virtual {v2, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 134
    .local v1, "broadcastIntent":Landroid/content/Intent;
    const-string v7, "com.samsung.android.providers.context.log.action.USE_APP_FEATURE_SURVEY"

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    const-string v7, "data"

    invoke-virtual {v1, v7, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 137
    const-string v7, "com.samsung.android.providers.context"

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 139
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public static osType()I
    .locals 3

    .prologue
    .line 503
    sget v1, Lcom/samsung/android/sdk/pen/Spen;->os:I

    if-eqz v1, :cond_0

    .line 504
    sget v1, Lcom/samsung/android/sdk/pen/Spen;->os:I

    .line 512
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return v1

    .line 507
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->SPenSdk_OSType()I

    move-result v1

    sput v1, Lcom/samsung/android/sdk/pen/Spen;->os:I

    .line 508
    sget v1, Lcom/samsung/android/sdk/pen/Spen;->os:I
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 509
    :catch_0
    move-exception v0

    .line 510
    .restart local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    const-string v1, "SpenSdk"

    const-string v2, "osType - old so"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    const/16 v1, 0x20

    sput v1, Lcom/samsung/android/sdk/pen/Spen;->os:I

    .line 512
    sget v1, Lcom/samsung/android/sdk/pen/Spen;->os:I

    goto :goto_0
.end method


# virtual methods
.method V58433d09d024(I)I
    .locals 1
    .param p1, "input"    # I

    .prologue
    .line 100
    mul-int v0, p1, p1

    add-int/2addr v0, p1

    return v0
.end method

.method public getVersionCode()I
    .locals 1

    .prologue
    .line 174
    const v0, 0x8f88b13

    return v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    const-string v0, "3.0.259"

    return-object v0
.end method

.method public initialize(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/sdk/SsdkUnsupportedException;
        }
    .end annotation

    .prologue
    .line 224
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sdk/pen/Spen;->initialize(Landroid/content/Context;I)V

    .line 225
    return-void
.end method

.method public initialize(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "maxCacheSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/sdk/SsdkUnsupportedException;
        }
    .end annotation

    .prologue
    .line 249
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/android/sdk/pen/Spen;->initialize(Landroid/content/Context;II)V

    .line 250
    return-void
.end method

.method public initialize(Landroid/content/Context;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "maxCacheSize"    # I
    .param p3, "createMode"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/sdk/SsdkUnsupportedException;
        }
    .end annotation

    .prologue
    .line 276
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/samsung/android/sdk/pen/Spen;->initialize(Landroid/content/Context;IIZ)V

    .line 277
    return-void
.end method

.method public initialize(Landroid/content/Context;IIZ)V
    .locals 26
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "maxCacheSize"    # I
    .param p3, "createMode"    # I
    .param p4, "isForce32BitMode"    # Z
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/sdk/SsdkUnsupportedException;
        }
    .end annotation

    .prologue
    .line 307
    const-string v23, "SpenSdk"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "SpenSdk version level = "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/Spen;->getVersionCode()I

    move-result v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    const-string v23, "SpenSdk"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "SpenSdk jar version = "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/Spen;->getVersionName()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    if-nez p1, :cond_0

    .line 311
    new-instance v23, Ljava/lang/IllegalArgumentException;

    const-string v24, "context is invalid."

    invoke-direct/range {v23 .. v24}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 313
    :cond_0
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/Spen;->mContext:Landroid/content/Context;

    .line 315
    sget-boolean v23, Lcom/samsung/android/sdk/pen/Spen;->IS_SPEN_PRELOAD_MODE:Z

    if-eqz v23, :cond_2

    .line 316
    const-string v23, "SpenSdk"

    const-string v24, "SpenSdk use preload library"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    const-string v23, "com.samsung.android.sdk.spenv10"

    sput-object v23, Lcom/samsung/android/sdk/pen/Spen;->mSpenPackageName:Ljava/lang/String;

    .line 350
    :cond_1
    :goto_0
    const-string v23, "SpenSdk"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "mSpenPackageName : "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v25, Lcom/samsung/android/sdk/pen/Spen;->mSpenPackageName:Ljava/lang/String;

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    const-string v23, "SpenSdk"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "IS_SPEN_PRELOAD_MODE : "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v25, Lcom/samsung/android/sdk/pen/Spen;->IS_SPEN_PRELOAD_MODE:Z

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    :try_start_0
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/Spen;->insertLog(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    .line 360
    .local v3, "appFilesDir":Ljava/io/File;
    if-nez v3, :cond_3

    .line 361
    const-string v23, "SpenSdk"

    const-string v24, "Cannot get the path of the directory holding application files."

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    new-instance v23, Ljava/lang/IllegalStateException;

    const-string v24, "Cannot get the path of the directory holding application files."

    invoke-direct/range {v23 .. v24}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 319
    .end local v3    # "appFilesDir":Ljava/io/File;
    :cond_2
    packed-switch p3, :pswitch_data_0

    .line 345
    const-string v23, "SpenSdk"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "SpenSdk use mode : "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ". Skip!!!"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    :goto_1
    return-void

    .line 321
    :pswitch_0
    const-string v23, "SpenSdk"

    const-string v24, "SpenSdk use static library"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    sget-boolean v23, Lcom/samsung/android/sdk/pen/Spen;->mIsInitialized:Z

    if-nez v23, :cond_1

    .line 323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/Spen;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v23

    sput-object v23, Lcom/samsung/android/sdk/pen/Spen;->mSpenPackageName:Ljava/lang/String;

    goto :goto_0

    .line 328
    :pswitch_1
    const-string v23, "SpenSdk"

    const-string v24, "SpenSdk use dynamic library"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    const-string v23, "com.samsung.android.sdk.spenv10"

    sput-object v23, Lcom/samsung/android/sdk/pen/Spen;->mSpenPackageName:Ljava/lang/String;

    .line 330
    sget v23, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v24, 0x15

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_1

    .line 332
    sget-object v23, Landroid/os/Build;->SUPPORTED_64_BIT_ABIS:[Ljava/lang/String;

    if-eqz v23, :cond_1

    sget-object v23, Landroid/os/Build;->SUPPORTED_64_BIT_ABIS:[Ljava/lang/String;

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    if-eqz v23, :cond_1

    .line 333
    const-string v23, "com.samsung.android.sdk.spen30_64"

    sput-object v23, Lcom/samsung/android/sdk/pen/Spen;->mSpenPackageName:Ljava/lang/String;

    .line 336
    if-eqz p4, :cond_1

    .line 337
    const-string v23, "com.samsung.android.sdk.spenv10"

    sput-object v23, Lcom/samsung/android/sdk/pen/Spen;->mSpenPackageName:Ljava/lang/String;

    .line 338
    const-string v23, "SpenSdk"

    const-string v24, "OS 64bit & isForce32BitMode = true"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 355
    :catch_0
    move-exception v5

    .line 356
    .local v5, "e":Ljava/lang/SecurityException;
    new-instance v23, Ljava/lang/SecurityException;

    const-string v24, "com.samsung.android.providers.context.permission.WRITE_USE_APP_FEATURE_SURVEY permission is required."

    invoke-direct/range {v23 .. v24}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 364
    .end local v5    # "e":Ljava/lang/SecurityException;
    .restart local v3    # "appFilesDir":Ljava/io/File;
    :cond_3
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v23

    if-nez v23, :cond_4

    .line 365
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    move-result v23

    if-nez v23, :cond_4

    .line 366
    const-string v23, "SpenSdk"

    const-string v24, "Cannot create application files directory"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    new-instance v23, Ljava/lang/IllegalStateException;

    const-string v24, "Cannot create application directory."

    invoke-direct/range {v23 .. v24}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 374
    :cond_4
    const-string/jumbo v23, "window"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/view/WindowManager;

    .line 377
    .local v22, "windowManager":Landroid/view/WindowManager;
    :try_start_1
    new-instance v18, Landroid/graphics/Point;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Point;-><init>()V

    .line 378
    .local v18, "point":Landroid/graphics/Point;
    invoke-interface/range {v22 .. v22}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 379
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    .line 380
    .local v21, "width":I
    move-object/from16 v0, v18

    iget v10, v0, Landroid/graphics/Point;->y:I
    :try_end_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_1 .. :try_end_1} :catch_1

    .line 407
    .end local v18    # "point":Landroid/graphics/Point;
    .local v10, "height":I
    :goto_2
    sget-boolean v23, Lcom/samsung/android/sdk/pen/Spen;->mIsInitialized:Z

    if-eqz v23, :cond_6

    .line 408
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v21

    move/from16 v2, p2

    invoke-static {v0, v1, v10, v2}, Lcom/samsung/android/sdk/pen/Spen;->SPenSdk_init2(Ljava/lang/String;III)Z

    move-result v23

    if-nez v23, :cond_5

    .line 409
    new-instance v23, Ljava/lang/IllegalStateException;

    const-string v24, "SDK Cache directory is not initialized."

    invoke-direct/range {v23 .. v24}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 381
    .end local v10    # "height":I
    .end local v21    # "width":I
    :catch_1
    move-exception v5

    .line 384
    .local v5, "e":Ljava/lang/NoSuchMethodError;
    :try_start_2
    invoke-interface/range {v22 .. v22}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    .line 385
    .local v4, "display":Landroid/view/Display;
    const-class v23, Landroid/view/Display;

    const-string v24, "getRawWidth"

    const/16 v25, 0x0

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v25, v0

    invoke-virtual/range {v23 .. v25}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v9

    .line 386
    .local v9, "getRawW":Ljava/lang/reflect/Method;
    const-class v23, Landroid/view/Display;

    const-string v24, "getRawHeight"

    const/16 v25, 0x0

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v25, v0

    invoke-virtual/range {v23 .. v25}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    .line 387
    .local v8, "getRawH":Ljava/lang/reflect/Method;
    const/16 v23, 0x0

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v9, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Integer;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v21

    .line 388
    .restart local v21    # "width":I
    const/16 v23, 0x0

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v8, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Integer;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result v10

    .restart local v10    # "height":I
    goto :goto_2

    .line 389
    .end local v4    # "display":Landroid/view/Display;
    .end local v8    # "getRawH":Ljava/lang/reflect/Method;
    .end local v9    # "getRawW":Ljava/lang/reflect/Method;
    .end local v10    # "height":I
    .end local v21    # "width":I
    :catch_2
    move-exception v6

    .line 392
    .local v6, "e1":Ljava/lang/Exception;
    :try_start_3
    new-instance v18, Landroid/graphics/Point;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Point;-><init>()V

    .line 393
    .restart local v18    # "point":Landroid/graphics/Point;
    invoke-interface/range {v22 .. v22}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 395
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    .line 396
    .restart local v21    # "width":I
    move-object/from16 v0, v18

    iget v10, v0, Landroid/graphics/Point;->y:I
    :try_end_3
    .catch Ljava/lang/NoSuchMethodError; {:try_start_3 .. :try_end_3} :catch_3

    .restart local v10    # "height":I
    goto/16 :goto_2

    .line 397
    .end local v10    # "height":I
    .end local v18    # "point":Landroid/graphics/Point;
    .end local v21    # "width":I
    :catch_3
    move-exception v7

    .line 399
    .local v7, "e2":Ljava/lang/NoSuchMethodError;
    invoke-interface/range {v22 .. v22}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    .line 401
    .restart local v4    # "display":Landroid/view/Display;
    invoke-virtual {v4}, Landroid/view/Display;->getWidth()I

    move-result v21

    .line 402
    .restart local v21    # "width":I
    invoke-virtual {v4}, Landroid/view/Display;->getHeight()I

    move-result v10

    .restart local v10    # "height":I
    goto/16 :goto_2

    .line 411
    .end local v4    # "display":Landroid/view/Display;
    .end local v5    # "e":Ljava/lang/NoSuchMethodError;
    .end local v6    # "e1":Ljava/lang/Exception;
    .end local v7    # "e2":Ljava/lang/NoSuchMethodError;
    :cond_5
    const-string v23, "SpenSdk"

    const-string v24, "initialize complete"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 415
    :cond_6
    sget-object v19, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 416
    .local v19, "strBrand":Ljava/lang/String;
    sget-object v20, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 417
    .local v20, "strManufacturer":Ljava/lang/String;
    if-eqz v19, :cond_7

    if-nez v20, :cond_8

    .line 418
    :cond_7
    new-instance v23, Lcom/samsung/android/sdk/SsdkUnsupportedException;

    const-string v24, "Vendor is not supported"

    const/16 v25, 0x0

    invoke-direct/range {v23 .. v25}, Lcom/samsung/android/sdk/SsdkUnsupportedException;-><init>(Ljava/lang/String;I)V

    throw v23

    .line 420
    :cond_8
    const-string v23, "Samsung"

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-eqz v23, :cond_9

    const-string v23, "Samsung"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v23

    if-eqz v23, :cond_9

    .line 421
    new-instance v23, Lcom/samsung/android/sdk/SsdkUnsupportedException;

    const-string v24, "Vendor is not supported"

    const/16 v25, 0x0

    invoke-direct/range {v23 .. v25}, Lcom/samsung/android/sdk/SsdkUnsupportedException;-><init>(Ljava/lang/String;I)V

    throw v23

    .line 423
    :cond_9
    sget v23, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v24, 0xe

    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_a

    .line 424
    new-instance v23, Lcom/samsung/android/sdk/SsdkUnsupportedException;

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "Device SDK version codes is "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v25, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 425
    const/16 v25, 0x1

    .line 424
    invoke-direct/range {v23 .. v25}, Lcom/samsung/android/sdk/SsdkUnsupportedException;-><init>(Ljava/lang/String;I)V

    throw v23

    .line 428
    :cond_a
    if-nez p3, :cond_13

    .line 430
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    .line 431
    .local v17, "pm":Landroid/content/pm/PackageManager;
    const/4 v15, 0x0

    .line 434
    .local v15, "packageInfo":Landroid/content/pm/PackageInfo;
    :try_start_4
    sget-object v23, Lcom/samsung/android/sdk/pen/Spen;->mSpenPackageName:Ljava/lang/String;

    const/16 v24, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v15

    .line 436
    const-string v23, "SpenSdk"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "SpenSdk apk version = "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v15, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    iget-object v0, v15, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "\\."

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 438
    .local v16, "packageVersion":[Ljava/lang/String;
    const-string v23, "3.0.259"

    const-string v24, "\\."

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 440
    .local v13, "javaVersion":[Ljava/lang/String;
    const/16 v23, 0x4

    move/from16 v0, v23

    new-array v14, v0, [I

    .line 441
    .local v14, "packageData":[I
    const/16 v23, 0x4

    move/from16 v0, v23

    new-array v12, v0, [I

    .line 442
    .local v12, "javaData":[I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_3
    const/16 v23, 0x4

    move/from16 v0, v23

    if-lt v11, v0, :cond_c

    .line 453
    const/4 v11, 0x0

    :goto_4
    const/16 v23, 0x4

    move/from16 v0, v23

    if-lt v11, v0, :cond_f

    .line 465
    const/16 v23, 0x0

    aget v23, v12, v23

    const/16 v24, 0x0

    aget v24, v14, v24

    move/from16 v0, v23

    move/from16 v1, v24

    if-gt v0, v1, :cond_b

    const/16 v23, 0x0

    aget v23, v12, v23

    const/16 v24, 0x0

    aget v24, v14, v24

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_12

    const/16 v23, 0x1

    aget v23, v12, v23

    const/16 v24, 0x1

    aget v24, v14, v24

    move/from16 v0, v23

    move/from16 v1, v24

    if-le v0, v1, :cond_12

    .line 466
    :cond_b
    const-string v23, "SpenSdk"

    const-string v24, "You SHOULD UPDATE SpenSdk apk file !!!!"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    new-instance v23, Lcom/samsung/android/sdk/SsdkUnsupportedException;

    const-string v24, "SpenSdk apk version is low."

    .line 468
    const/16 v25, 0x2

    .line 467
    invoke-direct/range {v23 .. v25}, Lcom/samsung/android/sdk/SsdkUnsupportedException;-><init>(Ljava/lang/String;I)V

    throw v23
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_4

    .line 472
    .end local v11    # "i":I
    .end local v12    # "javaData":[I
    .end local v13    # "javaVersion":[Ljava/lang/String;
    .end local v14    # "packageData":[I
    .end local v16    # "packageVersion":[Ljava/lang/String;
    :catch_4
    move-exception v6

    .line 473
    .local v6, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v23, "SpenSdk"

    const-string v24, "You SHOULD INSTALL SpenSdk apk file !!!!"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    new-instance v23, Lcom/samsung/android/sdk/SsdkUnsupportedException;

    const-string v24, "SpenSdk apk is not installed."

    .line 475
    const/16 v25, 0x2

    .line 474
    invoke-direct/range {v23 .. v25}, Lcom/samsung/android/sdk/SsdkUnsupportedException;-><init>(Ljava/lang/String;I)V

    throw v23

    .line 443
    .end local v6    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v11    # "i":I
    .restart local v12    # "javaData":[I
    .restart local v13    # "javaVersion":[Ljava/lang/String;
    .restart local v14    # "packageData":[I
    .restart local v16    # "packageVersion":[Ljava/lang/String;
    :cond_c
    :try_start_5
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    if-gt v0, v11, :cond_d

    .line 444
    const/16 v23, 0x0

    aput v23, v14, v11

    .line 442
    :goto_5
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 447
    :cond_d
    aget-object v23, v16, v11

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->isEmpty()Z

    move-result v23

    if-eqz v23, :cond_e

    .line 448
    const/16 v23, 0x0

    aput v23, v14, v11

    goto :goto_5

    .line 451
    :cond_e
    aget-object v23, v16, v11

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v23

    aput v23, v14, v11

    goto :goto_5

    .line 454
    :cond_f
    array-length v0, v13

    move/from16 v23, v0

    move/from16 v0, v23

    if-gt v0, v11, :cond_10

    .line 455
    const/16 v23, 0x0

    aput v23, v12, v11

    .line 453
    :goto_6
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_4

    .line 458
    :cond_10
    aget-object v23, v13, v11

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->isEmpty()Z

    move-result v23

    if-eqz v23, :cond_11

    .line 459
    const/16 v23, 0x0

    aput v23, v12, v11

    goto :goto_6

    .line 462
    :cond_11
    aget-object v23, v13, v11

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v23

    aput v23, v12, v11

    goto :goto_6

    .line 470
    :cond_12
    const-string v23, "SpenSdk"

    const-string v24, "Server UPDATE Check"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5 .. :try_end_5} :catch_4

    .line 479
    .end local v11    # "i":I
    .end local v12    # "javaData":[I
    .end local v13    # "javaVersion":[Ljava/lang/String;
    .end local v14    # "packageData":[I
    .end local v15    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v16    # "packageVersion":[Ljava/lang/String;
    .end local v17    # "pm":Landroid/content/pm/PackageManager;
    :cond_13
    const-string v23, "SPenBase"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    const-string v23, "SPenPluginFW"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    const-string v23, "SPenInit"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    .line 480
    const-string v23, "SPenModel"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    const-string v23, "SPenSkia"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    const-string v23, "SPenEngine"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    .line 481
    const-string v23, "SPenBrush"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    const-string v23, "SPenChineseBrush"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    const-string v23, "SPenInkPen"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    .line 482
    const-string v23, "SPenMarker"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    const-string v23, "SPenPencil"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    const-string v23, "SPenNativePen"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    .line 483
    const-string v23, "SPenMontblancFountainPen"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    const-string v23, "SPenMontblancCalligraphyPen"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    .line 484
    const-string v23, "SPenMagicPen"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    const-string v23, "SPenObliquePen"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    const-string v23, "SPenFountainPen"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v23

    if-nez v23, :cond_15

    .line 485
    :cond_14
    new-instance v23, Ljava/lang/IllegalStateException;

    invoke-direct/range {v23 .. v23}, Ljava/lang/IllegalStateException;-><init>()V

    throw v23

    .line 488
    :cond_15
    const-string v23, "SpenSdk"

    const-string v24, "SpenSdk Libraries are loaded."

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v21

    move/from16 v2, p2

    invoke-static {v0, v1, v10, v2}, Lcom/samsung/android/sdk/pen/Spen;->SPenSdk_init2(Ljava/lang/String;III)Z

    move-result v23

    if-nez v23, :cond_16

    .line 491
    new-instance v23, Ljava/lang/IllegalStateException;

    const-string v24, "SDK Cache directory is not initialized."

    invoke-direct/range {v23 .. v24}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 494
    :cond_16
    const/16 v23, 0x1

    sput-boolean v23, Lcom/samsung/android/sdk/pen/Spen;->mIsInitialized:Z

    .line 495
    const-string v23, "SpenSdk"

    const-string v24, "initialize complete"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 319
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public isFeatureEnabled(I)Z
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 198
    packed-switch p1, :pswitch_data_0

    .line 206
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 200
    :pswitch_0
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/class/sec/sec_epen"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 201
    .local v0, "path":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 202
    const/4 v1, 0x1

    .line 208
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 198
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method loadLibrary(Ljava/lang/String;)Z
    .locals 8
    .param p1, "libraryName"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SdCardPath"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 144
    sget-boolean v6, Lcom/samsung/android/sdk/pen/Spen;->IS_SPEN_PRELOAD_MODE:Z

    if-eqz v6, :cond_0

    .line 146
    :try_start_0
    invoke-static {p1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :goto_0
    return v4

    .line 147
    :catch_0
    move-exception v1

    .local v1, "error":Ljava/lang/Exception;
    move v4, v5

    .line 148
    goto :goto_0

    .line 152
    .end local v1    # "error":Ljava/lang/Exception;
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "/data/data/"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Lcom/samsung/android/sdk/pen/Spen;->mSpenPackageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/lib/lib"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".so"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 153
    .local v2, "latestLib":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 154
    .local v3, "libFilePath":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 156
    :try_start_1
    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/System;->load(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 157
    :catch_1
    move-exception v0

    .line 158
    .local v0, "e":Ljava/lang/Throwable;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    move v4, v5

    .line 159
    goto :goto_0

    .end local v0    # "e":Ljava/lang/Throwable;
    :cond_1
    move v4, v5

    .line 163
    goto :goto_0
.end method
