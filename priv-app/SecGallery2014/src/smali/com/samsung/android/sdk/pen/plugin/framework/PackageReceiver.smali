.class Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PackageReceiver.java"


# static fields
.field static final META_DATA_KEY_SPEN_PLUGIN_INFO:Ljava/lang/String; = "SPEN_PLUGIN_INFO"

.field private static mListener:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;->mListener:Ljava/util/List;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 23
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v6

    .line 24
    .local v6, "packageName":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 25
    .local v0, "action":Ljava/lang/String;
    const-string v9, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 26
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 27
    .local v8, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x0

    .line 29
    .local v1, "ai":Landroid/content/pm/ApplicationInfo;
    const/16 v9, 0x80

    :try_start_0
    invoke-virtual {v8, v6, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 36
    iget-object v2, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 37
    .local v2, "bundle":Landroid/os/Bundle;
    if-nez v2, :cond_1

    .line 67
    .end local v1    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v8    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    :goto_0
    return-void

    .line 30
    .restart local v1    # "ai":Landroid/content/pm/ApplicationInfo;
    .restart local v8    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v3

    .line 32
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 40
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2    # "bundle":Landroid/os/Bundle;
    :cond_1
    const-string v9, "SPEN_PLUGIN_INFO"

    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 41
    .local v7, "pluginInfoData":Ljava/lang/String;
    if-eqz v7, :cond_0

    .line 46
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    move-result-object v9

    invoke-virtual {v9, v6}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getPluginInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v4

    .line 47
    .local v4, "info":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    if-eqz v4, :cond_0

    .line 50
    sget-object v9, Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;->mListener:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;

    .line 51
    .local v5, "listener":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;
    if-eqz v5, :cond_2

    .line 52
    iget-object v10, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    invoke-virtual {v5, v10, v6}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;->onInstalled(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 55
    .end local v1    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v4    # "info":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    .end local v5    # "listener":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;
    .end local v7    # "pluginInfoData":Ljava/lang/String;
    .end local v8    # "pm":Landroid/content/pm/PackageManager;
    :cond_3
    const-string v9, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 56
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    move-result-object v9

    invoke-virtual {v9, v6}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getPluginInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v4

    .line 57
    .restart local v4    # "info":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    if-eqz v4, :cond_0

    .line 58
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    move-result-object v9

    invoke-virtual {v9, v6}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->deleteAPKPluginInfo(Ljava/lang/String;)V

    .line 60
    sget-object v9, Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;->mListener:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_4
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;

    .line 61
    .restart local v5    # "listener":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;
    if-eqz v5, :cond_4

    .line 62
    iget-object v10, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    invoke-virtual {v5, v10, v6}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;->onUninstalled(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public setNotifyListener(Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;

    .prologue
    .line 70
    sget-object v0, Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;->mListener:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    return-void
.end method
