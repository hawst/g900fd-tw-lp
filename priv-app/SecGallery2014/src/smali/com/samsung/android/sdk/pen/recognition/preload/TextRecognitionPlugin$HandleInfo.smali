.class Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$HandleInfo;
.super Ljava/lang/Object;
.source "TextRecognitionPlugin.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HandleInfo"
.end annotation


# instance fields
.field private mInput:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end field

.field private mOutput:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "input":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    .local p2, "output":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    const/4 v0, 0x0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$HandleInfo;->mInput:Ljava/util/List;

    .line 97
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$HandleInfo;->mOutput:Ljava/util/List;

    .line 100
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$HandleInfo;->mInput:Ljava/util/List;

    .line 101
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$HandleInfo;->mOutput:Ljava/util/List;

    .line 102
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$HandleInfo;)Ljava/util/List;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$HandleInfo;->mInput:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$HandleInfo;)Ljava/util/List;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$HandleInfo;->mOutput:Ljava/util/List;

    return-object v0
.end method
