.class Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;
.super Ljava/lang/Object;
.source "SpenNotePad.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/engine/SpenNotePad;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    .line 219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdate(Landroid/view/MotionEvent;)V
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 222
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->getBeautifyPen()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->getMagicPen()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->getMarkerPen()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 223
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->refresh()V

    .line 224
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 225
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setBeautifyPen(Z)V

    .line 226
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setMagicPen(Z)V

    .line 227
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setMarkerPen(Z)V

    .line 257
    :cond_1
    :goto_0
    return-void

    .line 229
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->absoluteCoordinate(Landroid/view/MotionEvent;Z)Landroid/view/MotionEvent;
    invoke-static {v3, p1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;Landroid/view/MotionEvent;Z)Landroid/view/MotionEvent;

    move-result-object v2

    .line 230
    .local v2, "ev":Landroid/view/MotionEvent;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 231
    .local v0, "absoluteX":F
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onUpdate(Landroid/view/MotionEvent;)V

    .line 232
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 234
    .local v1, "action":I
    if-eq v1, v6, :cond_3

    const/4 v3, 0x3

    if-ne v1, v3, :cond_4

    .line 235
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-static {v3, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;Z)V

    .line 238
    :cond_4
    if-nez v1, :cond_5

    .line 239
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v5, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->setMovingEnabled(ZF)V

    .line 240
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->removeMessages(I)V

    .line 242
    :cond_5
    if-ne v1, v6, :cond_1

    .line 243
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget-boolean v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isArabic:Z

    if-eqz v3, :cond_7

    .line 244
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v4

    cmpl-float v3, v3, v0

    if-lez v3, :cond_6

    .line 245
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    move-result-object v3

    invoke-virtual {v3, v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->setMovingEnabled(ZF)V

    .line 252
    :cond_6
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->isMovingEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 253
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    move-result-object v3

    const-wide/16 v4, 0x258

    invoke-virtual {v3, v6, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 248
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v4

    cmpg-float v3, v3, v0

    if-gez v3, :cond_6

    .line 249
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    move-result-object v3

    invoke-virtual {v3, v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->setMovingEnabled(ZF)V

    goto :goto_1
.end method
