.class Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;
.super Ljava/lang/Object;
.source "SpenStrokeFrame.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->startCamera()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    .line 697
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 701
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    .line 830
    const/4 v7, 0x0

    :goto_0
    return v7

    .line 703
    :pswitch_0
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchCheckPath:Landroid/graphics/Path;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$31(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Path;->reset()V

    .line 704
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchCheckPath:Landroid/graphics/Path;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$31(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 705
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPrevTouchPoint:Landroid/graphics/PointF;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/PointF;

    move-result-object v7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/PointF;->set(FF)V

    .line 706
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$33(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    .line 707
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    invoke-static {v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$34(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;F)V

    .line 708
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomCur:I
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$35(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v9

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$36(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    invoke-static {v7, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$37(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    .line 709
    const/4 v7, 0x1

    goto :goto_0

    .line 712
    :pswitch_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 713
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->cancelAutoFocus()V

    .line 715
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 716
    .local v3, "curX":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 730
    .local v4, "curY":F
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchCheckPath:Landroid/graphics/Path;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$31(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v7

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 731
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPrevTouchPoint:Landroid/graphics/PointF;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/PointF;

    move-result-object v7

    invoke-virtual {v7, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 733
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomY:F
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$38(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v7

    sub-float/2addr v7, v4

    float-to-int v6, v7

    .line 734
    .local v6, "zoom":I
    div-int/lit8 v6, v6, 0x5

    .line 736
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomLast:I
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$39(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v7

    rsub-int/lit8 v7, v7, 0x64

    if-ge v7, v6, :cond_1

    .line 737
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomLast:I
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$39(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v7

    rsub-int/lit8 v6, v7, 0x64

    .line 740
    :cond_1
    const/16 v7, 0xa

    if-le v6, v7, :cond_2

    .line 741
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/4 v8, 0x2

    invoke-static {v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$33(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    .line 744
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomLast:I
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$39(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v8

    add-int/2addr v8, v6

    invoke-static {v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$40(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    .line 746
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomCur:I
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$35(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v7

    if-gez v7, :cond_3

    .line 747
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$40(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    .line 748
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$36(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    .line 749
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v7, v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$34(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;F)V

    .line 750
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 752
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomCur:I
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$35(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v7

    const/16 v8, 0x64

    if-le v7, v8, :cond_4

    .line 753
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/16 v8, 0x64

    invoke-static {v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$40(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    .line 754
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/16 v8, 0x64

    invoke-static {v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$36(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    .line 755
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v7, v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$34(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;F)V

    .line 756
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 759
    :cond_4
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomPrev:I
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$41(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomCur:I
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$35(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v8

    if-ne v7, v8, :cond_5

    .line 760
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 763
    :cond_5
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomCur:I
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$35(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v8

    invoke-static {v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$37(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    .line 764
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    move-result-object v7

    if-eqz v7, :cond_6

    .line 765
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomCur:I
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$35(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->setZoom(I)V

    .line 767
    :cond_6
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 770
    .end local v3    # "curX":F
    .end local v4    # "curY":F
    .end local v6    # "zoom":I
    :pswitch_2
    new-instance v5, Landroid/graphics/PathMeasure;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchCheckPath:Landroid/graphics/Path;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$31(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v7

    const/4 v8, 0x0

    invoke-direct {v5, v7, v8}, Landroid/graphics/PathMeasure;-><init>(Landroid/graphics/Path;Z)V

    .line 771
    .local v5, "pm":Landroid/graphics/PathMeasure;
    const/4 v2, 0x0

    .line 772
    .local v2, "checkDistance":F
    const/4 v7, 0x0

    invoke-virtual {p2, v7}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_8

    .line 773
    const/high16 v2, 0x42480000    # 50.0f

    .line 777
    :goto_1
    invoke-virtual {v5}, Landroid/graphics/PathMeasure;->getLength()F

    move-result v7

    cmpg-float v7, v7, v2

    if-gez v7, :cond_9

    .line 778
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    move-result-object v7

    if-eqz v7, :cond_7

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mReadyCameraPreview:Z
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 779
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->takePicture()V

    .line 780
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v7

    const-string v8, "CAMERA_TYPE"

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->getCameraType()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setExtraDataInt(Ljava/lang/String;I)V

    .line 781
    const/4 v7, 0x0

    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$42(Z)V

    .line 783
    :cond_7
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$33(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    .line 784
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 775
    :cond_8
    const/high16 v2, 0x41f00000    # 30.0f

    goto :goto_1

    .line 787
    :cond_9
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomMode:I
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$43(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_e

    .line 790
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v8

    div-float/2addr v7, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$26(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v8

    cmpl-float v7, v7, v8

    if-lez v7, :cond_f

    .line 792
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    div-float/2addr v7, v8

    const/high16 v8, -0x3b060000    # -2000.0f

    mul-float/2addr v7, v8

    float-to-int v0, v7

    .line 793
    .local v0, "cX":I
    const/16 v7, -0x3ca

    if-ge v0, v7, :cond_a

    .line 794
    const/16 v0, -0x3ca

    .line 796
    :cond_a
    const/16 v7, 0x3ca

    if-le v0, v7, :cond_b

    .line 797
    const/16 v0, 0x3ca

    .line 799
    :cond_b
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$26(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v9

    mul-float/2addr v8, v9

    div-float/2addr v7, v8

    const/high16 v8, 0x44fa0000    # 2000.0f

    mul-float/2addr v7, v8

    float-to-int v1, v7

    .line 800
    .local v1, "cY":I
    const/16 v7, -0x3ca

    if-ge v1, v7, :cond_c

    .line 801
    const/16 v1, -0x3ca

    .line 803
    :cond_c
    const/16 v7, 0x3ca

    if-le v1, v7, :cond_d

    .line 804
    const/16 v1, 0x3ca

    .line 823
    :cond_d
    :goto_2
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    move-result-object v7

    if-eqz v7, :cond_e

    .line 824
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    move-result-object v7

    new-instance v8, Landroid/graphics/Rect;

    add-int/lit8 v9, v1, -0x1e

    add-int/lit8 v10, v0, -0x1e

    add-int/lit8 v11, v1, 0x1e

    add-int/lit8 v12, v0, 0x1e

    invoke-direct {v8, v9, v10, v11, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->setFocus(Landroid/graphics/Rect;)V

    .line 827
    .end local v0    # "cX":I
    .end local v1    # "cY":I
    :cond_e
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$33(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    .line 828
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 808
    :cond_f
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$26(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v9

    div-float/2addr v8, v9

    div-float/2addr v7, v8

    const/high16 v8, -0x3b060000    # -2000.0f

    mul-float/2addr v7, v8

    float-to-int v0, v7

    .line 809
    .restart local v0    # "cX":I
    const/16 v7, -0x3ca

    if-ge v0, v7, :cond_10

    .line 810
    const/16 v0, -0x3ca

    .line 812
    :cond_10
    const/16 v7, 0x3ca

    if-le v0, v7, :cond_11

    .line 813
    const/16 v0, 0x3ca

    .line 815
    :cond_11
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v8

    div-float/2addr v7, v8

    const/high16 v8, 0x44fa0000    # 2000.0f

    mul-float/2addr v7, v8

    float-to-int v1, v7

    .line 816
    .restart local v1    # "cY":I
    const/16 v7, -0x3ca

    if-ge v1, v7, :cond_12

    .line 817
    const/16 v1, -0x3ca

    .line 819
    :cond_12
    const/16 v7, 0x3ca

    if-le v1, v7, :cond_d

    .line 820
    const/16 v1, 0x3ca

    goto/16 :goto_2

    .line 701
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
