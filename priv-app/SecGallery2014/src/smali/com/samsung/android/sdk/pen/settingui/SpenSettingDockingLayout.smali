.class public Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;
.super Landroid/widget/LinearLayout;
.source "SpenSettingDockingLayout.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;
    }
.end annotation


# static fields
.field private static mSdkVersion:I


# instance fields
.field private mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private mEnable:Z

.field private mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

.field private mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

.field private mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

.field private mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

.field private mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

.field onEraserClearListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$EventListener;

.field private onPenPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PopupListener;

.field private onRemoverPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;

.field private onSelectPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;

.field private onTextPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSdkVersion:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/lang/String;Landroid/widget/RelativeLayout;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p4, "imagePath"    # Ljava/lang/String;
    .param p5, "canvasLayout"    # Landroid/widget/RelativeLayout;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/widget/RelativeLayout;",
            ")V"
        }
    .end annotation

    .prologue
    .line 147
    .local p2, "resourceIds":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .local p3, "fontName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 539
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onPenPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PopupListener;

    .line 558
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onTextPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;

    .line 578
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onRemoverPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;

    .line 598
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onSelectPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;

    .line 618
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onEraserClearListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$EventListener;

    .line 149
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    .line 150
    .local v8, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v7, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 151
    .local v7, "deviceWidth":I
    iget v6, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 152
    .local v6, "deviceHeight":I
    const/16 v0, 0x640

    if-lt v7, v0, :cond_0

    const/16 v0, 0x640

    if-ge v6, v0, :cond_1

    .line 153
    :cond_0
    const-string v0, "It does not support the current resolution."

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    .line 200
    :goto_0
    return-void

    .line 158
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    .line 159
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, p1, p4, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 164
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, p1, p4, p5, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    .line 165
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v1, p1

    move-object v2, p4

    move-object v3, p3

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/HashMap;Landroid/widget/RelativeLayout;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    .line 166
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, p1, p4, p5, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    .line 167
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, p1, p4, p5, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    .line 169
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_2

    .line 170
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "vienna_popup_bg"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 174
    :goto_1
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 175
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setOrientation(I)V

    .line 177
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->addView(Landroid/view/View;)V

    .line 178
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->addView(Landroid/view/View;)V

    .line 179
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->addView(Landroid/view/View;)V

    .line 180
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->addView(Landroid/view/View;)V

    .line 182
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setVisibility(I)V

    .line 183
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setVisibility(I)V

    .line 184
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setVisibility(I)V

    .line 185
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setVisibility(I)V

    .line 187
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    .line 188
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    .line 189
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    .line 190
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    .line 192
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onPenPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PopupListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopupListenr(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PopupListener;)V

    .line 193
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onTextPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopupListenr(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;)V

    .line 194
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onRemoverPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopupListenr(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;)V

    .line 195
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onSelectPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopupListenr(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;)V

    .line 197
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->onEraserClearListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$EventListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setEraserListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$EventListener;)V

    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    goto/16 :goto_0

    .line 172
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "vienna_popup_bg"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setTopBottomToZeroMarginItem()V

    return-void
.end method

.method private setTopBottomToZeroMarginItem()V
    .locals 4

    .prologue
    .line 79
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 80
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V

    .line 104
    const-wide/16 v2, 0x64

    .line 80
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 105
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 209
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_1

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    if-eqz v0, :cond_2

    .line 214
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->close()V

    .line 215
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    .line 217
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    if-eqz v0, :cond_3

    .line 218
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->close()V

    .line 219
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    .line 221
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    if-eqz v0, :cond_4

    .line 222
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->close()V

    .line 223
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    .line 225
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->close()V

    .line 227
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    .line 62
    :goto_0
    return-void

    .line 60
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setTopBottomToZeroMarginItem()V

    .line 61
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    .line 48
    :goto_0
    return-void

    .line 47
    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    goto :goto_0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    .line 76
    :goto_0
    return-void

    .line 74
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setTopBottomToZeroMarginItem()V

    .line 75
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onVisibilityChanged(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public setEraserInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 1
    .param p1, "settingInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    .line 510
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    .line 515
    :goto_0
    return-void

    .line 514
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    goto :goto_0
.end method

.method public setListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    .prologue
    .line 362
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_1

    .line 369
    :cond_0
    :goto_0
    return-void

    .line 366
    :cond_1
    if-eqz p1, :cond_0

    .line 367
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    goto :goto_0
.end method

.method public setPenInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 1
    .param p1, "settingInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .prologue
    .line 386
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    .line 391
    :goto_0
    return-void

    .line 390
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto :goto_0
.end method

.method public setPenInfoList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 466
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    .line 471
    :goto_0
    return-void

    .line 470
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPenInfoList(Ljava/util/List;)V

    goto :goto_0
.end method

.method public setPopupMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 271
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_1

    .line 323
    :cond_0
    :goto_0
    return-void

    .line 274
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 312
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    .line 313
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    .line 314
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    .line 315
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    .line 316
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;->onPopup(I)V

    goto :goto_0

    .line 276
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    .line 277
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    .line 278
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    .line 279
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    .line 280
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;->onPopup(I)V

    goto :goto_0

    .line 285
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    .line 286
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    .line 287
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    .line 288
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    .line 289
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;->onPopup(I)V

    goto :goto_0

    .line 294
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    .line 295
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    .line 296
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    .line 297
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    .line 298
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;->onPopup(I)V

    goto :goto_0

    .line 303
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    .line 304
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    .line 305
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    .line 306
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    .line 307
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;->onPopup(I)V

    goto/16 :goto_0

    .line 274
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setPosition(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 344
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v1, :cond_0

    .line 352
    :goto_0
    return-void

    .line 348
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 349
    .local v0, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 350
    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 351
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public setSelectionInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V
    .locals 1
    .param p1, "settingInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    .prologue
    .line 532
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    .line 537
    :goto_0
    return-void

    .line 536
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V

    goto :goto_0
.end method

.method public setSpenView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V
    .locals 1
    .param p1, "spenView"    # Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .prologue
    .line 246
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    .line 254
    :goto_0
    return-void

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    .line 251
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    .line 252
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    .line 253
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    goto :goto_0
.end method

.method public setTextInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 1
    .param p1, "settingInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    .line 488
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    .line 493
    :goto_0
    return-void

    .line 492
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    goto :goto_0
.end method
