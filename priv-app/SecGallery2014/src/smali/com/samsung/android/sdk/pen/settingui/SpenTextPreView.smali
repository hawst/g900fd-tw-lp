.class Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;
.super Landroid/view/View;
.source "SpenTextPreView.java"


# static fields
.field private static final TAB_A_CANVAS_WIDTH:I = 0x300

.field private static final TEXT_PREVIEW_LEFT_MARGIN:I = 0x14

.field protected static final TEXT_PREVIEW_WIDTH:I = 0xde

.field private static final VIENNA_CANVAS_WIDTH:I = 0x640


# instance fields
.field private isCheckUnderLine:Z

.field private mBold:I

.field private mColor:I

.field private mCustomString:Ljava/lang/String;

.field private mOnePoint:F

.field private final mPaint:Landroid/graphics/Paint;

.field private mPreviewOffset:F

.field private final mTextRect:Landroid/graphics/Rect;

.field private mTextSize:F

.field private mTextSkewValue:F

.field private mTypeFace:Landroid/graphics/Typeface;

.field private mWidth:F

.field private maxFontSize1:F

.field private maxFontSize2:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 51
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 21
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mColor:I

    .line 22
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSize:F

    .line 23
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mBold:I

    .line 24
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSkewValue:F

    .line 25
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->isCheckUnderLine:Z

    .line 26
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTypeFace:Landroid/graphics/Typeface;

    .line 27
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    .line 28
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    .line 32
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mOnePoint:F

    .line 33
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->maxFontSize1:F

    .line 34
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->maxFontSize2:F

    .line 36
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPreviewOffset:F

    .line 37
    const/high16 v0, 0x44870000    # 1080.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mWidth:F

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 6
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "mCanvasWidth"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 58
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 21
    const/high16 v2, -0x1000000

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mColor:I

    .line 22
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSize:F

    .line 23
    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mBold:I

    .line 24
    const/high16 v2, -0x80000000

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSkewValue:F

    .line 25
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->isCheckUnderLine:Z

    .line 26
    sget-object v2, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTypeFace:Landroid/graphics/Typeface;

    .line 27
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    .line 28
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    .line 32
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mOnePoint:F

    .line 33
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->maxFontSize1:F

    .line 34
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->maxFontSize2:F

    .line 36
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPreviewOffset:F

    .line 37
    const/high16 v2, 0x44870000    # 1080.0f

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mWidth:F

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 61
    .local v1, "localDisplayMetrics":Landroid/util/DisplayMetrics;
    iget v2, v1, Landroid/util/DisplayMetrics;->density:F

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mOnePoint:F

    .line 62
    int-to-double v2, p2

    const-wide v4, 0x4076800000000000L    # 360.0

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 64
    .local v0, "fontPointPixel":F
    const-string v2, "ABCD abcd"

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->getMaxFontSize(Ljava/lang/String;F)F

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->maxFontSize1:F

    .line 65
    const-string v2, "ABC abc"

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->getMaxFontSize(Ljava/lang/String;F)F

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->maxFontSize2:F

    .line 67
    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v2, v3, :cond_0

    .line 68
    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v2, v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mWidth:F

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_0
    iget v2, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v2, v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mWidth:F

    goto :goto_0
.end method

.method private getMaxFontSize(Ljava/lang/String;F)F
    .locals 11
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "fontPointPixel"    # F

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 75
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontList()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 76
    .local v2, "mFontList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, ""

    .line 77
    .local v3, "maxFontName":Ljava/lang/String;
    const/4 v5, 0x0

    .line 78
    .local v5, "maxLength":F
    const/4 v4, 0x0

    .line 80
    .local v4, "maxFontSize":F
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->getFontSizeList()Ljava/util/ArrayList;

    move-result-object v0

    .line 82
    .local v0, "fontSizeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v1, v6, :cond_0

    .line 94
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v1, v6, :cond_2

    .line 107
    :goto_2
    return v4

    .line 83
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    const/high16 v7, 0x41f00000    # 30.0f

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 85
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(I)Landroid/graphics/Typeface;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 86
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v6, p1, v9, v7, v8}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 87
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v6, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 88
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    cmpl-float v6, v6, v5

    if-lez v6, :cond_1

    .line 89
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v5, v6

    .line 90
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "maxFontName":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 82
    .restart local v3    # "maxFontName":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 96
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, p2

    invoke-virtual {v7, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 98
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-static {v3}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 99
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v6, p1, v9, v7, v8}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 100
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v6, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 101
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x43540000    # 212.0f

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mOnePoint:F

    mul-float/2addr v7, v8

    cmpl-float v6, v6, v7

    if-ltz v6, :cond_3

    .line 102
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-float v6, v6

    mul-float v4, p2, v6

    .line 103
    goto/16 :goto_2

    .line 94
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1
.end method


# virtual methods
.method protected getFontSizeList()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 114
    .local v0, "fontList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/16 v1, 0x8

    .local v1, "txtSize":I
    :goto_0
    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 117
    const/16 v1, 0x16

    :goto_1
    const/16 v2, 0x21

    if-lt v1, v2, :cond_1

    .line 120
    const/16 v1, 0x24

    :goto_2
    const/16 v2, 0x41

    if-lt v1, v2, :cond_2

    .line 124
    return-object v0

    .line 115
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 118
    :cond_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    add-int/lit8 v1, v1, 0x2

    goto :goto_1

    .line 121
    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    add-int/lit8 v1, v1, 0x4

    goto :goto_2
.end method

.method public getPreviewTextColor()I
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mColor:I

    return v0
.end method

.method public getTextStyle()C
    .locals 3

    .prologue
    .line 301
    const/4 v0, 0x0

    .line 302
    .local v0, "nTextStyle":C
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mBold:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_0

    .line 303
    const/4 v1, 0x1

    int-to-char v0, v1

    .line 305
    :cond_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSkewValue:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1

    .line 306
    or-int/lit8 v1, v0, 0x2

    int-to-char v0, v1

    .line 308
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->isCheckUnderLine:Z

    if-eqz v1, :cond_2

    .line 309
    or-int/lit8 v1, v0, 0x4

    int-to-char v0, v1

    .line 311
    :cond_2
    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 138
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 140
    const-string v2, "ABCD abcd"

    .line 142
    .local v2, "str":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mColor:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 143
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 144
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSize:F

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 146
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTypeFace:Landroid/graphics/Typeface;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 147
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mBold:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setFlags(I)V

    .line 148
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSkewValue:F

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextSkewX(F)V

    .line 149
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->isCheckUnderLine:Z

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setUnderlineText(Z)V

    .line 150
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v5, v2, v8, v6, v7}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 153
    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSize:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->maxFontSize1:F

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_0

    .line 154
    const-string v2, "ABC abc"

    .line 155
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v5, v2, v8, v6, v7}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 158
    :cond_0
    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSize:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->maxFontSize2:F

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_1

    .line 159
    const-string v2, "AB ab"

    .line 160
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v5, v2, v8, v6, v7}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 163
    :cond_1
    iput v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPreviewOffset:F

    .line 164
    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSize:F

    const/high16 v6, 0x43430000    # 195.0f

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_2

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mWidth:F

    const/high16 v6, 0x44c80000    # 1600.0f

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_3

    .line 165
    :cond_2
    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSize:F

    const/high16 v6, 0x42b40000    # 90.0f

    cmpl-float v5, v5, v6

    if-lez v5, :cond_4

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mWidth:F

    const/high16 v6, 0x44400000    # 768.0f

    cmpl-float v5, v5, v6

    if-nez v5, :cond_4

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mOnePoint:F

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v5, v5, v6

    if-nez v5, :cond_4

    .line 166
    :cond_3
    const-string v2, "Aa"

    .line 167
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v5, v2, v8, v6, v7}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 168
    iget-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->isCheckUnderLine:Z

    if-eqz v5, :cond_5

    .line 169
    const/high16 v5, 0x41200000    # 10.0f

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPreviewOffset:F

    .line 175
    :cond_4
    :goto_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 178
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 179
    .local v0, "height":I
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mCustomString:Ljava/lang/String;

    if-eqz v5, :cond_8

    .line 180
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mCustomString:Ljava/lang/String;

    .line 182
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->getWidth()I

    move-result v6

    add-int/lit8 v6, v6, -0x14

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_7

    .line 183
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->getWidth()I

    move-result v5

    add-int/lit8 v5, v5, -0x14

    int-to-float v5, v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    const-string/jumbo v7, "\u2026"

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v6

    sub-float v4, v5, v6

    .line 184
    .local v4, "width":F
    invoke-virtual {v2, v8, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 185
    .local v3, "tempString":Ljava/lang/String;
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    cmpg-float v5, v5, v4

    if-lez v5, :cond_6

    .line 188
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v2, v8, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v6, "\u2026"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 190
    const/16 v1, 0x14

    .line 200
    .end local v3    # "tempString":Ljava/lang/String;
    .end local v4    # "width":F
    .local v1, "leftMargin":I
    :goto_2
    int-to-float v5, v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    div-int/lit8 v7, v0, 0x2

    add-int/2addr v6, v7

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPreviewOffset:F

    sub-float/2addr v6, v7

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v5, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 202
    return-void

    .line 171
    .end local v0    # "height":I
    .end local v1    # "leftMargin":I
    :cond_5
    iput v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPreviewOffset:F

    goto :goto_0

    .line 186
    .restart local v0    # "height":I
    .restart local v3    # "tempString":Ljava/lang/String;
    .restart local v4    # "width":F
    :cond_6
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v2, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 192
    .end local v3    # "tempString":Ljava/lang/String;
    .end local v4    # "width":F
    :cond_7
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v5, v2, v8, v6, v7}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 193
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int v1, v5, v6

    .line 196
    .restart local v1    # "leftMargin":I
    goto :goto_2

    .line 197
    .end local v1    # "leftMargin":I
    :cond_8
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int v1, v5, v6

    .restart local v1    # "leftMargin":I
    goto :goto_2
.end method

.method public setPreviewBold(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 276
    if-eqz p1, :cond_0

    .line 277
    const/16 v0, 0x20

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mBold:I

    .line 281
    :goto_0
    return-void

    .line 279
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mBold:I

    goto :goto_0
.end method

.method public setPreviewTextColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    const/4 v0, 0x0

    .line 225
    if-nez p1, :cond_0

    .line 226
    invoke-static {v0, v0, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result p1

    .line 228
    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mColor:I

    .line 229
    return-void
.end method

.method public setPreviewTextSize(F)V
    .locals 0
    .param p1, "textSize"    # F

    .prologue
    .line 250
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSize:F

    .line 251
    return-void
.end method

.method public setPreviewTextSkewX(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 261
    if-eqz p1, :cond_0

    .line 262
    const v0, -0x41666666    # -0.3f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSkewValue:F

    .line 266
    :goto_0
    return-void

    .line 264
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTextSkewValue:F

    goto :goto_0
.end method

.method public setPreviewTypeface(Landroid/graphics/Typeface;)V
    .locals 0
    .param p1, "typeface"    # Landroid/graphics/Typeface;

    .prologue
    .line 291
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mTypeFace:Landroid/graphics/Typeface;

    .line 292
    return-void
.end method

.method public setPreviewUnderLine(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 239
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->isCheckUnderLine:Z

    .line 240
    return-void
.end method

.method public setTextPreview(Ljava/lang/String;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->mCustomString:Ljava/lang/String;

    .line 206
    return-void
.end method
