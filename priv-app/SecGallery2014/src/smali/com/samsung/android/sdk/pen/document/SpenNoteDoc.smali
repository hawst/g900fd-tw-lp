.class public Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
.super Ljava/lang/Object;
.source "SpenNoteDoc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/document/SpenNoteDoc$AuthorInfo;
    }
.end annotation


# static fields
.field public static final MODE_READ_ONLY:I = 0x0

.field public static final MODE_WRITABLE:I = 0x1

.field private static final NATIVE_COMMAND_APPEND_PAGES:I = 0x1

.field private static final NATIVE_COMMAND_DUMMY:I = 0x0

.field public static final ORIENTATION_LANDSCAPE:I = 0x1

.field public static final ORIENTATION_PORTRAIT:I = 0x0

.field private static final SCHEME_TEMPLATE_NAME:Ljava/lang/String; = "template_name://"


# instance fields
.field private final is32Bit:Z

.field private mContext:Landroid/content/Context;

.field private mHandle:I

.field private mHandle2:J


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v0

    const/16 v2, 0x20

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 102
    iput v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 103
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 107
    return-void

    .line 100
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v1

    const/16 v4, 0x20

    if-ne v1, v4, :cond_1

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 101
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 102
    iput v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 103
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 136
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 138
    if-le p2, p3, :cond_2

    .line 139
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v3, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;III)I

    move-result v0

    .line 143
    .local v0, "rnt":I
    :goto_1
    if-nez v0, :cond_0

    .line 144
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 150
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 153
    :cond_0
    return-void

    .end local v0    # "rnt":I
    :cond_1
    move v1, v3

    .line 100
    goto :goto_0

    .line 141
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v2, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;III)I

    move-result v0

    .restart local v0    # "rnt":I
    goto :goto_1

    .line 146
    :sswitch_0
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 148
    :sswitch_1
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SpenNoteDoc("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is already closed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 144
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method public constructor <init>(Landroid/content/Context;III)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "orientation"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v1

    const/16 v3, 0x20

    if-ne v1, v3, :cond_1

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 101
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 102
    iput v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 103
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 186
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 187
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;III)I

    move-result v0

    .line 188
    .local v0, "rnt":I
    if-nez v0, :cond_0

    .line 189
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 195
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 198
    :cond_0
    return-void

    .line 100
    .end local v0    # "rnt":I
    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    .line 191
    .restart local v0    # "rnt":I
    :sswitch_0
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 193
    :sswitch_1
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SpenNoteDoc("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is already closed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 189
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/io/InputStream;II)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "stream"    # Ljava/io/InputStream;
    .param p3, "width"    # I
    .param p4, "mode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;,
            Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v1

    const/16 v3, 0x20

    if-ne v1, v3, :cond_1

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 101
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 102
    iput v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 103
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 202
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 203
    const/4 v0, 0x0

    .line 204
    .local v0, "rnt":I
    instance-of v1, p2, Ljava/io/ByteArrayInputStream;

    if-eqz v1, :cond_2

    .line 205
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    check-cast p2, Ljava/io/ByteArrayInputStream;

    .end local p2    # "stream":Ljava/io/InputStream;
    invoke-direct {p0, v1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;Ljava/io/ByteArrayInputStream;II)I

    move-result v0

    .line 215
    :goto_1
    if-nez v0, :cond_0

    .line 216
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 226
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 229
    :cond_0
    :goto_2
    return-void

    .line 100
    .end local v0    # "rnt":I
    .restart local p2    # "stream":Ljava/io/InputStream;
    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    .line 206
    .restart local v0    # "rnt":I
    :cond_2
    instance-of v1, p2, Ljava/io/FileInputStream;

    if-eqz v1, :cond_3

    .line 207
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    check-cast p2, Ljava/io/FileInputStream;

    .end local p2    # "stream":Ljava/io/InputStream;
    invoke-virtual {p2}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-direct {p0, v1, v2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;Ljava/io/FileDescriptor;II)I

    move-result v0

    .line 208
    goto :goto_1

    .line 210
    .restart local p2    # "stream":Ljava/io/InputStream;
    :cond_3
    const/4 v1, 0x7

    .line 211
    const-string v2, "The parameter \'stream\' is unsupported type. This method supports only ByteArrayInputStream and FileInputStream"

    .line 210
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_2

    .line 218
    .end local p2    # "stream":Ljava/io/InputStream;
    :pswitch_1
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 220
    :pswitch_2
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    const-string v2, "It does not correspond to the NoteDoc file format"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 222
    :pswitch_3
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;

    const-string v2, "E_INVALID_PASSWORD : the password is wrong"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 224
    :pswitch_4
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SpenNoteDoc("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is already closed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 216
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;DI)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "rotation"    # D
    .param p5, "mode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;,
            Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v0

    const/16 v2, 0x20

    if-ne v0, v2, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 101
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 102
    iput v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 103
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 328
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 329
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-wide v4, p3

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DI)I

    move-result v7

    .line 331
    .local v7, "rnt":I
    if-nez v7, :cond_0

    .line 332
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 343
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 346
    :cond_0
    return-void

    .line 100
    .end local v7    # "rnt":I
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 334
    .restart local v7    # "rnt":I
    :pswitch_1
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 336
    :pswitch_2
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    .line 337
    const-string v1, "E_UNSUPPORTED_TYPE : It does not correspond to the NoteDoc file format"

    .line 336
    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 339
    :pswitch_3
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;

    const-string v1, "E_INVALID_PASSWORD : the password is wrong"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 341
    :pswitch_4
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenNoteDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 332
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;II)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "width"    # I
    .param p4, "mode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;,
            Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    move v0, v6

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 101
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 102
    iput v6, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 103
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 270
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 271
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)I

    move-result v7

    .line 273
    .local v7, "rnt":I
    if-nez v7, :cond_0

    .line 274
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 285
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 288
    :cond_0
    return-void

    .line 100
    .end local v7    # "rnt":I
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 276
    .restart local v7    # "rnt":I
    :pswitch_1
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 278
    :pswitch_2
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    .line 279
    const-string v1, "E_UNSUPPORTED_TYPE : It does not correspond to the NoteDoc file format"

    .line 278
    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 281
    :pswitch_3
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;

    const-string v1, "E_INVALID_PASSWORD : the password is wrong"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283
    :pswitch_4
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenNoteDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 274
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IIZ)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "width"    # I
    .param p4, "mode"    # I
    .param p5, "discardUnsavedData"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;,
            Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 510
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v0

    const/16 v2, 0x20

    if-ne v0, v2, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 101
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 102
    iput v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 103
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 513
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 514
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)I

    move-result v7

    .line 516
    .local v7, "rnt":I
    if-nez v7, :cond_0

    .line 517
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 528
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 531
    :cond_0
    return-void

    .line 100
    .end local v7    # "rnt":I
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 519
    .restart local v7    # "rnt":I
    :pswitch_1
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 521
    :pswitch_2
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    .line 522
    const-string v1, "E_UNSUPPORTED_TYPE : It does not correspond to the NoteDoc file format"

    .line 521
    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 524
    :pswitch_3
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;

    const-string v1, "E_INVALID_PASSWORD : the password is wrong"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 526
    :pswitch_4
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenNoteDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 517
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;DI)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;
    .param p4, "rotation"    # D
    .param p6, "mode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;,
            Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v0

    const/16 v2, 0x20

    if-ne v0, v2, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 102
    iput v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 103
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 450
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 451
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DI)I

    move-result v7

    .line 453
    .local v7, "rnt":I
    if-nez v7, :cond_0

    .line 454
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 465
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 468
    :cond_0
    return-void

    .line 100
    .end local v7    # "rnt":I
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 456
    .restart local v7    # "rnt":I
    :pswitch_1
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 458
    :pswitch_2
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    .line 459
    const-string v1, "E_UNSUPPORTED_TYPE : It does not correspond to the NoteDoc file format"

    .line 458
    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 461
    :pswitch_3
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;

    const-string v1, "E_INVALID_PASSWORD : the password is wrong"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 463
    :pswitch_4
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenNoteDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 454
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;
    .param p4, "width"    # I
    .param p5, "mode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;,
            Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    move v0, v6

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 102
    iput v6, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 103
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 387
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 388
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)I

    move-result v7

    .line 390
    .local v7, "rnt":I
    if-nez v7, :cond_0

    .line 391
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 402
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 405
    :cond_0
    return-void

    .line 100
    .end local v7    # "rnt":I
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 393
    .restart local v7    # "rnt":I
    :pswitch_1
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 395
    :pswitch_2
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    .line 396
    const-string v1, "E_UNSUPPORTED_TYPE : It does not correspond to the NoteDoc file format"

    .line 395
    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 398
    :pswitch_3
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;

    const-string v1, "E_INVALID_PASSWORD : the password is wrong"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 400
    :pswitch_4
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenNoteDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 391
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIZ)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;
    .param p4, "width"    # I
    .param p5, "mode"    # I
    .param p6, "discardUnsavedData"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;,
            Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v0

    const/16 v2, 0x20

    if-ne v0, v2, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 102
    iput v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 103
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    .line 578
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    .line 579
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)I

    move-result v7

    .line 582
    .local v7, "rnt":I
    if-nez v7, :cond_0

    .line 583
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 594
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 597
    :cond_0
    return-void

    .line 100
    .end local v7    # "rnt":I
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 585
    .restart local v7    # "rnt":I
    :pswitch_1
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 587
    :pswitch_2
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    .line 588
    const-string v1, "E_UNSUPPORTED_TYPE : It does not correspond to the NoteDoc file format"

    .line 587
    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 590
    :pswitch_3
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;

    const-string v1, "E_INVALID_PASSWORD : the password is wrong"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 592
    :pswitch_4
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenNoteDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 583
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private native Native_command(ILjava/util/ArrayList;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private native NoteDoc_appendPage(II)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_appendPage(ILjava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_appendPages(Ljava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_appendTemplatePage(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_attachFile(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private native NoteDoc_attachTemplatePage(Ljava/lang/String;Ljava/lang/String;I)Z
.end method

.method private native NoteDoc_attachToFile(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_backupObjectList(Ljava/util/ArrayList;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation
.end method

.method private native NoteDoc_close(Z)Z
.end method

.method private native NoteDoc_copyPage(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_detachFile(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_detachTemplatePage(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_discard()Z
.end method

.method private native NoteDoc_finalize()V
.end method

.method private native NoteDoc_getAppMajorVersion()I
.end method

.method private native NoteDoc_getAppMinorVersion()I
.end method

.method private native NoteDoc_getAppName()Ljava/lang/String;
.end method

.method private native NoteDoc_getAppPatchName()Ljava/lang/String;
.end method

.method private native NoteDoc_getAttachedFile(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private native NoteDoc_getAttachedFileCount()I
.end method

.method private native NoteDoc_getAuthorInfo()Lcom/samsung/android/sdk/pen/document/SpenNoteDoc$AuthorInfo;
.end method

.method private native NoteDoc_getCoverImagePath()Ljava/lang/String;
.end method

.method private native NoteDoc_getExtraDataByteArray(Ljava/lang/String;)[B
.end method

.method private native NoteDoc_getExtraDataInt(Ljava/lang/String;)I
.end method

.method private native NoteDoc_getExtraDataString(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private native NoteDoc_getExtraDataStringArray(Ljava/lang/String;)[Ljava/lang/String;
.end method

.method private native NoteDoc_getGeoTagLatitude()D
.end method

.method private native NoteDoc_getGeoTagLongitude()D
.end method

.method private native NoteDoc_getHeight()I
.end method

.method private native NoteDoc_getId()Ljava/lang/String;
.end method

.method private native NoteDoc_getInternalDirectory()Ljava/lang/String;
.end method

.method private native NoteDoc_getLastEditedPageIndex()I
.end method

.method private native NoteDoc_getOrientation()I
.end method

.method private native NoteDoc_getOrientation2(Ljava/io/ByteArrayInputStream;)I
.end method

.method private native NoteDoc_getOrientation3(Ljava/io/FileDescriptor;)I
.end method

.method private native NoteDoc_getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_getPageCount()I
.end method

.method private native NoteDoc_getPageIdByIndex(I)Ljava/lang/String;
.end method

.method private native NoteDoc_getPageIndexById(Ljava/lang/String;)I
.end method

.method private native NoteDoc_getRotation()I
.end method

.method private native NoteDoc_getTemplatePage(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_getTemplatePageCount()I
.end method

.method private native NoteDoc_getTemplatePageName(I)Ljava/lang/String;
.end method

.method private native NoteDoc_getTemplateUri()Ljava/lang/String;
.end method

.method private native NoteDoc_getWidth()I
.end method

.method private native NoteDoc_hasAttachedFile(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_hasExtraDataByteArray(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_hasExtraDataInt(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_hasExtraDataString(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_hasExtraDataStringArray(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_hasTaggedPage()Z
.end method

.method private native NoteDoc_hasTemplatePage(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_init(Ljava/lang/String;III)I
.end method

.method private native NoteDoc_init(Ljava/lang/String;Ljava/io/ByteArrayInputStream;II)I
.end method

.method private native NoteDoc_init(Ljava/lang/String;Ljava/io/ByteArrayInputStream;Ljava/lang/String;II)I
.end method

.method private native NoteDoc_init(Ljava/lang/String;Ljava/io/FileDescriptor;II)I
.end method

.method private native NoteDoc_init(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/lang/String;II)I
.end method

.method private native NoteDoc_init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DI)I
.end method

.method private native NoteDoc_init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)I
.end method

.method private native NoteDoc_insertPage(III)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_insertPage(IILjava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_insertPages(Ljava/lang/String;II)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_insertTemplatePage(ILjava/lang/String;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.end method

.method private native NoteDoc_isAllPageTextOnly()Z
.end method

.method private native NoteDoc_isChanged()Z
.end method

.method private native NoteDoc_movePageIndex(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;I)Z
.end method

.method private native NoteDoc_removeExtraDataByteArray(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_removeExtraDataInt(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_removeExtraDataString(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_removeExtraDataStringArray(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_removePage(I)Z
.end method

.method private native NoteDoc_requestSave(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_restoreObjectList(Ljava/lang/String;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native NoteDoc_revertToTemplatePage(I)Z
.end method

.method private native NoteDoc_reviseObjectList(Ljava/util/ArrayList;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)Z"
        }
    .end annotation
.end method

.method private native NoteDoc_save(Ljava/io/ByteArrayOutputStream;)Z
.end method

.method private native NoteDoc_save(Ljava/io/FileDescriptor;)Z
.end method

.method private native NoteDoc_save(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_setAppName(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_setAppVersion(IILjava/lang/String;)Z
.end method

.method private native NoteDoc_setAuthorInfo(Lcom/samsung/android/sdk/pen/document/SpenNoteDoc$AuthorInfo;)Z
.end method

.method private native NoteDoc_setCoverImage(Ljava/lang/String;)Z
.end method

.method private native NoteDoc_setExtraDataByteArray(Ljava/lang/String;[BI)Z
.end method

.method private native NoteDoc_setExtraDataInt(Ljava/lang/String;I)Z
.end method

.method private native NoteDoc_setExtraDataString(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private native NoteDoc_setExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;I)Z
.end method

.method private native NoteDoc_setGeoTag(DD)Z
.end method

.method private native NoteDoc_setTemplateUri(Ljava/lang/String;)Z
.end method

.method private static isBuildTypeEngMode()Z
    .locals 2

    .prologue
    .line 619
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private throwUncheckedException(I)V
    .locals 3
    .param p1, "errno"    # I

    .prologue
    .line 2548
    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    .line 2549
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenNoteDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2551
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2553
    return-void
.end method


# virtual methods
.method public appendPage()Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 4

    .prologue
    .line 1313
    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_appendPage(ILjava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    .line 1314
    .local v0, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v0, :cond_0

    .line 1315
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1317
    :cond_0
    return-object v0
.end method

.method public appendPage(ILjava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 2
    .param p1, "backgroundColor"    # I
    .param p2, "backgroundImagePath"    # Ljava/lang/String;
    .param p3, "backgourndImageMode"    # I

    .prologue
    .line 1346
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_appendPage(ILjava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    .line 1347
    .local v0, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v0, :cond_0

    .line 1348
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1350
    :cond_0
    return-object v0
.end method

.method public appendPages(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 5
    .param p1, "count"    # I

    .prologue
    const/4 v4, 0x1

    .line 1367
    if-ge p1, v4, :cond_0

    .line 1368
    const/4 v3, 0x7

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1370
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1371
    .local v0, "input":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1373
    invoke-direct {p0, v4, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->Native_command(ILjava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1374
    .local v1, "output":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-nez v1, :cond_1

    .line 1375
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1378
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 1379
    .local v2, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v2, :cond_2

    .line 1380
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1382
    :cond_2
    return-object v2
.end method

.method public appendPages(Ljava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 4
    .param p1, "templateName"    # Ljava/lang/String;
    .param p2, "count"    # I

    .prologue
    .line 2425
    const/4 v2, 0x1

    if-ge p2, v2, :cond_0

    .line 2426
    const/4 v2, 0x7

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 2429
    :cond_0
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_appendPages(Ljava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    .line 2430
    .local v1, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v1, :cond_1

    .line 2431
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2437
    .end local v1    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :cond_1
    :goto_0
    return-object v1

    .line 2434
    :catch_0
    move-exception v0

    .line 2435
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2436
    const-string v2, "SpenNoteDoc"

    const-string v3, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2437
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public appendTemplatePage(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 14
    .param p1, "templatePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v13, 0x0

    .line 1456
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1457
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1458
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_appendTemplatePage(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v6

    .line 1459
    .local v6, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v6, :cond_0

    .line 1460
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    .line 1466
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v10

    invoke-static {v10}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 1520
    .end local v6    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :cond_0
    :goto_0
    return-object v6

    .line 1462
    .restart local v6    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :pswitch_1
    new-instance v10, Ljava/io/IOException;

    invoke-direct {v10}, Ljava/io/IOException;-><init>()V

    throw v10

    .line 1464
    :pswitch_2
    new-instance v10, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    const-string v11, "It does not correspond to the NoteDoc file format"

    invoke-direct {v10, v11}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 1472
    .end local v6    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :cond_1
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 1473
    .local v0, "assetMgr":Landroid/content/res/AssetManager;
    invoke-virtual {v0, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    .line 1474
    .local v5, "is":Ljava/io/InputStream;
    invoke-virtual {v5}, Ljava/io/InputStream;->available()I

    move-result v4

    .line 1475
    .local v4, "fileSize":I
    new-array v7, v4, [B

    .line 1477
    .local v7, "tempData":[B
    const/4 v11, 0x0

    :try_start_0
    array-length v12, v7

    invoke-virtual {v5, v7, v11, v12}, Ljava/io/InputStream;->read([BII)I

    move-result v11

    array-length v12, v7

    if-eq v11, v12, :cond_2

    .line 1478
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 1479
    new-instance v10, Ljava/io/IOException;

    const-string v11, "Failed to is.read()"

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1481
    :catch_0
    move-exception v2

    .line 1482
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 1483
    throw v2

    .line 1485
    .end local v2    # "e":Ljava/io/IOException;
    :cond_2
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 1487
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v7}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 1489
    .local v1, "byteArrayIS":Ljava/io/ByteArrayInputStream;
    new-instance v8, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getWidth()I

    move-result v12

    invoke-direct {v8, v11, v1, v12, v13}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;-><init>(Landroid/content/Context;Ljava/io/InputStream;II)V

    .line 1491
    .local v8, "templateNote":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getOrientation()I

    move-result v11

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getOrientation()I

    move-result v12

    if-eq v11, v12, :cond_3

    .line 1492
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1493
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1494
    const/4 v11, 0x7

    .line 1495
    const-string v12, "The orientation of the template is not matched with this NoteDoc."

    .line 1494
    invoke-static {v11, v12}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 1498
    :cond_3
    invoke-virtual {v8, v13}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v9

    .line 1499
    .local v9, "templatePage":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v9, :cond_4

    .line 1500
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1501
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1502
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v11

    invoke-static {v11}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    move-object v6, v10

    .line 1503
    goto :goto_0

    .line 1506
    :cond_4
    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v11

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v12

    invoke-direct {p0, v11, v12}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_appendPage(II)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v6

    .line 1507
    .restart local v6    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v6, :cond_5

    .line 1508
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1509
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1510
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v11

    invoke-static {v11}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    move-object v6, v10

    .line 1511
    goto/16 :goto_0

    .line 1514
    :cond_5
    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->copy(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 1515
    invoke-virtual {v6, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->setTemplateUri(Ljava/lang/String;)V

    .line 1516
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->clearChangedFlagOfLayer()V

    .line 1518
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1519
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    goto/16 :goto_0

    .line 1460
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public attachFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1973
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_attachFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1974
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1976
    :cond_0
    return-void
.end method

.method public attachTemplatePage(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "templateName"    # Ljava/lang/String;
    .param p2, "templatePath"    # Ljava/lang/String;
    .param p3, "pageIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;,
            Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;
        }
    .end annotation

    .prologue
    .line 2264
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_attachTemplatePage(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2265
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2273
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 2280
    :cond_0
    :goto_0
    return-void

    .line 2267
    :sswitch_0
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2276
    :catch_0
    move-exception v0

    .line 2277
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2278
    const-string v1, "SpenNoteDoc"

    const-string v2, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2269
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :sswitch_1
    :try_start_1
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    const-string v2, "It does not correspond to the NoteDoc file format"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2271
    :sswitch_2
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;

    const-string v2, "E_INVALID_PASSWORD : template page is locked."

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_0

    .line 2265
    nop

    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0xd -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method public attachToFile(Ljava/lang/String;)V
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2127
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_attachToFile(Ljava/lang/String;)Z

    move-result v0

    .line 2129
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2130
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2139
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2142
    :cond_0
    return-void

    .line 2132
    :sswitch_0
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 2137
    :sswitch_1
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SpenNoteDoc("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is already closed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2130
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method public backupObjectList(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 1
    .param p2, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2181
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_backupObjectList(Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2182
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 2184
    :cond_0
    return-void
.end method

.method public close()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, -0x1

    .line 685
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    if-eqz v0, :cond_1

    .line 686
    iget v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    if-ne v0, v2, :cond_2

    .line 711
    :cond_0
    :goto_0
    return-void

    .line 690
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 695
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_close(Z)Z

    move-result v0

    if-nez v0, :cond_3

    .line 696
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 702
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 705
    :cond_3
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 706
    iput v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 710
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    goto :goto_0

    .line 698
    :sswitch_0
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 700
    :sswitch_1
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenNoteDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 708
    :cond_4
    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    goto :goto_1

    .line 696
    nop

    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method public close(Z)V
    .locals 6
    .param p1, "removeCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, -0x1

    .line 739
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    if-eqz v0, :cond_1

    .line 740
    iget v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    if-ne v0, v2, :cond_2

    .line 766
    :cond_0
    :goto_0
    return-void

    .line 744
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 749
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_close(Z)Z

    move-result v0

    if-nez v0, :cond_3

    .line 750
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 756
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 760
    :cond_3
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 761
    iput v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 765
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    goto :goto_0

    .line 752
    :sswitch_0
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 754
    :sswitch_1
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenNoteDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 763
    :cond_4
    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    goto :goto_1

    .line 750
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method public copyPage(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 4
    .param p1, "sourcePage"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "pageIndex"    # I

    .prologue
    .line 2500
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_copyPage(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    .line 2501
    .local v1, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v1, :cond_0

    .line 2502
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2508
    .end local v1    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :cond_0
    :goto_0
    return-object v1

    .line 2505
    :catch_0
    move-exception v0

    .line 2506
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2507
    const-string v2, "SpenNoteDoc"

    const-string v3, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2508
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public detachFile(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1989
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_detachFile(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1990
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1992
    :cond_0
    return-void
.end method

.method public detachTemplatePage(Ljava/lang/String;)V
    .locals 3
    .param p1, "templateName"    # Ljava/lang/String;

    .prologue
    .line 2300
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_detachTemplatePage(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2301
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2307
    :cond_0
    :goto_0
    return-void

    .line 2303
    :catch_0
    move-exception v0

    .line 2304
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2305
    const-string v1, "SpenNoteDoc"

    const-string v2, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public discard()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, -0x1

    .line 640
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    if-eqz v0, :cond_1

    .line 641
    iget v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    if-ne v0, v2, :cond_2

    .line 666
    :cond_0
    :goto_0
    return-void

    .line 645
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 650
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_discard()Z

    move-result v0

    if-nez v0, :cond_3

    .line 651
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 657
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 660
    :cond_3
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 661
    iput v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 665
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    goto :goto_0

    .line 653
    :sswitch_0
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 655
    :sswitch_1
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenNoteDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 663
    :cond_4
    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    goto :goto_1

    .line 651
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    .line 2208
    instance-of v1, p1, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    if-eqz v1, :cond_2

    .line 2209
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    if-eqz v1, :cond_1

    .line 2210
    iget v1, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    .end local p1    # "o":Ljava/lang/Object;
    iget v2, p1, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    if-ne v1, v2, :cond_2

    .line 2219
    :cond_0
    :goto_0
    return v0

    .line 2214
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    .end local p1    # "o":Ljava/lang/Object;
    iget-wide v4, p1, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 2219
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 605
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->Native_command(ILjava/util/ArrayList;)Ljava/util/ArrayList;

    .line 607
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_finalize()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 609
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 611
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    if-eqz v0, :cond_0

    .line 612
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 616
    :goto_0
    return-void

    .line 608
    :catchall_0
    move-exception v0

    .line 609
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 610
    throw v0

    .line 614
    :cond_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    goto :goto_0
.end method

.method public getAppMajorVersion()I
    .locals 1

    .prologue
    .line 1029
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getAppMajorVersion()I

    move-result v0

    return v0
.end method

.method public getAppMinorVersion()I
    .locals 1

    .prologue
    .line 1041
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getAppMinorVersion()I

    move-result v0

    return v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 997
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getAppName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppPatchName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1053
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getAppPatchName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAttachedFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 2018
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getAttachedFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2019
    .local v0, "filepath":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 2020
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 2022
    :cond_0
    return-object v0
.end method

.method public getAttachedFileCount()I
    .locals 1

    .prologue
    .line 2002
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getAttachedFileCount()I

    move-result v0

    return v0
.end method

.method public getAuthorInfo()Lcom/samsung/android/sdk/pen/document/SpenNoteDoc$AuthorInfo;
    .locals 1

    .prologue
    .line 928
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getAuthorInfo()Lcom/samsung/android/sdk/pen/document/SpenNoteDoc$AuthorInfo;

    move-result-object v0

    return-object v0
.end method

.method public getCoverImagePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 878
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getCoverImagePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExtraDataByteArray(Ljava/lang/String;)[B
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1193
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getExtraDataByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getExtraDataInt(Ljava/lang/String;)I
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1165
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getExtraDataInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getExtraDataString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1151
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getExtraDataString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExtraDataStringArray(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1179
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getExtraDataStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGeoTagLatitude()D
    .locals 2

    .prologue
    .line 959
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getGeoTagLatitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public getGeoTagLongitude()D
    .locals 2

    .prologue
    .line 971
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getGeoTagLongitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 824
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getHeight()I

    move-result v0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 776
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInternalDirectory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2152
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getInternalDirectory()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastEditedPageIndex()I
    .locals 1

    .prologue
    .line 2163
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getLastEditedPageIndex()I

    move-result v0

    return v0
.end method

.method public getOrientation()I
    .locals 2

    .prologue
    .line 848
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getOrientation()I

    move-result v0

    .line 849
    .local v0, "rnt":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 850
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 852
    :cond_0
    return v0
.end method

.method public getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 2
    .param p1, "pageIndex"    # I

    .prologue
    .line 1925
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    .line 1926
    .local v0, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v0, :cond_0

    .line 1927
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1929
    :cond_0
    return-object v0
.end method

.method public getPageCount()I
    .locals 4

    .prologue
    .line 1940
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    if-eqz v0, :cond_0

    .line 1941
    const-string v0, "Model_SpenNoteDoc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mHandle = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1945
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getPageCount()I

    move-result v0

    return v0

    .line 1943
    :cond_0
    const-string v0, "Model_SpenNoteDoc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mHandle = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getPageIdByIndex(I)Ljava/lang/String;
    .locals 2
    .param p1, "pageIndex"    # I

    .prologue
    .line 1887
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getPageIdByIndex(I)Ljava/lang/String;

    move-result-object v0

    .line 1888
    .local v0, "id":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 1889
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1891
    :cond_0
    return-object v0
.end method

.method public getPageIndexById(Ljava/lang/String;)I
    .locals 2
    .param p1, "pageId"    # Ljava/lang/String;

    .prologue
    .line 1906
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getPageIndexById(Ljava/lang/String;)I

    move-result v0

    .line 1907
    .local v0, "index":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1908
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1910
    :cond_0
    return v0
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 835
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getRotation()I

    move-result v0

    return v0
.end method

.method public getTemplatePage(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 4
    .param p1, "templateName"    # Ljava/lang/String;

    .prologue
    .line 2396
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getTemplatePage(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    .line 2397
    .local v1, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v1, :cond_0

    .line 2398
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2404
    .end local v1    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :cond_0
    :goto_0
    return-object v1

    .line 2401
    :catch_0
    move-exception v0

    .line 2402
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2403
    const-string v2, "SpenNoteDoc"

    const-string v3, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2404
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTemplatePageCount()I
    .locals 3

    .prologue
    .line 2318
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getTemplatePageCount()I
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2322
    :goto_0
    return v1

    .line 2319
    :catch_0
    move-exception v0

    .line 2320
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2321
    const-string v1, "SpenNoteDoc"

    const-string v2, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2322
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTemplatePageName(I)Ljava/lang/String;
    .locals 4
    .param p1, "templateIndex"    # I

    .prologue
    .line 2340
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getTemplatePageName(I)Ljava/lang/String;

    move-result-object v1

    .line 2341
    .local v1, "templatePageName":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 2342
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2348
    .end local v1    # "templatePageName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 2345
    :catch_0
    move-exception v0

    .line 2346
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2347
    const-string v2, "SpenNoteDoc"

    const-string v3, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2348
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTemplateUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 802
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getTemplateUri()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 813
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_getWidth()I

    move-result v0

    return v0
.end method

.method public hasAttachedFile(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 2036
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_hasAttachedFile(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasExtraDataByteArray(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1250
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_hasExtraDataByteArray(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasExtraDataInt(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1222
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_hasExtraDataInt(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasExtraDataString(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1207
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_hasExtraDataString(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasExtraDataStringArray(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1236
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_hasExtraDataStringArray(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasTaggedPage()Z
    .locals 1

    .prologue
    .line 890
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_hasTaggedPage()Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 2229
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->is32Bit:Z

    if-eqz v0, :cond_0

    .line 2230
    iget v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle:I

    .line 2232
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mHandle2:J

    long-to-int v0, v0

    goto :goto_0
.end method

.method public insertPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 4
    .param p1, "pageIndex"    # I

    .prologue
    .line 1398
    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, p1, v1, v2, v3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_insertPage(IILjava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    .line 1399
    .local v0, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v0, :cond_0

    .line 1400
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1402
    :cond_0
    return-object v0
.end method

.method public insertPage(IILjava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 2
    .param p1, "pageIndex"    # I
    .param p2, "backgroundColor"    # I
    .param p3, "backgroundImagePath"    # Ljava/lang/String;
    .param p4, "backgourndImageMode"    # I

    .prologue
    .line 1426
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_insertPage(IILjava/lang/String;I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    .line 1427
    .local v0, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v0, :cond_0

    .line 1428
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1430
    :cond_0
    return-object v0
.end method

.method public insertPages(Ljava/lang/String;II)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 4
    .param p1, "templateName"    # Ljava/lang/String;
    .param p2, "pageIndex"    # I
    .param p3, "count"    # I

    .prologue
    .line 2464
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_insertPages(Ljava/lang/String;II)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    .line 2465
    .local v1, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v1, :cond_0

    .line 2466
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2472
    .end local v1    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :cond_0
    :goto_0
    return-object v1

    .line 2469
    :catch_0
    move-exception v0

    .line 2470
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2471
    const-string v2, "SpenNoteDoc"

    const-string v3, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2472
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public insertTemplatePage(ILjava/lang/String;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 13
    .param p1, "pageIndex"    # I
    .param p2, "templatePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    .line 1549
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1550
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1551
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_insertTemplatePage(ILjava/lang/String;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v6

    .line 1552
    .local v6, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v6, :cond_0

    .line 1553
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    .line 1561
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v10

    invoke-static {v10}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 1613
    .end local v6    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :cond_0
    :goto_0
    return-object v6

    .line 1555
    .restart local v6    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :sswitch_0
    new-instance v10, Ljava/io/IOException;

    invoke-direct {v10}, Ljava/io/IOException;-><init>()V

    throw v10

    .line 1557
    :sswitch_1
    new-instance v10, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    const-string v11, "It does not correspond to the NoteDoc file format"

    invoke-direct {v10, v11}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 1559
    :sswitch_2
    new-instance v10, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "SpenNoteDoc("

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ") is already closed."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 1566
    .end local v6    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :cond_1
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 1567
    .local v0, "assetMgr":Landroid/content/res/AssetManager;
    invoke-virtual {v0, p2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    .line 1568
    .local v5, "is":Ljava/io/InputStream;
    invoke-virtual {v5}, Ljava/io/InputStream;->available()I

    move-result v4

    .line 1569
    .local v4, "fileSize":I
    new-array v7, v4, [B

    .line 1571
    .local v7, "tempData":[B
    const/4 v10, 0x0

    :try_start_0
    array-length v11, v7

    invoke-virtual {v5, v7, v10, v11}, Ljava/io/InputStream;->read([BII)I

    move-result v10

    array-length v11, v7

    if-eq v10, v11, :cond_2

    .line 1572
    new-instance v10, Ljava/io/IOException;

    const-string v11, "Failed to is.read()"

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1574
    :catch_0
    move-exception v2

    .line 1575
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 1576
    const/4 v6, 0x0

    goto :goto_0

    .line 1578
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 1580
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v7}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 1582
    .local v1, "byteArrayIS":Ljava/io/ByteArrayInputStream;
    new-instance v8, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getWidth()I

    move-result v11

    const/4 v12, 0x0

    invoke-direct {v8, v10, v1, v11, v12}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;-><init>(Landroid/content/Context;Ljava/io/InputStream;II)V

    .line 1584
    .local v8, "templateNote":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getOrientation()I

    move-result v10

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getOrientation()I

    move-result v11

    if-eq v10, v11, :cond_3

    .line 1585
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1586
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1587
    const/4 v10, 0x7

    .line 1588
    const-string v11, "The orientation of the template is not matched with this NoteDoc."

    .line 1587
    invoke-static {v10, v11}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 1591
    :cond_3
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v9

    .line 1592
    .local v9, "templatePage":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v9, :cond_4

    .line 1593
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1594
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1595
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v10

    invoke-static {v10}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 1596
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 1599
    :cond_4
    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v10

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v11

    invoke-direct {p0, p1, v10, v11}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_insertPage(III)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v6

    .line 1600
    .restart local v6    # "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v6, :cond_5

    .line 1601
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1602
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1603
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v10

    invoke-static {v10}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 1604
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 1607
    :cond_5
    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->copy(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 1608
    invoke-virtual {v6, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->setTemplateUri(Ljava/lang/String;)V

    .line 1609
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->clearChangedFlagOfLayer()V

    .line 1611
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1612
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    goto/16 :goto_0

    .line 1553
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0xd -> :sswitch_1
        0x13 -> :sswitch_2
    .end sparse-switch
.end method

.method public isAllPageTextOnly()Z
    .locals 1

    .prologue
    .line 902
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_isAllPageTextOnly()Z

    move-result v0

    return v0
.end method

.method public isChanged()Z
    .locals 1

    .prologue
    .line 1957
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_isChanged()Z

    move-result v0

    return v0
.end method

.method public movePageIndex(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;I)V
    .locals 1
    .param p1, "page"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "step"    # I

    .prologue
    .line 1870
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_movePageIndex(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1871
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1873
    :cond_0
    return-void
.end method

.method public removeExtraDataByteArray(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1300
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_removeExtraDataByteArray(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1301
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1303
    :cond_0
    return-void
.end method

.method public removeExtraDataInt(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1274
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_removeExtraDataInt(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1275
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1277
    :cond_0
    return-void
.end method

.method public removeExtraDataString(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1261
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_removeExtraDataString(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1262
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1264
    :cond_0
    return-void
.end method

.method public removeExtraDataStringArray(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1287
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_removeExtraDataStringArray(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1288
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1290
    :cond_0
    return-void
.end method

.method public removePage(I)V
    .locals 1
    .param p1, "pageIndex"    # I

    .prologue
    .line 1846
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_removePage(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1847
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1849
    :cond_0
    return-void
.end method

.method public restoreObjectList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2198
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_restoreObjectList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public revertToTemplatePage(I)V
    .locals 25
    .param p1, "pageIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    .line 1633
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v15

    .line 1634
    .local v15, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-virtual {v15}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getTemplateUri()Ljava/lang/String;

    move-result-object v21

    .line 1636
    .local v21, "templatePath":Ljava/lang/String;
    if-nez v21, :cond_1

    .line 1637
    invoke-virtual {v15}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->removeAllObject()V

    .line 1753
    :cond_0
    :goto_0
    return-void

    .line 1641
    :cond_1
    const/4 v10, 0x0

    .line 1643
    .local v10, "flagTemplatePage":Z
    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_hasTemplatePage(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v22

    if-eqz v22, :cond_2

    .line 1644
    const/4 v10, 0x1

    .line 1655
    :cond_2
    :goto_1
    if-eqz v10, :cond_3

    .line 1656
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_hasTemplatePage(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 1657
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_revertToTemplatePage(I)Z

    move-result v16

    .line 1658
    .local v16, "rnt":Z
    if-nez v16, :cond_0

    .line 1659
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v22

    sparse-switch v22, :sswitch_data_0

    .line 1667
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v22

    invoke-static/range {v22 .. v22}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    .line 1646
    .end local v16    # "rnt":Z
    :catch_0
    move-exception v7

    .line 1647
    .local v7, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v7}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 1648
    const-string v22, "SpenNoteDoc"

    const-string v23, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1650
    const-string/jumbo v22, "template_name://"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 1651
    const/4 v10, 0x1

    goto :goto_1

    .line 1661
    .end local v7    # "e":Ljava/lang/UnsatisfiedLinkError;
    .restart local v16    # "rnt":Z
    :sswitch_0
    new-instance v22, Ljava/io/IOException;

    invoke-direct/range {v22 .. v22}, Ljava/io/IOException;-><init>()V

    throw v22

    .line 1663
    :sswitch_1
    new-instance v22, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    const-string v23, "It does not correspond to the NoteDoc file format"

    invoke-direct/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 1665
    :sswitch_2
    new-instance v22, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "SpenNoteDoc("

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ") is already closed."

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 1674
    .end local v16    # "rnt":Z
    :cond_3
    new-instance v8, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1676
    .local v8, "file":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v22

    if-eqz v22, :cond_4

    .line 1677
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_revertToTemplatePage(I)Z

    move-result v16

    .line 1678
    .restart local v16    # "rnt":Z
    if-nez v16, :cond_0

    .line 1679
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v22

    sparse-switch v22, :sswitch_data_1

    .line 1687
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v22

    invoke-static/range {v22 .. v22}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto/16 :goto_0

    .line 1681
    :sswitch_3
    new-instance v22, Ljava/io/IOException;

    invoke-direct/range {v22 .. v22}, Ljava/io/IOException;-><init>()V

    throw v22

    .line 1683
    :sswitch_4
    new-instance v22, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;

    const-string v23, "It does not correspond to the NoteDoc file format"

    invoke-direct/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 1685
    :sswitch_5
    new-instance v22, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "SpenNoteDoc("

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ") is already closed."

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 1692
    .end local v16    # "rnt":Z
    :cond_4
    const/16 v18, 0x0

    .line 1693
    .local v18, "tempData":[B
    const/4 v12, 0x0

    .line 1694
    .local v12, "is":Ljava/io/InputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    .line 1696
    .local v4, "assetMgr":Landroid/content/res/AssetManager;
    :try_start_1
    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v12

    .line 1697
    invoke-virtual {v12}, Ljava/io/InputStream;->available()I

    move-result v9

    .line 1698
    .local v9, "fileSize":I
    if-nez v9, :cond_6

    .line 1699
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V

    .line 1700
    invoke-virtual {v15}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->removeAllObject()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 1710
    .end local v9    # "fileSize":I
    :catch_1
    move-exception v7

    .line 1711
    .local v7, "e":Ljava/io/IOException;
    if-eqz v12, :cond_5

    .line 1712
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V

    .line 1714
    :cond_5
    invoke-virtual {v15}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->removeAllObject()V

    goto/16 :goto_0

    .line 1703
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v9    # "fileSize":I
    :cond_6
    :try_start_2
    new-array v0, v9, [B

    move-object/from16 v18, v0

    .line 1705
    const/16 v22, 0x0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v12, v0, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v22

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_7

    .line 1706
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V

    .line 1707
    new-instance v22, Ljava/io/IOException;

    const-string v23, "Failed to is.read()"

    invoke-direct/range {v22 .. v23}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 1709
    :cond_7
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1718
    new-instance v5, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, v18

    invoke-direct {v5, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 1720
    .local v5, "byteArrayIS":Ljava/io/ByteArrayInputStream;
    new-instance v19, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getWidth()I

    move-result v23

    const/16 v24, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;-><init>(Landroid/content/Context;Ljava/io/InputStream;II)V

    .line 1722
    .local v19, "templateNote":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getOrientation()I

    move-result v22

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getOrientation()I

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_8

    .line 1723
    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1724
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1725
    const/16 v22, 0x7

    .line 1726
    const-string v23, "The orientation of the template is not matched with this NoteDoc."

    .line 1725
    invoke-static/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 1729
    :cond_8
    invoke-virtual {v15}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->removeAllObject()V

    .line 1731
    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v20

    .line 1733
    .local v20, "templatePage":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v20, :cond_9

    .line 1734
    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1735
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1736
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v22

    invoke-static/range {v22 .. v22}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto/16 :goto_0

    .line 1740
    :cond_9
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getObjectList()Ljava/util/ArrayList;

    move-result-object v13

    .line 1741
    .local v13, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1742
    .local v6, "count":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    if-lt v11, v6, :cond_a

    .line 1751
    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->close()V

    .line 1752
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    goto/16 :goto_0

    .line 1743
    :cond_a
    invoke-virtual {v13, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 1744
    .local v17, "temp":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->createObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v14

    .line 1745
    .local v14, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    if-eqz v14, :cond_b

    .line 1746
    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 1747
    invoke-virtual {v15, v14}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 1742
    :cond_b
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 1659
    nop

    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0xd -> :sswitch_1
        0x13 -> :sswitch_2
    .end sparse-switch

    .line 1679
    :sswitch_data_1
    .sparse-switch
        0xb -> :sswitch_3
        0xd -> :sswitch_4
        0x13 -> :sswitch_5
    .end sparse-switch
.end method

.method public revertToTemplatePage(ILjava/lang/String;)V
    .locals 15
    .param p1, "pageIndex"    # I
    .param p2, "absolutePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
        }
    .end annotation

    .prologue
    .line 1774
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v7

    .line 1776
    .local v7, "page":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->removeAllObject()V

    .line 1778
    if-nez p2, :cond_0

    .line 1828
    :goto_0
    return-void

    .line 1782
    :cond_0
    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1784
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1786
    const/4 v9, 0x0

    .line 1788
    .local v9, "templateNote":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    :try_start_0
    new-instance v10, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getWidth()I

    move-result v13

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-direct {v10, v12, v0, v13, v14}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;-><init>(Landroid/content/Context;Ljava/lang/String;II)V
    :try_end_0
    .catch Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException; {:try_start_0 .. :try_end_0} :catch_1

    .end local v9    # "templateNote":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    .local v10, "templateNote":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    move-object v9, v10

    .line 1797
    .end local v10    # "templateNote":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    .restart local v9    # "templateNote":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    :goto_1
    if-nez v9, :cond_1

    .line 1798
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v12

    invoke-static {v12}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    .line 1789
    :catch_0
    move-exception v2

    .line 1791
    .local v2, "e":Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;->printStackTrace()V

    goto :goto_1

    .line 1792
    .end local v2    # "e":Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;
    :catch_1
    move-exception v2

    .line 1794
    .local v2, "e":Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;->printStackTrace()V

    goto :goto_1

    .line 1802
    .end local v2    # "e":Lcom/samsung/android/sdk/pen/document/SpenUnsupportedVersionException;
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getOrientation()I

    move-result v12

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getOrientation()I

    move-result v13

    if-eq v12, v13, :cond_2

    .line 1803
    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1804
    const/4 v12, 0x7

    .line 1805
    const-string v13, "The orientation of the template is not matched with this NoteDoc."

    .line 1804
    invoke-static {v12, v13}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 1808
    :cond_2
    const/4 v12, 0x0

    invoke-virtual {v9, v12}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v11

    .line 1810
    .local v11, "templatePage":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    if-nez v11, :cond_3

    .line 1811
    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 1812
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v12

    invoke-static {v12}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    .line 1816
    :cond_3
    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getObjectList()Ljava/util/ArrayList;

    move-result-object v5

    .line 1817
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 1818
    .local v1, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    if-lt v4, v1, :cond_4

    .line 1827
    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    goto :goto_0

    .line 1819
    :cond_4
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 1820
    .local v8, "temp":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v12

    invoke-virtual {v7, v12}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->createObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v6

    .line 1821
    .local v6, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    if-eqz v6, :cond_5

    .line 1822
    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 1823
    invoke-virtual {v7, v6}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 1818
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1830
    .end local v1    # "count":I
    .end local v4    # "i":I
    .end local v5    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    .end local v6    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .end local v8    # "temp":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .end local v9    # "templateNote":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    .end local v11    # "templatePage":Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    :cond_6
    new-instance v12, Ljava/io/IOException;

    const-string v13, "The file does not exist."

    invoke-direct {v12, v13}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v12
.end method

.method public reviseObjectList(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2523
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 2524
    :cond_0
    const/4 v4, 0x7

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 2526
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2527
    .local v3, "reviseObjectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_2

    .line 2537
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_5

    .line 2545
    .end local v1    # "i":I
    .end local v3    # "reviseObjectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    :goto_1
    return-void

    .line 2528
    .restart local v1    # "i":I
    .restart local v3    # "reviseObjectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    :cond_2
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 2529
    .local v2, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    if-eqz v2, :cond_3

    .line 2530
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_4

    .line 2531
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2527
    .end local v2    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_3
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2532
    .restart local v2    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_4
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_3

    .line 2533
    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .end local v2    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->reviseObjectList(Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 2541
    .end local v1    # "i":I
    .end local v3    # "reviseObjectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    :catch_0
    move-exception v0

    .line 2542
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 2543
    const-string v4, "SpenNoteDoc"

    const-string v5, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2540
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    .restart local v1    # "i":I
    .restart local v3    # "reviseObjectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    :cond_5
    :try_start_1
    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_reviseObjectList(Ljava/util/ArrayList;)Z
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public save(Ljava/io/OutputStream;)V
    .locals 4
    .param p1, "stream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2054
    const/4 v0, 0x0

    .line 2055
    .local v0, "rnt":Z
    instance-of v1, p1, Ljava/io/ByteArrayOutputStream;

    if-eqz v1, :cond_1

    .line 2056
    check-cast p1, Ljava/io/ByteArrayOutputStream;

    .end local p1    # "stream":Ljava/io/OutputStream;
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_save(Ljava/io/ByteArrayOutputStream;)Z

    move-result v0

    .line 2073
    :goto_0
    if-nez v0, :cond_0

    .line 2074
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2083
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2086
    :cond_0
    :goto_1
    return-void

    .line 2057
    .restart local p1    # "stream":Ljava/io/OutputStream;
    :cond_1
    instance-of v1, p1, Ljava/io/FileOutputStream;

    if-eqz v1, :cond_2

    .line 2059
    check-cast p1, Ljava/io/FileOutputStream;

    .end local p1    # "stream":Ljava/io/OutputStream;
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_save(Ljava/io/FileDescriptor;)Z

    move-result v0

    .line 2060
    goto :goto_0

    .line 2068
    .restart local p1    # "stream":Ljava/io/OutputStream;
    :cond_2
    const/4 v1, 0x7

    .line 2069
    const-string v2, "The parameter \'stream\' is unsupported type. This method supports only ByteArrayOutputStream and FileOutputStream"

    .line 2068
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_1

    .line 2076
    .end local p1    # "stream":Ljava/io/OutputStream;
    :sswitch_0
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 2081
    :sswitch_1
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SpenNoteDoc("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is already closed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2074
    nop

    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method public save(Ljava/lang/String;)V
    .locals 4
    .param p1, "filepath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2100
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_save(Ljava/lang/String;)Z

    move-result v0

    .line 2102
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2103
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2112
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2115
    :cond_0
    return-void

    .line 2105
    :sswitch_0
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 2110
    :sswitch_1
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SpenNoteDoc("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is already closed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2103
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method public setAppName(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 983
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setAppName(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 984
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 986
    :cond_0
    return-void
.end method

.method public setAppVersion(IILjava/lang/String;)V
    .locals 1
    .param p1, "majorVersion"    # I
    .param p2, "minorVersion"    # I
    .param p3, "patchName"    # Ljava/lang/String;

    .prologue
    .line 1015
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setAppVersion(IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1016
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1018
    :cond_0
    return-void
.end method

.method public setAuthorInfo(Lcom/samsung/android/sdk/pen/document/SpenNoteDoc$AuthorInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/document/SpenNoteDoc$AuthorInfo;

    .prologue
    .line 914
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setAuthorInfo(Lcom/samsung/android/sdk/pen/document/SpenNoteDoc$AuthorInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 915
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 917
    :cond_0
    return-void
.end method

.method public setCoverImage(Ljava/lang/String;)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 864
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setCoverImage(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 865
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 867
    :cond_0
    return-void
.end method

.method public setExtraDataByteArray(Ljava/lang/String;[B)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [B

    .prologue
    .line 1129
    if-eqz p2, :cond_1

    .line 1130
    array-length v0, p2

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setExtraDataByteArray(Ljava/lang/String;[BI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1131
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1138
    :cond_0
    :goto_0
    return-void

    .line 1134
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setExtraDataByteArray(Ljava/lang/String;[BI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1135
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    goto :goto_0
.end method

.method public setExtraDataInt(Ljava/lang/String;I)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 1087
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setExtraDataInt(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1088
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1090
    :cond_0
    return-void
.end method

.method public setExtraDataString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1069
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setExtraDataString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1070
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1072
    :cond_0
    return-void
.end method

.method public setExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [Ljava/lang/String;

    .prologue
    .line 1105
    if-eqz p2, :cond_1

    .line 1106
    array-length v0, p2

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1107
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 1114
    :cond_0
    :goto_0
    return-void

    .line 1110
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1111
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    goto :goto_0
.end method

.method public setGeoTag(DD)V
    .locals 1
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 945
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setGeoTag(DD)Z

    move-result v0

    if-nez v0, :cond_0

    .line 946
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 948
    :cond_0
    return-void
.end method

.method public setTemplateUri(Ljava/lang/String;)V
    .locals 1
    .param p1, "templateUri"    # Ljava/lang/String;

    .prologue
    .line 788
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->NoteDoc_setTemplateUri(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 789
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->throwUncheckedException(I)V

    .line 791
    :cond_0
    return-void
.end method
