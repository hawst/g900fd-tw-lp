.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$20;
.super Ljava/lang/Object;
.source "SpenSettingEraserLayout.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->eraserTypeLayout()Landroid/view/ViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    .line 1461
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1467
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1475
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 1469
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mEraserType02:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->access$43(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->access$35(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v1

    const-string/jumbo v2, "snote_eraser_popup_text_press"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 1472
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mEraserType02:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->access$43(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->access$35(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v1

    const-string/jumbo v2, "snote_eraser_popup_text"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 1467
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
