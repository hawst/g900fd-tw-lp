.class public Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;
.super Landroid/app/Fragment;
.source "VideoIntentFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;
    }
.end annotation


# static fields
.field private static mBackupListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;


# instance fields
.field private mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 40
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 63
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 64
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    if-eqz v1, :cond_0

    .line 65
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 66
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.action.PICK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    const-string/jumbo v1, "video/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 70
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 79
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 80
    if-eqz p3, :cond_2

    .line 81
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    if-nez v0, :cond_1

    .line 82
    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mBackupListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    if-eqz v0, :cond_0

    .line 83
    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mBackupListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    invoke-interface {v0, p3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;->onResult(Landroid/content/Intent;)V

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    invoke-interface {v0, p3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;->onResult(Landroid/content/Intent;)V

    goto :goto_0

    .line 89
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    invoke-interface {v0, p3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;->onResult(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 26
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    if-eqz v0, :cond_0

    .line 27
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;->onDestroy()V

    .line 29
    :cond_0
    return-void
.end method

.method public setVideoIntentFragmentListener(Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    .prologue
    .line 100
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mBackupListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    .line 103
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    .line 104
    return-void
.end method
