.class Lcom/samsung/android/sdk/pen/engine/SpenInView$SetPageDocHandler;
.super Landroid/os/Handler;
.source "SpenInView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenInView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SetPageDocHandler"
.end annotation


# instance fields
.field private final mSpenView:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenInView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 1
    .param p1, "view"    # Lcom/samsung/android/sdk/pen/engine/SpenInView;

    .prologue
    .line 6302
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 6303
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SetPageDocHandler;->mSpenView:Ljava/lang/ref/WeakReference;

    .line 6304
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 6308
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SetPageDocHandler;->mSpenView:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;

    .line 6309
    .local v0, "spenView":Lcom/samsung/android/sdk/pen/engine/SpenInView;
    if-nez v0, :cond_0

    .line 6315
    :goto_0
    return-void

    .line 6312
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreenFrameBuffer()V

    .line 6313
    const/4 v1, 0x0

    const/4 v2, 0x1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V
    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$43(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;Z)V

    .line 6314
    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateNotepad()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$48(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    goto :goto_0
.end method
