.class public Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;
.super Ljava/lang/Object;
.source "FountainPen.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mIs64:Z

.field public final nativeFountainPen:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    .line 32
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    .line 34
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_init()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    .line 35
    return-void

    .line 32
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private Native_construct(J)Z
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 329
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 330
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_construct(J)Z

    move-result v0

    .line 332
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_construct(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_draw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "event"    # Landroid/view/MotionEvent;
    .param p4, "rect"    # Landroid/graphics/RectF;
    .param p5, "toolType"    # I

    .prologue
    .line 364
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 365
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_draw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    .line 367
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_draw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_getAdvancedSetting(J)Ljava/lang/String;
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 448
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 449
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_getAdvancedSetting(J)Ljava/lang/String;

    move-result-object v0

    .line 451
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_getAdvancedSetting(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_getColor(J)I
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 420
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 421
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_getColor(J)I

    move-result v0

    .line 423
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getMaxSettingValue(J)F
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 406
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 407
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_getMaxSettingValue(J)F

    move-result v0

    .line 409
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_getMaxSettingValue(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getMinSettingValue(J)F
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 399
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 400
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_getMinSettingValue(J)F

    move-result v0

    .line 402
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_getMinSettingValue(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getPenAttribute(JI)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "attribute"    # I

    .prologue
    .line 465
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 466
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_getPenAttribute(JI)Z

    move-result v0

    .line 468
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_getPenAttribute(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_getProperty(JLandroid/os/Bundle;)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "propertyMap"    # Landroid/os/Bundle;

    .prologue
    .line 357
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 358
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_getProperty(JLandroid/os/Bundle;)Z

    move-result v0

    .line 360
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_getProperty(ILandroid/os/Bundle;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_getSize(J)F
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 392
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 393
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_getSize(J)F

    move-result v0

    .line 395
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_getSize(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getStrokeRect(J[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;
    .locals 7
    .param p1, "nativePen"    # J
    .param p3, "points"    # [Landroid/graphics/PointF;
    .param p4, "pressures"    # [F
    .param p5, "timestamps"    # [I
    .param p6, "size"    # F
    .param p7, "isCurvable"    # Z
    .param p8, "advanced"    # Ljava/lang/String;

    .prologue
    .line 456
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 457
    invoke-static/range {p1 .. p8}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_getStrokeRect(J[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;

    move-result-object v0

    .line 460
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move v4, p6

    move v5, p7

    move-object v6, p8

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_getStrokeRect(I[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_init()J
    .locals 2

    .prologue
    .line 322
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 323
    invoke-static {}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_init_64()J

    move-result-wide v0

    .line 325
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_init()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method private Native_isCurveEnabled(J)Z
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 434
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 435
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_isCurveEnabled(J)Z

    move-result v0

    .line 437
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_isCurveEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_onLoad(J)V
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 336
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 337
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_onLoad(J)V

    .line 340
    :goto_0
    return-void

    .line 339
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_onLoad(I)V

    goto :goto_0
.end method

.method private Native_onUnload(J)V
    .locals 1
    .param p1, "nativePen"    # J

    .prologue
    .line 343
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 344
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_onUnload(J)V

    .line 347
    :goto_0
    return-void

    .line 346
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_onUnload(I)V

    goto :goto_0
.end method

.method private Native_redraw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "event"    # Landroid/view/MotionEvent;
    .param p4, "rect"    # Landroid/graphics/RectF;
    .param p5, "toolType"    # I

    .prologue
    .line 371
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 372
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_redraw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    .line 374
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_redraw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setAdvancedSetting(JLjava/lang/String;)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "advancedSetting"    # Ljava/lang/String;

    .prologue
    .line 441
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 442
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_setAdvancedSetting(JLjava/lang/String;)Z

    move-result v0

    .line 444
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_setAdvancedSetting(ILjava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setBitmap(JLandroid/graphics/Bitmap;)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 378
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 379
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_setBitmap(JLandroid/graphics/Bitmap;)Z

    move-result v0

    .line 381
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_setBitmap(ILandroid/graphics/Bitmap;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setColor(JI)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "color"    # I

    .prologue
    .line 413
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 414
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_setColor(JI)Z

    move-result v0

    .line 416
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_setColor(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setCurveEnabled(JZ)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "curve"    # Z

    .prologue
    .line 427
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 428
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_setCurveEnabled(JZ)Z

    move-result v0

    .line 430
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_setCurveEnabled(IZ)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setProperty(JLandroid/os/Bundle;)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "propertyMap"    # Landroid/os/Bundle;

    .prologue
    .line 350
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 351
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_setProperty(JLandroid/os/Bundle;)Z

    move-result v0

    .line 353
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_setProperty(ILandroid/os/Bundle;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setSize(JF)Z
    .locals 1
    .param p1, "nativePen"    # J
    .param p3, "size"    # F

    .prologue
    .line 385
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mIs64:Z

    if-eqz v0, :cond_0

    .line 386
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_setSize(JF)Z

    move-result v0

    .line 388
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->native_setSize(IF)Z

    move-result v0

    goto :goto_0
.end method

.method private static native native_command(IILjava/util/ArrayList;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_command(JILjava/util/ArrayList;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_construct(I)Z
.end method

.method private static native native_construct(J)Z
.end method

.method private static native native_draw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_draw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_end(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_end(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_finalize(J)V
.end method

.method private static native native_getAdvancedSetting(I)Ljava/lang/String;
.end method

.method private static native native_getAdvancedSetting(J)Ljava/lang/String;
.end method

.method private static native native_getColor(I)I
.end method

.method private static native native_getColor(J)I
.end method

.method private static native native_getMaxSettingValue(I)F
.end method

.method private static native native_getMaxSettingValue(J)F
.end method

.method private static native native_getMinSettingValue(I)F
.end method

.method private static native native_getMinSettingValue(J)F
.end method

.method private static native native_getPenAttribute(II)Z
.end method

.method private static native native_getPenAttribute(JI)Z
.end method

.method private static native native_getProperty(ILandroid/os/Bundle;)Z
.end method

.method private static native native_getProperty(JLandroid/os/Bundle;)Z
.end method

.method private static native native_getSize(I)F
.end method

.method private static native native_getSize(J)F
.end method

.method private static native native_getStrokeRect(I[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;
.end method

.method private static native native_getStrokeRect(J[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;
.end method

.method private static native native_init()I
.end method

.method private static native native_init_64()J
.end method

.method private static native native_isCurveEnabled(I)Z
.end method

.method private static native native_isCurveEnabled(J)Z
.end method

.method private static native native_move(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_move(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_onLoad(I)V
.end method

.method private static native native_onLoad(J)V
.end method

.method private static native native_onUnload(I)V
.end method

.method private static native native_onUnload(J)V
.end method

.method private static native native_redraw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_redraw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_setAdvancedSetting(ILjava/lang/String;)Z
.end method

.method private static native native_setAdvancedSetting(JLjava/lang/String;)Z
.end method

.method private static native native_setBitmap(ILandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setBitmap(JLandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setColor(II)Z
.end method

.method private static native native_setColor(JI)Z
.end method

.method private static native native_setCurveEnabled(IZ)Z
.end method

.method private static native native_setCurveEnabled(JZ)Z
.end method

.method private static native native_setProperty(ILandroid/os/Bundle;)Z
.end method

.method private static native native_setProperty(JLandroid/os/Bundle;)Z
.end method

.method private static native native_setReferenceBitmap(ILandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setReferenceBitmap(JLandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setSize(IF)Z
.end method

.method private static native native_setSize(JF)Z
.end method

.method private static native native_start(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_start(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method


# virtual methods
.method public construct()V
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_construct(J)Z

    .line 43
    return-void
.end method

.method public draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v1, 0x0

    .line 115
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mBitmap:Landroid/graphics/Bitmap;

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v1, v1}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 126
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_draw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public getAdvancedSetting()Ljava/lang/String;
    .locals 2

    .prologue
    .line 282
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_getAdvancedSetting(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getColor()I
    .locals 2

    .prologue
    .line 242
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_getColor(J)I

    move-result v0

    return v0
.end method

.method public getMaxSettingValue()F
    .locals 2

    .prologue
    .line 222
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_getMaxSettingValue(J)F

    move-result v0

    return v0
.end method

.method public getMinSettingValue()F
    .locals 2

    .prologue
    .line 213
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_getMinSettingValue(J)F

    move-result v0

    return v0
.end method

.method public getNativeHandle()J
    .locals 2

    .prologue
    .line 106
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    return-wide v0
.end method

.method public getPenAttribute(I)Z
    .locals 2
    .param p1, "attribute"    # I

    .prologue
    .line 317
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_getPenAttribute(JI)Z

    move-result v0

    return v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "propertyMap"    # Landroid/os/Bundle;

    .prologue
    .line 97
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_getProperty(JLandroid/os/Bundle;)Z

    .line 98
    return-void
.end method

.method public getSize()F
    .locals 2

    .prologue
    .line 204
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_getSize(J)F

    move-result v0

    return v0
.end method

.method public getStrokeRect([Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;
    .locals 10
    .param p1, "points"    # [Landroid/graphics/PointF;
    .param p2, "pressures"    # [F
    .param p3, "timestamps"    # [I
    .param p4, "size"    # F
    .param p5, "isCurvable"    # Z
    .param p6, "advanced"    # Ljava/lang/String;

    .prologue
    .line 308
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move v7, p4

    move v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v1 .. v9}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_getStrokeRect(J[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public hideAdvancedSetting()V
    .locals 0

    .prologue
    .line 299
    return-void
.end method

.method public isCurveEnabled()Z
    .locals 2

    .prologue
    .line 262
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_isCurveEnabled(J)Z

    move-result v0

    return v0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_onLoad(J)V

    .line 52
    return-void
.end method

.method public onUnload()V
    .locals 2

    .prologue
    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mBitmap:Landroid/graphics/Bitmap;

    .line 79
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_onUnload(J)V

    .line 80
    return-void
.end method

.method public redrawPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 7
    .param p1, "allEvent"    # Landroid/view/MotionEvent;
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v1, 0x0

    .line 137
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mBitmap:Landroid/graphics/Bitmap;

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v1, v1}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 148
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_redraw(JLandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setAdvancedSetting(Ljava/lang/String;)V
    .locals 2
    .param p1, "advancedSetting"    # Ljava/lang/String;

    .prologue
    .line 271
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_setAdvancedSetting(JLjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 272
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 274
    :cond_0
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mBitmap:Landroid/graphics/Bitmap;

    .line 160
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_setBitmap(JLandroid/graphics/Bitmap;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 231
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_setColor(JI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 234
    :cond_0
    return-void
.end method

.method public setCurveEnabled(Z)V
    .locals 2
    .param p1, "curve"    # Z

    .prologue
    .line 251
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_setCurveEnabled(JZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 252
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 254
    :cond_0
    return-void
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "propertyMap"    # Landroid/os/Bundle;

    .prologue
    .line 88
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_setProperty(JLandroid/os/Bundle;)Z

    .line 89
    return-void
.end method

.method public setReferenceBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 185
    return-void
.end method

.method public setSize(F)V
    .locals 2
    .param p1, "size"    # F

    .prologue
    .line 193
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->nativeFountainPen:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/pen/preload/FountainPen;->Native_setSize(JF)Z

    move-result v0

    if-nez v0, :cond_0

    .line 194
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 196
    :cond_0
    return-void
.end method

.method public showAdvancedSetting(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 291
    return-void
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 69
    const/4 v0, 0x1

    return v0
.end method
