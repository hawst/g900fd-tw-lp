.class Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;
.super Landroid/view/View;
.source "SpenColorPickerLayout.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# static fields
.field private static final LL_BUILD_VERSION:I = 0x15

.field private static final TAG:Ljava/lang/String; = "settingui-colorPicker"


# instance fields
.field private drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private final localDisplayMetrics:Landroid/util/DisplayMetrics;

.field mColorPickerColorImage:Landroid/view/View;

.field mColorPickerCurrentColor:Landroid/view/View;

.field mColorPickerHandle:Landroid/view/View;

.field mColorPickerdExitBtn:Landroid/view/View;

.field private mCurrentColor:I

.field private final mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

.field private mIsRotated:Z

.field mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

.field private final mOldParentRect:Landroid/graphics/Rect;

.field private final mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

.field private final mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

.field private mParentRelativeLayout:Landroid/widget/RelativeLayout;

.field mSpoidExitListener:Landroid/view/View$OnTouchListener;

.field mSpoidSettingListener:Landroid/view/View$OnTouchListener;

.field mSpuitSettings:Landroid/view/View;

.field mSpuitdBG:Landroid/view/View;

.field private final mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

.field private mXDelta:I

.field private mYDelta:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;FII)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "canvasLayout"    # Landroid/widget/RelativeLayout;
    .param p3, "ratio"    # F
    .param p4, "x"    # I
    .param p5, "y"    # I

    .prologue
    const/4 v2, 0x0

    .line 53
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    .line 35
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mCurrentColor:I

    .line 40
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mIsRotated:Z

    .line 615
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    .line 644
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpoidSettingListener:Landroid/view/View$OnTouchListener;

    .line 726
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpoidExitListener:Landroid/view/View$OnTouchListener;

    .line 748
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    .line 757
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    .line 54
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 55
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string v1, ""

    invoke-direct {v0, p1, v1, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 56
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 58
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    .line 59
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-le v0, v1, :cond_0

    .line 60
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setLayoutDirection(I)V

    .line 62
    :cond_0
    invoke-direct {p0, p4, p5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->initSpuitSetting(II)V

    .line 63
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mOldParentRect:Landroid/graphics/Rect;

    .line 64
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mIsRotated:Z

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;Z)V
    .locals 0

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mIsRotated:Z

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)V
    .locals 0

    .prologue
    .line 464
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->checkPosition()V

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;I)V
    .locals 0

    .prologue
    .line 33
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mXDelta:I

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;I)V
    .locals 0

    .prologue
    .line 34
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mYDelta:I

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mXDelta:I

    return v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mYDelta:I

    return v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    return-object v0
.end method

.method private bodyLayout()Landroid/view/ViewGroup;
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 282
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 284
    .local v0, "bodyLayout":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 286
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->spuitdBg()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitdBG:Landroid/view/View;

    .line 287
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitdBG:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 288
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitdBG:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 289
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->spuitdHandle()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerHandle:Landroid/view/View;

    .line 290
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->spuitExitBtn()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerdExitBtn:Landroid/view/View;

    .line 291
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->spuitColorImage()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerColorImage:Landroid/view/View;

    .line 292
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->spuitCurrentColor()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerCurrentColor:Landroid/view/View;

    .line 294
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 295
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 296
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 297
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 298
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 299
    return-object v0
.end method

.method private checkPosition()V
    .locals 14

    .prologue
    .line 465
    const/4 v11, 0x2

    new-array v1, v11, [I

    .line 467
    .local v1, "location":[I
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v12, 0x42d40000    # 106.0f

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 468
    .local v3, "minWidth":I
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v12, 0x42140000    # 37.0f

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 470
    .local v2, "minHeight":I
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v11, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 472
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v4

    .line 474
    .local v4, "parentRect":Landroid/graphics/Rect;
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerHandle:Landroid/view/View;

    .line 475
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    .line 474
    check-cast v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 477
    .local v8, "spoidSettinghandleParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget v0, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 478
    .local v0, "lMargin":I
    iget v10, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 480
    .local v10, "tMargin":I
    const/4 v11, 0x0

    aget v11, v1, v11

    iget v12, v4, Landroid/graphics/Rect;->left:I

    if-ge v11, v12, :cond_0

    .line 481
    const/4 v11, 0x0

    iput v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 483
    :cond_0
    const/4 v11, 0x1

    aget v11, v1, v11

    iget v12, v4, Landroid/graphics/Rect;->top:I

    if-ge v11, v12, :cond_1

    .line 484
    const/4 v11, 0x0

    iput v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 487
    :cond_1
    iget v11, v4, Landroid/graphics/Rect;->right:I

    const/4 v12, 0x0

    aget v12, v1, v12

    sub-int/2addr v11, v12

    if-ge v11, v3, :cond_2

    .line 488
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v11

    sub-int/2addr v11, v3

    iput v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 490
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-gez v11, :cond_2

    .line 491
    const/4 v11, 0x0

    iput v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 494
    :cond_2
    iget v11, v4, Landroid/graphics/Rect;->bottom:I

    const/4 v12, 0x1

    aget v12, v1, v12

    sub-int/2addr v11, v12

    if-ge v11, v2, :cond_3

    .line 495
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v11

    sub-int/2addr v11, v2

    iput v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 497
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    if-gez v11, :cond_3

    .line 498
    const/4 v11, 0x0

    iput v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 502
    :cond_3
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-ne v0, v11, :cond_4

    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    if-ne v10, v11, :cond_4

    .line 537
    :goto_0
    return-void

    .line 506
    :cond_4
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 507
    .local v7, "spoidSettingBgParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerColorImage:Landroid/view/View;

    .line 508
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 507
    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 509
    .local v5, "spoidColorImageParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerdExitBtn:Landroid/view/View;

    .line 510
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    .line 509
    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 511
    .local v6, "spoidExitBtnParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerCurrentColor:Landroid/view/View;

    .line 512
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    .line 511
    check-cast v9, Landroid/widget/RelativeLayout$LayoutParams;

    .line 514
    .local v9, "spuitCurrentColorParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 515
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v13, 0x41b00000    # 22.0f

    invoke-virtual {v12, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v12

    add-int/2addr v11, v12

    .line 514
    iput v11, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 516
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v11, v7, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 518
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 519
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v13, 0x41c80000    # 25.0f

    invoke-virtual {v12, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v12

    add-int/2addr v11, v12

    .line 518
    iput v11, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 520
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 521
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v13, 0x40e00000    # 7.0f

    invoke-virtual {v12, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v12

    add-int/2addr v11, v12

    .line 520
    iput v11, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 523
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 524
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v13, 0x42480000    # 50.0f

    invoke-virtual {v12, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v12

    add-int/2addr v11, v12

    .line 523
    iput v11, v9, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 525
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 526
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v13, 0x40e00000    # 7.0f

    invoke-virtual {v12, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v12

    add-int/2addr v11, v12

    .line 525
    iput v11, v9, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 528
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 529
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v13, 0x429c0000    # 78.0f

    invoke-virtual {v12, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v12

    add-int/2addr v11, v12

    .line 528
    iput v11, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 530
    iget v11, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v11, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 532
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v11, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 533
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v11, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 534
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v11, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 535
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v11, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 536
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v11, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0
.end method

.method private getMovableRect()Landroid/graphics/Rect;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 540
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 541
    .local v0, "location":[I
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 543
    .local v1, "r":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getLocationOnScreen([I)V

    .line 545
    aget v2, v0, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 546
    aget v2, v0, v4

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 547
    aget v2, v0, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 548
    aget v2, v0, v4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 550
    return-object v1
.end method

.method private initSpuitSetting(II)V
    .locals 7
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/16 v6, 0x8

    const/4 v5, -0x2

    const/4 v4, 0x0

    .line 242
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->totalLayout()Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    .line 243
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 245
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerHandle:Landroid/view/View;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpoidSettingListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 247
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 250
    .local v1, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 253
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_0

    .line 254
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v2, v3, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 256
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    :cond_0
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->movePosition(II)V

    .line 262
    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->setVisibility(I)V

    .line 264
    return-void

    .line 258
    :catch_0
    move-exception v0

    .line 259
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method private setCanvasSize()V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 186
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->invalidate()V

    .line 188
    :cond_0
    return-void
.end method

.method private spuitColorImage()Landroid/view/View;
    .locals 8

    .prologue
    const/16 v5, 0x16

    const/4 v4, 0x0

    const/high16 v3, 0x41b00000    # 22.0f

    .line 415
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 418
    .local v1, "mSpuitColorView":Landroid/widget/ImageView;
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 419
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 418
    invoke-direct {v7, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 420
    .local v7, "mSpoidColorViewParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 421
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40e00000    # 7.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 420
    invoke-virtual {v7, v0, v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 422
    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 425
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v2, "snote_toolbar_icon_spoid_hover"

    .line 426
    const-string/jumbo v3, "snote_color_spoid_press"

    const-string/jumbo v4, "snote_color_spoid_focus"

    move v6, v5

    .line 425
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 428
    return-object v1
.end method

.method private spuitCurrentColor()Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x41b00000    # 22.0f

    .line 439
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 443
    .local v0, "mSpuitColorFirst":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 444
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 443
    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 445
    .local v1, "mSpuitColorFirstParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42480000    # 50.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 446
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40e00000    # 7.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 445
    invoke-virtual {v1, v3, v4, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 447
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 452
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v4, "snote_colorchip_shadow"

    invoke-virtual {v3, v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 454
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 455
    .local v2, "mSpuitCurrentColor":Landroid/widget/LinearLayout;
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 456
    const/high16 v3, -0x10000

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 457
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 461
    return-object v2
.end method

.method private spuitExitBtn()Landroid/view/View;
    .locals 14

    .prologue
    .line 354
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 356
    .local v1, "spuitExitLayout":Landroid/widget/RelativeLayout;
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 357
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41e00000    # 28.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42140000    # 37.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 356
    invoke-direct {v10, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 358
    .local v10, "spuitExitLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v0, v2, :cond_2

    .line 359
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x40800000    # 4.0f

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    float-to-double v2, v0

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    cmpg-double v0, v2, v4

    if-gez v0, :cond_1

    .line 360
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v2, "snote_toolbar_bg_right_normal_2"

    .line 361
    const-string/jumbo v3, "snote_toolbar_bg_right_selectedl_2"

    const-string/jumbo v4, "snote_toolbar_bg_right_focus_2"

    .line 360
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    :goto_0
    new-instance v8, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v8, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 381
    .local v8, "exitButton":Landroid/widget/ImageButton;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v2, "snote_popup_icon_close_2"

    const/16 v3, 0x1a

    const/16 v4, 0x1a

    invoke-virtual {v0, v8, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;II)V

    .line 383
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    .line 384
    const/4 v0, -0x2

    .line 385
    const/4 v2, -0x2

    .line 383
    invoke-direct {v9, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 386
    .local v9, "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    float-to-double v2, v0

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    cmpg-double v0, v2, v4

    if-gez v0, :cond_5

    .line 387
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 388
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 387
    invoke-virtual {v9, v0, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 394
    :goto_1
    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 395
    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 396
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_close"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 397
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpoidExitListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v8, v0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 398
    const/4 v0, 0x1

    iput-boolean v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 399
    const/16 v0, 0x9

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 400
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x429c0000    # 78.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v10, v0, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 401
    invoke-virtual {v1, v10}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 402
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 403
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_close"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 404
    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 405
    return-object v1

    .line 363
    .end local v8    # "exitButton":Landroid/widget/ImageButton;
    .end local v9    # "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v2, "snote_toolbar_bg_right_normal"

    .line 364
    const-string/jumbo v3, "snote_toolbar_bg_right_selectedl"

    const-string/jumbo v4, "snote_toolbar_bg_right_focus"

    const/16 v5, 0x1c

    const/16 v6, 0x26

    .line 363
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    .line 367
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x40800000    # 4.0f

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    float-to-double v2, v0

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    cmpg-double v0, v2, v4

    if-gez v0, :cond_4

    .line 368
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 369
    const-string/jumbo v2, "snote_toolbar_bg_right_normal_2"

    .line 370
    const-string/jumbo v3, "snote_toolbar_bg_right_selectedl_2"

    const-string/jumbo v4, "snote_toolbar_bg_right_focus_2"

    .line 371
    const/16 v5, 0x3d

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v11, 0x0

    invoke-static {v5, v6, v7, v11}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    .line 369
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 373
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v2, "snote_toolbar_bg_right_normal"

    .line 374
    const-string/jumbo v3, "snote_toolbar_bg_right_selectedl"

    const-string/jumbo v4, "snote_toolbar_bg_right_focus"

    const/16 v5, 0x1c

    const/16 v6, 0x26

    .line 375
    const/16 v7, 0x3d

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v7, v11, v12, v13}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    .line 373
    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    goto/16 :goto_0

    .line 390
    .restart local v8    # "exitButton":Landroid/widget/ImageButton;
    .restart local v9    # "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 391
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40800000    # 4.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 390
    invoke-virtual {v9, v0, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto/16 :goto_1
.end method

.method private spuitdBg()Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 303
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 305
    .local v0, "spoidBgButton":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 306
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42600000    # 56.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42140000    # 37.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 305
    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 307
    .local v1, "spoidBgParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 308
    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 309
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41b00000    # 22.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2, v5, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 310
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 313
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_gradation"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 314
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 318
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    const/high16 v3, 0x40800000    # 4.0f

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    float-to-double v2, v2

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    cmpg-double v2, v2, v4

    if-gez v2, :cond_1

    .line 319
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v3, "snote_toolbar_bg_center_normal_2"

    invoke-virtual {v2, v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 326
    :goto_0
    return-object v0

    .line 321
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v3, "snote_toolbar_bg_center2_normal"

    const/16 v4, 0x38

    const/16 v5, 0x26

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;II)V

    goto :goto_0
.end method

.method private spuitdHandle()Landroid/view/View;
    .locals 6

    .prologue
    .line 330
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 332
    .local v0, "spoidHandle":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 333
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41b00000    # 22.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42140000    # 37.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 332
    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 334
    .local v1, "spoidHandleParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 335
    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 337
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 338
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 339
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    const/high16 v3, 0x40800000    # 4.0f

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    float-to-double v2, v2

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    cmpg-double v2, v2, v4

    if-gez v2, :cond_1

    .line 340
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v3, "snote_toolbar_handle_2"

    invoke-virtual {v2, v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 345
    :goto_0
    return-object v0

    .line 342
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v3, "snote_toolbar_handle"

    const/16 v4, 0x16

    const/16 v5, 0x25

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;II)V

    goto :goto_0
.end method

.method private totalLayout()Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 273
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 275
    .local v0, "totalLayout":Landroid/widget/LinearLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 277
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->bodyLayout()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 278
    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 166
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 167
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    .line 168
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 169
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitdBG:Landroid/view/View;

    .line 170
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 171
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerHandle:Landroid/view/View;

    .line 172
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 173
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerdExitBtn:Landroid/view/View;

    .line 174
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 175
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerColorImage:Landroid/view/View;

    .line 176
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 177
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerCurrentColor:Landroid/view/View;

    .line 178
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    .line 179
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 180
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 181
    return-void
.end method

.method public getColorPickerCurrentColor()I
    .locals 1

    .prologue
    .line 228
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mCurrentColor:I

    return v0
.end method

.method public getColorPickerSettingVisible()I
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    return v0
.end method

.method public hide()V
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 220
    return-void
.end method

.method movePosition(II)V
    .locals 11
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v8, 0x0

    const/high16 v10, 0x40e00000    # 7.0f

    .line 554
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    if-nez v7, :cond_0

    .line 613
    :goto_0
    return-void

    .line 558
    :cond_0
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerHandle:Landroid/view/View;

    .line 559
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 558
    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 560
    .local v5, "spoidSettinghandleParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 561
    .local v4, "spoidSettingBgParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerColorImage:Landroid/view/View;

    .line 562
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 561
    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 563
    .local v2, "spoidColorImageParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerdExitBtn:Landroid/view/View;

    .line 564
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 563
    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 565
    .local v3, "spoidExitBtnParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerCurrentColor:Landroid/view/View;

    .line 566
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    .line 565
    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 568
    .local v6, "spuitCurrentColorParams":Landroid/widget/RelativeLayout$LayoutParams;
    iput p1, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 569
    iput p2, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 571
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-gez v7, :cond_1

    .line 572
    iput v8, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 574
    :cond_1
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    if-gez v7, :cond_2

    .line 575
    iput v8, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 578
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x42d40000    # 106.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 579
    .local v1, "minWidth":I
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x42140000    # 37.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 581
    .local v0, "minHeight":I
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v7

    if-lez v7, :cond_3

    .line 582
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v8

    sub-int/2addr v8, v1

    if-le v7, v8, :cond_3

    .line 583
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v7

    sub-int/2addr v7, v1

    iput v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 585
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v7

    if-lez v7, :cond_4

    .line 586
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v8

    sub-int/2addr v8, v0

    if-le v7, v8, :cond_4

    .line 587
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v7

    sub-int/2addr v7, v0

    iput v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 590
    :cond_4
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 591
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x41b00000    # 22.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    add-int/2addr v7, v8

    .line 590
    iput v7, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 592
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v7, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 594
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 595
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x41c80000    # 25.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    add-int/2addr v7, v8

    .line 594
    iput v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 596
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 597
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v8, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    add-int/2addr v7, v8

    .line 596
    iput v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 599
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 600
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x42480000    # 50.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    add-int/2addr v7, v8

    .line 599
    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 601
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 602
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v8, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    add-int/2addr v7, v8

    .line 601
    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 604
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 605
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x429c0000    # 78.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    add-int/2addr v7, v8

    .line 604
    iput v7, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 606
    iget v7, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v7, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 608
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v7, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 609
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v7, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 610
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v7, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 611
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v7, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 612
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v7, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0
.end method

.method protected rotatePosition()V
    .locals 20

    .prologue
    .line 73
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerHandle:Landroid/view/View;

    if-nez v15, :cond_1

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mIsRotated:Z

    if-eqz v15, :cond_0

    .line 81
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerHandle:Landroid/view/View;

    .line 82
    invoke-virtual {v15}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    .line 81
    check-cast v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 83
    .local v11, "spoidSettinghandleParams":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v15}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 84
    .local v10, "spoidSettingBgParams":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerColorImage:Landroid/view/View;

    .line 85
    invoke-virtual {v15}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    .line 84
    check-cast v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 86
    .local v8, "spoidColorImageParams":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerdExitBtn:Landroid/view/View;

    .line 87
    invoke-virtual {v15}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    .line 86
    check-cast v9, Landroid/widget/RelativeLayout$LayoutParams;

    .line 88
    .local v9, "spoidExitBtnParams":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerCurrentColor:Landroid/view/View;

    .line 89
    invoke-virtual {v15}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    .line 88
    check-cast v12, Landroid/widget/RelativeLayout$LayoutParams;

    .line 91
    .local v12, "spuitCurrentColorParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v5

    .line 93
    .local v5, "newParentRect":Landroid/graphics/Rect;
    const-string/jumbo v15, "settingui-colorPicker"

    const-string v16, "==== colorPicker ===="

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    const-string/jumbo v15, "settingui-colorPicker"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "old  = "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mOldParentRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mOldParentRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mOldParentRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 95
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mOldParentRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 94
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const-string/jumbo v15, "settingui-colorPicker"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "new  = "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v5, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget v0, v5, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget v0, v5, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 97
    iget v0, v5, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 96
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 100
    .local v6, "r":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mOldParentRect:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->left:I

    iget v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    move/from16 v16, v0

    add-int v15, v15, v16

    iput v15, v6, Landroid/graphics/Rect;->left:I

    .line 101
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mOldParentRect:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->top:I

    iget v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    move/from16 v16, v0

    add-int v15, v15, v16

    iput v15, v6, Landroid/graphics/Rect;->top:I

    .line 102
    iget v15, v6, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x42d40000    # 106.0f

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    add-int v15, v15, v16

    iput v15, v6, Landroid/graphics/Rect;->right:I

    .line 103
    iget v15, v6, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x42140000    # 37.0f

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    add-int v15, v15, v16

    iput v15, v6, Landroid/graphics/Rect;->bottom:I

    .line 105
    const-string/jumbo v15, "settingui-colorPicker"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string/jumbo v17, "view = "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v6, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget v0, v6, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget v0, v6, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    iget v15, v6, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mOldParentRect:Landroid/graphics/Rect;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v16, v0

    sub-int v15, v15, v16

    int-to-float v4, v15

    .line 108
    .local v4, "left":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mOldParentRect:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->right:I

    iget v0, v6, Landroid/graphics/Rect;->right:I

    move/from16 v16, v0

    sub-int v15, v15, v16

    int-to-float v7, v15

    .line 109
    .local v7, "right":F
    iget v15, v6, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mOldParentRect:Landroid/graphics/Rect;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v16, v0

    sub-int v15, v15, v16

    int-to-float v13, v15

    .line 110
    .local v13, "top":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mOldParentRect:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->bottom:I

    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    move/from16 v16, v0

    sub-int v15, v15, v16

    int-to-float v2, v15

    .line 112
    .local v2, "bottom":F
    add-float v15, v4, v7

    div-float v3, v4, v15

    .line 113
    .local v3, "hRatio":F
    add-float v15, v13, v2

    div-float v14, v13, v15

    .line 115
    .local v14, "vRatio":F
    const-string/jumbo v15, "settingui-colorPicker"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "left :"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", right :"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    const-string/jumbo v15, "settingui-colorPicker"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string/jumbo v17, "top :"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", bottom :"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    const-string/jumbo v15, "settingui-colorPicker"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "hRatio = "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", vRatio = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    float-to-double v0, v3

    move-wide/from16 v16, v0

    const-wide v18, 0x3fefae147ae147aeL    # 0.99

    cmpl-double v15, v16, v18

    if-lez v15, :cond_4

    .line 120
    const/high16 v3, 0x3f800000    # 1.0f

    .line 125
    :cond_2
    :goto_1
    float-to-double v0, v14

    move-wide/from16 v16, v0

    const-wide v18, 0x3fefae147ae147aeL    # 0.99

    cmpl-double v15, v16, v18

    if-lez v15, :cond_5

    .line 126
    const/high16 v14, 0x3f800000    # 1.0f

    .line 131
    :cond_3
    :goto_2
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v15

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v16

    sub-int v15, v15, v16

    int-to-float v15, v15

    mul-float/2addr v15, v3

    invoke-static {v15}, Ljava/lang/Math;->round(F)I

    move-result v15

    iput v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 132
    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v15

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v16

    sub-int v15, v15, v16

    int-to-float v15, v15

    mul-float/2addr v15, v14

    invoke-static {v15}, Ljava/lang/Math;->round(F)I

    move-result v15

    iput v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 134
    const-string/jumbo v15, "settingui-colorPicker"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "lMargin = "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", tMargin = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 135
    iget v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 134
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    iget v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 138
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x41b00000    # 22.0f

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    add-int v15, v15, v16

    .line 137
    iput v15, v10, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 139
    iget v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v15, v10, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 141
    iget v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 142
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x41c80000    # 25.0f

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    add-int v15, v15, v16

    .line 141
    iput v15, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 143
    iget v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x40e00000    # 7.0f

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    add-int v15, v15, v16

    .line 143
    iput v15, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 146
    iget v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 147
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x42480000    # 50.0f

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    add-int v15, v15, v16

    .line 146
    iput v15, v12, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 148
    iget v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 149
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x40e00000    # 7.0f

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    add-int v15, v15, v16

    .line 148
    iput v15, v12, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 151
    iget v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 152
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v16, v0

    const/high16 v17, 0x429c0000    # 78.0f

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v16

    add-int v15, v15, v16

    .line 151
    iput v15, v9, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 153
    iget v15, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v15, v9, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 155
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v15, v11}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 156
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v15, v10}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 157
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v15, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 158
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v15, v12}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 159
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v15, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 121
    :cond_4
    const/4 v15, 0x0

    cmpg-float v15, v3, v15

    if-gez v15, :cond_2

    .line 122
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 127
    :cond_5
    const/4 v15, 0x0

    cmpg-float v15, v14, v15

    if-gez v15, :cond_3

    .line 128
    const/4 v14, 0x0

    goto/16 :goto_2
.end method

.method public setColorPickerColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 198
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mCurrentColor:I

    .line 199
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerCurrentColor:Landroid/view/View;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mCurrentColor:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 200
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerCurrentColor:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 202
    return-void
.end method

.method protected setRotation()V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mOldParentRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mIsRotated:Z

    .line 69
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mLoaded:Z

    if-nez v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->loadImage()V

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 211
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->setCanvasSize()V

    .line 212
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 213
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->checkPosition()V

    .line 214
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 216
    return-void
.end method
