.class Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;
.super Landroid/widget/RelativeLayout;
.source "SPenSeekBarView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;
    }
.end annotation


# static fields
.field private static final LL_VERSION_CODE:I = 0x15

.field private static final REP_DELAY:I = 0x14

.field protected static final SEEKBAR_LAYOUT_HEIGHT:I = 0x32

.field protected static final SPEN_SEEKBAR_TYPE_ALPHA:I = 0x0

.field protected static final SPEN_SEEKBAR_TYPE_CURSIVE:I = 0x2

.field protected static final SPEN_SEEKBAR_TYPE_DUMMY:I = 0x4

.field protected static final SPEN_SEEKBAR_TYPE_MODULATION:I = 0x6

.field protected static final SPEN_SEEKBAR_TYPE_SIZE:I = -0x1

.field protected static final SPEN_SEEKBAR_TYPE_SUSTENANCE:I = 0x3

.field private static final handelFocusPath:Ljava/lang/String;

.field private static final handelPath:Ljava/lang/String;

.field private static final handelPressPath:Ljava/lang/String;

.field private static final lineDivider:Ljava/lang/String;

.field protected static mDefaultPath:Ljava/lang/String;

.field private static final minusBgDimPath:Ljava/lang/String;

.field private static final minusBgFocusPath:Ljava/lang/String;

.field private static final minusBgPath:Ljava/lang/String;

.field private static final minusBgPressPath:Ljava/lang/String;

.field private static final plusBgDimPath:Ljava/lang/String;

.field private static final plusBgFocusPath:Ljava/lang/String;

.field private static final plusBgPath:Ljava/lang/String;

.field private static final plusBgPressPath:Ljava/lang/String;

.field private static final progressShadowPath:Ljava/lang/String;


# instance fields
.field private bgDrawable:Landroid/graphics/drawable/Drawable;

.field private final localDisplayMetrics:Landroid/util/DisplayMetrics;

.field private mAutoDecrement:Z

.field private mAutoIncrement:Z

.field protected mContext:Landroid/content/Context;

.field private final mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private final mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

.field private mMinusButton:Landroid/widget/ImageButton;

.field private final mMinusButtonClickListener:Landroid/view/View$OnClickListener;

.field private final mMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mMinusButtonOnKeyListener:Landroid/view/View$OnKeyListener;

.field private final mMinusButtonOnTouchListener:Landroid/view/View$OnTouchListener;

.field private mPenSeekbarTextView:Landroid/widget/TextView;

.field private mPlusButton:Landroid/widget/ImageButton;

.field private final mPlusButtonClickListener:Landroid/view/View$OnClickListener;

.field private final mPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mPlusButtonOnKeyListener:Landroid/view/View$OnKeyListener;

.field private final mPlusButtonOnTouchListener:Landroid/view/View$OnTouchListener;

.field private final mRptRunnable:Ljava/lang/Runnable;

.field private mSPenSeekBarChangeListner:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;

.field private final mSdkVersion:I

.field protected mSeekBar:Landroid/widget/SeekBar;

.field private final mSeekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mSeekBarColor:Landroid/graphics/drawable/GradientDrawable;

.field private final mSeekbarKeyListner:Landroid/view/View$OnKeyListener;

.field private mSeekbarType:I

.field protected mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

.field private progressBgPath:Ljava/lang/String;

.field private final repeatUpdateHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 587
    const-string v0, ""

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    .line 588
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_line"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->lineDivider:Ljava/lang/String;

    .line 589
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_plus_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgPath:Ljava/lang/String;

    .line 590
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_plus_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgPressPath:Ljava/lang/String;

    .line 591
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_plus_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgFocusPath:Ljava/lang/String;

    .line 592
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_plus_dim"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgDimPath:Ljava/lang/String;

    .line 594
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_minus_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgPath:Ljava/lang/String;

    .line 595
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_minus_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgPressPath:Ljava/lang/String;

    .line 596
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_minus_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgFocusPath:Ljava/lang/String;

    .line 597
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_minus_dim"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgDimPath:Ljava/lang/String;

    .line 598
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "progress_handle_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->handelPath:Ljava/lang/String;

    .line 599
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "progress_handle_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->handelPressPath:Ljava/lang/String;

    .line 600
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "progress_handle_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->handelFocusPath:Ljava/lang/String;

    .line 602
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "progress_shadow"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->progressShadowPath:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;FLcom/samsung/android/sdk/pen/settingui/SpenImageLoader;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ratio"    # F
    .param p3, "imageLoader"    # Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;
    .param p4, "type"    # I

    .prologue
    const/4 v1, 0x0

    .line 67
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 37
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSdkVersion:I

    .line 49
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarType:I

    .line 52
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoDecrement:Z

    .line 53
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoIncrement:Z

    .line 63
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->repeatUpdateHandler:Landroid/os/Handler;

    .line 384
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 435
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonClickListener:Landroid/view/View$OnClickListener;

    .line 443
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonClickListener:Landroid/view/View$OnClickListener;

    .line 451
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 462
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 473
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 492
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 511
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonOnKeyListener:Landroid/view/View$OnKeyListener;

    .line 528
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonOnKeyListener:Landroid/view/View$OnKeyListener;

    .line 545
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarKeyListner:Landroid/view/View$OnKeyListener;

    .line 573
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mRptRunnable:Ljava/lang/Runnable;

    .line 601
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "progress_bg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->progressBgPath:Ljava/lang/String;

    .line 68
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    .line 69
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    const-string v2, ""

    invoke-direct {v0, v1, v2, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 70
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 71
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 72
    iput p4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarType:I

    .line 73
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 75
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->initView()V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;FLcom/samsung/android/sdk/pen/settingui/SpenImageLoader;ILjava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ratio"    # F
    .param p3, "imageLoader"    # Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;
    .param p4, "type"    # I
    .param p5, "bgProgressPath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 79
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 37
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSdkVersion:I

    .line 49
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarType:I

    .line 52
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoDecrement:Z

    .line 53
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoIncrement:Z

    .line 63
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->repeatUpdateHandler:Landroid/os/Handler;

    .line 384
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 435
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonClickListener:Landroid/view/View$OnClickListener;

    .line 443
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonClickListener:Landroid/view/View$OnClickListener;

    .line 451
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 462
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 473
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 492
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 511
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonOnKeyListener:Landroid/view/View$OnKeyListener;

    .line 528
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonOnKeyListener:Landroid/view/View$OnKeyListener;

    .line 545
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarKeyListner:Landroid/view/View$OnKeyListener;

    .line 573
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mRptRunnable:Ljava/lang/Runnable;

    .line 601
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "progress_bg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->progressBgPath:Ljava/lang/String;

    .line 80
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    .line 81
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    const-string v2, ""

    invoke-direct {v0, v1, v2, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 82
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 83
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 84
    iput p4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarType:I

    .line 85
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 86
    iput-object p5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->progressBgPath:Ljava/lang/String;

    .line 88
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->initView()V

    .line 89
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/SeekBar$OnSeekBarChangeListener;
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSPenSeekBarChangeListner:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;Z)V
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoDecrement:Z

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarType:I

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoDecrement:Z

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoIncrement:Z

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;Z)V
    .locals 0

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoIncrement:Z

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->repeatUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 573
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mRptRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method private penSeekbar(Landroid/graphics/drawable/GradientDrawable;)Landroid/widget/SeekBar;
    .locals 19
    .param p1, "mPenSeekbarColor"    # Landroid/graphics/drawable/GradientDrawable;

    .prologue
    .line 265
    new-instance v15, Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    invoke-direct {v15, v5}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 266
    .local v15, "seekBar":Landroid/widget/SeekBar;
    new-instance v16, Landroid/widget/RelativeLayout$LayoutParams;

    .line 267
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x43440000    # 196.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    add-int/lit8 v5, v5, -0xc

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41b00000    # 22.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 266
    move-object/from16 v0, v16

    invoke-direct {v0, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 268
    .local v16, "seekBarParams":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    const/high16 v6, 0x40000000    # 2.0f

    cmpl-float v5, v5, v6

    if-nez v5, :cond_1

    .line 270
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41c00000    # 24.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 271
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41980000    # 19.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    .line 272
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41c00000    # 24.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    const/4 v8, 0x0

    .line 270
    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 278
    :goto_0
    const/4 v5, 0x1

    move-object/from16 v0, v16

    iput-boolean v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 280
    invoke-virtual/range {v15 .. v16}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 281
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x40000000    # 2.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    const/4 v6, 0x0

    .line 282
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x40000000    # 2.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    const/4 v8, 0x0

    .line 281
    invoke-virtual {v15, v5, v6, v7, v8}, Landroid/widget/SeekBar;->setPadding(IIII)V

    .line 283
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSdkVersion:I

    const/16 v6, 0x15

    if-lt v5, v6, :cond_2

    .line 284
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->handelPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->handelPath:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->handelFocusPath:Ljava/lang/String;

    const/16 v6, 0x16

    const/16 v7, 0x16

    const/4 v8, 0x1

    invoke-virtual/range {v2 .. v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v5

    invoke-virtual {v15, v5}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 290
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x40400000    # 3.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-virtual {v15, v5}, Landroid/widget/SeekBar;->setThumbOffset(I)V

    .line 292
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 293
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 294
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x40900000    # 4.5f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    int-to-float v5, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 296
    new-instance v10, Landroid/graphics/drawable/ClipDrawable;

    const/4 v5, 0x3

    const/4 v6, 0x1

    move-object/from16 v0, p1

    invoke-direct {v10, v0, v5, v6}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    .line 297
    .local v10, "localClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->progressBgPath:Ljava/lang/String;

    const/16 v7, 0xbe

    const/16 v8, 0x9

    invoke-virtual {v5, v6, v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->bgDrawable:Landroid/graphics/drawable/Drawable;

    .line 299
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    const/high16 v6, 0x3fc00000    # 1.5f

    cmpg-float v5, v5, v6

    if-gez v5, :cond_3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41b00000    # 22.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    rem-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_3

    .line 300
    new-instance v2, Landroid/graphics/drawable/InsetDrawable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->bgDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 306
    .local v2, "bgInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    :goto_2
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarType:I

    if-lez v5, :cond_4

    .line 307
    new-instance v11, Landroid/graphics/drawable/LayerDrawable;

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/graphics/drawable/Drawable;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v6, 0x1

    aput-object v10, v5, v6

    invoke-direct {v11, v5}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 308
    .local v11, "localLayerDrawable":Landroid/graphics/drawable/LayerDrawable;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41200000    # 10.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    const/4 v6, 0x0

    .line 309
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41200000    # 10.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    const/4 v8, 0x0

    .line 308
    invoke-virtual {v15, v5, v6, v7, v8}, Landroid/widget/SeekBar;->setPadding(IIII)V

    .line 318
    :goto_3
    :try_start_0
    const-class v5, Landroid/widget/ProgressBar;

    const-string v6, "mMinHeight"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v14

    .line 319
    .local v14, "minHeight":Ljava/lang/reflect/Field;
    const/4 v5, 0x1

    invoke-virtual {v14, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 320
    const-class v5, Landroid/widget/ProgressBar;

    const-string v6, "mMaxHeight"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v12

    .line 321
    .local v12, "maxHeight":Ljava/lang/reflect/Field;
    const/4 v5, 0x1

    invoke-virtual {v12, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1

    .line 324
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    const/high16 v6, 0x3fc00000    # 1.5f

    cmpg-float v5, v5, v6

    if-gez v5, :cond_5

    .line 325
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41200000    # 10.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-virtual {v14, v15, v5}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 326
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41200000    # 10.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-virtual {v12, v15, v5}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_1

    .line 340
    .end local v12    # "maxHeight":Ljava/lang/reflect/Field;
    .end local v14    # "minHeight":Ljava/lang/reflect/Field;
    :goto_4
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSdkVersion:I

    const/16 v6, 0x15

    if-lt v5, v6, :cond_0

    .line 342
    :try_start_2
    const-class v5, Landroid/widget/AbsSeekBar;

    const-string v6, "setSplitTrack"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    sget-object v17, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v17, v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_4

    move-result-object v13

    .line 345
    .local v13, "method":Ljava/lang/reflect/Method;
    const/4 v5, 0x1

    :try_start_3
    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v13, v15, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_4

    .line 358
    .end local v13    # "method":Ljava/lang/reflect/Method;
    :goto_5
    new-instance v5, Landroid/graphics/drawable/RippleDrawable;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v7, 0x40

    const/4 v8, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v7, v8, v0, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v6

    const/4 v7, 0x0

    .line 359
    const/4 v8, 0x0

    invoke-direct {v5, v6, v7, v8}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 358
    invoke-virtual {v15, v5}, Landroid/widget/SeekBar;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 362
    :cond_0
    invoke-virtual {v15, v11}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 363
    new-instance v5, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$13;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$13;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;Landroid/widget/SeekBar;)V

    invoke-virtual {v15, v5}, Landroid/widget/SeekBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 381
    return-object v15

    .line 274
    .end local v2    # "bgInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    .end local v10    # "localClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    .end local v11    # "localLayerDrawable":Landroid/graphics/drawable/LayerDrawable;
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41c00000    # 24.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 275
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41980000    # 19.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41c00000    # 24.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    const/4 v8, 0x0

    .line 274
    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto/16 :goto_0

    .line 286
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->handelPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->handelPressPath:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->handelFocusPath:Ljava/lang/String;

    const/16 v6, 0x16

    const/16 v7, 0x16

    .line 287
    const/4 v8, 0x1

    .line 286
    invoke-virtual/range {v2 .. v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v5

    invoke-virtual {v15, v5}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 302
    .restart local v10    # "localClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    :cond_3
    new-instance v2, Landroid/graphics/drawable/InsetDrawable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->bgDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .restart local v2    # "bgInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    goto/16 :goto_2

    .line 311
    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->progressShadowPath:Ljava/lang/String;

    const/16 v7, 0xbe

    const/16 v8, 0x9

    invoke-virtual {v5, v6, v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 312
    .local v4, "shadowDrawable":Landroid/graphics/drawable/Drawable;
    new-instance v3, Landroid/graphics/drawable/InsetDrawable;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v8}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 313
    .local v3, "shadowInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    new-instance v11, Landroid/graphics/drawable/LayerDrawable;

    const/4 v5, 0x3

    new-array v5, v5, [Landroid/graphics/drawable/Drawable;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v6, 0x1

    aput-object v10, v5, v6

    const/4 v6, 0x2

    .line 314
    aput-object v3, v5, v6

    .line 313
    invoke-direct {v11, v5}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .restart local v11    # "localLayerDrawable":Landroid/graphics/drawable/LayerDrawable;
    goto/16 :goto_3

    .line 328
    .end local v3    # "shadowInsetDrawable":Landroid/graphics/drawable/InsetDrawable;
    .end local v4    # "shadowDrawable":Landroid/graphics/drawable/Drawable;
    .restart local v12    # "maxHeight":Ljava/lang/reflect/Field;
    .restart local v14    # "minHeight":Ljava/lang/reflect/Field;
    :cond_5
    :try_start_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41100000    # 9.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-virtual {v14, v15, v5}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 329
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x41100000    # 9.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-virtual {v12, v15, v5}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V
    :try_end_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_4

    .line 331
    :catch_0
    move-exception v9

    .line 332
    .local v9, "e":Ljava/lang/IllegalAccessException;
    :try_start_5
    invoke-virtual {v9}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_5
    .catch Ljava/lang/NoSuchFieldException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_4

    .line 336
    .end local v9    # "e":Ljava/lang/IllegalAccessException;
    .end local v12    # "maxHeight":Ljava/lang/reflect/Field;
    .end local v14    # "minHeight":Ljava/lang/reflect/Field;
    :catch_1
    move-exception v9

    .line 337
    .local v9, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v9}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto/16 :goto_4

    .line 333
    .end local v9    # "e":Ljava/lang/NoSuchFieldException;
    .restart local v12    # "maxHeight":Ljava/lang/reflect/Field;
    .restart local v14    # "minHeight":Ljava/lang/reflect/Field;
    :catch_2
    move-exception v9

    .line 334
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    :try_start_6
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_6
    .catch Ljava/lang/NoSuchFieldException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_4

    .line 346
    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    .end local v12    # "maxHeight":Ljava/lang/reflect/Field;
    .end local v14    # "minHeight":Ljava/lang/reflect/Field;
    .restart local v13    # "method":Ljava/lang/reflect/Method;
    :catch_3
    move-exception v9

    .line 347
    .local v9, "e":Ljava/lang/IllegalAccessException;
    :try_start_7
    invoke-virtual {v9}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_7
    .catch Ljava/lang/NoSuchMethodException; {:try_start_7 .. :try_end_7} :catch_4

    goto/16 :goto_5

    .line 354
    .end local v9    # "e":Ljava/lang/IllegalAccessException;
    .end local v13    # "method":Ljava/lang/reflect/Method;
    :catch_4
    move-exception v9

    .line 355
    .local v9, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v9}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto/16 :goto_5

    .line 348
    .end local v9    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v13    # "method":Ljava/lang/reflect/Method;
    :catch_5
    move-exception v9

    .line 349
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    :try_start_8
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_5

    .line 350
    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    :catch_6
    move-exception v9

    .line 351
    .local v9, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v9}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
    :try_end_8
    .catch Ljava/lang/NoSuchMethodException; {:try_start_8 .. :try_end_8} :catch_4

    goto/16 :goto_5
.end method

.method private seekbarLayout()V
    .locals 20

    .prologue
    .line 146
    new-instance v15, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    invoke-direct {v15, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 147
    .local v15, "sizeDisplayLayout":Landroid/widget/RelativeLayout;
    new-instance v16, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    .line 148
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42480000    # 50.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 147
    move-object/from16 v0, v16

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 149
    .local v16, "sizeDisplayLayoutParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual/range {v15 .. v16}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 152
    new-instance v1, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    .line 153
    new-instance v13, Landroid/widget/RelativeLayout$LayoutParams;

    .line 154
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c00000    # 24.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41c00000    # 24.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 153
    invoke-direct {v13, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 155
    .local v13, "plusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v1, 0x1

    iput-boolean v1, v13, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 156
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41000000    # 8.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v13, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 157
    const/16 v1, 0xb

    invoke-virtual {v13, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 158
    const/16 v1, 0x8

    invoke-virtual {v13, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 160
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v13}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 161
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_plus"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 163
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSdkVersion:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    .line 164
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgPressPath:Ljava/lang/String;

    .line 165
    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgFocusPath:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgDimPath:Ljava/lang/String;

    const/16 v6, 0x18

    const/16 v7, 0x18

    .line 164
    invoke-virtual/range {v1 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 178
    :goto_0
    new-instance v1, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    .line 179
    new-instance v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 180
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c00000    # 24.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41c00000    # 24.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 179
    invoke-direct {v11, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 181
    .local v11, "minusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41000000    # 8.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 182
    const/4 v1, 0x1

    iput-boolean v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 183
    const/16 v1, 0x9

    invoke-virtual {v11, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 184
    const/16 v1, 0x8

    invoke-virtual {v11, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 186
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v11}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 187
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_minus"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 189
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSdkVersion:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_2

    .line 190
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgPressPath:Ljava/lang/String;

    .line 191
    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgFocusPath:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgDimPath:Ljava/lang/String;

    const/16 v6, 0x18

    const/16 v7, 0x18

    .line 190
    invoke-virtual/range {v1 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 203
    :goto_1
    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBarColor:Landroid/graphics/drawable/GradientDrawable;

    .line 204
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBarColor:Landroid/graphics/drawable/GradientDrawable;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->penSeekbar(Landroid/graphics/drawable/GradientDrawable;)Landroid/widget/SeekBar;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    .line 206
    new-instance v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    .line 207
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 208
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    const/16 v2, 0x56

    const/16 v3, 0x57

    const/16 v4, 0x5b

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 209
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41400000    # 12.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 212
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarType:I

    if-lez v1, :cond_4

    .line 213
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    const/16 v2, 0x33

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 214
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x1

    invoke-direct {v12, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 215
    .local v12, "penSizeTextParam":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x420c0000    # 35.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v12, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 236
    :goto_2
    const/4 v1, 0x4

    invoke-virtual {v12, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 237
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 238
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    invoke-virtual {v15, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 239
    const/4 v1, 0x0

    invoke-virtual {v15, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 240
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v15, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 241
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v15, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 242
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v15, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 244
    new-instance v18, Landroid/widget/RelativeLayout$LayoutParams;

    .line 245
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x43440000    # 196.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    add-int/lit8 v1, v1, -0xc

    const/4 v2, -0x1

    .line 244
    move-object/from16 v0, v18

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 246
    .local v18, "touchLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c00000    # 24.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    const/4 v2, 0x0

    .line 247
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41c00000    # 24.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    const/4 v4, 0x0

    .line 246
    move-object/from16 v0, v18

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 249
    new-instance v17, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    move-object/from16 v0, v17

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 250
    .local v17, "touchLayout":Landroid/widget/RelativeLayout;
    invoke-virtual/range {v17 .. v18}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 251
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$12;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 259
    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 261
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->addView(Landroid/view/View;)V

    .line 262
    return-void

    .line 166
    .end local v11    # "minusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v12    # "penSizeTextParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v17    # "touchLayout":Landroid/widget/RelativeLayout;
    .end local v18    # "touchLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSdkVersion:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_1

    .line 167
    new-instance v14, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v1, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v1}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    invoke-direct {v14, v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 168
    .local v14, "shapeDrawable":Landroid/graphics/drawable/ShapeDrawable;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v4, 0x2d

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    const/4 v4, 0x0

    .line 169
    invoke-direct {v2, v3, v4, v14}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 168
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgPath:Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgFocusPath:Ljava/lang/String;

    const/4 v5, 0x0

    .line 171
    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgDimPath:Ljava/lang/String;

    const/16 v7, 0x18

    const/16 v8, 0x18

    .line 170
    invoke-virtual/range {v1 .. v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 174
    .end local v14    # "shapeDrawable":Landroid/graphics/drawable/ShapeDrawable;
    :cond_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgFocusPath:Ljava/lang/String;

    .line 175
    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->plusBgDimPath:Ljava/lang/String;

    const/16 v6, 0x18

    const/16 v7, 0x18

    .line 174
    invoke-virtual/range {v1 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 192
    .restart local v11    # "minusImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSdkVersion:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_3

    .line 193
    new-instance v14, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v1, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v1}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    invoke-direct {v14, v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 194
    .restart local v14    # "shapeDrawable":Landroid/graphics/drawable/ShapeDrawable;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v4, 0x2d

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    .line 195
    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v14}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 194
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 196
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgPath:Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgFocusPath:Ljava/lang/String;

    const/4 v5, 0x0

    .line 197
    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgDimPath:Ljava/lang/String;

    const/16 v7, 0x18

    const/16 v8, 0x18

    .line 196
    invoke-virtual/range {v1 .. v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 199
    .end local v14    # "shapeDrawable":Landroid/graphics/drawable/ShapeDrawable;
    :cond_3
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgFocusPath:Ljava/lang/String;

    .line 200
    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->minusBgDimPath:Ljava/lang/String;

    const/16 v6, 0x18

    const/16 v7, 0x18

    .line 199
    invoke-virtual/range {v1 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 217
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    const/16 v2, 0x31

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 218
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42780000    # 62.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 219
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41900000    # 18.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 218
    invoke-direct {v12, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 222
    .restart local v12    # "penSizeTextParam":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v10, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mContext:Landroid/content/Context;

    invoke-direct {v10, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 223
    .local v10, "imgDivider":Landroid/widget/ImageView;
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    .line 224
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 223
    invoke-direct {v9, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 225
    .local v9, "dividerParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v1, 0x1

    iput-boolean v1, v9, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 226
    const/4 v1, 0x6

    invoke-virtual {v9, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 227
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v9, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 228
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v9, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 229
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v9, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 230
    invoke-virtual {v10, v9}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 231
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->lineDivider:Ljava/lang/String;

    invoke-virtual {v1, v10, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 232
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->addView(Landroid/view/View;)V

    goto/16 :goto_2
.end method

.method private setSeekbarListener()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 127
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekbarKeyListner:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 131
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 133
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 134
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButtonOnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 137
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 138
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 140
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 141
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButtonOnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 143
    :cond_2
    return-void
.end method


# virtual methods
.method protected close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 619
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 620
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    .line 622
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 623
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mMinusButton:Landroid/widget/ImageButton;

    .line 624
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 625
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;

    .line 627
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 628
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    .line 630
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBarColor:Landroid/graphics/drawable/GradientDrawable;

    .line 631
    return-void
.end method

.method public getPenSeekbar()Landroid/widget/SeekBar;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method protected getSeekBarColor()Landroid/graphics/drawable/GradientDrawable;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBarColor:Landroid/graphics/drawable/GradientDrawable;

    return-object v0
.end method

.method protected getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected initView()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->seekbarLayout()V

    .line 93
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->setSeekbarListener()V

    .line 94
    return-void
.end method

.method protected setAlpha(I)V
    .locals 1
    .param p1, "alpha"    # I

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->bgDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 98
    return-void
.end method

.method protected setSPenSeekBarChangeListener(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;)V
    .locals 0
    .param p1, "mListener"    # Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;

    .prologue
    .line 605
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSPenSeekBarChangeListner:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;

    .line 606
    return-void
.end method

.method protected setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    return-void
.end method

.method protected setTextXPosition(I)V
    .locals 2
    .param p1, "x"    # I

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setX(F)V

    .line 118
    return-void
.end method

.method protected setTextYPosition(I)V
    .locals 2
    .param p1, "y"    # I

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPenSeekbarTextView:Landroid/widget/TextView;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setY(F)V

    .line 122
    return-void
.end method
