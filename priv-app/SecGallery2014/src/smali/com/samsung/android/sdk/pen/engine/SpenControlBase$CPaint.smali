.class public Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;
.super Ljava/lang/Object;
.source "SpenControlBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "CPaint"
.end annotation


# static fields
.field public static final BORDER_DASHED:I = 0x2

.field public static final BORDER_LINE:I = 0x1

.field public static final BORDER_STATIC_LINE_WIDTH:I = 0x4

.field public static final BORDER_TEXTBOX_LINE:I = 0x7

.field private static final DEFAULT_BORDER_DASHED:I = -0xfb7301

.field private static final DEFAULT_BORDER_LINE_COLOR:I = -0xfb7301

.field protected static final DEFAULT_BORDER_WIDTH:I = 0x4

.field private static final DEFAULT_COLOR_HANDLE_ROTATE_ICON:I = -0xff6634

.field private static final DEFAULT_TYPE_WIDE_BORDER_WIDTH:I = 0x6

.field public static final DEGREE_RECT:I = 0x4

.field public static final DEGREE_TEXT:I = 0x5

.field public static final DIMMING_WINDOW:I = 0x3

.field private static final HANDLE_ROTATE_ICON:I = 0x9

.field public static final HIGHLIGHT_STROKE:I = 0x6

.field public static final MOVING_OBJECT_PAINT:I = 0x0

.field private static final MOVING_OBJECT_PAINT_VIENNA:I = 0x8


# instance fields
.field private final mDashPathEffect:Landroid/graphics/DashPathEffect;

.field private final mDensity:F

.field private final mPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(F)V
    .locals 3
    .param p1, "density"    # F

    .prologue
    .line 412
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mDensity:F

    .line 414
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    .line 415
    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mDashPathEffect:Landroid/graphics/DashPathEffect;

    .line 416
    return-void

    .line 415
    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method


# virtual methods
.method public getPaint(I)Landroid/graphics/Paint;
    .locals 7
    .param p1, "option"    # I

    .prologue
    const/16 v6, 0x64

    const/high16 v5, 0x40800000    # 4.0f

    const/4 v4, 0x1

    const v3, -0xfb7301

    const/high16 v2, 0x40c00000    # 6.0f

    .line 419
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->reset()V

    .line 420
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 421
    packed-switch p1, :pswitch_data_0

    .line 493
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    return-object v0

    .line 423
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40200000    # 2.5f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 424
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 425
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 426
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    goto :goto_0

    .line 429
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40600000    # 3.5f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 430
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 431
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 432
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    goto :goto_0

    .line 435
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 436
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 437
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 438
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getType()I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 439
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto :goto_0

    .line 443
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 444
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 445
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 446
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getType()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 447
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 451
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mDensity:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto/16 :goto_0

    .line 456
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 457
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 458
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 459
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 460
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mDashPathEffect:Landroid/graphics/DashPathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 461
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getType()I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 462
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto/16 :goto_0

    .line 466
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 467
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    goto/16 :goto_0

    .line 470
    :pswitch_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 471
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    goto/16 :goto_0

    .line 474
    :pswitch_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41900000    # 18.0f

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mDensity:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 475
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 476
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 477
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_0

    .line 480
    :pswitch_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 481
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 482
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 483
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_0

    .line 486
    :pswitch_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    const v1, -0xff6634

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 487
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 488
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto/16 :goto_0

    .line 421
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_3
        :pswitch_1
        :pswitch_9
    .end packed-switch
.end method
