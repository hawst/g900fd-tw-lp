.class public interface abstract Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;
.super Ljava/lang/Object;
.source "SPenSeekBarView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60c
    name = "SPenSeekBarChangeListner"
.end annotation


# virtual methods
.method public abstract onProgressChanged(Landroid/widget/SeekBar;IZI)V
.end method

.method public abstract onStartTrackingTouch(Landroid/widget/SeekBar;I)V
.end method

.method public abstract onStopTrackingTouch(Landroid/widget/SeekBar;I)V
.end method

.method public abstract onUpdate(ZI)V
.end method
