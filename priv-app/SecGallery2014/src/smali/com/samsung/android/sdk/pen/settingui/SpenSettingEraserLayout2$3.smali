.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$3;
.super Ljava/lang/Object;
.source "SpenSettingEraserLayout2.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMaxButton:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 214
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;->onPopup(Z)V

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMinButton:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;->onPopup(Z)V

    goto :goto_0
.end method
