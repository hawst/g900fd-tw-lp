.class Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
.super Landroid/view/View;
.source "SpenNotePad.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;
    }
.end annotation


# static fields
.field private static final PAD_BG_COLOR:I = 0x33000000

.field private static final SIGMA:F = 1.0E-4f

.field private static final SIGMAX:F = 1.0f

.field private static final STATE_BOX_MOVE:I = 0x1

.field private static final STATE_BOX_RESIZE:I = 0x2

.field private static final STATE_BTN_DOWN:I = 0xb

.field private static final STATE_BTN_ENTER:I = 0x8

.field private static final STATE_BTN_LEFT:I = 0x6

.field private static final STATE_BTN_RIGHT:I = 0x7

.field private static final STATE_BTN_UP:I = 0xa

.field private static final STATE_DRAW:I = 0x9

.field private static final STATE_NONE:I = 0x0

.field private static final STATE_PAD_MOVE:I = 0x3

.field private static final STATE_PAD_MOVING_RECT:I = 0x5

.field private static final STATE_PAD_NONE:I = 0x4

.field private static final STROKE_BOX_LINE_COLOR:I = -0xf4742a

.field private static final STROKE_BOX_WIDTH:I = 0x2

.field public static final TAG:Ljava/lang/String; = "ZoomPad"

.field private static final penNameBrush:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Brush"

.field private static final penNameChineseBrush:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

.field private static final penNameFountainPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.FountainPen"

.field private static final penNameInkPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.InkPen"

.field private static final penNameMagicPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.MagicPen"

.field private static final penNameMarker:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Marker"

.field private static final penNameObliquePen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

.field private static final penNamePencil:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Pencil"


# instance fields
.field private BUTTON_HEIGHT:I

.field private BUTTON_LINE_HEIGHT:I

.field private BUTTON_LINE_WIDTH:I

.field private BUTTON_PADDING:I

.field private BUTTON_WIDTH:I

.field private PAD_HANDLE_HEIGHT:I

.field private PAD_HANDLE_WIDTH:I

.field private STROKE_BOX_HEIGHT_LIMIT:I

.field private STROKE_BOX_HEIGHT_LIMIT_FLOAT:F

.field private STROKE_BOX_RESIZE:I

.field private STROKE_BOX_RESIZE_GAP:I

.field private activePen:I

.field private bIsSupport:Z

.field dstRect:Landroid/graphics/RectF;

.field public isArabic:Z

.field private isFirstCanvasTouch:Z

.field private isSoundEnabled:Z

.field private mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

.field private mAntiAliasPaint:Landroid/graphics/Paint;

.field private mAutoMoveArea:F

.field private mAutoMovingRect:Landroid/graphics/RectF;

.field private mBitmap:[Landroid/graphics/Bitmap;

.field private mBitmapHeight:I

.field private mBitmapWidth:I

.field private mBoxHeight:F

.field private mBoxHeightChange:Z

.field private mBoxPaint:Landroid/graphics/Paint;

.field private mBoxRate:F

.field private mBoxSaveStartX:F

.field private mBoxSaveStartY:F

.field private mBoxStartX:F

.field private mBoxStartY:F

.field private mContext:Landroid/content/Context;

.field private mDeltaSaveX:F

.field private mDeltaX:F

.field private mDeltaY:F

.field private mDownButton:Z

.field private mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

.field private mEnable:Z

.field private mEnterButton:Z

.field private mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

.field private mIndexBrush:I

.field private mIndexEraser:I

.field private mIndexMarker:I

.field private mIndexPencil:I

.field private mIsMultiTouch:Z

.field private mIsStrokeDrawing:Z

.field private mIsTablet:Z

.field private mLeftButton:Z

.field private mMaxDeltaX:F

.field private mMaxDeltaY:F

.field private mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

.field private mOldX:F

.field private mOldY:F

.field private final mOnePT:F

.field private mPadBarRect:Landroid/graphics/RectF;

.field private mPadMargin:F

.field private mPadPaint:Landroid/graphics/Paint;

.field private mPadRect:Landroid/graphics/RectF;

.field private mParent:Landroid/view/ViewGroup;

.field private mPrePoint:Landroid/graphics/PointF;

.field private mRatio:F

.field private mReferenceBackground:Landroid/graphics/Bitmap;

.field private mRelative:Landroid/widget/RelativeLayout;

.field private mRightButton:Z

.field private mScreenHeight:I

.field private mScreenStartX:I

.field private mScreenStartY:I

.field private mScreenWidth:I

.field private mSdkResources:Landroid/content/res/Resources;

.field private mSmps:Lcom/samsung/audio/SmpsManager;

.field private mState:I

.field private mToolAndActionMap:Landroid/util/SparseIntArray;

.field private mUpButton:Z

.field private mUpdatePad:Z

.field private preEvent:Landroid/view/MotionEvent;

.field private preToolType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, -0x1

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 182
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 60
    const/16 v3, 0x11

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    .line 61
    const/4 v3, 0x7

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE_GAP:I

    .line 62
    const/16 v3, 0x1e

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT:I

    .line 63
    const/high16 v3, 0x41f00000    # 30.0f

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT_FLOAT:F

    .line 64
    const/16 v3, 0x1f

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    .line 65
    const/16 v3, 0x22

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_HEIGHT:I

    .line 66
    const/16 v3, 0x71

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    .line 67
    const/16 v3, 0x31

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 68
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_PADDING:I

    .line 69
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    .line 70
    const/16 v3, 0x1a

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    .line 84
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    .line 89
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    .line 92
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    .line 93
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isArabic:Z

    .line 100
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmapWidth:I

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmapHeight:I

    .line 102
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mUpdatePad:Z

    .line 106
    new-instance v3, Landroid/util/SparseIntArray;

    const/16 v4, 0xa

    invoke-direct {v3, v4}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    .line 109
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    .line 110
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsStrokeDrawing:Z

    .line 111
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsMultiTouch:Z

    .line 112
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeightChange:Z

    .line 113
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mLeftButton:Z

    .line 114
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRightButton:Z

    .line 115
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnterButton:Z

    .line 116
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mUpButton:Z

    .line 117
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDownButton:Z

    .line 119
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 120
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadMargin:F

    .line 121
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    .line 122
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    .line 123
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxSaveStartX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxSaveStartY:F

    .line 124
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    .line 125
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    const/high16 v3, 0x3f800000    # 1.0f

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    .line 126
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    .line 134
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPrePoint:Landroid/graphics/PointF;

    .line 138
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 140
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOldX:F

    .line 141
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOldY:F

    .line 143
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isFirstCanvasTouch:Z

    .line 146
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z

    .line 147
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isSoundEnabled:Z

    .line 148
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 149
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexPencil:I

    .line 150
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexMarker:I

    .line 151
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexBrush:I

    .line 152
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexEraser:I

    .line 153
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->activePen:I

    .line 966
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->dstRect:Landroid/graphics/RectF;

    .line 1362
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    .line 183
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    .line 185
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 186
    .local v1, "mDisplayMetrics":Landroid/util/DisplayMetrics;
    iget v3, v1, Landroid/util/DisplayMetrics;->density:F

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    .line 188
    const/4 v3, 0x6

    new-array v3, v3, [Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    .line 189
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadPaint:Landroid/graphics/Paint;

    .line 190
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x33000000

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 192
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 193
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 195
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxPaint:Landroid/graphics/Paint;

    .line 196
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 197
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x40000000    # 2.0f

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v4, v5

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 198
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxPaint:Landroid/graphics/Paint;

    const v4, -0xf4742a

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 200
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3, v6, v6}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPrePoint:Landroid/graphics/PointF;

    .line 201
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    .line 202
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    .line 203
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    .line 205
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    .line 207
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 208
    .local v2, "manager":Landroid/content/pm/PackageManager;
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    .end local v2    # "manager":Landroid/content/pm/PackageManager;
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v7, v7}, Landroid/util/SparseIntArray;->put(II)V

    .line 214
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    const/4 v4, 0x2

    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 215
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    const/4 v4, 0x4

    const/4 v5, 0x3

    invoke-virtual {v3, v4, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 216
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    const/4 v4, 0x3

    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 218
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    invoke-direct {v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;-><init>(Landroid/content/Context;F)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    .line 219
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setActionListener(Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;)V

    .line 259
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ar"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 260
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isArabic:Z

    .line 264
    :goto_1
    return-void

    .line 209
    :catch_0
    move-exception v0

    .line 210
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 262
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isArabic:Z

    goto :goto_1
.end method

.method private absoluteCoordinate(Landroid/view/MotionEvent;Z)Landroid/view/MotionEvent;
    .locals 26
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "isCancel"    # Z

    .prologue
    .line 268
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    and-int/lit16 v8, v9, 0xff

    .line 269
    .local v8, "action":I
    if-eqz p2, :cond_0

    .line 270
    const/4 v8, 0x3

    .line 272
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    .line 273
    .local v4, "downTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    .line 274
    .local v6, "eventTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v12

    .line 275
    .local v12, "metaState":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v13

    .line 276
    .local v13, "buttonState":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getXPrecision()F

    move-result v14

    .line 277
    .local v14, "xPrecision":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getYPrecision()F

    move-result v15

    .line 278
    .local v15, "yPrecision":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v16

    .line 279
    .local v16, "deviceId":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v17

    .line 280
    .local v17, "edgeFlags":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v18

    .line 281
    .local v18, "source":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getFlags()I

    move-result v19

    .line 283
    .local v19, "flags":I
    const/4 v9, 0x1

    new-array v10, v9, [Landroid/view/MotionEvent$PointerProperties;

    .line 284
    .local v10, "pp":[Landroid/view/MotionEvent$PointerProperties;
    const/4 v9, 0x0

    new-instance v24, Landroid/view/MotionEvent$PointerProperties;

    invoke-direct/range {v24 .. v24}, Landroid/view/MotionEvent$PointerProperties;-><init>()V

    aput-object v24, v10, v9

    .line 285
    const/4 v9, 0x0

    aget-object v9, v10, v9

    const/16 v24, 0x0

    move/from16 v0, v24

    iput v0, v9, Landroid/view/MotionEvent$PointerProperties;->id:I

    .line 286
    const/4 v9, 0x0

    aget-object v9, v10, v9

    const/16 v24, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v24

    move/from16 v0, v24

    iput v0, v9, Landroid/view/MotionEvent$PointerProperties;->toolType:I

    .line 287
    const/4 v9, 0x1

    new-array v11, v9, [Landroid/view/MotionEvent$PointerCoords;

    .line 288
    .local v11, "pc":[Landroid/view/MotionEvent$PointerCoords;
    const/4 v9, 0x0

    new-instance v24, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v24 .. v24}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v24, v11, v9

    .line 289
    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v9

    const/16 v24, 0x1

    move/from16 v0, v24

    if-ne v9, v0, :cond_1

    .line 290
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v24, 0x19

    const v25, 0x3f4ccccd    # 0.8f

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v9, v0, v1}, Landroid/view/MotionEvent$PointerCoords;->setAxisValue(IF)V

    .line 291
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/high16 v24, 0x3f000000    # 0.5f

    move/from16 v0, v24

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 293
    :cond_1
    const/4 v9, 0x0

    const/16 v24, 0x0

    aget-object v24, v11, v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v9, v1}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    .line 295
    const/16 v21, 0x0

    .line 296
    .local v21, "ev":Landroid/view/MotionEvent;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v9

    if-lez v9, :cond_6

    .line 297
    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move/from16 v24, v0

    mul-float v9, v9, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->height()F

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/RectF;->height()F

    move-result v25

    sub-float v24, v24, v25

    div-float v9, v9, v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move/from16 v24, v0

    add-float v22, v9, v24

    .line 298
    .local v22, "x":F
    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move/from16 v24, v0

    mul-float v9, v9, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->height()F

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/RectF;->height()F

    move-result v25

    sub-float v24, v24, v25

    div-float v9, v9, v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move/from16 v24, v0

    add-float v23, v9, v24

    .line 299
    .local v23, "y":F
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 300
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move/from16 v0, v23

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 301
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v24, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v24

    move/from16 v0, v24

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 302
    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v9

    const/16 v24, 0x1

    move/from16 v0, v24

    if-ne v9, v0, :cond_2

    .line 303
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v24, 0x19

    const v25, 0x3f4ccccd    # 0.8f

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v9, v0, v1}, Landroid/view/MotionEvent$PointerCoords;->setAxisValue(IF)V

    .line 304
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/high16 v24, 0x3f000000    # 0.5f

    move/from16 v0, v24

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 306
    :cond_2
    const/4 v9, 0x1

    invoke-static/range {v4 .. v19}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v21

    .line 309
    const/16 v20, 0x1

    .local v20, "a":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v9

    move/from16 v0, v20

    if-lt v0, v9, :cond_4

    .line 321
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move/from16 v24, v0

    mul-float v9, v9, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->height()F

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/RectF;->height()F

    move-result v25

    sub-float v24, v24, v25

    div-float v9, v9, v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move/from16 v24, v0

    add-float v22, v9, v24

    .line 322
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move/from16 v24, v0

    mul-float v9, v9, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->height()F

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/RectF;->height()F

    move-result v25

    sub-float v24, v24, v25

    div-float v9, v9, v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move/from16 v24, v0

    add-float v23, v9, v24

    .line 323
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 324
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move/from16 v0, v23

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 325
    const/4 v9, 0x0

    aget-object v9, v11, v9

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v24

    move/from16 v0, v24

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 326
    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v9

    const/16 v24, 0x1

    move/from16 v0, v24

    if-ne v9, v0, :cond_3

    .line 327
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v24, 0x19

    const v25, 0x3f4ccccd    # 0.8f

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v9, v0, v1}, Landroid/view/MotionEvent$PointerCoords;->setAxisValue(IF)V

    .line 328
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/high16 v24, 0x3f000000    # 0.5f

    move/from16 v0, v24

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 330
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v24

    move-object/from16 v0, v21

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2, v11, v12}, Landroid/view/MotionEvent;->addBatch(J[Landroid/view/MotionEvent$PointerCoords;I)V

    .line 344
    .end local v20    # "a":I
    :goto_1
    return-object v21

    .line 310
    .restart local v20    # "a":I
    :cond_4
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v24

    move/from16 v0, v24

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 311
    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move/from16 v24, v0

    mul-float v9, v9, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->height()F

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/RectF;->height()F

    move-result v25

    sub-float v24, v24, v25

    div-float v9, v9, v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move/from16 v24, v0

    add-float v22, v9, v24

    .line 312
    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move/from16 v24, v0

    mul-float v9, v9, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->height()F

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/RectF;->height()F

    move-result v25

    sub-float v24, v24, v25

    div-float v9, v9, v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move/from16 v24, v0

    add-float v23, v9, v24

    .line 313
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 314
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move/from16 v0, v23

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 315
    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v9

    const/16 v24, 0x1

    move/from16 v0, v24

    if-ne v9, v0, :cond_5

    .line 316
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v24, 0x19

    const v25, 0x3f4ccccd    # 0.8f

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v9, v0, v1}, Landroid/view/MotionEvent$PointerCoords;->setAxisValue(IF)V

    .line 317
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/high16 v24, 0x3f000000    # 0.5f

    move/from16 v0, v24

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 319
    :cond_5
    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v24

    move-object/from16 v0, v21

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2, v11, v12}, Landroid/view/MotionEvent;->addBatch(J[Landroid/view/MotionEvent$PointerCoords;I)V

    .line 309
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_0

    .line 332
    .end local v20    # "a":I
    .end local v22    # "x":F
    .end local v23    # "y":F
    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move/from16 v24, v0

    mul-float v9, v9, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->height()F

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/RectF;->height()F

    move-result v25

    sub-float v24, v24, v25

    div-float v9, v9, v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move/from16 v24, v0

    add-float v22, v9, v24

    .line 333
    .restart local v22    # "x":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move/from16 v24, v0

    mul-float v9, v9, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->height()F

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/RectF;->height()F

    move-result v25

    sub-float v24, v24, v25

    div-float v9, v9, v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move/from16 v24, v0

    add-float v23, v9, v24

    .line 334
    .restart local v23    # "y":F
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 335
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move/from16 v0, v23

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 336
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v24, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v24

    move/from16 v0, v24

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 337
    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v9

    const/16 v24, 0x1

    move/from16 v0, v24

    if-ne v9, v0, :cond_7

    .line 338
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v24, 0x19

    const v25, 0x3f4ccccd    # 0.8f

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v9, v0, v1}, Landroid/view/MotionEvent$PointerCoords;->setAxisValue(IF)V

    .line 339
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/high16 v24, 0x3f000000    # 0.5f

    move/from16 v0, v24

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 341
    :cond_7
    const/4 v9, 0x1

    invoke-static/range {v4 .. v19}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v21

    goto/16 :goto_1
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;I)V
    .locals 0

    .prologue
    .line 84
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    return v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    return v0
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    return-object v0
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;Landroid/view/MotionEvent;Z)Landroid/view/MotionEvent;
    .locals 1

    .prologue
    .line 266
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->absoluteCoordinate(Landroid/view/MotionEvent;Z)Landroid/view/MotionEvent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$14(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;Z)V
    .locals 0

    .prologue
    .line 102
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mUpdatePad:Z

    return-void
.end method

.method static synthetic access$15(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    return-object v0
.end method

.method static synthetic access$16(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$17(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnable:Z

    return v0
.end method

.method static synthetic access$18(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    return v0
.end method

.method static synthetic access$19(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    return v0
.end method

.method static synthetic access$20(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;F)V
    .locals 0

    .prologue
    .line 119
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    return-void
.end method

.method static synthetic access$21(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    return v0
.end method

.method static synthetic access$22(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$23(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mLeftButton:Z

    return v0
.end method

.method static synthetic access$24(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Z
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRightButton:Z

    return v0
.end method

.method static synthetic access$25(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mUpButton:Z

    return v0
.end method

.method static synthetic access$26(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDownButton:Z

    return v0
.end method

.method static synthetic access$27(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnterButton:Z

    return v0
.end method

.method static synthetic access$28(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/view/MotionEvent;
    .locals 1

    .prologue
    .line 1362
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    return v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    return v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;F)V
    .locals 0

    .prologue
    .line 119
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    return-void
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    return v0
.end method

.method private convertPenNameToMaxThicknessValue(Ljava/lang/String;)I
    .locals 2
    .param p1, "penName"    # Ljava/lang/String;

    .prologue
    .line 610
    const/4 v0, 0x0

    .line 612
    .local v0, "maxValue":I
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 613
    :cond_0
    const/16 v0, 0x40

    .line 624
    :cond_1
    :goto_0
    return v0

    .line 614
    :cond_2
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 615
    :cond_3
    const/16 v0, 0x20

    .line 616
    goto :goto_0

    :cond_4
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 617
    :cond_5
    const/16 v0, 0x50

    .line 618
    goto :goto_0

    :cond_6
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 619
    const/16 v0, 0x6c

    .line 620
    goto :goto_0

    :cond_7
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "Eraser"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 621
    :cond_8
    const/16 v0, 0x64

    goto :goto_0
.end method

.method private initHapticFeedback()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 545
    const-string v2, "ZoomPad"

    const-string v3, "initHapticFeedback() - Start"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-nez v2, :cond_0

    .line 548
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 549
    .local v0, "dm":Landroid/util/DisplayMetrics;
    new-instance v2, Lcom/samsung/hapticfeedback/HapticEffect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/hapticfeedback/HapticEffect;-><init>(Landroid/content/Context;II)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    .line 558
    .end local v0    # "dm":Landroid/util/DisplayMetrics;
    :cond_0
    :goto_0
    const-string v2, "ZoomPad"

    const-string v3, "initHapticFeedback() - End"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    return-void

    .line 550
    :catch_0
    move-exception v1

    .line 551
    .local v1, "error":Ljava/lang/UnsatisfiedLinkError;
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 552
    const-string v2, "TAG"

    const-string v3, "Haptic Effect UnsatisfiedLinkError"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 553
    .end local v1    # "error":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v1

    .line 554
    .local v1, "error":Ljava/lang/NoClassDefFoundError;
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 555
    const-string v2, "TAG"

    const-string v3, "Haptic Effect NoClassDefFoundError"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private initialize()V
    .locals 21

    .prologue
    .line 384
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    const/4 v15, 0x0

    cmpl-float v14, v14, v15

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v14}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_5

    .line 385
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 388
    .local v2, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v14, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v15, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v14, v15, :cond_6

    .line 389
    iget v9, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 390
    .local v9, "widthPixels":I
    iget v4, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 396
    .local v4, "heightPixels":I
    :goto_0
    iget v14, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v14, v14

    iget v15, v2, Landroid/util/DisplayMetrics;->xdpi:F

    div-float/2addr v14, v15

    float-to-double v14, v14

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    .line 397
    .local v10, "x":D
    iget v14, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v14, v14

    iget v15, v2, Landroid/util/DisplayMetrics;->ydpi:F

    div-float/2addr v14, v15

    float-to-double v14, v14

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    .line 398
    .local v12, "y":D
    add-double v14, v10, v12

    invoke-static {v14, v15}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    .line 400
    .local v6, "screenInches":D
    int-to-float v14, v9

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    div-float/2addr v14, v15

    float-to-int v8, v14

    .line 401
    .local v8, "w":I
    const/4 v3, 0x0

    .line 402
    .local v3, "h":I
    const-wide/high16 v14, 0x4020000000000000L    # 8.0

    cmpl-double v14, v6, v14

    if-lez v14, :cond_7

    .line 404
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    .line 405
    const/high16 v14, 0x44480000    # 800.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    float-to-int v8, v14

    .line 406
    const/high16 v14, 0x436a0000    # 234.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    float-to-int v3, v14

    .line 407
    if-le v8, v9, :cond_1

    .line 408
    move v8, v9

    .line 409
    mul-int/lit16 v14, v8, 0xea

    int-to-float v14, v14

    const/high16 v15, 0x44480000    # 800.0f

    div-float/2addr v14, v15

    float-to-int v3, v14

    .line 411
    :cond_1
    const/16 v14, 0x1b

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_PADDING:I

    .line 412
    const/16 v14, 0x28

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 413
    const/16 v14, 0x71

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    .line 422
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    sub-int v15, v4, v9

    int-to-float v15, v15

    const/high16 v16, 0x40000000    # 2.0f

    div-float v15, v15, v16

    sub-int v16, v4, v3

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    sub-int v17, v4, v9

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x40000000    # 2.0f

    div-float v17, v17, v18

    int-to-float v0, v8

    move/from16 v18, v0

    add-float v17, v17, v18

    .line 423
    int-to-float v0, v4

    move/from16 v18, v0

    .line 422
    invoke-virtual/range {v14 .. v18}, Landroid/graphics/RectF;->set(FFFF)V

    .line 425
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    mul-int/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    div-int/2addr v14, v15

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    .line 426
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    .line 428
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    sub-int v14, v3, v14

    int-to-float v14, v14

    const/high16 v15, 0x40a00000    # 5.0f

    div-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    div-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    int-to-float v15, v15

    mul-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_HEIGHT:I

    int-to-float v15, v15

    div-float/2addr v14, v15

    float-to-int v5, v14

    .line 429
    .local v5, "tmp":I
    int-to-float v14, v5

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_HEIGHT:I

    int-to-float v15, v15

    mul-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    int-to-float v15, v15

    div-float/2addr v14, v15

    float-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_HEIGHT:I

    .line 430
    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    .line 431
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v14, :cond_2

    .line 432
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_HEIGHT:I

    move/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setHandleSize(II)V

    .line 435
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v15, v15, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    invoke-virtual/range {v14 .. v18}, Landroid/graphics/RectF;->set(FFFF)V

    .line 436
    const-string v14, "ZoomPad"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "initialize["

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 437
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_HEIGHT:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "]"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 436
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    const-string v14, "ZoomPad"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "initialize["

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 439
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "]"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 438
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isArabic:Z

    if-eqz v14, :cond_8

    .line 442
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v14}, Landroid/graphics/RectF;->width()F

    move-result v14

    const/high16 v15, 0x40a00000    # 5.0f

    div-float/2addr v14, v15

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    .line 443
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v15, v15, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    move/from16 v18, v0

    add-float v17, v17, v18

    .line 444
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    .line 443
    invoke-virtual/range {v14 .. v18}, Landroid/graphics/RectF;->set(FFFF)V

    .line 451
    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v14}, Landroid/graphics/RectF;->height()F

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v15}, Landroid/graphics/RectF;->height()F

    move-result v15

    sub-float/2addr v14, v15

    float-to-double v14, v14

    const-wide/high16 v16, 0x400c000000000000L    # 3.5

    div-double v14, v14, v16

    double-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT:I

    .line 452
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v14}, Landroid/graphics/RectF;->height()F

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v15}, Landroid/graphics/RectF;->height()F

    move-result v15

    sub-float/2addr v14, v15

    float-to-double v14, v14

    const-wide/high16 v16, 0x400c000000000000L    # 3.5

    div-double v14, v14, v16

    double-to-float v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT_FLOAT:F

    .line 453
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT:I

    int-to-float v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    .line 454
    const/high16 v14, 0x42200000    # 40.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadMargin:F

    .line 455
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v14, :cond_9

    .line 456
    const/16 v14, 0x19

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    .line 457
    const/high16 v14, 0x40e00000    # 7.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    float-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE_GAP:I

    .line 462
    :goto_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v14}, Landroid/graphics/RectF;->width()F

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v15}, Landroid/graphics/RectF;->height()F

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->height()F

    move-result v16

    sub-float v15, v15, v16

    div-float/2addr v14, v15

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    .line 463
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v14, :cond_3

    .line 464
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isArabic:Z

    invoke-virtual {v14, v15}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setArabic(Z)V

    .line 465
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    move/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setHeightAndRate(FF)V

    .line 468
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    if-eqz v14, :cond_4

    .line 469
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->unbindDrawables(Landroid/view/View;)V

    .line 471
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->makeLayout()Landroid/widget/RelativeLayout;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    .line 473
    .end local v2    # "displayMetrics":Landroid/util/DisplayMetrics;
    .end local v3    # "h":I
    .end local v4    # "heightPixels":I
    .end local v5    # "tmp":I
    .end local v6    # "screenInches":D
    .end local v8    # "w":I
    .end local v9    # "widthPixels":I
    .end local v10    # "x":D
    .end local v12    # "y":D
    :cond_5
    return-void

    .line 392
    .restart local v2    # "displayMetrics":Landroid/util/DisplayMetrics;
    :cond_6
    iget v9, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 393
    .restart local v9    # "widthPixels":I
    iget v4, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .restart local v4    # "heightPixels":I
    goto/16 :goto_0

    .line 415
    .restart local v3    # "h":I
    .restart local v6    # "screenInches":D
    .restart local v8    # "w":I
    .restart local v10    # "x":D
    .restart local v12    # "y":D
    :cond_7
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    .line 416
    const/high16 v14, 0x43b40000    # 360.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    float-to-int v8, v14

    .line 417
    const/high16 v14, 0x43160000    # 150.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    float-to-int v3, v14

    .line 418
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_PADDING:I

    .line 419
    const/16 v14, 0x22

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 420
    const/16 v14, 0x32

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    goto/16 :goto_1

    .line 446
    .restart local v5    # "tmp":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v14}, Landroid/graphics/RectF;->width()F

    move-result v14

    const/high16 v15, 0x40800000    # 4.0f

    mul-float/2addr v14, v15

    const/high16 v15, 0x40a00000    # 5.0f

    div-float/2addr v14, v15

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    .line 447
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v15, v15, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    move/from16 v16, v0

    add-float v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v17, v0

    .line 448
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    .line 447
    invoke-virtual/range {v14 .. v18}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_2

    .line 459
    :cond_9
    const/16 v14, 0x11

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    .line 460
    const/high16 v14, 0x40800000    # 4.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    float-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE_GAP:I

    goto/16 :goto_3
.end method

.method private makeLayout()Landroid/widget/RelativeLayout;
    .locals 34
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1723
    new-instance v26, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-direct {v0, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1724
    .local v26, "relative":Landroid/widget/RelativeLayout;
    new-instance v11, Landroid/content/res/ColorStateList;

    const/4 v2, 0x1

    new-array v2, v2, [[I

    const/4 v3, 0x0

    const/4 v4, 0x1

    new-array v4, v4, [I

    const/4 v5, 0x0

    const v6, 0x10100a7

    aput v6, v4, v5

    aput-object v4, v2, v3

    .line 1725
    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/16 v5, 0x1e

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v33, 0x0

    move/from16 v0, v33

    invoke-static {v5, v6, v7, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    aput v5, v3, v4

    .line 1724
    invoke-direct {v11, v2, v3}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 1726
    .local v11, "colorStateList":Landroid/content/res/ColorStateList;
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-le v2, v3, :cond_0

    .line 1727
    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v11, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1729
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v2, :cond_1

    .line 1730
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1733
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-int v0, v2

    move/from16 v32, v0

    .line 1734
    .local v32, "width":I
    new-instance v19, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1735
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1734
    move-object/from16 v0, v19

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1736
    .local v19, "lButton":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v21, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v21

    invoke-direct {v0, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1737
    .local v21, "leftButton":Landroid/widget/ImageButton;
    const/16 v2, 0x9

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1738
    const/4 v2, 0x0

    move-object/from16 v0, v19

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1739
    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1740
    const/4 v2, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1741
    const-string/jumbo v2, "string_move_left"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getResourceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1742
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_2

    .line 1743
    const/4 v3, 0x0

    const-string/jumbo v4, "zoompad_menu_press"

    const-string/jumbo v5, "zoompad_menu_focus"

    .line 1744
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1743
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1759
    :goto_0
    new-instance v24, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1760
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1759
    move-object/from16 v0, v24

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1761
    .local v24, "rButton":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v27, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v27

    invoke-direct {v0, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1762
    .local v27, "rightButton":Landroid/widget/ImageButton;
    const/16 v2, 0x9

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1763
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, v24

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1764
    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1765
    const/4 v2, 0x1

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1766
    const-string/jumbo v2, "string_move_right"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getResourceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1767
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_5

    .line 1768
    const/4 v3, 0x0

    const-string/jumbo v4, "zoompad_menu_press"

    const-string/jumbo v5, "zoompad_menu_focus"

    .line 1769
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1768
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1784
    :goto_1
    new-instance v15, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1785
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1784
    invoke-direct {v15, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1786
    .local v15, "eButton":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v16, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1787
    .local v16, "enterButton":Landroid/widget/ImageButton;
    const/16 v2, 0x9

    invoke-virtual {v15, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1788
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v15, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1789
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1790
    const/4 v2, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1791
    const-string/jumbo v2, "string_enter"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getResourceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1792
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_8

    .line 1793
    const/4 v3, 0x0

    const-string/jumbo v4, "zoompad_menu_press"

    const-string/jumbo v5, "zoompad_menu_focus"

    .line 1794
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1793
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1810
    :goto_2
    new-instance v29, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1811
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1810
    move-object/from16 v0, v29

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1812
    .local v29, "uButton":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v30, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v30

    invoke-direct {v0, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1813
    .local v30, "upButton":Landroid/widget/ImageButton;
    const/16 v2, 0x9

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1814
    move/from16 v0, v32

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    mul-int/lit8 v3, v3, 0x3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, v29

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1815
    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1816
    const/4 v2, 0x1

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1817
    const-string/jumbo v2, "string_move_up"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getResourceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1818
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_b

    .line 1819
    const/4 v3, 0x0

    const-string/jumbo v4, "zoompad_menu_press"

    const-string/jumbo v5, "zoompad_menu_focus"

    .line 1820
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1819
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1835
    :goto_3
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1836
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1835
    invoke-direct {v12, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1837
    .local v12, "dButton":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v13, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-direct {v13, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1838
    .local v13, "downButton":Landroid/widget/ImageButton;
    const/16 v2, 0x9

    invoke-virtual {v12, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1839
    move/from16 v0, v32

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v12, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1840
    invoke-virtual {v13, v12}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1841
    const/4 v2, 0x1

    invoke-virtual {v13, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1842
    const-string/jumbo v2, "string_move_down"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getResourceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1843
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_e

    .line 1844
    const/4 v3, 0x0

    const-string/jumbo v4, "zoompad_menu_press"

    const-string/jumbo v5, "zoompad_menu_focus"

    .line 1845
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1844
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v13, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1860
    :goto_4
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1861
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1860
    invoke-direct {v8, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1862
    .local v8, "cButton":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v9, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-direct {v9, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1863
    .local v9, "closeButton":Landroid/widget/ImageButton;
    const/16 v2, 0x9

    invoke-virtual {v8, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1864
    move/from16 v0, v32

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1865
    invoke-virtual {v9, v8}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1866
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1867
    const-string/jumbo v2, "string_close"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getResourceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1868
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_11

    .line 1869
    const/4 v3, 0x0

    const-string/jumbo v4, "zoompad_menu_press"

    const-string/jumbo v5, "zoompad_menu_focus"

    .line 1870
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1869
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1880
    :goto_5
    new-instance v20, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1881
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1880
    move-object/from16 v0, v20

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1882
    .local v20, "lLine":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, v20

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1883
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    sub-int/2addr v2, v3

    move-object/from16 v0, v20

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1884
    new-instance v25, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1885
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1884
    move-object/from16 v0, v25

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1886
    .local v25, "rLine":Landroid/widget/RelativeLayout$LayoutParams;
    move/from16 v0, v32

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, v25

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1887
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    sub-int/2addr v2, v3

    move-object/from16 v0, v25

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1889
    new-instance v18, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1890
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1889
    move-object/from16 v0, v18

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1891
    .local v18, "l2Line":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, v18

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1892
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    sub-int/2addr v2, v3

    move-object/from16 v0, v18

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1893
    new-instance v23, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1894
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1893
    move-object/from16 v0, v23

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1895
    .local v23, "r2Line":Landroid/widget/RelativeLayout$LayoutParams;
    move/from16 v0, v32

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_WIDTH:I

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, v23

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1896
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_LINE_HEIGHT:I

    sub-int/2addr v2, v3

    move-object/from16 v0, v23

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1898
    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1899
    invoke-virtual/range {v26 .. v27}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1900
    move-object/from16 v0, v26

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1901
    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1902
    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1903
    move-object/from16 v0, v26

    invoke-virtual {v0, v9}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1905
    new-instance v22, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1906
    .local v22, "leftImage":Landroid/widget/ImageView;
    new-instance v19, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v19    # "lButton":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_PADDING:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1907
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1906
    move-object/from16 v0, v19

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1908
    .restart local v19    # "lButton":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0x9

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1909
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_PADDING:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, v19

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1910
    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1911
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_14

    .line 1912
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_13

    const-string/jumbo v2, "zoompad_menu_left"

    .line 1913
    :goto_6
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1912
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1924
    :goto_7
    new-instance v28, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v28

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1925
    .local v28, "rightImage":Landroid/widget/ImageView;
    new-instance v24, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v24    # "rButton":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_PADDING:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1926
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1925
    move-object/from16 v0, v24

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1927
    .restart local v24    # "rButton":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0x9

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1928
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_PADDING:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, v24

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1929
    move-object/from16 v0, v28

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1930
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_19

    .line 1931
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_18

    const-string/jumbo v2, "zoompad_menu_right"

    .line 1932
    :goto_8
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1931
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1943
    :goto_9
    new-instance v31, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v31

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1944
    .local v31, "upImage":Landroid/widget/ImageView;
    new-instance v29, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v29    # "uButton":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_PADDING:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1945
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1944
    move-object/from16 v0, v29

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1946
    .restart local v29    # "uButton":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0x9

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1947
    move/from16 v0, v32

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    mul-int/lit8 v3, v3, 0x3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_PADDING:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, v29

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1948
    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1949
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_1e

    .line 1950
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_1d

    const-string/jumbo v2, "zoompad_menu_up"

    .line 1951
    :goto_a
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1950
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1962
    :goto_b
    new-instance v14, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-direct {v14, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1963
    .local v14, "downImage":Landroid/widget/ImageView;
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v12    # "dButton":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_PADDING:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1964
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1963
    invoke-direct {v12, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1965
    .restart local v12    # "dButton":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0x9

    invoke-virtual {v12, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1966
    move/from16 v0, v32

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_PADDING:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v12, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1967
    invoke-virtual {v14, v12}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1968
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_23

    .line 1969
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_22

    const-string/jumbo v2, "zoompad_menu_down"

    .line 1970
    :goto_c
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1969
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v14, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1981
    :goto_d
    new-instance v17, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1982
    .local v17, "enterImage":Landroid/widget/ImageView;
    new-instance v15, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v15    # "eButton":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_PADDING:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1983
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1982
    invoke-direct {v15, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1984
    .restart local v15    # "eButton":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0x9

    invoke-virtual {v15, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1985
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    mul-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_PADDING:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v15, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1986
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1987
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_28

    .line 1988
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_27

    const-string/jumbo v2, "zoompad_menu_enter"

    .line 1989
    :goto_e
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1988
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2000
    :goto_f
    new-instance v10, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-direct {v10, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 2001
    .local v10, "closeImage":Landroid/widget/ImageView;
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v8    # "cButton":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_PADDING:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 2002
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 2001
    invoke-direct {v8, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2003
    .restart local v8    # "cButton":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0x9

    invoke-virtual {v8, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2004
    move/from16 v0, v32

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_PADDING:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2005
    invoke-virtual {v10, v8}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2006
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_2d

    .line 2007
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_2c

    const-string/jumbo v2, "zoompad_menu_close"

    .line 2008
    :goto_10
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 2007
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v10, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2014
    :goto_11
    move-object/from16 v0, v26

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2018
    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2020
    move-object/from16 v0, v26

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2027
    move-object/from16 v0, v26

    invoke-virtual {v0, v14}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2029
    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2033
    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2035
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$3;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$3;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2044
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$4;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$4;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2053
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$5;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$5;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2062
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$6;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$6;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V

    invoke-virtual {v13, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2071
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$7;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$7;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2080
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$8;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$8;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V

    invoke-virtual {v9, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2090
    return-object v26

    .line 1746
    .end local v8    # "cButton":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v9    # "closeButton":Landroid/widget/ImageButton;
    .end local v10    # "closeImage":Landroid/widget/ImageView;
    .end local v12    # "dButton":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v13    # "downButton":Landroid/widget/ImageButton;
    .end local v14    # "downImage":Landroid/widget/ImageView;
    .end local v15    # "eButton":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v16    # "enterButton":Landroid/widget/ImageButton;
    .end local v17    # "enterImage":Landroid/widget/ImageView;
    .end local v18    # "l2Line":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v20    # "lLine":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v22    # "leftImage":Landroid/widget/ImageView;
    .end local v23    # "r2Line":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v24    # "rButton":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v25    # "rLine":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v27    # "rightButton":Landroid/widget/ImageButton;
    .end local v28    # "rightImage":Landroid/widget/ImageView;
    .end local v29    # "uButton":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v30    # "upButton":Landroid/widget/ImageButton;
    .end local v31    # "upImage":Landroid/widget/ImageView;
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mLeftButton:Z

    if-eqz v2, :cond_4

    .line 1747
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-le v2, v3, :cond_3

    .line 1748
    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v11, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 1750
    :cond_3
    const/4 v3, 0x0

    const-string/jumbo v4, "zoompad_menu_press"

    const-string/jumbo v5, "zoompad_menu_focus"

    .line 1751
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1750
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 1754
    :cond_4
    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string/jumbo v5, "zoompad_menu_focus"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    .line 1755
    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1754
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 1771
    .restart local v24    # "rButton":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v27    # "rightButton":Landroid/widget/ImageButton;
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRightButton:Z

    if-eqz v2, :cond_7

    .line 1772
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-le v2, v3, :cond_6

    .line 1773
    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v11, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 1775
    :cond_6
    const/4 v3, 0x0

    const-string/jumbo v4, "zoompad_menu_press"

    const-string/jumbo v5, "zoompad_menu_focus"

    .line 1776
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1775
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 1779
    :cond_7
    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string/jumbo v5, "zoompad_menu_focus"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    .line 1780
    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1779
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 1796
    .restart local v15    # "eButton":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v16    # "enterButton":Landroid/widget/ImageButton;
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnterButton:Z

    if-eqz v2, :cond_a

    .line 1797
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-le v2, v3, :cond_9

    .line 1798
    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v11, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 1800
    :cond_9
    const/4 v3, 0x0

    const-string/jumbo v4, "zoompad_menu_press"

    const-string/jumbo v5, "zoompad_menu_focus"

    .line 1801
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1800
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 1804
    :cond_a
    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string/jumbo v5, "zoompad_menu_focus"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    .line 1805
    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1804
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 1822
    .restart local v29    # "uButton":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v30    # "upButton":Landroid/widget/ImageButton;
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mUpButton:Z

    if-eqz v2, :cond_d

    .line 1823
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-le v2, v3, :cond_c

    .line 1824
    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v11, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 1826
    :cond_c
    const/4 v3, 0x0

    const-string/jumbo v4, "zoompad_menu_press"

    const-string/jumbo v5, "zoompad_menu_focus"

    .line 1827
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1826
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 1830
    :cond_d
    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string/jumbo v5, "zoompad_menu_focus"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    .line 1831
    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1830
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 1847
    .restart local v12    # "dButton":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v13    # "downButton":Landroid/widget/ImageButton;
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDownButton:Z

    if-eqz v2, :cond_10

    .line 1848
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-le v2, v3, :cond_f

    .line 1849
    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v11, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v13, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    .line 1851
    :cond_f
    const/4 v3, 0x0

    const-string/jumbo v4, "zoompad_menu_press"

    const-string/jumbo v5, "zoompad_menu_focus"

    .line 1852
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1851
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v13, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    .line 1855
    :cond_10
    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string/jumbo v5, "zoompad_menu_focus"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    .line 1856
    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1855
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v13, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    .line 1872
    .restart local v8    # "cButton":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v9    # "closeButton":Landroid/widget/ImageButton;
    :cond_11
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-le v2, v3, :cond_12

    .line 1873
    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v11, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v9, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_5

    .line 1875
    :cond_12
    const/4 v3, 0x0

    const-string/jumbo v4, "zoompad_menu_press"

    const-string/jumbo v5, "zoompad_menu_focus"

    .line 1876
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    move-object/from16 v2, p0

    .line 1875
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_5

    .line 1912
    .restart local v18    # "l2Line":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v20    # "lLine":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v22    # "leftImage":Landroid/widget/ImageView;
    .restart local v23    # "r2Line":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v25    # "rLine":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_13
    const-string/jumbo v2, "zoompad_menu_left"

    goto/16 :goto_6

    .line 1915
    :cond_14
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mLeftButton:Z

    if-eqz v2, :cond_16

    .line 1916
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_15

    const-string/jumbo v2, "zoompad_menu_left"

    .line 1917
    :goto_12
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1916
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_7

    :cond_15
    const-string/jumbo v2, "zoompad_menu_left"

    goto :goto_12

    .line 1919
    :cond_16
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_17

    const-string/jumbo v2, "zoompad_menu_left_dim"

    .line 1920
    :goto_13
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1919
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_7

    :cond_17
    const-string/jumbo v2, "zoompad_menu_left_dim"

    goto :goto_13

    .line 1931
    .restart local v28    # "rightImage":Landroid/widget/ImageView;
    :cond_18
    const-string/jumbo v2, "zoompad_menu_right"

    goto/16 :goto_8

    .line 1934
    :cond_19
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRightButton:Z

    if-eqz v2, :cond_1b

    .line 1935
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_1a

    const-string/jumbo v2, "zoompad_menu_right"

    .line 1936
    :goto_14
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1935
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_9

    :cond_1a
    const-string/jumbo v2, "zoompad_menu_right"

    goto :goto_14

    .line 1939
    :cond_1b
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_1c

    const-string/jumbo v2, "zoompad_menu_right_dim"

    :goto_15
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1938
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_9

    .line 1939
    :cond_1c
    const-string/jumbo v2, "zoompad_menu_right_dim"

    goto :goto_15

    .line 1950
    .restart local v31    # "upImage":Landroid/widget/ImageView;
    :cond_1d
    const-string/jumbo v2, "zoompad_menu_up"

    goto/16 :goto_a

    .line 1953
    :cond_1e
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mUpButton:Z

    if-eqz v2, :cond_20

    .line 1954
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_1f

    const-string/jumbo v2, "zoompad_menu_up"

    :goto_16
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    .line 1955
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1954
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_b

    :cond_1f
    const-string/jumbo v2, "zoompad_menu_up"

    goto :goto_16

    .line 1957
    :cond_20
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_21

    const-string/jumbo v2, "zoompad_menu_up_dim"

    .line 1958
    :goto_17
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1957
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_b

    :cond_21
    const-string/jumbo v2, "zoompad_menu_up_dim"

    goto :goto_17

    .line 1969
    .restart local v14    # "downImage":Landroid/widget/ImageView;
    :cond_22
    const-string/jumbo v2, "zoompad_menu_down"

    goto/16 :goto_c

    .line 1972
    :cond_23
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDownButton:Z

    if-eqz v2, :cond_25

    .line 1973
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_24

    const-string/jumbo v2, "zoompad_menu_down"

    .line 1974
    :goto_18
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1973
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v14, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_d

    :cond_24
    const-string/jumbo v2, "zoompad_menu_down"

    goto :goto_18

    .line 1976
    :cond_25
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_26

    const-string/jumbo v2, "zoompad_menu_down_dim"

    .line 1977
    :goto_19
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1976
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v14, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_d

    :cond_26
    const-string/jumbo v2, "zoompad_menu_down_dim"

    goto :goto_19

    .line 1988
    .restart local v17    # "enterImage":Landroid/widget/ImageView;
    :cond_27
    const-string/jumbo v2, "zoompad_menu_enter"

    goto/16 :goto_e

    .line 1991
    :cond_28
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnterButton:Z

    if-eqz v2, :cond_2a

    .line 1992
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_29

    const-string/jumbo v2, "zoompad_menu_enter"

    .line 1993
    :goto_1a
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1992
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_f

    :cond_29
    const-string/jumbo v2, "zoompad_menu_enter"

    goto :goto_1a

    .line 1996
    :cond_2a
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_2b

    const-string/jumbo v2, "zoompad_menu_enter_dim"

    :goto_1b
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 1995
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_f

    .line 1996
    :cond_2b
    const-string/jumbo v2, "zoompad_menu_enter_dim"

    goto :goto_1b

    .line 2007
    .restart local v10    # "closeImage":Landroid/widget/ImageView;
    :cond_2c
    const-string/jumbo v2, "zoompad_menu_close"

    goto/16 :goto_10

    .line 2010
    :cond_2d
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsTablet:Z

    if-eqz v2, :cond_2e

    const-string/jumbo v2, "zoompad_menu_close"

    .line 2011
    :goto_1c
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    .line 2010
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v10, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_11

    :cond_2e
    const-string/jumbo v2, "zoompad_menu_close"

    goto :goto_1c
.end method

.method private onTouchSelection(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1562
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-nez v7, :cond_0

    .line 1563
    const/4 v7, 0x0

    .line 1718
    :goto_0
    return v7

    .line 1565
    :cond_0
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 1566
    .local v4, "tmpRect":Landroid/graphics/RectF;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    and-int/lit16 v0, v7, 0xff

    .line 1567
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    .line 1568
    .local v5, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    .line 1569
    .local v6, "y":F
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPrePoint:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    sub-float v1, v7, v5

    .line 1570
    .local v1, "dx":F
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPrePoint:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    sub-float v2, v7, v6

    .line 1572
    .local v2, "dy":F
    packed-switch v0, :pswitch_data_0

    .line 1714
    :cond_1
    :goto_1
    :pswitch_0
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v8, 0x1

    if-eq v7, v8, :cond_2

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v8, 0x2

    if-eq v7, v8, :cond_2

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v8, 0x3

    if-eq v7, v8, :cond_2

    .line 1715
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v8, 0x5

    if-eq v7, v8, :cond_2

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    if-nez v7, :cond_1c

    .line 1716
    :cond_2
    const/4 v7, 0x1

    goto :goto_0

    .line 1578
    :pswitch_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPrePoint:Landroid/graphics/PointF;

    iput v5, v7, Landroid/graphics/PointF;->x:F

    .line 1579
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPrePoint:Landroid/graphics/PointF;

    iput v6, v7, Landroid/graphics/PointF;->y:F

    .line 1581
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v7, v5, v6}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1582
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v7, v5, v6}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1583
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    const/high16 v8, 0x428c0000    # 70.0f

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->left:F

    .line 1584
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v10}, Landroid/graphics/RectF;->width()F

    move-result v10

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    add-float/2addr v9, v10

    const/high16 v10, 0x428c0000    # 70.0f

    add-float/2addr v9, v10

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    .line 1583
    invoke-virtual {v4, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1585
    invoke-virtual {v4, v5, v6}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1586
    const/4 v7, 0x3

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    .line 1632
    :cond_3
    :goto_2
    const-string v7, "ZoomPad"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "State="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1588
    :cond_4
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_WIDTH:I

    mul-int/lit8 v8, v8, 0x4

    int-to-float v8, v8

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->right:F

    .line 1589
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    .line 1588
    invoke-virtual {v4, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1590
    invoke-virtual {v4, v5, v6}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1591
    const/4 v7, 0x4

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    goto :goto_2

    .line 1593
    :cond_5
    const/4 v7, 0x0

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    goto :goto_2

    .line 1597
    :cond_6
    const/16 v7, 0x9

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    .line 1598
    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isArabic:Z

    if-eqz v7, :cond_7

    .line 1599
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    int-to-float v8, v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v9

    float-to-int v8, v8

    int-to-float v8, v8

    sub-float/2addr v7, v8

    .line 1600
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_HEIGHT:I

    int-to-float v9, v9

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v9, v10

    float-to-int v9, v9

    int-to-float v9, v9

    sub-float/2addr v8, v9

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->right:F

    .line 1601
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    int-to-float v10, v10

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v10, v11

    float-to-int v10, v10

    int-to-float v10, v10

    add-float/2addr v9, v10

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    .line 1599
    invoke-virtual {v4, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1607
    :goto_3
    invoke-virtual {v4, v5, v6}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1608
    const/4 v7, 0x5

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    goto/16 :goto_2

    .line 1603
    :cond_7
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    int-to-float v8, v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v9

    float-to-int v8, v8

    int-to-float v8, v8

    sub-float/2addr v7, v8

    .line 1604
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_HEIGHT:I

    int-to-float v9, v9

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v9, v10

    float-to-int v9, v9

    int-to-float v9, v9

    sub-float/2addr v8, v9

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->left:F

    .line 1605
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    int-to-float v10, v10

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v10, v11

    float-to-int v10, v10

    int-to-float v10, v10

    add-float/2addr v9, v10

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    .line 1603
    invoke-virtual {v4, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_3

    .line 1611
    :cond_8
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getStrokeBoxRect()Landroid/graphics/Rect;

    move-result-object v7

    float-to-int v8, v5

    float-to-int v9, v6

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1612
    const/4 v7, 0x1

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    .line 1613
    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isArabic:Z

    if-eqz v7, :cond_9

    .line 1614
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    float-to-int v7, v7

    int-to-float v7, v7

    .line 1615
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v9, v9

    add-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    int-to-float v9, v9

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v9, v10

    sub-float/2addr v8, v9

    float-to-int v8, v8

    int-to-float v8, v8

    .line 1616
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v10, v10

    add-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    int-to-float v10, v10

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    float-to-int v9, v9

    int-to-float v9, v9

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1617
    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v10, v11

    float-to-int v10, v10

    int-to-float v10, v10

    .line 1614
    invoke-virtual {v4, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1618
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE_GAP:I

    int-to-float v7, v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE_GAP:I

    neg-int v8, v8

    int-to-float v8, v8

    invoke-virtual {v4, v7, v8}, Landroid/graphics/RectF;->offset(FF)V

    .line 1626
    :goto_4
    invoke-virtual {v4, v5, v6}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1627
    const/4 v7, 0x2

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    goto/16 :goto_2

    .line 1620
    :cond_9
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    int-to-float v8, v8

    .line 1621
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    float-to-int v7, v7

    int-to-float v7, v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v9, v9

    add-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    int-to-float v9, v9

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v9, v10

    sub-float/2addr v8, v9

    float-to-int v8, v8

    int-to-float v8, v8

    .line 1622
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v10, v10

    add-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    int-to-float v10, v10

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    float-to-int v9, v9

    int-to-float v9, v9

    .line 1623
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    int-to-float v11, v11

    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v11, v12

    add-float/2addr v10, v11

    float-to-int v10, v10

    int-to-float v10, v10

    .line 1620
    invoke-virtual {v4, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1624
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE_GAP:I

    neg-int v7, v7

    int-to-float v7, v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE_GAP:I

    neg-int v8, v8

    int-to-float v8, v8

    invoke-virtual {v4, v7, v8}, Landroid/graphics/RectF;->offset(FF)V

    goto :goto_4

    .line 1630
    :cond_a
    const/4 v7, 0x0

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    goto/16 :goto_2

    .line 1636
    :pswitch_2
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_e

    .line 1637
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    sub-float/2addr v7, v1

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1638
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    sub-float/2addr v7, v2

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1639
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->checkBox()V

    .line 1686
    :cond_b
    :goto_5
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPrePoint:Landroid/graphics/PointF;

    iput v5, v7, Landroid/graphics/PointF;->x:F

    .line 1687
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPrePoint:Landroid/graphics/PointF;

    iput v6, v7, Landroid/graphics/PointF;->y:F

    .line 1688
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v8, 0x1

    if-eq v7, v8, :cond_c

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v8, 0x2

    if-eq v7, v8, :cond_c

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v8, 0x3

    if-eq v7, v8, :cond_c

    .line 1689
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v8, 0x5

    if-ne v7, v8, :cond_1

    .line 1690
    :cond_c
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->invalidate()V

    .line 1691
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v8, 0x1

    if-eq v7, v8, :cond_d

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_1

    .line 1692
    :cond_d
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateFrameBuffer()V

    goto/16 :goto_1

    .line 1640
    :cond_e
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_12

    .line 1641
    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeightChange:Z

    if-eqz v7, :cond_b

    .line 1642
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-float v3, v8

    .line 1643
    .local v3, "mBoxEndX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    sub-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v8, v8

    sub-float/2addr v7, v8

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    .line 1644
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-float v7, v8

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    .line 1645
    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isArabic:Z

    if-eqz v7, :cond_11

    .line 1646
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT:I

    int-to-float v8, v8

    cmpg-float v7, v7, v8

    if-gez v7, :cond_f

    .line 1647
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT:I

    int-to-float v7, v7

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    .line 1649
    :cond_f
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v7, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    cmpl-float v7, v7, v8

    if-lez v7, :cond_10

    .line 1650
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    div-float/2addr v7, v8

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    .line 1652
    :cond_10
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v7, v8

    sub-float v7, v3, v7

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1655
    :cond_11
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->checkBox()V

    goto/16 :goto_5

    .line 1657
    .end local v3    # "mBoxEndX":F
    :cond_12
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v8, 0x3

    if-ne v7, v8, :cond_13

    .line 1658
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    neg-float v8, v1

    neg-float v9, v2

    invoke-virtual {v7, v8, v9}, Landroid/graphics/RectF;->offset(FF)V

    .line 1660
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    goto/16 :goto_5

    .line 1661
    :cond_13
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v8, 0x5

    if-ne v7, v8, :cond_b

    .line 1662
    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isArabic:Z

    if-eqz v7, :cond_16

    .line 1663
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    sub-float/2addr v7, v1

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    .line 1664
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    cmpl-float v7, v7, v8

    if-lez v7, :cond_14

    .line 1665
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    .line 1667
    :cond_14
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    const/high16 v9, 0x40a00000    # 5.0f

    div-float/2addr v8, v9

    cmpg-float v7, v7, v8

    if-gez v7, :cond_15

    .line 1668
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    const/high16 v8, 0x40a00000    # 5.0f

    div-float/2addr v7, v8

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    .line 1670
    :cond_15
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    add-float/2addr v8, v9

    iput v8, v7, Landroid/graphics/RectF;->right:F

    .line 1682
    :goto_6
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    invoke-virtual {v4, v7}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1683
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    neg-float v7, v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    neg-float v8, v8

    invoke-virtual {v4, v7, v8}, Landroid/graphics/RectF;->offset(FF)V

    .line 1684
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v7, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->updateRect(Landroid/graphics/RectF;)V

    goto/16 :goto_5

    .line 1672
    :cond_16
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    sub-float/2addr v7, v1

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    .line 1673
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    cmpg-float v7, v7, v8

    if-gez v7, :cond_17

    .line 1674
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    .line 1676
    :cond_17
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    const/high16 v9, 0x40800000    # 4.0f

    mul-float/2addr v8, v9

    const/high16 v9, 0x40a00000    # 5.0f

    div-float/2addr v8, v9

    cmpl-float v7, v7, v8

    if-lez v7, :cond_18

    .line 1677
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    const/high16 v8, 0x40800000    # 4.0f

    mul-float/2addr v7, v8

    const/high16 v8, 0x40a00000    # 5.0f

    div-float/2addr v7, v8

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    .line 1679
    :cond_18
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    add-float/2addr v8, v9

    iput v8, v7, Landroid/graphics/RectF;->left:F

    goto :goto_6

    .line 1700
    :pswitch_3
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v8, 0x1

    if-eq v7, v8, :cond_19

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v8, 0x3

    if-eq v7, v8, :cond_19

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_1

    .line 1701
    :cond_19
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_1a

    .line 1702
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setHeightAndRate(FF)V

    .line 1704
    :cond_1a
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-float v7, v8

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1705
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-float v7, v8

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1706
    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1b

    const/4 v7, 0x1

    :goto_7
    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateBox(ZF)V

    .line 1707
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 1708
    const/4 v7, 0x0

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    .line 1709
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->invalidate()V

    goto/16 :goto_1

    .line 1706
    :cond_1b
    const/4 v7, 0x0

    goto :goto_7

    .line 1718
    :cond_1c
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 1572
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private registerPensoundSolution()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 571
    const-string v1, "ZoomPad"

    const-string v2, "registerPensoundSolution() - Start"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    :try_start_0
    sget-boolean v1, Lcom/samsung/audio/SmpsManager;->isSupport:Z

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    .line 583
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-nez v1, :cond_0

    .line 584
    new-instance v1, Lcom/samsung/audio/SmpsManager;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/audio/SmpsManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 585
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v1, :cond_0

    .line 586
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexPencil:I

    .line 587
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexMarker:I

    .line 588
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexBrush:I

    .line 589
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexEraser:I

    .line 590
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->activePen:I

    if-eq v1, v3, :cond_1

    .line 591
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->activePen:I

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 597
    :cond_0
    :goto_0
    const-string v1, "ZoomPad"

    const-string v2, "registerPensoundSolution() - End"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    :goto_1
    return-void

    .line 574
    :catch_0
    move-exception v0

    .line 575
    .local v0, "error":Ljava/lang/UnsatisfiedLinkError;
    const-string v1, "ZoomPad"

    const-string v2, "Smps is disabled in this model"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 576
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z

    goto :goto_1

    .line 578
    .end local v0    # "error":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v0

    .line 579
    .local v0, "error":Ljava/lang/NoClassDefFoundError;
    const-string v1, "ZoomPad"

    const-string v2, "Smps is disabled in this model"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z

    goto :goto_1

    .line 592
    .end local v0    # "error":Ljava/lang/NoClassDefFoundError;
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexPencil:I

    if-eq v1, v3, :cond_0

    .line 593
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexPencil:I

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    goto :goto_0
.end method

.method private relativeCoordinate()Landroid/graphics/RectF;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1024
    new-instance v1, Landroid/graphics/RectF;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmapWidth:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmapHeight:I

    int-to-float v3, v3

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1025
    .local v1, "srcRect":Landroid/graphics/RectF;
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 1026
    .local v0, "dstRect":Landroid/graphics/RectF;
    iget v2, v1, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->left:F

    .line 1027
    iget v2, v1, Landroid/graphics/RectF;->right:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 1028
    iget v2, v1, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 1029
    iget v2, v1, Landroid/graphics/RectF;->bottom:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 1031
    return-object v0
.end method

.method private releaseHapticFeedback()V
    .locals 2

    .prologue
    .line 562
    const-string v0, "ZoomPad"

    const-string v1, "releaseHapticFeedback() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-eqz v0, :cond_0

    .line 564
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    invoke-virtual {v0}, Lcom/samsung/hapticfeedback/HapticEffect;->closeDevice()V

    .line 565
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 567
    :cond_0
    const-string v0, "ZoomPad"

    const-string v1, "releaseHapticFeedback() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    return-void
.end method

.method private unregisterPensoundSolution()V
    .locals 2

    .prologue
    .line 601
    const-string v0, "ZoomPad"

    const-string/jumbo v1, "unregisterPensoundSolution() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    invoke-virtual {v0}, Lcom/samsung/audio/SmpsManager;->onDestroy()V

    .line 604
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 606
    :cond_0
    const-string v0, "ZoomPad"

    const-string/jumbo v1, "unregisterPensoundSolution() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    return-void
.end method


# virtual methods
.method checkBox()V
    .locals 8

    .prologue
    const/high16 v7, -0x40800000    # -1.0f

    const/4 v6, 0x0

    .line 1113
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    if-nez v1, :cond_2

    .line 1114
    :cond_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1115
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1173
    :cond_1
    :goto_0
    return-void

    .line 1118
    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_3

    .line 1119
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT:I

    int-to-float v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    .line 1121
    :cond_3
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    .line 1122
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    div-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    .line 1124
    :cond_4
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_5

    .line 1125
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v0, v2

    .line 1126
    .local v0, "tmpHeight":F
    cmpl-float v1, v0, v6

    if-lez v1, :cond_5

    .line 1127
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    .line 1131
    .end local v0    # "tmpHeight":F
    :cond_5
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    mul-float/2addr v1, v7

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_6

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    cmpl-float v1, v1, v6

    if-eqz v1, :cond_6

    .line 1132
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v1, v7

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1135
    :cond_6
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    mul-float/2addr v1, v7

    float-to-double v2, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v1, v4

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    cmpl-double v1, v2, v4

    if-lez v1, :cond_7

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    cmpl-float v1, v1, v6

    if-eqz v1, :cond_7

    .line 1136
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v1, v7

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1139
    :cond_7
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_8

    .line 1140
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_8

    .line 1141
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1144
    :cond_8
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_9

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_9

    .line 1145
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    sub-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1148
    :cond_9
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_a

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    cmpl-float v1, v1, v6

    if-nez v1, :cond_a

    .line 1149
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1152
    :cond_a
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_b

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_b

    .line 1153
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    if-lez v1, :cond_d

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mReferenceBackground:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_d

    .line 1154
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mReferenceBackground:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1160
    :cond_b
    :goto_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_c

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    cmpl-float v1, v1, v6

    if-nez v1, :cond_c

    .line 1161
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1164
    :cond_c
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    .line 1165
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    if-lez v1, :cond_e

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mReferenceBackground:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_e

    .line 1166
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mReferenceBackground:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    sub-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    goto/16 :goto_0

    .line 1156
    :cond_d
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    goto :goto_1

    .line 1168
    :cond_e
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    sub-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    goto/16 :goto_0
.end method

.method public close()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 348
    const-string v1, "ZoomPad"

    const-string v2, "close"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    if-eqz v1, :cond_0

    .line 351
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    invoke-virtual {v1}, Landroid/util/SparseIntArray;->clear()V

    .line 352
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    .line 354
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v1, :cond_1

    .line 355
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->close()V

    .line 356
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    .line 358
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    .line 359
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_3

    .line 364
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    .line 366
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->unbindDrawables(Landroid/view/View;)V

    .line 367
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    .line 368
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    .line 369
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    .line 370
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    .line 371
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->dstRect:Landroid/graphics/RectF;

    .line 372
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    .line 373
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPrePoint:Landroid/graphics/PointF;

    .line 374
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mParent:Landroid/view/ViewGroup;

    .line 375
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadPaint:Landroid/graphics/Paint;

    .line 376
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxPaint:Landroid/graphics/Paint;

    .line 377
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 378
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    .line 379
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    .line 380
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    .line 381
    return-void

    .line 359
    :cond_3
    aget-object v0, v2, v1

    .line 360
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_4

    .line 361
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 359
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getBoxHeight()F
    .locals 1

    .prologue
    .line 668
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    return v0
.end method

.method public getDrawingRectF()Landroid/graphics/RectF;
    .locals 5

    .prologue
    .line 1351
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method

.method getIntValueAppliedDensity(F)I
    .locals 1
    .param p1, "paramFloat"    # F

    .prologue
    .line 2198
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public getPadRectF()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 1347
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method getResourceString(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "strName"    # Ljava/lang/String;

    .prologue
    .line 2182
    const/4 v2, 0x0

    .line 2183
    .local v2, "string":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    if-eqz v3, :cond_1

    .line 2185
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    const-string/jumbo v4, "string"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 2186
    .local v1, "resId":I
    if-nez v1, :cond_0

    .line 2187
    const/4 v3, 0x0

    .line 2194
    .end local v1    # "resId":I
    :goto_0
    return-object v3

    .line 2189
    .restart local v1    # "resId":I
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .end local v1    # "resId":I
    :cond_1
    :goto_1
    move-object v3, v2

    .line 2194
    goto :goto_0

    .line 2190
    :catch_0
    move-exception v0

    .line 2191
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method getStrokeBoxRect()Landroid/graphics/Rect;
    .locals 4

    .prologue
    .line 1327
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1328
    .local v0, "rect":Landroid/graphics/Rect;
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1329
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 1330
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1331
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1333
    return-object v0
.end method

.method public getStrokeBoxRectF()Landroid/graphics/RectF;
    .locals 4

    .prologue
    .line 1337
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 1338
    .local v0, "rect":Landroid/graphics/RectF;
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 1339
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 1340
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 1341
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 1343
    return-object v0
.end method

.method public isEnabled()Z
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    .prologue
    .line 643
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnable:Z

    return v0
.end method

.method public isStroking()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1355
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsStrokeDrawing:Z

    if-eqz v1, :cond_0

    .line 1356
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/16 v2, 0x9

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 1358
    :cond_0
    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 970
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->dstRect:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 1021
    :cond_0
    :goto_0
    return-void

    .line 974
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float v4, v0, v2

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 975
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float/2addr v2, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v3, v3

    add-float/2addr v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v4, v4

    add-float/2addr v0, v4

    .line 976
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadPaint:Landroid/graphics/Paint;

    move-object v0, p1

    .line 975
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 977
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v2, v2

    add-float/2addr v0, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v2, v3

    add-float v3, v0, v2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float v4, v0, v2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v5, v0

    .line 978
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float/2addr v0, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float v6, v0, v2

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadPaint:Landroid/graphics/Paint;

    move-object v2, p1

    .line 977
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 979
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v2, v2

    add-float/2addr v0, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v2, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 982
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isArabic:Z

    if-eqz v0, :cond_3

    .line 983
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->dstRect:Landroid/graphics/RectF;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v1, v1

    .line 984
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 985
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    .line 986
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v4, v5

    float-to-int v4, v4

    int-to-float v4, v4

    .line 983
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 987
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->dstRect:Landroid/graphics/RectF;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE_GAP:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE_GAP:I

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 995
    :goto_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeightChange:Z

    if-eqz v0, :cond_2

    .line 996
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    if-eq v0, v9, :cond_5

    .line 997
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isArabic:Z

    if-eqz v0, :cond_4

    .line 998
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v10

    if-eqz v0, :cond_2

    .line 999
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v10

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->dstRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v8, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1018
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getStrokeBoxRect()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1020
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 989
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->dstRect:Landroid/graphics/RectF;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v1, v1

    .line 990
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 991
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v4, v5

    float-to-int v4, v4

    int-to-float v4, v4

    .line 989
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 992
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->dstRect:Landroid/graphics/RectF;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE_GAP:I

    neg-int v1, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE_GAP:I

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    goto :goto_1

    .line 1002
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v9

    if-eqz v0, :cond_2

    .line 1003
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v9

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->dstRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v8, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_2

    .line 1007
    :cond_5
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isArabic:Z

    if-eqz v0, :cond_6

    .line 1008
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    if-eqz v0, :cond_2

    .line 1009
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v10

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->dstRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v8, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 1012
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v11

    if-eqz v0, :cond_2

    .line 1013
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v11

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->dstRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v8, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_2
.end method

.method protected onLayout(ZIIII)V
    .locals 10
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    .line 915
    sub-int v3, p4, p2

    .line 916
    .local v3, "w":I
    sub-int v0, p5, p3

    .line 917
    .local v0, "h":I
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    if-ne v5, v3, :cond_0

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    if-eq v5, v0, :cond_1

    .line 918
    :cond_0
    const-string v5, "ZoomPad"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "onLayout w="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " h="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 919
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    .line 920
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    .line 922
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v4

    .line 923
    .local v4, "width":F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v2

    .line 924
    .local v2, "height":F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v6, v6

    sub-float/2addr v6, v4

    div-float/2addr v6, v9

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v7, v7

    sub-float/2addr v7, v2

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v8, v8

    sub-float/2addr v8, v4

    div-float/2addr v8, v9

    add-float/2addr v8, v4

    .line 925
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v9, v9

    .line 924
    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 927
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 928
    .local v1, "handler":Landroid/os/Handler;
    new-instance v5, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;

    invoke-direct {v5, p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V

    .line 962
    const-wide/16 v6, 0x0

    .line 928
    invoke-virtual {v1, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 964
    .end local v1    # "handler":Landroid/os/Handler;
    .end local v2    # "height":F
    .end local v4    # "width":F
    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 19
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1367
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    if-nez v2, :cond_1

    .line 1368
    :cond_0
    const/4 v2, 0x0

    .line 1558
    :goto_0
    return v2

    .line 1370
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnable:Z

    if-eqz v2, :cond_2a

    .line 1372
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onPreTouch(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1373
    const/4 v2, 0x1

    goto :goto_0

    .line 1375
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isFirstCanvasTouch:Z

    if-eqz v2, :cond_3

    .line 1376
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->absoluteCoordinate(Landroid/view/MotionEvent;Z)Landroid/view/MotionEvent;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onUpdate(Landroid/view/MotionEvent;)V

    .line 1377
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isFirstCanvasTouch:Z

    .line 1379
    :cond_3
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->onTouchSelection(Landroid/view/MotionEvent;)Z

    .line 1381
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v8, v2, 0xff

    .line 1382
    .local v8, "action":I
    new-instance v16, Landroid/graphics/RectF;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    sub-float/2addr v5, v6

    move-object/from16 v0, v16

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1384
    .local v16, "tmpRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/16 v3, 0x9

    if-ne v2, v3, :cond_29

    .line 1385
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    neg-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    add-float/2addr v3, v4

    neg-float v3, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 1387
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/util/SparseIntArray;->get(I)I

    move-result v17

    .line 1388
    .local v17, "toolTypeAction":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preToolType:I

    move/from16 v0, v17

    if-eq v2, v0, :cond_6

    .line 1389
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preToolType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    const/4 v2, 0x2

    move/from16 v0, v17

    if-eq v0, v2, :cond_6

    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preToolType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    const/4 v2, 0x1

    move/from16 v0, v17

    if-eq v0, v2, :cond_6

    .line 1390
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->refresh()V

    .line 1394
    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    .line 1395
    .local v9, "newX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    .line 1397
    .local v10, "newY":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_e

    .line 1398
    const/4 v2, 0x2

    move/from16 v0, v17

    if-eq v0, v2, :cond_7

    const/4 v2, 0x3

    move/from16 v0, v17

    if-eq v0, v2, :cond_7

    const/4 v2, 0x4

    move/from16 v0, v17

    if-ne v0, v2, :cond_e

    .line 1399
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v2, :cond_9

    .line 1400
    const/4 v2, 0x3

    move/from16 v0, v17

    if-eq v0, v2, :cond_8

    const/4 v2, 0x4

    move/from16 v0, v17

    if-ne v0, v2, :cond_9

    .line 1401
    :cond_8
    if-nez v8, :cond_14

    .line 1402
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexEraser:I

    invoke-virtual {v2, v3}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 1408
    :cond_9
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v14

    .line 1409
    .local v14, "tempX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v15

    .line 1410
    .local v15, "tempY":F
    move v12, v14

    .line 1411
    .local v12, "originX":F
    move v13, v15

    .line 1413
    .local v13, "originY":F
    move-object/from16 v0, v16

    iget v2, v0, Landroid/graphics/RectF;->left:F

    cmpg-float v2, v14, v2

    if-gez v2, :cond_15

    .line 1414
    move-object/from16 v0, v16

    iget v14, v0, Landroid/graphics/RectF;->left:F

    .line 1415
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isSoundEnabled:Z

    .line 1421
    :cond_a
    :goto_2
    move-object/from16 v0, v16

    iget v2, v0, Landroid/graphics/RectF;->top:F

    cmpg-float v2, v15, v2

    if-gez v2, :cond_16

    .line 1422
    move-object/from16 v0, v16

    iget v15, v0, Landroid/graphics/RectF;->top:F

    .line 1423
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isSoundEnabled:Z

    .line 1429
    :cond_b
    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1431
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isSoundEnabled:Z

    if-eqz v2, :cond_1a

    .line 1432
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v2, :cond_c

    .line 1433
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/audio/SmpsManager;->generateSound(Landroid/view/MotionEvent;)V

    .line 1436
    :cond_c
    if-nez v8, :cond_17

    .line 1437
    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOldX:F

    .line 1438
    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOldY:F

    .line 1459
    :cond_d
    :goto_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1460
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isSoundEnabled:Z

    .line 1462
    .end local v12    # "originX":F
    .end local v13    # "originY":F
    .end local v14    # "tempX":F
    .end local v15    # "tempY":F
    :cond_e
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_1b

    .line 1463
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsMultiTouch:Z

    .line 1468
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    if-eqz v2, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_11

    .line 1470
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preToolType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_f

    const/4 v2, 0x2

    move/from16 v0, v17

    if-eq v0, v2, :cond_f

    .line 1471
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1472
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->onTouchInput(Landroid/view/MotionEvent;)Z

    .line 1473
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    .line 1475
    :cond_f
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preToolType:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_10

    const/4 v2, 0x3

    move/from16 v0, v17

    if-eq v0, v2, :cond_10

    .line 1476
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1477
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->onTouchEraser(Landroid/view/MotionEvent;)Z

    .line 1478
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    .line 1480
    :cond_10
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preToolType:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_11

    .line 1481
    const/4 v2, 0x4

    move/from16 v0, v17

    if-eq v0, v2, :cond_11

    .line 1482
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1483
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->onTouchRemover(Landroid/view/MotionEvent;)Z

    .line 1484
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    .line 1488
    :cond_11
    const/4 v2, 0x2

    move/from16 v0, v17

    if-ne v0, v2, :cond_1c

    .line 1490
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsStrokeDrawing:Z

    .line 1491
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->onTouchInput(Landroid/view/MotionEvent;)Z

    .line 1492
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    .line 1540
    :cond_12
    :goto_6
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preToolType:I

    .line 1554
    .end local v9    # "newX":F
    .end local v10    # "newY":F
    .end local v17    # "toolTypeAction":I
    :cond_13
    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    if-eqz v2, :cond_2a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onPostTouch(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 1555
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1403
    .restart local v9    # "newX":F
    .restart local v10    # "newY":F
    .restart local v17    # "toolTypeAction":I
    :cond_14
    const/4 v2, 0x1

    if-ne v8, v2, :cond_9

    .line 1404
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->activePen:I

    invoke-virtual {v2, v3}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    goto/16 :goto_1

    .line 1416
    .restart local v12    # "originX":F
    .restart local v13    # "originY":F
    .restart local v14    # "tempX":F
    .restart local v15    # "tempY":F
    :cond_15
    move-object/from16 v0, v16

    iget v2, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->width()F

    move-result v3

    add-float/2addr v2, v3

    cmpl-float v2, v14, v2

    if-lez v2, :cond_a

    .line 1417
    move-object/from16 v0, v16

    iget v2, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->width()F

    move-result v3

    add-float v14, v2, v3

    .line 1418
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isSoundEnabled:Z

    goto/16 :goto_2

    .line 1424
    :cond_16
    move-object/from16 v0, v16

    iget v2, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->height()F

    move-result v3

    add-float/2addr v2, v3

    cmpl-float v2, v15, v2

    if-lez v2, :cond_b

    .line 1425
    move-object/from16 v0, v16

    iget v2, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->height()F

    move-result v3

    add-float v15, v2, v3

    .line 1426
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isSoundEnabled:Z

    goto/16 :goto_3

    .line 1439
    :cond_17
    const/4 v2, 0x2

    if-ne v8, v2, :cond_19

    .line 1441
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-eqz v2, :cond_18

    .line 1442
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOldX:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOldY:F

    invoke-virtual {v2, v3, v4, v9, v10}, Lcom/samsung/hapticfeedback/HapticEffect;->playEffectByDistance(FFFF)V

    .line 1444
    :cond_18
    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOldX:F

    .line 1445
    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOldY:F

    goto/16 :goto_4

    .line 1446
    :cond_19
    const/4 v2, 0x1

    if-ne v8, v2, :cond_d

    .line 1448
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-eqz v2, :cond_d

    .line 1449
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    invoke-virtual {v2}, Lcom/samsung/hapticfeedback/HapticEffect;->stopAllEffect()V

    goto/16 :goto_4

    .line 1452
    :cond_1a
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v2, :cond_d

    .line 1453
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v11

    .line 1455
    .local v11, "originAction":I
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1456
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/audio/SmpsManager;->generateSound(Landroid/view/MotionEvent;)V

    .line 1457
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->setAction(I)V

    goto/16 :goto_4

    .line 1465
    .end local v11    # "originAction":I
    .end local v12    # "originX":F
    .end local v13    # "originY":F
    .end local v14    # "tempX":F
    .end local v15    # "tempY":F
    :cond_1b
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsMultiTouch:Z

    goto/16 :goto_5

    .line 1494
    :cond_1c
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsStrokeDrawing:Z

    .line 1495
    const/4 v2, 0x3

    move/from16 v0, v17

    if-eq v0, v2, :cond_1d

    .line 1496
    const/4 v2, 0x4

    move/from16 v0, v17

    if-eq v0, v2, :cond_1d

    .line 1497
    const/4 v2, 0x5

    move/from16 v0, v17

    if-ne v0, v2, :cond_12

    .line 1498
    :cond_1d
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_24

    .line 1499
    const/4 v2, 0x2

    if-ne v8, v2, :cond_1e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    if-nez v2, :cond_1e

    .line 1500
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1502
    :cond_1e
    const/4 v2, 0x3

    move/from16 v0, v17

    if-ne v0, v2, :cond_22

    .line 1503
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->onTouchEraser(Landroid/view/MotionEvent;)Z

    .line 1507
    :cond_1f
    :goto_8
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsMultiTouch:Z

    if-nez v2, :cond_20

    .line 1508
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v4, v4

    add-float/2addr v4, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1509
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v5, v5

    add-float/2addr v5, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    sub-float/2addr v6, v7

    div-float v6, v3, v6

    .line 1510
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->height()F

    move-result v18

    sub-float v7, v7, v18

    div-float v7, v3, v7

    move-object/from16 v3, p1

    .line 1508
    invoke-interface/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onViewTouchEvent(Landroid/view/MotionEvent;FFFF)V

    .line 1512
    :cond_20
    const/4 v2, 0x1

    if-eq v8, v2, :cond_21

    const/4 v2, 0x3

    if-ne v8, v2, :cond_23

    .line 1513
    :cond_21
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 1517
    :goto_9
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    goto/16 :goto_6

    .line 1504
    :cond_22
    const/4 v2, 0x4

    move/from16 v0, v17

    if-ne v0, v2, :cond_1f

    .line 1505
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->onTouchRemover(Landroid/view/MotionEvent;)Z

    goto :goto_8

    .line 1515
    :cond_23
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateFrameBuffer()V

    goto :goto_9

    .line 1519
    :cond_24
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    if-eqz v2, :cond_12

    .line 1520
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_25

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_12

    .line 1521
    :cond_25
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1522
    const/4 v2, 0x3

    move/from16 v0, v17

    if-ne v0, v2, :cond_28

    .line 1523
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->onTouchEraser(Landroid/view/MotionEvent;)Z

    .line 1527
    :cond_26
    :goto_a
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsMultiTouch:Z

    if-nez v2, :cond_27

    .line 1528
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v4, v4

    add-float/2addr v4, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1529
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v5, v5

    add-float/2addr v5, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    sub-float/2addr v6, v7

    div-float v6, v3, v6

    .line 1530
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->height()F

    move-result v18

    sub-float v7, v7, v18

    div-float v7, v3, v7

    move-object/from16 v3, p1

    .line 1528
    invoke-interface/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onViewTouchEvent(Landroid/view/MotionEvent;FFFF)V

    .line 1532
    :cond_27
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    .line 1534
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    goto/16 :goto_6

    .line 1524
    :cond_28
    const/4 v2, 0x4

    move/from16 v0, v17

    if-ne v0, v2, :cond_26

    .line 1525
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->onTouchRemover(Landroid/view/MotionEvent;)Z

    goto :goto_a

    .line 1542
    .end local v9    # "newX":F
    .end local v10    # "newY":F
    .end local v17    # "toolTypeAction":I
    :cond_29
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/16 v3, 0x9

    if-eq v2, v3, :cond_13

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v3, 0x5

    if-eq v2, v3, :cond_13

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    if-eqz v2, :cond_13

    .line 1543
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isRefreshed()Z

    move-result v2

    if-nez v2, :cond_13

    .line 1544
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->refresh()V

    .line 1545
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_13

    .line 1546
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    goto/16 :goto_7

    .line 1558
    .end local v8    # "action":I
    .end local v16    # "tmpRect":Landroid/graphics/RectF;
    :cond_2a
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 629
    const-string v0, "ZoomPad"

    const-string v1, "onWindowFocusChanged() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    if-eqz p1, :cond_0

    .line 631
    const-string v0, "ZoomPad"

    const-string v1, "onWindowFocusChanged() - hasWindowFocus : true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->registerPensoundSolution()V

    .line 637
    :goto_0
    const-string v0, "ZoomPad"

    const-string v1, "onWindowFocusChanged() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 638
    return-void

    .line 634
    :cond_0
    const-string v0, "ZoomPad"

    const-string v1, "onWindowFocusChanged() - hasWindowFocus : false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->unregisterPensoundSolution()V

    goto :goto_0
.end method

.method public refresh()V
    .locals 1

    .prologue
    .line 2238
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isRefreshed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2239
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->refresh()V

    .line 2241
    :cond_0
    return-void
.end method

.method resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;
    .locals 14
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "resId"    # I
    .param p3, "iconWidth"    # I
    .param p4, "iconHeight"    # I

    .prologue
    .line 2204
    invoke-virtual/range {p1 .. p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v13

    .line 2205
    .local v13, "stream":Ljava/io/InputStream;
    invoke-static {v13}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2208
    .local v1, "BitmapOrg":Landroid/graphics/Bitmap;
    if-nez v1, :cond_0

    .line 2209
    const/4 v2, 0x0

    .line 2233
    :goto_0
    return-object v2

    .line 2211
    :cond_0
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 2212
    .local v4, "width":I
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 2213
    .local v5, "height":I
    move/from16 v0, p3

    int-to-float v2, v0

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getIntValueAppliedDensity(F)I

    move-result v9

    .line 2214
    .local v9, "newWidth":I
    move/from16 v0, p4

    int-to-float v2, v0

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 2217
    .local v8, "newHeight":I
    int-to-float v2, v9

    int-to-float v3, v4

    div-float v12, v2, v3

    .line 2218
    .local v12, "scaleWidth":F
    int-to-float v2, v8

    int-to-float v3, v5

    div-float v11, v2, v3

    .line 2221
    .local v11, "scaleHeight":F
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 2223
    .local v6, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v6, v12, v11}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 2229
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x1

    invoke-static/range {v1 .. v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 2233
    .local v10, "resizedBitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, p1, v10}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public setActionListener(Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    .prologue
    .line 910
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    .line 911
    return-void
.end method

.method public setArabicMode(Z)V
    .locals 0
    .param p1, "arabic"    # Z

    .prologue
    .line 647
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isArabic:Z

    .line 648
    return-void
.end method

.method public setBoxHeight(F)V
    .locals 7
    .param p1, "height"    # F

    .prologue
    const/4 v6, 0x0

    .line 651
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeightChange:Z

    if-nez v0, :cond_1

    .line 665
    :cond_0
    :goto_0
    return-void

    .line 654
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->initialize()V

    .line 655
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    mul-float/2addr v0, v1

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    float-to-double v0, p1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_HEIGHT_LIMIT_FLOAT:F

    float-to-double v2, v2

    const-wide/high16 v4, 0x400c000000000000L    # 3.5

    mul-double/2addr v2, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    cmpl-double v0, v0, v2

    if-gtz v0, :cond_0

    .line 658
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsStrokeDrawing:Z

    .line 659
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    .line 660
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->checkBox()V

    .line 661
    const/4 v0, 0x0

    invoke-virtual {p0, v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateBox(ZF)V

    .line 662
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 663
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setHeightAndRate(FF)V

    .line 664
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->invalidate()V

    goto :goto_0
.end method

.method public setBoxHeightEnabled(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 807
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeightChange:Z

    .line 808
    return-void
.end method

.method public setBoxPosition(FF)V
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 672
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 674
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsStrokeDrawing:Z

    .line 676
    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v3, v4, :cond_1

    .line 677
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 678
    .local v2, "widthPixels":I
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 684
    .local v1, "heightPixels":I
    :goto_0
    cmpg-float v3, p1, v6

    if-ltz v3, :cond_0

    int-to-float v3, v2

    cmpl-float v3, p1, v3

    if-gtz v3, :cond_0

    cmpg-float v3, p2, v6

    if-ltz v3, :cond_0

    int-to-float v3, v1

    cmpl-float v3, p2, v3

    if-lez v3, :cond_2

    .line 707
    :cond_0
    :goto_1
    return-void

    .line 680
    .end local v1    # "heightPixels":I
    .end local v2    # "widthPixels":I
    :cond_1
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 681
    .restart local v2    # "widthPixels":I
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .restart local v1    # "heightPixels":I
    goto :goto_0

    .line 687
    :cond_2
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isArabic:Z

    if-eqz v3, :cond_4

    .line 688
    const-string v3, ""

    const-string v4, "hanzz in SetBoxPosition"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, v2, v3

    int-to-float v3, v3

    sub-float/2addr v3, p1

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 690
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "hanzz in SetBoxPosition 2 : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 703
    :cond_3
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->checkBox()V

    .line 704
    invoke-virtual {p0, v7, v6}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateBox(ZF)V

    .line 705
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 706
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->invalidate()V

    goto :goto_1

    .line 692
    :cond_4
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 693
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 695
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    cmpg-float v3, v3, v6

    if-gez v3, :cond_5

    .line 696
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 698
    :cond_5
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    if-eqz v3, :cond_3

    .line 699
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    sub-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    goto :goto_2
.end method

.method public setButtonEnabled(ZZZZZ)V
    .locals 1
    .param p1, "left"    # Z
    .param p2, "right"    # Z
    .param p3, "enter"    # Z
    .param p4, "up"    # Z
    .param p5, "down"    # Z

    .prologue
    .line 811
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mLeftButton:Z

    .line 812
    iput-boolean p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRightButton:Z

    .line 813
    iput-boolean p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnterButton:Z

    .line 814
    iput-boolean p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mUpButton:Z

    .line 815
    iput-boolean p5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDownButton:Z

    .line 816
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnable:Z

    if-nez v0, :cond_1

    .line 817
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 818
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->unbindDrawables(Landroid/view/View;)V

    .line 820
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->makeLayout()Landroid/widget/RelativeLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    .line 822
    :cond_1
    return-void
.end method

.method setButtonState(IZ)V
    .locals 8
    .param p1, "direction"    # I
    .param p2, "isHandleMsg"    # Z

    .prologue
    const/4 v7, 0x0

    const v4, 0x38d1b717    # 1.0E-4f

    const/4 v6, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    .line 1035
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    if-nez v2, :cond_1

    .line 1110
    :cond_0
    :goto_0
    return-void

    .line 1038
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->preEvent:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    if-eqz p2, :cond_0

    .line 1041
    :cond_2
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    .line 1042
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsStrokeDrawing:Z

    .line 1043
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    invoke-virtual {v2, v7, v6}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->setMovingEnabled(ZF)V

    .line 1044
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMoveHandler:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->removeMessages(I)V

    .line 1045
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v2, v3

    div-float/2addr v2, v5

    float-to-int v0, v2

    .line 1046
    .local v0, "diff":I
    const/4 v1, 0x0

    .line 1047
    .local v1, "save":Z
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1049
    :pswitch_1
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    cmpg-float v2, v2, v6

    if-gtz v2, :cond_5

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    sub-float/2addr v2, v6

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v4

    if-gez v2, :cond_5

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v4

    if-lez v2, :cond_5

    .line 1050
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onChangePan(FF)V

    .line 1051
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-float v2, v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1052
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isArabic:Z

    if-eqz v2, :cond_4

    .line 1053
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v5

    add-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1104
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->checkBox()V

    .line 1105
    invoke-virtual {p0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateBox(ZF)V

    .line 1106
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 1108
    const-string v2, "ZoomPad"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "direction = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1109
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->invalidate()V

    goto/16 :goto_0

    .line 1055
    :cond_4
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v5

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    goto :goto_1

    .line 1058
    :cond_5
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    int-to-float v3, v0

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    goto :goto_1

    .line 1063
    :pswitch_2
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_8

    .line 1064
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_8

    .line 1067
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v2, v3

    float-to-int v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v3, v4

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_6

    .line 1068
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_3

    .line 1069
    :cond_6
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isArabic:Z

    if-eqz v2, :cond_7

    .line 1070
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v5

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1074
    :goto_2
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1075
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    invoke-interface {v2, v6, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onChangePan(FF)V

    goto/16 :goto_1

    .line 1072
    :cond_7
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v5

    add-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    goto :goto_2

    .line 1078
    :cond_8
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    int-to-float v3, v0

    add-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    goto/16 :goto_1

    .line 1083
    :pswitch_3
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v5

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    goto/16 :goto_1

    .line 1087
    :pswitch_4
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v5

    add-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    goto/16 :goto_1

    .line 1091
    :pswitch_5
    const-string v2, "ZoomPad"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "STATE_BTN_ENTER=["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxSaveStartX:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxSaveStartY:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1092
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v2, v3

    float-to-int v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v3, v4

    if-ne v2, v3, :cond_9

    .line 1093
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_a

    .line 1094
    :cond_9
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v3, v5

    add-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1095
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxSaveStartX:F

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1096
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaSaveX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onChangePan(FF)V

    .line 1098
    :cond_a
    const/4 v1, 0x1

    .line 1099
    goto/16 :goto_1

    .line 1047
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method setDrawableBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "drawableName"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 2119
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    if-nez v4, :cond_1

    .line 2157
    :cond_0
    :goto_0
    return-object v0

    .line 2122
    :cond_1
    const/4 v2, 0x0

    .line 2123
    .local v2, "drawable":Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "drawable"

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, p1, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 2125
    .local v3, "mDrawableResID":I
    if-nez v3, :cond_3

    .line 2126
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    const-string v5, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, p1, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 2127
    if-eqz v3, :cond_0

    .line 2130
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {p0, v4, v3, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2132
    const/4 v0, 0x0

    .line 2133
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_0

    .line 2134
    instance-of v4, v2, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v4, :cond_2

    .line 2135
    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 2137
    .restart local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_2
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    .line 2138
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 2137
    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2139
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2140
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v5

    invoke-virtual {v2, v7, v7, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2141
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 2145
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {p0, v4, v3, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2147
    const/4 v0, 0x0

    .line 2148
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_0

    .line 2149
    instance-of v4, v2, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v4, :cond_4

    .line 2150
    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 2152
    .restart local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_4
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2153
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2154
    .restart local v1    # "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v5

    invoke-virtual {v2, v7, v7, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2155
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method

.method setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p1, "drawableName"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v2, 0x0

    .line 2161
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    if-nez v3, :cond_1

    :cond_0
    move-object v0, v2

    .line 2178
    :goto_0
    return-object v0

    .line 2164
    :cond_1
    const/4 v0, 0x0

    .line 2165
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string v4, "drawable"

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 2167
    .local v1, "mDrawableResID":I
    if-nez v1, :cond_3

    .line 2168
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    const-string v4, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 2169
    if-nez v1, :cond_2

    move-object v0, v2

    .line 2170
    goto :goto_0

    .line 2172
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {p0, v2, v1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2174
    goto :goto_0

    .line 2176
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0, v2, v1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2178
    goto :goto_0
.end method

.method setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "defaultImg"    # Ljava/lang/String;
    .param p2, "pressedImg"    # Ljava/lang/String;
    .param p3, "focusedImg"    # Ljava/lang/String;
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2096
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 2097
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 2098
    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 2099
    invoke-virtual {p0, p1, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2098
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 2102
    :cond_0
    if-eqz p2, :cond_1

    .line 2103
    new-array v1, v4, [I

    const v2, 0x10100a7

    aput v2, v1, v3

    .line 2104
    invoke-virtual {p0, p2, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2103
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 2106
    :cond_1
    if-eqz p3, :cond_2

    .line 2107
    new-array v1, v4, [I

    const v2, 0x101009c

    aput v2, v1, v3

    .line 2108
    invoke-virtual {p0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2107
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 2110
    :cond_2
    if-eqz p2, :cond_3

    .line 2111
    new-array v1, v4, [I

    const v2, 0x10100a1

    aput v2, v1, v3

    .line 2112
    invoke-virtual {p0, p2, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2111
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 2115
    :cond_3
    return-object v0

    .line 2098
    nop

    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
        -0x101009c
    .end array-data
.end method

.method public setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .prologue
    .line 787
    const-string v0, "ZoomPad"

    const-string v1, "setEraserSettingInfo"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 788
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v0, :cond_0

    .line 789
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    .line 791
    :cond_0
    return-void
.end method

.method public setLayer(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 753
    .local p1, "layer":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v0, :cond_0

    .line 754
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setLayer(Ljava/util/ArrayList;)V

    .line 756
    :cond_0
    return-void
.end method

.method public setPadPosition(FF)V
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 710
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->initialize()V

    .line 711
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsStrokeDrawing:Z

    .line 713
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float v0, p1, v2

    .line 714
    .local v0, "dx":F
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    sub-float v1, p2, v2

    .line 715
    .local v1, "dy":F
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 716
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    .line 717
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->invalidate()V

    .line 718
    return-void
.end method

.method public setPanAndZoom(FFFFF)V
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "maxX"    # F
    .param p4, "maxY"    # F
    .param p5, "ratio"    # F

    .prologue
    .line 721
    const-string v0, "ZoomPad"

    const-string v1, "setPanAndZoom"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 722
    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    .line 723
    float-to-double v0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    .line 724
    float-to-double v0, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F

    .line 725
    float-to-double v0, p4

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    .line 726
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v0, :cond_0

    .line 727
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v0, p1, p2, p5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setPanAndZoom(FFF)V

    .line 728
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    cmpl-float v0, v0, p5

    if-eqz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p5, v0

    if-gez v0, :cond_0

    .line 729
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->createBitmap(II)V

    .line 732
    :cond_0
    iput p5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    .line 734
    return-void
.end method

.method public setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 4
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .prologue
    .line 764
    const-string v0, "ZoomPad"

    const-string v1, "setPenSettingInfo"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v0, :cond_0

    .line 766
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 770
    :cond_0
    if-eqz p1, :cond_3

    .line 771
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->bIsSupport:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v0, :cond_3

    .line 772
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 773
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 774
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexPencil:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->activePen:I

    .line 780
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->activePen:I

    invoke-virtual {v0, v1}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 781
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->convertPenNameToMaxThicknessValue(Ljava/lang/String;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/samsung/audio/SmpsManager;->setThickness(D)Z

    .line 784
    :cond_3
    return-void

    .line 775
    :cond_4
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 776
    :cond_5
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexBrush:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->activePen:I

    goto :goto_0

    .line 777
    :cond_6
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 778
    :cond_7
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIndexMarker:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->activePen:I

    goto :goto_0
.end method

.method public setReference(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "background"    # Landroid/graphics/Bitmap;

    .prologue
    .line 801
    if-eqz p1, :cond_0

    .line 802
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mReferenceBackground:Landroid/graphics/Bitmap;

    .line 804
    :cond_0
    return-void
.end method

.method public setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    .line 794
    const-string v0, "ZoomPad"

    const-string v1, "setRemoverSettingInfo"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 795
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v0, :cond_0

    .line 796
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    .line 798
    :cond_0
    return-void
.end method

.method public setScreenSize(II)V
    .locals 2
    .param p1, "widht"    # I
    .param p2, "height"    # I

    .prologue
    .line 737
    const-string v0, "ZoomPad"

    const-string v1, "setScreenSize"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 739
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    .line 740
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    .line 741
    return-void
.end method

.method public setScreenStart(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 744
    const-string v0, "ZoomPad"

    const-string v1, "setScreenStart"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    .line 746
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    .line 747
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v0, :cond_0

    .line 748
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setScreenStart(II)V

    .line 750
    :cond_0
    return-void
.end method

.method public setToolTypeAction(II)V
    .locals 2
    .param p1, "toolType"    # I
    .param p2, "action"    # I

    .prologue
    .line 759
    const-string v0, "ZoomPad"

    const-string v1, "setToolTypeAction"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mToolAndActionMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 761
    return-void
.end method

.method public start(Landroid/view/ViewGroup;II)V
    .locals 8
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "bitmapW"    # I
    .param p3, "bitmapH"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 476
    const-string v1, "ZoomPad"

    const-string/jumbo v2, "start"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnable:Z

    if-nez v1, :cond_0

    .line 478
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isFirstCanvasTouch:Z

    .line 479
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mParent:Landroid/view/ViewGroup;

    .line 481
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->initialize()V

    .line 482
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mParent:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 484
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    const-string/jumbo v3, "zoompad_bg"

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    float-to-int v4, v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v3

    aput-object v3, v1, v2

    .line 485
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    const-string/jumbo v2, "zoompad_handle_bottom"

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_WIDTH:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->PAD_HANDLE_HEIGHT:I

    invoke-virtual {p0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v6

    .line 487
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    const/4 v2, 0x2

    const-string/jumbo v3, "zoompad_selected_handle_normal"

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    invoke-virtual {p0, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v3

    aput-object v3, v1, v2

    .line 488
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    const/4 v2, 0x3

    const-string/jumbo v3, "zoompad_selected_handle_press"

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    invoke-virtual {p0, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v3

    aput-object v3, v1, v2

    .line 489
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    const/4 v2, 0x4

    const-string/jumbo v3, "zoompad_selected_handle_normal_arabic"

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    .line 490
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    .line 489
    invoke-virtual {p0, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v3

    aput-object v3, v1, v2

    .line 491
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    const/4 v2, 0x5

    const-string/jumbo v3, "zoompad_selected_handle_normal_arabic"

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    .line 492
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->STROKE_BOX_RESIZE:I

    .line 491
    invoke-virtual {p0, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setDrawableBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v3

    aput-object v3, v1, v2

    .line 498
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmap:[Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setBitmap([Landroid/graphics/Bitmap;)V

    .line 500
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmapWidth:I

    .line 501
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBitmapHeight:I

    .line 503
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->checkBox()V

    .line 504
    invoke-virtual {p0, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateBox(ZF)V

    .line 505
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    .line 507
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    new-instance v2, Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-direct {v2, v7, v7, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setRect(Landroid/graphics/RectF;)V

    .line 508
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 509
    .local v0, "tmpRect":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    neg-float v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    neg-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 510
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->updateRect(Landroid/graphics/RectF;)V

    .line 511
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->createBitmap(II)V

    .line 513
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 514
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->invalidate()V

    .line 515
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->initHapticFeedback()V

    .line 516
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->registerPensoundSolution()V

    .line 518
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnable:Z

    .line 520
    .end local v0    # "tmpRect":Landroid/graphics/RectF;
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 523
    const-string v0, "ZoomPad"

    const-string/jumbo v1, "stop"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnable:Z

    if-eqz v0, :cond_3

    .line 525
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnable:Z

    .line 526
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mIsStrokeDrawing:Z

    .line 527
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mReferenceBackground:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 528
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mReferenceBackground:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 529
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mReferenceBackground:Landroid/graphics/Bitmap;

    .line 531
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 532
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mParent:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 533
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mParent:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 535
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    if-eqz v0, :cond_2

    .line 536
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onStop()V

    .line 539
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->releaseHapticFeedback()V

    .line 540
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->unregisterPensoundSolution()V

    .line 542
    :cond_3
    return-void
.end method

.method unbindDrawables(Landroid/view/View;)V
    .locals 4
    .param p1, "root"    # Landroid/view/View;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2245
    if-nez p1, :cond_1

    .line 2285
    :cond_0
    :goto_0
    return-void

    .line 2249
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_7

    .line 2250
    invoke-virtual {p1, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2255
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2256
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2258
    :cond_2
    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_3

    .line 2259
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    move-object v1, p1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_8

    .line 2262
    instance-of v1, p1, Landroid/widget/AdapterView;

    if-nez v1, :cond_9

    move-object v1, p1

    .line 2263
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2269
    .end local v0    # "i":I
    :cond_3
    :goto_3
    instance-of v1, p1, Landroid/widget/SeekBar;

    if-eqz v1, :cond_4

    move-object v1, p1

    .line 2270
    check-cast v1, Landroid/widget/SeekBar;

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2273
    :cond_4
    instance-of v1, p1, Landroid/widget/ImageView;

    if-eqz v1, :cond_5

    move-object v1, p1

    .line 2274
    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object v1, p1

    .line 2275
    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2277
    :cond_5
    instance-of v1, p1, Landroid/widget/ImageButton;

    if-eqz v1, :cond_6

    move-object v1, p1

    .line 2278
    check-cast v1, Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object v1, p1

    .line 2279
    check-cast v1, Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2282
    :cond_6
    if-eqz p1, :cond_0

    .line 2283
    const/4 p1, 0x0

    goto :goto_0

    .line 2252
    :cond_7
    invoke-virtual {p1, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .restart local v0    # "i":I
    :cond_8
    move-object v1, p1

    .line 2260
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->unbindDrawables(Landroid/view/View;)V

    .line 2259
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2265
    :cond_9
    check-cast p1, Landroid/widget/AdapterView;

    .end local p1    # "root":Landroid/view/View;
    invoke-virtual {p1, v3}, Landroid/widget/AdapterView;->setAdapter(Landroid/widget/Adapter;)V

    .line 2266
    const/4 p1, 0x0

    .restart local p1    # "root":Landroid/view/View;
    goto :goto_3
.end method

.method updateBox(ZF)V
    .locals 17
    .param p1, "save"    # Z
    .param p2, "distance"    # F

    .prologue
    .line 1176
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    if-eqz v10, :cond_0

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    if-nez v10, :cond_2

    .line 1177
    :cond_0
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    move-result-wide v10

    double-to-float v10, v10

    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1178
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    move-result-wide v10

    double-to-float v10, v10

    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1278
    :cond_1
    :goto_0
    return-void

    .line 1182
    :cond_2
    if-eqz p1, :cond_6

    .line 1183
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v12, v12

    add-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    .line 1184
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v13, v13

    add-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    sub-float/2addr v12, v13

    .line 1185
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v14, v14

    add-float/2addr v13, v14

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v14, v15

    add-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    add-float/2addr v13, v14

    .line 1186
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v15, v15

    add-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v14, v15

    const/high16 v15, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    move/from16 v16, v0

    mul-float v15, v15, v16

    const/high16 v16, 0x40000000    # 2.0f

    div-float v15, v15, v16

    add-float/2addr v14, v15

    .line 1183
    invoke-virtual {v10, v11, v12, v13, v14}, Landroid/graphics/RectF;->intersects(FFFF)Z

    move-result v10

    .line 1186
    if-eqz v10, :cond_6

    .line 1188
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v3

    .line 1189
    .local v3, "height":F
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v12, v12

    add-float/2addr v11, v12

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    add-float/2addr v11, v12

    iput v11, v10, Landroid/graphics/RectF;->top:F

    .line 1190
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    add-float/2addr v11, v3

    iput v11, v10, Landroid/graphics/RectF;->bottom:F

    .line 1191
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v11, v11

    cmpl-float v10, v10, v11

    if-lez v10, :cond_3

    .line 1192
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v12, v12

    add-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    iput v11, v10, Landroid/graphics/RectF;->bottom:F

    .line 1193
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v11, v3

    iput v11, v10, Landroid/graphics/RectF;->top:F

    .line 1195
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    .line 1197
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v12, v12

    add-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    .line 1198
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v13, v13

    add-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    sub-float/2addr v12, v13

    .line 1199
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v14, v14

    add-float/2addr v13, v14

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v14, v15

    add-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    add-float/2addr v13, v14

    .line 1200
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v15, v15

    add-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v14, v15

    const/high16 v15, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    move/from16 v16, v0

    mul-float v15, v15, v16

    const/high16 v16, 0x40000000    # 2.0f

    div-float v15, v15, v16

    add-float/2addr v14, v15

    .line 1197
    invoke-virtual {v10, v11, v12, v13, v14}, Landroid/graphics/RectF;->intersects(FFFF)Z

    move-result v10

    .line 1200
    if-eqz v10, :cond_6

    .line 1202
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v10}, Landroid/graphics/RectF;->width()F

    move-result v9

    .line 1203
    .local v9, "width":F
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v11, v11

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    add-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    iput v11, v10, Landroid/graphics/RectF;->right:F

    .line 1204
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->right:F

    sub-float/2addr v11, v9

    iput v11, v10, Landroid/graphics/RectF;->left:F

    .line 1205
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    const/4 v11, 0x0

    cmpg-float v10, v10, v11

    if-gez v10, :cond_4

    .line 1206
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v11, v11

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    add-float/2addr v11, v12

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v12, v13

    add-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    add-float/2addr v11, v12

    iput v11, v10, Landroid/graphics/RectF;->left:F

    .line 1207
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->left:F

    add-float/2addr v11, v9

    iput v11, v10, Landroid/graphics/RectF;->right:F

    .line 1209
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    .line 1210
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v12, v12

    add-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    .line 1211
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v13, v13

    add-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    sub-float/2addr v12, v13

    .line 1212
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v14, v14

    add-float/2addr v13, v14

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v14, v15

    add-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v14, v15

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    add-float/2addr v13, v14

    .line 1213
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v15, v15

    add-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v14, v15

    const/high16 v15, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    move/from16 v16, v0

    mul-float v15, v15, v16

    const/high16 v16, 0x40000000    # 2.0f

    div-float v15, v15, v16

    add-float/2addr v14, v15

    .line 1210
    invoke-virtual {v10, v11, v12, v13, v14}, Landroid/graphics/RectF;->intersects(FFFF)Z

    move-result v10

    .line 1213
    if-eqz v10, :cond_5

    .line 1215
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v11, v11

    add-float v6, v10, v11

    .line 1216
    .local v6, "spaceLeft":F
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v10, v10

    sub-float/2addr v10, v6

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v11, v12

    sub-float v7, v10, v11

    .line 1217
    .local v7, "spaceRight":F
    cmpl-float v10, v6, v7

    if-lez v10, :cond_f

    .line 1218
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    const/4 v11, 0x0

    iput v11, v10, Landroid/graphics/RectF;->left:F

    .line 1219
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->left:F

    add-float/2addr v11, v9

    iput v11, v10, Landroid/graphics/RectF;->right:F

    .line 1226
    .end local v6    # "spaceLeft":F
    .end local v7    # "spaceRight":F
    :cond_5
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    .line 1230
    .end local v3    # "height":F
    .end local v9    # "width":F
    :cond_6
    new-instance v5, Landroid/graphics/RectF;

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v12, v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v13, v13

    invoke-direct {v5, v10, v11, v12, v13}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1231
    .local v5, "screenRect":Landroid/graphics/RectF;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->relativeCoordinate()Landroid/graphics/RectF;

    move-result-object v2

    .line 1232
    .local v2, "bitmapRect":Landroid/graphics/RectF;
    invoke-virtual {v5, v2}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v10

    if-nez v10, :cond_b

    .line 1233
    const/4 v10, 0x0

    cmpl-float v10, p2, v10

    if-nez v10, :cond_7

    .line 1234
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    float-to-int v10, v10

    int-to-float v0, v10

    move/from16 p2, v0

    .line 1236
    :cond_7
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v11, v11

    cmpg-float v10, v10, v11

    if-gez v10, :cond_8

    .line 1237
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    sub-float v11, v11, p2

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    invoke-interface {v10, v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onChangePan(FF)V

    .line 1238
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1240
    :cond_8
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v11, v12

    add-float/2addr v10, v11

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    sub-int/2addr v11, v12

    int-to-float v11, v11

    cmpl-float v10, v10, v11

    if-lez v10, :cond_9

    .line 1241
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    add-float v11, v11, p2

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    invoke-interface {v10, v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onChangePan(FF)V

    .line 1242
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    sub-int/2addr v10, v11

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v11, v12

    sub-float/2addr v10, v11

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    move-result-wide v10

    double-to-float v10, v10

    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1244
    :cond_9
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v11, v11

    cmpg-float v10, v10, v11

    if-gez v10, :cond_a

    .line 1245
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    sub-float/2addr v12, v13

    invoke-interface {v10, v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onChangePan(FF)V

    .line 1246
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1248
    :cond_a
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v10, v11

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v11, v12

    int-to-float v11, v11

    cmpl-float v10, v10, v11

    if-lez v10, :cond_b

    .line 1249
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    add-float v8, v10, v11

    .line 1250
    .local v8, "tempY":F
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v13, v14

    add-float/2addr v12, v13

    invoke-interface {v10, v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onChangePan(FF)V

    .line 1251
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    sub-float v10, v8, v10

    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1252
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v10, v11

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v11, v12

    int-to-float v11, v11

    cmpl-float v10, v10, v11

    if-lez v10, :cond_b

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaY:F

    cmpl-float v10, v10, v11

    if-nez v10, :cond_b

    .line 1253
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v10, v11

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    sub-float/2addr v10, v11

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    move-result-wide v10

    double-to-float v10, v10

    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1258
    .end local v8    # "tempY":F
    :cond_b
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    move-result-wide v10

    double-to-float v10, v10

    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1259
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    move-result-wide v10

    double-to-float v10, v10

    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1260
    if-eqz p1, :cond_c

    .line 1261
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F

    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaSaveX:F

    .line 1262
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxSaveStartX:F

    .line 1263
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxSaveStartY:F

    .line 1265
    :cond_c
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mReferenceBackground:Landroid/graphics/Bitmap;

    if-eqz v10, :cond_1

    .line 1266
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRatio:F

    const/high16 v11, 0x3f800000    # 1.0f

    cmpg-float v10, v10, v11

    if-gtz v10, :cond_1

    .line 1267
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    const/4 v11, 0x0

    cmpg-float v10, v10, v11

    if-gez v10, :cond_d

    .line 1268
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    .line 1270
    :cond_d
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    const/4 v11, 0x0

    cmpg-float v10, v10, v11

    if-gez v10, :cond_e

    .line 1271
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    .line 1273
    :cond_e
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mReferenceBackground:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    float-to-int v11, v11

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    float-to-int v12, v12

    .line 1274
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v13, v14

    float-to-int v13, v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    float-to-int v14, v14

    .line 1273
    invoke-static {v10, v11, v12, v13, v14}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1275
    .local v4, "mCropBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v10, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setReferenceBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 1221
    .end local v2    # "bitmapRect":Landroid/graphics/RectF;
    .end local v4    # "mCropBitmap":Landroid/graphics/Bitmap;
    .end local v5    # "screenRect":Landroid/graphics/RectF;
    .restart local v3    # "height":F
    .restart local v6    # "spaceLeft":F
    .restart local v7    # "spaceRight":F
    .restart local v9    # "width":F
    :cond_f
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v11, v11

    iput v11, v10, Landroid/graphics/RectF;->right:F

    .line 1222
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->right:F

    sub-float/2addr v11, v9

    iput v11, v10, Landroid/graphics/RectF;->left:F

    goto/16 :goto_1
.end method

.method public updateFrameBuffer()V
    .locals 3

    .prologue
    .line 904
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v0, :cond_0

    .line 905
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getStrokeBoxRectF()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->updateFrameBuffer(ZLandroid/graphics/RectF;)V

    .line 907
    :cond_0
    return-void
.end method

.method updateLayout()V
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 1281
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v3

    .line 1282
    .local v3, "width":F
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v0

    .line 1283
    .local v0, "height":F
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    cmpg-float v4, v4, v6

    if-gez v4, :cond_0

    .line 1284
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iput v6, v4, Landroid/graphics/RectF;->left:F

    .line 1285
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iput v3, v4, Landroid/graphics/RectF;->right:F

    .line 1287
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    .line 1288
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v5, v5

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 1289
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v5, v5

    sub-float/2addr v5, v3

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 1291
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    cmpg-float v4, v4, v6

    if-gez v4, :cond_2

    .line 1292
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iput v6, v4, Landroid/graphics/RectF;->top:F

    .line 1293
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iput v0, v4, Landroid/graphics/RectF;->bottom:F

    .line 1295
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadMargin:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    .line 1296
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadMargin:F

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 1297
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadMargin:F

    add-float/2addr v5, v0

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    .line 1299
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_4

    .line 1300
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v5, v5

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    .line 1301
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v5, v5

    sub-float/2addr v5, v0

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 1303
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->BUTTON_HEIGHT:I

    int-to-float v9, v9

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1304
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isArabic:Z

    if-eqz v4, :cond_7

    .line 1305
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    .line 1306
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    add-float/2addr v7, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1311
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v4, :cond_5

    .line 1312
    new-instance v1, Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    invoke-direct {v1, v4}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 1313
    .local v1, "tmpRect":Landroid/graphics/RectF;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    neg-float v4, v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    neg-float v5, v5

    invoke-virtual {v1, v4, v5}, Landroid/graphics/RectF;->offset(FF)V

    .line 1314
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v4, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->updateRect(Landroid/graphics/RectF;)V

    .line 1317
    .end local v1    # "tmpRect":Landroid/graphics/RectF;
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    if-eqz v4, :cond_6

    .line 1318
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    .line 1319
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    float-to-double v4, v4

    .line 1318
    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    .line 1319
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    .line 1318
    invoke-direct {v2, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1320
    .local v2, "totalLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1321
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1322
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mRelative:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1324
    .end local v2    # "totalLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_6
    return-void

    .line 1308
    :cond_7
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMoveArea:F

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    .line 1309
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    .line 1308
    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0
.end method

.method public updatePad()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/high16 v12, 0x40000000    # 2.0f

    .line 825
    const-string v6, "ZoomPad"

    const-string/jumbo v7, "updatePad"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 826
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v12

    div-float/2addr v8, v12

    sub-float/2addr v7, v8

    .line 827
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v9, v9

    add-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v9, v12

    div-float/2addr v9, v12

    sub-float/2addr v8, v9

    .line 828
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v10, v10

    add-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v10, v12

    div-float/2addr v10, v12

    add-float/2addr v9, v10

    .line 829
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v11, v12

    div-float/2addr v11, v12

    add-float/2addr v10, v11

    .line 826
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->intersects(FFFF)Z

    move-result v6

    .line 829
    if-eqz v6, :cond_3

    .line 831
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 832
    .local v1, "height":F
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v12

    div-float/2addr v8, v12

    add-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/RectF;->top:F

    .line 833
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    add-float/2addr v7, v1

    iput v7, v6, Landroid/graphics/RectF;->bottom:F

    .line 834
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-lez v6, :cond_0

    .line 835
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v12

    div-float/2addr v8, v12

    sub-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/RectF;->bottom:F

    .line 836
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v7, v1

    iput v7, v6, Landroid/graphics/RectF;->top:F

    .line 838
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    .line 840
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v12

    div-float/2addr v8, v12

    sub-float/2addr v7, v8

    .line 841
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v9, v9

    add-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v9, v12

    div-float/2addr v9, v12

    sub-float/2addr v8, v9

    .line 842
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v10, v10

    add-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v10, v12

    div-float/2addr v10, v12

    add-float/2addr v9, v10

    .line 843
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v11, v12

    div-float/2addr v11, v12

    add-float/2addr v10, v11

    .line 840
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->intersects(FFFF)Z

    move-result v6

    .line 843
    if-eqz v6, :cond_3

    .line 845
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v5

    .line 846
    .local v5, "width":F
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v12

    div-float/2addr v8, v12

    sub-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/RectF;->right:F

    .line 847
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    sub-float/2addr v7, v5

    iput v7, v6, Landroid/graphics/RectF;->left:F

    .line 848
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    cmpg-float v6, v6, v13

    if-gez v6, :cond_1

    .line 849
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v12

    div-float/2addr v8, v12

    add-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/RectF;->left:F

    .line 851
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    add-float/2addr v7, v5

    iput v7, v6, Landroid/graphics/RectF;->right:F

    .line 853
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    .line 854
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v8, v12

    div-float/2addr v8, v12

    sub-float/2addr v7, v8

    .line 855
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v9, v9

    add-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v9, v12

    div-float/2addr v9, v12

    sub-float/2addr v8, v9

    .line 856
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v10, v10

    add-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v10, v12

    div-float/2addr v10, v12

    add-float/2addr v9, v10

    .line 857
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    add-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F

    mul-float/2addr v11, v12

    div-float/2addr v11, v12

    add-float/2addr v10, v11

    .line 854
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->intersects(FFFF)Z

    move-result v6

    .line 857
    if-eqz v6, :cond_2

    .line 859
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    int-to-float v7, v7

    add-float v3, v6, v7

    .line 860
    .local v3, "spaceLeft":F
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v6, v6

    sub-float/2addr v6, v3

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F

    mul-float/2addr v7, v8

    sub-float v4, v6, v7

    .line 861
    .local v4, "spaceRight":F
    cmpl-float v6, v3, v4

    if-lez v6, :cond_5

    .line 862
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iput v13, v6, Landroid/graphics/RectF;->left:F

    .line 863
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    add-float/2addr v7, v5

    iput v7, v6, Landroid/graphics/RectF;->right:F

    .line 870
    .end local v3    # "spaceLeft":F
    .end local v4    # "spaceRight":F
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    .line 876
    .end local v1    # "height":F
    .end local v5    # "width":F
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    if-eqz v6, :cond_4

    .line 878
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mState:I

    const/16 v7, 0x9

    if-ne v6, v7, :cond_6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isRefreshed()Z

    move-result v6

    if-nez v6, :cond_6

    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mUpdatePad:Z

    if-nez v6, :cond_6

    .line 879
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mUpdatePad:Z

    .line 880
    const-string v6, "ZoomPad"

    const-string v7, "don\'t do anything"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 900
    :cond_4
    :goto_1
    return-void

    .line 865
    .restart local v1    # "height":F
    .restart local v3    # "spaceLeft":F
    .restart local v4    # "spaceRight":F
    .restart local v5    # "width":F
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I

    int-to-float v7, v7

    iput v7, v6, Landroid/graphics/RectF;->right:F

    .line 866
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    sub-float/2addr v7, v5

    iput v7, v6, Landroid/graphics/RectF;->left:F

    goto :goto_0

    .line 882
    .end local v1    # "height":F
    .end local v3    # "spaceLeft":F
    .end local v4    # "spaceRight":F
    .end local v5    # "width":F
    :cond_6
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isRefreshed()Z

    move-result v6

    if-nez v6, :cond_7

    .line 883
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->refresh()V

    .line 885
    :cond_7
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 886
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_8

    .line 887
    const-string v6, "ZoomPad"

    const-string/jumbo v7, "updatePad bitmap is null"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 891
    :cond_8
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 892
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getStrokeBoxRectF()Landroid/graphics/RectF;

    move-result-object v2

    .line 893
    .local v2, "rect":Landroid/graphics/RectF;
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I

    neg-int v6, v6

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartY:I

    neg-int v7, v7

    int-to-float v7, v7

    invoke-virtual {v2, v6, v7}, Landroid/graphics/RectF;->offset(FF)V

    .line 894
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v9

    sub-float/2addr v8, v9

    div-float/2addr v7, v8

    .line 895
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v10

    sub-float/2addr v9, v10

    div-float/2addr v8, v9

    .line 894
    invoke-interface {v6, v0, v2, v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onCropBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;FF)V

    .line 896
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setPadLayer(Landroid/graphics/Bitmap;)V

    .line 897
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->update()V

    goto :goto_1
.end method
